<?php

namespace App;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class PostReport extends Model
{
    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    public $table='post_reports';
    protected $fillable = ['id','post_id' ,'reporter_id','status','created_at','updated_at'];
    
}
