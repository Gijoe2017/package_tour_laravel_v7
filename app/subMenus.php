<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class subMenus extends Model
{
    //
    public $table='manage_sub_menu';

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
