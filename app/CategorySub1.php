<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CategorySub1 extends Model
{
    //
    protected $dates = ['deleted_at'];
    public $table='categories_sub1';

    protected $fillable = [
        'category_sub1_id', 'category_id','sub1_name', 'language_code', 'active',
    ];

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }

    public function sub2category()
    {
        return $this->belongsTo('App\CategorySub2','category_sub1_id','category_sub1_id');
//        return $this->belongsTo('App\Public_album');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 'Y')->where('language_code',Auth::user()->language)->where('category_type',0);
    }
}
