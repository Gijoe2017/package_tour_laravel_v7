<?php

namespace App;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class PostMedia extends Model
{
    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    public $table='post_media';
    protected $fillable = ['id','post_id' ,'media_id','created_at','updated_at'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function media()
    {
        return $this->belongsTo('App\Media', 'media_id');
    }

}
