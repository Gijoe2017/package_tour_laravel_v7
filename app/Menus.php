<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Menus extends Model
{
    //
    public $table='manage_menus';

    public function sub_menu(){
//        return $this->hasMany('App\subMenus');
        return $this->belongsTo('App\subMenus','menu_id','menu_id');
    }

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
