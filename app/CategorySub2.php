<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CategorySub2 extends Model
{
    //
    protected $dates = ['deleted_at'];
    public $table='categories_sub2';

    protected $fillable = [
        'category_sub2_id','category_sub1_id', 'category_id','category_sub2_name', 'language_code', 'active',
    ];

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }

    public function sub_categories()
    {
        return $this->belongsTo('App\CategorySub1');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 'Y')->where('language_code',Auth::user()->language);
    }
}
