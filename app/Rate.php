<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Rate extends Model
{
    //
    public $table='package_tour_commission_rate';

    public function get_rate($rate){
        $Rate=DB::table('package_tour_commission_rate')
            ->where('price_rate_start','<=',$rate)
            ->where('price_rate_end','>=',$rate)
            ->first();

        if(!$Rate){
            $Rate=DB::table('package_tour_commission_rate')
                ->orderby('commission_rate','asc')
                ->first();
        }
        return $Rate->commission_rate;

    }
}
