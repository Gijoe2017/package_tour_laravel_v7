<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentNotification extends Model
{
    //
    public $table="payment_notification";

    public function notification_invoice(){

//        return $this->belongsTo('App\PaymentNotificationSub', 'payment_id');
        return $this->hasMany('App\PaymentNotificationSub', 'payment_id', 'payment_id');
    }
}
