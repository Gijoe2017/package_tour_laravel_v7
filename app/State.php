<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class State extends Model
{
    //
    public $table='states';

    public function scopeActive($query)
    {
        $user = User::where('id', '=', Auth::user()->id)->first();
        //  dd($user);
        return $query->where('language_code',$user->language);
    }
}
