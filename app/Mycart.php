<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;


class Mycart extends Model
{
    //
    public $table="package_booking_cart";


    public function count_cart()
    {
        return $this->belongsTo('App\Mycart_details','booking_cart_id','booking_cart_id');
    }
}
