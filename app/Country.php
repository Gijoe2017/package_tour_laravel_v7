<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Country extends Model
{
    //
    public $table='countries';

//    protected $fillable = [
//        'country','country_id','language_code'
//    ];

    public function country()
    {
        return $this->belongsTo('App\Country')->withDefault();

    }

    public function city()
    {
        return $this->belongsTo('App\City', 'country_id', 'country_id');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'country_id','country_id');
    }

    public function get_state()
    {
        return $this->belongsTo('App\State', 'country_id','country_id');
    }

    public function scopeActive($query)
    {
        // $user = User::where('id', '=', Auth::user()->id)->first();
        //  dd($user);
        return $query->where('language_code',Auth::user()->language);
    }
}
