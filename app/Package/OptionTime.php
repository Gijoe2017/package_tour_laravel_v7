<?php

namespace App\Package;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OptionTime extends Model
{
    //
    public $table="package_times";

    public function scopeActive($query)
    {
        return $query->where('LanguageCode',Auth::user()->language);
    }
}
