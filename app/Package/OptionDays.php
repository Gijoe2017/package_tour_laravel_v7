<?php

namespace App\Package;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OptionDays extends Model
{
    //
    public $table="package_days";

    public function scopeActive($query)
    {
        return $query->where('LanguageCode',Auth::user()->language);
    }
}
