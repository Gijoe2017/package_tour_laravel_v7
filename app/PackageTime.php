<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Session;

class PackageTime extends Model
{
    public $table='package_times';

    public function scopeActive($query)
    {
        return $query->where('LanguageCode',Session::get('language'));
    }
}
