<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Policy extends Model
{
    //
    public $table="privacy_policys";

    public function scopeActive($query)
    {
        return $query->where('language_code',Session::get('language')?Session::get('language'):'th');
    }
}
