<?php
// app/Http/Middleware/Language.php

namespace App\Http\Middleware;

use App\Setting;
use Carbon\Carbon;
use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App;
use Auth;
use Config;
use Schema;
use Session;

class Language
{
    public function handle($request, Closure $next)
    {
        if(Schema::hasTable('settings')) {

            if(Auth::check()){
                if(Auth::user()->language != null){
                    App::setLocale(Auth::user()->language);
                }else{
                    App::setLocale(Setting::get('language', 'th'));
                }
            }else{
                App::setLocale(Session::get('language'));
//                App::setLocale(Setting::get('language', 'th'));
            }

        }else{
            App::setLocale(Session::get('language'));
        }

         Carbon::setLocale(Session::get('language'));
         return $next($request);
    }
}
?>