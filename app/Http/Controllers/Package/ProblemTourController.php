<?php

namespace App\Http\Controllers\Package;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Session;

class ProblemTourController extends Controller
{
    //
    public function problem_invoice($id){
        $data=array(
            'booking_id'=>$id,
            'problem'=>trans('common.report_a_problem_with_no_payment_notification_from_this_booking'),
            'send_by'=>Auth::user()->id,
            'send_date'=>date('Y-m-d H:i:s'),
            'status'=>'1'
        );
        DB::table('package_booking_problems')->insert($data);
        Session::flash('message',trans('common.the_problem_has_been_submitted_to_the_system_inspector'));
        return redirect()->back();
    }
}
