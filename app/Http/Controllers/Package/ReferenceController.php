<?php

namespace App\Http\Controllers\Package;

use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;

class ReferenceController extends Controller
{
    //
    public function reference_list(){

        $Reference=DB::table('users_passport_reference1 as a')
            ->join('users_passport_reference2 as d','d.reference_id','=','a.reference_id')
            ->join('users_title as b','b.user_title_id','=','a.user_title_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','c.country','b.user_title','d.*')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('d.language_code',Auth::user()->language)
//            ->where('b.language_code',Auth::user()->language)
//            ->where('c.language_code',Auth::user()->language)
                ->groupby('d.language_code')
            ->get();
       // dd($Reference);
        Session::put('mode_lang','reference');
        return view('packages.order_member.reference.list')
            ->with('Reference',$Reference);

    }

    public function change_language($id){
        $Reference=DB::table('users_passport_reference2')
            ->where('reference_id',$id)
            ->groupby('language_code')
            ->get();
        // dd($MemberLang);
        return view('member.reference.ref-show-lang')
            ->with('Reference',$Reference)
            ->with('reference_id',$id);
    }


    public function create(){
        $Phones=DB::table('users_passport_reference_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        return view('packages.order_member.reference.create')->with('Phones',$Phones);
    }

    public function edit($id){
        $Phones=DB::table('users_passport_reference_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $Reference=DB::table('users_passport_reference1 as a')
            ->where('a.reference_id',$id)
            ->first();

        $ReferenceInfo=DB::table('users_passport_reference2')
            ->where('language_code',Auth::user()->language)
            ->where('reference_id',$id)
            ->first();

        //dd($ReferenceInfo);
        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Reference->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Reference->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Reference->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Reference->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Reference->country_id)
                ->where('state_id',$Reference->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Reference->country_id)
                    ->where('state_id',$Reference->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Reference->country_id)
                    ->where('state_id',$Reference->state_id)
                    ->where('city_id',$Reference->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Reference->country_id)
                        ->where('state_id',$Reference->state_id)
                        ->where('city_id',$Reference->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }

        //dd($Reference);
        return view('packages.order_member.reference.edit')
            ->with('Reference',$Reference)
            ->with('ReferenceInfo',$ReferenceInfo)
            ->with('state',$state)
            ->with('country',$country)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('Phones',$Phones);
    }

    public function store(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'reference_first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }


                $data = array(
                    'passport_id' => Session::get('passport_id'),
                    'user_title_id' => $post['user_title_id'],
                    'country_id' => $post['country_id'],
                    'state_id' => $state,
                    'city_id' => $city,
                    'city_sub1_id' => $citySub,
                    'zip_code' => $zip_code,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_user_id' => Auth::user()->id,
                    'updated_user_id' => Auth::user()->id,
                    'language_code' => Auth::user()->language,
                );
                // dd($data);
                $reference_id=DB::table('users_passport_reference1')->insertGetId($data);





            $data = array(
                'reference_id' => $reference_id,
                'passport_id' => Session::get('passport_id'),
                'reference_first_name' => $post['reference_first_name'],
                'reference_middle_name' => $post['reference_middle_name'],
                'reference_last_name' =>$post['reference_last_name'],
                'reference_address' => $post['reference_address'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,
                'language_code' => Auth::user()->language,
            );
            // dd($data);
            DB::table('users_passport_reference2')->insert($data);

            if($request->phone_country_id) {

                $check = DB::table('users_passport_reference_phone')
                    ->where('phone_number', $request->phone_number)
                    ->where('passport_id', Session::get('passport_id'))
                    ->first();
                // dd($check);
                if (!$check) {
                    $data = array(
                        'passport_id' => Session::get('passport_id'),
                        'country_id' => $request->phone_country_id,
                        'phone_country_code' => $request->phone_country_code,
                        'phone_number' => $request->ref_phone_number,
                        'phone_type' => $request->phone_type,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_user_id' => Auth::user()->id,
                        'updated_user_id' => Auth::user()->id,
                        'active' => '1',
                    );
                    DB::table('users_passport_reference_phone')->insert($data);
                    // dd($data);
                }
            }
            return redirect('/package/reference');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'reference_first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }


                $data = array(
                    'passport_id' => Session::get('passport_id'),
                    'user_title_id' => $post['user_title_id'],
                    'country_id' => $post['country_id'],
                    'state_id' => $state,
                    'city_id' => $city,
                    'city_sub1_id' => $citySub,
                    'zip_code' => $zip_code,
                    'updated_user_id' => Auth::user()->id,
                    'language_code' => Auth::user()->language,
                );
              //   dd($data);
                DB::table('users_passport_reference1')->where('reference_id',$post['reference_id'])->update($data);



            $data = array(
                'reference_id' => $post['reference_id'],
                'passport_id' => Session::get('passport_id'),
                'reference_first_name' => $post['reference_first_name'],
                'reference_middle_name' => $post['reference_middle_name'],
                'reference_last_name' =>$post['reference_last_name'],
                'reference_address' => $post['reference_address'],
                'updated_user_id' => Auth::user()->id,
                'language_code' => Auth::user()->language
            );
            // dd($data);
            $check=DB::table('users_passport_reference2')
                ->where('reference_id',$post['reference_id'])
                ->where('language_code',Auth::user()->language)
                ->first();
            if($check){
                DB::table('users_passport_reference2')
                    ->where('reference_id',$post['reference_id'])
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }else{
                DB::table('users_passport_reference2')->insert($data);
            }

            if($request->phone_country_id){
                $check=DB::table('users_passport_reference_phone')
                    ->where('phone_number',$request->ref_phone_number)
                    ->where('passport_id',Session::get('passport_id'))
                    ->first();
                // dd($check);
                if(!$check){
                    $data=array(
                        'passport_id'=>Session::get('passport_id'),
                        'country_id'=>$request->phone_country_id,
                        'phone_country_code'=>$request->phone_country_code,
                        'phone_number'=>$request->ref_phone_number,
                        'phone_type'=>$request->phone_type,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_user_id'=>Auth::user()->id,
                        'updated_user_id'=>Auth::user()->id,
                        'active'=>'1',
                    );
                    //  dd($data);

                    DB::table('users_passport_reference_phone')->insert($data);
                    //dd($data);
                }
            }





            return redirect('/package/reference');
        }
    }



    public function delete($id){
        $check=DB::table('users_passport_reference1')
            ->where('reference_id',$id)
            ->first();

        DB::table('users_passport_reference1')
            ->where('reference_id',$id)
            ->delete();
        DB::table('users_passport_reference2')
            ->where('reference_id',$id)
            ->delete();
        DB::table('users_passport_reference_phone')
            ->where('passport_id',$check->passport_id)
            ->delete();
        return redirect()->back();
    }

    public function save_phone(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'country_id'=>$request->phone_country_id,
                'phone_country_code'=>$request->phone_country_code,
                'phone_number'=>$request->phone_number,
                'phone_type'=>$request->phone_type,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_user_id'=>Auth::user()->id,
                'updated_user_id'=>Auth::user()->id,
                'active'=>'1',
            );
            DB::table('users_passport_reference_phone')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Phones=DB::table('users_passport_reference_phone as a')
                ->join('countries as b','b.country_id','=','a.country_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->where('b.language_code',Auth::user()->language)
                ->get();
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Country')."</th>
                                <th>".trans('profile.phone_code')."</th>
                                <th>".trans('profile.phone_type')."</th>
                                <th>".trans('profile.phone_name')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Phones as $phone){
                $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm ref-phone-delete\" id=\"ref-phone-delete\" data-id=\"".$phone->id."\"  ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";
            }
            $output.=" </tbody></table>";
            return Response($output);
        }
    }

    public function delete_phone(Request $request){

        DB::table('users_passport_reference_phone')->where('id',$request->id)->delete();
        $Phones=DB::table('users_passport_reference_phone')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('profile.Country')."</th>
                                        <th>".trans('profile.phone_code')."</th>
                                        <th>".trans('profile.phone_type')."</th>
                                        <th>".trans('profile.phone_name')."</th>
                                        <th>".trans('profile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
        foreach ($Phones as $phone){
            $output.="<tr>
                        <td>$phone->country_id</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                        <td align='center'><button class=\"btn btn-danger btn-sm ref-phone-delete\" id=\"ref-phone-delete\" data-id=\"".$phone->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }
}
