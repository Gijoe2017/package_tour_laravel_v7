<?php

namespace App\Http\Controllers\Package;

use App\Timeline;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Intervention\Image\Response;
use Session;
use Auth;
use Cookie;

class AgencyController extends Controller
{
    //
    public function list_agency(){
        \App::setLocale(Session::get('language'));
        $Agency=DB::table('business_verified_modules')
            ->where('module_type_id','1')
            ->where('business_verified_status','verified')
            ->get();

//        if(!Session::get('PackageCountry')){
//            $PackageCountry=DB::table('package_details as a')
//                ->join('package_tourin as c','c.packageID','=','a.packageID')
//                ->join('countries as b','b.country_id','=','c.CountryCode')
//                ->select('b.*')
//                ->where('a.packageDateStart','>',date('Y-m-d'))
//                ->where('a.status','Y')
//                ->where('b.language_code',Session::get('language'))
//                ->groupby('b.country_id')
//                ->get();
//            // dd($PackageCountry);
//
//            Session::put('PackageCountry',$PackageCountry);
//        }

        return view('packages.agency.list')
//            ->with('PackageCountry',$PackageCountry)
            ->with('Agency',$Agency);
    }

    public function list_partner(){
        $Partners=DB::table('package_tour_partner')
            ->where('partner_id',Auth::user()->timeline_id)
            ->get();

        $Requests=DB::table('package_tour_partner')
            ->where('request_partner_id',Auth::user()->timeline_id)
            ->get();

       // dd($Partners);
        $PackageCountry=DB::table('package_details as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('b.*')
            ->where('status','Y')
            ->where('language_code',Session::get('language'))
            ->groupby('Country_id')
            ->get();

        return view('packages.partner.list')
            ->with('PackageCountry',$PackageCountry)
            ->with('Requests',$Requests)
            ->with('Partners',$Partners);
    }

    public function accept_partner($id){
        DB::table('package_tour_partner')
            ->where('request_partner_id',$id)
            ->where('partner_id',Auth::user()->timeline_id)
            ->update(['request_status'=>'accept']);

        return back();
    }


    public function partner_form($id){
        $timeline=Timeline::where('id',$id)->first();
        $timeline_me=Timeline::where('id',Auth::user()->timeline_id)->first();
        dd($timeline);
        return view('packages.agency.partner-form')
            ->with('timeline_me',$timeline_me)
            ->with('timeline',$timeline);
    }

    public function send_request(Request $request){
        $data=array(
            'request_partner_id'=>intval($request->timeline_id),
            'partner_id'=>Auth::user()->timeline_id,
            'request_date'=>date('Y-m-d H:i:s'),
            'request_status'=>'send',
            'action_status_date'=>date('Y-m-d H:i:s'),
        );


        $check=DB::table('package_tour_partner')
            ->where('request_partner_id',$request->timeline_id)
            ->where('partner_id',Auth::user()->timeline_id)
            ->first();

        if(!$check){
           DB::table('package_tour_partner')->insert($data);
        }

        return Response('Success!');
    }

}
