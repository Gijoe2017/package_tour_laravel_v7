<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Http\Response;
use Session;
use Image;
use App;

class ConditionController extends Controller
{

    public function ShowCondition($id){
        $Condition=DB::table('package_condition')
            ->where('LanguageCode',Session::get('Language'))
            ->where('packageDescID',$id)->get();
        $ConValue=DB::table('package_details')->where('packageDescID',$id)->first();
      
        return view('ajax.condition')
            ->with('ConValue',$ConValue)
            ->with('packageDescID',$id)
            ->with('Condition',$Condition);
    }


    public function FormCondition($id){
        Session::put('packageDescID',$id);
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('b.LanguageCode',Auth::user()->language)
            ->where('a.packageID',Session::get('package'))->first();
        $Condition=DB::table('package_details')->where('packageDescID',Session::get('packageDescID'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        //dd($Condition);
        if(isset($Condition)){
        //    dd($Package);
            return view('module.packages.condition.edit-form')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package);
        }else{
            return view('module.packages.condition.form')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package);
        }

    }

    public function Edit_Condition($id){
        Session::put('packageDescID',$id);
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('b.LanguageCode',Auth::user()->language)
            ->where('a.packageID',Session::get('package'))->first();
        $Condition=DB::table('package_details')->where('packageDescID',Session::get('packageDescID'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        if(isset($Condition)){
            return view('packages.setting.edit-condition')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package);
        }else{
            return view('packages.setting.edit-condition')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package);
        }

    }

    public function SaveFormCondition(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'BookingBefore_travel'=>'required',
            'BookingDeposit'=>'required',
            'PaymentBefore'=>'required',
            'CancelBefore'=>'required',
            'Cancellation'=>'required',
            'CancellationPrice'=>'required',
            'CancelSmallthan'=>'required',
            'Agepassport'=>'required'
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            $data = array(
                'BookingBefore_travel'=>$post['BookingBefore_travel'],
                'BookingDeposit'=>$post['BookingDeposit'],
                'PaymentBefore'=>$post['PaymentBefore'],
                'CancelBefore'=>$post['CancelBefore'],
                'Cancellation'=>$post['Cancellation'],
                'CancellationPrice'=>$post['CancellationPrice'],
                'CancelSmallthan'=>$post['CancelSmallthan'],
                'Agepassport'=>$post['Agepassport']
            );

            DB::table('package_details')->where('packageDescID',Session::get('packageDescID'))->update($data);
            // return redirect('package/details');
            return redirect('package/details');
        }
    }

    public function SaveEditCondition(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'BookingBefore_travel'=>'required',
            'BookingDeposit'=>'required',
            'PaymentBefore'=>'required',
            'CancelBefore'=>'required',
            'Cancellation'=>'required',
            'CancellationPrice'=>'required',
            'CancelSmallthan'=>'required',
            'Agepassport'=>'required'
        ]);
        if($v->fails()){
           // dd($v);
            return redirect()->back()->withErrors($v->errors());
        }else{

            $data = array(
                'BookingBefore_travel'=>$post['BookingBefore_travel'],
                'BookingDeposit'=>$post['BookingDeposit'],
                'PaymentBefore'=>$post['PaymentBefore'],
                'CancelBefore'=>$post['CancelBefore'],
                'Cancellation'=>$post['Cancellation'],
                'CancellationPrice'=>$post['CancellationPrice'],
                'CancelSmallthan'=>$post['CancelSmallthan'],
                'Agepassport'=>$post['Agepassport']
            );

            DB::table('package_details')->where('packageDescID',Session::get('packageDescID'))->update($data);
            return redirect('package/details');
        }


    }

    public function MoreCondition($id){
        Session::put('packageDescID',$id);
        $Condition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->get();
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();

//        $Condition=DB::table('condition_in_package_details as a')
//            ->join('package_condition as b','b.id','=','a.condition_id')
//            ->join('condition_group as c','c.condition_group_id','=','b.condition_group_id')
//            ->select('b.*','c.condition_group_title')
//            ->where('a.packageDescID',Session::get('packageDescID'))
////            ->where('b.formula_id','!=','0')
//            ->orderby('b.condition_group_id','asc')
//            ->get();
      //  echo Session::get('timeline_id');
        $Condition=DB::table('package_condition as a')
            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
            ->select('a.*','b.condition_group_title')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
          //  ->groupby('a.formula_id')
            ->orderby('a.condition_group_id','asc')
            ->get();
       // dd($Condition);

        return view('module.packages.condition.more')
            ->with('Condition',$Condition)
            ->with('Condition_group',$Condition_group)
            ->with('Unit',$Unit);
    }

    public function ViewCondition($id){
        Session::put('packageDescID',$id);
//        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Codition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->get();
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();

        $Condition=DB::table('condition_in_package_details as a')
            ->join('package_condition as b','b.condition_code','=','a.condition_id')
            ->join('condition_group as c','c.condition_group_id','=','b.condition_group_id')
            ->select('b.*','c.condition_group_title')
            ->where('a.packageDescID',Session::get('packageDescID'))
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
//            ->where('b.formula_id','!=','0')
//            ->groupby('b.condition_group_id')
            ->orderby('b.condition_group_id','asc')
            ->get();

        //dd($Condition);
//        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        return view('module.packages.condition.view')
            ->with('Condition',$Condition)
            ->with('Codition_group',$Codition_group)
            ->with('Unit',$Unit);
    }

    public function ListCondition(){

        App::setLocale(Auth::user()->language);
//        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))
            ->first();
      //  dd($Package);
        if($Package->owner_timeline_id){
            $timeline_id=$Package->owner_timeline_id;
        }else{
            $timeline_id=$Package->timeline_id;
        }

        $Condition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->orderby('order','asc')->get();
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();

        $Condition=DB::table('package_condition as a')
            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
            ->select('a.*','b.condition_group_title')
            ->where('a.timeline_id',$timeline_id)
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
            ->orderby('a.condition_group_id','asc')
            ->get();
//        echo $Package->owner_timeline_id;
       // dd($Condition);
//      $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        return view('module.packages.condition.list-condition-group')
            ->with('timeline_id',$timeline_id)
            ->with('Package',$Package)
            ->with('Condition',$Condition)
            ->with('Package',$Package)
            ->with('Condition_group',$Condition_group)
            ->with('Unit',$Unit);
    }

    public function ListConditions($id){
        Session::put('packageDescID',$id);
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Codition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->get();
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();
        //dd($Package);
        $Condition=DB::table('package_condition as a')
            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
            ->select('a.*','b.condition_group_title')
            ->where('a.timeline_id',$Package->owner_timeline_id)
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
            ->orderby('a.condition_group_id','asc')
            ->get();
       // dd($Condition);
//        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
      //  dd($Package);
        return view('module.packages.condition.list')
            ->with('Package',$Package)
            ->with('Condition',$Condition)
            ->with('Codition_group',$Codition_group)
            ->with('Unit',$Unit);
    }

    public function ListMoreCondition($id){
        App::setLocale(Auth::user()->language);
        Session::put('condition_group',$id);
        // dd(Session::get('package'));
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))
            ->first();
        //dd($Package);
        if($Package->owner_timeline_id){
            $timeline_id=$Package->owner_timeline_id;
        }else{
            $timeline_id=$Package->timeline_id;
        }
       // Session::put('timeline',$timeline_id);
        //dd($timeline_id);

        $ConditionIn=DB::table('package_condition as a')
            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
            ->select('a.*','b.condition_group_title')
            ->where('a.timeline_id',$timeline_id)
            ->where('a.condition_group_id',$id)
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
            ->whereIn('a.condition_code',function ($query) {
                $query->select('condition_id')->from('condition_in_package_details')
                    ->where('status','y')
                    ->where('packageID',Session::get('package'));
            })
            ->orderby('a.condition_group_id','asc')
            ->get();

        $Condition=DB::table('package_condition as a')
            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
            ->select('a.*','b.condition_group_title')
            ->where('a.timeline_id',$timeline_id)
            ->where('a.condition_group_id',$id)
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
            ->wherenotIn('a.condition_code',function ($query){
                $query->select('condition_id')->from('condition_in_package_details')
                    ->where('status','y')
                    ->where('packageID',Session::get('package'));
            })
            ->orderby('a.condition_group_id','asc')
            ->get();
//dd($Condition);
        //dd($timeline_id);
        $Condition_group=DB::table('condition_group')
            ->where('condition_group_id',Session::get('condition_group'))
            ->where('language_code',Auth::user()->language)
            ->first();
        //dd($Condition);
        return view('module.packages.condition.list-more')
            ->with('Package',$Package)
            ->with('Condition_group',$Condition_group)
            ->with('ConditionIn',$ConditionIn)
            ->with('Condition',$Condition);
    }

    public function UpdateStatusAllCondition(Request $request){
        $return="";$i=0;
        $Check=DB::table('package_condition')
            ->where('timeline_id',$request->timeline_id)
            ->where('condition_group_id',Session::get('condition_group'))
            ->get();

//        $Condition=DB::table('package_condition as a')
//            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
//            ->select('a.*','b.condition_group_title')
//            ->where('a.timeline_id',Session::get('timeline_id'))
//            ->where('a.condition_group_id',Session::get('condition_group'))
//            ->where('a.language_code',Auth::user()->language)
//            ->where('b.language_code',Auth::user()->language)
//            ->orderby('a.condition_group_id','asc')
//            ->get();

//        echo Session::get('timeline_id').Session::get('condition_group');
       // dd($Check);
        if($request->status=='y'){
            foreach ($Check as $rows){
                $data=array(
                    'packageID'=>Session::get('package'),
                    'condition_group_id'=>Session::get('condition_group'),
                    'condition_id'=>$rows->condition_code,
                    'status'=>'y'
                );
                $i=DB::table('condition_in_package_details')->insert($data);
            }
            if($i>0){
                $return ="Condition All Selected";
            }


        }else{
            foreach ($Check as $rows) {
                $i=DB::table('condition_in_package_details')
                    ->where('packageID', Session::get('package'))
                    ->where('condition_id', $rows->condition_code)
                    ->delete();
            }
            if($i>0) {
                $return = "Condition All Deleted";
            }

        }
        return $return;
    }

    public function UpdateStatusCondition(Request $request){
//        $i=DB::table('condition_in_package_details')->where('id',$request->id)->update(['status'=>$request->status]);
        $return=""; $i=0;
        if($request->status=='y'){
            $data=array(
                'packageID'=>Session::get('package'),
                'condition_group_id'=>Session::get('condition_group'),
                'condition_id'=>$request->id,
                'status'=>'y'
            );
            $i=DB::table('condition_in_package_details')->insert($data);
//            $i=DB::table('condition_in_package_details')
//                ->where('packageID',Session::get('package'))
//                ->where('condition_id',$request->id)
//                ->update(['status'=>$request->status]);
            if($i>0){
                $return ="Condition Selected";
            }
        }else{
            $i=DB::table('condition_in_package_details')
                ->where('packageID',Session::get('package'))
                ->where('condition_id',$request->id)
                ->delete();
            if($i>0){
                $return ="Condition Deleted";
            }
        }
        return $return;
    }

    public function deleteFromList($id){
        DB::table('condition_in_package_details')->where('id',$id)->delete();
          return  back();
    }

    public function setOperatorGroup(Request $request){
        $Operate=DB::table('operations')
            ->where('condition_group_id',$request->group)
            ->where('language_code',Auth::user()->language)
            ->get();

        $option="";
        if($Operate!=null){
            foreach ($Operate as $rows){
                $option.="<option value='".$rows->operator_code."'>".$rows->operator_title."</option>";
            }
        }

        return Response($option);

    }

    public function setOperatorSub(Request $request){
        $check=DB::table('operations')->where('operator_code',$request->operator_code)->first();
        $Sub=DB::table('operation_sub')->where('condition_group_id',$check->condition_group_id)->where('language_code',Auth::user()->language)->get();
        $str='';
        $i=0;
        foreach ($Sub as $rows){
            $i++;
            if($i==3){
                $required='';
            }else{
                $required='required';
            }
            $str.="<div class=\"deposit col-md-12\">
                        <div class=\"col-md-7\">
                        <input type=\"hidden\" name=\"operation_sub_id[]\" value=\"".$rows->operation_sub_id."\" >
                            <label class=\"radio-inline\">
                               $rows->operator_title
                            </label>
                        </div>
                        <div class=\"col-md-2\">
                            <input type=\"text\" id=\"number_of_day".$i."\" name=\"number_of_day[]\"  class=\"number_of_day form-control form-control-sm text-center\" placeholder=\"".trans('common.number_of_days')."\" ".$required.">
                        </div>
                    </div>";
        }

        return Response($str);
    }

    public function createCondition_new($id){
        App::setLocale(Auth::user()->language);
        Session::put('condition_group',$id);

        $Deposit=DB::table('package_condition as a')
            ->join('operations as b','b.condition_group_id','=','a.condition_group_id')
            ->join('mathematical_formula as c','c.formula_id','=','a.formula_id')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.condition_group_id','2')
            ->whereIn('a.formula_id',function ($query) {
                $query->select('formula_id')->from('mathematical_formula')
                    ->where('operator_code','>=');
            })
            ->groupby('a.formula_id')
            ->first();

        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Formula=DB::table('operations')
            ->where('condition_group_id',$id)
            ->where('language_code',Auth::user()->language)->get();
      //  dd($Formula);
        $Condition_group=DB::table('condition_group')
            ->where('condition_group_id',Session::get('condition_group'))
            ->where('language_code',Auth::user()->language)
            ->first();
        //dd($Condition_group);
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();
        // dd($Formula);
        return view('module.packages.condition.create-new')
            ->with('Package',$Package)
            ->with('Deposit',$Deposit)
            ->with('Formula',$Formula)
            ->with('Condition_group',$Condition_group)
            ->with('Unit',$Unit);
    }

    public function createCondition(){

        $Deposit=DB::table('package_condition as a')
            ->join('operations as b','b.condition_group_id','=','a.condition_group_id')
            ->join('mathematical_formula as c','c.formula_id','=','a.formula_id')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.condition_group_id','2')
            ->whereIn('a.formula_id',function ($query) {
                $query->select('formula_id')->from('mathematical_formula')
                    ->where('operator_code','>=');
            })
            ->groupby('a.formula_id')
            ->first();

        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Formula=DB::table('operations')->where('language_code',Auth::user()->language)->get();
        $Condition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->get();
        $Unit=DB::table('unit')->where('language_code',Auth::user()->language)->get();
        // dd($Formula);
        return view('module.packages.condition.create')
            ->with('Package',$Package)
            ->with('Deposit',$Deposit)
            ->with('Formula',$Formula)
            ->with('Condition_group',$Condition_group)
            ->with('Unit',$Unit);
    }

    public function BegindeleteCondition($id){
        $Condition=DB::table('condition_in_package_details as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('condition_id',$id)
            ->groupby('a.packageID')
            ->get();
       // dd($Condition);
        if($Condition->count()>0){
            return view('module.packages.condition.begin_delete')
                ->with('id',$id)
                ->with('Condition',$Condition);
        }else{
            //dd($id);
//            $this->deleteCondition($id);
            return redirect('package/condition/delete/'.$id);
        }
    }

    public function BegineditCondition($id){
        App::setLocale(Auth::user()->language);
        $Condition=DB::table('condition_in_package_details as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.condition_id',$id)
            ->groupby('a.packageID')
            ->get();
        //dd($Condition);
        if($Condition->count()>0){
            return view('module.packages.condition.begin_edit')
                ->with('id',$id)
                ->with('Condition',$Condition);
        }else{
            return redirect('package/condition/edit/'.$id);
            //$this->editCondition($id);
        }
    }

    public function editCondition($id){
        App::setLocale(Auth::user()->language);
//          $Formula = DB::table('operations')->where('language_code', Auth::user()->language)->get();
        $UseFormula=array();$Formula=array();$Sub=array();
        $Condition_group = DB::table('condition_group')->where('language_code', Auth::user()->language)->get();
        $Unit = DB::table('unit')->where('language_code', Auth::user()->language)->get();

        $Condition = DB::table('package_condition')
            ->where('language_code', Auth::user()->language)
            ->where('condition_code', $id)->first();

        if (!$Condition) {
            $Condition = DB::table('package_condition')
                ->where('language_code', 'en')
                ->where('condition_code', $id)->first();
        }

        $Package = DB::table('package_tour')->where('packageID', Session::get('package'))->first();
        // echo Auth::user()->language;
        if($Condition){
            $UseFormula = array();
            if ($Condition->formula_id != 0) {
                $UseFormula = DB::table('mathematical_formula')->where('formula_id', $Condition->formula_id)->first();
            }
            Session::put('condition_group',$Condition->condition_group_id);

            $Condition_group=DB::table('condition_group')
                ->where('condition_group_id',$Condition->condition_group_id)
                ->where('language_code',Auth::user()->language)
                ->first();
            // dd($Condition);
            $Formula=DB::table('operations')
                ->where('condition_group_id',$Condition->condition_group_id)
                ->where('language_code',Auth::user()->language)->get();

            $Sub=DB::table('operation_sub')
                ->where('condition_group_id',$Condition->condition_group_id)
                ->where('language_code',Auth::user()->language)
                ->get();

        }

        //  dd($UseFormula);
            return view('module.packages.condition.edit-new')
                ->with('Condition', $Condition)
                ->with('Condition_group', $Condition_group)
                ->with('UseFormula', $UseFormula)
                ->with('Formula', $Formula)
                ->with('Package', $Package)
                ->with('Condition_group', $Condition_group)
                ->with('Sub', $Sub)
                ->with('Unit', $Unit);

    }

    public function deleteCondition($id){
        $check=DB::table('condition_in_package_details')->where('id',$id)->first();
        if($check){
            DB::table('condition_deposit_operation')->where('condition_code',$check->condition_id)->delete();
        }


        //dd($id);
        //dd($check->condition_id);
        DB::table('condition_in_package_details')->where('condition_id',$id)->delete();
        DB::table('condition_deposit_operation')->where('condition_code',$id)->delete();
        $Condition=DB::table('package_condition')->where('condition_code',$id)->first();
        if($Condition){
            if($Condition->formula_id!='0'){
                DB::table('mathematical_formula')->where('formula_id',$Condition->formula_id)->delete();
            }
        }
        DB::table('package_condition')->where('condition_code',$id)->delete();

        if(Session::get('event')=='details'){
            return redirect('package/details/'.Session::get('package'));
        }else{
            return redirect('package/condition/list_more/'.Session::get('condition_group'));
        }
//        return redirect('package/condition/list_more/'.Session::get('condition_group'));
        //return redirect('package/condition/list');
        //  dd($i);

       //    return 'Deleted';
    }


    public function saveCondition(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'condition_group_id'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            $Condition = DB::table('package_condition')
                //                ->where('language_code',$Package->packageLanguage)
                ->orderby('condition_code', 'desc')
                ->first();
            $condition_code = '';
            if (isset($Condition)) {
                $condition_code = $Condition->condition_code + 1;
            } else {
                $condition_code = 1;
            }

            $formula_id = "";
            $use_operator = "";
            $condition_right = "";
            $condition_left = "";
            $condition_value = "";
            $value_deposit = "";
            $unit_deposit = "";
            $value_tour = "";
            $unit_value_tour = "";
            $keep_all_costs = "";
            $return_all_costs = "";
            $visa_expiry_date = "";

            if(isset($post['operator_code'])) {
                $Operator = DB::table('operations')
                    ->where('operator_code', $post['operator_code'])
                    ->where('language_code', Auth::user()->language)
                    ->first();

                if (isset($post['use_operator'])) {
                    $use_operator = $post['use_operator'];
                }

                if (isset($post['visa_expiry_date'])) {
                    $visa_expiry_date = $post['visa_expiry_date'];
                }
                if (isset($post['condition_right'])) {
                    $condition_right = $post['condition_right'];
                }


                if (isset($post['condition_left'])) {
                    $condition_left = $post['condition_left'];
                }
                if (!$condition_left) {
                    $condition_left = $visa_expiry_date;
                }
                if (isset($post['condition_value'])) {
                    $condition_value = $post['condition_value'];
                }
                //dd($post['operator_code']);
                if (isset($post['value_deposit']) and $post['operator_code'] != '>') {
                    $value_deposit = $post['value_deposit'];
                }

                if (!$value_deposit) {
                    $value_deposit = $condition_value;
                }

                if (!$value_deposit) {
                    $value_deposit = $post['deposit'];
                }
                //            dd($value_deposit);
                if (isset($post['unit_deposit'])) {
                    $unit_deposit = $post['unit_deposit'];
                }

                if (isset($post['value_tour'])) {
                    $value_tour = $post['value_tour'];
                }

                if (isset($post['unit_value_tour'])) {
                    $unit_value_tour = $post['unit_value_tour'];
                }

                if (isset($post['keep_all_costs'])) {
                    $keep_all_costs = $post['keep_all_costs'];
                }
                if (isset($post['return_all_costs'])) {
                    $return_all_costs = $post['return_all_costs'];
                }

                $deposit_type = isset($_POST['deposit_type']) ? $_POST['deposit_type'] : '';
                $tour_type = isset($_POST['tour_type']) ? $_POST['tour_type'] : '';

                $keep_value_deposit=isset($post['keep_value_deposit'])?$post['keep_value_deposit']:'';
                $keep_value_tour=isset($post['keep_value_tour'])?$post['keep_value_tour']:'';

                if(isset($post['keep_all_deposit'])){
                    $value_deposit=100;
                    $deposit_type='K';
                    $unit_deposit='%';
                }
                if(isset($post['return_all_deposit'])){
                    $value_deposit=0;
                    $deposit_type='R';
                    $unit_deposit='%';
                }

                if ($use_operator == 'Y') {
                    $data = array(
                        'operator_code' => $post['operator_code'],
                        'condition_left' => $condition_left,
                        'condition_right' => $condition_right,
                        'value_deposit' => $value_deposit,
                        'unit_deposit' => $unit_deposit,
                        'deposit_type' => $deposit_type,
                        'value_tour' => $value_tour,
                        'unit_value_tour' => $unit_value_tour,
                        'tour_type' => $tour_type,
                        'keep_value_deposit' => $keep_value_deposit,
                        'keep_value_tour' => $keep_value_tour,
                        'keep_all_costs' => $keep_all_costs,
                        'return_all_costs' => $return_all_costs,
                    );
                    //  dd($data);
                    $formula_id = DB::table('mathematical_formula')->insertGetId($data);
                }

                if($post['operator_code']=='Deposit'){
                  //  DB::table('condition_deposit_operation')->where('condition_code',$Condition->condition_code)->delete();
                    if(is_array($post['operation_sub_id'])){
                       // dd($post['operation_sub_id']);
                        foreach ($post['operation_sub_id'] as $key=>$value){
                            $data = array(
                                'condition_code' => $condition_code,
                                'operation_sub_id' => $value,
                                'formula_id' => '<=',
                                'number_of_day' => $post['number_of_day'][$value-1],
                            );

                            DB::table('condition_deposit_operation')->insert($data);
                        }

                    }

                }

//                if($post['operator_code']=='Deposit'){
//                    if(is_array($post['operation_sub_id'])){
//                        foreach ($post['operation_sub_id'] as $key=>$value){
//                            $data = array(
//                                'condition_code' => $condition_code,
//                                'operation_sub_id' => $value,
//                                'formula_id' => '<=',
//                                'number_of_day' => $post['number_of_day'][$key],
//                            );
//                            DB::table('condition_deposit_operation')->insert($data);
//                        }
//                    }
//                }

                if (isset($post['condition_title']) and $post['condition_title'] != '') {
                    $condition_title = $post['condition_title'];
                    //dd($condition_title);
                } else {
                    $date = "";
                    if ($post['condition_group_id'] == '2') {
                        if ($post['operator_code'] == 'Between') {
                            $date = $condition_left . ' ' . trans('common.to') . $condition_right;
                        } else {
                            $date = $condition_left;
                        }
                        //  dd($condition_value);
                        $keep_value_deposit = isset($_POST['keep_value_deposit']) ? $_POST['keep_value_deposit'] : '';
                        if ($keep_value_deposit == 'Y') { //********** Deposit ******************
                            if($deposit_type=='K'){
                                if ($value_deposit == 100 and $unit_deposit == '%') {
                                    $condition_value = trans('common.deposit_all');
                                } else {
                                    $condition_value = trans('common.deposit') . ' ' . number_format($value_deposit) . ' ' . $post['unit_deposit'];
                                }
                            }else{
                                if ($value_deposit == 100 and $unit_deposit == '%') {
                                    $condition_value = trans('common.not_deposit_all');
                                } else {
                                    $condition_value = trans('common.return_deposit') . ' ' . number_format($value_deposit) . ' ' . $post['unit_deposit'];
                                }
                            }
                          //  dd($condition_value);
                        }

                        if(isset($post['keep_all_deposit'])){
                            $condition_value = trans('common.deposit_all');
                        }
                        if(isset($post['return_all_deposit'])){
                            $condition_value = trans('common.not_deposit_all');
                        }

                        $condition_value2='';
                        $keep_value_tour = isset($_POST['keep_value_tour']) ? $_POST['keep_value_tour'] : '';
                        if ($keep_value_tour == 'Y') { //************** Service Charge ****************
                            if($tour_type=='K'){
                                if ($value_tour == 100 and $unit_value_tour == '%') {
                                    $condition_value2 =  trans('common.keep_service_charge_all');
                                } else {
                                    $condition_value2 =  trans('common.keep_service_charge') . ' ' . number_format($value_tour) . ' ' . $post['unit_value_tour'];
                                }
                            }else{
                                if ($value_tour == 100 and $unit_value_tour == '%') {
                                    $condition_value2 = trans('common.return_service_charge_all');
                                } else {
                                    $condition_value2 = trans('common.return_service_charge') . ' ' . number_format($value_tour) . ' ' . $post['unit_value_tour'];
                                }
                            }

                        }

                      //  $keep_all_costs = isset($_POST['keep_all_costs']) ? $_POST['keep_all_costs'] : '';
                        if ($keep_all_costs == 'Y') { //**************** Keep Service charge all *******************
                            $condition_value = trans('common.service_charge_all');
                        }else if ($return_all_costs == 'Y') { //**************** Return Service charge all *******************
                            $condition_value = trans('common.return_service_charge_all');
                        }
                      //  dd($condition_value);
                        if($condition_value){
                            $condition_title = $Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value;
                        }



                        if($condition_value2 && $condition_value){
                            $condition_title.='<br>'. $Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value2;
                        }elseif($condition_value2){
                            $condition_title=$Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value2;
                        }


                    } else if ($post['condition_group_id'] == '1') {
                        $Package = DB::table('package_tour as a')
                            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                            ->where('b.LanguageCode', Auth::user()->language)
                            ->where('a.packageID', Session::get('package'))->first();

                        $condition_value = number_format($post['deposit']) . ' ' . $Package->packageCurrency;
                        $condition_title = $Operator->operator_title . ' ' . $condition_value;
                    } else {
                        $date = $post['visa_expiry_date'];
                        $condition_title = $Operator->operator_title . ' ' . $date . ' ' . trans('common.day');
                    }
                    //  dd($condition_title);
                }
            }else{
                $condition_title=$post['condition_title'];
            }

           // dd($condition_title);

            $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
           // dd($Package);
                if($Package->package_owner=='Yes'){
                    $timeline_id=$Package->timeline_id;
                }else{
                    $timeline_id=$Package->owner_timeline_id;
                }
            }
        //    dd(Session::get('package').$timeline_id);
            $data = array(
                'formula_id'=>$formula_id,
                'timeline_id'=>$timeline_id,
                'condition_code'=>$condition_code,
                'condition_group_id'=>$post['condition_group_id'],
                'condition_title'=>$condition_title,
                'use_operator'=>$use_operator,
                'language_code'=>Auth::user()->language,
            );
           // dd($data);
            $i=DB::table('package_condition')->insert($data);

            $data=array(
                'packageID'=>Session::get('package'),
                'condition_group_id'=>$post['condition_group_id'],
                'condition_id'=>$condition_code,
                'status'=>'y',
            );
           // dd($data);
            DB::table('condition_in_package_details')->insert($data);
       // dd($data);
        if(Session::get('event')=='details'){
            return redirect('package/details/'.Session::get('package'));
        }else{
            return redirect('package/condition/list_more/'.Session::get('condition_group'));
        }
//            if($i>0){
//                return redirect('package/condition/list_more/'.Session::get('condition_group'));
//            }

    }


    public function updateCondition(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'condition_group_id'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{
//            $Package=DB::table('package_tour')->orderby('packageID',Session::get('package'))->first();
            $use_operator="";
            $condition_right="";
            $condition_left="";
            $condition_value="";
            $value_deposit="";
            $unit_deposit="";
            $value_tour="";
            $unit_value_tour="";
            $keep_all_costs="";
            $return_all_costs="";
            $visa_expiry_date="";
            $formula_id="";

            $Condition = DB::table('package_condition')
                ->where('id', $post['id'])
                ->where('language_code', Auth::user()->language)
                ->first();

          //  dd($Condition);
            if(isset($post['operator_code']) && $post['operator_code']!="") {
                 $Operator = DB::table('operations')
                    ->where('operator_code', $post['operator_code'])
                    ->where('language_code', Auth::user()->language)
                    ->first();

                if (isset($post['use_operator'])) {
                    $use_operator = $post['use_operator'];
                }

                if (isset($post['visa_expiry_date'])) {
                    $visa_expiry_date = $post['visa_expiry_date'];
                }
                if (isset($post['condition_right'])) {
                    $condition_right = $post['condition_right'];
                }

                if (isset($post['condition_left'])) {
                    $condition_left = $post['condition_left'];
                }
                if (!$condition_left) {
                    $condition_left = $visa_expiry_date;
                }
//                if (isset($post['condition_value'])) {
//                    $condition_value = $post['condition_value'];
//                }

                if (isset($post['value_deposit']) and $post['operator_code'] != '>') {
                    $value_deposit = $post['value_deposit'];
                }

                if (!$value_deposit) {
                    $value_deposit = $condition_value;
                }

                if (!$value_deposit) {
                    $value_deposit = isset($post['deposit']) ? $post['deposit'] : '';
                }
//            dd($value_deposit);
                if (isset($post['unit_deposit'])) {
                    $unit_deposit = $post['unit_deposit'];
                }

                if (isset($post['value_tour'])) {
                    $value_tour = $post['value_tour'];
                }

                if (isset($post['unit_value_tour'])) {
                    $unit_value_tour = $post['unit_value_tour'];
                }

                if (isset($post['keep_all_costs'])) {
                    $keep_all_costs = $post['keep_all_costs'];
                }

                if (isset($post['return_all_costs'])) {
                    $return_all_costs = $post['return_all_costs'];
                }

                if(isset($post['keep_all_deposit'])){
                    $value_deposit=100;
                    $deposit_type='K';
                    $unit_deposit='%';
                }
                if(isset($post['return_all_deposit'])){
                    $value_deposit=0;
                    $deposit_type='R';
                    $unit_deposit='%';
                }

                $deposit_type = isset($_POST['deposit_type']) ? $_POST['deposit_type'] : '';
                $tour_type = isset($_POST['tour_type']) ? $_POST['tour_type'] : '';
                $keep_value_deposit=isset($post['keep_value_deposit'])?$post['keep_value_deposit']:'';
                $keep_value_tour=isset($post['keep_value_tour'])?$post['keep_value_tour']:'';
                if ($use_operator == 'Y') {
                    $check = DB::table('mathematical_formula')->where('formula_id', $Condition->formula_id)->first();
                    $data = array(
                        'operator_code' => $post['operator_code'],
                        'condition_left' => $condition_left,
                        'condition_right' => $condition_right,
                        'value_deposit' => $value_deposit,
                        'unit_deposit' => $unit_deposit,
                        'value_tour' => $value_tour,
                        'unit_value_tour' => $unit_value_tour,
                        'keep_all_costs' => $keep_all_costs,
                        'deposit_type' => $deposit_type,
                        'tour_type' => $tour_type,
                        'keep_value_deposit' => $keep_value_deposit,
                        'keep_value_tour' => $keep_value_tour,
                        'return_all_costs' => $return_all_costs,
                    );
                   //dd($data);
                    if ($check->operator_code == $post['operator_code']) {
                        DB::table('mathematical_formula')->where('formula_id', $Condition->formula_id)->update($data);
                        $formula_id = $Condition->formula_id;
                    } else {
                        DB::table('mathematical_formula')->where('formula_id', $Condition->formula_id)->delete();
                        $formula_id = DB::table('mathematical_formula')->insertGetId($data);
                    }
                }

                if (isset($post['condition_title']) and $post['use_operator'] != 'Y') {
                    $condition_title = $post['condition_title'];
                    //                dd($condition_title);
                } else {
                    $date = "";
                    if ($post['condition_group_id'] == '2') {
                        if ($post['operator_code'] == 'Between') {
                            $date = $condition_left . ' ' . trans('common.to') . $condition_right;
                        } else {
                            $date = $condition_left;
                        }
                        //  dd($condition_value);
                        $keep_value_deposit = isset($_POST['keep_value_deposit']) ? $_POST['keep_value_deposit'] : '';
                        if ($keep_value_deposit == 'Y') { //********** Deposit ******************
                            if($deposit_type=='K'){
                                if ($value_deposit == 100 and $post['unit_deposit'] == '%') {
                                    $condition_value = trans('common.deposit_all');
                                } else {
                                    $condition_value = trans('common.deposit') . ' ' . number_format($value_deposit) . ' ' . $post['unit_deposit'];
                                }
                            }else{
                                if ($value_deposit == 100 and $post['unit_deposit'] == '%') {
                                    $condition_value = trans('common.not_deposit_all');
                                } else {
                                    $condition_value = trans('common.return_deposit') . ' ' . number_format($value_deposit) . ' ' . $post['unit_deposit'];
                                }
                            }
                        }

                        if(isset($post['keep_all_deposit'])){
                            $condition_value = trans('common.deposit_all');
                        }
                        if(isset($post['return_all_deposit'])){
                            $condition_value = trans('common.not_deposit_all');
                        }

                        $condition_value2="";
                        $keep_value_tour = isset($_POST['keep_value_tour']) ? $_POST['keep_value_tour'] : '';
                        if ($keep_value_tour == 'Y') { //************** Service Charge ****************
                            if($tour_type=='K') {
                                if ($value_tour == 100 and $post['unit_value_tour'] == '%') {
                                    $condition_value2 = trans('common.keep_service_charge_all');
                                } else {
                                    $condition_value2 = trans('common.keep_service_charge') . ' ' . number_format($value_tour) . ' ' . $post['unit_value_tour'];
                                }
                            }else{
                                if ($value_tour == 100 and $post['unit_value_tour'] == '%') {
                                    $condition_value2 = trans('common.return_service_charge_all');
                                } else {
                                    $condition_value2 = trans('common.return_service_charge') . ' ' . number_format($value_tour) . ' ' . $post['unit_value_tour'];
                                }
                               // dd($condition_value2);
                            }
                        }

                        $keep_all_costs = isset($_POST['keep_all_costs']) ? $_POST['keep_all_costs'] : '';
                        if ($keep_all_costs == 'Y') { //**************** Keep Service charge all *******************
                            $condition_value = trans('common.service_charge_all');
                        }
                        $return_all_costs = isset($_POST['return_all_costs']) ? $_POST['return_all_costs'] : '';
                        if ($return_all_costs == 'Y') { //**************** Keep Service charge all *******************
                            $condition_value = trans('common.return_service_charge_all');
                        }
                        if($condition_value){
                            $condition_title = $Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value;
                        }

                        if($condition_value2 && $condition_value){
                            $condition_title.='<BR>'.$Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value2;
                        }elseif($condition_value2){
                            $condition_title=$Operator->operator_title . ' ' . $date . trans('common.day') . ' ' . $condition_value2;
                        }


                    } else if ($post['condition_group_id'] == '1') {
                        $Package = DB::table('package_tour as a')
                            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                            ->where('b.LanguageCode', Auth::user()->language)
                            ->where('a.packageID', Session::get('package'))->first();

                        $condition_value = number_format($post['deposit']) . ' ' . $Package->packageCurrency;
                        $condition_title = $Operator->operator_title . ' ' . $condition_value;
                    } else {
                        $date = $post['visa_expiry_date'];
                        $condition_title = $Operator->operator_title . ' ' . $date . ' ' . trans('common.day');
                    }
                    //  dd($condition_title);
                }
            }else{
                $condition_title=$post['condition_title'];
               // dd($condition_title);
            }

            if($post['operator_code']=='Deposit'){
                DB::table('condition_deposit_operation')->where('condition_code',$Condition->condition_code)->delete();
                if(is_array($post['operation_sub_id'])){
                    //dd($post['operation_sub_id']);
                    foreach ($post['operation_sub_id'] as $key=>$value){
                        $data = array(
                            'condition_code' => $Condition->condition_code,
                            'operation_sub_id' => $value,
                            'formula_id' => '<=',
                            'number_of_day' => $post['number_of_day'][$value-1],
                        );
                        DB::table('condition_deposit_operation')->insert($data);
                    }
                }
            }

            $data = array(
                'formula_id'=>$formula_id,
                'condition_title'=>$condition_title,
                );

           // dd($data);
            DB::table('package_condition')
                ->where('formula_id',$Condition->formula_id)
                ->where('id',$post['id'])
                ->update($data);

            if(Session::get('event')=='details'){
                return redirect('package/details/'.Session::get('package'));
            }else{
                return redirect('package/condition/list_more/'.Session::get('condition_group'));
            }

//            return redirect('package/condition/more/'.Session::get('packageDescID'));
        }
    }

    public function saveConditionOther(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'packageCondition'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{
            $Package=DB::table('package_tour')->orderby('packageID',Session::get('package'))->first();
            $Condition=DB::table('package_condition')
                ->where('LanguageCode',Auth::user()->language)
                ->orderby('conditionCode','desc')
                ->first();


            if($Condition){
                $conditionCode=$Condition->conditionCode+1;
            }else{
                $conditionCode=1;
            }

            $data = array(
                'packageDescID'=>Session::get('packageDescID'),
                'conditionCode'=>$conditionCode,
                'groupCode'=>$post['groupCode'],
                'packageCondition'=>$post['packageCondition'],
                'packageConditionDesc'=>$post['packageConditionDesc'],
                'LanguageCode'=>Auth::user()->language,
            );

            $i= DB::table('package_condition')->insert($data);

            if ($request->hasFile('picture')) {
                $Condition=DB::table('package_condition')->where('ID',$i)->first();
                $artPics = $request->file('picture');
                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/small/' . $filename));
                $data = array(
                    'conditionCode' => $Condition->conditionCode,
                    'Image' => $filename,
                    'OrderBy'=>'1'
                );
                DB::table('package_condition_images')->insert($data);
            }

            $check=DB::table('package_sub_condition')
                ->where('packageDescID',Session::get('packageDescID'))
                ->where('groupCode',$post['groupCode'])
                ->orderby('packageConditionOrder','desc')
                ->first();
           // dd($check);
            if($check){
                $order=$check->packageConditionOrder+1;
            }else{
                $order=1;
            }
            $data = array(
                'packageDescID'=>Session::get('packageDescID'),
                'conditionCode'=>$conditionCode,
                'groupCode'=>$post['groupCode'],
                'packageConditionOrder'=>$order
            );

            $i=DB::table('package_sub_condition')->insert($data);


            if(Session::get('event')=='details'){
                return redirect('package/details/'.Session::get('package'));
            }else{
                return redirect('package/condition/edit/'.Session::get('packageDescID'));
            }

        }
    }

    public function setCondition(Request $request){
        $post=$request->all();
        if(is_array($post['conidtion_id'])) {
            DB::table('condition_in_package_details')
                ->where('packageID',Session::get('package'))
                ->where('condition_group_id',Session::get('condition_group'))
                ->delete();
            foreach ($post['conidtion_id'] as $value){
                $data = array(
                    'packageID' => Session::get('package'),
                    'condition_id' => $value,
                    'condition_group_id' => Session::get('condition_group'),
                    'status' => 'y'
                );
                DB::table('condition_in_package_details')->insert($data);
            }
        }
        Session::flash('message','Success!');
        return back();

    }


    public function copyProgram($id){
        $Condition=DB::table('package_condition')->where('ID',$id)->first();

        if(isset($Condition)){
            $Condition1=DB::table('package_condition')
                ->where('LanguageCode',$Condition->LanguageCode)
                ->orderby('packageDescID','desc')
                ->first();

            if(isset($Condition1)){
                $conditionCode=$Condition1->conditionCode+1;
            }else{
                $conditionCode=1;
            }
            $data=array(
                'packageDescID'=>$Condition->packageDescID,
                'conditionCode'=>$conditionCode,
                'groupCode'=>$Condition->groupCode,
                'packageCondition'=>$Condition->packageCondition,
                'packageConditionDesc'=>$Condition->packageConditionDesc,
                'LanguageCode'=>$Condition->LanguageCode,
            );
            $i=DB::table('package_condition')->insert($data);

            if($i>0){
                return redirect('package/condition/list/'.$Condition->packageDescID);
            }
        }
    }

    public function _EditCondition($id){
        // $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        // $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        // $Condition=DB::table('package_condition')
        //     ->where('LanguageCode',Session::get('Language'))
        //     ->where('ID',$id)->first();

        // $ConditionImage=DB::table('package_condition_images')
        //     ->where('conditionCode',$id)->first();

        // // return view('package.condition.edit')
        // return view('packages.setting.edict-condition')
        //         ->with('Default',$Package)
        //         ->with('ConditionImage',$ConditionImage)
        //         ->with('SiteName',$SiteName)
        //         ->with('Condition',$Condition);
        Session::put('packageDescID',$id);
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('b.LanguageCode',Session::get('Language'))
            ->where('a.packageID',Session::get('package'))->first();
        $Condition=DB::table('package_details')->where('packageDescID',Session::get('packageDescID'))->first();
        //dd($Condition);
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        if(isset($Condition)){
            return view('packages.setting.edit-condition')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package)
                ->with('pd_active',"active")
                ->with('p_open',"menu-open")
                ->with('p_disp',"display: block;");
        }else{
            return view('packages.setting.edit-condition')
                ->with('Condition',$Condition)
                ->with('SiteName',$SiteName)
                ->with('Default',$Package)
                ->with('pd_active',"active")
                ->with('p_open',"menu-open")
                ->with('p_disp',"display: block;");
        }
    }

    public function EditConditionOther($id){
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        $Condition=DB::table('package_condition')
            ->where('LanguageCode',Session::get('Language'))
            ->where('conditionCode',$id)->first();

        $ConditionImage=DB::table('package_condition_images')
            ->where('conditionCode',$id)->first();

        return view('module.packages.condition.editOther')
            ->with('Default',$Package)
            ->with('ConditionImage',$ConditionImage)
            ->with('SiteName',$SiteName)
            ->with('Condition',$Condition);
    }

//    public function UpdateCondition(Request $request){
//        $post=$request->all();
//        $v=\Validator::make($request->all(),[
//            'packageCondition'=>'required',
//        ]);
//        if($v->fails()){
//            return redirect()->back()->withErrors($v->errors());
//        }else{
//            $Package=DB::table('package_tour')->orderby('packageID',Session::get('package'))->first();
//
//            $data = array(
//                'packageDescID'=>Session::get('packageDescID'),
//                'packageCondition'=>$post['packageCondition'],
//                'packageConditionDesc'=>$post['packageConditionDesc']
//            );
//
//
//
//            DB::table('package_condition')
//                ->where('LanguageCode',Session::get('Language'))
//                ->where('ID',$post['id'])->update($data);
//
//            if ($request->hasFile('picture')) {
//                $Condition=DB::table('package_condition')->where('conditionCode',$post['id'])->first();
//                $artPics = $request->file('picture');
//                $filename = time() . "." . $artPics->getClientOriginalExtension();
//                list($ori_w, $ori_h) = getimagesize($artPics);
//
//                if ($ori_w >= 1000) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 1000;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 650;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));
//
//                if ($ori_w >= 650) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 650;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 480;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/small/' . $filename));
//                $data = array(
//                    'conditionCode' => $Condition->conditionCode,
//                    'Image' => $filename,
//                    'OrderBy'=>'1'
//                );
//                DB::table('package_condition_images')->insert($data);
//            }
//
//             return redirect('package/condition/list/'.Session::get('packageDescID'));
//
//
//        }
//    }

    public function UpdateConditionOther(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'packageCondition'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{


            $data = array(
                'packageDescID'=>Session::get('packageDescID'),
                'packageCondition'=>$post['packageCondition'],
                'packageConditionDesc'=>$post['packageConditionDesc']
            );

            DB::table('package_condition')
                ->where('LanguageCode',Session::get('Language'))
                ->where('conditionCode',$post['id'])->update($data);

            if ($request->hasFile('picture')) {
                $Condition=DB::table('package_condition')->where('conditionCode',$post['id'])->first();
                $artPics = $request->file('picture');
                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/small/' . $filename));
                $data = array(
                    'conditionCode' => $Condition->conditionCode,
                    'Image' => $filename,
                    'OrderBy'=>'1'
                );
                DB::table('package_condition_images')->insert($data);
            }

            return redirect('package/condition/form/'.Session::get('packageDescID'));


        }
    }

    public function DelConditionOther($id){

        DB::table('package_condition')
            ->where('LanguageCode',Auth::user()->language)
            ->where('conditionCode',$id)->delete();
        $check=DB::table('package_condition')
            ->where('conditionCode',$id)->get();

        if(!isset($check)){
            DB::table('package_sub_condition')
               ->where('conditionCode',$id)->delete();
        }
        return redirect('package/condition/edit/'.Session::get('packageDescID'));

    }

    public function DelCondition($id){

        $i=DB::table('package_condition')->where('ID',$id)->delete();
        if($i>0){
            return redirect('package/condition/list/'.Session::get('packageDescID'));
        }


    }



}
