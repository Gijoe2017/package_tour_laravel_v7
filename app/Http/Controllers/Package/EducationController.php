<?php

namespace App\Http\Controllers\Package;
use App\Timeline;
use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use App\Category;
use App\Setting;
use App\User;
use App\Location;
use App\Role;


class EducationController extends Controller
{
    //

    public function education_list($id=null){
        if($id>0){
            Session::put('passport_id',$id);
        }

        $Education=DB::table('users_passport_education as a')
            ->join('users_passport_education_info as c','c.education_id','=','a.education_id')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('education_type as d','d.education_type_id','=','a.education_type_id')
            ->where('c.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('a.passport_id',Session::get('passport_id'))
            ->get();
      //  dd($Education);
        Session::put('mode_lang','education');
        return view('packages.order_member.education.list')
            ->with('Education',$Education);

    }

    public function change_language($id){
        $EducationLang=DB::table('users_passport_education_info')
            ->where('education_id',$id)
            ->groupby('language_code')
            ->get();

        return view('member.education.show-lang')
            ->with('EducationLang',$EducationLang)
            ->with('education_id',$id);
    }

    public function create(){

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        return view('packages.order_member.education.create')->with('category_options',$category_options);
    }

    public function edit($id){
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $Education=DB::table('users_passport_education as a')
            ->join('users_passport_education_info as d','d.education_id','=','a.education_id')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('d.language_code',Auth::user()->language)
            ->where('a.education_id',$id)
            ->first();

        if(!$Education){
            $Education=DB::table('users_passport_education as a')
                ->join('timelines as b','b.id','=','a.timeline_id')
//                ->join('locations as c','c.timeline_id','=','a.timeline_id')
                ->where('a.education_id',$id)
                ->first();
//            dd($Education);
        }


        // dd($PlaceInfo);
        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Education->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Education->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Education->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Education->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Education->country_id)
                ->where('state_id',$Education->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Education->country_id)
                    ->where('state_id',$Education->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Education->country_id)
                    ->where('state_id',$Education->state_id)
                    ->where('city_id',$Education->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Education->country_id)
                        ->where('state_id',$Education->state_id)
                        ->where('city_id',$Education->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }


        // dd($Family);
        return view('packages.order_member.education.edit')
            ->with('category_options',$category_options)
            ->with('state',$state)
            ->with('country',$country)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('Education',$Education);
    }

    public function store(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'education_type_id' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
           
            if (isset($request->country_id)) {
                $country_id = $request->country_id;
            }
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }


//            $educationCount=DB::table('users_passport_education')
//                ->where('user_id',Session::get('passport_id'))->count();
//            $education=DB::table('users_passport_education')
//                ->where('user_id',Session::get('passport_id'))
//                ->where('education_type_id',$post['education_type_id'])
//                ->first();
//            if($educationCount==0){
//                $education_id=1;
//                $event='addnew';
//            }else{
//                if(!isset($education)){
//                    $education=DB::table('users_passport_education')->orderby('education_id','desc')->first();
//                    $education_id=$education->education_id+1;
//                    $event='addnew';
//                }else{
//                    $education=DB::table('users_passport_education')
//                        ->where('user_id',Session::get('passport_id'))
//                        ->where('education_type_id',$post['education_type_id'])
//                        ->where('language_code',Auth::user()->language)
//                        ->first();
//                    if(isset($education)){
//                        $event='update';
//                    }
//                    $education_id=$education->education_id;
//                }
//            }

//            $education=DB::table('users_passport_education')->orderby('education_id','desc')->first();
//            $education_id=$education->education_id+1;
            $cate=DB::table('map_education_type')->where('education_type_id',$post['education_type_id'])->first();

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;
                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $cate->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => $cate->category_sub1_id,
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                if($request->q==''){
                    $timeline_id = '38072';
                }else{
                    $check=Timeline::where('name',$request->q)->first();
                    $timeline_id = $check->id;
                }

               // dd($timeline_id);
            }
            
            $data = array(
                'education_type_id' => $post['education_type_id'],
                'passport_id' => Session::get('passport_id'),
                'date_graduate' => date('Y-m-d',strtotime($post['date_graduate'])),
                'timeline_id' => $timeline_id,
                'created_at' => date('Y-m-d H:i:s'),
                'create_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,
            );
             // dd($data);
            $education_id=DB::table('users_passport_education')->insertGetId($data);

            $data = array(
                'education_id' => $education_id,
                'passport_id' => Session::get('passport_id'),
                'education_program' => $post['education_program'],
                'language_code' => Auth::user()->language,
            );
            DB::table('users_passport_education_info')->insert($data);
//            DB::table('users_passport_education')->insert($data);
            return redirect('/package/education');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'education_type_id' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

//            $data=array(
//                'country_id' => $post['country_id'],
//                'state_id' => $state,
//                'city_id' => $city,
//                'city_sub1_id' => $citySub,
//                'zip_code' => $zip_code,
//                'name' => $post['name'],
//            );
//            DB::table('timelines')->where('id',$check->timeline_id)->update($data);
//
//            $data=array(
//                'address' => $post['address'],
//            );
//            DB::table('locations')->where('timeline_id',$check->timeline_id)->update($data);



            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;
                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;
                // dd($timeline_id);
            }

            $data = array(
                'education_type_id' => $post['education_type_id'],
                'passport_id' => Session::get('passport_id'),
                'date_graduate' => date('Y-m-d',strtotime($post['date_graduate'])),
                'timeline_id' => $timeline_id,
                'updated_user_id' => Auth::user()->id,
            );
            // dd($data);
            DB::table('users_passport_education')
                ->where('education_id',$post['education_id'])
                ->update($data);

            $data = array(
                'education_id' => $post['education_id'],
                'passport_id' => Session::get('passport_id'),
                'education_program' => $post['education_program'],
                'language_code' => Auth::user()->language,
            );

            $check=DB::table('users_passport_education_info')
                ->where('education_id',$post['education_id'])
                ->where('language_code',Auth::user()->language)
                ->first();
            if($check){
                DB::table('users_passport_education_info')
                    ->where('education_id',$post['education_id'])
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }else{
                DB::table('users_passport_education_info')->insert($data);
            }


            return redirect('/package/education');
        }
    }

    public function delete($id){
        DB::table('users_passport_education')
            ->where('education_id',$id)
            ->delete();
        DB::table('users_passport_education_info')
            ->where('education_id',$id)
            ->delete();
        return redirect()->back();
    }
}
