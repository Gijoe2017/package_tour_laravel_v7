<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use App\Country;
use App\State;
use App\CitySub1;
use App\City;
use App;
use Illuminate\Pagination\Paginator;
use Route;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\models\Occupation;
use Session;
use Illuminate\Support\Facades\Input;
use Mail;
use File;
use Image;
use Validator;
use Response;
use URL;


class MemberController extends Controller
{

    public function __construct()
    {
         //$this->middleware('auth');
         if (!Auth::guest()) {
            return view('home.index');
         }//home
    }


    public function dashboard($id){
        Session::forget('passport_id');
        Session::forget('checking');
        $Timelines=DB::table('timelines as a')
            ->join('locations as b','b.timeline_id','=','a.id')
            ->join('location_user as c','c.location_id','=','b.id')
            ->select('a.id','a.name')
            ->whereIn('b.timeline_id',function ($query) {
               $query->select('timeline_id')->from('business_verified_modules')
                   ->where('business_verified_status','verified')
                   ->where('module_type_id','1');
               })
            ->where('c.user_id',Auth::user()->id)
            ->get();
//        $timeline=array();
//        if($Timelines){
//            foreach ($Timelines as $rows){
//                $timeline[$rows->id]=$rows->name;
//                $timelineID[]=$rows->id;
//            }
//            $timeline_options = ['' => 'Select Category'] + $timeline;
//            Session::put('timeline_options',$timeline_options);
//        }
//
//        dd($timeline_options);
        Session::put('timeline_id',$id);
        Session::put('represent_timeline_id',$id);

        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->get();

      //  dd($Packages->count());

        Session::put('Packages',$Packages->count());
        Session::put('module_id',null);
        Session::put('Timelines',$Timelines);
        Session::put('language',Auth::user()->language);
       // dd(Session::get('Timelines'));
        $bgColor=array(
            '1'=>'bg-green',
            '2'=>'bg-yellow',
            '3'=>'bg-blue',
            '4'=>'bg-red'
        );

        return view('member.dashboard')
            ->with('bgColor',$bgColor);
    }

    public function change_language($id){
        $MemberLang=DB::table('users_passport_info')
            ->where('passport_id',$id)
            ->groupby('language_code')
            ->get();
       // dd($MemberLang);
        return view('member.forms2.show-lang')
            ->with('MemberLang',$MemberLang)
            ->with('passport_id',$id);
    }

    public function create(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        return view('member.forms2.general')->with('country',$country);
    }


    public function save_step1(Request $request){

        $post=$request->all();
        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {

           return redirect()->back()->withErrors($v->errors());
        } else {

            $identification_id=str_replace('-','',$post['identification_id']);
            $data = array(
                'identification_id' => $identification_id,
                'user_title_id' => $post['user_title_id'],
                'sex_id' => $post['sex_id'],
                'marital_status_id' => $post['marital_status_id'],
                'date_of_birth' => date('Y-m-d',strtotime($post['date_of_birth'])),
                'religion_id' => $post['religion_id'],
                'origin_id' => $post['origin_id'],
                'country_of_birth_id' => $post['country_of_birth_id'],
                'current_nationality_id' => $post['current_nationality_id'],
                'naturalization_id' => $post['naturalization_id'],
                'user_status'=>'D',
                'timeline_id'=>Session::get('timeline_id'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
          //  dd($data);
            $passport_id=DB::table('users_passport')->insertGetId($data);

            $data = array(
                'passport_id' => $passport_id,
                'identification_id' => $identification_id,
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'language_code' => Auth::user()->language,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
            DB::table('users_passport_info')->insert($data);


            Session::put('passport_id',$passport_id);
            if ($request->hasFile('passport_cover_image')) {
//                $dir=base_path() . '/public/images/member/cover/';
                $dir=public_path('/images/member/cover/') ;
                $artPics = $request->file('passport_cover_image');
                $filename = $identification_id .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('passport_cover_image' => $filename);
                DB::table('users_passport')->where('passport_id', $passport_id)->update($data);
            }
           // dd($data);
        }
        Session::put('member_info','( '.$post['first_name'].' '.$post['last_name'].' )');
        return redirect('member/step2');
    }

    public function EditStep1(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();
       // dd($Member);
        $event="";
        if(!isset($Member)){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
            $event='change';
        }

        return view('member.forms2.edit_step1')
            ->with('event',$event)
            ->with('country',$country)
            ->with('Member',$Member);
    }

    public function update_step1(Request $request){
        $post=$request->all();
        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $identification_id=str_replace('-','',$post['identification_id']);

                $data = array(
                    'identification_id' => $identification_id,
                    'user_title_id' => $post['user_title_id'],
                    'sex_id' => $post['sex_id'],
                    'marital_status_id' => $post['marital_status_id'],
//                    'date_of_birth' => $post['date_of_birth'],
                    'date_of_birth' => date('Y-m-d',strtotime($post['date_of_birth'])),
                    'religion_id' => $post['religion_id'],
                    'origin_id' => $post['origin_id'],
                    'country_of_birth_id' => $post['country_of_birth_id'],
                    'current_nationality_id' => $post['current_nationality_id'],
                    'naturalization_id' => $post['naturalization_id'],
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                //  dd($data);
                DB::table('users_passport')
                    ->where('passport_id',Session::get('passport_id'))
                    ->update($data);


            Session::put('passport_id',Session::get('passport_id'));
            if ($request->hasFile('passport_cover_image')) {
                $Member=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->first();
                $dir=public_path('/images/member/cover/') ;
                if($Member->passport_cover_image){
                    $fileNameDel = $dir . $Member->passport_cover_image;
                    if (file_exists($fileNameDel)) {
                        unlink($fileNameDel);
                    }
                }
                $artPics = $request->file('passport_cover_image');
                $filename = $identification_id .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('passport_cover_image' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }

            $Check=DB::table('users_passport_info')
                ->where('passport_id',Session::get('passport_id'))
                ->where('language_code',Auth::user()->language)
                ->first();

            $data = array(
                'passport_id' => Session::get('passport_id'),
                'identification_id' => $identification_id,
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'language_code' => Auth::user()->language,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
            if($Check){
                DB::table('users_passport_info')
                    ->where('passport_id',Session::get('passport_id'))
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }else{
                DB::table('users_passport_info')->insert($data);
            }

            // dd($data);
        }
        Session::flash('message','Record has been update success!');
        return redirect()->back();
    }

    public function create_step2(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        return view('member.forms2.step2')->with('country',$country);
    }

    public function save_step2(Request $request){
        $post=$request->all();
        $v = \Validator::make($request->all(), [
            'passport_number' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $passport_number=str_replace('-','',$post['passport_number']);
          //  dd($passport_number);
            $data=array(
                'passport_country_id' => $post['passport_country_id'],
                'passport_number' => $passport_number,
                'passport_DOI' => $post['passport_DOI'],
                'passport_DOE' => $post['passport_DOE'],
            );
            DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->update($data);

            if ($request->hasFile('photo_for_visa')) {
//                $dir=base_path() . '/images/member/visa/';
                $dir=public_path('/images/member/visa/') ;
                $artPics = $request->file('photo_for_visa');
                $filename = $passport_number .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('photo_for_visa' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }



        }

        return redirect('member/contact');
    }

    public function EditStep2(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();

        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();
        if(!$Member){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
        }

       // dd($Member);
        return view('member.forms2.edit_step2')
            ->with('country',$country)
            ->with('Member',$Member);
    }

    public function update_step2(Request $request){
        $post=$request->all();
        $v = \Validator::make($request->all(), [
            'passport_number' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $passport_number=str_replace('-','',$post['passport_number']);
            //  dd($passport_number);
            $data=array(
                'passport_country_id' => $post['passport_country_id'],
                'passport_number' => $passport_number,
                'passport_DOI' => date('Y-m-d',strtotime($post['passport_DOI'])),
                'passport_DOE' => date('Y-m-d',strtotime($post['passport_DOE'])),
            );
           // dd($data);
            $check=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->update($data);

            if ($request->hasFile('photo_for_visa')) {
//                $dir=base_path() . '/images/member/visa/';
                $dir=public_path('/images/member/visa/') ;
                $Member=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->first();

                if($Member->passport_cover_image){
                    $fileNameDel = $dir . $Member->photo_for_visa;
                    if (file_exists($fileNameDel)) {
                        unlink($fileNameDel);
                    }
                }

                $artPics = $request->file('photo_for_visa');
                $filename = $passport_number .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('photo_for_visa' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }

        }
            if($post['event']=='continue'){
                return redirect('/member/edit-step3');
            }else{
                Session::flash('message','Record has been update success!');
                return redirect()->back();
            }




    }

    public function contact(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();
        $Phones=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('member.forms2.address')
            ->with('Phones',$Phones)
            ->with('Email',$Email)
            ->with('AddressAll',$AddressAll)
            ->with('country',$country);
    }

    public function delete_address(Request $request){
        DB::table('users_passport_address1')->where('address_id',$request->address_id)->delete();
        DB::table('users_passport_address2')->where('address_id',$request->address_id)->delete();

        $Address=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();
        // dd($Address);
        $output = "<table class=\"table table-bordered\">
                    <thead>
                    <tr>
                        <th>" . trans('profile.AddressType') . "</th>
                        <th>" . trans('profile.Address') . "</th>
                        <th>" . trans('profile.Zip_code') . "</th>
                        <th>" . trans('profile.action') . "</th>
                    </tr>
                    </thead>
                    <tbody>";
        foreach ($Address as $rows){
            $address=$rows->address.' '.$rows->additional.' '.$rows->country;
            $output .= "<tr>
                            <td>" . $rows->address_type . "</td>
                            <td>" . $address . "</td>
                            <td>" . $rows->zip_code . "</td>
                            <td>
                                <a class=\"btn btn-sm btn-warning\" href=\"".url('/member/edit-address/'.$rows->address_id)."\" ><i class=\"fa fa-edit\"></i> ".trans('profile.Edit')."</a>
                                <button class=\"btn btn-sm btn-danger delete_address\" data-id='".$rows->address_id."'><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')."</button>
                            </td>
                        </tr>";
        }
        $output.=" </tbody></table>";

        return Response($output);


    }

    public function add_address(Request $request){
        $state = "";
        $city = "";
        $citySub = "";
        $zip_code = '';

        if (isset($request->state_id)) {
            $state = $request->state_id;
        }
        if (isset($request->city_id)) {
            $city = $request->city_id;
        }
        if (isset($request->city_sub1_id)) {
            $citySub = $request->city_sub1_id;
            $zip_code = $request->zip_code;
        }
        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address_type_id' => $request->address_type_id,
            'country_id' => $request->country_id,
            'state_id' => $state,
            'city_id' => $city,
            'city_sub1_id' => $citySub,
            'zip_code' => $zip_code,
            'created_by_user_id' => Auth::user()->id,
            'created_by_site_id' => URL::current(),
        );
        // dd($data);
        $address_id=DB::table('users_passport_address1')->insertGetId($data);

        $data = array(
            'address_id' => $address_id,
            'passport_id' => Session::get('passport_id'),
            'address' => $request->address,
            'additional' => $request->additional,
            'address_type_id' => $request->address_type_id,
            'language_code' => Auth::user()->language,
            'created_by_user_id' => Auth::user()->id,
            'created_by_site_id' => URL::current(),
        );
//        dd($data);
        DB::table('users_passport_address2')->insert($data);

        return redirect('member/edit-step3');
    }

    public function update_address(Request $request){
        $state = "";
        $city = "";
        $citySub = "";
        $zip_code = '';

        if (isset($request->state_id)){
            $state = $request->state_id;
        }
        if (isset($request->city_id)) {
            $city = $request->city_id;
        }
        if (isset($request->city_sub1_id)) {
            $citySub = $request->city_sub1_id;
            $zip_code = $request->zip_code;
        }
        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address_type_id' => $request->address_type_id,
            'country_id' => $request->country_id,
            'state_id' => $state,
            'city_id' => $city,
            'city_sub1_id' => $citySub,
            'zip_code' => $zip_code,

        );

        $address_id=DB::table('users_passport_address1')
            ->where('address_id',$request->address_id)
            ->update($data);

        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address' => $request->address,
            'additional' => $request->additional,
            'address_type_id' => $request->address_type_id,
            'language_code' => Auth::user()->language,

        );
//        dd($data);
        DB::table('users_passport_address2')
            ->where('address_id',$request->address_id)
            ->update($data);

        return redirect('member/edit-step3');
    }

//    public function save_step3(Request $request){
//        $post=$request->all();
//
//        $v = \Validator::make($request->all(), [
//            'address' => 'required',
//        ]);
//        if ($v->fails()) {
//            return redirect()->back()->withErrors($v->errors());
//        } else {
//
//            $state="";$city="";$citySub="";$zip_code='';
//            if(isset($post['state_id'])){
//                $state=$post['state_id'];
//            }
//            if(isset($post['city_id'])){
//                $city=$post['city_id'];
//            }
//            if(isset($post['city_sub1_id'])){
//                $citySub=$post['city_sub1_id'];
//                $zip_code=$post['zip_code'];
//            }
//            $data=array(
//                'passport_id'=>Session::get('passport_id'),
//                'address_type_id'=>$post['address_type_id'],
//                'country_id'=>$post['country_id'],
//                'state_id'=>$state,
//                'city_id'=>$city,
//                'city_sub1_id'=>$citySub,
//                'zip_code'=>$zip_code,
//                'created_by_user_id'=>Auth::user()->id,
//                'created_by_site_id'=>URL::current(),
//            );
//           // dd($data);
//            DB::table('users_passport_address1')->insert($data);
//
//            $data=array(
//                'passport_id'=>Session::get('passport_id'),
//                'address'=>$post['address'],
//                'additional'=>$post['additional'],
//                'address_type_id'=>$post['address_type_id'],
//                'language_code'=>Auth::user()->language,
//                'created_by_user_id'=>Auth::user()->id,
//                'created_by_site_id'=>URL::current(),
//            );
//            //dd($data);
//            DB::table('users_passport_address2')->insert($data);
//
//            if(is_array($post['email'])){
//                foreach ($post['email'] as $email){
//                    if($email!=''){
//                        $data=array(
//                            'passport_id'=>Session::get('passport_id'),
//                            'email'=>$email,
//                            'created_by_user_id'=>Auth::user()->id,
//                            'created_by_site_id'=>URL::current(),
//                            'active'=>'1',
//                        );
//                        DB::table('users_passport_email')->insert($data);
//                    }
//
//                }
//            }
//
//            $check=DB::table('users_passport_phone')->where('phone_number',$post['phone_number'])->first();
//            if(!isset($check)){
//                $data=array(
//                    'passport_id'=>Session::get('passport_id'),
//                    'country_id'=>$post['phone_country_id'],
//                    'phone_number'=>$post['phone_number'],
//                    'phone_country_code'=>$post['phone_country_code'],
//                    'phone_type'=>$post['phone_type'],
//                    'created_by_user_id'=>Auth::user()->id,
//                    'created_by_site_id'=>URL::current(),
//                    'active'=>'1',
//                );
//                DB::table('users_passport_phone')->insert($data);
//            }
//
//
//
//        }
//
//        return redirect('member/step4');
//    }
    
    public function delete_phone(Request $request){

        DB::table('users_passport_phone')->where('id',$request->id)->delete();
//        $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
        $Phones=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

        $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('profile.Country')."</th>
                                        <th>".trans('profile.phone_code')."</th>
                                        <th>".trans('profile.phone_type')."</th>
                                        <th>".trans('profile.phone_name')."</th>
                                        <th>".trans('profile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
        foreach ($Phones as $phone){
            $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                        <td align='center'><button class=\"btn btn-danger btn-sm phone-delete\" data-id=\"".$phone->id."\"  ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }
    
    public function save_phone(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'country_id'=>$request->phone_country_id,
                'phone_country_code'=>$request->phone_country_code,
                'phone_number'=>$request->phone_number,
                'phone_type'=>$request->phone_type,
                'created_by_user_id'=>Auth::user()->id,
                'created_by_site_id'=>URL::current(),
                'active'=>'1',
            );
            DB::table('users_passport_phone')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Phones=DB::table('users_passport_phone as a')
                ->join('countries as b','b.country_id','=','a.country_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->where('b.language_code',Auth::user()->language)
                ->get();
            $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('profile.Country')."</th>
                                        <th>".trans('profile.phone_code')."</th>
                                        <th>".trans('profile.phone_type')."</th>
                                        <th>".trans('profile.phone_name')."</th>
                                        <th>".trans('profile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
            foreach ($Phones as $phone){
                    $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm phone-delete\" data-id=\"".$phone->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

            }
            $output.=" </tbody></table>";
           return Response($output);
        }
    }

    public function save_email(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'email'=>$request->email,
                'created_by_user_id'=>Auth::user()->id,
                'created_by_site_id'=>URL::current(),
                'active'=>'1',
            );
         
            
            DB::table('users_passport_email')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Email=DB::table('users_passport_email')
                ->where('passport_id',Session::get('passport_id'))
                ->get();
           
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Email')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Email as $rows){
                    $output.="<tr>
                         <td>".$rows->email."</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm delete_email\" data-id=\"".$rows->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

            }
            $output.=" </tbody></table>";
           return Response($output);
        }
    }

    public function delete_email(Request $request){
        DB::table('users_passport_email')->where('id',$request->id)->delete();
        $Email=DB::table('users_passport_email')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Email')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
        foreach ($Email as $rows){
            $output.="<tr>
                         <td>".$rows->email."</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm delete_email\" data-id=\"".$rows->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }

    public function EditAddress($id){
        $Address=DB::table('users_passport_address1 as a')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('a.address_id',$id)
            ->first();
//        dd($Address);
        $AddressInfo=null;
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();

        if($Address){
            $AddressInfo=DB::table('users_passport_address2')
                ->where('language_code',Auth::user()->language)
                ->where('address_id',$Address->address_id)
                ->first();
        }

        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Address->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Address->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Address->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Address->country_id)
                ->where('state_id',$Address->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('city_id',$Address->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Address->country_id)
                        ->where('state_id',$Address->state_id)
                        ->where('city_id',$Address->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }


        $phone=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

        //  dd($Address);



        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('member.forms2.edit_step3')
            ->with('AddressInfo',$AddressInfo)
            ->with('AddressAll',$AddressAll)
            ->with('Address',$Address)
            ->with('Email',$Email)
            ->with('Phones',$phone)
            ->with('state',$state)
            ->with('city',$city)
            ->with('event','edit')
            ->with('city_sub',$city_sub)
            ->with('country',$country);

    }

    public function EditStep3(){
//        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $Address=DB::table('users_passport_address1 as a')
            ->where('a.passport_id',Session::get('passport_id'))
            ->first();
        //dd($Address);
        if(!isset($Address)){
          return $this->contact();
        }
        $AddressInfo=null;
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();
//        echo Auth::user()->language;
       // dd($AddressAll);

        if($Address){
            $AddressInfo=DB::table('users_passport_address2')
                ->where('language_code',Auth::user()->language)
                ->where('address_id',$Address->address_id)
                ->first();
        }
 //dd($AddressAll);

        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Address->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Address->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Address->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Address->country_id)
                ->where('state_id',$Address->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('city_id',$Address->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Address->country_id)
                        ->where('state_id',$Address->state_id)
                        ->where('city_id',$Address->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }


        $phone=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

      //  dd($Address);

        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('member.forms2.address')
            ->with('AddressInfo',$AddressInfo)
            ->with('AddressAll',$AddressAll)
            ->with('Address',$Address)
            ->with('Email',$Email)
            ->with('Phones',$phone)
            ->with('state',$state)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('country',$country);
    }

//    public function save_address(Request $request){
//        if($request->ajax()) {
//            $state = "";
//            $city = "";
//            $citySub = "";
//            $zip_code = '';
//
//            if (isset($request->state_id)) {
//                $state = $request->state_id;
//            }
//            if (isset($request->city_id)) {
//                $city = $request->city_id;
//            }
//            if (isset($request->city_sub1_id)) {
//                $citySub = $request->city_sub1_id;
//                $zip_code = $request->zip_code;
//            }
//            $data = array(
//                'passport_id' => Session::get('passport_id'),
//                'address_type_id' => $request->address_type,
//                'country_id' => $request->country_id,
//                'state_id' => $state,
//                'city_id' => $city,
//                'city_sub1_id' => $citySub,
//                'zip_code' => $zip_code,
//                'created_by_user_id' => Auth::user()->id,
//                'created_by_site_id' => URL::current(),
//            );
//            // dd($data);
//            $address_id=DB::table('users_passport_address1')->insertGetId($data);
//
//            $data = array(
//                'address_id' => $address_id,
//                'passport_id' => Session::get('passport_id'),
//                'address' => $request->address,
//                'additional' => $request->additional,
//                'address_type_id' => $request->address_type,
//                'language_code' => Auth::user()->language,
//                'created_by_user_id' => Auth::user()->id,
//                'created_by_site_id' => URL::current(),
//            );
//            //dd($data);
//            DB::table('users_passport_address2')->insert($data);
//            $Address=DB::table('users_passport_address1 as a')
//                ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
//                ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
//                ->join('countries as c','c.country_id','=','a.country_id')
//                ->select('a.*','b.address','b.additional','d.address_type','c.country')
//                ->where('a.passport_id',Session::get('passport_id'))
//                ->where('b.language_code',Auth::user()->language)
//                ->where('c.language_code',Auth::user()->language)
//                ->groupby('a.address_type_id')
//                ->get();
//           // dd($Address);
//                $output = "<table class=\"table table-bordered\">
//                    <thead>
//                    <tr>
//                        <th>" . trans('LProfile.AddressType') . "</th>
//                        <th>" . trans('LProfile.Address') . "</th>
//                        <th>" . trans('LProfile.Zip_code') . "</th>
//                        <th>" . trans('LProfile.action') . "</th>
//                    </tr>
//                    </thead>
//                    <tbody>";
//                foreach ($Address as $rows){
//                    $address=$rows->address.' '.$rows->additional.' '.$rows->country;
//                        $output .= "<tr>
//                            <td>" . $rows->address_type . "</td>
//                            <td>" . $address . "</td>
//                            <td>" . $rows->zip_code . "</td>
//                            <td>
//                                <button class=\"btn btn-sm btn-warning address-edit\" data-id=\"".$rows->address_id."\"'><i class=\"fa fa-edit\"></i> ".trans('LProfile.Edit')."</button>
//                                <button class=\"btn btn-sm btn-danger delete_address\" data-id='".$rows->address_id."'><i class=\"fa fa-trash\"></i> ".trans('LProfile.Delete')."</button>
//                            </td>
//                        </tr>";
//                }
//                $output.=" </tbody></table>";
//
//            return Response($output);
//
//        }
//    }

//    public function address_edit(Request $request){
//        $AddressInfo=DB::table('users_passport_address1 as a')
//            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
//            ->where('b.language_code',Auth::user()->language)
//            ->where('a.address_id',$request->address_id)
//            ->first();
////        dd($AddressInfo);
////        return Response::json($AddressInfo);
//        return response()->json($AddressInfo)->setEncodingOptions(JSON_NUMERIC_CHECK);
//    }


//    public function edit_address($id){
//        $AddressInfo=DB::table('users_passport_address1 as a')
//            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
//            ->where('b.language_code',Auth::user()->language)
//            ->where('a.address_id',$id)
//            ->first();
//   // dd($AddressInfo);
//        $Address=DB::table('users_passport_address1 as a')
//            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
//            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
//            ->join('countries as c','c.country_id','=','a.country_id')
//            ->select('a.*','b.address','b.additional','d.address_type','c.country')
//            ->where('a.passport_id',Session::get('passport_id'))
//            ->where('b.language_code',Auth::user()->language)
//            ->where('c.language_code',Auth::user()->language)
//            ->groupby('a.address_type_id')
//            ->get();
//
//        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();
//
//        return view('member.forms2.edit_step3')
//            ->with('Email',$Email)
//            ->with('Address',$Address)
//            ->with('AddressInfo',$AddressInfo);
//       // dd($Address);
////        return response()->json($AddressInfo)->setEncodingOptions(JSON_NUMERIC_CHECK);
////        return Response($AddressInfo);
//    }

    public function edit($id){
        Session::put('passport_id',$id);
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();

        $event="";
        if(!$Member){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
            $event='change';
        }
//         dd($Member);
        Session::put('member_info','( '.$Member->first_name.' '.$Member->last_name.' )');

        return view('member.forms2.edit_step1')
            ->with('event',$event)
            ->with('country',$country)
            ->with('Member',$Member);

    }

    public function delete($id){
        DB::table('users_passport')->where('passport_id',$id)->delete();
        DB::table('users_passport_info')->where('passport_id',$id)->delete();
        DB::table('users_passport_address1')->where('passport_id',$id)->delete();
        DB::table('users_passport_address2')->where('passport_id',$id)->delete();
        DB::table('users_passport_email')->where('passport_id',$id)->delete();
        DB::table('users_passport_phone')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference1')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference2')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference_phone')->where('passport_id',$id)->delete();
        return redirect()->back();
    }
//    public function edit($id){
//        $Education=DB::table('users_education')
//            ->where('active','Y')
//            ->where('language_code',Auth::user()->language)
//            ->get();
//
//        $Award=DB::table('users_award')
//            ->where('active','Y')
//            ->where('language_code',Auth::user()->language)
//            ->get();
//
//        $National=DB::table('users_nationality')
//
//            ->where('language_code',Auth::user()->language)
//            ->get();
//
//        $Occupation=DB::table('users_occupation')
//            ->where('active','Y')
//            ->where('language_code',Auth::user()->language)
//            ->get();
//
//
//        $active=DB::table('users_marital_active')
//            ->where('active','Y')
//            ->where('language_code',Auth::user()->language)
//            ->get();
//
//        $Sex=DB::table('users_sex_active')
//            ->where('active','Y')
//            ->where('language_code',Auth::user()->language)
//            ->get();
//        $Member=DB::table('users_passport')
//            ->where('passport_id',$id)
//            ->first();
//
////        $Member=DB::table('users_passport as a')
////            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
////            ->join('users_title as c','c.user_title_id','=','a.user_title_id')
////            ->join('users_sex_active as s','s.SexID','=','a.SexID')
////            ->join('users_occupation as d','d.OccupationID','=','a.OccupationID')
////            ->join('users_marital_active as e','e.MaritalactiveID','=','a.MaritalactiveID')
////            ->join('education_type as f','f.EducationTypeID','=','a.EducationTypeID')
////            ->join('users_nationality as g','g.NationalityID','=','a.CurrentNationalityID')
////            ->join('users_religion_active as h','h.ReligionID','=','a.ReligionID')
////            ->join('country as i','i.CountryID','=','a.PassportCountryID')
////            ->select('a.*','b.*','c.UserTitle','s.Sexactive','d.Occupation','e.Maritalactive','f.EducationTypeName','g.Nationality','h.Religion','i.Country')
////            ->where('b.language_code',Auth::user()->language)
////            ->where('d.language_code',Auth::user()->language)
////            ->where('s.language_code',Auth::user()->language)
////            ->where('c.language_code',Auth::user()->language)
////            ->where('e.language_code',Auth::user()->language)
////            ->where('f.language_code',Auth::user()->language)
////            ->where('g.language_code',Auth::user()->language)
////            ->where('h.language_code',Auth::user()->language)
////            ->where('i.language_code',Auth::user()->language)
////            ->where('a.passport_id',$id)
////            ->first();
//
//       // dd($Member);
//        return view('member.edit')
//            ->with('Member',$Member)
//            ->with('Sex',$Sex)
//            ->with('active',$active)
//            ->with('Occupation',$Occupation)
//            ->with('National',$National)
//            ->with('Award',$Award)
//            ->with('Education',$Education);
//    }
    public function MemberManage($id){
       // dd(Auth::user()->language);

        $Timelines=DB::table('timelines as a')
            ->join('locations as b','b.timeline_id','=','a.id')
            ->join('location_user as c','c.location_id','=','b.id')
            ->select('a.id','a.name')
            ->where('c.user_id',Auth::user()->id)
            ->get();
        foreach ($Timelines as $rows){
            $timeline[$rows->id]=$rows->name;
        }
        $timeline_options = ['' => 'Select Category'] + $timeline;
        Session::put('timeline_options',$timeline_options);

        Session::put('timeline_id',$id);
        $Members=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('b.language_code',Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->orderby('a.updated_at','desc')
            ->groupby('a.passport_id')
//            ->limit(16)
            ->get();

       // dd($Members);
        Session::put('passport_id',null);
        Session::put('mode','member');
        Session::put('mode_lang','member');
         // dd($Members);
        return view('member.list-member')
            ->with('timeline_options',$timeline_options)
            ->with('Members',$Members);
    }

    public function member_list(){

        $Members=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
//            ->join('users_title as c','c.user_title_id','=','a.user_title_id')
            ->where('b.language_code',Auth::user()->language)
            ->where('a.timeline_id',Auth::user()->timeline_id)
//            ->where('c.language_code',Auth::user()->language)
            ->orderby('a.updated_at','desc')
            ->groupby('a.passport_id')
//            ->limit(16)
            ->get();


        return view('member.list-member')
            ->with('Members',$Members);
    }

    public function MemberInfo($id){

        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->join('users_title as c','c.user_title_id','=','a.user_title_id')
            ->join('users_sex_active as s','s.SexID','=','a.SexID')
            ->join('users_occupation as d','d.OccupationID','=','a.OccupationID')
            ->join('users_marital_active as e','e.MaritalactiveID','=','a.MaritalactiveID')
            ->join('education_type as f','f.EducationTypeID','=','a.EducationTypeID')
            ->join('users_nationality as g','g.NationalityID','=','a.CurrentNationalityID')
            ->join('users_religion_active as h','h.ReligionID','=','a.ReligionID')
            ->join('country as i','i.CountryID','=','a.PassportCountryID')
            ->select('a.*','b.*','c.UserTitle','s.Sexactive','d.Occupation','e.Maritalactive','f.EducationTypeName','g.Nationality','h.Religion','i.Country')
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('s.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->where('e.language_code',Auth::user()->language)
            ->where('f.language_code',Auth::user()->language)
            ->where('g.language_code',Auth::user()->language)
            ->where('h.language_code',Auth::user()->language)
            ->where('i.language_code',Auth::user()->language)
            ->where('a.passport_id',$id)
            ->first();
        //  dd($User);
        return view('member.info')
            ->with('Member',$Member);
    }


    public function setTimeline(Request $request){
            Session::put('timeline_id',$request->timeline);
            return redirect('/member/manage/'.$request->timeline);
    }




}
