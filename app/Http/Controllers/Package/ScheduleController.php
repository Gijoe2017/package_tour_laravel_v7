<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Http\Response;
use Session;
use Image;

class ScheduleController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
//        return view('home.index');
    }

    public function schedule()
    {
        // dd(Session::get('days'));

        if(@$_GET['d']){
           Session::put('days',$_GET['d']);
        }

        if(!Session::get('days')){
            Session::put('days','1');
        }

        // dd(Session::get('days'));

        Session::put('groupImage','2');
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))->first();

        $Schedule=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('a.packageDays',Session::get('days'))
            ->where('packageID',Session::get('package'))
            ->where('b.LanguageCode',Auth::user()->language)
            ->orderBy('a.programID', 'asc')
            ->get();

        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Auth::user()->language)
            ->where('DayCode','<=',$Package->packageDays)
            ->orderby('DayCode','asc')->get();

        $Times=DB::table('package_times')
            ->where('LanguageCode',Auth::user()->language)
            ->orderby('Time_number','asc')->get();

        //  dd($NameDay);
        $HighlightToday=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
            ->where('a.packageID',Session::get('package'))
            ->where('a.packageDays',Session::get('days'))
            ->first();

        $HighlightImage=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->first();

        return view('module.packages.setting.schedule')
            ->with('HighlightToday',$HighlightToday)
            ->with('HighlightImage',$HighlightImage)
            ->with('Times',$Times)
            ->with('NameDay',$NameDay)
            ->with('Schedule',$Schedule)
            ->with('Default',$Package);
    }

   
    
    public function createProgram(){
        $HighlightToday=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
            ->where('a.packageID',Session::get('package'))
            ->where('a.packageDays',Session::get('days'))
            ->first();
        return view('module.packages.program.create1')->with('HighlightToday',$HighlightToday);
    }


    public function ImageSort(Request $request){
        if($request->ajax()){
            foreach ($request->img as $key=>$value) {
                $data=array(
                    'OrderBy'=>$key,
                );
                DB::table('package_images')->where('ImageID',$value)->update($data);
                if($key==0){
                    $check=DB::table('package_images')->where('ImageID',$value)->first();
                    $default=$check->image_path."/small/".$check->Image;
                }
            }

            return Response($default);
        }
    }

    public function sortDay(Request $request)
    {
       if($request->ajax()){
           Session::put('days',$request->id);
           //$Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
           $Schedule=DB::table('package_programs as a')
               ->join('package_program_info as b','b.programID','=','a.programID')
               ->where('packageID',Session::get('package'))
               ->where('a.packageDays',$request->id)
               ->where('b.LanguageCode',Session::get('Language'))
               ->get();

           $HighlightToday=DB::table('package_program_highlight as a')
               ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
               ->where('a.packageID',Session::get('package'))
               ->where('a.packageDays',Session::get('days'))
               ->first();

           $output='';
           if(isset($HighlightToday)){
               $Breakfast=$HighlightToday->Breakfast=='Y'?'อาหารเช้า':'';
               $Lunch=$HighlightToday->Lunch=='Y'?', อาหารกลางวัน':'';
               $Dinner=$HighlightToday->Dinner=='Y'?', อาหารเย็น':'' ;
               $output.=" <div class=\"col-md-12\" >
                                <div class='pull-right'>
                                  <a href=\"".url('editHighlight/'.$HighlightToday->HighlightID)."\">
                                  <i class=\"fa fa-edit\"></i> ".trans('LPackageTour.Edit')."
                                  </a>
                                </div>
                              <h4>". Markdown::convertToHtml($HighlightToday->Highlight) ."</h4>
                              <p>Foot for to day : ".$Breakfast.$Lunch.$Dinner." </p>
                          </div>";
           }
           if(isset($Schedule)) {
               $output.=" <table class=\"table\">
                            <thead>
                            <tr>
                                <th>".trans('LPackageTour.Time')."</th>
                                <th>".trans('LPackageTour.PackageHighlight')."</th>
                                <th><div align=\"center\">".trans('LPackageTour.Action')."</div> </th>
                            </tr>
                            </thead><tbody>";
               foreach ($Schedule as $rows){
                   $output.= " <tr>
                            <td>".$rows->packageTime."</td>
                            <td>".Markdown::convertToHtml($rows->packageDetails)."</td>
                            <td align=\"right\">
                                <a class=\"btn btn-sm btn-info\" href=\"".url('package/program/copy/'.$rows->programID)."\" class=\"btn btn-sm\"><i class=\"fa fa-copy\"></i> Copy</a>
                                <a class=\"btn btn-sm btn-success\" href=\"".url('package/program/highlight/'.$rows->programID)."\" class=\"btn btn-sm\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> Highlight</a>
                                <a class=\"btn btn-sm btn-default\" href=\"".url('package/program/edit/'.$rows->programID)."\" class=\"btn btn-sm\"><i class=\"fa fa-edit\"></i> Edit</a>
                                <a class=\"btn btn-sm btn-danger\" href=\"".url('package/program/del/'.$rows->programID)."\" class=\"btn btn-sm\" onclick=\"return confirmDel()\"><i class=\"fa fa-trash\"></i> Delete</a>
                            </td>
                        </tr>";
               }
               $output.="</tbody></table>";
           }
           return Response($output);
       }

    }

    public function store(Request $request){
        $post=$request->all();

//        Session::put('days',$post['Days']);
        $v=\Validator::make($request->all(),[
            'packageTime'=>'required',
            'packageDetails'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            //$Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
            if(isset($post['packageTime2'])){
                $Time2=$post['packageTime2'];
            }else{
                $Time2="";
            }

            $data = array(
                'packageID'=>Session::get('package'),
                'packageDays'=>Session::get('days'),
                'packageDate'=>date('Y-m-d'),
                'packageTime'=>$post['packageTime'],
                'packageTime2'=>$Time2,
//                'packageOrder'=>$post['packageOrder'],
            );
            $programID= DB::table('package_programs')->insertGetId($data);


            if ($request->hasFile('picture')) {
                $pImage = DB::table('package_images')
                    ->where('packageID', Session::get('package'))
                    ->where('GroupImage','2')
                    ->first();
                if(isset($pImage)){
                    $fileNameDel = public_path('package-tour/mid/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    $fileNameDel = public_path('package-tour/small/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                }

                $artPics = $request->file('picture');

                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/small/' . $filename));

                if ($ori_w >= 480) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 480;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 350;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/x-small/' . $filename));

                $data = array(
                    'packageID' => Session::get('package'),
                    'programID' => $programID,
                    'GroupImage' => '2',
                    'image_path' => Session::get('MyImagePath'),
                    'Image' => $filename,
//                    'OrderBy'=>'1'
                );
                DB::table('package_images')->insert($data);
            }

            $data = array(
                'programID'=>$programID,
              //  'packageTitle'=>$post['packageTitle'],
                'packageDetails'=>$post['packageDetails'],
                'LanguageCode'=>Auth::user()->language
            );

            $i= DB::table('package_program_info')->insert($data);

            if($i>0){
                if(Session::has('checking')) {
                    return redirect('package/details_check/' . Session::get('package'));
                }elseif(Session::has('detail')){
                    return redirect('package/details/'.Session::get('package'));
                }else{
                    return redirect('/package/schedule');
                }
            }

        }
    }

    public function copyProgram($id){
        $Program=DB::table('package_programs')->where('programID',$id)->first();
        if(isset($Program)){
            $data=array(
                'packageID'=>$Program->packageID,
                'packageDays'=>$Program->packageDays,
                'packageDate'=>$Program->packageDate,
                'packageTime'=>$Program->packageTime,
                'packageTime2'=>$Program->packageTime2,
//                'packageOrder'=>$Program->packageOrder
            );
            $programID=DB::table('package_programs')->insertGetId($data);
        }

        $ProgramInfo=DB::table('package_program_info')->where('programID',$id)->get();
        if(isset($ProgramInfo)) {
            foreach ($ProgramInfo as $rows){
                $data = array(
                    'programID' => $programID,
                    'packageDetails' => $rows->packageDetails,
                    'LanguageCode' => $rows->LanguageCode
                );
                 DB::table('package_program_info')->insert($data);
            }
        }
        // return redirect('package/schedule');
        if(Session::has('checking')) {
            return redirect('package/details_check/' . Session::get('package'));
        }elseif(Session::has('detail')){
            return redirect('package/details/'.Session::get('package'));
        }else{
            return redirect('/package/schedule');
        }

    }

    public function getTime(Request $request){
        if($request->ajax()){
            $Times=DB::table('package_times')->where('TimeCode',$request->id)->first();
            return $Times->Time_number;
        }
    }

    public function editProgram($id){
        Session::put('programID',$id);
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        $Program=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('LanguageCode',Auth::user()->language)
            ->where('b.programID',$id)->first();
      //  dd($Program);
        $Photos=DB::table('package_images')
            ->where('GroupImage','2')
            ->where('programID',$id)
            ->where('packageID',Session::get('package'))
            ->orderby('OrderBy','asc')
            ->get();

        $TourImage=DB::table('package_images')
            ->where('GroupImage','2')
            ->where('programID',$id)
            ->where('packageID',Session::get('package'))
            ->orderby('OrderBy','asc')
            ->first();

        $Times=DB::table('package_times')
            ->where('LanguageCode',Auth::user()->language)
            ->orderby('Time_number','asc')->get();

        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Auth::user()->language)
            ->where('DayCode',$Program->packageDays)->first();

        return view('module.packages.program.edit-program')
        // return view('packages.setting.package-edit')
            ->with('Times',$Times)
            ->with('TourImage',$TourImage)
            ->with('Default',$Package)
            ->with('SiteName',$SiteName)
            ->with('NameDay',$NameDay)
            ->with('Photos',$Photos)
            ->with('Program',$Program);
    }

    public function highlight($id){
        Session::put('programID',$id);
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Program=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('LanguageCode',Session::get('Language'))
            ->where('b.programID',$id)->first();

        $TourImage=DB::table('package_images')
            ->where('GroupImage','2')
            ->where('programID',$id)
            ->where('packageID',Session::get('package'))
            ->orderby('OrderBy','asc')
            ->first();

        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Session::get('Language'))
            ->where('DayCode',$Program->packageDays)->first();

        $MyItems=DB::table('item as a')
            ->join('item_info as b','b.ItemID','=','a.ItemID')
            ->where('LanguageCode',Session::get('Language'))
            ->where('a.CreateByID',Auth::user()->id)
            ->groupby('a.ItemID')
            ->get();

        $MyLocation=DB::table('package_location_info as a')
            ->join('package_images as b','b.ImageID','=','a.ImageID')
            ->where('a.LanguageCode',Session::get('Language'))
            ->where('a.CreateByID',Auth::user()->id)
            ->get();

        return view('package.program.highlight_schedule')
            ->with('TourImage',$TourImage)
            ->with('Default',$Package)
            ->with('NameDay',$NameDay)
            ->with('MyItems',$MyItems)
            ->with('MyLocation',$MyLocation)
            ->with('Program',$Program);
    }

    public function create(){
        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('DayCode','asc')->get();

        return view('package.program.create');
    }

    public function updateProgram(Request $request){
        $post=$request->all();

        $v=\Validator::make($request->all(),[
            'packageTime'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{
            $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
            if(isset($post['packageTime2'])){
                $Time2=$post['packageTime2'];
            }else{
                $Time2="";
            }
            $data = array(
                'packageID'=>Session::get('package'),
                'packageDays'=>$post['Days'],
                'packageTime'=>$post['packageTime'],
                'packageTime2'=>$Time2,
//                'packageOrder'=>$post['packageOrder'],
            );
            DB::table('package_programs')
                ->where('programID',$post['id'])
                ->update($data);

            $data = array(
                'packageDetails'=>$post['packageDetails'],
                'LanguageCode'=>Auth::user()->language
            );
            DB::table('package_program_info')
                ->where('programID',$post['id'])
                ->update($data);

//            return back();
//                return redirect('/package/schedule?d='.$post['Days']);
            if(Session::has('checking')) {
                return redirect('package/details_check/' . Session::get('package'));
            }elseif(Session::has('detail')){
                    return redirect('package/details/'.Session::get('package'));
            }else{
                return redirect('/package/schedule');
            }

        }
    }

    public function showImage(Request $request){
        if($request->ajax()){
            // $output = "";
            $Img = DB::table('package_images')
                ->where('programID', Session::get('programID'))
                ->where('packageID', Session::get('package'))
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\">";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"> </li>';

            }
            $output.='</ul>';

            return Response($output);
        }
    }

    public function deleteImage(Request $request)
    {
        if ($request->ajax()) {
            $image = DB::table('package_images')->where('ImageID', $request->picID)->first();
            $fileNameDel = public_path('package-tour/small/' . $image->Image);
            $fileNameDel2 = public_path('package-tour/mid/' . $image->Image);

            if (file_exists($fileNameDel) and $image->Image!='default-add.jpg') {
                unlink($fileNameDel);
                unlink($fileNameDel2);
            }

            DB::table('package_images')->where('ImageID', $image->ImageID)->delete();
           

            $check=DB::table('package_location_info')->where('ImageID', $image->ImageID)->first();
            if(isset($check)){
                DB::table('package_location_info')->where('ImageID', $image->ImageID)->delete();
                DB::table('highlight_in_schedule')->where('LocationID', $check->LocationID)->delete();
            }


            //  $output = "";
            $Img = DB::table('package_images')
                ->where('packageID', Session::get('package'))
                ->where('programID', Session::get('programID'))
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\" >";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"></li>';
            }
            $output.="</ul>";
            return Response($output);
        }
    }

    public function delProgram($id){
        $Program=DB::table('package_programs')->where('programID',$id)->first();

        if(isset($Program)) {
            $Image = DB::table('package_images')
                ->where('GroupImage', '2')
                ->where('packageID', $Program->packageID)->get();

            foreach ($Image as $rows) {

                $fileNameDel = public_path('package-tour/mid/' . $rows->Image);
               
                if (file_exists($fileNameDel) and $rows->Image != 'default.jpg') {
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('package-tour/small/' . $rows->Image);
                if (file_exists($fileNameDel) and $rows->Image != 'default.jpg') {
                    unlink($fileNameDel);
                }

                DB::table('location_images_info')->where('ImageID', $rows->ImageID)->delete();
            }
            DB::table('package_images')
                ->where('GroupImage', '2')
                ->where('packageID', $Program->packageID)->delete();

            DB::table('package_program_info')->where('programID', $id)->delete();
            DB::table('package_programs')->where('programID', $id)->delete();
        }

        if(Session::has('checking')) {
            return redirect('package/details_check/' . Session::get('package'));
        }else if(Session::get('event')=='details'){
                return redirect('package/details/'.Session::get('package'));
        }else{
            return redirect('/package/schedule');
        }
            // return redirect('package/schedule');

    }
    



}
