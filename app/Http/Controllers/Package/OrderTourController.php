<?php

namespace App\Http\Controllers\Package;

use App\Bookings;
use App\City;
use App\CitySub1;
use App\Country;
use App\PaymentNotification;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Intervention\Image\Response;
use Soumen\Agent\Agent;
use Session;
use Auth;
use Cookie;
use App;
//use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;

use Illuminate\Support\Facades\Mail;

class OrderTourController extends Controller
{
    //

    public function get_rate($rate){
        $Rate=DB::table('package_tour_commission_rate')
            ->where('price_rate_start','<=',$rate)
            ->where('price_rate_end','>=',$rate)
            ->first();
        if(!$Rate){
            $Rate=DB::table('package_tour_commission_rate')
                ->orderby('commission_rate','asc')
                ->first();
        }
        return $Rate->commission_rate;
    }

    public function someone_share(Request $request){
        DB::table('package_booking_cart_additional')->where('id',$request->id)->update(['need_someone_share'=>$request->status]);
        if($request->step=='1'){
            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }
    }


    public function invoice_detail($id){
        $Invoice=DB::table('package_invoice')
            ->where('invoice_id',$id)
            ->first();

        return view('packages.order.invoice-details')
            ->with('Invoice',$Invoice);
    }

    public function invoice_detail_all($id){
        $Invoices=DB::table('package_invoice')
            ->where('invoice_booking_id',$id)
            ->get();
        //dd($Invoice);
        return view('packages.order.invoice-detail-all')
            ->with('Invoices',$Invoices);
    }

    public function status_booking($id){
        $str=str_replace('TC','',$id);
        $id=$str*1;

        $Invoices=DB::table('package_invoice')->where('invoice_booking_id',$id)->orderby('invoice_status','asc')->get();
        //dd($Invoices);
        return view('packages.order.show-booking-details')
            ->with('Invoices',$Invoices);
    }



    public function update_status_booking($id,$booking){
        $checkInvoice=DB::table('package_invoice')->where('invoice_id',$id)->first();
        $status='4';
        if($checkInvoice->invoice_type=='1'){
            $status='2';
        }

        $payment=DB::table('payment_notification as a')
            ->join('payment_notification_sub as b','b.payment_id','=','a.payment_id')
            ->where('a.invoice_booking_id',$booking)
            ->where('b.status','s')
            ->first();

        $data=array(
            'invoice_status'=>$status,
            'payment_option'=>'2',
            'payment_date'=>$payment->transfer_date,
            'payment_time'=>$payment->transfer_time,
        );
        DB::table('package_invoice')->where('invoice_id',$id)->update($data);


        $Details=DB::table('package_invoice')->where('invoice_id',$id)->first();
        DB::table('package_booking_details')
            ->where('package_detail_id',$Details->invoice_package_detail_id)
            ->where('booking_id',$Details->invoice_booking_id)
            ->where('package_id',$Details->invoice_package_id)
            ->update(['booking_detail_status'=>$status,'updated_at'=>date('Y-m-d H:i:s')]);

        DB::table('payment_notification_sub')->where('payment_invoice_id',$id)->update(['status'=>'y']);

        $check=DB::table('payment_notification as a')
            ->join('payment_notification_sub as b','b.payment_id','=','a.payment_id')
            ->where('a.invoice_booking_id',$booking)
            ->where('b.status','s')
            ->first();
       // dd($check);

        if(!$check){
            DB::table('payment_notification')->where('invoice_booking_id',$booking)->update(['status'=>'y']);
            $checkPay=DB::table('package_invoice')
                ->where('invoice_id',$id)
                ->where('invoice_type','2')
                ->where('invoice_status','4')
                ->first();
            if($checkPay){
                $data=array(
                    'booking_status'=>2,
                    'lastupdate'=>date('Y-m-d H:i:s'),
                    'update_by'=>Auth::user()->id
                );
                DB::table('package_bookings')->where('booking_id',$booking)->update($data);
            }
        }


        $checkBooking=DB::table('package_invoice')
            ->where('invoice_booking_id',$booking)
            ->where('invoice_type','1')
            ->first();

        if($checkInvoice->invoice_type==1 || !$checkBooking){
            $Booking=DB::table('package_booking_details')->where('booking_id',$booking)->get();
            foreach ($Booking as $rows){
                DB::table('package_details')->where('packageDescID',$rows->package_detail_id)->decrement('NumberOfPeople', $rows->number_of_person);
            }
        }

        $data=array(
            'invoice_id'=>$id,
            'invoice_type'=>$checkInvoice->invoice_type,
            'payment_option'=>'2',
            'payment_date'=>$payment->transfer_date,
            'payment_time'=>$payment->transfer_time,
            'payment_amount'=>$payment->transfer_amount,
            'payment_status'=>'p',
        );
        DB::table('package_payments')->insert($data);

        $message=trans('common.order_id').$booking;
        $Payment=DB::table('payment_notification')->where('invoice_booking_id',$booking)->first();
        //dd($Payment);

        $dataMail=array(
            'email'=>$Payment->tax_email,
            'name'=>$Payment->tax_business_name,
            'message_customer'=>$message,
            'booking_id'=>$booking,
            'invoice_date'=>date('Y-m-d H:i:s'),
            'subject'=>trans('email.payment_confirmation')
        );
        //  dd($dataMail);

        Mail::send('emails.payment-confirm', $dataMail, function ($m) use ($dataMail) {
            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
            $m->to($dataMail['email'], $dataMail['name'])->subject(trans('email.payment_confirmation'));
        });

        return redirect()->back();
    }





    public function payment_notification(Request $request){

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

            $data=array(
                'need_tax_invoice'=>$request->need_tax_invoice,
                'invoice_booking_id'=>$request->invoice_booking_id,
                'transfer_amount'=>$request->transfer_amount,
                'transfer_phone'=>$request->transfer_phone,
                'transfer_date'=>$request->transfer_date,
                'transfer_time'=>$request->transfer_time,
                'transfer_bank'=>$request->transfer_bank,
                'bank_transfer'=>$request->bank_transfer,
                'sub_bank'=>$request->sub_bank,
                'transfer_remark'=>$request->transfer_remark,
                'status'=>'s'
            );

            $id=DB::table('payment_notification')->insertGetId($data);

            if(is_array($request->invoice_no)){
                foreach ($request->invoice_no as $key=>$val) {
                    $Invoice=DB::table('package_invoice')->where('invoice_id',$val)->first();
                    $data = array(
                        'payment_id' => $id,
                        'payment_invoice_id' => $val,
                        'payment_amount' => $Invoice->invoice_amount,
                        'status' => 's',
                    );
                    DB::table('payment_notification_sub')->insert($data);
                }
            }

            if($request->file){
                $dir=public_path('/images/member/slip/') ;
                $type='image';
                $extension=$request->file('file')->getClientOriginalExtension();
                $fileName = $request->invoice_booking_id.'-'.$val.'-'.rand(1,9). '.' . $extension; // renameing image

                if($extension=='pdf' || $extension=='doc' || $extension=='xls' || $extension=='docx' || $extension=='xlsx') {
                    $type='file';
                }

                $upload_success = $request->file('file')->move($dir, $fileName); //

                if($upload_success){
                    $data=array(
                        'transfer_slip'=>$fileName,
                        'transfer_slip_type'=>$type,
                    );
                    DB::table('payment_notification')->where('payment_id',$id)->update($data);
                }
            }

            if($request->add_address=='1'){

                $tax_address=$request->address;
                $Country=Country::where('language_code',Auth::user()->language)->where('country_id',$request->country_id)->first();
                if(!$Country){
                    $Country=Country::where('language_code','en')->where('country_id',$request->country_id)->first();
                }

                $State=State::where('language_code',Auth::user()->language)->where('state_id',$request->state_id)->where('country_id',$request->country_id)->first();
                if(!$State){
                    $State=State::where('language_code','en')->where('state_id',$request->state_id)->where('country_id',$request->country_id)->first();
                }

                $City=City::where('language_code',Auth::user()->language)->where('city_id',$request->city_id)->where('country_id',$request->country_id)->first();
                if(!$City){
                    $City=City::where('language_code','en')->where('city_id',$request->city_id)->where('country_id',$request->country_id)->first();
                }

                if(isset($request->city_sub1_id)){
                    $CitySub=CitySub1::where('language_code',Auth::user()->language)->where('city_sub1_id',$request->city_id)->where('city_sub1_id',$request->country_id)->first();
                    if(!$CitySub){
                        $CitySub=CitySub1::where('language_code','en')->where('city_sub1_id',$request->city_sub1_id)->where('country_id',$request->country_id)->first();
                    }
                }

                $tax_address.=$CitySub->city_sub1_name.' ';
                $tax_address.=$City->city.' ';
                $tax_address.=$State->state.', ';
                $tax_address.=$Country->country.' ';

                $data=array(
                    'tax_business_name'=>$request->tax_business_name,
                    'id_card_number'=>$request->id_card_number,
                    'tax_email'=>$request->tax_email,
                    'tax_phone'=>$request->tax_phone,
                    'tax_address'=>$tax_address.$request->zip_code,
                );
                DB::table('payment_notification')->where('payment_id',$id)->update($data);
                DB::table('address_book')->where('timeline_id',Auth::user()->timeline_id)->update(['default_address'=>'0']);

                $city_sub=array();
                $city=array();
                $states=array();

                $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
                if (!$country) {
                    $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
                }

                if (isset($request->state_id)){
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();
                    if (!$states) {
                        $states = DB::table('states')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_id)){
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city) {
                            $city = DB::table('cities')
                                ->where('country_id', $request->country_id)
                                //->where('state_id',$AddressBook->entry_state)
                                ->where('city_id', $request->city_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                        if(isset($request->city_sub1_id)){
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', Auth::user()->language)
                                ->first();

                            if (!$city_sub) {
                                $city_sub = DB::table('cities_sub1')
                                    ->where('country_id', $request->country_id)
                                    ->where('state_id', $request->state_id)
                                    ->where('city_id', $request->city_id)
                                    ->where('city_sub1_id', $request->city_sub1_id)
                                    ->where('language_code', 'en')
                                    ->first();
                            }
                        }

                    }

                }

                $address_show=$request->address.' ';
                if($city_sub){
                    $address_show.=$city_sub->city_sub1_name.'<BR>';
                }
                if($city){
                    $address_show.=$city->city.' ';
                }
                if($states){
                    $address_show.=$states->state.', ';
                }
                $address_show.=$country->country.' '.$request->zip_code;



                $data=array(
                    'timeline_id'=>Auth::user()->timeline_id,
                    'address_type'=>'2',
                    'id_card_number'=>$request->id_card_number,
                    'default_address'=>'1',
                    'entry_company'=>$request->tax_business_name,
                    'entry_phone'=>$request->tax_phone,
                    'entry_email'=>$request->tax_email,
                    'entry_street_address'=>$request->address,
                    'entry_postcode'=>$request->zip_code,
                    'entry_city'=>$request->city_id,
                    'entry_city_sub'=>$request->city_sub1_id,
                    'entry_state'=>$request->state_id,
                    'entry_country_id'=>$request->country_id,
                    'address_show'=>$address_show,
                );
                DB::table('address_book')->insert($data);
                $email=$request->tax_email;
                $name=$request->tax_business_name;
            }else{

                $address=DB::table('address_book')
                    ->where('timeline_id',Auth::user()->timeline_id)
                    ->where('address_type','2')
                    ->where('default_address','1')
                    ->first();
                if(!$address){
                    $address=DB::table('address_book')
                        ->where('timeline_id',Auth::user()->timeline_id)
                        ->where('address_type','1')
                        ->where('default_address','1')
                        ->first();
                }

                $data=array(
                    'tax_business_name'=>$address->entry_company,
                    'id_card_number'=>$address->id_card_number,
                    'tax_email'=>$address->entry_email,
                    'tax_phone'=>$address->entry_phone,
                );
                DB::table('payment_notification')->where('payment_id',$id)->update($data);
                $email=$address->entry_email;
                $name=$address->entry_company;
            }

            if($request->type=='all') {
                $message = trans('email.thank_you_for_paying_for_the_tour') .' #'. $request->invoice_booking_id;
                $message_admin = trans('email.tour_payment_notification_from') . Auth::user()->name.' '.trans('common.emails').' '.Auth::user()->email;
            }else if($request->type=='1'){
                $message = trans('email.thank_you_for_paying_deposit_number') .' #'. $request->invoice_booking_id;
                $message_admin = trans('email.notify_deposit_payment_from') . Auth::user()->name.' '.trans('common.emails').' '.Auth::user()->email;
            }else{
                $message = trans('email.thank_you_for_paying_the_rest_of_the_tour_fee') .' #'. $request->invoice_booking_id;
                $message_admin = trans('email.payment_of_the_remaining_tour_payment_from') . Auth::user()->name.' '.trans('common.emails').' '.Auth::user()->email;
            }

            $dataMail=array(
                'email'=>$email,
                'name'=>$name,
                'message_customer'=>$message,
                'message_admin'=>$message_admin,
                'booking_id'=>$request->invoice_booking_id,
                'invoice_date'=>$request->transfer_date.' '.$request->transfer_time,
                'subject'=>trans('email.thanks_for_payment')
            );

            Mail::send('emails.payment-notification', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject('Payment notification');
            });

            Mail::send('emails.payment-notification-admin', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to('sale@toechok.com', 'Sale Toechok.com')->subject('Payment notification.');
            });

            Session::flash('message',$message);
            return redirect()->back();
    }

    public function payment_list(){
     //   App::setLocale(Auth::user()->language);
        Session::put('currentUrl',url()->current());
        $Payments=DB::table('payment_notification as a')
            ->join('payment_notification_sub as b','b.payment_id','=','a.payment_id')
            ->where('b.status','s')
            ->get();
        // dd($Payments);
        return view('packages.booking.payment_list')
            ->with('Payments', $Payments);
    }

    public function waiting_confirm_list(){

        $Bookings=DB::table('package_booking_details as a')
            ->join('package_bookings as b','b.booking_id','=','a.booking_id')
            ->join('booking_status as c','c.id','=','b.booking_status')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.booking_detail_status','7')
            ->groupby('a.booking_id')
            ->orderby('b.booking_date','desc')
            ->get();
        $Status=DB::table('booking_status')->get();
         //dd($Bookings);
        return view('packages.booking.waiting-confirm-list')
            ->with('Status', $Status)
            ->with('Bookings', $Bookings);
    }


    public function booking_list(){
        Session::put('currentUrl',url()->current());
        $Bookings=DB::table('package_booking_details as a')
            ->join('package_bookings as b','b.booking_id','=','a.booking_id')
            ->join('booking_status as c','c.id','=','b.booking_status')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->groupby('a.booking_id')
            ->orderby('b.booking_date','desc')
            ->get();

        $Status=DB::table('booking_status')->get();

//        $Invoices=DB::table('package_invoice')->where('invoice_timeline_id',Session::get('timeline_id'))->groupby('invoice_booking_id')->get();
       // dd($Invoices);
        return view('packages.booking.list')
               ->with('Status', $Status)
            ->with('Bookings', $Bookings);

    }

    public function sort_by_status(Request $request){
        Session::put('status',$request->status);
        $Bookings=DB::table('package_booking_details as a')
            ->join('package_bookings as b','b.booking_id','=','a.booking_id')
            ->join('booking_status as c','c.id','=','b.booking_status')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->whereIn('package_detail_id',function ($query){
                $query->select('invoice_package_detail_id')->from('package_invoice')
                    ->where(['invoice_type'=>'1'])
                    ->where('invoice_status',Session::get('status'));
            })
            ->groupby('a.booking_id')
            ->orderby('b.booking_date','desc')
            ->get();

      //  dd($Bookings);
        return view('packages.booking.list-by-status')
            ->with('Bookings', $Bookings);
    }







    public function main()
    {
        \Date::setLocale(Session::get('language'));
//        if(Session::get('country') || Session::get('partner') || Session::get('txtsearch')){
//            return redirect('/package/list_search');
//        }
//        dd();check_package
        Session::forget('passport_id');
        Session::forget('checking');
        $PackageCountry=DB::table('package_details as a')
             ->join('package_tour as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','a.Country_id')
            ->select('c.*')
            ->where('b.timeline_id',Session::get('timeline_id'))
            ->whereIn('a.packageID',function ($query){
                $query->select('packageID')->from('package_tourin');
            })
            ->where('c.country_id','>','0')
            ->groupby('a.Country_id')
            ->where('b.packageStatus', 'CP')
            ->where('b.package_close', 'N')
            ->get();


        //dd($PackageCountry);
        Session::put('PackageCountry',$PackageCountry);

        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.packageName', 'b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus', '!=', 'X')
            ->where('a.packageStatus', 'CP')
            ->where('a.package_close', 'N')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
            ->paginate(12, ['*'], 'Packages');

       // dd($Packages);

        Session::put('mode','package');
        Session::put('event','');

        return view('packages.booking.main')
            ->with('PackageCountry', $PackageCountry)
            ->with('Packages', $Packages);
    }

    public function main_list()
    {
        \Date::setLocale(Session::get('language'));

        Session::forget('passport_id');
        Session::forget('checking');
        $PackageCountry=DB::table('package_details as a')
             ->join('package_tour as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','a.Country_id')
            ->select('c.*')
            ->where('b.timeline_id',Session::get('timeline_id'))
            ->whereIn('a.packageID',function ($query){
                $query->select('packageID')->from('package_tourin');
            })
            ->where('c.country_id','>','0')
            ->groupby('a.Country_id')
            ->where('b.packageStatus', 'CP')
            ->where('b.package_close', 'N')
            ->get();

        //dd($PackageCountry);
        Session::put('PackageCountry',$PackageCountry);

        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.packageName', 'b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus', '!=', 'X')
            ->where('a.packageStatus', 'CP')
            ->where('a.package_close', 'N')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
            ->paginate(12, ['*'], 'Packages');

        //dd($Packages);

        Session::put('mode','package');
        Session::put('event','');

        return view('packages.booking.main-list')
            ->with('PackageCountry', $PackageCountry)
            ->with('Packages', $Packages);
    }

    public function check_package($id){
        $PackageDetails=DB::table('package_details as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->where('a.NumberOfPeople','>','0')
            ->where('a.packageID',$id)
            ->where('a.status','Y')
            ->get();
        //dd($PackageDetails);
        return view('packages.booking.check_seat')
            ->with('PackageDetails',$PackageDetails);
    }

    public function check_seat(Request $request){

        $PackageDetail=DB::table('package_details as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->where('a.packageDescID',$request->id)
            ->where('a.status','Y')
            ->first();

        return view('packages.booking.check_popup_seat')
            ->with('person',$request->person)
            ->with('booking',$request->booking)
            ->with('PackageDetail',$PackageDetail);
    }

    public function confirm_seat($booking,$id){
        DB::table('package_booking_details')->where('booking_id',$booking)->where('package_detail_id',$id)->update(['booking_detail_status'=>'1','updated_at'=>date('Y-m-d H:i:s')]);

        $Invoice=DB::table('package_invoice')->where('invoice_booking_id',$booking)->where('invoice_package_detail_id',$id)->get();
        foreach ($Invoice as $rows){
            DB::table('package_invoice')->where('invoice_id',$rows->invoice_id)->update(['invoice_status'=>'1']);
        }

        $User=DB::table('package_bookings as a')
            ->join('users as b','b.id','=','a.booking_by')
            ->where('a.booking_id',$booking)
            ->first();

        $dataMail=array(
            'email'=>$User->email,
            'name'=>$User->name,
            'subject'=>trans('email.this_email_confirms_seat_availability').'-'.trans('common.order_id').$booking,
            'message'=>trans('email.confirm_booking_date').date('Y-m-d H:i:s'),
            'booking_id'=>$booking,
        );

        Mail::send('emails.respond-booking-confirmation', $dataMail, function ($m) use ($dataMail) {
            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
            $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
        });

        return back();
    }


    public function cancel_seat($booking,$id){
        DB::table('package_booking_details')->where('booking_id',$booking)->where('package_detail_id',$id)->update(['booking_detail_status'=>'6','updated_at'=>date('Y-m-d H:i:s')]);

        $Invoice=DB::table('package_invoice')->where('invoice_booking_id',$booking)->where('invoice_package_detail_id',$id)->get();
        foreach ($Invoice as $rows){
            DB::table('package_invoice')->where('invoice_id',$rows->invoice_id)->update(['invoice_status'=>'6']);
        }

        $message_cancel=trans('common.message_cancel_number_tourist_fully').'<br>'.trans('common.order_id').':#'.$booking;

        $User=DB::table('package_bookings as a')
            ->join('users as b','b.id','=','a.booking_by')
            ->where('a.booking_id',$booking)
            ->first();

        $Package=DB::table('package_booking_details')
            ->where('booking_id',$booking)
            ->where('package_detail_id',$id)
            ->first();

        $dataMail=array(
            'email'=>$User->email,
            'name'=>$User->name,
            'subject'=>trans('email.cancellation'),
            'message_details'=>trans('common.this_email_is_to_inform_you_that_your_tour_package_has_been_canceled').'<br>'.$message_cancel,
            'package_title'=>$Package->package_detail_title,
            'booking_id'=>$booking,

        );
        //dd($dataMail) ;
        Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
            $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
        });

        return back();
    }



    public function package($id){

//        $sessionID=Agent::header('user-agent');
//        Session::put('sessionID',$sessionID);

        Session::put('package',$id);
        $PackageDesc=DB::table('package_details as a')
            ->where('a.packageID',Session::get('package'))
            ->where('a.status','Y')
            ->orderby('a.packageDateStart','desc')
            ->get();

        return view('packages.booking.details')
            ->with('PackageDesc',$PackageDesc);
    }

    public function add_cart(Request $request){

        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDescID','d.packageDateEnd','e.TourType','e.price_system_fees','e.Price_by_promotion', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('d.packageDescID',$request->id)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->first();

       // dd($PackageTourOne);
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->first();


        $data=array(
            'booking_cart_date'=>date('Y-m-d H:i:s'),
            'session_id'=>Session::get('sessionID'),
            'auth_id'=>Auth::user()->id
        );

        if(!$Cart){
            $booking_cart_id=DB::table('package_booking_cart')->insertGetId($data);
        }else{
            $booking_cart_id=$Cart->booking_cart_id;
        }
       // dd($data);
        $booking_cart_person_number=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->sum('booking_cart_person_number');

        $tour_type=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->where('package_detail_id',$request->id)
            ->where('tour_type',$request->tour_type)
            ->first();
        //dd($tour_type);
        if($tour_type){
            $data=array(
                'number_of_person'=>($tour_type->number_of_person+$request->number_of_person),
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );

            DB::table('package_booking_cart_details')
                ->where('booking_cart_id',$booking_cart_id)
                ->where('package_detail_id',$request->id)
                ->where('tour_type',$request->tour_type)
                ->update($data);
        }else{

            $data=array(
                'booking_cart_id'=>$booking_cart_id,
                'tour_type'=>$request->tour_type,
                'item_status'=>'Y',
                'package_id'=>$PackageTourOne->packageID,
                'package_detail_id'=>$PackageTourOne->packageDescID,
                'booking_cart_normal_price'=>$PackageTourOne->PriceSale,
                'booking_cart_realtime_price'=>$PackageTourOne->Price_by_promotion,
                'number_of_person'=>$request->number_of_person,
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );
           //  dd($data);
            $booking_cart_detail_id=DB::table('package_booking_cart_details')->insertGetId($data);
            $Promotion=DB::table('package_promotion')
                ->where('promotion_operator','Between')
                ->where('promotion_status','Y')
                ->where('packageDescID',$PackageTourOne->packageDescID)
                ->orderby('promotion_date_start','asc')
                ->first();
            if(!$Promotion){
                $Promotion=DB::table('package_promotion')
                    ->where('promotion_operator','Mod')
                    ->where('packageDescID',$PackageTourOne->packageDescID)
                    ->where('promotion_status','Y')
                    ->first();
            }
            if($Promotion){
                $data=array(
                    'booking_cart_detail_id'=>$booking_cart_detail_id,
                    'promotion_title'=>$Promotion->promotion_title,
                    'promotion_date'=>date('Y-m-d'),
                );
                DB::table('package_booking_cart_promotion')->insert($data);
            }

        }

                    $countCart=0;
                    if(Auth::check()){
                        $Mycart=\App\Mycart::where('auth_id',Auth::user()->id)->get();
                        if($Mycart){
                            foreach ($Mycart as $cart){
                                $countCart+=$cart->count_cart()->groupby('package_id')->get()->count();
                            }
                        }
                        $countWithlist=DB::table('package_wishlist')->where('user_id',Auth::user()->id)->count();
                    }else{
                        $Mycart=\App\Mycart::where('session_id',Session::getId())->first();
                        if($Mycart){
                            $countCart=$Mycart->count_cart()->groupby('package_id')->get()->count();
                        }
                    }

        return $countCart;
    }

    public function add_withlish(Request $request){
        $data=array(
            'packageDescID'=>$request->id,
            'user_id'=>Auth::user()->id,
            'date_wishlist'=>date('Y-m-d H:i:s')
        );
        $check=DB::table('package_wishlist')
            ->where('packageDescID',$request->id)
            ->where('user_id',Auth::user()->id)
            ->first();

        if(!$check){
            DB::table('package_wishlist')->insert($data);
        }

        return $this->show_cart();
    }

    public function delete_withlish(Request $request){

        $check=DB::table('package_wishlist')
            ->where('packageDescID',$request->id)
            ->where('user_id',Auth::user()->id)
            ->delete();
        return 'Deleted';
    }

    public function set_payment_option(Request $request){


        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();

        foreach ($Cart as $rows){
            $data=array(
                'booking_cart_id'=>$rows->booking_cart_id,
                'bank_code'=>'',
                'payment_status'=>'1',
                'payment_option'=>$request->option,
            );
            $check=DB::table('package_payments')->where('booking_cart_id',$rows->booking_cart_id)->first();
            if($check){
                DB::table('package_payments')->where('booking_cart_id',$rows->booking_cart_id)->update($data);
            }else{
                DB::table('package_payments')->insert($data);
            }

        }


    }

//    public function payment(){
//        $booking_cart_id=array();
//        $str=DB::table('package_booking_cart');
//        if(Auth::check()) {
//            $str->where('auth_id',Auth::user()->id);
//        }else{
//            $str->where('session_id', Session::getId());
//        }
//        $Cart=$str->get();
//        foreach ($Cart as $rows){
//            $booking_cart_id[]=$rows->booking_cart_id;
//            $step=$rows->booking_step;
//        }
//
//        $Carts=null;
//        if($Cart){
//            $Carts=DB::table('package_booking_cart_details')
//                ->whereIn('booking_cart_id',$booking_cart_id)
//                ->whereIn('package_detail_id',function ($query){
//                    $query->select('packageDescID')->from('package_details_sub')
//                        ->where('status','Y');
//                })
//                ->where('item_status','Y')
//                ->groupby('tour_type')
//                ->groupby('package_id')
//                ->get();
//        }
//
//        //dd($Carts);
//        $PackageCountry=DB::table('package_details as a')
//            ->join('countries as b','b.country_id','=','a.country_id')
//            ->select('b.*')
//            ->where('status','Y')
//            ->where('language_code',Session::get('language'))
//            ->groupby('Country_id')
//            ->get();
//
//        if($step<3){
//            DB::table('package_booking_cart')->whereIn('booking_cart_id',$booking_cart_id)->update(['booking_step'=>'3']);
//        }
//
//        if($step<=Session::get('step')){
//            Session::put('step','3');
//        }
//
//        $dataMail=array(
//            'email'=>Auth::user()->email,
//            'name'=>Auth::user()->name,
//            'message'=>'Payment Date :'.date('Y-m-d H:i:s'),
//            'booking_cart_id'=>$booking_cart_id,
//        );
//
//        //  dd($dataMail);
//
//        Mail::send('emails.reply-to-payment', $dataMail, function ($m) use ($dataMail) {
//            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
//            $m->to($dataMail['email'], $dataMail['name'])->subject('Your Payments!');
//        });
//
//        return view('packages.order.continuous-payment')
//            ->with('Carts',$Carts)
//            ->with('PackageCountry',$PackageCountry);
//
//
////        return view('packages.order.continuous-payment')
////        return view('packages.order.continuous-payment')
////            ->with('Carts',$Carts)
////            ->with('PackageCountry',$PackageCountry);
//    }


    function decode($string,$key) {
        $j=0;$hash='';
        $key = sha1($key);
        $strLen = strlen($string);
        $keyLen = strlen($key);
        for ($i = 0; $i < $strLen; $i+=2) {
            $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
            if ($j == $keyLen) { $j = 0; }
            $ordKey = ord(substr($key,$j,1));
            $j++;
            $hash .= chr($ordStr - $ordKey);
        }
        return $hash;
    }



    public function print_invoice1($id){
        $getId=$this->decode($id,'Invoice No.');

        $Invoice=explode('/',$getId);
        if(!Auth::check()){
            Auth::loginUsingId($Invoice[2]);
        }

        $Invoice=DB::table('package_invoice')
            ->where('invoice_id',$Invoice[0])
            ->where('invoice_package_detail_id',$Invoice[1])
            ->get();
            //dd($Invoice);
        ini_set('display_errors',true);
        $pdf = PDF::loadView('packages.order.pdf-invoice-deposit-email', compact('Invoice'));
        return $pdf->download('invoice-deposit-'.date('Y-m-d').'.pdf');
    }

    public function print_invoice2($id){
        $getId=$this->decode($id,'Invoice No.');

        $Invoice=explode('/',$getId);
        if(!Auth::check()){
            Auth::loginUsingId($Invoice[2]);
        }

        $Invoice=DB::table('package_invoice')
            ->where('invoice_booking_id',$Invoice[0])
            ->where('invoice_package_detail_id',$Invoice[1])
            ->get();

        ini_set('display_errors',true);
        $pdf = PDF::loadView('packages.order.pdf-invoice-deposit-email', compact('Invoice'));
        return $pdf->download('invoice-deposit-'.date('Y-m-d').'.pdf');
    }


    public function get_current($packageId,$type){
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$packageId)
            ->first();
        if($type=='symbol'){
            if($Package){
                $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();
                $currency_symbol=$current->currency_symbol;
            }else{
                $currency_symbol='฿';
            }
        }else{
            $currency_symbol=$Package->packageCurrency;
        }
        return $currency_symbol;

    }

    public function GetId($table,$colum){
        $GetId=DB::table($table)->orderby($colum,'desc')->first();
        if($GetId){
            $return=$GetId->$colum+1;
        }else{
            $return=1;
        }

        return $return;
    }

    public function check_invoice(){

        $Address=DB::table('address_book')->where('address_type','1')->where('timeline_id',Auth::user()->timeline_id)->first();
        if(!$Address){
            return redirect('booking/add/address');
        }

        $booking_cart_id=array();$booking_create='';
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();

        if($Cart->count()==0){
            return redirect('my/bookings');
        }

        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }

        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('package_detail_id',function ($query){
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->where('item_status','Y')
                ->get();
        }

       // dd($Carts);

        if($Cart->count()){
            $data=array(
                'booking_date'=>date('Y-m-d H:i:s'),
                'booking_by'=>Auth::user()->id,
                'booking_status'=>'1',
            );
            $booking_id=DB::table('package_bookings')->insertGetId($data);
            $booking_create='y';
            Session::put('booking_id',$booking_id);
        }

        $package_detail_title='';

        $package_details="";

        foreach ($Carts as $rows) {

            $Package=DB::table('package_tour_info')
                ->where('show_in_timeline',$rows->timeline_id)
                ->where('packageID',$rows->package_id)
                ->first();
            //dd($Package);

            $Detail=DB::table('package_booking_cart_details as a')
                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                ->where('a.package_detail_id',$rows->package_detail_id)
                ->where('a.timeline_id',$rows->timeline_id)
                ->first();




            $Timeline=\App\Timeline::where('id',$Detail->timeline_id)->first();
            $st=explode('-',$Detail->packageDateStart);
            $end=explode('-',$Detail->packageDateEnd);

            if($st[1]==$end[1]){
                $date=\Date::parse($Detail->packageDateStart);
                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                // dd($end[0]);
            }else{
                $date=\Date::parse($Detail->packageDateStart);
                $date1=\Date::parse($Detail->packageDateEnd);
                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
            }

            $package_detail_title='<strong>'.$Detail->TourType.':'. $package_date.'</strong><br>'.$Package->packageName.'<br>';
            $package_detail_title.= trans('common.sell_by').':'. $Timeline->username;


            $PackageDetailsOne=DB::table('package_details as a')
                ->join('package_tour as b','b.packageID','=','a.packageID')
                ->select('a.*','b.package_owner')
                ->where('packageDescID',$rows->package_detail_id)
                ->first();

            if($PackageDetailsOne->season=='Y'){
                $order_by="desc";
            }else{
                $order_by="asc";
            }
            $status=1;
            if($PackageDetailsOne->package_owner!='Yes'){
                $status='7';
            }

            $Condition=DB::table('condition_in_package_details as a')
                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                ->where('b.condition_group_id','1')
                ->where('b.formula_id','>',0)
                ->where('a.packageID',$rows->package_id)
                ->orderby('c.value_deposit',$order_by)
                ->first();

            $deposit_price=0;
            if($Condition){
                $deposit_price=$Condition->value_deposit;
            }




            //  dd($package_detail_title);
            $data = array(
                'booking_id' => $booking_id,
                'package_id' => $rows->package_id,
                'timeline_id' => $rows->timeline_id,
                'package_detail_id' => $rows->package_detail_id,
                'package_detail_title' => $package_detail_title,
                'tour_type' => $rows->tour_type,
                'TourType' => $Detail->TourType,
                'booking_normal_price' => $Detail->price_system_fees,
                'booking_realtime_price' => $rows->booking_cart_realtime_price,
                'deposit_price' => $deposit_price,
                'price_include_visa' => $Detail->price_include_visa,
                'price_for_visa' => $Detail->price_for_visa,
                'price_visa_details' => $Detail->price_visa_details,
                'number_of_need_visa' => $rows->number_of_need_visa,
                'price_include_vat' => $rows->price_include_vat,
                'number_of_need_visa' => $rows->number_of_need_visa,
                'number_of_person' => $rows->number_of_person,
                'booking_person_number' => $rows->booking_cart_person_number,
                'booking_detail_status' => $status,
            );

            $booking_detail=DB::table('package_booking_details')->insertGetId($data);


            /****** Get date invoice *******/

            $Deposit=0;$find_date_condition=array();

            if($Condition){
                $find_date_condition = DB::table('condition_deposit_operation')
                    ->where('condition_code', $Condition->condition_id)
                    ->where('operation_sub_id', '1')
                    ->first();
                $Deposit=round($Condition->value_deposit*$rows->number_of_person);
            }
            $number_of_day_deposit = 3;
            if ($find_date_condition) {
                $number_of_day_deposit = $find_date_condition->number_of_day;
            }

            $date_deposit = date("Y-m-d", strtotime("+" . $number_of_day_deposit . " day"));

                /********************* end get date ********************/
            $number_of_person=0;

            if($package_details!=$rows->package_detail_id) {
//                $number_of_person=DB::table('package_booking_cart_details')
//                    ->where('package_id',$rows->package_id)
//                    ->where('package_detail_id',$rows->package_detail_id)
//                    ->where('booking_cart_id',$rows->booking_cart_id)
//                    ->sum('number_of_person');

                if ($Condition) {
                    $data = array(
                        'invoice_booking_id' => $booking_id,
                        'invoice_package_id' => $rows->package_id,
                        'invoice_package_detail_id' => $rows->package_detail_id,
                        'invoice_timeline_id' => $rows->timeline_id,
                        'invoice_date' => date('Y-m-d H:i:s'),
                        'invoice_type' => '1',
                        'invoice_amount' => $Deposit,
                        'payment_option' => '',
                        'invoice_payment_date' => $date_deposit,
                        'invoice_status' => $status,
                        'currency_symbol' => self::get_current($rows->package_id, 'symbol'),
                        'currency_code' => self::get_current($rows->package_id, 'code'),
                    );
                    DB::table('package_invoice')->insert($data);
                }



                $Amount=0;$PriceVisa=0;$PriceVat=0;

                $Details=DB::table('package_booking_cart_details as a')
                    ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                    ->join('package_tour as c','c.packageID','=','a.package_id')
                    ->select('c.package_owner','a.*','d.*','b.*')
                    ->where('a.package_detail_id',$rows->package_detail_id)
                    ->where('booking_cart_id',$rows->booking_cart_id)
                    ->get();

                //dd($Details);

                foreach ($Details as $detail) {
                    $PAdditional=0;$discount=0;$pay_more=0;
                    $SubPrice=round($detail->number_of_person*$detail->price_system_fees);
                    if($detail->price_for_visa>0){
                        $PriceVisa=$detail->price_for_visa*$detail->number_of_need_visa;
                    }
                    $Additionals = DB::table('package_booking_cart_additional')
                        ->where('booking_cart_detail_id', $detail->booking_cart_detail_id)
                        ->get();

                    foreach ($Additionals as $rowA){
                        if($rowA->need_someone_share!='Y'){
                            $PAdditional+=$rowA->price_service;
                        }
                    }

                    if($detail->package_owner!='Yes'){
                        $booking_create='n';
                    }


//                    $Price=DB::table('package_details_sub')
//                        ->where('psub_id',$rows->tour_type)
//                        ->where('packageDescID',$rows->package_detail_id)
//                        ->first();

                    $Promotion=\App\PackagePromotion::where('packageDescID',$rows->package_detail_id)->active()
                        ->where('psub_id',$rows->tour_type)
                        ->orderby('promotion_date_start','asc')
                        ->first();


                    if($Promotion){
                        if($Promotion->promotion_operator=='Between'){
                            if($Promotion->promotion_unit=='%'){
                                $discount=$detail->price_system_fees*$Promotion->promotion_value/100;
                            }else{
                                $discount=$detail->price_system_fees-$Promotion->promotion_value;
                            }
                        }else{
                            if($Promotion->promotion_operator2=='Up'){
                                if($Promotion->promotion_unit=='%'){
                                    $pay_more=$detail->Price_by_promotion*$Promotion->promotion_value/100;
                                }else{
                                    $pay_more=$Promotion->promotion_value;
                                }
                            }else{
                                if($Promotion->promotion_unit=='%'){
                                    $discount=$detail->Price_by_promotion*$Promotion->promotion_value/100;
                                }else{
                                    $discount=$detail->Price_by_promotion-$Promotion->promotion_value;
                                }
                            }
                        }
                    }

                    if($pay_more>0){
                        $PromotionPrice=$pay_more*$detail->number_of_person;
                        $PriceTotal=$SubPrice+$PAdditional+$PriceVisa+$PromotionPrice;
                    }elseif($discount>0){
                        $PromotionPrice=$discount*$detail->number_of_person;
                        $PriceTotal=$SubPrice+$PAdditional+$PriceVisa-$PromotionPrice;
                    }else{
                        $PriceTotal=$SubPrice+$PAdditional+$PriceVisa;
                    }

                    if($detail->price_include_vat!='Y'){
                        $PriceVat=round($PriceTotal*7/100);
                    }

                    $Amount+=$PriceTotal+$PriceVat;
                }


                $Additional = DB::table('package_booking_cart_additional')
                    ->where('booking_cart_detail_id', $rows->booking_cart_detail_id)
                    ->get();

                foreach ($Additional as $rowA) {
                    $data = array(
                        'booking_detail_id' => $booking_detail,
                        'additional_service' => $rowA->additional_service,
                        'additional_id'=>$rowA->additional_id,
                        'price_service' => $rowA->price_service,
                        'need_someone_share' => $rowA->need_someone_share,
                    );
                    DB::table('package_booking_additional')->insert($data);
                    DB::table('package_booking_cart_additional')
                        ->where('booking_cart_detail_id', $rows->booking_cart_detail_id)
                        ->delete();

                }

                if ($Condition) {
                    $find_date_condition = DB::table('condition_deposit_operation')
                        ->where('condition_code', $Condition->condition_id)
                        ->where('operation_sub_id', '2')
                        ->first();
                }
                $number_of_day_tour = 30;
                $date_pay_all = date("Y-m-d", strtotime("+" . $number_of_day_tour . " day"));
                if ($find_date_condition) {
                    $number_of_day_tour = $find_date_condition->number_of_day;
                    $date_pay_all = date("Y-m-d", strtotime("+" . $number_of_day_tour . " day"));
                }else{
                    $dateStart=$Detail->packageDateStart;
                    $date_pay_all = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( $dateStart ) ) . "-1 month" ) );
                }

                $Totals = $Amount - $Deposit;

                $data = array(
                    'invoice_booking_id' => $booking_id,
                    'invoice_package_id' => $rows->package_id,
                    'invoice_package_detail_id' => $rows->package_detail_id,
                    'invoice_timeline_id' => $rows->timeline_id,
                    'invoice_date' => date('Y-m-d H:i:s'),
                    'invoice_type' => '2',
                    'invoice_amount' => $Totals,
                    'payment_option' => '',
                    'invoice_payment_date' => $date_pay_all,
                    'invoice_status' => $status,
                    'currency_symbol' => self::get_current($rows->package_id, 'symbol'),
                    'currency_code' => self::get_current($rows->package_id, 'code'),
                );
                DB::table('package_invoice')->insert($data);
            }

            DB::table('package_booking_cart_details')->where('booking_cart_detail_id', $rows->booking_cart_detail_id)->delete(); // delete in cart

            $Promotion = \App\PackagePromotion::where('packageDescID', $rows->package_detail_id)->active()
                ->where('psub_id', $rows->tour_type)
                ->first();

            if ($Promotion) {
                $data = array(
                    'booking_detail_id' => $booking_detail,
                    'promotion_title' => $Promotion->promotion_title,
                    'promotion_date' => date('Y-m-d'),
                    'promotion_operator' => $Promotion->promotion_operator,
                    'promotion_value' => $Promotion->promotion_value,
                    'promotion_unit' => $Promotion->promotion_unit,
                );
                DB::table('package_booking_promotion')->insert($data);
            }

            $check = DB::table('package_booking_cart as a')
                ->join('package_booking_cart_details as b', 'b.booking_cart_id', '=', 'a.booking_cart_id')
                ->where('a.auth_id', Auth::user()->id)->get();

            if ($check->count() == 0) {
                DB::table('package_booking_cart')->where('auth_id', Auth::user()->id)->delete();
            }
            $package_details=$rows->package_detail_id;
        }
        //  dd($Carts);

        /*************** For show invoice ***************/

        Session::put('step','3');
        Session::put('step_click','3');

        $Booking=DB::table('package_bookings')
            ->where('booking_by',Auth::user()->id)
            ->where('booking_status','1')
            ->orderby('booking_id','desc')
            ->first();


        $dataMail=array(
            'email'=>Auth::user()->email,
            'name'=>Auth::user()->name,
            'subject'=>trans('email.thank_you_for_your_booking_with_us').$Booking->booking_id,
            'message'=>trans('email.confirm_booking_date').date('Y-m-d H:i:s'),
            'booking_id'=>$Booking->booking_id,
        );

        if($booking_create=='y'){
            Mail::send('emails.respond-booking-confirmation', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }else{
            Mail::send('emails.respond-booking-check-confirmation', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }


//        Mail::send('emails.confirm-order', $dataMail, function ($m) use ($dataMail) {
//            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
//            $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
//        });



        $Invoices=DB::table('package_invoice as a')
            ->where('a.invoice_booking_id',$Booking->booking_id)
            ->get();

        return view('packages.order.show-invoice-all')
            ->with('Invoices',$Invoices);

//        return view('packages.order.continuous-show-invoice')
//            ->with('PackageInOrder',$PackageInOrder)
//            ->with('Booking',$Booking)
//            ->with('Invoice',$Invoice);
    }


    public function show_all_invoice($id){
        $Invoice=DB::table('package_invoice')
            ->where('invoice_booking_id',$id)
            ->where('invoice_type','1')
            ->get();


        $PackageInOrder=DB::table('package_invoice as a')
            ->join('package_tour as b','b.packageID','=','a.invoice_package_id')
            ->select('a.*','b.packageID','b.package_partner','b.package_owner','b.owner_timeline_id','b.timeline_id')
            ->where('a.invoice_booking_id',$id)
            ->groupby('a.invoice_package_id')
            ->get();

        $Booking=DB::table('package_bookings')
            ->where('booking_by',Auth::user()->id)
            ->where('booking_status','1')
            ->orderby('booking_id','desc')
            ->first();

        return view('packages.order.continuous-show-invoice')
            ->with('PackageInOrder',$PackageInOrder)
            ->with('Booking',$Booking)
            ->with('Invoice',$Invoice);

    }


    public function continuous(){ //step 2
        Session::put('step_click','2');
        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();


        if(!$Cart->count()){
           return redirect('my/bookings');
        }

        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }
        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('package_detail_id',function ($query){
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->where('item_status','Y')
                ->groupby('package_id')
                ->get();
        }

        if($step<2){
            DB::table('package_booking_cart')->whereIn('booking_cart_id',$booking_cart_id)->update(['booking_step'=>'2']);
            Session::put('step','2');
        }

        if($Carts->count()==0){
            return redirect('home/package/all');
        }

        $AddressBook=DB::table('address_book')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->where('default_address','1')
            ->where('address_type','1')
            ->first();


        return view('packages.order.continuous-order')
            ->with('AddressBook',$AddressBook)
            ->with('Carts',$Carts);
    }



    public function cart_tour($id){ //step 1
        //echo Session::get('sessionID');
        Session::put('step_click','1');
        $step=0;
        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();

        if(!$Cart->count()){
            return redirect('/home/package/all');
        }

        $booking_cart_id=array();
        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }

        //****************** Update Status if promotion finish **********************//
        DB::table('package_promotion')
            ->where('promotion_date_end','<',date('Y-m-d H:i:s'))
            ->where('promotion_status','Y')
            ->where('promotion_operator','Between')
            ->update(['promotion_status'=>'N']);
        //****************** Update Status if promotion finish **********************//

        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details as a')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('a.package_detail_id',function ($query) {
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->groupby('package_id')
                ->get();
        }
        //dd($Carts);

        if($step<1){
            DB::table('package_booking_cart')->whereIn('booking_cart_id',$booking_cart_id)->update(['booking_step'=>'1']);
            Session::put('step','1');
        }
        $AddressBook=array();
        if(Auth::check()){
            $AddressBook=DB::table('address_book')->where('timeline_id',Auth::user()->timeline_id)->where('address_type','1')->first();
        }

        return view('packages.order.create-order')
                ->with('AddressBook',$AddressBook)
                ->with('Carts',$Carts);

    }

    public function tax_address(Request $request){
        return view('packages.order.form_tax_address');
    }

    public function invoice_amount(Request $request){

       if($request->invoice_no=='all'){
            if($request->type=='all'){

                if($request->invoice_no=='all') {
                    $Invoice = DB::table('package_invoice as a')
                        ->join('package_details_sub as b', 'b.packageDescID', '=', 'a.invoice_package_detail_id')
                        ->where('a.invoice_booking_id', $request->booking_id)
                        ->where('a.invoice_status', '1')
                        ->groupby('a.invoice_id')
                        ->get();
                }else{
                    $Invoice=DB::table('package_invoice as a')
                        ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
                        ->where('a.invoice_booking_id',$request->booking_id)
                        ->whereIn('a.invoice_id',$request->invoice_no)
                        ->where('a.invoice_status', '1')
                        ->groupby('a.invoice_id')
                        ->get();

               }

            }else{
                    $Invoice=DB::table('package_invoice as a')
                        ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
                        ->where('a.invoice_booking_id',$request->booking_id)
                        ->where('a.invoice_type',$request->type)
                        ->where('a.invoice_status', '1')
                        ->groupby('a.invoice_id')
                        ->get();

            }
       }else{
           $Invoice=DB::table('package_invoice as a')
               ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
               ->whereIn('a.invoice_id',$request->invoice_no)
               ->where('a.invoice_status', '1')
               ->groupby('a.invoice_id')
               ->get();
       }

        $Totals=0;

        foreach($Invoice as $rows){
//            $tax=0;
//            if($rows->invoice_type==2){
//                if(!$rows->price_include_vat=='Y'){
//                    $tax=$rows->price_system_fees*7/100;
//                }
//            }
            $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$rows->invoice_id)->first();
            if(!$check){
                $Totals+=$rows->invoice_amount;
            }


        }

        return $Totals;
    }

    public function show_form_notification($id,$type){


        Session::put('CurrentUrl',url()->current());
        if($type=='all'){
            $Invoices=DB::table('package_invoice as a')
                ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
                ->where('a.invoice_booking_id',$id)
                ->where('a.invoice_status','1')
                ->groupby('a.invoice_id')
                ->get();
            $check=DB::table('payment_notification')->where('invoice_booking_id',$id)->first();
        }else{
            $Invoices=DB::table('package_invoice as a')
                ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
                ->where('a.invoice_booking_id',$id)
                ->where('a.invoice_type',$type)
                ->where('a.invoice_status','1')
                ->groupby('a.invoice_id')
                ->get();
           // dd($Invoices);
            $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$id)->first();
        }

        $Banks=DB::table('business_verified_bank')
            ->where('timeline_id','37850')
            ->get();

        $AddressBook=DB::table('address_book')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->where('address_type','2')
            ->where('address_show','!=','')
            ->where('default_address','1')
            ->first();

        if(!$AddressBook) {
            $AddressBook = DB::table('address_book')
                ->where('timeline_id', Auth::user()->timeline_id)
                ->where('address_type', '1')
                ->where('default_address', '1')
                ->first();
        }

      //  dd($Invoice);
        return view('packages.order.form_notification')
            ->with('type',$type)
            ->with('check',$check)
            ->with('Banks',$Banks)
            ->with('AddressBook',$AddressBook)
            ->with('Invoices',$Invoices);
    }

    public function show_form_notification_by_id($id){

        $Invoice=DB::table('package_invoice as a')
            ->join('package_details_sub as b','b.packageDescID','=','a.invoice_package_detail_id')
            ->where('a.invoice_id',$id)
            ->where('a.invoice_status','1')
            ->groupby('a.invoice_id')
            ->first();

        $Banks=DB::table('business_verified_bank')
            ->where('timeline_id','37850')
            ->get();

        $AddressBook=DB::table('address_book')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->where('address_type','2')
            ->where('address_show','!=','')
            ->where('default_address','1')
            ->first();

        if(!$AddressBook){
            $AddressBook=DB::table('address_book')
                ->where('timeline_id',Auth::user()->timeline_id)
                ->where('address_type','1')
                ->where('default_address','1')
                ->first();
        }

        $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$id)->first();

            return view('packages.order.form_notification_by_id')
                ->with('check',$check)
                ->with('Banks',$Banks)
                ->with('AddressBook',$AddressBook)
                ->with('Invoice',$Invoice);
    }

    public function cancel_bookings($id){
        $Booking=DB::table('package_bookings as a')
            ->where('a.booking_id',$id)
            ->first();

        $Booking_details=DB::table('package_booking_details as a')
            ->where('a.booking_id',$id)
            ->get();

        $check_package=DB::table('package_booking_details as a')
             ->where('a.booking_id',$id)
             ->groupby('package_id')
             ->get();
      //  dd($check_package);
        return view('packages.order.cancel')
            ->with('Booking',$Booking)
            ->with('check_package',$check_package)
            ->with('Booking_details',$Booking_details);
    }

    public function cancel_option(Request $request){

        $Booking=DB::table('package_bookings')
            ->where('booking_id',$request->booking_id)
            ->first();
        $Booking_details=DB::table('package_booking_details as a')
            ->where('a.booking_id',$request->booking_id)
            ->get();

        if($request->option=='all') {
            return view('packages.booking.cancel-all')->with('Booking',$Booking);
        }else if($request->option=='package'){
            return view('packages.booking.cancel-package')
                ->with('Booking_details',$Booking_details)
                ->with('Booking',$Booking);
        }else{
            return view('packages.booking.cancel-person')
                ->with('Booking_details',$Booking_details)
                ->with('Booking',$Booking);
        }
    }

    public function cancel_package(Request $request){
        $package="";

        $Invoices=DB::table('package_invoice')
            ->where('invoice_booking_id',$request->booking_id)
            ->where('invoice_package_detail_id',$request->package_detail_id)
            ->where('invoice_type','2')
            ->where('invoice_status','4')
            ->get();
        if(!$Invoices->count()){
            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$request->booking_id)
                ->where('invoice_package_detail_id',$request->package_detail_id)
                ->where('invoice_type','1')
                ->where('invoice_status','2')
                ->get();

        }

        if($Invoices->count()>0){

            foreach ($Invoices as $rows){
                $message_cancel='';
                $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
                $dateStart=$Detail->packageDateStart;
                $dateCancel=date('Y-m-d h:i:s');

//                $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
//                $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';

                $dateStart=\Date::parse($dateStart);
                $dateCancel=\Date::parse($dateCancel);

                $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
                $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';

                $days=$this->dateDiv($dateStart,$dateCancel);
                // dd($days);
                $package=$rows->invoice_package_id;

                $condition=DB::table('package_condition as a')
                    ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                    ->whereIn('a.condition_code',function ($query) use($package){
                        $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                    })
                    ->where('a.condition_group_id','2')
                    ->where('a.timeline_id',$rows->invoice_timeline_id)
                    ->where('a.formula_id','!=','0')
                    ->where('language_code',Session::get('language'))
                    ->get();

               // dd($condition);
                $remark='';$message_cancel2='';
                foreach ($condition as $rowC){
                    if($rowC->operator_code=='Between'){
                        if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                            $remark=$rowC->condition_title;
                        }

                    }else if($rowC->operator_code=='<'){
                        if($days<$rowC->condition_left){
                           $remark=$rowC->condition_title;
                        }

                    }else{

                        if($days>$rowC->condition_left){
                           $remark=$rowC->condition_title;
                        }

                    }
                    $remark?$message_cancel2.=$remark.'<br>':'';
                    // dd($value_tour.'='.$remark);
                }

            }
        }

        if($message_cancel2==''){
            $message_cancel.= trans('common.no_condition_cancel').'<hr>';
        }else{
            $message_cancel.=$message_cancel2;
        }

        return $message_cancel;
    }

    public function cancel_person(Request $request){
        $package="";
        $Invoices=DB::table('package_invoice')
            ->where('invoice_booking_id',$request->booking_id)
            ->where('invoice_package_detail_id',$request->package_detail_id)
            ->where('invoice_type','2')
            ->where('invoice_status','4')
            ->get();
        if(!$Invoices->count()){
            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$request->booking_id)
                ->where('invoice_package_detail_id',$request->package_detail_id)
                ->where('invoice_type','1')
                ->where('invoice_status','2')
                ->get();
        }

        if($Invoices->count()>0){

            foreach ($Invoices as $rows){
                $message_cancel='';
                $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
                $dateStart=$Detail->packageDateStart;
                $dateCancel=date('Y-m-d h:i:s');

//                $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
//                $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';

                $dateStart=\Date::parse($dateStart);
                $dateCancel=\Date::parse($dateCancel);

                $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
                $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';

                $days=$this->dateDiv($dateStart,$dateCancel);
                // dd($days);
                $package=$rows->invoice_package_id;

                $condition=DB::table('package_condition as a')
                    ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                    ->whereIn('a.condition_code',function ($query) use($package){
                        $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                    })
                    ->where('a.condition_group_id','2')
                    ->where('a.timeline_id',$rows->invoice_timeline_id)
                    ->where('a.formula_id','!=','0')
                    ->where('language_code',Session::get('language'))
                    ->get();

               // dd($condition);
                $remark=''; $message_cancel2='';
                foreach ($condition as $rowC){
                    if($rowC->operator_code=='Between'){
                        if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                            $remark=$rowC->condition_title;
                        }
                    }else if($rowC->operator_code=='<'){
                        if($days<$rowC->condition_left){
                           $remark=$rowC->condition_title;
                        }
                    }else{
                        if($days>$rowC->condition_left){
                           $remark=$rowC->condition_title;
                        }
                    }

                    $remark?$message_cancel2.=$remark.'<br>':'';
                    // dd($value_tour.'='.$remark);
                }

            }
        }

        if($message_cancel2==''){
            $message_cancel.= trans('common.no_condition_cancel').'<hr>';
        }else{
            $message_cancel.=$message_cancel2;
        }

        return $message_cancel;
    }

    public function confirm_cancel_package(Request $request){
        $post=$request->all();
        foreach ($post['package'] as $key=>$value){
            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$request->booking_id)
                ->where('invoice_package_id',$value)
                ->where('invoice_type','2')
                ->where('invoice_status','4')
                ->get();
            if(!$Invoices->count()){
                $Invoices=DB::table('package_invoice')
                    ->where('invoice_booking_id',$request->booking_id)
                    ->where('invoice_package_id',$value)
                    ->where('invoice_type','1')
                    ->where('invoice_status','2')
                    ->get();

            }

            foreach ($Invoices as $rows){
                $message_cancel='';
                $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
                $package=$rows->invoice_package_id;

                $dateStart=$Detail->packageDateStart;
                $dateCancel=date('Y-m-d h:i:s');
                $days=$this->dateDiv($dateStart,$dateCancel);
                $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
                $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';
                $condition=DB::table('package_condition as a')
                    ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                    ->whereIn('a.condition_code',function ($query) use($package){
                        $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                    })
                    ->where('a.condition_group_id','2')
                    ->where('a.timeline_id',$rows->invoice_timeline_id)
                    ->where('a.formula_id','!=','0')
                    ->where('language_code',Session::get('language'))
                    ->get();

                $value_tour=0;$remark='';$message_cancel2='';
                foreach ($condition as $rowC){
                    if($rowC->operator_code=='Between'){
                        if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                            $remark=$rowC->condition_title;
                        }

                    }else if($rowC->operator_code=='<'){
                        if($days<$rowC->condition_left){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                            $remark=$rowC->condition_title;
                        }

                    }else{

                        if($days>$rowC->condition_left){
                            if($days<$rowC->condition_left){
                                if($rowC->keep_all_costs=='Y'){
                                    $value_tour=$rows->invoice_amount;
                                }else if($rowC->keep_value_tour=='Y'){
                                    if($rowC->unit_deposit=='%'){
                                        $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                    }else{
                                        $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                    }
                                }elseif($rowC->return_all_costs=='Y'){
                                    $value_tour=0;
                                }
                            }
                            $remark=$rowC->condition_title;
                        }

                    }
                    $remark?$message_cancel2.=$remark:'';
                } //****************  end condition loop ****************


                if($message_cancel2==''){
                    $message_cancel.= trans('common.no_condition_cancel').'<hr>';
                }else{
                    $message_cancel.=$message_cancel2;
                }

                $data = array(
                    'payment_refund' => $rows->invoice_amount - $value_tour,
                    'payment_remark' => $remark
                );

                DB::table('package_payments')->where('invoice_id', $rows->invoice_id)->update($data);
                DB::table('package_invoice')->where('invoice_id', $rows->invoice_id)->update(['invoice_status' => '6']);

                if($rows->invoice_status>1){
                    $Details = DB::table('package_booking_details')
                        ->where('booking_id', $rows->invoice_booking_id)
                        ->where('package_id', $rows->invoice_package_id)
                        ->where('package_detail_id', $rows->invoice_package_detail_id)
                        ->where('booking_detail_status','!=','6')
                        ->get();
                    // dd($Details);
                    foreach ($Details as $rows) {
                        DB::table('package_details')->where('packageDescID', $rows->package_detail_id)->increment('NumberOfPeople', $rows->number_of_person);
                        DB::table('package_booking_details')
                            ->where('package_detail_id', $rows->package_detail_id)
                            ->where('booking_id', $rows->invoice_booking_id)
                            ->decrement('number_of_person', $rows->number_of_person);
                        DB::table('package_booking_details')->where('package_detail_id', $rows->package_detail_id)->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.canceled_by_myself'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }

                $User=DB::table('package_bookings as a')
                    ->join('users as b','b.id','=','a.booking_by')
                    ->where('a.booking_id',$rows->invoice_booking_id)
                    ->first();

                $Package=DB::table('package_booking_details')
                    ->where('booking_id',$rows->invoice_booking_id)
                    ->where('package_detail_id',$rows->invoice_package_detail_id)
                    ->first();

                $dataMail=array(
                    'email'=>$User->email,
                    'name'=>$User->name,
                    'subject'=>trans('email.cancellation'),
                    'message_details'=>trans('common.this_email_is_to_inform_you_that_you_have_canceled_the_tour_package').'<br>'.$message_cancel,
                    'package_title'=>$Package->package_detail_title,
                    'booking_id'=>$rows->invoice_booking_id,
                    'invoice_id'=>$rows->invoice_id,
                );
                Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
                    $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                    $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
                });
            }



        }//********************* end loop package *****************
        return trans('common.cancel_successful');
    }

    public function confirm_cancel_person(Request $request){
        $post=$request->all();
        foreach ($post['package'] as $key=>$value){

            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$request->booking_id)
                ->where('invoice_package_id',$value)
                ->where('invoice_type','2')
                ->where('invoice_status','4')
                ->get();
            if(!$Invoices->count()){
                $Invoices=DB::table('package_invoice')
                    ->where('invoice_booking_id',$request->booking_id)
                    ->where('invoice_package_id',$value)
                    ->where('invoice_type','1')
                    ->where('invoice_status','2')
                    ->get();
            }

            foreach ($Invoices as $rows){
                $message_cancel='';
                $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
                $package=$rows->invoice_package_id;

                $dateStart=$Detail->packageDateStart;
                $dateCancel=date('Y-m-d h:i:s');
                $days=$this->dateDiv($dateStart,$dateCancel);
//                $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
//                $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';

                $dateStart=\Date::parse($dateStart);
                $dateCancel=\Date::parse($dateCancel);

                $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
                $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';


                $condition=DB::table('package_condition as a')
                    ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                    ->whereIn('a.condition_code',function ($query) use($package){
                        $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                    })
                    ->where('a.condition_group_id','2')
                    ->where('a.timeline_id',$rows->invoice_timeline_id)
                    ->where('a.formula_id','!=','0')
                    ->where('language_code',Session::get('language'))
                    ->get();

                $value_tour=0;$remark='';$message_cancel2='';
                foreach ($condition as $rowC){
                    if($rowC->operator_code=='Between'){
                        if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                            $remark=$rowC->condition_title;
                        }

                    }else if($rowC->operator_code=='<'){
                        if($days<$rowC->condition_left){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                            $remark=$rowC->condition_title;
                        }

                    }else{

                        if($days>$rowC->condition_left){
                            if($days<$rowC->condition_left){
                                if($rowC->keep_all_costs=='Y'){
                                    $value_tour=$rows->invoice_amount;
                                }else if($rowC->keep_value_tour=='Y'){
                                    if($rowC->unit_deposit=='%'){
                                        $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                    }else{
                                        $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                    }
                                }elseif($rowC->return_all_costs=='Y'){
                                    $value_tour=0;
                                }
                            }
                            $remark=$rowC->condition_title;
                        }

                    }
                    $remark?$message_cancel2.=$remark:'';
                } //****************  end condition loop ****************


                if($message_cancel2==''){
                    $message_cancel.= trans('common.no_condition_cancel').'<hr>';
                }else{
                    $message_cancel.=$message_cancel2;
                }

                $data = array(
                    'payment_refund' => $rows->invoice_amount - $value_tour,
                    'payment_remark' => $remark
                );

                DB::table('package_payments')->where('invoice_id', $rows->invoice_id)->update($data);
                DB::table('package_invoice')->where('invoice_id', $rows->invoice_id)->update(['invoice_status' => '6']);

                if($rows->invoice_status>1){
                    $Details = DB::table('package_booking_details')
                        ->where('booking_id', $rows->invoice_booking_id)
                        ->where('package_id', $rows->invoice_package_id)
                        ->where('package_detail_id', $rows->invoice_package_detail_id)
                        ->where('booking_detail_status','!=','6')
                        ->get();
                    // dd($Details);
                    foreach ($Details as $rows) {
                        DB::table('package_details')->where('packageDescID', $rows->package_detail_id)->increment('NumberOfPeople', $post['person'][$key]);
                        DB::table('package_booking_details')
                            ->where('package_detail_id', $rows->package_detail_id)
                            ->where('booking_id', $rows->invoice_booking_id)
                            ->decrement('number_of_person', $post['person'][$key]);
                        DB::table('package_booking_details')->where('package_detail_id', $rows->package_detail_id)->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.canceled_by_myself'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }

                $User=DB::table('package_bookings as a')
                    ->join('users as b','b.id','=','a.booking_by')
                    ->where('a.booking_id',$rows->invoice_booking_id)
                    ->first();

                $Package=DB::table('package_booking_details')
                    ->where('booking_id',$rows->invoice_booking_id)
                    ->where('package_detail_id',$rows->invoice_package_detail_id)
                    ->first();

                $dataMail=array(
                    'email'=>$User->email,
                    'name'=>$User->name,
                    'subject'=>trans('email.cancellation'),
                    'message_details'=>trans('common.this_email_is_to_inform_you_that_you_have_canceled_the_tour_package').'<br>'.$message_cancel,
                    'package_title'=>$Package->package_detail_title,
                    'booking_id'=>$rows->invoice_booking_id,
                    'invoice_id'=>$rows->invoice_id,
                );
                Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
                    $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                    $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
                });
            }

        }//********************* end loop package *****************
        return trans('common.cancel_successful');
    }

    public function dateDiv($t1,$t2){

        $secondsDifference=strtotime($t1)-strtotime($t2);
        return $secondsDifference/86400;
    }

    public function cancel_confirm($id,$option){
        if($option=='all') {
            $Invoices = DB::table('package_invoice')
                ->where('invoice_booking_id', $id)
                ->get();

            foreach ($Invoices as $rows) {
                $message_cancel = '';
                $Detail = DB::table('package_details')->where('packageDescID', $rows->invoice_package_detail_id)->first();
                $dateStart = $Detail->packageDateStart;
                $dateCancel = date('Y-m-d h:i:s');
                $dateStart=\Date::parse($dateStart);
                $dateCancel=\Date::parse($dateCancel);

                $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
                $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';
                $days = $this->dateDiv($dateStart, $dateCancel);
                // dd($days);
                $package = $rows->invoice_package_id;

                $condition = DB::table('package_condition as a')
                    ->join('mathematical_formula as b', 'b.formula_id', '=', 'a.formula_id')
                    ->whereIn('a.condition_code', function ($query) use ($package) {
                        $query->select('condition_id')->from('condition_in_package_details')->where('packageID', $package);
                    })
                    ->where('a.condition_group_id', '2')
                    ->where('a.timeline_id', $rows->invoice_timeline_id)
                    ->where('a.formula_id', '!=', '0')
                    ->where('language_code', Session::get('language'))
                    ->get();

                // dd($condition);
                $value_tour = 0;
                $remark = '';
                $message_cancel2='';

                foreach ($condition as $rowC) {
                    if ($rowC->operator_code == 'Between') {
                        if ($days >= $rowC->condition_left && $days <= $rowC->condition_right) {
                            if ($rowC->keep_all_costs == 'Y') {
                                $value_tour = $rows->invoice_amount;
                            } else if ($rowC->keep_value_tour == 'Y') {
                                if ($rowC->unit_deposit == '%') {
                                    $value_tour = $rows->invoice_amount * $rowC->value_tour / 100;
                                } else {
                                    $value_tour = $rows->invoice_amount - $rowC->value_tour;
                                }
                            } elseif ($rowC->return_all_costs == 'Y') {
                                $value_tour = 0;
                            }
                            $remark = $rowC->condition_title;
                        }

                    } else if ($rowC->operator_code == '<') {
                        if ($days < $rowC->condition_left) {
                            if ($rowC->keep_all_costs == 'Y') {
                                $value_tour = $rows->invoice_amount;
                            } else if ($rowC->keep_value_tour == 'Y') {
                                if ($rowC->unit_deposit == '%') {
                                    $value_tour = $rows->invoice_amount * $rowC->value_tour / 100;
                                } else {
                                    $value_tour = $rows->invoice_amount - $rowC->value_tour;
                                }
                            } elseif ($rowC->return_all_costs == 'Y') {
                                $value_tour = 0;
                            }
                            $remark = $rowC->condition_title;
                        }

                    } else {

                        if ($days > $rowC->condition_left) {
                            if ($days < $rowC->condition_left) {
                                if ($rowC->keep_all_costs == 'Y') {
                                    $value_tour = $rows->invoice_amount;
                                } else if ($rowC->keep_value_tour == 'Y') {
                                    if ($rowC->unit_deposit == '%') {
                                        $value_tour = $rows->invoice_amount * $rowC->value_tour / 100;
                                    } else {
                                        $value_tour = $rows->invoice_amount - $rowC->value_tour;
                                    }
                                } elseif ($rowC->return_all_costs == 'Y') {
                                    $value_tour = 0;
                                }
                            }
                            $remark = $rowC->condition_title;
                        }

                    }
                    $remark?$message_cancel2.=$remark:'';
                }

                if($message_cancel2==''){
                    $message_cancel.= trans('common.no_condition_cancel').'<hr>';
                }else{
                    $message_cancel.=$message_cancel2;
                }

                $data = array(
                    'payment_refund' => $rows->invoice_amount - $value_tour,
                    'payment_remark' => $remark
                );

                DB::table('package_payments')->where('invoice_id', $rows->invoice_id)->update($data);
                DB::table('package_invoice')->where('invoice_id', $rows->invoice_id)->update(['invoice_status' => '6']);

                if($rows->invoice_status>1){
                    $Details = DB::table('package_booking_details')
                    ->where('booking_id', $rows->invoice_booking_id)
                    ->where('package_id', $rows->invoice_package_id)
                    ->where('package_detail_id', $rows->invoice_package_detail_id)
                    ->where('booking_detail_status','!=','6')
                    ->get();
                   // dd($Details);
                    foreach ($Details as $rowD) {
                        DB::table('package_details')->where('packageDescID', $rowD->package_detail_id)->increment('NumberOfPeople', $rowD->number_of_person);
                        DB::table('package_booking_details')
                            ->where('package_detail_id', $rowD->package_detail_id)
                            ->where('booking_id', $rowD->booking_id)
                            ->decrement('number_of_person', $rowD->number_of_person);
                        DB::table('package_booking_details')->where('package_detail_id', $rowD->package_detail_id)->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.canceled_by_myself')]);
                    }
                }

                $User=DB::table('package_bookings as a')
                    ->join('users as b','b.id','=','a.booking_by')
                    ->where('a.booking_id',$rows->invoice_booking_id)
                    ->first();

                $Package=DB::table('package_booking_details')
                    ->where('booking_id',$rows->invoice_booking_id)
                    ->where('package_detail_id',$rows->invoice_package_detail_id)
                    ->first();

                $dataMail=array(
                    'email'=>$User->email,
                    'name'=>$User->name,
                    'subject'=>trans('email.cancellation'),
                    'message_details'=>trans('common.this_email_is_to_inform_you_that_you_have_canceled_the_tour_package').'<br>'.$message_cancel,
                    'package_title'=>$Package->package_detail_title,
                    'booking_id'=>$rows->invoice_booking_id,
                    'invoice_id'=>$rows->invoice_id,
                );



                Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
                    $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                    $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
                });

            } //*************** end loop condition ******************
            Session::flash('message',trans('common.canceled'));
            return redirect()->back();
        }
    }

    public function my_bookings(){
        if(Auth::check()) {
            $Bookings = DB::table('package_bookings as a')
                ->where('a.booking_by', Auth::user()->id)
                ->orderby('a.booking_date', 'desc')
                ->get();

            $BookingStatus=DB::table('booking_status')->get();

            return view('packages.order.my-bookings')
                ->with('Bookings', $Bookings)
                ->with('BookingStatus', $BookingStatus);
        }else{
            return redirect('home/package/all');
        }
    }

    public function search_my_bookings(Request $request){
            $status=$request->search;
            $Bookings = DB::table('package_bookings as a')
                ->where('a.booking_by', Auth::user()->id)
                ->whereIn('a.booking_id',function ($query) use($status){
                    $query->select('booking_id')->from('package_booking_details')->where('booking_detail_status',$status);
                })
                ->orderby('a.booking_date', 'desc')
                ->get();

            return view('packages.order.search-my-bookings')
                ->with('status', $status)
                ->with('Bookings', $Bookings);

    }




    public function view_invoice_by_id($id){

        Session::put('step_click','4');
        Session::put('invoice_id',$id);

        $Invoice=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->where('a.invoice_id',$id)
            ->first();

//        $PackageCountry=DB::table('package_details as a')
//            ->join('countries as b','b.country_id','=','a.country_id')
//            ->select('b.*')
//            ->where('status','Y')
//            ->where('language_code',Session::get('language'))
//            ->groupby('Country_id')
//            ->get();

            return view('packages.order.view_invoice')
//                ->with('PackageCountry',$PackageCountry)
                ->with('Invoice',$Invoice);



    }

    public function view_invoice_by($id){
        Session::put('step_click','3');
        Session::put('invoice_id',$id);

        if(!Auth::check()){
            Session::put('url_back',url()->current());
            return redirect('/register/2');
        }

        $Invoice=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->where('a.invoice_id',$id)
            ->first();
        //dd($Invoice);
//        $PackageCountry=DB::table('package_details as a')
//            ->join('countries as b','b.country_id','=','a.country_id')
//            ->select('b.*')
//            ->where('status','Y')
//            ->where('language_code',Session::get('language'))
//            ->groupby('Country_id')
//            ->get();

        return view('packages.order.view_invoice')
//                ->with('PackageCountry',$PackageCountry)
                ->with('Invoice',$Invoice);



    }

    public function view_invoice($id,$type){
        Session::put('step_click','4');
        Session::put('invoice_id',$id);
        Session::put('invoice_type',$type);
        if($type=='all'){
            $Invoices=DB::table('package_invoice as a')
                ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
                ->where('a.invoice_booking_id',$id)
                ->get();
            //dd($Invoices);
            return view('packages.order.view-invoice-tour-all')
                ->with('Invoices',$Invoices);
        }else{
            $Invoice=DB::table('package_invoice as a')
                ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
                ->where('a.invoice_id',$id)
                ->where('a.invoice_type',$type)
                ->first();
            return view('packages.order.view-invoice-deposit')
                ->with('invoice_type',$type)
                ->with('Invoice',$Invoice);
        }

    }

    public function view_invoice_all($id,$detail_id,$type){
        Session::put('step_click','4');
        Session::put('invoice_id',$id);
        Session::put('invoice_type',$type);
        if($type=='all'){
            $Invoice=DB::table('package_invoice as a')
                ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
                ->where('a.invoice_booking_id',$id)
                ->where('a.invoice_package_detail_id',$detail_id)
                ->first();
            //dd($Invoice);
            return view('packages.order.view-invoice-tour-all')
                ->with('Invoice',$Invoice);
        }
    }

    public function show_invoice($id,$type){
        Session::put('step_click','3');
        Session::put('invoice_booking_id',$id);
        Session::put('invoice_type',$type);

        if(!Auth::check()){
            Session::put('url_back',url()->current());
            Session::put('from_email','yes');
            return redirect('register/2');
        }

        if($type=='all'){
            $Invoices=DB::table('package_invoice as a')
                ->where('a.invoice_booking_id',$id)
//                ->orderby('a.invoice_type','asc')
                ->get();

            return view('packages.order.show-invoice-all')
                ->with('invoice_type',$type)
                ->with('booking_id',$id)
                ->with('Invoices',$Invoices);

        }else{
            $Booking=DB::table('package_invoice as a')
                ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
                ->where('a.invoice_booking_id',$id)
                ->first();
            $Invoice=DB::table('package_invoice as a')
                ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
                ->where('a.invoice_booking_id',$id)
                ->where('a.invoice_type',$type)
                ->first();
           // dd($Invoice);
            return view('packages.order.show-invoice')
                ->with('invoice_type',$type)
                ->with('Booking',$Booking)
                ->with('Invoice',$Invoice);
        }


    }


    public function booking_confirm(){
        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();
        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }
        $Carts=null;

        if($Cart){
            $data=array(
                'booking_date'=>date('Y-m-d'),
                'booking_by'=>Auth::user()->id,
                'update_by'=>Auth::user()->id,
                'booking_status'=>'1',
            );
            $booking_id=DB::table('package_bookings')->insertGetId($data);
            $Carts=DB::table('package_booking_cart_details')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->groupby('package_id')
                ->get();
            //dd($Carts);
            foreach ($Carts as $rows){
                $data=array(
                    'booking_id'=>$booking_id,
                    'package_id'=>$rows->package_id,
                    'package_detail_id'=>$rows->package_detail_id,
                    'tour_type'=>$rows->tour_type,
                    'booking_normal_price'=>$rows->booking_cart_normal_price,
                    'booking_realtime_price'=>$rows->booking_cart_realtime_price,
                    'price_include_vat'=>$rows->price_include_vat,
                    'number_of_person'=>$rows->number_of_person,
                    'booking_person_number'=>$rows->booking_cart_person_number,
                );

                DB::table('package_booking_details')->insert($data);

                $promotions=DB::table('package_booking_cart_promotion')->where('booking_cart_detail_id',$rows->package_detail_id)->get();
                if($promotions){
                    foreach ($promotions as $promotion){
                        $data=array(
                            'booking_detail_id'=>$promotion->booking_cart_detail_id,
                            'promotion_title'=>$promotion->promotion_title,
                            'promotion_date'=>$promotion->promotion_date,
                        );
                        DB::table('package_booking_promotion')->insert($data);
                    }
                }
                DB::table('package_booking_cart_promotion')->where('booking_cart_detail_id',$rows->package_detail_id)->delete();


                $additional=DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$rows->package_detail_id)->get();
                if($additional){
                    foreach ($additional as $addi){
                        $data=array(
                            'booking_detail_id'=>$addi->booking_cart_detail_id,
                            'additional_service'=>$addi->additional_service,
                            'additional_id'=>$addi->additional_id,
                            'price_service'=>$addi->price_service,
                            'need_someone_share' => $addi->need_someone_share,
                        );
                        DB::table('package_booking_additional')->insert($data);
                    }
                }

                DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$rows->package_detail_id)->delete();

                DB::table('package_booking_cart_details')->where('booking_cart_id',$rows->booking_cart_id)->delete();
            }

            if(Auth::check()){
                DB::table('package_booking_cart')
                    ->where('auth_id',Auth::user()->id)
                    ->delete();
            }

            DB::table('package_booking_cart')
                ->where('session_id',Session::getId())
                ->delete();

            return view('packages.order.confirm');
        }else{
            return redirect()->back();
        }
    }


    public function set_cart_detail_all(Request $request){
        $booking_cart_id=array();
        if(Auth::check()){
            DB::table('package_booking_cart_details')
                ->where('package_id',$request->package_id)
                ->whereIn('booking_cart_id',function ($query){
                    $query->select('booking_cart_id')->from('package_booking_cart')->where('auth_id', Auth::user()->id);
                })
                ->update(['item_status'=>$request->status]);
        }else{
            DB::table('package_booking_cart_details')
                ->where('package_id',$request->package_id)
                ->whereIn('booking_cart_id',function ($query){
                    $query->select('booking_cart_id')->from('package_booking_cart')->where('session_id', Session::getId());
                })

                ->update(['item_status'=>$request->status]);
        }


            $str=DB::table('package_booking_cart');
            if(Auth::check()) {
                $str->where('auth_id',Auth::user()->id);
            }else{
                $str->where('session_id', Session::getId());
            }
            $Cart=$str->get();
            foreach ($Cart as $rows){
                $booking_cart_id[]=$rows->booking_cart_id;
                $step=$rows->booking_step;
            }

            $Carts=null;
            if($Cart){
                $Carts=DB::table('package_booking_cart_details as a')
                    ->whereIn('booking_cart_id',$booking_cart_id)
                    ->whereIn('a.package_detail_id',function ($query) {
                        $query->select('packageDescID')->from('package_details_sub')
                            ->where('status','Y');
                    })
                    ->groupby('package_id')
                    ->get();
            }

            return view('packages.order.ajax.cart-details')->with('Carts',$Carts);

    }

    public function set_cart_detail(Request $request){

        DB::table('package_booking_cart_details')->where('booking_cart_detail_id',$request->package_detail_id)->update(['item_status'=>$request->status]);
        return $this->show_cart();
    }

    public function check_out(){
        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();
        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
        }
        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details as a')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('a.package_detail_id',function ($query) {
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->groupby('package_id')
                ->get();
        }

        return view('packages.order.admin.cart-details')
            ->with('Carts',$Carts);
    }

    public function count_item_cart(){
        $countCart=0;
        if(Auth::check()){
            $Mycart=\App\Mycart::where('auth_id',Auth::user()->id)->get();

            if($Mycart){
                foreach ($Mycart as $cart){
                    $countCart+=$cart->count_cart()->get()->sum('number_of_person');
                }
            }
        }else{

            $Mycart=\App\Mycart::where('session_id',Session::get('cart_session_id'))->first();

            if($Mycart){
                $countCart=$Mycart->count_cart()->get()->sum('number_of_person');
            }

        }

        return $countCart;
    }

    public function show_cart(){
        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();
        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
        }
        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details as a')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('a.package_detail_id',function ($query) {
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->groupby('package_id')
                ->get();
        }

        return view('packages.order.ajax.cart-details')->with('Carts',$Carts);
    }

    public function show_cart_step2(){

        $booking_cart_id=array();
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }
        $Cart=$str->get();
        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }
        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details as a')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('a.package_detail_id',function ($query) {
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->groupby('package_id')
                ->get();
        }
        $AddressBook=DB::table('address_book')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->where('default_address','1')
            ->where('address_type','1')
            ->first();

        return view('packages.order.ajax.cart-details-step2')
            ->with('AddressBook',$AddressBook)
            ->with('Carts',$Carts);
    }

    public function addition_cart_detail(Request $request){

        $Addi=DB::table('package_additional_services')->where('id',$request->addition)->first();
        $data=array(
            'booking_cart_detail_id'=>$request->cart_id,
            'additional_service'=>$Addi->additional_service,
            'additional_id'=>$Addi->id,
            'price_service'=>$Addi->price_service,
            'bind_package' => 'Y',
        );
        DB::table('package_booking_cart_additional')->insert($data);
        if($request->step=='1'){
            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }
    }

    public function create_order($id){ //step 1
        //  dd(Session::get('language'));

        $booking_cart_id=array();
        if(!Session::has('cart_session_id')){
            Session::put('cart_session_id',Session::getId());
//            Session::put('cart_session_id',Session::get('sessionID'));
        }
        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
        }else{
            $str->where('session_id', Session::getId());
        }

        $Cart=$str->get();

        foreach ($Cart as $rows){
            $booking_cart_id[]=$rows->booking_cart_id;
            $step=$rows->booking_step;
        }
//        dd($booking_cart_id);
        $Carts=null;
        if($Cart){
            $Carts=DB::table('package_booking_cart_details as a')
                ->whereIn('booking_cart_id',$booking_cart_id)
                ->whereIn('a.package_detail_id',function ($query) {
                    $query->select('packageDescID')->from('package_details_sub')
                        ->where('status','Y');
                })
                ->groupby('package_id')
                ->get();
        }

        $TourType=DB::table('package_details_sub')
            ->select('psub_id','TourType')
            ->where('packageDescID',$id)
            ->get();
        //dd($CartDetails);

        $TourAdditional=DB::table('package_additional_services')
            ->select('id','additional_service')
            ->where('packageDescID',$id)
            ->get();

        $AddressBook=array();
        if(Auth::check()){
            $AddressBook = DB::table('address_book')
                ->where('timeline_id', Auth::user()->timeline_id)
                ->where('address_type', '1')
                ->where('default_address', '1')
                ->first();
        }

       // dd($TourAdditional);
        return view('packages.order.create-order')
            ->with('TourType',$TourType)
            ->with('TourAdditional',$TourAdditional)
            ->with('AddressBook',$AddressBook)
            ->with('Carts',$Carts);

    }

    public function booking($id){
//        $session_id = Session::get('sessionID');
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//            ->join('users', 'a.packageBy', '=', 'users.id')
//            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','e.psub_id','d.packageDescID','d.packageDateEnd','e.TourType','e.price_system_fees','e.Price_by_promotion','e.price_include_vat', 'b.packageName','b.packageHighlight')
//            ->select('a.*','d.packageDateStart','e.psub_id','d.packageDescID','d.packageDateEnd','e.TourType','e.price_system_fees','e.Price_by_promotion','e.price_include_vat','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('d.packageDescID',$id)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->first();

        // dd($PackageTourOne);
        $auth_id=0;
        if(Auth::check()){
            $auth_id=Auth::user()->id;
        }

        $data=array(
            'booking_cart_date'=>date('Y-m-d H:i:s'),
            'session_id'=>Session::getId(),
            'auth_id'=>$auth_id
        );

        $str=DB::table('package_booking_cart');
        if(Auth::check()) {
            $str->where('auth_id',Auth::user()->id);
            $link='u/'.Auth::user()->id;
        }else{
            $str->where('session_id', Session::getId());
            $link=$id;
        }
        $Cart=$str->first();
        // dd($link);
        if(!$Cart){
            $booking_cart_id=DB::table('package_booking_cart')->insertGetId($data);
        }else{
            $booking_cart_id=$Cart->booking_cart_id;
        }

        $booking_cart_person_number=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->sum('booking_cart_person_number');
        // dd($PackageTourOne);
        $tour_type=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->where('package_detail_id',$PackageTourOne->packageDescID)
            ->where('tour_type',$PackageTourOne->psub_id)
            ->first();
        // dd($tour_type);
        if($tour_type){
            // dd($tourtype);
            $data=array(
                'number_of_person'=>($tour_type->number_of_person+1),
                'number_of_need_visa'=>($tour_type->number_of_need_visa+1),
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );

            $i=DB::table('package_booking_cart_details')
                ->where('booking_cart_id',$booking_cart_id)
                ->where('package_detail_id',$PackageTourOne->packageDescID)
                ->where('tour_type',$PackageTourOne->psub_id)
                ->update($data);

            $check=DB::table('package_booking_cart_details')
                ->where('tour_type',$PackageTourOne->psub_id)
                ->where('package_detail_id',$PackageTourOne->packageDescID)
                ->first();

            if($check->number_of_person>1){
                DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$check->booking_cart_detail_id)->delete();
            }

        }else{

            $data=array(
                'booking_cart_id'=>$booking_cart_id,
                'tour_type'=>$PackageTourOne->psub_id,
                'package_id'=>$PackageTourOne->packageID,
                'package_detail_id'=>$PackageTourOne->packageDescID,
                'timeline_id'=>$PackageTourOne->timeline_id,
                'booking_cart_normal_price'=>$PackageTourOne->price_system_fees,
                'booking_cart_realtime_price'=>$PackageTourOne->Price_by_promotion,
                'price_include_vat'=>$PackageTourOne->price_include_vat,
                'number_of_person'=>1,
                'number_of_need_visa'=>1,
                'item_status'=>'Y',
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );
            // dd($data);
            $booking_cart_detail_id=DB::table('package_booking_cart_details')->insertGetId($data);


            $CheckAdditional=DB::table('package_details_sub')->where('psub_id',$PackageTourOne->psub_id)->where('additional_id','!=','0')->first();

            if($CheckAdditional){
                $Adi=DB::table('package_additional_services')->where('id',$CheckAdditional->additional_id)->first();
                $data = array(
                    'booking_cart_detail_id' => $booking_cart_detail_id,
                    'additional_service' => $Adi->additional_service,
                    'additional_id'=>$Adi->id,
                    'price_service' => $Adi->price_service,
                    'bind_package' => 'Y',
                );

                DB::table('package_booking_cart_additional')->insert($data);
            }



            $Promotion=DB::table('package_promotion')
                ->where('promotion_operator','Between')
                ->where('promotion_status','Y')
                ->orderby('promotion_date_start','asc')
                ->first();
            if(!$Promotion){
                $Promotion=DB::table('package_promotion')
                    ->where('promotion_operator','Mod')
                    ->where('promotion_status','Y')
                    ->first();
            }

            if($Promotion){
                $data=array(
                    'booking_cart_detail_id'=>$booking_cart_detail_id,
                    'promotion_title'=>$Promotion->promotion_title,
                    'promotion_date'=>date('Y-m-d'),
                    'promotion_operator' => $Promotion->promotion_operator,
                    'promotion_operator2' => $Promotion->promotion_operator2,
                    'promotion_value' => $Promotion->promotion_value,
                    'promotion_unit' => $Promotion->promotion_unit,
                );
                DB::table('package_booking_cart_promotion')->insert($data);
            }


        }

        return redirect('/booking/'.$link);

    }

    public function store_order(Request $request){

     //   $session_id = Session::get('sessionID');
        Session::put('step_click','1');

        if(!Session::has('cart_session_id')){
            Session::put('cart_session_id',Session::getId());
//            Session::put('cart_session_id',Session::get('sessionID'));
        }
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDescID','d.packageDateEnd','e.TourType','e.PriceSale','e.price_system_fees','e.Price_by_promotion','e.price_include_vat', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('d.packageDescID',$request->id)
            ->where('e.psub_id',$request->tour_type)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->first();
       // dd($PackageTourOne);
        $auth_id=0;
        if(Auth::check()){
            $auth_id=Auth::user()->id;
        }

        // dd($PackageTourOne);

        $cart=DB::table('package_booking_cart');
        if(Auth::check()) {
            $cart->where('auth_id', Auth::user()->id);
        }else{
            $cart->where('session_id', Session::get('cart_session_id'));
        }
        $check=$cart->first();
       // dd($check);
        $data=array(
            'booking_cart_date'=>date('Y-m-d H:i:s'),
            'session_id'=>Session::get('cart_session_id'),
            'auth_id'=>$auth_id
        );

        if(!$check){
            $booking_cart_id=DB::table('package_booking_cart')->insertGetId($data);
        }else{
            $booking_cart_id=$check->booking_cart_id;
        }

       // dd($booking_cart_id);
        $booking_cart_person_number=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->where('package_detail_id',$PackageTourOne->packageDescID)
            ->sum('booking_cart_person_number');

        $tour_type=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$booking_cart_id)
            ->where('package_detail_id',$PackageTourOne->packageDescID)
            ->where('tour_type',$request->tour_type)
            ->first();


        if($tour_type){
            $data=array(
                'number_of_person'=>($tour_type->number_of_person+$request->number_of_person),
                'number_of_need_visa'=>($tour_type->number_of_need_visa+$request->number_of_person),
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );

            DB::table('package_booking_cart_details')
                ->where('booking_cart_id',$booking_cart_id)
                ->where('package_detail_id',$PackageTourOne->packageDescID)
                ->where('tour_type',$request->tour_type)
                ->update($data);

            $check=DB::table('package_booking_cart_details')
                ->where('tour_type',$request->tour_type)
                ->where('package_detail_id',$PackageTourOne->packageDescID)
                ->first();

            if($check->number_of_person>1){
                DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$check->booking_cart_detail_id)->delete();
            }

        }else{
            $data=array(
                'booking_cart_id'=>$booking_cart_id,
                'item_status'=>'Y',
                'tour_type'=>$request->tour_type,
                'package_id'=>$PackageTourOne->packageID,
                'timeline_id'=>$PackageTourOne->timeline_id,
                'package_detail_id'=>$PackageTourOne->packageDescID,
                'booking_cart_normal_price'=>$PackageTourOne->price_system_fees,
                'booking_cart_realtime_price'=>$PackageTourOne->Price_by_promotion,
                'price_include_vat'=>$PackageTourOne->price_include_vat,
                'number_of_person'=>$request->number_of_person,
                'number_of_need_visa'=>$request->number_of_person,
                'booking_cart_person_number'=>$booking_cart_person_number+1,
            );

            $booking_cart_detail_id=DB::table('package_booking_cart_details')->insertGetId($data);

            if($request->number_of_person=='1'){
                $CheckAdditional=DB::table('package_details_sub')->where('psub_id',$request->tour_type)->where('additional_id','!=','0')->first();
                if($CheckAdditional){
                    $Adi=DB::table('package_additional_services')->where('id',$CheckAdditional->additional_id)->first();
                    $data = array(
                        'booking_cart_detail_id' => $booking_cart_detail_id,
                        'additional_service' => $Adi->additional_service,
                        'additional_id'=>$Adi->id,
                        'price_service' => $Adi->price_service,
                        'bind_package' => 'Y',
                    );
                    DB::table('package_booking_cart_additional')->insert($data);
                }

            }


            $Promotion=DB::table('package_promotion')
                ->where('promotion_operator','Between')
                ->where('promotion_status','Y')
                ->where('packageDescID',$PackageTourOne->packageDescID)
                ->orderby('promotion_date_start','asc')
                ->first();
          //  dd($Promotion);
            if(!$Promotion){
                $Promotion=DB::table('package_promotion')
                    ->where('promotion_operator','Mod')
                    ->where('packageDescID',$PackageTourOne->packageDescID)
                    ->where('promotion_status','Y')
                    ->first();
            }
            if($Promotion){
                $data=array(
                    'booking_cart_detail_id'=>$booking_cart_detail_id,
                    'promotion_title'=>$Promotion->promotion_title,
                    'promotion_date'=>date('Y-m-d'),
                    'promotion_operator' => $Promotion->promotion_operator,
                    'promotion_operator2' => $Promotion->promotion_operator2,
                    'promotion_value' => $Promotion->promotion_value,
                    'promotion_unit' => $Promotion->promotion_unit,
                );
                DB::table('package_booking_cart_promotion')->insert($data);
            }

        }

        if(isset($request->submit_order)){
            return redirect('booking/'.$request->id);
        }else{
            return back();
        }

    }
    
    public function update_cart(Request $request){

        if($request->colum=='type') {
            $check = DB::table('package_details_sub')->where('psub_id', $request->tour_type)->first();
            // dd($check);
            $data = array(
                'tour_type' => $request->tour_type,
                'booking_cart_normal_price' => $check->price_system_fees,
                'booking_cart_realtime_price' => $check->Price_by_promotion,
            );
        }else if($request->colum=='visa'){

            $data=array(
                'number_of_need_visa'=>$request->number_of_need_visa,
            );
        }else if($request->colum=='amount'){

            $check=DB::table('package_booking_cart_details')
                ->where('booking_cart_detail_id',$request->cart_id)
                ->first();
            if($request->number_of_person==1) {
                $CheckAdditional=DB::table('package_details_sub')->where('psub_id',$check->tour_type)->where('additional_id','!=','0')->first();

                if($CheckAdditional){
                    $Adi=DB::table('package_additional_services')->where('id',$CheckAdditional->additional_id)->first();
                    $data = array(
                        'booking_cart_detail_id' => $check->booking_cart_detail_id,
                        'additional_service' => $Adi->additional_service,
                        'additional_id'=>$Adi->id,
                        'price_service' => $Adi->price_service,
                        'bind_package' => 'Y',
                    );
                    $AdditionHave=DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$check->booking_cart_detail_id)->where('additional_id',$Adi->id)->first();
                    if(!$AdditionHave){
                        DB::table('package_booking_cart_additional')->insert($data);
                    }

                }
            }else{
                DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$check->booking_cart_detail_id)->where('bind_package','Y')->delete();
            }
            if($request->number_of_person > $check->number_of_person){
                $up_person=$request->number_of_person-$check->number_of_person;
                $data=array(
                    'number_of_person'=>$request->number_of_person,
                    'number_of_need_visa'=>$request->number_of_person,
                    'booking_cart_person_number'=>$check->number_of_person+$up_person,
                );
            }else if($request->number_of_person < $check->number_of_person){

                  $down=$check->number_of_person-$request->number_of_person;
                  if($check->booking_cart_person_number>$request->number_of_person){
                      $update_num=$check->booking_cart_person_number-$down;
                  }else{
                      $update_num=$request->number_of_person;
                  }

                  $data=array(
                    'number_of_person'=>$request->number_of_person,
                    'number_of_need_visa'=>$request->number_of_person,
                    'booking_cart_person_number'=>$update_num,
                  );
            }

        }

        DB::table('package_booking_cart_details')
            ->where('booking_cart_detail_id',$request->cart_id)
            ->update($data);

        if($request->step=='1'){

            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }

    }

    public function addition_cart_delete(Request $request){
        DB::table('package_booking_cart_additional')
            ->where('id',$request->id)
            ->delete();

        if($request->step=='1'){
            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }
    }

    public function delete_cart_detail(Request $request){

        $i=DB::table('package_booking_cart_details')
            ->where('booking_cart_detail_id',$request->cart_id)
            ->delete();

        DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$request->cart_id)->delete();
        DB::table('package_booking_cart_promotion')->where('booking_cart_detail_id',$request->cart_id)->delete();
        //dd($i);
        if($request->step=='1'){
            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }

    }

    public function delete_cart(Request $request){

        $DelCart=DB::table('package_booking_cart_details')
            ->where('booking_cart_id',$request->cart_id)
            ->where('package_id',$request->package_id)
            ->get();
        foreach ($DelCart as $rows){
            DB::table('package_booking_cart_additional')->where('booking_cart_detail_id',$rows->booking_cart_detail_id)->delete();
            DB::table('package_booking_cart_promotion')->where('booking_cart_detail_id',$rows->booking_cart_detail_id)->delete();
            DB::table('package_booking_cart_details')->where('booking_cart_detail_id',$rows->booking_cart_detail_id)->delete();
        }
        $str=DB::table('package_booking_cart as a')
            ->join('package_booking_cart_details as b','b.booking_cart_id','=','b.booking_cart_id')
            ->where('a.booking_cart_id',$request->cart_id);
            if(Auth::check()) {
                $str->where('a.auth_id',Auth::user()->id);
            }else{
                $str->where('a.session_id', Session::getId());
            }
            $Check=$str->get();

        if(!$Check){
            DB::table('package_booking_cart')->where('booking_cart_id',$request->cart_id)->delete();
        }
        // dd($i.$request->cart_id);
        if($request->step=='1'){
            return $this->show_cart();
        }else{
            return $this->show_cart_step2();
        }

    }


    public function make_pdf_invoice($id,$invoice_type){
        ini_set('display_errors',true);

        if($invoice_type=='all'){
            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$id)
                ->get();

            $Booking_id=$Invoices[0]->invoice_booking_id;
            $pdf = PDF::loadView('packages.order.pdf-invoice-tour', compact('Invoices','Booking_id'));
        }else{

//            $Booking=DB::table('package_bookings')
//                ->where('booking_id',$Invoice->invoice_booking_id)
//                ->first();
//
//            $Booking_detail=DB::table('package_booking_details')
//                ->where('booking_id',$Invoice->invoice_booking_id)
////            ->where('package_id',$package)
//                ->get();

            if($invoice_type==1) {
                $Invoice=DB::table('package_invoice')
                    ->where('invoice_id',$id)
                    ->first();
                $pdf = PDF::loadView('packages.order.pdf-invoice-deposit', compact( 'Invoice'));
            }else if($invoice_type==2){
                $Invoice=DB::table('package_invoice')
                    ->where('invoice_id',$id)
                    ->where('invoice_type',2)
                    ->first();
                //dd($Invoice);
                $pdf = PDF::loadView('packages.order.pdf-invoice-balance', compact('Invoice'));
            }
        }

        return $pdf->download('invoice-deposit-'.date('Y-m-d').'.pdf');
}

    public function pdf_invoice_deposit($id,$type){
        $Booking=DB::table('package_bookings')
            ->where('booking_id',$id)
            ->first();

        $Booking_detail=DB::table('package_booking_details')
            ->where('booking_id',$id)
//            ->where('package_id',$package)
            ->get();

        $Invoices=DB::table('package_invoice')
            ->where('invoice_booking_id',$id)
            ->where('invoice_type',$type)
            ->get();

        ini_set('display_errors',true);
        $pdf = PDF::loadView('packages.order.pdf-invoice-deposit', compact('Booking','Booking_detail','Invoices'));
        return $pdf->download('invoice-deposit-'.date('Y-m-d').'.pdf');
    }


    public function download_pdf_invoice($id){
            ini_set('display_errors',true);
            $Invoices=DB::table('package_invoice')
                ->where('invoice_booking_id',$id)
                ->get();
//            dd($Invoices);
            $pdf = PDF::loadView('packages.order.pdf-booking-confirmation2', compact('Invoices'));
            return $pdf->download('invoice-tour-'.date('Y-m-d').'.pdf');

    }

    public function download_pdf_one_invoice($id){
        ini_set('display_errors',true);
        $Invoice=DB::table('package_invoice')
            ->where('invoice_id',$id)
            ->first();
        $pdf = PDF::loadView('packages.order.pdf-invoice-by-id', compact('Invoice'));
        return $pdf->download('invoice-tour-'.date('Y-m-d').'.pdf');
    }

    public function download_pdf_invoice_dd($id){
        $Invoices=DB::table('package_invoice')
            ->where('invoice_booking_id',$id)
            ->get();

       foreach($Invoices as $Invoice){
           $Booking=DB::table('package_bookings')
               ->where('booking_id',$Invoice->invoice_package_id)
               ->first();

           $Booking_detail=DB::table('package_booking_details')
               ->where('booking_id',$Invoice->invoice_booking_id)
//            ->where('package_id',$package)
               ->get();

           ini_set('display_errors',true);
//          $pdf = PDF::loadView('packages.order.pdf-invoice-type', compact('Booking','Booking_detail','Invoice'));
           $pdf = PDF::loadView('packages.order.pdf-booking-confirmation', compact('Booking','Booking_detail','Invoice'));

           if($Invoice->invoice_type=='1'){
               return $pdf->download('invoice-deposit-'.date('Y-m-d').'.pdf');
           }else{
               return $pdf->download('invoice-tour-'.date('Y-m-d').'.pdf');
           }
       }

    }


}
