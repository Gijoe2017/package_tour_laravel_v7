<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    //use SoftDeletes;

    // *
    //  * The attributes that should be mutated to dates.
    //  *
    //  * @var array
     
    // protected $dates = ['deleted_at'];
    
    protected $table = 'media';


    protected $fillable = ['id','title','rate_group','type','review_group','posted_position','source','timeline_id','user_id','created_at','updated_at','from_site','permission','photo_credit_type','photo_credit_name','photo_credit_url'];
    public function album()
    {
        return $this->hasOne('App\Album', 'id', 'preview_id');
    }

    public function albums()
    {
        return $this->belongsToMany('App\Album', 'album_media', 'media_id', 'album_id');
    }

    public function post()
    {
        return $this->belongsToMany('App\Post', 'post_media', 'media_id', 'post_id');
    }

    public function wallpaper()
    {
        return $this->belongTo('App\Wallpaper');
    }
}
