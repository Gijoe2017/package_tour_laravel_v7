<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use App\Category;
use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use App\User;
use App\Timeline;
use App\Role;
use App\Location;
use App\Setting;

class FamilyController extends Controller
{
    //
    public function family_list($id=null){
        if($id){
            Session::put('passport_id',$id);
        }
        $bgColor=array(
            '1'=>'bg-green',
            '2'=>'bg-yellow',
            '3'=>'bg-blue',
            '4'=>'bg-red'
        );
        $Families=DB::table('users_passport_family as a')
            ->join('users_relation_type as b','b.relation_type_id','=','a.relation_type_id')
            ->join('timelines as c','c.id','=','a.place_of_birth')
            ->join('countries as d','d.country_id','=','a.country_of_birth_id')
//            ->select('a.*','c.country','b.relation_type')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('a.language_code',Auth::user()->language)
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.relation_id')
            ->get();
        // dd($Families);
        Session::put('mode_lang','family');
        return view('packages.order_member.forms2.step4')
            ->with('bgColor',$bgColor)
            ->with('Families',$Families);

    }

    public function change_language($id){
        $FamilyLang=DB::table('users_passport_family')
            ->where('relation_id',$id)
            ->where('passport_id',Session::get('passport_id'))
            ->groupby('language_code')
            ->get();

        return view('packages.order_member.forms2.family-show-lang')
            ->with('FamilyLang',$FamilyLang)
            ->with('relation_id',$id);
    }

    public function create(){
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        return view('packages.order_member.forms2.add_family')->with('category_options',$category_options);
    }

    public function edit($id){
        $Family=DB::table('users_passport_family as a')
            ->join('timelines as b','b.id','=','a.place_of_birth')
            ->where('a.relation_id',$id)
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('a.language_code',Auth::user()->language)
            ->first();
       // dd($Family);
        $event="";
        if(!$Family){
            $Family=DB::table('users_passport_family')
                ->where('relation_id',$id)
                ->first();
            $event="change";
        }
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        // dd($Family);
      //  dd($event);
        Session::put('relation_id',$id);

        return view('packages.order_member.forms2.edit_family')
            ->with('category_options',$category_options)
            ->with('Family',$Family)
            ->with('event',$event);
    }

    public function save(Request $request){
        $post = $request->all();

        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;
            }

            $relationCount=DB::table('users_passport_family')
                ->where('passport_id',Session::get('passport_id'))->count();
            $relation=DB::table('users_passport_family')
                ->where('passport_id',Session::get('passport_id'))
                ->where('relation_type_id',$post['relation_type_id'])
                ->first();

            if($relationCount==0){
                $relation_id=1;
                $event='addnew';
            }else{
                if(!isset($relation)){
                    $event='addnew';
                    $relation=DB::table('users_passport_family')->orderby('relation_id','desc')->first();
                    $relation_id=$relation->relation_id+1;
                }else{
                    $relation=DB::table('users_passport_family')
                        ->where('passport_id',Session::get('passport_id'))
                        ->where('relation_type_id',$post['relation_type_id'])
                        ->where('language_code',Auth::user()->language)
                        ->first();
                    if(!$relation){
                        $relation=DB::table('users_passport_family')
                            ->where('passport_id',Session::get('passport_id'))
                            ->where('relation_type_id',$post['relation_type_id'])
                            ->first();
                    }
                    if(isset($relation)){
                        $event='update';
                        $relation_id=$relation->relation_id;
                    }
                }
            }

            $prev_nationality_id="";
            if(isset($post['prev_nationality_id'])){
                $prev_nationality_id=$post['prev_nationality_id'];
            }

            $data = array(
                'relation_id' => $relation_id,
                'passport_id' => Session::get('passport_id'),
                'relation_type_id' => $post['relation_type_id'],
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'nationality_id' => $post['nationality_id'],
                'religion_id' => $post['religion_id'],
                'prev_nationality_id' => $prev_nationality_id,
                'country_of_birth_id' => $post['country_of_birth_id'],
                'place_of_birth' => $timeline_id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'language_code' => Auth::user()->language,
                'created_by_site_id' => URL::current(),
            );
             // dd($data);
            if($event=='addnew'){
                DB::table('users_passport_family')->insert($data);
            }else{
                DB::table('users_passport_family')
                    ->where('relation_id',$relation_id)
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }
            return redirect('package/member/step4');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

//            $check=DB::table('users_passport_family')->where('relation_id',$post['relation_id'])->first();
//            //dd($check);
//            $data=array(
//                'name' => $post['place_of_birth'],
//            );
//            DB::table('timelines')->where('id',$check->place_of_birth)->update($data);

//            $data=array(
//                'address' => $post['address'],
//            );
//            DB::table('locations')->where('timeline_id',$check->place_of_birth)->update($data);



            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;
            }


            $prev_nationality_id="";
            if(isset($post['prev_nationality_id'])){
                $prev_nationality_id=$post['prev_nationality_id'];
            }
            $check=DB::table('users_passport_family')
                ->where('relation_id',$post['relation_id'])
                ->where('passport_id',Session::get('passport_id'))
                ->where('language_code',Auth::user()->language)
                ->first();

            $data = array(
                'relation_id' => $post['relation_id'],
                'passport_id' => Session::get('passport_id'),
                'relation_type_id' => $post['relation_type_id'],
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'religion_id' => $post['religion_id'],
                'nationality_id' => $post['nationality_id'],
                'prev_nationality_id' => $prev_nationality_id,
                'country_of_birth_id' => $post['country_of_birth_id'],
                'place_of_birth' => $timeline_id,
                'created_by_user_id' => Auth::user()->id,
                'language_code' => Auth::user()->language,
                'created_by_site_id' => URL::current(),
            );
             // dd($data);


            if($check){
                DB::table('users_passport_family')
                    ->where('relation_id',$post['relation_id'])
                    ->where('passport_id',Session::get('passport_id'))
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }else{
                DB::table('users_passport_family')->insert($data);
            }

            Session::flash('message','Record has been update success!');
            return redirect()->back();
        }
    }


    public function delete($id){
        DB::table('users_passport_family')
            ->where('relation_id',$id)
            ->where('language_code',Auth::user()->language)
            ->delete();
        return redirect()->back();
    }
}
