<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Http\Response;
use Session;
use Image;
use App\Package\OptionTime;
use App\Package\OptionDays;
class HighlightOfDayController extends Controller
{


    public function FormAddHighlight(){

//        $DayName=DB::table('package_days')
//            ->where('DayCode',Session::get('days'))
//            ->where('LanguageCode',Auth::user()->language)->first();

        $DayName=OptionDays::where('DayCode',Session::get('days'))->active()->first();

        return view('module.packages.program.addhighlight')->with('DayName',$DayName);
    }

    public function FormEditHighlight($id){

        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $HighlightToday=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
            ->where('a.HighlightID',$id)
           // ->where('a.packageDays',Session::get('days'))
            ->first();
            // ->toSql();
            // dd($HighlightToday);
        Session::put('days',$HighlightToday->packageDays);
        Session::put('event','edit');
        $Photos=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->get();

        $HighlightImage=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->first();

        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Auth::user()->language)
            ->where('DayCode',Session::get('days'))->first();
       // echo $id;

        // return view('package.program.edit-highlight-new')
        return view('module.packages.setting.edit-highlight')
            ->with('HighlightImage',$HighlightImage)
            ->with('NameDay',$NameDay)
            ->with('Default',$Package)
            ->with('Photos',$Photos)
            ->with('HighlightToday',$HighlightToday);
    }

    public function store(Request $request){
        $post=$request->all();
        Session::put('days',$post['Days']);
        $v=\Validator::make($request->all(),[
            'Highlight'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{
           // dd($post);
            if(isset($post['Breakfast'])){$Breakfast=$post['Breakfast'];}else{$Breakfast='N';}
            if(isset($post['Lunch'])){$Lunch=$post['Lunch'];}else{$Lunch='N';}
            if(isset($post['Dinner'])){$Dinner=$post['Dinner'];}else{$Dinner='N';}

            $check = DB::table('package_program_highlight')->where('packageID',Session::get('package'))->where('packageDays','1')->first();

            if(!$check){
                Session::put('days','1');
            }

            $data = array(
                'packageID'=>Session::get('package'),
                'packageDays'=>Session::get('days'),
                'Breakfast'=>$Breakfast,
                'Lunch'=>$Lunch,
                'Dinner'=>$Dinner
            );


            $HignlightID= DB::table('package_program_highlight')->insertGetId($data);

//            if ($request->hasFile('picture')) {
//                $pImage = DB::table('package_images')
//                    ->where('packageID', Session::get('package'))
//                    ->where('GroupImage','2')
//                    ->first();
//                if(isset($pImage)){
//                    $fileNameDel = public_path('package-tour/mid/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                    $fileNameDel = public_path('package-tour/small/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                }
//
//                $artPics = $request->file('picture');
//
//                $filename = time() . "." . $artPics->getClientOriginalExtension();
//                list($ori_w, $ori_h) = getimagesize($artPics);
//
//                if ($ori_w >= 1000) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 1000;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 650;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/mid/' . $filename));
//
//                if ($ori_w >= 650) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 650;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 480;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/small/' . $filename));
//                $data = array(
//                    'packageID' => Session::get('package'),
//                    'GroupImage' => '4',
//                    'image_path' => Session::get('MyImagePath').'/package-tour',
//                    'Image' => $filename,
//                    'OrderBy'=>'1'
//                );
//                DB::table('package_images')->insert($data);
//            }

            $data = array(
                'HighlightID'=>$HignlightID,
                'Highlight'=>$post['Highlight'],
                'LanguageCode'=>Auth::user()->language
            );

            $i= DB::table('package_program_highlight_info')->insert($data);

            if($i>0){
                return redirect('/package/schedule');
            }

        }
    }

    public function edit($id){

        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();

        $HighlightToday=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
            ->where('a.HighlightID',$id)
            ->where('a.packageDays',Session::get('days'))
            ->first();

        $Photos=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->get();

        $HighlightImage=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->first();

        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Session::get('Language'))
            ->where('DayCode',Session::get('days'))->first();
       // echo $id;

        // return view('package.program.edit-highlight')
        return view('packages.setting.edit-highlight')
            ->with('HighlightImage',$HighlightImage)
            ->with('NameDay',$NameDay)
            ->with('Default',$Package)
            ->with('Photos',$Photos)
            ->with('HighlightToday',$HighlightToday);
    }

    public function update(Request $request){
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'Highlight'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{
            if(isset($post['Breakfast'])){$Breakfast=$post['Breakfast'];}else{$Breakfast='N';}
            if(isset($post['Lunch'])){$Lunch=$post['Lunch'];}else{$Lunch='N';}
            if(isset($post['Dinner'])){$Dinner=$post['Dinner'];}else{$Dinner='N';}

            $data = array(
                'packageID'=>Session::get('package'),
                'packageDays'=>$post['Days'],
                'Breakfast'=>$Breakfast,
                'Lunch'=>$Lunch,
                'Dinner'=>$Dinner
            );

            DB::table('package_program_highlight')
                ->where('HighlightID',$post['id'])
                ->update($data);

//            if ($request->hasFile('picture')) {
//                $pImage = DB::table('package_images')
//                    ->where('packageID', Session::get('package'))
//                    ->where('GroupImage','4')
//                    ->orderby('OrderBy','asc')
//                    ->first();
//                if(isset($pImage)){
//                    $fileNameDel = public_path('package-tour/mid/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                    $fileNameDel = public_path('package-tour/small/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                }
//
//                $artPics = $request->file('picture');
//                $filename = time() . "." . $artPics->getClientOriginalExtension();
//                list($ori_w, $ori_h) = getimagesize($artPics);
//
//                if ($ori_w >= 1000) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 1000;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 650;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/mid/' . $filename));
//
//                if ($ori_w >= 650) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 650;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 480;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('package-tour/small/' . $filename));
//                $data = array(
//                    'packageID' => Session::get('package'),
//                    'programID' => $post['id'],
//                    'GroupImage' => '4',
//                    'Image' => $filename,
//                    'OrderBy'=>'1'
//                );
//                DB::table('package_images')->insert($data);
//            }

            $data = array(
                'Highlight'=>$post['Highlight']
            );

            $check=DB::table('package_program_highlight_info')
                ->where('HighlightID',$post['id'])
                ->where('LanguageCode',Session::get('language'))
                ->update($data);
          // echo Session::get('language');
          //  dd($check);
            if(Session::has('checking')){
                return redirect()->back();
            }
                return redirect('/package/schedule');
        }
    }

    public function showImage(Request $request){
        if($request->ajax()){
            // $output = "";
            $Img = DB::table('package_images')
                ->where('programID', Session::get('programID'))
                ->where('packageID', Session::get('package'))
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\">";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                // $output .= ' <div class="col-lg-2 col-sm-3 col-xs-6" style="margin-bottom: 20px; padding-right: 5px; padding-left: 5px; height: 150px;>';
                //  $output .= '<a style="position: absolute"  href="' . $row->image_path.'/mid/' . $row->Image . '" data-toggle="lightbox" data-gallery="imagesizes" data-title="' . $row->Image . '"  >';
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"> </li>';

            }
            $output.='</ul>';

            return Response($output);
        }
    }

    public function deleteImage(Request $request)
    {
        if ($request->ajax()) {
            $image = DB::table('package_images')->where('ImageID', $request->picID)->first();
            $fileNameDel = public_path('package-tour/small/' . $image->Image);
            $fileNameDel2 = public_path('package-tour/mid/' . $image->Image);

            if (file_exists($fileNameDel) and $image->Image!='default-add.jpg') {
                unlink($fileNameDel);
                unlink($fileNameDel2);
            }

            DB::table('package_images')->where('ImageID', $image->ImageID)->delete();

            //  $output = "";
            $Img = DB::table('package_images')
                ->where('packageID', Session::get('package'))
                ->where('GroupImage', '4')
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\" >";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"></li>';
            }
            $output.="</ul>";
            return Response($output);
        }
    }

    public function del($id){
        $Program=DB::table('package_programs')->where('programID',$id)->first();

        if(isset($Program)) {
            $Image = DB::table('package_images')
                ->where('GroupImage', '2')
                ->where('packageID', $Program->packageID)->get();

            foreach ($Image as $rows) {

                $fileNameDel = public_path('package-tour/mid/' . $rows->Image);
               
                if (file_exists($fileNameDel) and $rows->Image != 'default.jpg') {
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('package-tour/small/' . $rows->Image);
                if (file_exists($fileNameDel) and $rows->Image != 'default.jpg') {
                    unlink($fileNameDel);
                }

                DB::table('package_images_info')->where('ImageID', $rows->ImageID)->delete();
            }
            DB::table('package_images')
                ->where('GroupImage', '2')
                ->where('packageID', $Program->packageID)->delete();

            DB::table('package_program_info')->where('programID', $id)->delete();
            DB::table('package_programs')->where('programID', $id)->delete();
        }

            return redirect('package/schedule');

    }
    



}
