<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use DB;
use Auth;
use Session;

class PaymentController extends Controller
{
    //

    public function OmisePayment(Request $request){
        Session::put('step_click','4');
        $Bookings=DB::table('package_bookings as a')
            ->join('package_invoice as b','b.invoice_booking_id','=','a.booking_id')
            ->where('b.invoice_id',$request->invoice_id)
            ->first();

        if($Bookings->invoice_type=='1'){
            $data=array(
                'invoice_status'=>'2',
                'payment_amount'=>$Bookings->invoice_amount,
                'payment_option'=>'1',
                'payment_date'=>date('Y-m-d'),
                'payment_time'=>date('H:i:a'),
            );

            DB::table('package_invoice')
                ->where('invoice_id',$Bookings->invoice_id)
                ->where('invoice_type',$Bookings->invoice_type)
                ->update($data);

            $Details=DB::table('package_booking_details')
                ->where('booking_id',$Bookings->invoice_booking_id)
                ->where('package_id',$Bookings->invoice_package_id)
                ->where('package_detail_id',$Bookings->invoice_package_detail_id)
                ->get();

            foreach ($Details as $rows){
                DB::table('package_booking_details')->where('booking_detail_id',$rows->booking_detail_id)->update(['booking_detail_status'=>'2']);
            }
        }

        $data=array(
            'invoice_id'=>$request->invoice_id,
            'invoice_type'=>$Bookings->invoice_type,
            'payment_option'=>'1',
            'payment_date'=>date('Y-m-d'),
            'payment_time'=>date('H:i:a'),
            'payment_amount'=>$Bookings->invoice_amount,
            'payment_status'=>'p',
        );
        DB::table('package_payments')->insert($data);

        $str=DB::table('package_invoice')
            ->where('invoice_id',$Bookings->invoice_id);
        $Invoice=$str->first();

        $dataMail=array(
            'email'=>Auth::user()->email,
            'name'=>Auth::user()->name,
            'message'=>Auth::user()->name.' Payment Date :'.date('Y-m-d H:i:s'),
            'subject'=>trans('email.thank_you_for_paying_for_the_tour'),
            'booking_id'=>$request->booking_id,
            'invoice_id'=>$request->invoice_id,
            'invoice_package_detail_id'=>$request->invoice_package_detail_id
        );

        Mail::send('emails.payment-deposit', $dataMail, function ($m) use ($dataMail) {
            $m->from('info@toechok.com', 'Toechok Co.,ltd.');
            $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
        });

        // dd($Invoice);
        return view('packages.order.payment-status-deposit')
            ->with('Invoice',$Invoice);
    }

    public function OmisePayment_All_invoice(Request $request){

        Session::put('step_click','4');

        if($request->type=='1'){
            $Bookings=DB::table('package_bookings as a')
            ->join('package_invoice as b','b.invoice_booking_id','=','a.booking_id')
            ->where('a.booking_id',$request->booking_id)
            ->where('b.invoice_type',$request->type)
            ->get();
            foreach ($Bookings as $Booking) {
                $data = array(
                    'invoice_status' => '2',
                    'payment_amount' => $request->amount,
                    'payment_date' => date('Y-m-d'),
                    'payment_time' => date('H:i:s'),
                );
                $check = DB::table('package_invoice')
                    ->where('invoice_id', $Booking->invoice_id)
                    ->update($data);

                if ($check) {
                    DB::table('package_booking_details')->where('package_detail_id', $Booking->invoice_package_detail_id)->update(['booking_detail_status' => '2']);
                }
            }

        }else{
            $Invoice=DB::table('package_invoice')->where('invoice_id',$request->invoice_id)->first();
            $Bookings=DB::table('package_bookings as a')
                ->join('package_invoice as b','b.invoice_booking_id','=','a.booking_id')
                ->where('b.invoice_package_detail_id',$Invoice->invoice_package_detail_id)
                ->where('a.booking_id',$request->booking_id)
                ->get();
            //dd($Bookings);
            foreach($Bookings as $rows){
                if($rows->invoice_type==1){
                    $amount2=$request->deposit;
                }else{
                    $amount2=($request->amount-$request->deposit);
                }

                $data=array(
                    'invoice_id'=>$rows->invoice_id,
                    'invoice_type'=>$rows->invoice_type,
                    'payment_option'=>'1',
                    'payment_date'=>date('Y-m-d'),
                    'payment_time'=>date('H:i:a'),
                    'payment_amount'=>$amount2,
                    'payment_status'=>'p',
                );
                DB::table('package_payments')->insert($data);

                $data=array(
                    'invoice_status'=>'4',
                    'payment_amount'=>$amount2,
                    'payment_date'=>date('Y-m-d'),
                    'payment_time'=>date('H:i:a'),
                );

                $check=DB::table('package_invoice')
                    ->where('invoice_id',$rows->invoice_id)
                    ->where('invoice_type',$rows->invoice_type)
                    ->update($data);

                if($check){
                    DB::table('package_booking_details')->where('package_detail_id', $rows->invoice_package_detail_id)->update(['booking_detail_status' => '4']);
                    $check_status=DB::table('package_invoice')->where('invoice_booking_id',$request->booking_id)->where('invoice_status','!=','4')->first();
                    if(!$check_status){
                        DB::table('package_bookings')->where('booking_id',$request->booking_id)->update(['booking_status'=>'4']);
                    }

                    $Booking=DB::table('package_booking_details')->where('booking_id',$request->booking_id)->get();
                    foreach ($Booking as $rows){
                        DB::table('package_details')->where('packageDescID',$rows->package_detail_id)->decrement('NumberOfPeople', $rows->number_of_person);
                    }

                }

            }
        }

        $str=DB::table('package_invoice')
            ->where('invoice_booking_id',$request->booking_id)
            ->where('invoice_package_detail_id',$Invoice->invoice_package_detail_id);
           if($request->type=='1'){
               $str->where('invoice_type',$request->type);
           }
           $Invoice=$str->get();

        $dataMail=array(
            'email'=>Auth::user()->email,
            'name'=>Auth::user()->name,
            'message'=>Auth::user()->name.' Payment Date :'.date('Y-m-d H:i:s'),
            'subject'=>$request->type=='1'?trans('email.thank_you_for_paying_deposit_number').$request->booking_id:trans('email.thank_you_for_paying_the_rest_of_the_tour_fee').$request->booking_id,
            'booking_id'=>$request->booking_id,
            'invoice_id'=>$request->invoice_id,
            'invoice_package_detail_id'=>$request->invoice_package_detail_id
        );

      //  dd($dataMail);
        if($request->type=='1'){
            Mail::send('emails.reply-to-payment', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }else{
            Mail::send('emails.payment-all', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }

        return view('packages.order.payment-status-balance')
            ->with('Invoice',$Invoice);
    }




    public function store(Request $request){
        $dir=public_path('/images/docs/') ;
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $ext=$file->getClientOriginalExtension();
            $brochures='slip-'.$request->date_transfer.'-'.$request->invoice_id.'.'.$ext;
            $check=move_uploaded_file($file,$dir . $brochures);
        }

        $data=array(
            'payment_amount'=>$request->amount,
            'payment_date'=>$request->date_transfer,
            'payment_time'=>$request->time_transfer,
            'payment_by_bank'=>$request->bank,
            'payment_slip'=>$brochures,
            'invoice_status'=>'2',
        );
        DB::table('package_invoice')
            ->where('invoice_id',$request->invoice_id)
            ->update($data);




        if($request->type==1){
            $str='แจ้งการชำระค่ามัดจำสำเร็จ';
        }else{
            $str='แจ้งการชำระยอดคงเหลือสำเร็จ';
        }
        Session::flash('message',$str);
        return back();
    }

    public function Notification($id,$type){
        $PackageCountry=DB::table('package_details as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('b.*')
            ->where('status','Y')
            ->where('language_code',Session::get('language'))
            ->groupby('Country_id')
            ->get();
//        $Invoice=DB::table('package_invoice')->where('invoice_id',$id)->first();

        $Invoice=DB::table('package_invoice as a')
            ->join('package_booking_details as b','b.package_id','=','a.invoice_package_id')
            ->where('a.invoice_id',$id)
            ->where('a.invoice_type',$type)
            ->first();

        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Invoice->invoice_package_id)
            ->first();
        $Details=DB::table('package_booking_details as a')
            ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.booking_id',$Invoice->invoice_booking_id)
            ->where('a.timeline_id',$Invoice->invoice_timeline_id)
            ->get();

        $BankInfo=DB::table('business_verified_bank as a')
            ->join('banks as b','b.bank_code','=','a.bank_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->where('a.timeline_id',$Package->timeline_id)
            ->where('b.language_code',Session::get('language'))
            ->where('c.language_code',Session::get('language'))
            ->groupby('a.timeline_id')
            ->groupby('b.bank_code')
            ->get();

        $Deposit=0;$Totals=0;$Additional_price=0;
        foreach($Details as $rows) {
            $PackageDetailsOne = DB::table('package_details')
                ->where('packageDescID', $rows->package_detail_id)
                ->first();
            $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();
            foreach ($Additional as $rowA){
                $Additional_price+=$rowA->price_service;
            }
            if ($PackageDetailsOne->season == 'Y') {
                $order_by = "desc";
            } else {
                $order_by = "asc";
            }
            $Condition = DB::table('condition_in_package_details as a')
                ->join('package_condition as b', 'b.condition_code', '=', 'a.condition_id')
                ->join('mathematical_formula as c', 'c.formula_id', '=', 'b.formula_id')
                ->where('b.condition_group_id', '1')
                ->where('b.formula_id', '>', 0)
                ->where('a.packageID', $rows->package_id)
                ->orderby('c.value_deposit', $order_by)
                ->first();

            if ($Condition) {
                $Deposit += $Condition->value_deposit * $rows->number_of_person;
            }
            $Totals+=$rows->booking_normal_price*$rows->number_of_person;
        }

        $Tax=$Totals*7/100;

        if($type==1){
            $Amount=$Deposit;
        }else{
            $Amount=$Totals-$Deposit+$Tax+$Additional_price;
        }

        $InvoiceAll=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->where('a.invoice_type',$type)
            ->where('a.invoice_status','1')
            ->where('b.booking_by',Auth::user()->id)
            ->orderby('a.invoice_id','asc')
            ->get();


       // dd($InvoiceAll);
        return view('packages.order.payment')
            ->with('PackageCountry',$PackageCountry)
            ->with('Details',$Details)
            ->with('BankInfo',$BankInfo)
            ->with('Amount',$Amount)
            ->with('InvoiceAll',$InvoiceAll)
            ->with('Invoice',$Invoice);
    }

    public function SetNotification(Request $request){

        $Invoice=DB::table('package_invoice as a')
            ->join('package_booking_details as b','b.package_id','=','a.invoice_package_id')
            ->where('a.invoice_id',$request->invoice_id)
            ->where('a.invoice_type',$request->type)
            ->first();

        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Invoice->invoice_package_id)
            ->first();

        $Details=DB::table('package_booking_details as a')
            ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.package_id',$Invoice->invoice_package_id)
            ->get();

        $Deposit=0;$Totals=0;$Additional_price=0;
        foreach($Details as $rows) {

            $PackageDetailsOne = DB::table('package_details')
                ->where('packageDescID', $rows->package_detail_id)
                ->first();
            $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();
            foreach ($Additional as $rowA){
                $Additional_price+=$rowA->price_service;
            }

            if ($PackageDetailsOne->season == 'Y') {
                $order_by = "desc";
            } else {
                $order_by = "asc";
            }
            $Condition = DB::table('condition_in_package_details as a')
                ->join('package_condition as b', 'b.condition_code', '=', 'a.condition_id')
                ->join('mathematical_formula as c', 'c.formula_id', '=', 'b.formula_id')
                ->where('b.condition_group_id', '1')
                ->where('b.formula_id', '>', 0)
                ->where('a.packageID', $rows->package_id)
                ->orderby('c.value_deposit', $order_by)
                ->first();

            if ($Condition) {
                $Deposit += $Condition->value_deposit * $rows->number_of_person;
            }
            $Totals+=$rows->booking_normal_price*$rows->number_of_person;
        }


        $Tax=$Totals*7/100;

        if($request->type==1){
            $Amount=$Deposit;
        }else{
            $Amount=$Totals-$Deposit+$Tax+$Additional_price;
        }



        $InvoiceAll=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->where('a.invoice_type',$request->type)
            ->where('a.invoice_status','1')
            ->where('b.booking_by',Auth::user()->id)
            ->orderby('a.invoice_id','asc')
            ->get();

        $BankInfo=DB::table('business_verified_bank as a')
            ->join('banks as b','b.bank_code','=','a.bank_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->where('a.timeline_id',$Package->timeline_id)
            ->where('b.language_code',Session::get('language'))
            ->where('c.language_code',Session::get('language'))
            ->groupby('a.timeline_id')
            ->groupby('b.bank_code')
            ->get();

      //  dd($BankInfo);
        return view('packages.order.ajax.payment')
            ->with('BankInfo',$BankInfo)
            ->with('Details',$Details)
            ->with('Amount',$Deposit)
            ->with('InvoiceAll',$InvoiceAll)
            ->with('Invoice',$Invoice);
    }
}
