<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Http\Response;
use Session;
use Image;

class PromotionController extends Controller
{

    public function promotion_list(){
//        Session::put('packageDescID',$id);
        $promotion=DB::table('package_promotion')->where('timeline_id',Session::get('timeline_id'))->get();
        return view('module.packages.promotion.index')
            ->with('promotion',$promotion);
    }

    public function create(){
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Condition=DB::table('promotion_operations')->where('language_code',Auth::user()->language)->get();
        return view('module.packages.promotion.create')
            ->with('Package',$Package)
            ->with('Condition',$Condition);
    }

    public function create_additional(){
        return view('module.packages.setting.additional.create');
    }

    public function edit_additional($id){
        $Additional=DB::table('package_additional_services')->where('id',$id)->first();
        return view('module.packages.setting.additional.edit')->with('Additional',$Additional);
    }


    public function save_additionalDetail(Request $request){
        $post=$request->all();
        $data_sub = array(
            'packageID' => Session::get('package'),
            'packageDescID' => Session::get('packageDescID'),
            'additional_service' => $post['additional_service'],
            'price_service' => intval($post['price_service']),
            'status' => 'Y',

        );
        DB::table('package_additional_services')->insert($data_sub);

//        return back();
        return redirect('package/details/price/'.Session::get('packageDescID'));
    }

    public function update_additionalDetail(Request $request){
        $post=$request->all();
        $data_sub = array(
            'packageID' => Session::get('package'),
            'packageDescID' => Session::get('packageDescID'),
            'additional_service' => $post['additional_service'],
            'price_service' => intval($post['price_service']),

        );
        DB::table('package_additional_services')->where('id',$post['id'])->update($data_sub);

//        return back();
        return redirect('package/details/price/'.Session::get('packageDescID'));
    }


    public function create1($id){
        Session::put('psub_id',$id);
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Condition=DB::table('promotion_operations')->where('language_code',Auth::user()->language)->get();
        return view('module.packages.promotion.create')
            ->with('Package',$Package)
            ->with('Condition',$Condition);
    }

    public function edit1($id){
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Promotion=DB::table('package_promotion')->where('promotion_id',$id)->first();
        $Condition=DB::table('promotion_operations')->where('language_code',Auth::user()->language)->get();
        return view('module.packages.promotion.edit')
            ->with('promotion',$Promotion)
            ->with('Package',$Package)
            ->with('Condition',$Condition);
    }

    public function edit($id){
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Promotion=DB::table('package_promotion')->where('promotion_id',$id)->first();
        $Condition=DB::table('promotion_operations')->where('language_code',Auth::user()->language)->get();
        return view('module.packages.promotion.edit')
            ->with('promotion',$Promotion)
            ->with('Package',$Package)
            ->with('Condition',$Condition);
    }


    public function save(Request $request){
        $post=$request->all();

        $check=DB::table('promotion_operations')
            ->where('operator_code',$post['operator_code'])
            ->where('language_code',Auth::user()->language)
            ->first();
        $promotion_date_start='';
        $promotion_date_start=isset($post['promotion_date_start'])?$post['promotion_date_start']:'';
        $promotion_date_end=isset($post['promotion_date_end'])?$post['promotion_date_end']:'';
        $time_start=isset($post['time_start'])?$post['time_start']:'';
        $time_end=isset($post['time_end'])?$post['time_end']:'';
        $every_booking=isset($post['every_booking'])?$post['every_booking']:'';
       // dd($time_start);

        if($post['operator_code']=='Between'){
            $title=$check->operator_title." ".$promotion_date_start.' '.date('h:i a',strtotime($time_start)).' '.trans('common.to').' '.$promotion_date_end.' '.date('h:i a',strtotime($time_end)).' '.trans('common.'.$post['promotion_operator2']).' '.$post['value_promotion'].' '. $post['unit_promotion'];
            $promotion_date_start=date('Y-m-d H:i:s',strtotime($promotion_date_start.$time_start));
            $promotion_date_end= date('Y-m-d H:i:s',strtotime($promotion_date_end.$time_end));
        }else{
            $title=$check->operator_title." ".$every_booking.' '.trans('common.booking').' '.trans('common.'.$post['promotion_operator2']).' '.$post['value_promotion'].' '. $post['unit_promotion'];
        }
     //   dd($promotion_date_start);

//         dd(date('Y-m-d',strtotime($post['promotion_date_start'])));
      //   dd(date('Y-m-d',$post['promotion_date_start']));
        $data=array(
            'packageDescID'=>Session::get('packageDescID'),
            'psub_id'=>Session::get('psub_id'),
            'promotion_title'=>$title,
            'promotion_operator'=>$post['operator_code'],
            'promotion_date_start'=>$promotion_date_start,
            'promotion_date_end'=>$promotion_date_end,
            'every_booking'=>$every_booking,
            'promotion_operator2'=>$post['promotion_operator2'],
            'promotion_value'=>$post['value_promotion'],
            'promotion_unit'=>$post['unit_promotion'],
            'promotion_status'=>'Y',
        );
      // dd($data);
        DB::table('package_promotion')->insert($data);

//        return back();
        return redirect('package/details/price/'.Session::get('packageDescID'));
//        return redirect('package/detail/promotion');

    }

    public function update(Request $request){
        $post=$request->all();
        $check=DB::table('promotion_operations')
            ->where('operator_code',$post['operator_code'])
            ->where('language_code',Auth::user()->language)
            ->first();

        $promotion_date_start='';
        $promotion_date_start=isset($post['promotion_date_start'])?$post['promotion_date_start']:'';
        $promotion_date_end=isset($post['promotion_date_end'])?$post['promotion_date_end']:'';
        $time_start=isset($post['time_start'])?$post['time_start']:'';
        $time_end=isset($post['time_end'])?$post['time_end']:'';
        $every_booking=isset($post['every_booking'])?$post['every_booking']:'';
        // dd($time_start);

        if($post['operator_code']=='Between'){
            $title=$check->operator_title." ".$promotion_date_start.' '.date('h:i a',strtotime($time_start)).' '.trans('common.to').' '.$promotion_date_end.' '.date('h:i a',strtotime($time_end)).' '.trans('common.'.$post['promotion_operator2']).' '.$post['value_promotion'].' '. $post['unit_promotion'];
            $promotion_date_start=date('Y-m-d H:i:s',strtotime($promotion_date_start.$time_start));
            $promotion_date_end= date('Y-m-d H:i:s',strtotime($promotion_date_end.$time_end));
        }else{
            $title=$check->operator_title." ".$every_booking.' '.trans('common.booking').' '.trans('common.'.$post['promotion_operator2']).' '.$post['value_promotion'].' '. $post['unit_promotion'];
        }
        //   dd(date('Y-m-d',strtotime($post['promotion_date_start'])));
        //   dd(date('Y-m-d',$post['promotion_date_start']));
        $data=array(
            'packageDescID'=>Session::get('packageDescID'),
            'promotion_title'=>$title,
            'promotion_operator'=>$post['operator_code'],
            'promotion_date_start'=>$promotion_date_start,
            'promotion_date_end'=>$promotion_date_end,
            'every_booking'=>$every_booking,
            'promotion_operator2'=>$post['promotion_operator2'],
            'promotion_value'=>$post['value_promotion'],
            'promotion_unit'=>$post['unit_promotion'],
            'promotion_status'=>'Y',
        );
  //  dd($data);
        DB::table('package_promotion')->where('promotion_id',$post['promotion_id'])->update($data);
        return redirect('package/details/price/'.Session::get('packageDescID'));
//        return back();
//        return redirect('package/detail/promotion');

    }


    public function setPromotion(Request $request){
        $post=$request->all();
        if(is_array($post['promotion_id'])) {
            DB::table('promotion_in_package_details')->where('packageDescID',Session::get('packageDescID'))->delete();
            foreach ($post['promotion_id'] as $value){
                $data = array(
                    'packageDescID' => Session::get('packageDescID'),
                    'promotion_id' => $value
                );
                DB::table('promotion_in_package_details')->insert($data);
            }
        }
        Session::flash('message','Success!');
        return back();
    }

    public function set_status(Request $request){
        $data=array('promotion_status'=>$request->status);
        DB::table('package_promotion')->where('promotion_id',$request->id)->update($data);
        return "Success!";
    }
    
    public function DelConditionOther($id){

        DB::table('package_condition')
            ->where('LanguageCode',Auth::user()->language)
            ->where('conditionCode',$id)->delete();
        $check=DB::table('package_condition')
            ->where('conditionCode',$id)->get();

        if(!isset($check)){
            DB::table('package_sub_condition')
               ->where('conditionCode',$id)->delete();
        }
        return redirect('package/condition/edit/'.Session::get('packageDescID'));

    }

    public function delete_additional($id){

        $i=DB::table('package_additional_services')->where('id',$id)->delete();
        if($i>0){
//            return redirect('package/detail/promotion');
            return redirect('package/details/price/'.Session::get('packageDescID'));
        }


    }

    public function delete($id){

        $i=DB::table('package_promotion')->where('promotion_id',$id)->delete();
        if($i>0){
//            return redirect('package/detail/promotion');
//            return back();
            return redirect('package/details/price/'.Session::get('packageDescID'));
        }


    }



}
