<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Package;
use App\Timeline;
use Setting;
use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use DB;
use Auth;
use Image;
use Session;
use App;
use Jenssegers\Date\Date as Date;

class PackageTourController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
       // $this->middleware('auth');
        // if ($i==0) {
        //     // return redirect('/location/list');
        //     // echo $i;die();
        //     // return view('home.index');
        // }else{
        //     if (!Session::get('Language')) {
        //         return redirect('/home');
        //     }

        // }

    }

    public function login_form2(){
        return view('auth.login2');
    }

    public function package_member($id){

        $timeline = Timeline::where('id', Session::get('timeline_id'))->first();

        $package = $timeline->package;
       // dd($package);
        $package_members = $package->members();
       // dd($package_members);
        return view('module.packages.setting.member_list')->with('package_members',$package_members)->with('id',$id);
    }



    public function send_contact_supplier(Request $request){
           // Send mail to contact
    }


    public function with_list(){

//        $PackageTour = DB::table('package_tour as a')
//            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//            ->join('users', 'a.packageBy', '=', 'users.id')
//            ->join('package_details as d','d.packageID','=','a.packageID')
//            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
//            ->select('a.*','d.packageDescID','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','b.packageName','b.packageHighlight')
//            ->whereIn('d.packageDescID',function ($query) {
//                $query->select('packageDescID')->from('package_wishlist')
//                      ->where('user_id',Auth::user()->id);
//            })
//            ->where('b.LanguageCode', Session::get('language'))
//            ->where('b.Status_Info','P')
//            ->where('d.status','Y')
//            ->groupby('d.packageDescID')
//            ->orderby('e.price_system_fees', 'asc')
//            ->orderby('e.psub_id', 'asc')
//            ->get();



        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->whereIn('d.packageDescID',function ($query) {
                $query->select('packageDescID')->from('package_wishlist')
                    ->where('user_id',Auth::user()->id);
            })
            ->where('b.LanguageCode', Session::get('language'))
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->get();




        return view('packages.package-wishlist')

            ->with('PackageTour',$PackageTour);



    }

    public function getMembers(Request $request)
    {
        $timelines = Timeline::where('username', 'like', "%{$request->searchname}%")->where('type', 'user')->where('username', '!=', Auth::user()->username)->get();
        $page_id = $request->page_id;
        $package = Package::where('packageID',$request->page_id);
        $users = new \Illuminate\Database\Eloquent\Collection();

        foreach ($timelines as $key => $timeline) {
            $user = $timeline->user()->with(['package_tour' => function ($query) use ($page_id) {
                $query->where('a.packageID', $page_id);
            }])->get();

            $users->add($user);
        }
        return response()->json(['status' => '200', 'data' => $users]);
    }

    public function search_tour(Request $request){
        $post=$request->all();
        $Package = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'));

            if(isset($post['min_price'])){
                $Package->where('e.price_system_fees','>',$post['min_price'])
                ->where('e.price_system_fees','>',$post['max_price']);
            }

            if(isset($post['travel_date'])){
               if($post['travel_date']!=''){
                   $Package->where('d.packageDateStart','<=',$post['travel_date'])
                       ->where('d.packageDateEnd','>=',$post['travel_date']);
               }
            }

            if(isset($post['search_country'])){
                if($post['search_country']){
                    $country_code=$post['search_country'];
                    $Package->whereIn('a.packageID',function ($query) use($country_code) {
                        $query->select('packageID')->from('package_tourin')
                            ->where('CountryCode',$country_code);
                    });
                }
            }

            if(isset($post['txtsearch'])){
                $Package->where('b.packageName','like','%'.$post['txtsearch'].'%');
            }

            $Package->where('b.Status_Info','P')
                ->where('d.closing_date','>',date('Y-m-d'))
                ->where('d.status','Y')
                ->groupby('a.packageID')
                ->orderby('e.price_system_fees', 'asc')
                ->orderby('e.psub_id', 'asc');

            $PackageTour=$Package->get();

        return view('packages.search_list')
            ->with('PackageTour', $PackageTour);

    }

    public function uploadImage(){
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        return view('package.upload')->with('SiteName',$SiteName);
    }

    public function index(){
        $num =strpos('site'.url()->current(),'http://');
        if($num>0){
            $mysite=str_replace('http://','',url()->current());
            $mysite=str_replace('www.','',$mysite);
            $TitleSite='http://';
        }else{
            $mysite=str_replace('https://','',url()->current());
            $mysite=str_replace('www.','',$mysite);
            $TitleSite='https://';
        }
        $num =strpos($mysite,'/');
        if($num>0){
            $mysite=substr($mysite,0,$num);
        }

        Session::put('Mysite',$mysite);
        Session::put('MyImagePath',$TitleSite.$mysite.'/images');

       // if(!Session::get('Language')){
       //     Session::put('Language','th');
       // }
        $checkarray=array('en','th');

        if(!in_array(Session::get('Language'),$checkarray)){
            Session::put('Language','en');
        }
        App::setLocale(Session::get('Language'));

        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
             //->join('package_images as img','img.packageID','=','a.packageID')
            ->select('users.userURL','users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('Language'))
            ->where('users_info.LanguageCode', Session::get('Language'))
            ->where('b.Status_Info','P')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')->get();
        
     // dd($PackageTour);
     //   $LangUser=DB::table('language')
     //       ->whereIn('LanguageCode',function ($query) {
     //           $query->select('b.LanguageCode')->from('package_tour as a')
     //               ->join('package_tour_info as b','b.packageID','=','a.packageID')
     //               ->where('a.packageBy',Session::get('UserProfile'));
     //       })
     //       ->orderby('LanguageName','asc')->get();
     //   Session::put('LangUser',$LangUser);

        return view('package.index')
            ->with('PackageTour', $PackageTour);

    }

    public function list_package_sub_cate($subcate){


        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->join('package_tour_category as f','f.packageID','=','a.packageID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('f.TourSubCateID',$subcate)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.PriceSale', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->first();
//        echo $country.Session::get('language');
        // dd($PackageTourOne);


        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->join('package_tour_category as f','f.packageID','=','a.packageID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
//            ->whereIn('a.packageID',function ($query) {
//                $query->select('packageID')->from('package_tour_category')
//                      ->where('TourCategoryID',Session::get('group'));
//            })
            ->where('b.LanguageCode', Session::get('language'))
            // ->where('users_info.LanguageCode', Session::get('language'))
            ->where('f.TourSubCateID',$subcate)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.PriceSale', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->get();



        $GroupCounties=DB::table('package_tourin as a')
            ->join('countries as b','b.country_id','=','a.CountryCode')
            ->join('package_tour as c', 'c.packageID', '=', 'a.packageID')
            ->select(DB::raw('count(a.CountryCode) as country_code'),'b.country_id','b.country')
            ->where('b.language_code', Session::get('language'))
            ->where('c.packageStatus','P')
            ->groupby('a.CountryCode')
            ->orderby('country_code','desc')
            ->limit(5)
            ->get();
        //  dd($GroupCounties);
        foreach ($GroupCounties as $rows){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('users', 'a.packageBy', '=', 'users.id')
                ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
                ->join('package_details as d','d.packageID','=','a.packageID')
                ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
                ->join('package_tour_category as f','f.packageID','=','a.packageID')
                ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Session::get('language'))
                ->where('f.TourSubCateID',$subcate)
                ->where('b.Status_Info','P')
                ->where('d.Country_id',$rows->country_id)
                ->where('d.status','Y')
                ->where('e.status','Y')
                ->groupby('a.packageID')
                ->orderby('e.PriceSale', 'asc')
                ->orderby('e.psub_id', 'asc')
                ->get();
            if($Packages->count()>0){
                $PackageCounties[]=$Packages;
                $CountryName[]=$rows->country;
            }

        }


        return view('packages.list_by_category')
            ->with('PackageCounties', $PackageCounties)

            ->with('CountryName', $CountryName)
            ->with('PackageTourOne', $PackageTourOne)
            ->with('PackageTour', $PackageTour);

    }

    public function list_package_cate($cate){

        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->join('package_tour_category as f','f.packageID','=','a.packageID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('f.TourCategoryID',$cate)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.PriceSale', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->first();
//        echo $country.Session::get('language');
        // dd($PackageTourOne);


        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->join('package_tour_category as f','f.packageID','=','a.packageID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
//            ->whereIn('a.packageID',function ($query) {
//                $query->select('packageID')->from('package_tour_category')
//                      ->where('TourCategoryID',Session::get('group'));
//            })
            ->where('b.LanguageCode', Session::get('language'))
            // ->where('users_info.LanguageCode', Session::get('language'))
            ->where('f.TourCategoryID',$cate)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.PriceSale', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->get();



        $GroupCounties=DB::table('package_tourin as a')
            ->join('countries as b','b.country_id','=','a.CountryCode')
            ->join('package_tour as c', 'c.packageID', '=', 'a.packageID')
            ->select(DB::raw('count(a.CountryCode) as country_code'),'b.country_id','b.country')
            ->where('b.language_code', Session::get('language'))
            ->where('c.packageStatus','P')
            ->groupby('a.CountryCode')
            ->orderby('country_code','desc')
            ->limit(5)
            ->get();
        //  dd($GroupCounties);
        foreach ($GroupCounties as $rows){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('users', 'a.packageBy', '=', 'users.id')
                ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
                ->join('package_details as d','d.packageID','=','a.packageID')
                ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
                ->join('package_tour_category as f','f.packageID','=','a.packageID')
                ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Session::get('language'))
                ->where('f.TourCategoryID',$cate)
                ->where('b.Status_Info','P')
                ->where('d.Country_id',$rows->country_id)
                ->where('d.status','Y')
                ->where('e.status','Y')
                ->groupby('a.packageID')
                ->orderby('e.PriceSale', 'asc')
                ->orderby('e.psub_id', 'asc')
                ->get();
            if($Packages->count()>0){
                $PackageCounties[]=$Packages;
                $CountryName[]=$rows->country;
            }

        }


        return view('packages.list_by_category')
            ->with('PackageCounties', $PackageCounties)
            ->with('CountryName', $CountryName)

            ->with('PackageTourOne', $PackageTourOne)
            ->with('PackageTour', $PackageTour);

    }

    public function list_package_country($country){
        \App::setLocale(Session::get('language'));
        Session::put('CountryCode',$country);
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
//            ->where('d.country_id',$country)
            ->whereIn('a.packageID',function ($query) {
                $query->select('packageID')->from('package_tourin')
                    ->where('CountryCode',Session::get('CountryCode'));
            })
            ->where('b.Status_Info','P')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->first();

        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
//            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->whereIn('a.packageID',function ($query) {
                $query->select('packageID')->from('package_tourin')
                      ->where('CountryCode',Session::get('CountryCode'));
            })

            ->where('b.LanguageCode', Session::get('language'))

            ->where('a.packageStatus','CP')
            ->where('a.package_close','N')
            ->where('b.Status_Info','P')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->get();

//dd($PackageTour);




        return view('packages.list_by_country')

            ->with('PackageTourOne', $PackageTourOne)
            ->with('PackageTour', $PackageTour);

    }

    public function list_package_agent($id){
        \App::setLocale(Session::get('language'));
        Session::put('whoner',$id);
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.timeline_id',$id)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->first();

//        dd($PackageTourOne);

        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select('a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
//            ->whereIn('a.packageID',function ($query) {
//                $query->select('packageID')->from('package_tour_category')
//                      ->where('TourCategoryID',Session::get('group'));
//            })
            ->where('b.LanguageCode', Session::get('language'))
            // ->where('users_info.LanguageCode', Session::get('language'))
            ->where('a.timeline_id',$id)
            ->where('b.Status_Info','P')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->get();
        Session::put('timeline_id',$id);
       // dd($PackageTour);


        return view('packages.list_by_agent')

            ->with('PackageTourOne', $PackageTourOne)
            ->with('PackageTour', $PackageTour);
    }

    public function switchLanguage(Request $request)
    {
        if(Auth::check()){
            Auth::user()->update(['language' => $request->language]);
        }
        \App::setLocale($request->language);

        Session::put('language',$request->language);

       // Setting::set('language', $request->language);
      //  dd(\App::getLocale());
        //dd(Session::get('language'));
//         return redirect()->back();

        return response()->json(['status' => '200', 'message' => 'Switched language to '.$request->language,'checklanguage'=>Session::get('language')]);
    }

    public function list_package_more(){

          if(Session::get('CountryCode')){
              return $this->list_package_country(Session::get('CountryCode'));
          }else{
              return $this->list_tour();
          }
    }

    public function list_tour($group = null){

          if(!Session::get('language')){
             Session::put('language','th');
          }



          if(is_numeric($group)){
              if(!$group){
                  Session::put('group','22');
              }else{
                  Session::put('group',$group);
              }
          }else if($group!=null && $group!='all'){
              Session::put('language',$group);
          }

        \App::setLocale(Session::get('language'));
        Session::forget('CountryCode');

       // Session::put('whoner','');
      // Session::flush();

        DB::table('package_details')->where('packageDateStart','<=',date('Y-m-d'))->update(['status'=>'N']);
        $Packages=Package::where('package_close','N')->where('packageStatus','CP')->get();

        foreach ($Packages as $rows){
            $check=DB::table('package_details')
                ->where('packageID',$rows->packageID)
                ->where('status','Y')
                ->count();
                if($check==0){
                  DB::table('package_tour')->where('packageID',$rows->packageID)->update(['package_close'=>'Y']);
                }
        }

        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','b.packageName','b.packageHighlight')
//            ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('b.Status_Info','P')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('e.price_system_fees', 'asc')
            ->orderby('e.psub_id', 'asc')
            ->first();
        //dd($PackageTourOne);



        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('b.Status_Info','P')
            ->where('a.packageStatus','CP')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->limit(20)
            ->get();


        $PackageSuggest = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//            ->join('users', 'a.packageBy', '=', 'users.id')
//            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
//            ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
//            ->whereIn('a.packageID',function ($query) {
//                $query->select('packageID')->from('package_tour_category')
//                      ->where('TourCategoryID',Session::get('group'));
//            })
            ->where('b.LanguageCode', Session::get('language'))
            // ->where('users_info.LanguageCode', Session::get('language'))
            ->where('b.Status_Info','P')
            ->where('a.packageStatus','CP')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('e.psub_id', 'asc')
            ->limit(20)
            ->get();


        $PackageLatest = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//            ->join('users', 'a.packageBy', '=', 'users.id')
//            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
//            ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->whereIn('a.packageID',function ($query) {
                $query->select('packageID')->from('package_tourin')
                      ->groupby('CountryCode');
            })
            ->where('b.LanguageCode', Session::get('language'))
            // ->where('users_info.LanguageCode', Session::get('language'))
            ->where('b.Status_Info','P')
            ->where('a.packageStatus','CP')
            ->where('d.closing_date','>',date('Y-m-d'))
            ->where('d.status','Y')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('e.psub_id', 'asc')
            ->limit(20)
            ->get();




        return view('packages.list')
            ->with('PackageLatest', $PackageLatest)
            ->with('PackageTourOne', $PackageTourOne)
            ->with('PackageSuggest', $PackageSuggest)
            ->with('PackageTour', $PackageTour);
    }

    public function common_member_info($type){
        $Content=$type;


        return view('member.common-profile')

            ->with('Content',$Content);
    }

    public function Detail($id,$lang){
        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        $hostname = '';
        $city = '';
        $country = '';
        $region = '';
        $loc = '';
        $org = '';
        $postal = '';
        if (isset($details->hostname)) {
            $hostname = $details->hostname;
        }
        if (isset($details->city)) {
            $city = $details->city;
        }
        if (isset($details->region)) {
            $region = $details->region;
        }
        if (isset($details->country)) {
            $country = $details->country;
        }
        if (isset($details->loc)) {
            $loc = $details->loc;
        }
        if (isset($details->org)) {
            $org = $details->org;
        }
        if (isset($details->postal)) {
            $postal = $details->postal;
        }

        $data = array(
            'packageID' => $id,
            'IP' => $details->ip,
            'HostName' => $hostname,
            'FromCity' => $city,
            'FromRegion' => $region,
            'FromCountry' => $country,
            'LatitudeLongitude' => $loc,
            'WebProvider' => $org,
            'ZipCode' => $postal,
            'DateTimeView' => date('Y-m-d H:i:s')
        );
        DB::table('package_tour_tracking')->insert($data);
        
        if($lang){
            Session::put('Language',$lang);
        }
        $Langs = Session::get('Language');
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();

        Date::setLocale($Langs);
        $DetailPackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'users.id', '=', 'a.CreateByID')
            ->join('users_info', 'users_info.UserID', '=', 'a.CreateByID')
            ->join('package_tour_category', 'package_tour_category.packageID', '=', 'a.packageID')
            ->join('package_tour_type', 'package_tour_category.PackageTourTypeID', '=', 'package_tour_type.PackageTourTypeID')
            ->join('package_tour_schedule', 'package_tour_schedule.packageID', '=', 'a.packageID')
            ->select('package_tour_type.PackageTourTypeName', 'users_info.FirstName', 'users_info.LastName',
                'users.userURL', 'users_info.LastName', 'b.Title', 'b.Address', 'b.Contact',
                'a.CreateByID', 'a.packageID', 'a.DateCreate', 'a.DateStart', 'a.DateEnd',
                'a.ZipCode', 'package_tour_schedule.Detail',
                'b.Description', 'b.Contact', 'a.Image')
            ->where('b.LanguageCode', $Langs)
            ->where('package_tour_schedule.LanguageCode', $Langs)
            ->where('users_info.LanguageCode', $Langs)
            ->where('package_tour_type.LanguageCode', $Langs)
            ->where('a.packageID', $id)
            ->orderby('a.DateCreate', 'desc')->first();


        $CreateBy = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'users.id', '=', 'a.CreateByID')
            ->join('users_info', 'users_info.UserID', '=', 'a.CreateByID')
            ->select('b.Title', 'a.packageID',
                'a.DateCreate', 'a.DateStart', 'a.CreateByID', 'b.Description','a.image_path', 'a.Image',
                'users_info.FirstName', 'users.userURL', 'users_info.LastName')
            ->where('b.LanguageCode', $Langs)
            ->where('users_info.LanguageCode', $Langs)
            ->orderby('a.DateCreate', 'desc')->get();
        //dd($Artist);

        $MorePackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'users.id', '=', 'a.CreateByID')
            ->join('users_info', 'users_info.UserID', '=', 'a.CreateByID')
            ->select('b.Title',  'a.packageID',
                'a.DateCreate', 'a.DateStart', 'a.CreateByID', 'b.Description',
                'a.Image',
                'users_info.FirstName', 'users.userURL', 'users_info.LastName')
            ->where('b.LanguageCode', $Langs)
            ->where('users_info.LanguageCode', $Langs)
            ->orderby('a.DateCreate', 'desc')->get();

        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'users.id', '=', 'a.CreateByID')
            ->join('users_info', 'users_info.UserID', '=', 'a.CreateByID')
            ->select('b.Title', 'a.packageID',
                'a.DateCreate', 'a.DateStart', 'a.CreateByID',
                'b.Description', 'a.Image',
                'users_info.FirstName', 'users.userURL', 'users_info.LastName')
            ->where('b.LanguageCode', $Langs)
            ->where('users_info.LanguageCode', $Langs)
            ->orderby('a.DateCreate', 'desc')->get();

        $WorkOfArtDisplay = DB::table('exhibition')
            ->join('work_of_art_in_exhibition', 'work_of_art_in_exhibition.ExhibitionID', '=', 'exhibition.ExhibitionID')
            ->join('exhibition_info', 'exhibition_info.ExhibitionID', '=', 'exhibition.ExhibitionID')
            ->join('work_of_art', 'work_of_art_in_exhibition.WorkOfArtID', '=', 'work_of_art.WorkOfArtID')
            ->join('work_of_art_info', 'work_of_art_in_exhibition.WorkOfArtID', '=', 'work_of_art_info.WorkOfArtID')
            ->where('work_of_art_info.LanguageCode', Session::get('Language'))
            ->where('exhibition_info.LanguageCode', Session::get('Language'))
            ->where('work_of_art.status', 'Y')
            ->where('work_of_art_in_exhibition.ExhibitionID', $id)
            ->orderby('work_of_art_info.Title', 'desc')->get();

        $ArtistDisplay = DB::table('artist')
            ->join('exhibition_artist', 'exhibition_artist.ArtistID', '=', 'artist.artistID')
            ->where('artist.LanguageCode', Session::get('Language'))
            ->where('exhibition_artist.ExhibitionID', $id)
            ->orderby('artist.artistName', 'asc')->get();

        $CountViews = DB::table('exhibition_tracking')->where('ExhibitionID', $id)->isset();
        //dd($CountViews);
        $Views = DB::table('exhibition_tracking')->where('ExhibitionID', $id)->orderby('DateTimeView', 'desc')->first();
        return view('Package-Tour.index')
            ->with('CountViews', $CountViews)
            ->with('Views', $Views)
            ->with('CreateBy', $CreateBy)
            ->with('MorePackageTour', $MorePackageTour)
            ->with('PackageTour', $PackageTour)
            ->with('WorkOfArtDisplay', $WorkOfArtDisplay)
            ->with('ArtistDisplay', $ArtistDisplay)
            ->with('DetailPackageTour', $DetailPackageTour)
            ->with('lang', $lang);
    }

    public function DetailProfile($id,$createby,$lang){

        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        $hostname = '';
        $city = '';
        $country = '';
        $region = '';
        $loc = '';
        $org = '';
        $postal = '';
        if (isset($details->hostname)) {
            $hostname = $details->hostname;
        }
        if (isset($details->city)) {
            $city = $details->city;
        }
        if (isset($details->region)) {
            $region = $details->region;
        }
        if (isset($details->country)) {
            $country = $details->country;
        }
        if (isset($details->loc)) {
            $loc = $details->loc;
        }
        if (isset($details->org)) {
            $org = $details->org;
        }
        if (isset($details->postal)) {
            $postal = $details->postal;
        }

        $data = array(
            'packageID' => $id,
            'IP' => $details->ip,
            'HostName' => $hostname,
            'FromCity' => $city,
            'FromRegion' => $region,
            'FromCountry' => $country,
            'LatitudeLongitude' => $loc,
            'WebProvider' => $org,
            'ZipCode' => $postal,
            'DateTimeView' => date('Y-m-d H:i:s')
        );
        DB::table('package_tour_tracking')->insert($data);
        if($lang){
            Session::put('Language',$lang);
        }
        $Langs = Session::get('Language');
      //  $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();

        Date::setLocale($Langs);
        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.userURL','users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('Language'))
            ->where('users_info.LanguageCode', Session::get('Language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')->first();

        $Programs=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->join('package_days as c','c.DayCode','=','a.packageDays')
            ->where('a.packageID',$id)
            ->where('b.LanguageCode',Session::get('Language'))
            ->where('c.LanguageCode',Session::get('Language'))
            ->orderby('c.DayCode','asc')
            ->get();
        // dd($Programs);
        $Datails=DB::table('package_details')->where('packageID',$id)->get();

        $Pics = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)->get();
         // $Photo = DB::table('package_images')
         //   ->where('GroupImage','1')
         //   ->where('packageID', $id)->first();

        // dd($DetailArt);
        $SiteName=DB::table('users')->where('id',$createby)->first();
        $CountViews = DB::table('package_tour_tracking')->where('packageID', $id)->isset();
      
        $Views = DB::table('package_tour_tracking')->where('packageID', $id)->orderby('DateTimeView', 'desc')->first();
        return view('package.agen.index')
            ->with('CountViews', $CountViews)
            ->with('SiteName', $SiteName)
            ->with('Views', $Views)
            ->with('Pics', $Pics)
            ->with('Datails', $Datails)
            ->with('Programs', $Programs)
            ->with('PackageTour', $PackageTour);
    }

    public function Detail2($id){
        $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.userURL','users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('Language'))
            ->where('users_info.LanguageCode', Session::get('Language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')->first();

        $Programs=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->join('package_days as c','c.DayCode','=','a.packageDays')
            ->orderby('c.DayCode','asc')
            ->get();

        $Pics = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)->get();
        $Photo = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)->first();
        return view('ajax.index')
            ->with('Programs',$Programs)
            ->with('Photo', $Photo)
            ->with('Pics', $Pics)
            ->with('PackageTour', $PackageTour);
    }

    public function copyPackage($id){
        $Package=DB::table('package_tour_info')->where('packageID',$id)->first();
        $data=array(
            'packageID'=>$Package->packageID,
            'show_in_timeline'=>Session::get('timeline_id'),
            'commission_percent'=>$Package->commission_percent,
            'packageName'=>$Package->packageName,
            'packageHighlight'=>$Package->packageHighlight,
            'image'=>$Package->image,
            'Status_Info'=>$Package->Status_Info,
            'LanguageCode'=>$Package->LanguageCode,
        );
        DB::table('package_tour_info')->insert($data);
      return back();
    }

    public function checking($id){
        \Date::setLocale(Session::get('language'));
        Session::put('checking','yes');
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('users_info.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')
            ->first();
       // dd($PackageTourOne);
        return view('module.packages.setting.checking')->with('PackageTourOne',$PackageTourOne);
    }

//    public function update_last_event($id){
//        $data=array(
//            'package_latest_view'=>date('Y-m-d H:i:s')
//        );
//        DB::table('package_tour')->where('packageID',$id)->update($data);
//
//        return back();
//    }

    public function update_last_event($id){

        $check=DB::table('package_tour_latest_log')
            ->where('package_latest_log_by',Auth::user()->id)
            ->get();
        $date=array(
            'package_id'=>$id,
            'package_latest_log_by'=>Auth::user()->id,
            'log_type'=>'Open',
            'package_latest_log'=>date('Y-m-d H:i:s')
        );

        if($check->count()>=10){

            $Package=DB::table('package_tour_latest_log')
                ->where('package_latest_log_by',Auth::user()->id)
                ->orderby('package_latest_log','asc')
                ->first();
            DB::table('package_tour_latest_log')->where('id',$Package->id)->update($date);
        }else{
            DB::table('package_tour_latest_log')->insert($date);
        }
        DB::table('package_tour')->where('packageID',$id)->update(['package_latest_view'=>date('Y-m-d H:i:s')]);
        return back();
    }

    public function view_package_check($id){
        \Date::setLocale(Session::get('language'));
        if(!Session::get('language')){
            Session::put('language','th');
        }
        $this->update_last_event($id);
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('users_info.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')
            ->first();
            // ->toSql();

        if($PackageTourOne) {
            $category_arr = array();
//            $pid = $PackageTourOne->packageID;
        }

        $Cate=DB::table('package_tour_category as a')
            ->select('a.TourCategoryID')
            ->where('a.packageID',$id)->get();
        // dd($Cate);
        foreach ($Cate as $rows){
            $category_arr[]=$rows->TourCategoryID;
        }
        // dd($pid);
        $PackageSlide = DB::table('highlight_in_schedule as a')
            ->join('package_location_info as b', 'b.LocationID', '=', 'a.LocationID')
            ->join('package_images as d','d.ImageID','=','b.ImageID')
           // ->where('b.LanguageCode', Session::get('Language'))
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID', $id)
            ->where('a.makeSlideshow','Y')
            ->groupby('a.LocationID')
            ->orderby('a.OrderBy', 'asc')
            ->get();
            // ->toSql();

        $GetTime=DB::table('package_times')
            ->where('LanguageCode',Session::get('language'))
            ->orderby('TimeCode','asc')->get();
        $i=0;
        foreach ($GetTime as $rows){
            $i++;
            $Times[$i]=$rows->Time_text;
        }

      //  dd($GetTime);
        $PackageIn=DB::table('package_tourin')->where('packageID',$id)->get();

        $Programs=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('a.packageID',$id)
            ->where('b.LanguageCode',Session::get('language'))
            ->orderby('a.packageDays','asc')
            ->orderby('a.packageTime','asc')
            ->get();
            // ->toSql();
            //  dd($Programs);

        $PackageHighlight=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
            ->where('b.LanguageCode',Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('packageDays','asc')
            ->get();
            // ->toSql();
            // dd($PackageHighlight);
      //  dd($PackageHighlight);
        $Datails=DB::table('package_details as a')
            ->join('airline as b','b.airline','=','a.Airline')
            ->where('a.packageID',$id)
            ->orderby('a.packageDateStart','asc')
            ->get();
            // ->toSql();
          //   dd($Datails);
        $TourDate=null;$i=1;$SpecialPrice=0;
        foreach ($Datails as $rows){
            if($i==1){
                $SpecialPrice=$rows->PriceSale;
            }
            $date=$this->setDate($rows->packageDateStart,$rows->packageDateEnd);
            $TourDate.=$date.',';
            $i++;
        }
        $Cateroty=DB::table('package_tour_category as a')
            ->join('tour_category as b','b.TourCategoryID','=','a.TourCategoryID')
            ->select('b.*')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)->first();

        $SubCateroty=DB::table('package_tour_category as a')
            ->join('tour_category_sub1 as b','b.groupID','=','a.TourSubCateID')
            ->select('b.*')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)->first();
        //dd($SubCateroty);

        $Pics = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->get();
            // ->toSql();
            // dd($Pics);
        $Photo = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->first();
            // ->toSql();
            // dd($Photo);
       // $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        if($PackageTourOne){
            Session::put('pname',$PackageTourOne->packageName);
        }

       // dd($PackageTourOne);
        Session::put('package',$id);



        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))
            ->first();
        //  dd($Package);
        if($Package->owner_timeline_id){
            $timeline_id=$Package->owner_timeline_id;
        }else{
            $timeline_id=$Package->timeline_id;
        }
      //  dd($timeline_id);
        $Condition_group=DB::table('condition_group')->where('language_code',Auth::user()->language)->orderby('order','asc')->get();

        return view('module.packages.setting.view-details_check')
            ->with('PackageHighlight',$PackageHighlight)
            ->with('Programs',$Programs)

            ->with('Category_arr',$category_arr)
            ->with('Category',$Cateroty)
            ->with('SubCateroty',$SubCateroty)
            ->with('PackageIn',$PackageIn)
            ->with('Datails',$Datails)
            ->with('Condition_group',$Condition_group)
            ->with('PackageSlide', $PackageSlide)
            ->with('TourDate', $TourDate)
            ->with('timeline_id', $timeline_id)
            ->with('Photo', $Photo)
            ->with('SpecialPrice', $SpecialPrice)
            ->with('Pics', $Pics)
           // ->with('Times', $Times)
            ->with('PackageTourOne', $PackageTourOne);
    }

    public function view_package($id){
        \Date::setLocale(Session::get('language'));
        Session::forget('checking');
        Session::put('event','details');
        if(!Session::get('language')){
            Session::put('language','th');
        }
        $this->update_last_event($id);
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('users_info.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')
            ->first();
            // ->toSql();
//dd($PackageTourOne);
        if($PackageTourOne) {
            $category_arr = array();
//            $pid = $PackageTourOne->packageID;
        }

        $Cate=DB::table('package_tour_category as a')
            ->select('a.TourCategoryID')
            ->where('a.packageID',$id)->get();
        // dd($Cate);
        foreach ($Cate as $rows){
            $category_arr[]=$rows->TourCategoryID;
        }

        // dd($pid);
        $PackageSlide = DB::table('highlight_in_schedule as a')
            ->join('package_location_info as b', 'b.LocationID', '=', 'a.LocationID')
            ->join('package_images as d','d.ImageID','=','b.ImageID')
           // ->where('b.LanguageCode', Session::get('Language'))
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID', $id)
            ->where('a.makeSlideshow','Y')
            ->groupby('a.LocationID')
            ->orderby('a.OrderBy', 'asc')
            ->get();
            // ->toSql();


        $GetTime=DB::table('package_times')
            ->where('LanguageCode',Session::get('language'))
            ->orderby('TimeCode','asc')->get();
        $i=0;
        foreach ($GetTime as $rows){
            $i++;
            $Times[$i]=$rows->Time_text;
        }

      //  dd($GetTime);
        $PackageIn=DB::table('package_tourin')->where('packageID',$id)->get();

        $Programs=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('a.packageID',$id)
            ->where('b.LanguageCode',Session::get('language'))
            ->orderby('a.packageDays','asc')
            ->orderby('a.packageTime','asc')
            ->get();
            // ->toSql();
            //  dd($Programs);

        $PackageHighlight=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
            ->where('b.LanguageCode',Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('packageDays','asc')
            ->get();
            // ->toSql();
            // dd($PackageHighlight);

      //  dd($PackageHighlight);
        $Datails=DB::table('package_details')
            ->where('packageID',$id)
            ->orderby('PriceSale','asc')
            ->get();
            // ->toSql();
            // dd($Datails);
        $TourDate=null;$i=1;$SpecialPrice=0;
        foreach ($Datails as $rows){
            if($i==1){
                $SpecialPrice=$rows->PriceSale;
            }
            $date=$this->setDate($rows->packageDateStart,$rows->packageDateEnd);
            $TourDate.=$date.',';
            $i++;
        }
        $Cateroty=DB::table('package_tour_category as a')
            ->join('tour_category as b','b.TourCategoryID','=','a.TourCategoryID')
            ->select('b.*')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)->first();

        $SubCateroty=DB::table('package_tour_category as a')
            ->join('tour_category_sub1 as b','b.groupID','=','a.TourSubCateID')
            ->select('b.*')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)->first();
        //dd($SubCateroty);

        $Pics = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->get();
            // ->toSql();
            // dd($Pics);
        $Photo = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->first();
            // ->toSql();
            // dd($Photo);
       // $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        if($PackageTourOne){
            Session::put('pname',$PackageTourOne->packageName);
        }

       // dd($PackageTourOne);
        Session::put('package',$id);


        return view('module.packages.setting.view-details2')
            ->with('PackageHighlight',$PackageHighlight)
            ->with('Programs',$Programs)

            ->with('Category_arr',$category_arr)
            ->with('Category',$Cateroty)
            ->with('SubCateroty',$SubCateroty)
            ->with('PackageIn',$PackageIn)
            ->with('Datails',$Datails)
            ->with('PackageSlide', $PackageSlide)
            ->with('TourDate', $TourDate)
            ->with('Photo', $Photo)
            ->with('SpecialPrice', $SpecialPrice)
            ->with('Pics', $Pics)
           // ->with('Times', $Times)
            ->with('PackageTourOne', $PackageTourOne);
    }

    public function price_select(Request $request){

        $Price=DB::table('package_details_sub')
            ->where('status','Y')
            ->where('psub_id',$request->psub_id)
            ->first();

        $Package=DB::table('package_tour')->where('packageID',$Price->packageID)->first();
        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();
        $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
            ->where('psub_id',$request->psub_id)
            ->orderby('promotion_date_start','asc')
            ->first();

        return  view('packages.ajax.show-price')
            ->with('promotion',$promotion)
            ->with('Price',$Price)
            ->with('current',$current);

    }

    public function View_details($id){
     //   dd(Auth::user()->language);
        if(Auth::check()){
            Session::put('language',Auth::user()->language);
        }
        App::setLocale(Session::get('language'));


        $getLanguage=DB::table('package_tour')->where('packageID',$id)->first();

        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select( 'a.*','b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('users_info.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')
            ->first();

        if(!$PackageTourOne){

            $PackageTourOne = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('users', 'a.packageBy', '=', 'users.id')
                ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
                ->select( 'a.*','b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', $getLanguage->packageLanguage)
                ->where('users_info.LanguageCode', Session::get('language'))
                ->where('a.packageID',$id)
                ->orderby('a.LastUpdate', 'desc')
                ->first();
        }
        //dd($PackageTourOne);


        $pid = $id;

        $Cate=DB::table('package_tour_category as a')
            ->select('a.TourCategoryID')
            ->where('a.packageID',$id)->get();
        foreach ($Cate as $rows){
            $category_arr[]=$rows->TourCategoryID;
        }

        $Cate=DB::table('package_tour_category')
            ->select('TourCategoryID')
            ->where('packageID',$id)->get();

        foreach ($Cate as $rows){
            $category[]=$rows->TourCategoryID;
        }

       // dd($pid);
        $PackageSlide = DB::table('highlight_in_schedule as a')
            ->join('package_location_info as b', 'b.LocationID', '=', 'a.LocationID')
            ->join('package_images as d','d.ImageID','=','b.ImageID')
           // ->where('b.LanguageCode', Session::get('Language'))
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID', $pid)
            ->where('a.makeSlideshow','Y')
            ->groupby('a.LocationID')
            ->orderby('a.OrderBy', 'asc')
            ->get();
            // ->toSql();
          //   dd($PackageSlide);

        $GetTime=DB::table('package_times')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('TimeCode','asc')->get();
        $i=0;$Times=array();
        foreach ($GetTime as $rows){
            $i++;
            $Times[$i]=$rows->Time_text;
        }

      //  dd($GetTime);
        $PackageIn=DB::table('package_tourin')->where('packageID',$id)->get();

        $Programs=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('a.packageID',$id)
            ->where('b.LanguageCode',Session::get('Language'))
            ->orderby('a.packageDays','asc')
            ->orderby('a.packageTime','asc')
            ->get();

        if(!$Programs){
            $Programs=DB::table('package_programs as a')
                ->join('package_program_info as b','b.programID','=','a.programID')
                ->where('a.packageID',$id)
                ->where('b.LanguageCode',$getLanguage->packageLanguage)
                ->orderby('a.packageDays','asc')
                ->orderby('a.packageTime','asc')
                ->get();
        }
            // ->toSql();
            // dd($Programs);

        $PackageHighlight=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
            ->where('b.LanguageCode',Session::get('Language'))
            ->where('a.packageID',$id)
            ->orderby('packageDays','asc')
            ->get();
        if(!$PackageHighlight){
            $PackageHighlight=DB::table('package_program_highlight as a')
                ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
                ->where('b.LanguageCode',$getLanguage->packageLanguage)
                ->where('a.packageID',$id)
                ->orderby('packageDays','asc')
                ->get();
        }

            // ->toSql();
            // dd($PackageHighlight);

        $Datails=DB::table('package_details')
            ->where('packageID',$id)
            ->orderby('PriceSale','asc')
            ->get();
            // ->toSql();
            // dd($Datails);
        $TourDate=null;$i=1;$SpecialPrice=0;
        foreach ($Datails as $rows){
            if($i==1){
                $SpecialPrice=$rows->PriceSale;
            }
            $date=$this->setDate($rows->packageDateStart,$rows->packageDateEnd);
            $TourDate.=$date.',';
            $i++;
        }

        $Cateroty=DB::table('package_tour_category as a')
            ->join('tour_category as b','b.TourCategoryID','=','a.TourCategoryID')
            ->select('b.*')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)->first();

        $Pics = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->get();
            // ->toSql();
            // dd($Pics);
        $Photo = DB::table('package_images')
            ->where('GroupImage','1')
            ->where('packageID', $id)
            ->first();

            // ->toSql();
            // dd($Photo);
            // $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
            // dd($PackageTour->packageName);

        $package_name=isset($PackageTourOne->packageName)?$PackageTourOne->packageName:'';
        Session::put('pname',$package_name);

        return view('packages.view-details')
            ->with('PackageHighlight',$PackageHighlight)
            ->with('getLanguage',$getLanguage)
            ->with('Programs',$Programs)
            ->with('PackageIn',$PackageIn)
            ->with('Category_arr',$category_arr)
            ->with('Category',$Cateroty)
            ->with('Datails',$Datails)
            ->with('PackageSlide', $PackageSlide)
            ->with('TourDate', $TourDate)

            ->with('Photo', $Photo)
            ->with('SpecialPrice', $SpecialPrice)
            ->with('Pics', $Pics)
            ->with('Times', $Times)
            ->with('PackageTourOne', $PackageTourOne);
    }


    function contat_supplier($id){
        $PackageTourOne = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*','b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.packageID',$id)
            ->orderby('a.LastUpdate', 'desc')
            ->first();

        if($PackageTourOne->package_owner=='Yes'){
            $timeline_id= $PackageTourOne->timeline_id;
        }else{
            $timeline_id= $PackageTourOne->owner_timeline_id;
        }

        $timeline=\App\Timeline::where('id',$timeline_id)->first();

        return view('packages.contact-supplier')->with('PackageTourOne',$PackageTourOne)->with('timeline',$timeline);
    }


    function setDate($St,$End){
        $date = new Date($St);
        $date2 = new Date($End);
        if($date->format('m')==$date2->format('m')){
            return $date->format('d').'-'.$date2->format('d F y');
        }else{
            return $date->format('d F').'-'.$date2->format('d F y');
        }

    }

    public function ShowBylang($id){
        Session::put('lang', $id);
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $cate = DB::table('exhibition')
            ->join('exhibition_info', 'exhibition_info.ExhibitionID', '=', 'exhibition.ExhibitionID')
            ->select('exhibition.ExhibitionID', 'exhibition.OrganizerID', 'exhibition.OrganizerFrom', 'exhibition.CreateByID', 'exhibition.Status', 'exhibition_info.Title', 'exhibition_info.LanguageCode')
            ->where('exhibition_info.LanguageCode', $id)->orderby('exhibition_info.Title', 'asc')->paginate(25);
        return view('Event-Exhibition.index')
            ->with('lang', $lang)
            ->with('langCode', $id)
            ->with('data', $cate);
    }

    public function addPic(Request $request){
        ini_set('memory_limit', '-1');
        $post = $request->all();
        $files = $request->file('file');

        if (!empty($files)) {
            $i = 0;
            foreach ($files as $file) {
                $filename = time() . '-' . $i++ . "." . $file->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($file);
                Image::make($file)->resize($ori_w, $ori_h)->save(public_path('images/event-exhibition/original/' . $filename));

                if($ori_w>=650){
                    if($ori_w>=$ori_h){
                        $new_w=650;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=480;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($file)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/small/' . $filename));

                if($ori_w>=1200){
                    if($ori_w>=$ori_h){
                        $new_w=1200;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=750;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($file)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/mid/' . $filename));

                $data = array(
                    'ExhibitionID' => $post['id'],
                    'Image' => $filename,
                    'DateUpload' => date('Y-m-d H:i:s'),
                    'LastUpdate' => date('Y-m-d H:i:s')

                );
                DB::table('exhibition_images')->insert($data);
            }
        }
        if ($post['event']=='edit') {
            return redirect('editEvent-Exhibition/' . $post['id']);
        } else {
            return redirect('createEvent-Exhibition');
        }

    }

    public function create(){
        react();
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $Package = DB::table('package_tour')->orderby('packageID', 'desc')->first();
        if (isset($Package)){
            $PackageID = $Package->packageID + 1;
        }else{
            $PackageID=1;
        }

        $Currency=DB::table('currency')
            ->where('LanguageCode','en')
            ->orderby('CurrencyName','asc')
            ->get();

        $Country=DB::table('country')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('Country','asc')
            ->get();

        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        // return view('packages.setting.create')
        return view('package.create')
            ->with('lang', $lang)
            ->with('Package',$Package)
            ->with('SiteName', $SiteName)
            ->with('Currency',$Currency)
            ->with('Country',$Country)
            ->with('PackageID', $PackageID);
    }

    public function edit_Setting($id){
        $lang = DB::table('language')
            ->orderby('LanguageName', 'asc')->get();
        $Package = DB::table('package_tour')->where('packageID',$id)->first();

        Session::put('package',$id);
        $Currency=DB::table('currency')
            ->where('LanguageCode','en')
            ->orderby('CurrencyName','asc')
            ->get();

        $Country=DB::table('country')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('Country','asc')
            ->get();



        return view('package.edit-setting')
           // ->with('LangUser', $LangUser)
            ->with('lang', $lang)
            ->with('Package',$Package)
          //  ->with('SiteName', $SiteName)
            ->with('Currency',$Currency)
            ->with('Country',$Country);
    }

    public function store(Request $request){
//        $lang=DB::table('language')->orderby('LanguageName','asc')->get();
        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'packageLanguage'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            $Package=DB::table('package_tour')->orderby('packageID','desc')->first();
            if(isset($Package)){
                $packageID=$Package->packageID+1;
            }else{
                $packageID=1;
            }


           $data = array(
                'packageID'=>$packageID,
                   // 'packageTourType'=>$post['packageTourType'],
                'packageCurrency'=>$post['packageCurrency'],
                'packageDays'=>$post['packageDays'],
                'packageNight'=>$post['packageNight'],
             //   'packagePeopleIn'=>$post['packagePeopleIn'],
                'packageLanguage'=>$post['packageLanguage'],
                'packageCreated' => date('Y-m-d h:i:s'),
                'LastUpdate' => date('Y-m-d h:i:s'),
                'packageBy' => Auth::user()->id,
                'packageStatus' => 'D'
            );
            $i= DB::table('package_tour')->insert($data);


            if(isset($post['packagePeopleIn'])){

                DB::table('package_tourin')->where('packageID',$packageID)->delete();
                foreach($post['packagePeopleIn'] as $val){
                    $Country=DB::table('countries')->where('country_id',$val)->first();

                    $data=array(
                        'packageID'=>$packageID,
                        'CountryCode'=>$Country->country_id,
                        'CountryISOCode'=>strtolower($Country->country_iso_code)
                    );
                    DB::table('package_tourin')->insert($data);

                }

            }

            $data=array(
                'packageID'=>$packageID,
                'packageName'=>'Untitled',
                'packageHighlight'=>'Untitled',
                'LanguageCode'=>$post['packageLanguage'],
                'Status_Info'=>'D'
            );
            DB::table('package_tour_info')->insert($data);

            Session::put('package',$packageID);
//            App::setLocale($post['packageLanguage']);
//            Session::put('Language',$post['packageLanguage']);
            Date::setLocale($post['packageLanguage']);
            if($i>0){
                return redirect('/package/info');
            }

        }
    }

    public function updatePackage(Request $request){

        $post=$request->all();
        $v=\Validator::make($request->all(),[
            'packageLanguage'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            $data = array(
             // 'packageTourType'=>$post['packageTourType'],
                'packageCurrency'=>$post['packageCurrency'],
                'packageDays'=>$post['packageDays'],
                'packageNight'=>$post['packageNight'],
                //'packagePeopleIn'=>$post['packagePeopleIn'],
                'packageLanguage'=>$post['packageLanguage'],
                'LastUpdate' => date('Y-m-d h:i:s')
            );
            $i= DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);

            App::setLocale($post['packageLanguage']);

            Date::setLocale($post['packageLanguage']);

            if(isset($post['packagePeopleIn'])){
                DB::table('package_tourin')->where('packageID',Session::get('package'))->delete();

                foreach($post['packagePeopleIn'] as $val){
                    $Country=DB::table('country')->where('CountryID',$val)->first();
                    $data=array(
                        'packageID'=>Session::get('package'),
                        'CountryCode'=>$Country->CountryID,
                        'CountryISOCode'=>strtolower($Country->CountryISOCode)
                    );
                    DB::table('package_tourin')->insert($data);
                }

            }

            if($i>0){
              // ****************  Delete Programs tour setup before ******************
                $countDays=DB::table('package_programs')
                    ->where('packageID',Session::get('package'))
                    ->groupby('packageDays')
                    ->isset();

                if($countDays>$post['packageDays']){
                    $check=DB::table('package_programs')->where('packageDays','>',$post['packageDays'])->get();
                    if(isset($check)){
                        foreach ($check as $rows){
                            $Img=DB::table('package_images')->where('programID',$rows->programID)->get();
                            if(isset($Img)){
                                foreach ($Img as $rowP){
                                        $fileNameDel = public_path('images/package-tour/mid/' . $rowP->Image);
                                        if (file_exists($fileNameDel) and $rowP->Image != 'default.jpg') {
                                            unlink($fileNameDel);
                                        }
                                        $fileNameDel = public_path('images/package-tour/small/' . $rowP->Image);
                                        if (file_exists($fileNameDel) and $rowP->Image != 'default.jpg') {
                                            unlink($fileNameDel);
                                        }
                                        DB::table('package_images')->where('ImageID',$rowP->ImageID)->delete();
                                }
                            }
                            DB::table('package_programs')->where('programID',$rows->programID)->delete();
                        }
                    }
                }

             // **************  Set DateStart and DateEnd In Schedule ********************
                $check=DB::table('package_details')->where('packageID',Session::get('package'))->get();

                if(isset($check)){
                    $addDate=$post['packageDays']-1;
                    foreach ($check as $rows){

                        $dateStart = str_replace('-', '/', $rows->packageDateStart);
                        $dateEnd=date('Y-m-d',strtotime($dateStart . "+$addDate days"));
                        $data=array(
                            'packageDateEnd'=>$dateEnd
                        );
                        DB::table('package_details')->where('packageDescID',$rows->packageDescID)->update($data);
                    }
                }

                return redirect('package/info');
            }

        }
    }

    public function set_type($id){
        $Type=DB::table('tour_category_sub1')
            ->where('TourCategoryID',$id)
            ->where('LanguageCode',Session::get('Language'))
            ->where('Status','Y')
            ->get();
        return Response($Type);
    }

    public function showImage(Request $request){
        if($request->ajax()){
            // $output = "";
            $Img = DB::table('package_images')
                ->where('packageID', Session::get('package'))
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\">";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                //  $output .= ' <div class="col-lg-2 col-sm-3 col-xs-6" style="margin-bottom: 20px; padding-right: 5px; padding-left: 5px; height: 150px;>';
                //  $output .= '<a style="position: absolute"  href="' . $row->image_path.'/mid/' . $row->Image . '" data-toggle="lightbox" data-gallery="imagesizes" data-title="' . $row->Image . '"  >';
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"> </li>';
            }
            $output.='</ul>';

            return Response($output);
        }
    }

    public function info(){
        Session::put('groupImage','1');
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->join('language as c','c.LanguageCode','=','a.packageLanguage')
            ->select('a.*','b.*','c.LanguageName')
            ->where('a.packageID',Session::get('package'))->first();

        $packageInfo=DB::table('package_tour_info')
             ->where('packageID',Session::get('package'))
            ->where('LanguageCode',Session::get('Language'))
            ->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        $TourCategory=DB::table('tour_category')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('TourCategoryName','asc')->get();

        $TourType=DB::table('tour_category_sub1')
            ->where('LanguageCode',Session::get('Language'))
            ->whereIn('groupID',function ($query) {
                $query->select('TourSubCateID')->from('package_tour_category')
                    ->where('packageID',Session::get('package'));
            })
            ->orderby('TourCategorySub1Name','asc')->get();
        $Photos=DB::table('highlight_in_schedule as a')
            ->join('package_location_info as c','c.LocationID','=','a.LocationID')
            ->join('package_images as b','b.ImageID','=','c.ImageID')
            ->where('c.LanguageCode',Session::get('Language'))
            ->where('a.packageID',Session::get('package'))
            ->where('a.makeSlideshow','Y')
            ->groupby('a.LocationID')
            ->get();       
        $TourImage=DB::table('package_images')
            ->where('GroupImage','1')
            ->where('programID',null)
            ->where('packageID',Session::get('package'))
            ->orderby('OrderBy','asc')
            ->first();
        $LangUser=DB::table('language')
            ->whereIn('LanguageCode',function ($query) {
                $query->select('b.LanguageCode')->from('package_tour as a')
                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
                    ->where('a.packageBy',Auth::user()->id);
            })
            ->orderby('LanguageName','asc')->get();
        return view('module.packages.setting.info')
            ->with('LangUser',$LangUser)
            ->with('packageInfo',$packageInfo)
            ->with('TourImage',$TourImage)
            ->with('Photos',$Photos)
            ->with('SiteName',$SiteName)
            ->with('TourCategory',$TourCategory)
            ->with('TourType',$TourType)
            ->with('Default',$Package);
    }

    public function saveInfo(Request $request){
        ini_set('memory_limit', '-1');
        $post=$request->all();

        $v=\Validator::make($request->all(),[
            'packageName'=>'required',
            'packageHighlight'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }else{

            if(isset($post['TourCategoryID'])){

                DB::table('package_tour_category')->where('packageID',Session::get('package'))->delete();
                foreach($post['TourCategoryID'] as $val){
                    $data=array(
                        'packageID'=>Session::get('package'),
                        'TourCategoryID'=>$val,
                    );

                    $chk=DB::table('package_tour_category')
                        ->where('packageID',Session::get('package'))
                        ->where('TourCategoryID',$val)
                        ->first();
                    if(!isset($chk)){
                       DB::table('package_tour_category')->insert($data);
                    }
                }

            }


            if(isset($post['TourSubCateID'])){
              
                foreach($post['TourSubCateID'] as $val){
                    $Type=DB::table('tour_category_sub1')->where('LanguageCode',Session::get('Language'))->where('groupID',$val)->first();

                    if(isset($Type)){

                        $check=DB::table('package_tour_category')
                            ->where('packageID',Session::get('package'))
                            ->where('TourCategoryID',$Type->TourCategoryID)
                            ->where('TourSubCateID',$Type->groupID)
                            ->first();
                        if(!isset($check)){

                            $data = array(
                                'packageID'=>Session::get('package'),
                                'TourCategoryID'=>$Type->TourCategoryID,
                                'TourSubCateID' => $val,
                            );
                            DB::table('package_tour_category')->insert($data);
                        }
                    }
                }
            }

            $check=DB::table('package_tour_info')->where('packageID',$post['id'])
                ->where('LanguageCode',Session::get('Language'))
                ->first();
            if(!isset($check)){
                $Status='D';
            }else{
                if($check->Status_Info=='P'){
                    $Status='P';
                }else{
                    $Status='S';
                }
            }
            
            $data = array(
                'packageID'=>$post['id'],
                'packageName'=>$post['packageName'],
                'packageHighlight'=>$post['packageHighlight'],
                'Status_Info'=>$Status,
                'LanguageCode'=>Session::get('Language'),
            );


            if(isset($check)){
                $i= DB::table('package_tour_info')->where('packageID',$post['id'])
                    ->where('LanguageCode',Session::get('Language'))
                    ->update($data);
            }else{
                $i= DB::table('package_tour_info')->insert($data);
            }


            if ($request->hasFile('picture')) {
                $pImage = DB::table('package_images')
                    ->where('packageID', $post['id'])
                    ->where('GroupImage','1')
                    ->orderby('OrderBy','asc')
                    ->first();
                if(isset($pImage)){
                    $fileNameDel = public_path('images/package-tour/mid/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    $fileNameDel = public_path('images/package-tour/small/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    DB::table('package_images')->where('ImageID',$pImage->ImageID)->delete();
                }

                $Pics = $request->file('picture');

                $filename = time() . "." . $Pics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($Pics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(public_path('images/package-tour/small/' . $filename));

                if ($ori_w >= 480) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 480;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 350;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(public_path('images/package-tour/x-small/' . $filename));
                $data = array(
                    'packageID' => $post['id'],
                    'image_path' => Session::get('MyImagePath')."/package-tour",
                    'GroupImage' => '1',
                    'Image' => $filename,
                    'OrderBy'=>'0'
                );
                DB::table('package_images')->insert($data);
                $data = array(
                    'Image'=>$filename
                );
                DB::table('package_tour')->where('packageID',$post['id'])->update($data);
            }
            return redirect('package/schedule');
        }
        
    }

    public function schedule(){
        if(!Session::has('days')){
            Session::put('days','1');
        }
        Session::put('groupImage','2');
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))->first();
        $Schedule=DB::table('package_programs as a')
            ->join('package_program_info as b','b.programID','=','a.programID')
            ->where('a.packageDays',Session::get('days'))
            ->where('packageID',Session::get('package'))
            ->where('b.LanguageCode',Session::get('Language'))
            ->get();
        $NameDay=DB::table('package_days')
            ->where('LanguageCode',Session::get('Language'))
            ->where('DayCode','<=',$Package->packageDays)
            ->orderby('DayCode','asc')->get();

        $Times=DB::table('package_times')
            ->where('LanguageCode',Session::get('Language'))
            ->orderby('Time_number','asc')->get();
        $HighlightToday=DB::table('package_program_highlight as a')
            ->join('package_program_highlight_info as b' ,'b.HighlightID','=','a.HighlightID')
            ->where('a.packageID',Session::get('package'))
            ->where('a.packageDays',Session::get('days'))
            ->first();

        $HighlightImage=DB::table('package_images')
            ->where('packageID',Session::get('package'))
            ->where('GroupImage','4')
            ->orderby('OrderBy','asc')
            ->first();
        $LangUser=DB::table('language')
            ->whereIn('LanguageCode',function ($query) {
                $query->select('b.LanguageCode')->from('package_tour as a')
                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
                    ->where('a.packageBy',Auth::user()->id);
            })
            ->orderby('LanguageName','asc')->get();

        return view('package.schedule')
            ->with('HighlightToday',$HighlightToday)
            ->with('HighlightImage',$HighlightImage)
            ->with('Times',$Times)
            ->with('LangUser',$LangUser)
            ->with('NameDay',$NameDay)
            ->with('Schedule',$Schedule)
            ->with('Default',$Package);
    }

    public function packagePublic($id){
        $data=array(
            'Status_Info'=>'P'
        );
        $i=DB::table('package_tour_info')->where('packageID',$id)->update($data);
        if($i>0){
            return redirect('package/details/'.$id);
        }
    }

    public function details(){
        // dd(Session::get('package'));
        App::setLocale(Session::get('language'));
        \Date::setLocale(Session::get('language'));
        Session::put('groupImage','3');
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
      //  $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))->first();

        $PackageDesc=DB::table('package_details')
            ->where('packageID',Session::get('package'))
            ->orderby('packageDateStart','asc')
            ->get();

        $LangUser=DB::table('language')
            ->whereIn('LanguageCode',function ($query) {
                $query->select('b.LanguageCode')->from('package_tour as a')
                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
                    ->where('a.packageBy',Auth::user()->id);
            })
            ->orderby('LanguageName','asc')->get();

        return view('package.details')
            ->with('LangUser',$LangUser)
            ->with('SiteName',$SiteName)
            ->with('PackageDesc',$PackageDesc)
            ->with('Default',$Package);
    }

    public function saveDetail(Request $request){
        $post = $request->all();
        // $count = sizeof($post['TourType']);
        // dd($post['TourType'][0]);
        // dd($post['TourType']);
        $v = \Validator::make($request->all(), [
            'packageDateStart' => 'required',
            'packageDateEnd' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $data = array(
                'packageID' => Session::get('package'),
                'packageDateStart' => $post['packageDateStart'],
                'packageDateEnd' => $post['packageDateEnd'],
                'Flight' => $post['Flight'],
                'Airline' => $post['Airline'],
                'NumberOfPeople' => $post['NumberOfPeople'],
                'Commission' => $post['Commission'],
            );
            $packageDescID=DB::table('package_details')->insertGetId($data);
            $count = sizeof($post['TourType']);
            for ($i=0; $i < $count; $i++) { 
                $data_sub = array(
                    'packageID' => Session::get('package'),
                    'packageDescID' => $packageDescID,
                    'TourType' => $post['TourType'][$i],
                    'PriceSale' => intval($post['PriceSale'][$i]),
                    'PriceAndTicket' => intval($post['PriceAndTicket'][$i]),
                );
                $pid=DB::table('package_details_sub')->insertGetId($data_sub);
            }
            if ($request->hasFile('picture')) {
                $pImage = DB::table('package_images')
                    ->where('packageID', Session::get('package'))
                    ->where('GroupImage', '3')
                    ->first();
                if (isset($pImage)) {
                    $fileNameDel = public_path('images/package-tour/mid/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    $fileNameDel = public_path('images/package-tour/small/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                }

                $artPics = $request->file('picture');

                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                // Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/x-small/' . $filename));

                if ($ori_w >= 480) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 480;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 350;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                // Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/x-small/' . $filename));


                $data = array(
                    'packageID' => Session::get('package'),
                    'GroupImage' => '3',
                    'Image' => $filename,
                    'OrderBy' => '1'
                );
                DB::table('package_images')->insert($data);
            }


            //********************************* Add Condition  ***********************************/

        $getCondition1=DB::table('package_condition as a')
            ->join('package_details as b','b.packageDescID','=','a.packageDescID')
            ->where('b.packageID',Session::get('package'))
            ->first();
            // ->toSql();
            // dd($getCondition1);
         if(isset($getCondition1)){

             $data=array(
                 'BookingBefore_travel'=>$getCondition1->BookingBefore_travel,
                 'BookingDeposit'=>$getCondition1->BookingDeposit,
                 'PaymentBefore'=>$getCondition1->PaymentBefore,
                 'CancelBefore'=>$getCondition1->CancelBefore,
                 'Cancellation'=>$getCondition1->Cancellation,
                 'CancellationPrice'=>$getCondition1->CancellationPrice,
                 'CancelSmallthan'=>$getCondition1->CancelSmallthan,
                 'Agepassport'=>$getCondition1->Agepassport
             );
             DB::table('package_details')->where('packageDescID',$packageDescID)->update($data);

             $getCondition=DB::table('package_condition')->where('packageDescID',$getCondition1->packageDescID)->get();
             
             if(isset($getCondition)){
                foreach ($getCondition as $rows){
                     $check=DB::table('package_condition')
                         ->orderby('conditionCode','desc')
                         ->first();
                     if(isset($check)){
                         $conditionCode=$check->conditionCode+1;
                     }else{
                         $conditionCode=1;
                     }
                     $data = array(
                         'packageDescID'=>$packageDescID,
                         'conditionCode'=>$conditionCode,
                         'packageCondition'=>$rows->packageCondition,
                         'packageConditionDesc'=>$rows->packageConditionDesc,
                         'LanguageCode'=>$rows->LanguageCode,
                     );

                     DB::table('package_condition')->insert($data);

                     $getCondition=DB::table('package_sub_condition')
                         ->where('conditionCode',$rows->conditionCode)
                         ->where('packageDescID',$rows->packageDescID)
                         ->first();
                     if(isset($getCondition)){
                         $data = array(
                             'packageDescID'=>$packageDescID,
                             'groupCode'=>$getCondition->groupCode,
                             'conditionCode'=>$conditionCode,
                             'packageConditionOrder'=>$getCondition->packageConditionOrder,
                         );

                         DB::table('package_sub_condition')->insert($data);
                     }

                 }



             }
         }
            return redirect('member/package/details');

        }
    }

    public function copy_details($id){
        $packdetail=DB::table('package_details')->where('packageDescID',$id)->first();
        if(isset($packdetail)){
            $data=array(
                'packageID'=>$packdetail->packageID,
                'packageDateStart'=>$packdetail->packageDateStart,
                'packageDateEnd'=>$packdetail->packageDateEnd,
                'Flight'=>$packdetail->Flight,
                'Airline'=>$packdetail->Airline,
                'NumberOfPeople'=>$packdetail->NumberOfPeople,
                'Commission'=>$packdetail->Commission,
                'PriceSale'=>$packdetail->PriceSale,
                'PriceAndTicket'=>$packdetail->PriceAndTicket,
                'BookingBefore_travel'=>$packdetail->BookingBefore_travel,
                'BookingDeposit'=>$packdetail->BookingDeposit,
                'PaymentBefore'=>$packdetail->PaymentBefore,
                'CancelBefore'=>$packdetail->CancelBefore,
                'Cancellation'=>$packdetail->Cancellation,
                'CancellationPrice'=>$packdetail->CancellationPrice,
                'CancelSmallthan'=>$packdetail->CancelSmallthan,
                'Agepassport'=>$packdetail->Agepassport,
                'Image'=>$packdetail->Image
             );
            $packageDescID=DB::table('package_details')->insertGetId($data);



            $getCondition=DB::table('package_condition')->where('packageDescID',$id)->get();
            if(isset($getCondition)){

                foreach ($getCondition as $rows){
                    $check=DB::table('package_condition')
                    ->orderby('conditionCode','desc')
                    ->first();
                    if(isset($check)){
                        $conditionCode=$check->conditionCode+1;
                    }else{
                        $conditionCode=1;
                    }
                    $data = array(
                        'packageDescID'=>$packageDescID,
                        'conditionCode'=>$conditionCode,
                        'packageCondition'=>$rows->packageCondition,
                        'packageConditionDesc'=>$rows->packageConditionDesc,
                        'LanguageCode'=>$rows->LanguageCode,
                    );

                    DB::table('package_condition')->insert($data);

                    $getCondition=DB::table('package_sub_condition')
                        ->where('conditionCode',$rows->conditionCode)
                        ->where('packageDescID',$rows->packageDescID)
                        ->first();
                    if(isset($getCondition)){
                         $data = array(
                             'packageDescID'=>$packageDescID,
                             'groupCode'=>$getCondition->groupCode,
                             'conditionCode'=>$conditionCode,
                             'packageConditionOrder'=>$getCondition->packageConditionOrder,
                         );

                         DB::table('package_sub_condition')->insert($data);
                    }

                }
            }
        }
        return redirect('package/details');
    }

    public function edit_details($id){
        $packdetail=DB::table('package_details')->where('packageDescID',$id)->first();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('b.LanguageCode',Session::get('Language'))
            ->where('a.packageID',Session::get('package'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        return view('package.editdetail')
            ->with('Default',$Package)
            ->with('SiteName',$SiteName)
            ->with('packdetail',$packdetail);
    }


    public function del_details($id){
        $i=DB::table('package_details')->where('packageDescID',$id)->delete();
        if($i>0){
            return redirect('member/package/details');
        }
    }

    public function edit($id,$createby){
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $Agency = DB::table('agency')->where('LanguageCode', Session::get('Language'))->orderby('agencyName', 'asc')->get();
        $Country=DB::table('country')->where('LanguageCode', Session::get('Language'))->orderby('Country','asc')->get();
        $works = DB::table('package_tour')
            ->join('package_tour_info', 'b.packageID', '=', 'a.packageID')
            ->where('a.packageID', $id)
            ->where('b.LanguageCode', Session::get('Language'))->first();
        $Info = DB::table('package_tour_category')->where('packageID', $id)->first();
        $PackageTourTypeName = DB::table('package_tour_type')->where('LanguageCode', Session::get('Language'))->orderby('PackageTourTypeName', 'asc')->get();
        $Pics = DB::table('package_tour_images')->where('packageID', $id)->get();
        $ExhiInfo = DB::table('package_tour_info')->where('packageID', $id)
            ->where('LanguageCode', Session::get('Language'))->first();
        $SiteName=DB::table('users')->where('id',$createby)->first();


        if(!Session::get('Language')){
            Session::put('Language','en');
        }
        
        return view('Package-Tour.edit')
            ->with('Country',$Country)
            ->with('SiteName', $SiteName)
            ->with('lang', $lang)
            ->with('Pics', $Pics)
            ->with('works', $works)
            ->with('Agency', $Agency)
            ->with('Info', $Info)
            ->with('PackageTourTypeName', $PackageTourTypeName)
            ->with('ExhiInfo', $ExhiInfo);
    }

    public function update(Request $request){
        ini_set('memory_limit', '-1');
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'Titleen' => 'required'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $data = array(
                'DateStart' => $post['DateStart'],
                'DateEnd' => $post['DateEnd'],
                'ZipCode' => $post['ZipCode'],
                'CountryID' => $post['CountryID'],
                'LastUpdate' => date('Y-m-d h:i:s'),
                'SlideShow' => $post['SlideShow'],
                'Status' => 'Y'

            );

            $i = DB::table('package_tour')->where('packageID' , $post['packageID'])->update($data);

            $data = array(
                'PackageTourTypeID' => $post['PackageTourTypeID']
            );
            DB::table('package_tour_category')->where('packageID', $post['packageID'])->update($data);


            $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
            foreach ($lang as $row) {
                $data = array(
                    'Title' => $post['Title' . $row->LanguageCode],
                    'Address' => $post['Address' . $row->LanguageCode],
                    'Contact' => $post['Contact' . $row->LanguageCode],
                    'Description' => $post['Description' . $row->LanguageCode],
                );
                DB::table('package_tour_info')
                    ->where([
                        ['packageID', $post['packageID']],
                        ['LanguageCode', $row->LanguageCode]
                    ])->update($data);
            }

            if ($request->hasFile('picture')) {
                $works = DB::table('package_tour')->where('packageID', $post['packageID'])->first();
                $fileNameDel = public_path('images/package-tour/original/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('images/package-tour/mid/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('images/package-tour/small/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $artPics = $request->file('picture');

                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                Image::make($artPics)->resize($ori_w, $ori_h)->save(public_path('images/package-tour/original/' . $filename));

                if($ori_w>=1200){
                    if($ori_w>=$ori_h){
                        $new_w=1200;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=750;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));


                if($ori_w>=650){
                    if($ori_w>=$ori_h){
                        $new_w=650;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=480;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/small/' . $filename));

                if($ori_w>=480){
                    if($ori_w>=$ori_h){
                        $new_w=480;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=350;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                // Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/x-small/' . $filename));


                $data = array(
                    'image_path' => Session::get('MyImagePath').'/package-tour',
                    'Image' => $filename
                );
                DB::table('package_tour')->where('packageID', $post['packageID'])->update($data);
            }

            if($i>0){
                return redirect('editPackage-Tour/'.$post['packageID'].'/'.Auth::user()->id);
            }
        }
    }

    public function editPic($id,$createby){
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $Artist = DB::table('organizer')->where('LanguageCode', Session::get('Language'))->orderby('organizerName', 'asc')->get();
        $Country=DB::table('country')->where('LanguageCode', Session::get('Language'))->orderby('Country','asc')->get();
        $works = DB::table('exhibition')->where('ExhibitionID', $id)->first();
        $Info = DB::table('exhibition_category')->where('ExhibitionID', $id)->first();
        $Artstyle = DB::table('artstyle')->where('LanguageCode', Session::get('Language'))->orderby('ArtstyleName', 'asc')->get();
        $Pics = DB::table('exhibition_images')->where('ExhibitionID', $id)->get();
        $ExhiInfo = DB::table('exhibition_info')->where('ExhibitionID', $id)->where('LanguageCode', Session::get('Language'))->first();
        $SiteName=DB::table('users')->where('id',$createby)->first();


        if(!Session::get('Language')){
            Session::put('Language','en');
        }

        return view('Event-Exhibition.edit-picture')
            ->with('Country',$Country)
            ->with('SiteName',$SiteName)
            ->with('lang', $lang)
            ->with('Pics', $Pics)
            ->with('works', $works)
            ->with('Artist', $Artist)
            ->with('Info', $Info)
            ->with('Artstyle', $Artstyle)
            ->with('ExhiInfo', $ExhiInfo);
    }

    public function updatePic(Request $request){
        ini_set('memory_limit', '-1');
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'Titleen' => 'required'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            if($post['OrganizerFrom']=='ONU'){
                $artID=$post['OrganizerID'];
            }else{
                $artID=Auth::user()->id;
            }
            //echo $artID;

            if($post['organizerName'.Session::get('Language')]){
                $artis=DB::table('organizer')->orderby('organizerID','desc')->first();
                $id=sprintf('%08d',substr($artis->organizerID,1)+1);

                $artID="O".$id;

                $data = array(
                    'organizerID' => $artID,
                    'organizerName' => $post['organizerName' . Session::get('Language')],
                    'LanguageCode' => Session::get('Language'),
                    'CreateByUserID' => Auth::user()->id,
                    'CreateDate' => date('Y-m-d h:i:s'),
                    'LastUpdateDate' => date('Y-m-d h:i:s'),
                    'Status' => 'Y'
                );
                DB::table('organizer')->insert($data);
            }


            $data = array(

                'OrganizerID' => $artID,
                'DateStart' => $post['DateStart'],
                'DateEnd' => $post['DateEnd'],
                'Email' => $post['Email'],
                'Tel' => $post['Tel'],
                'ZipCode' => $post['ZipCode'],
                'CountryID' => $post['CountryID'],
                'DateCreate' => date('Y-m-d h:i:s'),
                'Status' => $post['Status'],
                'OrganizerFrom' => $post['OrganizerFrom']

            );

            $i = DB::table('exhibition')->where('ExhibitionID' , $post['ExhibitionID'])->update($data);

            $data = array(
                'ArtstyleID' => $post['ArtstyleID'],
                'CategoryID' => $post['CategoryID']

            );
            DB::table('exhibition_category')->where('ExhibitionID', $post['ExhibitionID'])->update($data);


            $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
            foreach ($lang as $row) {
                $data = array(
                    'Title' => $post['Title' . $row->LanguageCode],
                    'Address' => $post['Address' . $row->LanguageCode],
                    'Description' => $post['Description' . $row->LanguageCode],
                );
                DB::table('exhibition_info')
                    ->where([
                        ['ExhibitionID', $post['ExhibitionID']],
                        ['LanguageCode', $row->LanguageCode]
                    ])->update($data);
            }

            if ($request->hasFile('picture')) {
                $works = DB::table('exhibition')->where('ExhibitionID', $post['ExhibitionID'])->first();
                $fileNameDel = public_path('images/event-exhibition/original/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('images/event-exhibition/mid/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $fileNameDel = public_path('images/event-exhibition/small/' . $works->Image);
                if (file_exists($fileNameDel) and $works->Image != 'default-user.png'){
                    unlink($fileNameDel);
                }
                $artPics = $request->file('picture');

                $filename = time() . "." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                Image::make($artPics)->resize($ori_w, $ori_h)->save(public_path('images/event-exhibition/original/' . $filename));

                if($ori_w>=1200){
                    if($ori_w>=$ori_h){
                        $new_w=1200;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=750;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/mid/' . $filename));


                if($ori_w>=650){
                    if($ori_w>=$ori_h){
                        $new_w=650;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=480;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/small/' . $filename));
                $data = array('Image' => $filename);
                DB::table('exhibition')->where('ExhibitionID', $post['ExhibitionID'])->update($data);
            }

            if($i>0){
                return redirect('Event-Exhibition-Picture/'.$post['ExhibitionID'].'/'.Auth::user()->id);
            }
        }
    }
   
    public function addPic2(Request $request){
        ini_set('memory_limit', '-1');
        $post = $request->all();
        $files = $request->file('file');

        if (!empty($files)) {
            $i = 0;
            foreach ($files as $file) {

                $filename = time() . '-' . $i++ . "." . $file->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($file);
                Image::make($file)->resize($ori_w, $ori_h)->save(public_path('images/event-exhibition/original/' . $filename));

                if($ori_w>=650){
                    if($ori_w>=$ori_h){
                        $new_w=650;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=480;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($file)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/small/' . $filename));

                if($ori_w>=1200){
                    if($ori_w>=$ori_h){
                        $new_w=1200;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=750;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($file)->resize($new_w, $new_h)->save(public_path('images/event-exhibition/mid/' . $filename));

                $data = array(
                    'ExhibitionID' => $post['id'],
                    'Image' => $filename,
                    'DateUpload' => date('Y-m-d H:i:s'),
                    'LastUpdate' => date('Y-m-d H:i:s')

                );
                DB::table('exhibition_images')->insert($data);
            }
        }
        if ($post['event']=='edit') {
            return redirect('editEvent-Exhibition-Picture/'. $post['id'].'/'.Auth::user()->id);
        } else {
            return redirect('createEvent-Exhibition-Picture');
        }

    }

    public function editSchedule($id,$createby){
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $works = DB::table('exhibition')->where('ExhibitionID', $id)->first();
        $ExhiSche = DB::table('exhibition_schedule')->where('ExhibitionID', $id)->where('LanguageCode', Session::get('Language'))->first();
        $ExhiInfo = DB::table('exhibition_info')->where('ExhibitionID', $id)->where('LanguageCode', Session::get('Language'))->first();
        $SiteName=DB::table('users')->where('id',$createby)->first();


        if(!Session::get('Language')){
            Session::put('Language','en');
        }

        return view('Event-Exhibition.edit-schedule')
            ->with('lang', $lang)
            ->with('SiteName', $SiteName)
            ->with('ExhiSche', $ExhiSche)
            ->with('ExhiInfo', $ExhiInfo)
            ->with('works', $works);
    }

    public function updateSchedule(Request $request){
        $post = $request->all();

            $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
            foreach ($lang as $row) {
                $data = array(
                    'Detail' => $post['Detail' . $row->LanguageCode],
                );
                DB::table('exhibition_schedule')
                    ->where([
                        ['ExhibitionID', $post['ExhibitionID']],
                        ['LanguageCode', $row->LanguageCode]
                    ])->update($data);
            }

            if($post>0){
                return redirect('editEvent-Exhibition-Schedule/'.$post['ExhibitionID'].'/'.Auth::user()->id);
            }



        }

    public function deleteImage(Request $request){
        if ($request->ajax()) {
            $image = DB::table('package_images')->where('ImageID', $request->picID)->first();
            $fileNameDel = public_path('package-tour/small/' . $image->Image);
            $fileNameDel2 = public_path('package-tour/mid/' . $image->Image);

            if (file_exists($fileNameDel) and $image->Image!='default-add.jpg') {
                unlink($fileNameDel);
            }
            if (file_exists($fileNameDel2 and $image->Image!='default-add.jpg')) {
                unlink($fileNameDel2);
            }

            DB::table('package_images')->where('ImageID', $image->ImageID)->delete();
            $check=DB::table('package_tour')->where('Image',$image->Image)->first();
            if(isset($check)){
                $image = DB::table('package_images')
                    ->where('packageID', Session::get('package'))
                    ->orderby('OrderBy','asc')
                    ->first();
                if(isset($image)){
                    $data=array(
                        'Image'=>$image->Image
                    );
                }else{
                    $data=array(
                        'Image'=>''
                    );
                }

                DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
            }

            //  $output = "";
            $Img = DB::table('package_images')
                ->where('packageID', Session::get('package'))
                ->orderby('OrderBy','asc')
                ->get();

            $output=" <ul id=\"sortable\" class=\"ui-sortable\" >";
            foreach ($Img as $row) {
                $output.="<li id=\"img-$row->ImageID\">";
                $output.'<a href="#" onclick="deleteItem(' . $row->ImageID . ')"  class="btn btn-quickview1"><i class="fa fa-times"></i>Delete</a>';
                $output .= '<img src="' . $row->image_path.'/small/' . $row->Image. '"   class="quickview1 img-thumbnail"></li>';
            }
            $output.="</ul>";
            return Response($output);
        }
    }

    public function ImageSort(Request $request){
        if($request->ajax()){
            foreach ($request->img as $key=>$value) {
                if($key==0){
                    $check=DB::table('package_location_info as a')
                        ->join('package_images as c','c.ImageID','=','a.ImageID')
                        ->where('a.LocationID',$value)
                        ->where('a.LanguageCode',Session::get('Language'))
                        ->first();
                    if(isset($check)){
                        $data=array(
                            'image_path'=>$check->image_path,
                            'Image'=>$check->Image
                        );
                        DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
                    }
                }

                $data=array(
                    'OrderBy'=>$key,
                );

                DB::table('package_images')->where('ImageID',$value)->update($data);
                if($key==0){
                    $check=DB::table('package_images')->where('ImageID',$value)->first();
                    $default=$check->image_path."/small/".$check->Image;
                    $data=array(
                        'Image'=>$check->Image,
                    );
                    DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
                }
            }

            return Response($default);
        }
    }

    public function deleteImage_bck(Request $request){
        if ($request->ajax()) {

            $Img = DB::table('exhibition_images')->where('ExhibitionImageID', $request->picID)->get();
            // var_dump($Img);
            if (isset($Img) > 0) {
                foreach ($Img as $image) {
                    $fileNameDel = public_path('images/event-exhibition/small/' . $image->Image);
                    $fileNameDel2 = public_path('images/event-exhibition/mid/' . $image->Image);
                    $fileNameDel3 = public_path('images/event-exhibition/original/' . $image->Image);
                    if (file_exists($fileNameDel) and $image->Image != 'default-user.png') {
                        unlink($fileNameDel);
                    }
                    if (file_exists($fileNameDel2) and $image->Image != 'default-user.png') {
                        unlink($fileNameDel2);
                    }
                    if (file_exists($fileNameDel3) and $image->Image != 'default-user.png') {
                        unlink($fileNameDel3);
                    }
                    DB::table('exhibition_images')->where('ExhibitionImageID', $image->ExhibitionImageID)->delete();

                }
            }
            $output = "";
            $Img = DB::table('exhibition_images')->where('ExhibitionID', $request->exhibitionID)->get();
            //var_dump($Img);
            $output .= '<Div class="col-sm-4" style="margin-bottom: 20px; padding-right: 5px; padding-left: 5px;">
                                                                <a href="#addpicture" data-toggle="modal" >
            
                                                                <img src="' . asset('public/images/default-add.jpg') . '" class=" img-responsive"></a>
                                                            </Div>';
            foreach ($Img as $row) {
                $output .= ' <div class="col-sm-4" style="margin-bottom: 20px">';
                $output .= '<a href="' . asset(public_path('images/event-exhibition/mid/' . $row->Image)) . '" data-toggle="lightbox" data-gallery="imagesizes" data-title="' . $row->Image . '"  >';
                $output .= '<img src="' . asset(public_path('images/event-exhibition/small/' . $row->Image)) . '"   class=" img-responsive"> <a href="#" onclick="deleteItem(' . $row->ExhibitionImageID . ')"  style="margin-top: -20px; margin-left: 2px;" class="btn btn-success btn-xs"><i class="fa fa-times"></i>Delete</a></a></div>';

            }
            return Response($output);


        }
    }

    public function DelImage($id){
        $Image=DB::table('package_images')->where('ImageID',$id)->first();

        if(isset($Image)){
            $fileNameDel = public_path('/images/package-tour/small/' . $Image->Image);
            if (file_exists($fileNameDel) and $Image->Image != 'default-user.png') {
                unlink($fileNameDel);
            }
            $fileNameDel = public_path('/images/package-tour/mid/' . $Image->Image);
            if (file_exists($fileNameDel) and $Image->Image != 'default-user.png') {
                unlink($fileNameDel);
            }

            $chk=DB::table('package_images')->where('ImageID',$id)->delete();


        }
        return back();
    }

    public function destroy($id){
        
        $Detail=DB::table('package_details')->where('PackageID', $id)->get();
        if(isset($Detail)){
            foreach ($Detail as $rows){
                $Condition=DB::table('package_condition')->where('packageDescID', $rows->packageDescID)->get();
                 foreach ($Condition as $rowD){
                     $Image=DB::table('package_condition_images')->where('conditionCode', $rowD->conditionCode)->get();
                     if(isset($Image)){
                         foreach ($Image as $rowM){
                             $fileNameDel = public_path('images/package-tour/small/' . $rowM->Image);
                             $fileNameDel2 = public_path('images/package-tour/mid/' . $rowM->Image);
                             //  $fileNameDel3 = public_path('images/package-tour/original/' . $image->Image);
                             if (file_exists($fileNameDel) and $rowM->Image != 'default-add.jpg') {
                                 unlink($fileNameDel);
                                 unlink($fileNameDel2);
                             }
                         }
                     }

                     DB::table('package_condition_images')->where('conditionCode', $rowD->conditionCode)->delete();

                     DB::table('package_sub_condition')->where('PackageID', $id)->delete();
                 }

                DB::table('package_condition')->where('packageDescID', $rows->packageDescID)->delete();

            }
        }

        DB::table('package_details')->where('PackageID', $id)->delete();
        DB::table('package_tour_category')->where('packageID', $id)->delete();

        $Program=DB::table('package_programs')->where('packageID', $id)->get();
        if(isset($Program)) {
            foreach ($Program as $rows){
                DB::table('package_program_info')->where('programID', $rows->programID)->delete();
            }
        }
        DB::table('package_programs')->where('packageID', $id)->delete();

        $Highlight=DB::table('package_program_highlight')->where('packageID', $id)->get();
        if(isset($Highlight)) {
            foreach ($Highlight as $rows){
                DB::table('package_program_highlight_info')->where('HighlightID', $rows->HighlightID)->delete();
            }
        }
        DB::table('package_program_highlight')->where('packageID', $id)->delete();

        DB::table('package_tourin')->where('packageID', $id)->delete();
        DB::table('package_tour_category')->where('packageID', $id)->delete();

        DB::table('package_images')->where('packageID', $id)->delete();
        DB::table('item_in_package_tour')->where('PackageTourID', $id)->delete();
        DB::table('package_tour_info')->where('packageID', $id)->delete();
        $i = DB::table('package_tour')->where('packageID', $id)->delete();
        if ($i > 0) {
            return redirect('/'.Auth::user()->id);
        }
    }

    public function settype(Request $request){
        if($request->ajax()){
            $check=DB::table('tour_category_sub1')
                ->where('LanguageCode',Session::get('Language'))
                ->where('TourCategoryID',$request->type)
                ->get();
            $data="";
          if(isset($check)){
              foreach ($check as $rows){
                  $data.="<promotion value=\"$rows->groupID\">$rows->TourCategorySub1Name</promotion>";
              }

          }
            return Response($data);
        }
    }

    public function add_cart(Request $request){
        $adult = ($request->select_adult==""?0:$request->select_adult);
        $child = ($request->select_child==""?0:$request->select_child);      
        $qty = intval($adult) + intval($child);
        $price = ($request->price==""?0:$request->price);
        $a = 0;
        if(Cart::instance('tour')->content()->isset()!=0){
            foreach(Cart::instance('tour')->content() as $row){
                if($row->id==date('Ymd').'-t'.$request->pid){
                    Cart::instance('tour')->update($row->rowId, ['child'=>$request->select_child,'adult'=>$request->select_adult, 'qty' => $qty, 'price' => $price]);
                    return redirect("/booking/order");           
                }else{
                    $a = 1;
                }
            }       
        }else{
            $a = 1;
        }
        if($a==1){
            $PackageTour = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('users', 'a.packageBy', '=', 'users.id')
            ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
            ->select('users.userURL','users.id', 'a.*','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('Language'))
            ->where('users_info.LanguageCode', Session::get('Language'))
            ->where('a.packageID',$request->pid)
            ->orderby('a.LastUpdate', 'desc')->first();
             Cart::instance('tour')->add(['id' => date('Ymd').'-t'.$request->pid, 'tid'=>$request->pid,'name' => $PackageTour->packageName,'child'=>$request->select_child,'adult'=>$request->select_adult, 'qty' => $qty, 'price' => $price, 'pic' => $PackageTour->image_path.'/'.$PackageTour->Image]);
        }
        return redirect("/booking/order");
    }

    public function update_cart($rid,$adult,$child,$price){
        $adult = ($adult==""?0:$adult);
        $child = ($child==""?0:$child);      
        $qty = intval($adult) + intval($child);
        Cart::instance('tour')->update($rid, ['child'=>$child,'adult'=>$adult, 'qty' => $qty, 'price' => $price]);
        $string = Cart::instance('tour')->total();
        $arr = explode(',', $string);
        $money = $arr[0].$arr[1];
        $tax = Cart::instance('tour')->tax();
        $tarr = explode(',', $tax);
        $tmoney = $tarr[0].$tarr[1];
        $total = $money - $tmoney;
        // return $rid.' >> '.$adult.' >> '.$child.' >> '.$price;
        return number_format($total,2);

    }

    public function order(Request $request){
        // $rowId = 'da39a3ee5e6b4b0d3255bfef95601890afd80709';        

        // echo Cart::instance('tour')->content()->isset();die();
        // dd($_POST);
        // dd(Cart::instance('tour')->content());
        // Cart::destroy('tour');
        // print_r($_POST);
        // First we'll add the item to the cart.
        // Add some items in your Controller.
        // Cart::instance('tourx')->add(Auth::user()->name.'-t'.$request->select_package, $request->select_package, $PackageTour->packageName, $request->select_child, $request->select_adult, $qty, $price, ['size' => 'medium']);
        // Cart::instance('tour')->add("Auth::user()->name.'-t'.$request->select_package", '5', 2, 3,'xxxx',5,60000, ['size' => 'large']);
        // foreach(Cart::instance('tour')->content() as $row){
        //     echo $row->name.'<br/>';
        //     echo ($row->options->has('size') ? $row->options->size : '').'<br/>';
        //     echo $row->qty.'<br/>';
        //     echo $row->price.'<br/>';
        //     echo $row->total.'<br/>';
        // }
        return view('Package-Tour.order');
        // ->with('PackageTour',$PackageTour);
        // Session::put('Language','en');
    }



    public function remove_cart($rid){
        Cart::instance('tour')->remove($rid);
        return 'success';

    }

    public function add_address(Request $request){
        $data = DB::table('users AS u')
                ->where('u.LanguageCode', Session::get('Language'))
                ->where('u.id',$request->uid)->get();
        if(sizeof($data)>0){
            DB::table('users')
            ->where('id', $request->uid)
            ->update(['name' => $request->fname,'lastname' => $request->lname,'email' => $request->email,'CityID' => $request->CityID,'StateID' => $request->state,'CountryID' => $request->country,'Zipcode' => $request->Zipcode, 'Tel' => $request->mobile, 'updated_at' => date('Y-m-d H:i:s'), 'LanguageCode' => Session::get('Language')]);
            DB::table('users_info')
                ->where('UserId', $request->uid)
                ->where('LanguageCode', Session::get('Language'))
                ->update(['FirstName' => $request->fname,'LastName' => $request->lname,'Address' => $request->address]);
        }else{
            DB::table('users')->insert(
                ['name' => $request->fname,'lastname' => $request->lname,'email' => $request->email,'CityID' => $request->CityID,'StateID' => $request->state,'CountryID' => $request->country,'Zipcode' => $request->Zipcode, 'LanguageCode' => Session::get('Language'), 'created_at' => date('Y-m-d H:i:s'), 'Tel' => $request->mobile]
            );
            $user = DB::table('users')
                ->orderBy('id','DESC')
                ->first();
            Session::push('uid',$user->id);
            DB::table('users_info')->insert(
                ['UserId' => $user->id, 'FirstName' => $request->fname,'LastName' => $request->lname,'Address' => $request->address, 'LanguageCode' => Session::get('Language')]
            );
        }
        if(!Session::get('CityName')){
            $city = DB::table('city')->where('LanguageCode', Session::get('Language'))
                ->where('CityID',$request->CityID)
                ->get();
            if(isset($city)){ 
                Session::put('CityName',$city[0]->City);
            }
        }         
        Session::put(['fname' =>$request->fname,'lname' => $request->lname,"email"=>$request->email,"company"=>$request->company,'address' => $request->address,'CityID' => $request->CityID,'StateID' => $request->state,'CountryID' => $request->country,'Zipcode' => $request->Zipcode,'tel' => $request->mobile]);
        return redirect(Session::get('Language').'/booking/billing');
    }

    public function city_name(Request $request){
        $city = DB::table('city')->where('LanguageCode', Session::get('Language'))
                ->where('City',$request->input('CityName'))
                ->get();
        $id = 0;
        if(isset($city)){
            $id = $city[0]->CityID;                
        }
        return $id;
    }

    public function address(){  
        if(Auth::guest()){
            $arr['uid'] = 0;
            $arr['fname'] = '';
            $arr['lname'] = '';
            $arr['email'] = '';
            $arr['address'] = '';
            $arr['CityID'] = '';
            $arr['StateID'] = '';
            $arr['CountryID'] = '';
            $arr['CitySub1ID'] = '';
            $arr['Zipcode'] = '';
            $arr['tel'] = '';
            $arr['CityName'] = '';
            $Account = 0;
        }else{
            $id = Auth::user()->id;
            $data = DB::table('users AS u')
                ->join('users_info AS uf', 'u.id', '=', 'uf.UserID')
                ->join('country AS c', 'u.CountryID', '=', 'c.CountryID')
                ->join('state AS s', function($join){
                    $join->on('u.StateID', '=', 's.StateID');
                    $join->on('u.CountryID','=','s.CountryID');
                 })
                ->where('u.LanguageCode', Session::get('Language'))
                ->where('uf.LanguageCode', Session::get('Language'))
                ->where('c.LanguageCode', Session::get('Language'))
                ->where('s.LanguageCode', Session::get('Language'))
                ->where('u.id',$id)
                // ->toSql();
                // dd($data);
                ->get();
            $arr['uid'] = $id;
            $arr['fname'] = $data[0]->name;
            $arr['lname'] = $data[0]->lastname;
            $arr['email'] = $data[0]->email;
            $arr['address'] = $data[0]->Address;
            $arr['CityID'] = $data[0]->CityID;
            $arr['StateID'] = $data[0]->StateID;
            $arr['State'] = $data[0]->State;
            $arr['CountryID'] = $data[0]->CountryID;
            $arr['Country'] = $data[0]->Country;
            $arr['CitySub1ID'] = $data[0]->CitySub1ID;
            $arr['Zipcode'] = $data[0]->Zipcode;
            $arr['tel'] = $data[0]->Tel; 
            Session::put(['fname' => $arr['fname'],'lname' => $arr['lname'],"email"=>$arr['email'],'address' => $arr['address'],'CityID' => $arr['CityID'],'StateID' => $arr['StateID'],'CountryID' => $arr['CountryID'],'CitySub1ID' => $arr['CitySub1ID'],'Zipcode' => $arr['Zipcode'],'tel' => $arr['tel'],'Country' => $arr['Country'],'State' => $arr['State']]);
            $city = DB::table('city')->where('LanguageCode', Session::get('Language'))
                ->where('CityID',$arr['CityID'])
                ->get();
            if(isset($city)){
                $arr['CityName'] = $city[0]->City; 
                Session::put('CityName',$arr['CityName']);
            }            
            
            $Account=DB::table('users')
            ->join('users_info', 'users_info.UserID', '=', 'users.id')
            ->where('users.userURL', $id)
            ->where('users_info.LanguageCode', Session::get('Language'))
            ->first();
            if(!isset($Account)){
                $Account=DB::table('users')
                    ->join('users_info', 'users_info.UserID', '=', 'users.id')
                    ->orwhere('users.id', $id)
                    ->first();
            }          
        }
        $State=DB::table('state')->where('LanguageCode', Session::get('Language'))
            ->where('State','!=','')
            ->groupby('State')
            ->orderby('StateID','asc')->get();
        $Country=DB::table('country')
        ->where('LanguageCode', Session::get('Language'))
        ->orderby('Country','asc')
        ->get();
        return view('Package-Tour.address')
        ->with('data',$arr)
        ->with('State',$State)
        ->with('Account',$Account)
        ->with('Country',$Country);
    }

    public function billing(Request $request){
        $Account = 0;
        $State=DB::table('state')->where('LanguageCode', Session::get('Language'))
            ->where('State','!=','')
            ->groupby('State')
            ->orderby('StateID','asc')->get();
        $Country=DB::table('country')
        ->where('LanguageCode', Session::get('Language'))
        ->orderby('Country','asc')
        ->get();
        return view('Package-Tour.billing')
        ->with('State',$State)
        ->with('Account',$Account)
        ->with('Country',$Country);
    }

    public function shipping(Request $request){
        return view('Package-Tour.shipping');
    }

    public function payment(Request $request){        
        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;
        $data['email'] = $request->mail;
        $data['address'] = $request->address;
        $data['address2'] = $request->address2; 
        $data['company'] = $request->company; 
        $data['city_id'] = $request->CityID; 
        $data['state_id'] = $request->state; 
        $data['country_id'] = $request->country;
        $data['zipcode'] = $request->Zipcode; 
        $data['tel'] = $request->mobile; 
        $data['info'] = $request->information;               
        $data['language_code'] = Session::get('Language');
        // dd($data);
        $chk=DB::table('billing')->where('created_by', Auth::user()->id)->get();
        // dd($chk);
        if(sizeof($chk)){
            $data['updated_at'] = date('Y-m-d H:i:s');
            $State=DB::table('billing')->where('language_code', Session::get('Language'))->update($data);
        }else{
            $data['website'] = $_SERVER['SERVER_NAME']; 
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = Auth::user()->id;
            // dd($data);
            DB::table('billing')->insert($data);
            // DB::table('billing')->insert(
            //     [
            //         'fname' => $request->fname,
            //         'lname' => $request->lname,
            //         'email' => $request->mail,
            //         'address' => $request->address,
            //         'address2' => $request->address2, 
            //         'city_id' => $request->CityID, 
            //         'state_id' => $request->state, 
            //         'country_id' => $request->country,
            //         'zipcode' => $request->Zipcode, 
            //         'tel' => $request->mobile, 
            //         'info' => $request->alias,
            //         'website' => $_SERVER['SERVER_NAME'], 
            //         'created_at' => date('Y-m-d H:i:s'),
            //         'created_by' => Auth::user()->id,
            //         'language_code' => Session::get('Language'),
            //     ]
            // );
        }        

        return view('Package-Tour.payment');
    }

    public function confirm_order(){
        return view('Package-Tour.confirm');
    }

    public function booking_insert(Request $request){
        return view('Package-Tour.confirm');
        // dd($_POST);
        // dd(Cart::instance('tour')->content());
        // foreach(Cart::instance('tour')->content() as $row){
        //     $price = $row->qty * $row->price;
        //     DB::table('booking')->insert(
        //             ['UserId' => Session::get('uid'),'tid' => $row->tid,'adult' => $row->adult,'child' => $row->child,'price' => $price, 'start_tour' => date('Y-m-d'), 'end_tour' => date('Y-m-d'), 'created_at' => date('Y-m-d H:i:s')]
        //     );
        // }

        // return redirect(Session::get('Language').'/confirm/order');
    }
}
