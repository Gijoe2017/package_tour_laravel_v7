<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use App\Country;
use App\State;
use App\CitySub1;
use App\City;
use App;
use Illuminate\Pagination\Paginator;
use Route;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\models\Occupation;

use Illuminate\Support\Facades\Input;
use Mail;
use File;
use Image;
use Validator;
use Response;
use URL;
use Session;


class OrderMemberController extends Controller
{

//    public function __construct()
//    {
//
//    }

    public function dashboard($id){
        Session::forget('passport_id');
        Session::forget('checking');
        $Timelines=DB::table('timelines as a')
            ->join('locations as b','b.timeline_id','=','a.id')
            ->join('location_user as c','c.location_id','=','b.id')
            ->select('a.id','a.name')
            ->whereIn('b.timeline_id',function ($query) {
               $query->select('timeline_id')->from('business_verified_modules')
                   ->where('business_verified_status','verified')
                   ->where('module_type_id','1');
               })
            ->where('c.user_id',Auth::user()->id)
            ->get();

        $timeline=array();
        if($Timelines){
            foreach ($Timelines as $rows){
                $timeline[$rows->id]=$rows->name;
                $timelineID[]=$rows->id;
            }
            $timeline_options = ['' => 'Select Category'] + $timeline;
            Session::put('timeline_options',$timeline_options);
        }
       // dd($timelineID);
        Session::put('timeline_id',$id);
        Session::put('represent_timeline_id',$id);

        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->get();

      //  dd($Packages->count());

        Session::put('Packages',$Packages->count());
        Session::put('module_id',null);
        Session::put('Timelines',$Timelines);
        Session::put('language',Auth::user()->language);

        $bgColor=array(
            '1'=>'bg-green',
            '2'=>'bg-yellow',
            '3'=>'bg-blue',
            '4'=>'bg-red'
        );

        return view('member.dashboard')
            ->with('bgColor',$bgColor);
    }

    public function change_language($id){
        $MemberLang=DB::table('users_passport_info')
            ->where('passport_id',$id)
            ->groupby('language_code')
            ->get();
       // dd($MemberLang);
        return view('member.forms2.show-lang')
            ->with('MemberLang',$MemberLang)
            ->with('passport_id',$id);
    }

    public function show_address_book(){
        $AddressBook=DB::table('address_book')
            ->where('address_type','1')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->get();
       // dd($AddressBook);
        return view('member.account.show_address')->with('AddressBook',$AddressBook);
    }

    public function show_invoice_address_book(){
        $AddressBook=DB::table('address_book')
            ->where('address_type','2')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->get();

        return view('member.account.show_invoice_address')->with('AddressBook',$AddressBook);
    }

    public function set_address_book(Request $request){
        DB::table('address_book')
            ->where('timeline_id',Auth::user()->timeline_id)
            ->where('address_type',$request->type)
            ->update(['default_address'=>'0']);

        $chk= DB::table('address_book')->where('address_book_id',$request->address_id)->update(['default_address'=>'1']);
        if($chk>0){
            return 'Update Success!';
        }else{
            return 'Update problem!';
        }

    }

    public function address_book(){
        return view('member.account.create_address');
    }

    public function edit_address_book($id){
        $Address=DB::table('address_book')->where('address_book_id',$id)->first();
        return view('member.account.edit_address')->with('Address',$Address);
    }
    public function edit_invoice_address_book($id){
        $Address=DB::table('address_book')->where('address_book_id',$id)->first();
        return view('member.account.edit_invoice_address')->with('Address',$Address);
    }

    public function more_address_book(){
        return view('member.account.create_more_address');
    }

    public function more_invoice_address_book(){
        return view('member.account.create_more_invoice_address');
    }

    public function booking_address_store(Request $request)
    {
        $city_sub=array();
        $city=array();
        $states=array();

        $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
        if (!$country) {
            $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
        }

        if (isset($request->state_id)){
            $states = DB::table('states')
                ->where('country_id', $request->country_id)
                ->where('state_id', $request->state_id)
                ->where('language_code', Auth::user()->language)
                ->first();
                if (!$states) {
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', 'en')
                        ->first();
                }
                if(isset($request->city_id)){
                    $city = DB::table('cities')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('city_id', $request->city_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();

                    if (!$city) {
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            //->where('state_id',$AddressBook->entry_state)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_sub1_id)){
                        $city_sub = DB::table('cities_sub1')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('city_sub1_id', $request->city_sub1_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city_sub) {
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                    }

                }

        }

        $address_show=$request->address.' ';
        if($city_sub){
            $address_show.=$city_sub->city_sub1_name.'<BR>';
        }
        if($city){
            $address_show.=$city->city.' ';
        }
        if($states){
            $address_show.=$states->state.', ';
        }
        $address_show.=$country->country.' '.$request->zip_code;

        $data=array(
            'timeline_id'=>Auth::user()->timeline_id,
            'address_type'=>'1',
            'default_address'=>'1',
            'entry_company'=>$request->entry_firstname,
            'entry_firstname'=>$request->entry_firstname,
            'entry_lastname'=>$request->entry_lastname,
            'entry_phone'=>$request->tax_phone,
            'entry_email'=>$request->tax_email,
            'entry_street_address'=>$request->address,
            'entry_country_id'=>$request->country_id,
            'entry_state'=>$request->state_id,
            'entry_city'=>$request->city_id,
            'entry_city_sub'=>$request->city_sub1_id,
            'entry_postcode'=>$request->zip_code,
            'address_show'=>$address_show,
        );

        $check=DB::table('address_book')->insert($data);

        if($check){
            return redirect('booking/continuous/invoice');
        }else{
            return back();
        }
    }

    public function booking_address_update(Request $request)
    {
        $city_sub=array();
        $city=array();
        $states=array();

        $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
        if (!$country) {
            $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
        }

        if (isset($request->state_id)){
            $states = DB::table('states')
                ->where('country_id', $request->country_id)
                ->where('state_id', $request->state_id)
                ->where('language_code', Auth::user()->language)
                ->first();
                if (!$states) {
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', 'en')
                        ->first();
                }
                if(isset($request->city_id)){
                    $city = DB::table('cities')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('city_id', $request->city_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();

                    if (!$city) {
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            //->where('state_id',$AddressBook->entry_state)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_sub1_id)){
                        $city_sub = DB::table('cities_sub1')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('city_sub1_id', $request->city_sub1_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city_sub) {
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                    }

                }

        }

        $address_show=$request->address.' ';
        if($city_sub){
            $address_show.=$city_sub->city_sub1_name.'<BR>';
        }
        if($city){
            $address_show.=$city->city.' ';
        }
        if($states){
            $address_show.=$states->state.', ';
        }
        $address_show.=$country->country.' '.$request->zip_code;

        $data=array(
            'timeline_id'=>Auth::user()->timeline_id,
            'address_type'=>'1',
            'default_address'=>'1',
            'entry_company'=>$request->entry_firstname,
            'entry_firstname'=>$request->entry_firstname,
            'entry_lastname'=>$request->entry_lastname,
            'entry_phone'=>$request->tax_phone,
            'entry_email'=>$request->tax_email,
            'entry_street_address'=>$request->address,
            'entry_country_id'=>$request->country_id,
            'entry_state'=>$request->state_id,
            'entry_city'=>$request->city_id,
            'entry_city_sub'=>$request->city_sub1_id,
            'entry_postcode'=>$request->zip_code,
            'address_show'=>$address_show,
        );

        $check=DB::table('address_book')->where('address_book_id',$request->address_book_id)->update($data);

        if($check){
            return redirect('booking/show/address');
        }else{
            return back();
        }
    }

    public function booking_invoice_address_update(Request $request)
    {
        $city_sub=array();
        $city=array();
        $states=array();

        $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
        if (!$country) {
            $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
        }

        if (isset($request->state_id)){
            $states = DB::table('states')
                ->where('country_id', $request->country_id)
                ->where('state_id', $request->state_id)
                ->where('language_code', Auth::user()->language)
                ->first();
                if (!$states) {
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', 'en')
                        ->first();
                }
                if(isset($request->city_id)){
                    $city = DB::table('cities')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('city_id', $request->city_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();

                    if (!$city) {
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            //->where('state_id',$AddressBook->entry_state)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_sub1_id)){
                        $city_sub = DB::table('cities_sub1')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('city_sub1_id', $request->city_sub1_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city_sub) {
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                    }

                }

        }

        $address_show=$request->address.' ';
        if($city_sub){
            $address_show.=$city_sub->city_sub1_name.'<BR>';
        }
        if($city){
            $address_show.=$city->city.' ';
        }
        if($states){
            $address_show.=$states->state.', ';
        }
        $address_show.=$country->country.' '.$request->zip_code;

        $data=array(
            'timeline_id'=>Auth::user()->timeline_id,
            'address_type'=>'2',
            'default_address'=>'1',
            'entry_company'=>$request->entry_firstname,
            'entry_firstname'=>$request->entry_firstname,
            'entry_lastname'=>$request->entry_lastname,
            'entry_phone'=>$request->tax_phone,
            'entry_email'=>$request->tax_email,
            'entry_street_address'=>$request->address,
            'entry_country_id'=>$request->country_id,
            'entry_state'=>$request->state_id,
            'entry_city'=>$request->city_id,
            'entry_city_sub'=>$request->city_sub1_id,
            'entry_postcode'=>$request->zip_code,
            'address_show'=>$address_show,
        );

        $check=DB::table('address_book')->where('address_book_id',$request->address_book_id)->update($data);

        if($check){
            return redirect('booking/show/invoice_address');
        }else{
            return back();
        }
    }

    public function booking_more_address_store(Request $request)
    {
        $city_sub=array();
        $city=array();
        $states=array();

        $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
        if (!$country) {
            $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
        }

        if (isset($request->state_id)){
            $states = DB::table('states')
                ->where('country_id', $request->country_id)
                ->where('state_id', $request->state_id)
                ->where('language_code', Auth::user()->language)
                ->first();
                if (!$states) {
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', 'en')
                        ->first();
                }
                if(isset($request->city_id)){
                    $city = DB::table('cities')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('city_id', $request->city_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();

                    if (!$city) {
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            //->where('state_id',$AddressBook->entry_state)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_sub1_id)){
                        $city_sub = DB::table('cities_sub1')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('city_sub1_id', $request->city_sub1_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city_sub) {
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                    }

                }

        }

        $address_show=$request->address.' ';
        if($city_sub){
            $address_show.=$city_sub->city_sub1_name.'<BR>';
        }
        if($city){
            $address_show.=$city->city.' ';
        }
        if($states){
            $address_show.=$states->state.', ';
        }
        $address_show.=$country->country.' '.$request->zip_code;

        DB::table('address_book')->where('timeline_id',Auth::user()->timeline_id)->where('address_type','1')->update(['default_address'=>'0']);

        $data=array(
            'timeline_id'=>Auth::user()->timeline_id,
            'address_type'=>'1',
            'default_address'=>'1',
            'entry_company'=>$request->entry_firstname,
            'entry_firstname'=>$request->entry_firstname,
            'entry_lastname'=>$request->entry_lastname,
            'entry_phone'=>$request->tax_phone,
            'entry_email'=>$request->tax_email,
            'entry_street_address'=>$request->address,
            'entry_country_id'=>$request->country_id,
            'entry_state'=>$request->state_id,
            'entry_city'=>$request->city_id,
            'entry_city_sub'=>$request->city_sub1_id,
            'entry_postcode'=>$request->zip_code,
            'address_show'=>$address_show,
        );

        $check=DB::table('address_book')->insert($data);

        if($check){
            return redirect('booking/show/address');
        }else{
            return back();
        }
    }

    public function booking_more_invoice_address_store(Request $request)
    {
        $city_sub=array();
        $city=array();
        $states=array();

        DB::table('address_book')->where('timeline_id',Auth::user()->timeline_id)->where('address_type','2')->update(['default_address'=>'0']);

        $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', Auth::user()->language)->first();
        if (!$country) {
            $country = DB::table('country')->where('country_id', $request->country_id)->where('language_code', 'en')->first();
        }

        if (isset($request->state_id)){
            $states = DB::table('states')
                ->where('country_id', $request->country_id)
                ->where('state_id', $request->state_id)
                ->where('language_code', Auth::user()->language)
                ->first();
                if (!$states) {
                    $states = DB::table('states')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('language_code', 'en')
                        ->first();
                }
                if(isset($request->city_id)){
                    $city = DB::table('cities')
                        ->where('country_id', $request->country_id)
                        ->where('state_id', $request->state_id)
                        ->where('city_id', $request->city_id)
                        ->where('language_code', Auth::user()->language)
                        ->first();

                    if (!$city) {
                        $city = DB::table('cities')
                            ->where('country_id', $request->country_id)
                            //->where('state_id',$AddressBook->entry_state)
                            ->where('city_id', $request->city_id)
                            ->where('language_code', 'en')
                            ->first();
                    }
                    if(isset($request->city_sub1_id)){
                        $city_sub = DB::table('cities_sub1')
                            ->where('country_id', $request->country_id)
                            ->where('state_id', $request->state_id)
                            ->where('city_id', $request->city_id)
                            ->where('city_sub1_id', $request->city_sub1_id)
                            ->where('language_code', Auth::user()->language)
                            ->first();

                        if (!$city_sub) {
                            $city_sub = DB::table('cities_sub1')
                                ->where('country_id', $request->country_id)
                                ->where('state_id', $request->state_id)
                                ->where('city_id', $request->city_id)
                                ->where('city_sub1_id', $request->city_sub1_id)
                                ->where('language_code', 'en')
                                ->first();
                        }
                    }

                }

        }

        $address_show=$request->address.' ';
        if($city_sub){
            $address_show.=$city_sub->city_sub1_name.'<BR>';
        }
        if($city){
            $address_show.=$city->city.' ';
        }
        if($states){
            $address_show.=$states->state.', ';
        }
        $address_show.=$country->country.' '.$request->zip_code;

        $data=array(
            'timeline_id'=>Auth::user()->timeline_id,
            'address_type'=>'2',
            'default_address'=>'1',
            'entry_company'=>$request->entry_firstname,
            'entry_firstname'=>$request->entry_firstname,
            'entry_lastname'=>$request->entry_lastname,
            'entry_phone'=>$request->tax_phone,
            'entry_email'=>$request->tax_email,
            'entry_street_address'=>$request->address,
            'entry_country_id'=>$request->country_id,
            'entry_state'=>$request->state_id,
            'entry_city'=>$request->city_id,
            'entry_city_sub'=>$request->city_sub1_id,
            'entry_postcode'=>$request->zip_code,
            'address_show'=>$address_show,
        );

        $check=DB::table('address_book')->insert($data);

        if($check){
            return redirect('booking/show/invoice_address');
        }else{
            return back();
        }
    }

    public function create($id){
        Session::forget('member_info');
        Session::put('tour_type',$id);
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        return view('packages.order_member.forms2.general')->with('country',$country);
    }

    public function create_start($id){
        Session::forget('member_info');
        Session::put('BookingID',$id);
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        return view('packages.order_member.forms2.general')->with('country',$country);
    }

    public function save_step1(Request $request){

        $post=$request->all();
        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {
           return redirect()->back()->withErrors($v->errors());
        } else {
            $identification_id=str_replace('-','',$post['identification_id']);
            $data = array(
                'identification_id' => $identification_id,
                'user_title_id' => $post['user_title_id'],
                'sex_id' => $post['sex_id'],
                'marital_status_id' => $post['marital_status_id'],
                'date_of_birth' => date('Y-m-d',strtotime($post['date_of_birth'])),
                'religion_id' => $post['religion_id'],
                'origin_id' => $post['origin_id'],
                'country_of_birth_id' => $post['country_of_birth_id'],
                'current_nationality_id' => $post['current_nationality_id'],
                'naturalization_id' => $post['naturalization_id'],
                'user_status'=>'D',
                'timeline_id'=>Auth::user()->timeline_id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
          //  dd($data);
            $passport_id=DB::table('users_passport')->insertGetId($data);

            $data = array(
                'passport_id' => $passport_id,
                'identification_id' => $identification_id,
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'language_code' => Auth::user()->language,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
            DB::table('users_passport_info')->insert($data);

            $Tourist=DB::table('package_tourist_member')
                ->where('booking_id',Session::get('BookingID'))
                ->first();
            if($Tourist){
                $order=$Tourist->orderby+1;
            }else{
                $order=1;
            }

            $check=DB::table('package_booking_details')
                ->where('booking_id',Session::get('BookingID'))
                ->whereNotIn('booking_detail_id',function ($query){
                    $query->select('booking_detail_id')->from('package_tourist_member')->where('booking_id',Session::get('BookingID'));
                })
                ->first();
            if(!$check){
                $check=DB::table('package_booking_details')
                    ->where('booking_id',Session::get('BookingID'))
                    ->first();
            }
           // dd($check);
            $data=array(
                'passport_id'=>$passport_id,
                'booking_id'=>Session::get('BookingID'),
                'booking_detail_id'=>$check->booking_detail_id,
                'tour_type'=>Session::get('tour_type'),
                'orderby'=>$order,
            );

            DB::table('package_tourist_member')->insert($data);

            Session::put('passport_id',$passport_id);

            if ($request->hasFile('passport_cover_image')) {
//                $dir=base_path() . '/public/images/member/cover/';
                $dir=public_path('/images/member/cover/') ;

//                $dir='/images/member/cover/' ;
                $artPics = $request->file('passport_cover_image');
                $filename = $identification_id .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('passport_cover_image' => $filename);
                DB::table('users_passport')->where('passport_id', $passport_id)->update($data);
            }
           // dd($data);
        }
        Session::put('member_info','( '.$post['first_name'].' '.$post['last_name'].' )');
        return redirect('package/member/step2');
    }

    public function EditStep1(){

        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();
       // dd($Member);
        $event="";
        if(!isset($Member)){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
            $event='change';
        }

        return view('packages.order_member.forms2.edit_step1')
            ->with('event',$event)
            ->with('country',$country)
            ->with('Member',$Member);
    }

    public function update_step1(Request $request){
        $post=$request->all();
//        Session::put('passport_id',$post['passport_id']);
       // dd(Session::get('passport_id'));
        $v = \Validator::make($request->all(), [
            'first_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $identification_id=str_replace('-','',$post['identification_id']);

                $data = array(
                    'identification_id' => $identification_id,
                    'user_title_id' => $post['user_title_id'],
                    'sex_id' => $post['sex_id'],
                    'marital_status_id' => $post['marital_status_id'],
//                    'date_of_birth' => $post['date_of_birth'],
                    'date_of_birth' => date('Y-m-d',strtotime($post['date_of_birth'])),
                    'religion_id' => $post['religion_id'],
                    'origin_id' => $post['origin_id'],
                    'country_of_birth_id' => $post['country_of_birth_id'],
                    'current_nationality_id' => $post['current_nationality_id'],
                    'naturalization_id' => $post['naturalization_id'],
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                DB::table('users_passport')
                    ->where('passport_id',Session::get('passport_id'))
                    ->update($data);



            if ($request->hasFile('passport_cover_image')) {
                $Member=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->first();
                $dir=public_path('/images/member/cover/') ;
                if($Member->passport_cover_image){
                    $fileNameDel = $dir . $Member->passport_cover_image;
                    if (file_exists($fileNameDel)) {
                        unlink($fileNameDel);
                    }
                }
                $artPics = $request->file('passport_cover_image');
                $filename = $identification_id .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('passport_cover_image' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }

            $Check=DB::table('users_passport_info')
                ->where('passport_id',Session::get('passport_id'))
                ->where('language_code',Auth::user()->language)
                ->first();

            $data = array(
                'passport_id' => Session::get('passport_id'),
                'identification_id' => $identification_id,
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'middle_name' => $post['middle_name'],
                'language_code' => Auth::user()->language,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by_user_id' => Auth::user()->id,
                'created_by_site_id' => URL::current(),
            );
            if($Check){
                DB::table('users_passport_info')
                    ->where('passport_id',Session::get('passport_id'))
                    ->where('language_code',Auth::user()->language)
                    ->update($data);
            }else{
                DB::table('users_passport_info')->insert($data);
            }

            // dd($data);
        }

        if($request->continue=='Yes'){
            return redirect('package/member/edit-step2/');
        }else{

            Session::flash('message','Record has been update success!');
            return redirect()->back();
        }

    }

    public function create_step2(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        return view('packages.order_member.forms2.step2')->with('country',$country);
    }

    public function save_step2(Request $request){
        $post=$request->all();

        $v = \Validator::make($request->all(), [
            'passport_number' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $passport_number=str_replace('-','',$post['passport_number']);
          //  dd($passport_number);
            $data=array(
                'passport_country_id' => $post['passport_country_id'],
                'passport_number' => $passport_number,
                'passport_DOI' => $post['passport_DOI'],
                'passport_DOE' => $post['passport_DOE'],
            );
            DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->update($data);

            if ($request->hasFile('photo_for_visa')) {
//                $dir=base_path() . '/images/member/visa/';
                $dir=public_path('/images/member/visa/') ;
                $artPics = $request->file('photo_for_visa');
                $filename = $passport_number .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('photo_for_visa' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }
        }
        return redirect('package/member/contact');
    }

    public function EditStep2($id=null){
        if($id){
            Session::put('passport_id',$id);
        }
        $country=App\Country::where('language_code',Auth::user()->language)->get();

        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();
        if(!$Member){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
        }

        //dd($Member);
        return view('packages.order_member.forms2.edit_step2')
            ->with('passport_id',$id)
            ->with('country',$country)
            ->with('Member',$Member);
    }

    public function update_step2(Request $request){
        $post=$request->all();

       // dd($post);

        $v = \Validator::make($request->all(), [
            'passport_number' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $passport_number=str_replace('-','',$post['passport_number']);
            //  dd($passport_number);
            $data=array(
                'passport_country_id' => $post['passport_country_id'],
                'passport_number' => $passport_number,
                'passport_DOI' => date('Y-m-d',strtotime($post['passport_DOI'])),
                'passport_DOE' => date('Y-m-d',strtotime($post['passport_DOE'])),
            );
           // dd($data);
            $check=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->update($data);

            if ($request->hasFile('photo_for_visa')) {
//                $dir=base_path() . '/images/member/visa/';
                $dir=public_path('/images/member/visa/') ;
                $Member=DB::table('users_passport')->where('passport_id',Session::get('passport_id'))->first();

                if($Member->passport_cover_image){
                    $fileNameDel = $dir . $Member->photo_for_visa;
                    if (file_exists($fileNameDel)) {
                        unlink($fileNameDel);
                    }
                }

                $artPics = $request->file('photo_for_visa');
                $filename = $passport_number .'-'.time() ."." . $artPics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($artPics);
                if($ori_w>=450){
                    if($ori_w>=$ori_h){
                        $new_w=450;
                        $new_h=round(($new_w/$ori_w)*$ori_h);
                    }else{
                        $new_h=380;
                        $new_w=round(($new_h/$ori_h)*$ori_w);
                    }
                }else{
                    $new_w=$ori_w;
                    $new_h=$ori_h;
                }
                Image::make($artPics)->resize($new_w, $new_h)->save($dir. $filename);
                $data = array('photo_for_visa' => $filename);
                DB::table('users_passport')->where('passport_id', Session::get('passport_id'))->update($data);
            }

        }

           // dd($post['event']);
            if($post['event']=='continue'){
                return redirect('package/member/edit-step3/'.$post['passport_id']);
            }else{
                Session::flash('message','Record has been update success!');
                return redirect()->back();
            }




    }

    public function contact(){
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();
        $Phones=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('packages.order_member.forms2.address')
            ->with('Phones',$Phones)
            ->with('Email',$Email)
            ->with('AddressAll',$AddressAll)
            ->with('country',$country);
    }

    public function delete_address(Request $request){
        Session::put('passport_id',$request->passport_id);
        DB::table('users_passport_address1')->where('address_id',$request->address_id)->delete();
        DB::table('users_passport_address2')->where('address_id',$request->address_id)->delete();

        $Address=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();

        $output = "<table class=\"table table-bordered\">
                    <thead>
                    <tr>
                        <th>" . trans('profile.AddressType') . "</th>
                        <th>" . trans('profile.Address') . "</th>
                        <th>" . trans('profile.Zip_code') . "</th>
                        <th>" . trans('profile.action') . "</th>
                    </tr>
                    </thead>
                    <tbody>";
        foreach ($Address as $rows){
            $address=$rows->address.' '.$rows->additional.' '.$rows->country;
            $State=DB::table('states')->where(['state_id'=>$rows->state_id,'language_code'=>Auth::user()->language])->first();
            $City=DB::table('cities')->where(['city_id'=>$rows->city_id,'country_id'=>$rows->country_id,'language_code'=>Auth::user()->language])->first();
            if($State){
                $state=$State->state.', ';
            }
            if($City){
                $city=$City->city.' ';
            }
            $output .= "<tr>
                            <td>" . $rows->address_type . "</td>
                            <td>" . $address .$state.$city.$rows->country. "</td>
                            <td>" . $rows->zip_code . "</td>
                            <td class='text-right'>
                                <a class=\"btn btn-sm btn-warning\" href=\"".url('/member/edit-address/'.$rows->address_id)."\" ><i class=\"fa fa-edit\"></i> ".trans('profile.Edit')."</a>
                                <button class=\"btn btn-sm btn-danger order_delete_address\" data-id='".$rows->address_id."'><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')."</button>
                            </td>
                        </tr>";
        }
        $output.=" </tbody></table>";

        return Response($output);
    }

    public function add_address(Request $request){
        $state = "";
        $city = "";
        $citySub = "";
        $zip_code = '';

        Session::put('passport_id',$request->passport_id);

        if (isset($request->state_id)) {
            $state = $request->state_id;
        }
        if (isset($request->city_id)) {
            $city = $request->city_id;
        }
        if (isset($request->city_sub1_id)) {
            $citySub = $request->city_sub1_id;
            $zip_code = $request->zip_code;
        }
        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address_type_id' => $request->address_type_id,
            'country_id' => $request->country_id,
            'state_id' => $state,
            'city_id' => $city,
            'city_sub1_id' => $citySub,
            'zip_code' => $zip_code,
            'created_by_user_id' => Auth::user()->id,
            'created_by_site_id' => URL::current(),
        );
        // dd($data);
        $address_id=DB::table('users_passport_address1')->insertGetId($data);

        $data = array(
            'address_id' => $address_id,
            'passport_id' => Session::get('passport_id'),
            'address' => $request->address,
            'additional' => $request->additional,
            'address_type_id' => $request->address_type_id,
            'language_code' => Auth::user()->language,
            'created_by_user_id' => Auth::user()->id,
            'created_by_site_id' => URL::current(),
        );
//        dd($data);
        DB::table('users_passport_address2')->insert($data);

        return redirect('package/member/edit-step3/'.$request->passport_id);
    }

    public function update_address(Request $request){
        $state = "";
        $city = "";
        $citySub = "";
        $zip_code = '';

        if (isset($request->state_id)){
            $state = $request->state_id;
        }
        if (isset($request->city_id)) {
            $city = $request->city_id;
        }
        if (isset($request->city_sub1_id)) {
            $citySub = $request->city_sub1_id;
            $zip_code = $request->zip_code;
        }
        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address_type_id' => $request->address_type_id,
            'country_id' => $request->country_id,
            'state_id' => $state,
            'city_id' => $city,
            'city_sub1_id' => $citySub,
            'zip_code' => $zip_code,

        );

        $address_id=DB::table('users_passport_address1')
            ->where('address_id',$request->address_id)
            ->update($data);

        $data = array(
            'passport_id' => Session::get('passport_id'),
            'address' => $request->address,
            'additional' => $request->additional,
            'address_type_id' => $request->address_type_id,
            'language_code' => Auth::user()->language,

        );
//        dd($data);
        DB::table('users_passport_address2')
            ->where('address_id',$request->address_id)
            ->update($data);

        return redirect('package/member/edit-step3');
    }

    public function delete_phone(Request $request){

        DB::table('users_passport_phone')->where('id',$request->id)->delete();
//        $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
        $Phones=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

        $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('profile.Country')."</th>
                                        <th>".trans('profile.phone_code')."</th>
                                        <th>".trans('profile.phone_type')."</th>
                                        <th>".trans('profile.phone_name')."</th>
                                        <th>".trans('profile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
        foreach ($Phones as $phone){
            $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                        <td align='center'><button class=\"btn btn-danger btn-sm order-phone-delete\" data-id=\"".$phone->id."\"  ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }
    
    public function save_phone(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'country_id'=>$request->phone_country_id,
                'phone_country_code'=>$request->phone_country_code,
                'phone_number'=>$request->phone_number,
                'phone_type'=>$request->phone_type,
                'created_by_user_id'=>Auth::user()->id,
                'created_by_site_id'=>URL::current(),
                'active'=>'1',
            );
            DB::table('users_passport_phone')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Phones=DB::table('users_passport_phone as a')
                ->join('countries as b','b.country_id','=','a.country_id')
                ->select('a.*','b.country')
                ->where('a.passport_id',Session::get('passport_id'))
                ->where('b.language_code',Auth::user()->language)
                ->get();
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Country')."</th>
                                <th>".trans('profile.phone_code')."</th>
                                <th>".trans('profile.phone_type')."</th>
                                <th>".trans('profile.phone_name')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Phones as $phone){
                    $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm order-phone-delete\" data-id=\"".$phone->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

            }
            $output.=" </tbody></table>";
           return Response($output);
        }
    }

    public function save_email(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'email'=>$request->email,
                'created_by_user_id'=>Auth::user()->id,
                'created_by_site_id'=>URL::current(),
                'active'=>'1',
            );
         
            DB::table('users_passport_email')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Email=DB::table('users_passport_email')
                ->where('passport_id',Session::get('passport_id'))
                ->get();
           
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Email')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Email as $rows){
                    $output.="<tr>
                         <td>".$rows->email."</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm order_delete_email\" data-id=\"".$rows->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

            }
            $output.=" </tbody></table>";
           return Response($output);
        }
    }

    public function delete_email(Request $request){
        DB::table('users_passport_email')->where('id',$request->id)->delete();
        $Email=DB::table('users_passport_email')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Email')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
        foreach ($Email as $rows){
            $output.="<tr>
                         <td>".$rows->email."</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm order_delete_email\" data-id=\"".$rows->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }

    public function EditAddress($id){
        $Address=DB::table('users_passport_address1 as a')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('a.address_id',$id)
            ->first();
        //dd($Address);
        $AddressInfo=null;
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();

        if($Address){
            $AddressInfo=DB::table('users_passport_address2')
                ->where('language_code',Auth::user()->language)
                ->where('address_id',$Address->address_id)
                ->first();
        }

        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Address->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Address->country_id)->active()->get();
        if(count($state)==0){
            $state=State::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }
        if($state){
            $city=City::where('country_id',$Address->country_id)
                ->where('state_id',$Address->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('city_id',$Address->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Address->country_id)
                        ->where('state_id',$Address->state_id)
                        ->where('city_id',$Address->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }

        $phone=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

        //  dd($Address);

        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('packages.order_member.forms2.edit_step3')
            ->with('AddressInfo',$AddressInfo)
            ->with('AddressAll',$AddressAll)
            ->with('Address',$Address)
            ->with('Email',$Email)
            ->with('Phones',$phone)
            ->with('state',$state)
            ->with('city',$city)
            ->with('event','edit')
            ->with('city_sub',$city_sub)
            ->with('country',$country);

    }

    public function EditStep3($id=null){
//        $country=App\Country::where('language_code',Auth::user()->language)->get();
        if($id){
            Session::put('passport_id',$id);
        }
        $Address=DB::table('users_passport_address1 as a')
            ->where('a.passport_id',Session::get('passport_id'))
            ->first();
      //  dd($Address);
        if(!isset($Address)){
          return $this->contact();
        }
        $AddressInfo=null;
        $AddressAll=DB::table('users_passport_address1 as a')
            ->join('users_passport_address2 as b','b.address_id','=','a.address_id')
            ->join('users_address_type as d','d.address_type_id','=','a.address_type_id')
            ->join('countries as c','c.country_id','=','a.country_id')
            ->select('a.*','b.address','b.additional','d.address_type','c.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.address_type_id')
            ->get();
//        echo Auth::user()->language;
       // dd($AddressAll);

        if($Address){
            $AddressInfo=DB::table('users_passport_address2')
                ->where('language_code',Auth::user()->language)
                ->where('address_id',$Address->address_id)
                ->first();
        }
 //dd($AddressAll);

        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Address->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }
        $state=State::where('country_id',$Address->country_id)->active()->get();

        if(count($state)==0){
            $state=State::where('country_id',$Address->country_id)->where('language_code','en')->get();
        }

        if($state){
            $city=City::where('country_id',$Address->country_id)
                ->where('state_id',$Address->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Address->country_id)
                    ->where('state_id',$Address->state_id)
                    ->where('city_id',$Address->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Address->country_id)
                        ->where('state_id',$Address->state_id)
                        ->where('city_id',$Address->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }


        $phone=DB::table('users_passport_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        //dd($phone);
      //  dd($Address);

        $Email=DB::table('users_passport_email')->where('passport_id',Session::get('passport_id'))->get();

        return view('packages.order_member.forms2.address')
            ->with('AddressInfo',$AddressInfo)
            ->with('AddressAll',$AddressAll)
            ->with('Address',$Address)
            ->with('Email',$Email)
            ->with('passport_id',$id)
            ->with('Phones',$phone)
            ->with('state',$state)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('country',$country);
    }

    public function edit($id){

        Session::put('passport_id',$id);
        $country=App\Country::where('language_code',Auth::user()->language)->get();
        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('language_code',Auth::user()->language)
            ->first();

        $event="";
        if(!$Member){
            $Member=DB::table('users_passport as a')
                ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->first();
            $event='change';
        }
        // dd($Member);

        Session::put('member_info','( '.$Member->first_name.' '.$Member->last_name.' )');

        return view('packages.order_member.forms2.edit_step1')
            ->with('passport_id',$id)
            ->with('event',$event)
            ->with('country',$country)
            ->with('Member',$Member);

    }

    public function delete($id){
        DB::table('users_passport')->where('passport_id',$id)->delete();
        DB::table('users_passport_info')->where('passport_id',$id)->delete();
        DB::table('users_passport_address1')->where('passport_id',$id)->delete();
        DB::table('users_passport_address2')->where('passport_id',$id)->delete();
        DB::table('users_passport_email')->where('passport_id',$id)->delete();
        DB::table('users_passport_phone')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference1')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference2')->where('passport_id',$id)->delete();
        DB::table('users_passport_reference_phone')->where('passport_id',$id)->delete();
        return redirect()->back();
    }

    public function MemberManage($id){


        $Timelines=DB::table('timelines as a')
            ->join('locations as b','b.timeline_id','=','a.id')
            ->join('location_user as c','c.location_id','=','b.id')
            ->select('a.id','a.name')
            ->where('c.user_id',Auth::user()->id)
            ->get();
        foreach ($Timelines as $rows){
            $timeline[$rows->id]=$rows->name;
        }
        $timeline_options = ['' => 'Select Category'] + $timeline;
        Session::put('timeline_options',$timeline_options);

        Session::put('timeline_id',$id);
        $Members=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('b.language_code',Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->orderby('a.updated_at','desc')
            ->groupby('a.passport_id')
//            ->limit(16)
            ->get();

       // dd($Members);
        Session::put('passport_id',null);
        Session::put('mode','member');
        Session::put('mode_lang','member');
         // dd($Members);
        return view('member.list-member')
            ->with('timeline_options',$timeline_options)
            ->with('Members',$Members);
    }



    public function tourist($booking,$tour_type){

        Session::put('BookingID',$booking);



        $Members=DB::table('package_tourist_member as a')
            ->join('users_passport as b','b.passport_id','=','a.passport_id')
            ->join('users_passport_info as c','c.passport_id','=','b.passport_id')
            ->where('a.booking_id',Session::get('BookingID'))
            ->where('a.tour_type',$tour_type)
            ->where('c.language_code',Auth::user()->language)
            ->orderby('b.updated_at','desc')
            ->groupby('b.passport_id')
            ->get();

//        dd($Members);

//        $Members=DB::table('package_tourist_member as a')
//            ->join('users_passport as b','b.passport_id','=','a.passport_id')
//            ->join('users_passport_info as c','c.passport_id','=','b.passport_id')
//            ->where('a.booking_id',$id)
//            ->where('c.language_code',Auth::user()->language)
//            ->orderby('b.updated_at','desc')
//            ->groupby('b.passport_id')
//            ->get();
//            //dd($Members);

        return view('packages.booking.list-member')
            ->with('Members',$Members);
    }

    public function member_list($id){

        Session::put('BookingID',$id);

        $BookingDetails=DB::table('package_booking_details as a')

            ->where('a.booking_id',$id)
            ->groupby('a.tour_type')
            ->groupby('a.package_detail_id')
            ->get();



        $Members=DB::table('package_tourist_member as a')
            ->join('users_passport as b','b.passport_id','=','a.passport_id')
            ->join('users_passport_info as c','c.passport_id','=','b.passport_id')
            ->where('a.booking_id',$id)
            ->where('c.language_code',Auth::user()->language)
            ->orderby('b.updated_at','desc')
            ->groupby('b.passport_id')
            ->get();
            //dd($Members);

        return view('packages.order_member.list-member')
            ->with('BookingDetails',$BookingDetails)
            ->with('Members',$Members);
    }

    public function member_list_by_timeline($id,$type){
        Session::put('BookingID',$id);
        Session::put('tour_type',$type);
        $BookingDetails=DB::table('package_booking_details as a')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.booking_id',$id)
            ->groupby('a.tour_type')
            ->groupby('a.package_detail_id')
            ->first();

        if($type==0){
            Session::put('tour_type',$BookingDetails->tour_type);
        }


        $Members=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->where('b.language_code',Auth::user()->language)
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->whereNotIn('a.passport_id',function ($query){
               $query->select('passport_id')->from('package_tourist_member')->where('booking_id',Session::get('BookingID'));
            })
            ->orderby('a.updated_at','desc')
            ->groupby('a.passport_id')
            ->get();

        // dd($Members);

        return view('packages.order_member.list-member-timeline')
            ->with('Members',$Members);
    }

    public function add_to_booking(Request $request){
         $Booking=DB::table('package_booking_details')->where('booking_id',Session::get('BookingID'))->get();
         $number_person=0; $key=0;
         if($Booking->count()==1){
             $number_person=$Booking[0]->number_of_person;
             $booking_detail_id=$Booking[0]->booking_detail_id;
             $package_detail_id=$Booking[0]->package_detail_id;
         }else{
             foreach ($Booking as $rows){
                 $number_person+=$rows->number_of_person;
                 $booking_detail_id=$rows->booking_detail_id;
                 $package_detail_id=$rows->package_detail_id;
                 $Num=DB::table('package_tourist_member')
                     ->where('booking_id',Session::get('BookingID'))
                     ->where('booking_detail_id',$rows->booking_detail_id)
                     ->where('package_detail_id',$rows->package_detail_id)
                     ->where('tour_type',Session::get('tour_type'))
                     ->count();
                 echo $rows->number_of_person.'='.$Num;
                 if($rows->number_of_person>$Num){
                     break;
                 }
             }
         }

//        dd($booking_detail_id);
//
         $Person=DB::table('package_tourist_member')
             ->where('passport_id',$request->passport_id)
             ->where('booking_id',Session::get('BookingID'))
             ->where('package_detail_id',$package_detail_id)
             ->where('tour_type',Session::get('tour_type'))
             ->orderby('orderby','desc')
             ->get();

         if($Person->count()){
             $orderby=$Person[0]->orderby+1;
         }else{
             $orderby=1;
         }
           // dd($orderby.'='.$number_person);

         if($orderby<=$number_person) {
             $data = array(
                 'booking_id' => Session::get('BookingID'),
                 'booking_detail_id' => $booking_detail_id,
                 'package_detail_id' => $package_detail_id,
                 'passport_id' => $request->passport_id,
                 'tour_type' => Session::get('tour_type'),
                 'orderby' => $orderby
             );
             DB::table('package_tourist_member')->insert($data);
         }

         if($Person->count() < $number_person){
             return "Success";
         }else{
             return "Not";
         }

    }

    public function MemberInfo($id){
        $Member=DB::table('users_passport as a')
            ->join('users_passport_info as b','b.passport_id','=','a.passport_id')
            ->join('users_title as c','c.user_title_id','=','a.user_title_id')
            ->join('users_sex_active as s','s.SexID','=','a.SexID')
            ->join('users_occupation as d','d.OccupationID','=','a.OccupationID')
            ->join('users_marital_active as e','e.MaritalactiveID','=','a.MaritalactiveID')
            ->join('education_type as f','f.EducationTypeID','=','a.EducationTypeID')
            ->join('users_nationality as g','g.NationalityID','=','a.CurrentNationalityID')
            ->join('users_religion_active as h','h.ReligionID','=','a.ReligionID')
            ->join('country as i','i.CountryID','=','a.PassportCountryID')
            ->select('a.*','b.*','c.UserTitle','s.Sexactive','d.Occupation','e.Maritalactive','f.EducationTypeName','g.Nationality','h.Religion','i.Country')
            ->where('b.language_code',Auth::user()->language)
            ->where('d.language_code',Auth::user()->language)
            ->where('s.language_code',Auth::user()->language)
            ->where('c.language_code',Auth::user()->language)
            ->where('e.language_code',Auth::user()->language)
            ->where('f.language_code',Auth::user()->language)
            ->where('g.language_code',Auth::user()->language)
            ->where('h.language_code',Auth::user()->language)
            ->where('i.language_code',Auth::user()->language)
            ->where('a.passport_id',$id)
            ->first();
        //  dd($User);
        return view('member.info')
            ->with('Member',$Member);
    }

    public function setTimeline(Request $request){
            Session::put('timeline_id',$request->timeline);
            return redirect('/member/manage/'.$request->timeline);
    }

    public function delete_member($id){

        DB::table('package_tourist_member')->where('passport_id',$id)->where('booking_id',Session::get('BookingID'))->delete();
        return redirect()->back();
    }




}
