<?php

namespace App\Http\Controllers\Package;

use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use App\Category;
use App\User;
use App\Timeline;
use App\Role;
use App\Location;
use App\Setting;

class PlaceController extends Controller
{
    //
    public function place_list($id=null){
        if($id){
            Session::put('passport_id',$id);
        }
        $Place=DB::table('users_passport_address_place_stay as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
//            ->where('b.language_code',Auth::user()->language)
//            ->where('c.language_code',Auth::user()->language)
            ->get();

        Session::put('mode_lang','place');
        return view('packages.order_member.place.list')
            ->with('Place',$Place);

    }

//    public function change_language($id){
//        $Place=DB::table('users_passport_address_place_stay2')
//            ->where('address_stay_place_group_id',$id)
//            ->groupby('language_code')
//            ->get();
//        // dd($MemberLang);
//        return view('member.place.place-show-lang')
//            ->with('Place',$Place)
//            ->with('address_stay_place_group_id',$id);
//    }


    public function create(){
        $Phones=DB::table('users_passport_address_place_stay_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

       // dd('test');
        return view('packages.order_member.place.create')
            ->with('category_options',$category_options)
            ->with('Phones',$Phones);
    }

    public function edit($id){
        $Phones=DB::table('users_passport_address_place_stay_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $Place=DB::table('users_passport_address_place_stay as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.place_stay_id',$id)
            ->where('b.language_code',Auth::user()->language)
            ->first();

        if(!$Place){
            $Place=DB::table('users_passport_address_place_stay as a')
                ->join('timelines as b','b.id','=','a.timeline_id')
                ->join('locations as c','c.timeline_id','=','a.timeline_id')
                ->where('a.place_stay_id',$id)
//                ->where('b.language_code',Auth::user()->language)
                ->first();
        }

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();


       // dd($Place);
        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Place->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Place->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Place->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Place->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Place->country_id)
                ->where('state_id',$Place->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Place->country_id)
                    ->where('state_id',$Place->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Place->country_id)
                    ->where('state_id',$Place->state_id)
                    ->where('city_id',$Place->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Place->country_id)
                        ->where('state_id',$Place->state_id)
                        ->where('city_id',$Place->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }

        //dd($Reference);
        return view('packages.order_member.place.edit')
            ->with('category_options',$category_options)
            ->with('Place',$Place)
            ->with('state',$state)
            ->with('country',$country)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('Phones',$Phones);
    }

    public function store(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
//            'place_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;

            }

            $data = array(
                'passport_id' => Session::get('passport_id'),
                'timeline_id' => $timeline_id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,
              
            );
             // dd($data);
            DB::table('users_passport_address_place_stay')->insert($data);


//            $data = array(
//                'address_stay_place_group_id' => $address_stay_place_group_id,
//                'passport_id' => Session::get('passport_id'),
//                'place_name' => $post['place_name'],
//                'place_address' => $post['place_address'],
//                'created_at' => date('Y-m-d H:i:s'),
//                'created_user_id' => Auth::user()->id,
//                'updated_user_id' => Auth::user()->id,
//                'language_code' => Auth::user()->language,
//            );
//            // dd($data);
//            DB::table('users_passport_address_place_stay2')->insert($data);

            if($request->place_phone_number){
                $check=DB::table('users_passport_address_place_stay_phone')
                    ->where('phone_number',$request->place_phone_number)
                    ->where('passport_id',Session::get('passport_id'))
                    ->first();
                // dd($check);
                if(!$check){
                    $data=array(
                        'passport_id'=>Session::get('passport_id'),
                        'country_id'=>$request->phone_country_id,
                        'phone_country_code'=>$request->phone_country_code,
                        'phone_number'=>$request->place_phone_number,
                        'phone_type'=>$request->phone_type,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_user_id'=>Auth::user()->id,
                        'updated_user_id'=>Auth::user()->id,
                        'active'=>'1',
                    );
                    DB::table('users_passport_address_place_stay_phone')->insert($data);
                    // dd($data);
                }
            }

            return redirect('/package/place/list');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;

            }

//            $check=DB::table('users_passport_address_place_stay')->where('place_stay_id',$post['place_stay_id'])->first();
//            $data=array(
//                'country_id' => $post['country_id'],
//                'state_id' => $state,
//                'city_id' => $city,
//                'city_sub1_id' => $citySub,
//                'zip_code' => $zip_code,
//                'name' => $post['name'],
//            );
//            DB::table('timelines')->where('id',$timeline_id)->update($data);
//
//            $data=array(
//                'address' => $post['address'],
//            );
//            DB::table('locations')->where('timeline_id',$timeline_id)->update($data);
//

            $data = array(
                'passport_id' => Session::get('passport_id'),
                'timeline_id' => $timeline_id,
                'updated_user_id' => Auth::user()->id,
                'language_code' => Auth::user()->language,
            );
             // dd($data);
            DB::table('users_passport_address_place_stay')->where('place_stay_id',$post['place_stay_id'])->update($data);

//            $data = array(
//                'address_stay_place_group_id' => $post['address_stay_place_group_id'],
//                'passport_id' => Session::get('passport_id'),
//                'place_name' => $post['place_name'],
//                'place_address' => $post['place_address'],
//                'updated_user_id' => Auth::user()->id,
//                'language_code' => Auth::user()->language
//            );
//            // dd($data);
//            $check=DB::table('users_passport_address_place_stay2')
//                ->where('address_stay_place_group_id',$post['address_stay_place_group_id'])
//                ->where('language_code',Auth::user()->language)
//                ->first();
//            if($check){
//                DB::table('users_passport_address_place_stay2')
//                    ->where('address_stay_place_group_id',$post['address_stay_place_group_id'])
//                    ->where('language_code',Auth::user()->language)
//                    ->update($data);
//            }else{
//                DB::table('users_passport_address_place_stay2')->insert($data);
//            }

        if($request->place_phone_number){
            $check=DB::table('users_passport_address_place_stay_phone')
                ->where('phone_number',$request->place_phone_number)
                ->where('passport_id',Session::get('passport_id'))
                ->first();
           // dd($check);
            if(!$check){
                $data=array(
                    'passport_id'=>Session::get('passport_id'),
                    'country_id'=>$request->phone_country_id,
                    'phone_country_code'=>$request->phone_country_code,
                    'phone_number'=>$request->place_phone_number,
                    'phone_type'=>$request->phone_type,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'created_user_id'=>Auth::user()->id,
                    'updated_user_id'=>Auth::user()->id,
                    'active'=>'1',
                );
                //  dd($data);
                DB::table('users_passport_address_place_stay_phone')->insert($data);
                //dd($data);
            }
        }
            return redirect('/package/place/list');
        }
    }

    public function delete($id){
        $check=DB::table('users_passport_address_place_stay')
            ->where('place_stay_id',$id)
            ->first();

        DB::table('users_passport_address_place_stay')
            ->where('place_stay_id',$id)
            ->delete();

        DB::table('users_passport_address_place_stay_phone')
            ->where('passport_id',$check->passport_id)
            ->delete();
        return redirect()->back();
    }

    public function save_phone(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'country_id'=>$request->phone_country_id,
                'phone_country_code'=>$request->phone_country_code,
                'phone_number'=>$request->phone_number,
                'phone_type'=>$request->phone_type,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_user_id'=>Auth::user()->id,
                'updated_user_id'=>Auth::user()->id,
                'active'=>'1',
            );
            DB::table('users_passport_address_place_stay_phone')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Phones=DB::table('users_passport_address_place_stay_phone as a')
                ->join('countries as b','b.country_id','=','a.country_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->where('b.language_code',Auth::user()->language)
                ->get();
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('profile.Country')."</th>
                                <th>".trans('profile.phone_code')."</th>
                                <th>".trans('profile.phone_type')."</th>
                                <th>".trans('profile.phone_name')."</th>
                                <th>".trans('profile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Phones as $phone){
                $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm order_place-phone-delete\"  data-id=\"".$phone->id."\"  ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";
            }
            $output.=" </tbody></table>";
            return Response($output);
        }
    }

    public function delete_phone(Request $request){

        DB::table('users_passport_address_place_stay_phone')->where('id',$request->id)->delete();
        $Phones=DB::table('users_passport_address_place_stay_phone')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('profile.Country')."</th>
                                        <th>".trans('profile.phone_code')."</th>
                                        <th>".trans('profile.phone_type')."</th>
                                        <th>".trans('profile.phone_name')."</th>
                                        <th>".trans('profile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
        foreach ($Phones as $phone){
            $output.="<tr>
                        <td>$phone->country_id</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                        <td align='center'><button class=\"btn btn-danger btn-sm order_place-phone-delete\"  data-id=\"".$phone->id."\" ><i class=\"fa fa-trash\"></i> ".trans('profile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }
}
