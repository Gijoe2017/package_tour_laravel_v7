<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use DB;
use Auth;
use Illuminate\Support\Facades\Schema;
use Image;
use Session;
use Markdown;
use Jenssegers\Date\Date;
use App;
use Cart;
use Cookie;
use App\Category;
use App\Location;

class PackageController extends Controller
{

    public function __construct(){

        if (!Auth::guest()) {
            return view('home.index');
        } else {

            if (!Session::get('language')) {
                return redirect('/home');
            }
        }
    }

    public function approve_public(){
        $data=array(
            'package_id'=>Session::get('package'),
            'package_event'=>'Confirm',
            'package_checked_by'=>Auth::user()->id,
        );

        DB::table('package_tour_checked_log')->insert($data);

        $data=array(
            'packageStatus'=>'CP',
            'package_close'=>'N',
            'package_checked_by'=>Auth::user()->id,
            'package_checked_date'=>date('Y-m-d H:i:s')
        );
       // dd($data);
        DB::table('package_tour')->where('packageID',Session::get('package'))
            ->update($data);
        Session::flash('message',trans('common.button_already_public'));
        //return back();
        return  redirect('package/list');
    }

    public function setProgramHighlight(Request $request){

        if($request->status=='N'){
            DB::table('highlight_in_schedule')
                ->where('packageID',Session::get('package'))
                ->where('programID',Session::get('program_id'))
                ->where('LocationID',$request->id)
                ->delete();
        }else{
            $data=array(
                'packageID'=>Session::get('package'),
                'programID'=>Session::get('program_id'),
                'makeSlideshow'=>'Y',
                'LocationID'=>$request->id,
            );
            DB::table('highlight_in_schedule')->insert($data);
        }
        return "success!".$request->status;

    }

    public function show(){
        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
//            ->where('users_info.LanguageCode', Auth::user()->language)
            ->where('b.Status_Info','P')
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->get();

        return view('')->with('Packages',$Packages);
    }

    //***************** Details *******************************/
    public function popup_edit_details($id){
        $packdetail=DB::table('package_details')->where('packageDescID',$id)->first();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('b.LanguageCode',Auth::user()->language)
            ->where('a.packageID',Session::get('package'))->first();
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
        return view('module.packages.setting.edit-details')
            ->with('Default',$Package)
            ->with('SiteName',$SiteName)
            ->with('packdetail',$packdetail);
    }


    public function getPackageBySearch(){
        \Date::setLocale(Session::get('language'));
         Session::forget('checking');
         Session::forget('passport_id');

        $str = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.packageName', 'b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id', Session::get('timeline_id'))
            ->where('a.packageStatus', '!=', 'X');
//            if(Session::get('status')=='close_package'){
//                $str->where('a.package_close', 'Y')
//                    ->where('a.packageStatus', 'CP');
//            }
//            if(Session::get('status')=='public'){
//                $str->where('a.package_close', 'N')
//                    ->where('a.packageStatus', 'CP');
//            }
//            if(Session::get('status')=='not_check'){
//                $str->where('a.package_checked_by', '0');
//            }
//            if(Session::get('status')=='Y'){
//                $str->where('a.package_checked_by', Auth::user()->id);
//            }
//            ->where('a.package_checked_by', '0');
            if(Session::get('partner') && Session::get('partner')!='all'){
                $str->where('a.owner_timeline_id', Session::get('partner'));
            }
            if(Session::get('country') && Session::get('country')!='all'){
                $str->where('c.Country_id', Session::get('country'));
            }
            if(Session::get('txtsearch')){
                $str->where(function ($query){
                    return $query->where('b.packageName', 'like', '%' . Session::get('txtsearch') . '%')
                        ->orwhere('a.original_package_code', 'like', '%' . Session::get('txtsearch') . '%')
                        ->orwhere('a.packageID', 'like', '%' . Session::get('txtsearch') . '%');
                });
//                $str->where('b.packageName', 'like', '%' . Session::get('txtsearch') . '%')
//                    ->orwhere('a.original_package_code', 'like', '%' . Session::get('txtsearch') . '%')
//                    ->orwhere('a.packageID', 'like', '%' . Session::get('txtsearch') . '%');
            }


            $Packages=$str->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
            ->paginate(12, ['*'], 'Packages');



        //   dd($Packages->total());

        $PackageLastest = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->whereIn('a.packageID', function ($query) {
                $query->select('package_id')->from('package_tour_latest_log')
                    ->where('package_latest_log_by', Auth::user()->id);
            })
            ->groupby('a.packageID')
            ->orderby('a.package_latest_view', 'desc')
            ->limit(3)
            ->get();

        $PackageOther = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
//            ->where('users_info.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->get();

        $PackageCountry=DB::table('package_details as a')
            ->join('package_tour as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','a.Country_id')
            ->select('c.*')
            ->where('b.timeline_id',Session::get('timeline_id'))
            ->where('c.country_id','>','0')
            ->groupby('a.Country_id')
            ->get();



      //  dd(Session::get('timeline_id').'='.$PackageLastest);
        return view('module.packages.setting.lists')
            ->with('PackageLastest', $PackageLastest)
            ->with('PackageCountry', $PackageCountry)
            ->with('PackageOther', $PackageOther)
            ->with('Packages', $Packages)
            ->with('pl_active',"active")
            ->with('p_open',"menu-open")
            ->with('p_disp',"display: block;");
//            return view('module.packages.setting.search-package-list')->with('Packages',$Packages);
    }

    public function clearSearch(){
        Session::forget('partner');
        Session::forget('country');
        Session::forget('txtsearch');
        Session::forget('status');
        return redirect('package/list');
    }

    public function getPackageByPartnerForBooking(Request $request){
        \Date::setLocale(Session::get('language'));

              //  dd($request->status);
        $search_query=$request->txtsearch;

        $str = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.packageName', 'b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id', Session::get('timeline_id'))
            ->where('a.packageStatus', 'CP')
            ->where('a.package_close', 'N');
            if($request->partner && $request->partner!='all'){
             $str->where('a.owner_timeline_id', $request->partner);
            }
            if($request->country && $request->country!='all'){
                $str->where('c.Country_id', $request->country);
            }
            if($request->txtsearch){
                $str->where(function($query) use ($search_query) {
                    $query->where('b.packageName','LIKE','%'.$search_query.'%')
                        ->orWhere('a.original_package_code','LIKE','%'.$search_query.'%')
                        ->orWhere('a.packageID','LIKE','%'.$search_query.'%');
                });
//                $str->where('b.packageName', 'like', '%' . $request->txtsearch . '%')
//                    ->orwhere('a.original_package_code', 'like', '%' . $request->txtsearch . '%')
//                    ->orwhere('a.packageID', 'like', '%' . $request->txtsearch . '%');

            }


           $Packages=$str->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
//               ->tosql();
            ->paginate(12, ['*'], 'Packages');


      //  dd($Packages);


        $request->country?Session::put('country',$request->country):'';
        $request->partner?Session::put('partner',$request->partner):'';
        $request->txtsearch?Session::put('txtsearch',$request->partner):'';


       return view('packages.booking.search-package-list')->with('Packages',$Packages);
    }



    public function getPackageByPartner(Request $request){
        \Date::setLocale(Session::get('language'));

        $str = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.packageName', 'b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id', Session::get('timeline_id'))
            ->where('a.packageStatus', '!=', 'X');


            if($request->status=='close_package'){
                $str->where('a.package_close', 'Y')
                    ->where('a.packageStatus', 'CP');
            }
            if($request->status=='public'){
                $str->where('a.package_close', 'N')
                    ->where('a.packageStatus', 'CP');
            }
            if($request->status=='not_check'){
                $str->where('a.package_checked_by', '0');
            }
            if($request->status=='my_checking'){
                $str->where('a.package_checked_by', Auth::user()->id);
            }
            if($request->partner && $request->partner!='all'){
                $str->where('a.owner_timeline_id', $request->partner);
            }
            if($request->country && $request->country!='all') {
                $country=$request->country;

                $str->whereIn('a.packageID',function ($query) use($country){
                    $query->select('packageID')->from('package_tourin')->where('CountryCode',$country);
                });
            }

            if($request->txtsearch) {
                $txtsearch =$request->txtsearch ;
                $str->where(function ($query) use($txtsearch){
                    return $query->where('b.packageName', 'like', '%' .$txtsearch . '%')->orwhere('a.original_package_code', 'like', '%' . $txtsearch . '%')
                    ->orwhere('a.packageID', 'like', '%' . $txtsearch . '%');
                });
//                $str->where('b.packageName', 'like', '%' . $request->txtsearch . '%')
//                    ->orwhere('a.original_package_code', 'like', '%' . $request->txtsearch . '%')
//                    ->orwhere('a.packageID', 'like', '%' . $request->txtsearch . '%');
            }

            $Packages=$str->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
            ->paginate(12, ['*'], 'Packages');




       // dd($Packages);

        $request->country?Session::put('country',$request->country):'';
        $request->partner?Session::put('partner',$request->partner):'';
        $request->txtsearch?Session::put('txtsearch',$request->partner):'';
        $request->status?Session::put('status',$request->status):'';


       return view('module.packages.setting.search-package-list')->with('Packages',$Packages);
    }

    public function listUserApprove($id){
        $Approve=DB::table('package_tour_checked_log as a')
            ->join('users_info as b','b.UserID','=','a.package_checked_by')
            ->where('b.LanguageCode',Auth::user()->language)
            ->where('a.package_id',$id)
            ->orderby('a.package_checked_date','desc')
            ->get();
       // dd($Approve);
        return view('module.packages.setting.list-approv')->with('Approve',$Approve);
    }



//    public function updatePackageDetail(Request $request){
//        $post = $request->all();
//        $v = \Validator::make($request->all(), [
//            'packageDateStart' => 'required',
//            'packageDateEnd' => 'required',
//        ]);
//        if ($v->fails()) {
//            return redirect()->back()->withErrors($v->errors());
//        } else {
//            $data = array(
//                'packageDateStart' => $post['packageDateStart'],
//                'packageDateEnd' => $post['packageDateEnd'],
//                'Flight' => $post['Flight'],
//                'Airline' => $post['Airline'],
//                'NumberOfPeople' => $post['NumberOfPeople'],
//                'Commission' => $post['Commission'],
//                'PriceSale' => $post['PriceSale'],
//                'PriceAndTicket' => $post['PriceAndTicket']
//            );
//            DB::table('package_details')->where('packageDescID',$post['id'])->update($data);
//            if ($request->hasFile('EditImg')) {
//                $pImage = DB::table('package_details')
//                    ->where('packageDescID',$post['id'])
//                    ->first();
//                if ($pImage->Image) {
//                    $fileNameDel = public_path('images/package-tour/mid/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                    $fileNameDel = public_path('images/package-tour/small/' . $pImage->Image);
//                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
//                        unlink($fileNameDel);
//                    }
//                }
//
//                $artPics = $request->file('picture');
//                $filename = time() . "." . $artPics->getClientOriginalExtension();
//                list($ori_w, $ori_h) = getimagesize($artPics);
//
//                if ($ori_w >= 1000) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 1000;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 650;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/mid/' . $filename));
//
//                if ($ori_w >= 650) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 650;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 480;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/small/' . $filename));
//
//                if ($ori_w >= 480) {
//                    if ($ori_w >= $ori_h) {
//                        $new_w = 480;
//                        $new_h = round(($new_w / $ori_w) * $ori_h);
//                    } else {
//                        $new_h = 350;
//                        $new_w = round(($new_h / $ori_h) * $ori_w);
//                    }
//                } else {
//                    $new_w = $ori_w;
//                    $new_h = $ori_h;
//                }
//                // Image::make($artPics)->resize($new_w, $new_h)->save(public_path('images/package-tour/x-small/' . $filename));
//
//
//                $data = array(
//                    'Image' => $filename
//                );
//                DB::table('package_details')->where('packageDescID',$post['id'])->update($data);
//            }
//            return redirect('member/package/details');
//        }
//    }
//******************************************************/

    public function admin(){
        return view('packages.setting.list_full');
    }

    public function sort_by_country(Request $request){
        \Date::setLocale(Session::get('language'));
        $txtsearch='';
        $PackageLastest = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.package_latest_view', 'desc')
            ->limit(6)
            ->get();

        if($request->partner=='all' || $request->partner==''){
            if($request->country=='all' || $request->country=='') {
                if ($request->txtsearch) {
                    $txtsearch = $request->txtsearch;
                    $Packages = DB::table('package_tour as a')
                        ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                        ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                        ->select('a.*', 'b.packageName', 'b.packageHighlight')
                        ->where('b.LanguageCode', Auth::user()->language)
                        ->where('a.timeline_id', Session::get('timeline_id'))
                        ->where('a.packageStatus', '!=', 'X')
                        ->where('b.packageName', 'like', '%' . $request->txtsearch . '%')
                        ->orwhere('a.original_package_code', 'like', '%' . $request->txtsearch . '%')
                        ->orwhere('a.packageID', 'like', '%' . $request->txtsearch . '%')
                        ->groupby('a.packageID')
                        ->orderby('a.LastUpdate', 'desc')
                        ->paginate(12, ['*'], 'Packages');
                    // dd($Packages);
                } else {
                    $Packages = DB::table('package_tour as a')
                        ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                        ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                        ->select('a.*', 'b.packageName', 'b.packageHighlight')
                        ->where('b.LanguageCode', Auth::user()->language)
                        ->where('a.timeline_id', Session::get('timeline_id'))
                        ->where('a.packageStatus', '!=', 'X')
                        ->groupby('a.packageID')
                        ->orderby('a.LastUpdate', 'desc')
                        ->paginate(12, ['*'], 'Packages');
                }
            }else if($request->country && $request->country!='all'){
                $Packages = DB::table('package_tour as a')
                    ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                    ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                    ->select( 'a.*', 'b.packageName','b.packageHighlight')
                    ->where('b.LanguageCode', Auth::user()->language)
                    ->where('a.timeline_id',Session::get('timeline_id'))
                    ->where('a.packageStatus','!=','X')
                    ->where('c.Country_id', $request->country)
                    ->groupby('a.packageID')
                    ->orderby('a.LastUpdate', 'desc')
                    ->paginate(12,['*'],'Packages');
            }

        }else if($request->country && $request->country!='all' && $request->partner && $request->partner!='all' && $request->txtsearch){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('c.Country_id', $request->country)
                ->where('a.owner_timeline_id',$request->partner)
                ->where('a.packageStatus','!=','X')
                ->where('b.packageName','like','%'.$request->txtsearch.'%')
                ->orwhere('a.original_package_code','like','%'.$request->txtsearch.'%')
                ->orwhere('a.packageID','like','%'.$request->txtsearch.'%')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }else if($request->country && $request->country!='all' && $request->txtsearch){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('c.Country_id', $request->country)
                ->where('b.packageName','like','%'.$request->txtsearch.'%')
                ->orwhere('a.original_package_code','like','%'.$request->txtsearch.'%')
                ->orwhere('a.packageID','like','%'.$request->txtsearch.'%')
                ->where('a.packageStatus','!=','X')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }else if($request->partner && $request->partner!='all' && $request->txtsearch){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('c.Country_id', $request->partner)
                ->where('b.packageName','like','%'.$request->txtsearch.'%')
                ->orwhere('a.original_package_code','like','%'.$request->txtsearch.'%')
                ->orwhere('a.packageID','like','%'.$request->txtsearch.'%')
                ->where('a.packageStatus','!=','X')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }else if($request->country && $request->country!='all' && $request->partner && $request->partner!='all'){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('c.Country_id', $request->country)
                ->where('a.owner_timeline_id',$request->partner)
                ->where('a.packageStatus','!=','X')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }else if($request->partner && $request->partner!='all'){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('a.owner_timeline_id',$request->partner)
                ->where('a.packageStatus','!=','X')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }else if($request->country && $request->country!='all'){
            $Packages = DB::table('package_tour as a')
                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                ->join('package_details as c', 'c.packageID', '=', 'a.packageID')
                ->select( 'a.*', 'b.packageName','b.packageHighlight')
                ->where('b.LanguageCode', Auth::user()->language)
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('c.Country_id', $request->country)
                ->where('a.packageStatus','!=','X')
                ->groupby('a.packageID')
                ->orderby('a.LastUpdate', 'desc')
                ->paginate(12,['*'],'Packages');
        }




//
        $PackageCountry=DB::table('package_details as a')
            ->join('package_tour as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','a.Country_id')
            ->select('c.*')
            ->where('b.timeline_id',Session::get('timeline_id'))
            ->where('c.country_id','!=','0')
            ->groupby('a.Country_id')
            ->get();



        Session::put('event','');
        return view('module.packages.setting.lists')
            ->with('PackageLastest', null)
            ->with('PackageCountry', $PackageCountry)
            ->with('Packages', $Packages)
            ->with('country',$request->country)
            ->with('partner',$request->partner)
            ->with('txtsearch',$txtsearch)
            ->with('pl_active',"active")
            ->with('p_open',"menu-open")
            ->with('p_disp',"display: block;");
    }

    public function partner_list(){
        $Partners = DB::table('package_tour_partner as a')
            ->join('timelines as b','b.id','=','a.request_partner_id')
            ->select('a.request_partner_id','b.id','b.name')
            ->where('a.partner_id', Session::get('timeline_id'))
            ->where('request_status','accept')
            ->get();

        return view('module.packages.setting.partner_list')->with('Partners',$Partners);
    }

    public function PackagePartnerList($id)
    {


        $PackageCountry=DB::table('package_details as a')
            ->join('package_tourin as c','c.packageID','=','a.packageID')
            ->join('countries as b','b.country_id','=','c.CountryCode')
            ->select('a.*','b.*')
            ->where('a.packageDateStart','>',date('Y-m-d'))
            ->where('a.status','Y')
            ->where('b.language_code',Session::get('language'))
            ->groupby('b.country_id')
            ->get();

//        $Packages = DB::table('package_tour as a')
//            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//            ->select( 'a.*', 'b.packageName','b.packageHighlight')
////            ->where('a.package_owner', 'Yes')
//            ->where('b.LanguageCode', Auth::user()->language)
////            ->where('users_info.LanguageCode', Auth::user()->language)
//            ->where('a.owner_timeline_id',$id)
//            ->where('a.packageStatus','!=','X')
//            ->groupby('a.packageID')
//            ->orderby('a.LastUpdate', 'desc')
//            ->paginate(12,['*'],'Packages');

        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->join('package_details as d','d.packageID','=','a.packageID')
            ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
            ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.price_system_fees', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Session::get('language'))
            ->where('a.package_owner', 'Yes')
            ->where('a.timeline_id',$id)
            ->where('b.Status_Info','P')
            ->where('a.packageStatus','CP')
            ->where('d.status','Y')
            ->where('e.status','Y')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('e.psub_id', 'asc')
            ->paginate(12,['*'],'Packages');

       // dd($Packages);
        Session::put('Packages',$Packages->count());
        Session::put('mode','package');
        Session::put('event','');

        return view('module.packages.setting.package_partner_list')
            ->with('PackageCountry', $PackageCountry)
            ->with('Packages', $Packages);
    }

    public function PackageList()
    {
        \Date::setLocale(Session::get('language'));
        if(Session::get('country') || Session::get('partner') || Session::get('txtsearch') || Session::get('status')){
              return redirect('/package/list_search');
        }
        Session::forget('passport_id');
        Session::forget('checking');
        $PackageCountry=DB::table('package_details as a')
            ->join('package_tour as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','a.Country_id')
            ->select('c.*')
            ->where('b.timeline_id',Session::get('timeline_id'))
            ->where('c.country_id','>','0')
            ->groupby('a.Country_id')
            ->get();





        $Packages = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
//            ->where('users_info.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->orderby('a.packageID', 'desc')
            ->paginate(12,['*'],'Packages');

        $PackageLastest = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->whereIn('a.packageID', function ($query) {
                $query->select('package_id')->from('package_tour_latest_log')
                    ->where('package_latest_log_by', Auth::user()->id);
            })
            ->groupby('a.packageID')
            ->orderby('a.package_latest_view', 'desc')
            ->limit(3)
            ->get();

        $PackageOther = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
//            ->where('users_info.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->orderby('a.LastUpdate', 'desc')
            ->get();
        //dd(Session::get('language'));
       // dd($Packages);

        $Packagescount = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select( 'a.*', 'b.packageName','b.packageHighlight')
            ->where('b.LanguageCode', Auth::user()->language)
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('a.packageStatus','!=','X')
            ->groupby('a.packageID')
            ->get();
       // dd($Packages);
     //   dd($Packagescount->count());

        Session::put('Packages',$Packagescount->count());
        Session::put('mode','package');
        Session::put('event','');

        return view('module.packages.setting.lists')
            ->with('PackageLastest', $PackageLastest)
            ->with('PackageCountry', $PackageCountry)
            ->with('PackageOther', $PackageOther)
            ->with('Packages', $Packages)
            ->with('pl_active',"active")
            ->with('p_open',"menu-open")
            ->with('p_disp',"display: block;");
    }


    public function create()
    {               
//      $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $Package = DB::table('package_tour')->orderby('packageID', 'desc')->first();
        if (isset($Package)) {
            $PackageID = $Package->packageID + 1;
        } else {
            $PackageID = 1;
        }

        $Currency = DB::table('currency')
            ->where('language_code', 'en')
            ->orderby('currency_name', 'asc')
            ->get();

        $Country = DB::table('countries')
            ->where('language_code', Auth::user()->language)
            ->orderby('country', 'asc')
            ->get();

        $Partners = DB::table('package_tour_partner as a')
            ->join('timelines as b','b.id','=','a.request_partner_id')
            ->where('a.partner_id', Session::get('timeline_id'))
            ->where('a.request_status','accept')
            ->where('b.language_code',Auth::user()->language)
            ->get();

//        echo Session::get('timeline_id');
        //echo Auth::user()->language;

        if(!$Partners->count()){
            $Partners = DB::table('package_tour_partner as a')
                ->join('timelines as b','b.id','=','a.request_partner_id')
                ->where('a.partner_id', Session::get('timeline_id'))
                ->where('a.request_status','accept')
               // ->where('b.language_code','en')
                ->get();
        }

       // dd($Partners);

        $SiteName = DB::table('users')->where('id', Auth::user()->id)->first();
        return view('module.packages.setting.create')
//            ->with('lang', $lang)
            ->with('Package', $Package)
            ->with('Partners', $Partners)
            ->with('SiteName', $SiteName)
            ->with('Currency', $Currency)
            ->with('Country', $Country)
            ->with('PackageID', $PackageID)
            ->with('p_active1',"active")
            ->with('p_open',"menu-open")
            ->with('p_disp',"display: block;");
    }

    public function update_last_event($id){

        $check=DB::table('package_tour_latest_log')
            ->where('package_latest_log_by',Auth::user()->id)
            ->get();
        $date=array(
            'package_id'=>$id,
            'package_latest_log_by'=>Auth::user()->id,
            'log_type'=>'Open',
            'package_latest_log'=>date('Y-m-d H:i:s')
        );

        if($check->count()>=6){
            $Package=DB::table('package_tour_latest_log')
                ->where('package_latest_log_by',Auth::user()->id)
                ->orderby('package_latest_log','asc')
                ->first();
            DB::table('package_tour_latest_log')->where('id',$Package->id)->update($date);
        }else{
            DB::table('package_tour_latest_log')->insert($date);
        }
        DB::table('package_tour')->where('packageID',$id)->update(['package_latest_view'=>date('Y-m-d H:i:s')]);
        return back();
    }

    public function edit($id)
    {
        // $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        // $Agency = DB::table('agency')->where('LanguageCode', Session::get('Language'))->orderby('agencyName', 'asc')->get();
        // $Country=DB::table('country')->where('LanguageCode', Session::get('Language'))->orderby('Country','asc')->get();
        // $works = DB::table('package_tour')
        //     ->join('package_tour_info', 'package_tour_info.packageID', '=', 'package_tour.packageID')
        //     ->where('package_tour.packageID', $id)
        //     ->where('package_tour_info.LanguageCode', Session::get('Language'))->first();
        // $Info = DB::table('package_tour_category')->where('packageID', $id)->first();
        // $PackageTourTypeName = DB::table('package_tour_type')->where('LanguageCode', Session::get('Language'))->orderby('PackageTourTypeName', 'asc')->get();
        // $Pics = DB::table('package_tour_images')->where('packageID', $id)->get();
        // $ExhiInfo = DB::table('package_tour_info')->where('packageID', $id)
        //     ->where('LanguageCode', Session::get('Language'))->first();
        // $SiteName=DB::table('users')->where('id',$createby)->first();

        // if(!Session::get('Language')){
        //     Session::put('Language','en');
        // }
        //    $lang = DB::table('language')
        //    ->orderby('LanguageName', 'asc')->get();



        $this->update_last_event($id);

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        $Package = DB::table('package_tour AS a')
            ->join('package_tourin as b', 'a.packageID', '=', 'b.packageID')
            ->where('a.packageID',$id)
            ->first();
        //     ->toSql();
       //  dd($Package);
        Session::put('package',$id);

        $Currency=DB::table('currency')
            ->where('language_code',Auth::user()->language)
            ->orderby('currency_name','asc')
            ->get();
        if(!$Currency){
            $Currency=DB::table('currency')
                ->where('language_code','en')
                ->orderby('currency_name','asc')
                ->get();
        }

        //     ->toSql();

        $Country=\App\Country::active()->get();
//        dd($Country);
        //     ->toSql();
        // dd($Country);

        $Partners = DB::table('package_tour_partner as a')
            ->join('timelines as b','b.id','=','a.request_partner_id')
            ->where('a.partner_id', Session::get('timeline_id'))
            ->where('a.request_status','accept')
            ->get();
       // dd($Partners);
        Session::put('event','edit');
        return view('module.packages.setting.edit')
            ->with('category_options',$category_options)
            ->with('Partners',$Partners)
            ->with('Country',$Country)
            ->with('Currency', $Currency)
            ->with('Package', $Package)
            ->with('e_active',"active")
            ->with('p_open',"menu-open")
            ->with('p_disp',"display: block;");
            // ->with('works', $works)
            // ->with('Agency', $Agency)
            // ->with('Info', $Info)
            // ->with('PackageTourTypeName', $PackageTourTypeName)
            // ->with('ExhiInfo', $ExhiInfo);
  }

  public function upload_package_file(Request $request){
      if ($request->hasFile('file')) {
          $file=$request->file('file');
          $ext=$file->getClientOriginalExtension();

          $file_name='docs-'.$request->id.'.'.$ext;
         // dd($file_name);
          $check=move_uploaded_file($file,public_path('images/package-tour/docs/' . $file_name));
          // $request->file()move($file,public_path('images/package-tour/docs/' . $file_name));
          $data=array(
              'original_file' => $file_name
          );
          DB::table('package_tour')->where('packageID',$request->id)->update($data);
          return back();
      }

  }

    public function store(Request $request)
    {
        $lang = DB::table('language')->orderby('LanguageName', 'asc')->get();
        $post = $request->all();

        $v = \Validator::make($request->all(), [
            'packageLanguage' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $Package = DB::table('package_tour')->orderby('packageID', 'desc')->first();
            if (isset($Package)) {
                $packageID = $Package->packageID + 1;
            } else {
                $packageID = 1;
            }

            if($post['package_owner']=='Yes'){
                $owner_timeline_id='';
            }else{
                $owner_timeline_id=$post['owner_timeline_id'];
            }

            $file_name='';

            if ($request->hasFile('file')) {
                $file=$request->file('file');
                $ext=$file->getClientOriginalExtension();

                $file_name='docs-'.$packageID.'.'.$ext;
                $check=move_uploaded_file($file,public_path('images/package-tour/docs/' . $file_name));
               // $request->file()move($file,public_path('images/package-tour/docs/' . $file_name));

               // dd($file_name);
            }
            $original_package_code="";
            if(isset($post['original_package_code'])){
                $original_package_code=$post['original_package_code'];
            }


            $data = array(
                'packageID' => $packageID,
                'timeline_id' => Session::get('timeline_id'),
                'package_owner' => $post['package_owner'],
                'owner_timeline_id' => $owner_timeline_id,
//              'packageTourType'=>$post['packageTourType'],
                'packageCurrency' => $post['packageCurrency'],
                'packageDays' => $post['packageDays'],
                'packageNight' => $post['packageNight'],
                'original_file' => $file_name,
                'original_package_code' => $original_package_code,
                //   'packagePeopleIn'=>$post['packagePeopleIn'],
                'packageLanguage' => $post['packageLanguage'],
                'have_visa' => $post['have_visa'],
                'condition_visa' => $post['condition_visa'],
                'packageCreated' => date('Y-m-d h:i:s'),
                'LastUpdate' => date('Y-m-d h:i:s'),
                'packageBy' => Auth::user()->id,
                'packageStatus' => 'P'
            );
            // dd($data);
            $i = DB::table('package_tour')->insert($data);

            if (isset($post['packagePeopleIn'])) {
                DB::table('package_tourin')->where('packageID', $packageID)->delete();
                foreach ($post['packagePeopleIn'] as $val) {
                    $Country = DB::table('countries')->where('country_id', $val)->first();
                    $data = array(
                        'packageID' => $packageID,
                        'CountryCode' => $Country->country_id,
                        'CountryISOCode' => strtolower($Country->country_iso_code)
                    );
                    DB::table('package_tourin')->insert($data);
                }
            }

            $data = array(
                'packageID' => $packageID,
                'packageName' => 'Untitled',
                'packageHighlight' => 'Untitled',
                'LanguageCode' => $post['packageLanguage'],
                'Status_Info' => 'P'
            );
            DB::table('package_tour_info')->insert($data);

            Session::put('package', $packageID);
            Session::put('event','create');
//            App::setLocale($post['packageLanguage']);
//            Session::put('Language', $post['packageLanguage']);
//            Date::setLocale($post['packageLanguage']);
            if ($i > 0) {
                return redirect('/package/info');
            }

        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'packageLanguage' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }else{
            if($post['package_owner']=='Yes'){
                $owner_timeline_id='';
            }else{
                $owner_timeline_id=$post['owner_timeline_id'];
            }
            $check=DB::table('package_tour')->where('packageID',$post['id'])->first();
            $file_name=$check->original_file;
            if ($request->hasFile('file')) {
                $file=$request->file('file');
                $ext=$file->getClientOriginalExtension();

            if($file_name){
                $fileNameDel = public_path('images/package-tour/docs/' . $file_name);
                if (file_exists($fileNameDel) and $file_name != 'default.jpg'){
                    unlink($fileNameDel);
                }
            }

                $file_name='docs-'.$post['id'].'.'.$ext;
                $check=move_uploaded_file($file,public_path('images/package-tour/docs/' . $file_name));
                // dd($file_name);
            }
            $original_package_code="";
            if(isset($post['original_package_code'])){
                $original_package_code=$post['original_package_code'];
            }

//            $data = array(
//                'package_owner' => $post['package_owner'],
//                'owner_timeline_id' => $owner_timeline_id,
//                'packageCurrency' => $post['packageCurrency'],
//                'packageDays' => $post['packageDays'],
//                'packageNight' => $post['packageNight'],
//                'original_file' => $file_name,
//                'original_package_code' => $original_package_code,
//                'packageLanguage' => $post['packageLanguage'],
//                'LastUpdate' => date('Y-m-d h:i:s')
//                // 'packageBy' => Auth::user()->id,
//            );

            $data = array(
                'timeline_id' => Session::get('timeline_id'),
                'package_owner' => $post['package_owner'],
                'owner_timeline_id' => $owner_timeline_id,
//              'packageTourType'=>$post['packageTourType'],
                'packageCurrency' => $post['packageCurrency'],
                'packageDays' => $post['packageDays'],
                'packageNight' => $post['packageNight'],
                'original_file' => $file_name,
                'original_package_code' => $original_package_code,

                'packageLanguage' => $post['packageLanguage'],
                'have_visa' => $post['have_visa'],
                'condition_visa' => $post['condition_visa'],
                'LastUpdate' => date('Y-m-d h:i:s'),
                'packageStatus' => 'P'
            );

            // dd($data);
            $i = DB::table('package_tour')
                ->where('packageID', $post['id'])
                ->update($data);
            // dd($i);

            if (isset($post['packagePeopleIn'])) {
                DB::table('package_tourin')->where('packageID', $post['id'])->delete();
                foreach ($post['packagePeopleIn'] as $val) {
                    $Country = DB::table('countries')->where('country_id', $val)->first();
                    $datap = array(
                        'packageID' => $post['id'],
                        'CountryCode' => $Country->country_id,
                        'CountryISOCode' => strtolower($Country->country_iso_code)
                    );
                    DB::table('package_tourin')->insert($datap);
                }
            }
            if(Session::has('checking')){
                return redirect()->back();
            }
            if ($i > 0) {
                return redirect('/package/info');
            }else{

                return redirect()->back();
            }
        }
        
    }

    public function info()
    {
        Session::put('info', '1');
        Session::put('groupImage', '1');



        $Package = DB::table('package_tour as a')
            ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
            ->select('a.*', 'b.*')
            ->where('a.packageID', Session::get('package'))->first();

        $packageInfo = DB::table('package_tour_info')
            ->where('packageID', Session::get('package'))
            ->where('LanguageCode', Auth::user()->language)
            ->first();
       //dd($packageInfo);
        $SiteName = DB::table('users')->where('id', Auth::user()->id)->first();



        $TourCategory = DB::table('tour_category')
            ->where('LanguageCode', Auth::user()->language)
            ->orderby('TourCategoryName', 'asc')->get();

        $check=DB::table('package_tour_category')
            ->select('TourCategoryID')
            ->where('packageID',$Package->packageID)
            ->get();

        $TourType = DB::table('tour_category_sub1')
            ->where('LanguageCode', Auth::user()->language)
//             ->whereIn('groupID', function ($query) {
//                 $query->select('TourSubCateID')->from('package_tour_category')
//                     ->where('packageID', Session::get('package'));
//             })
            ->orderby('TourCategorySub1Name', 'asc')
            ->get();

        if($check->count()){

            foreach ($check as $row){
               $cate[]=$row->TourCategoryID;
            }
            
            $TourType = DB::table('tour_category_sub1')
                ->where('LanguageCode', Auth::user()->language)
                ->whereIn('TourCategoryID',$cate)
                ->orderby('TourCategorySub1Name', 'asc')
                ->get();
        }

            // ->toSql();
            // dd($TourType);


        $Photos = DB::table('highlight_in_schedule as a')
            ->join('package_location_info as c', 'c.LocationID', '=', 'a.LocationID')
            ->join('package_images as b', 'b.ImageID', '=', 'c.ImageID')
            ->where('c.LanguageCode', Auth::user()->language)
            ->where('a.packageID', Session::get('package'))
            ->where('a.makeSlideshow', 'Y')
            ->groupby('a.LocationID')
            ->get();
        //  dd($Photos);

        $TourImage = DB::table('package_images')
            ->where('GroupImage', '1')
            ->where('programID', null)
            ->where('packageID', Session::get('package'))
            ->orderby('OrderBy', 'asc')
            ->first();
        //  dd($TourImage);

//        $LangUser = DB::table('language')
//            ->whereIn('LanguageCode', function ($query) {
//                $query->select('b.LanguageCode')->from('package_tour as a')
//                    ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//                    ->where('a.packageBy', Auth::user()->id);
//            })
//            ->orderby('LanguageName', 'asc')->get();
        //   dd($Package);
        return view('module.packages.setting.info')
            ->with('Package', $Package)
            ->with('packageInfo', $packageInfo)
            ->with('TourImage', $TourImage)
            ->with('Photos', $Photos)
            ->with('SiteName', $SiteName)
            ->with('TourCategory', $TourCategory)
            ->with('TourType', $TourType)
            ->with('Default', $Package);
    }

    public function SaveInfo(Request $request)
    {
        ini_set('memory_limit', '-1');
        $post = $request->all();

        $v = \Validator::make($request->all(), [
            'packageName' => 'required',
            'packageHighlight' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            if (isset($post['TourCategoryID'])) {
                DB::table('package_tour_category')->where('packageID', Session::get('package'))->delete();
                foreach ($post['TourCategoryID'] as $val) {
                    //dd($val);
                    $data = array(
                        'packageID' => Session::get('package'),
                        'TourCategoryID' => $val,
                    );
                    $chk = DB::table('package_tour_category')
                        ->where('packageID', Session::get('package'))
                        ->where('TourCategoryID', $val)
                        ->first();
                   // dd($data);
                    if (!isset($chk)) {
                        DB::table('package_tour_category')->insert($data);
                    }
                }
            }

            if (isset($post['TourSubCateID'])) {
                foreach ($post['TourSubCateID'] as $val) {
                    $Type = DB::table('tour_category_sub1')->where('LanguageCode', Auth::user()->language)->where('groupID', $val)->first();

                    if (isset($Type)) {

                        $check = DB::table('package_tour_category')
                            ->where('packageID', Session::get('package'))
                            ->where('TourCategoryID', $Type->TourCategoryID)
                            ->where('TourSubCateID', $Type->groupID)
                            ->first();

                        if (!isset($check)) {
                            $data = array(
                                'packageID' => Session::get('package'),
                                'TourCategoryID' => $Type->TourCategoryID,
                                'TourSubCateID' => $val,
                            );
                            echo $val;
                            DB::table('package_tour_category')->insert($data);
                        }
                    }
                }
            }

            $check = DB::table('package_tour_info')->where('packageID', $post['id'])
                ->where('LanguageCode', Auth::user()->language)
                ->first();
            if (!isset($check)) {
                $Status = 'D';
            } else {
                if ($check->Status_Info == 'P') {
                    $Status = 'P';
                } else {
                    $Status = 'S';
                }
            }

            $data = array(
                'packageID' => $post['id'],
                'show_in_timeline' => Session::get('timeline_id'),
                'commission_percent' => '100',
                'packageName' => $post['packageName'],
                'packageHighlight' => $post['packageHighlight'],
                'Status_Info' => $Status,
                'LanguageCode' => Auth::user()->language,
            );
          //  dd($check);
            if (isset($check)) {
                $i = DB::table('package_tour_info')->where('packageID', $post['id'])
                    ->where('LanguageCode', Auth::user()->language)
                    ->update($data);
            } else {
                $i = DB::table('package_tour_info')->insert($data);
            }

            if ($request->hasFile('picture')) {
                $pImage = DB::table('package_images')
                    ->where('packageID', $post['id'])
                    ->where('GroupImage', '1')
                    ->orderby('OrderBy', 'asc')
                    ->first();
                if (isset($pImage)) {
                    $fileNameDel = storage_path('uploads/package-tour/mid/' . $pImage->Image);
//                    $fileNameDel = public_path('images/package-tour/mid/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    $fileNameDel = storage_path('uploads/package-tour/small/' . $pImage->Image);
//                    $fileNameDel = public_path('images/package-tour/small/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }

                    $fileNameDel = storage_path('uploads/package-tour/x-small/' . $pImage->Image);
//                    $fileNameDel = public_path('images/package-tour/small/' . $pImage->Image);
                    if (file_exists($fileNameDel) and $pImage->Image != 'default.jpg') {
                        unlink($fileNameDel);
                    }
                    DB::table('package_images')->where('ImageID', $pImage->ImageID)->delete();
                }
                //   dd(public_path());

                $Pics = $request->file('picture');

                $filename = time() . "." . $Pics->getClientOriginalExtension();
                list($ori_w, $ori_h) = getimagesize($Pics);

                if ($ori_w >= 1000) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 1000;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 650;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(storage_path('uploads/package-tour/mid/' . $filename));

                if ($ori_w >= 650) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 650;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 480;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(storage_path('uploads/package-tour/small/' . $filename));

                if ($ori_w >= 480) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 480;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 350;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($Pics)->resize($new_w, $new_h)->save(storage_path('uploads/package-tour/x-small/' . $filename));

                $data = array(
                    'packageID' => $post['id'],
                    'image_path' => Session::get('MyImagePath') . "/package-tour",
                    'GroupImage' => '1',
                    'Image' => $filename,
                    'OrderBy' => '0'
                );
                DB::table('package_images')->insert($data);
                //  dd($data);
                $data = array(
                    'Image' => $filename
                );
                DB::table('package_tour')->where('packageID', $post['id'])->update($data);
            }
            if(Session::has('checking')){
                return redirect('/package/details_check/'.Session::get('package'));
            }else{
                //dd($check);
                return redirect('/package/schedule');
            }
        }
    }

    public function details(){
      //  App::setLocale(Session::get('language'));
        Session::put('groupImage','3');$PackageIn='';
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();
       // dd($SiteName);
      //  $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',Session::get('package'))->first();

          // ->where('a.packageID',8)->first();
//        $check=DB::table('package_tourin')->where('packageID',Session::get('package'))->where('CountryISOCode','th')->get();
//
//        if($check->count()){
//            if($check->count()==1){
//                $PackageIn='Yes';
//            }
//        }

        $PackageDesc=DB::table('package_details as a')
           // ->join('airline as b','b.airline','=','a.Airline')
            ->where('a.packageID',Session::get('package'))
            ->orderby('a.packageDateStart','desc')
            ->get();

          //  dd($PackageDesc);

        return view('module.packages.setting.details')
            ->with('SiteName',$SiteName)
            ->with('PackageDesc',$PackageDesc)
            ->with('PackageIn',$PackageIn)
            ->with('Default',$Package);
    }


    public function partner_details($id){
        Session::put('groupImage','3');$PackageIn='';
        $SiteName=DB::table('users')->where('id',Auth::user()->id)->first();

        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$id)->first();

        $PackageDesc=DB::table('package_details as a')
           // ->join('airline as b','b.airline','=','a.Airline')
            ->where('a.packageID',$id)
            ->orderby('a.packageDateStart','desc')
            ->get();

          //  dd($PackageDesc);

        return view('module.packages.setting.partner_details')
            ->with('SiteName',$SiteName)
            ->with('PackageDesc',$PackageDesc)
            ->with('PackageIn',$PackageIn)
            ->with('Default',$Package);
    }



    public function statusDetails(Request $request){
        $data=array(
            'status'=>$request->status
        );
        DB::table('package_details')->where('packageDescID',$request->id)->update($data);
        DB::table('package_tour')->where('packageID',Session::get('package'))->update(['packageStatus'=>'P']);
        return trans('common.'.$request->status);
    }

    public function statusRateDetails(Request $request){
        $data=array(
            'status'=>$request->status
        );
        DB::table('package_details_sub')->where('psub_id',$request->id)->update($data);
        return trans('common.'.$request->status);
    }

    public function set_price_visa(Request $request){
        DB::table('package_details_sub')->where('psub_id',$request->id)->update(['price_include_visa'=>$request->status]);

        return trans('common.price_include_visa_'.$request->status);
    }

    public function set_price_default(Request $request){

//      DB::table('package_details_sub')->where('packageDescID',Session::get('packageDescID'))->update(['price_system_default'=>'N']);
        DB::table('package_details_sub')->where('psub_id',$request->id)->update(['price_include_vat'=>$request->status]);
        return trans('common.price_include_visa_'.$request->status);
    }


    public function statusAdditionalDetails(Request $request){
           $data=array(
                'status'=>$request->status
           );

           $i=DB::table('package_additional_services')->where('id',$request->id)->update($data);

           if($i>0){

               return trans('common.'.$request->status);
           }
    }

    public function statusPromotionDetails(Request $request){
        $data=array(
            'promotion_status'=>$request->status
        );
        DB::table('package_promotion')->where('promotion_id',$request->id)->update($data);
        $check=DB::table('package_promotion')->where('promotion_id',$request->id)->first();
        if($check->promotion_operator=='Mod'){
            $data=array(
                'promotion_status'=>'N'
            );
            DB::table('package_promotion')
                ->where('psub_id',$check->psub_id)
                ->where('promotion_operator','!=','Mod')
                ->update($data);
        }else{
            $data=array(
                'promotion_status'=>'N'
            );
            DB::table('package_promotion')
                ->where('psub_id',$check->psub_id)
                ->where('promotion_operator','!=','Between')
                ->update($data);
        }
        return $check->promotion_operator;
    }


    public function createDetails(){
        Session::forget('checking');
        $PackageIn='';
        $country=App\Country::active()->get();

        $Vehicle=DB::table('the_vehicle')->where('language_code',Session::get('language'))->where('status','1')->get();
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();

        $PackageInfo=DB::table('package_tour_info')
            ->where('packageID',Session::get('package'))
            ->first();

        return view('module.packages.setting.create-package-popup')
            ->with('PackageInfo',$PackageInfo)
            ->with('Package',$Package)
            ->with('Vehicle',$Vehicle)
            ->with('PackageIn',$PackageIn)
            ->with('country',$country);
    }

    public function createCheckDetails(){
        $country=App\Country::active()->get();
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $PackageInfo=DB::table('package_tour_info')
            ->where('packageID',Session::get('package'))
            ->first();

        return view('module.packages.setting.create-package-check')
            ->with('PackageInfo',$PackageInfo)
            ->with('Package',$Package)
            ->with('country',$country);
    }

    public function copy_details($id){
        $packdetail=DB::table('package_details')->where('packageDescID',$id)->first();

        if(isset($packdetail)){
            $data=array(
                'packageID'=>$packdetail->packageID,
                'packageDateStart'=>$packdetail->packageDateStart,
                'packageDateEnd'=>$packdetail->packageDateEnd,
                'Flight'=>$packdetail->Flight,
                'Airline'=>$packdetail->Airline,
                'Country_id' => $packdetail->Country_id,
                'use_airplan' => $packdetail->use_airplan,
                'season' => $packdetail->season,
                'season_name' => $packdetail->season_name,
                'travel_by' => $packdetail->travel_by,
                'NumberOfPeople'=>$packdetail->NumberOfPeople,
                'Commission'=>$packdetail->Commission,
                'Commission_sell'=>$packdetail->Commission_sell,
                'unit_commission' => $packdetail->unit_commission,
            );
            $packageDescID=DB::table('package_details')->insertGetId($data);

            $getConditionRate=DB::table('package_details_sub')->where('packageDescID',$id)->get();
            if(isset($getConditionRate)){
                foreach ($getConditionRate as $rows){
                    $data = array(
                        'packageDescID'=>$packageDescID,
                        'packageID'=>$rows->packageID,
                        'TourType'=>$rows->TourType,
                        'PriceSale'=>$rows->PriceSale,
                        'Price_by_promotion'=>$rows->Price_by_promotion,
                        'price_system_fees'=>$rows->price_system_fees,
                        'price_include_visa'=>$rows->price_include_visa,
                        'price_for_visa'=>$rows->price_for_visa,
                        'price_visa_details'=>$rows->price_visa_details,
                        'price_include_vat'=>$rows->price_include_vat,
                        'status'=>'Y',
                    );
                    $psub_id=DB::table('package_details_sub')->insertGetId($data);

                    $promotion=DB::table('package_promotion')->where('psub_id',$rows->psub_id)->get();
                    if($promotion){
                        foreach ($promotion as $rows){
                            $data=array(
                                'packageDescID'=>$packageDescID,
                                'psub_id'=>$psub_id,
                                'promotion_title'=>$rows->promotion_title,
                                'promotion_operator'=>$rows->promotion_operator,
                                'promotion_date_start'=>$rows->promotion_date_start,
                                'promotion_date_end'=>$rows->promotion_date_end,
                                'every_booking'=>$rows->every_booking,
                                'promotion_operator2'=>$rows->promotion_operator2,
                                'promotion_value'=>$rows->promotion_value,
                                'promotion_unit'=>$rows->promotion_unit,
                                'promotion_status'=>$rows->promotion_status,
                            );
                            DB::table('package_promotion')->insert($data);
                        }
                    }
                }
            }

            $additional=DB::table('package_additional_services')
                ->where('packageDescID',$packdetail->packageDescID)
                ->where('packageID',$packdetail->packageID)
                ->get();
            if($additional){
                foreach ($additional as $rows){
                    $data=array(
                        'packageDescID'=>$packageDescID,
                        'packageID'=>$rows->packageID,
                        'additional_service'=>$rows->additional_service,
                        'price_service'=>$rows->price_service,
                        'status'=>$rows->status,
                    );
                    DB::table('package_additional_services')->insert($data);
                }
            }

//
//            $getCondition=DB::table('package_condition')->where('packageDescID',$id)->get();
//            if(isset($getCondition)){
//
//                foreach ($getCondition as $rows){
//                    $check=DB::table('package_condition')
//                        ->orderby('conditionCode','desc')
//                        ->first();
//                    if(isset($check)){
//                        $conditionCode=$check->conditionCode+1;
//                    }else{
//                        $conditionCode=1;
//                    }
//                    $data = array(
//                        'packageDescID'=>$packageDescID,
//                        'conditionCode'=>$conditionCode,
//                        'packageCondition'=>$rows->packageCondition,
//                        'packageConditionDesc'=>$rows->packageConditionDesc,
//                        'LanguageCode'=>$rows->LanguageCode,
//                    );
//
//                    DB::table('package_condition')->insert($data);
//
//                    $getCondition=DB::table('package_sub_condition')
//                        ->where('conditionCode',$rows->conditionCode)
//                        ->where('packageDescID',$rows->packageDescID)
//                        ->first();
//                    if(isset($getCondition)){
//                        $data = array(
//                            'packageDescID'=>$packageDescID,
//                            'groupCode'=>$getCondition->groupCode,
//                            'conditionCode'=>$conditionCode,
//                            'packageConditionOrder'=>$getCondition->packageConditionOrder,
//                        );
//
//                        DB::table('package_sub_condition')->insert($data);
//                    }
//
//                }
//            }

//            $Condition=DB::table('package_condition as a')
//                ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
//                ->select('a.*','b.condition_group_title')
//                ->where('a.timeline_id',Session::get('timeline_id'))
//                ->groupby('condition_group_id')
//                ->orderby('a.condition_group_id','asc')
//                ->get();
//            // ->toSql();
//            //  dd($Condition);
//            if(isset($Condition)){
//                foreach ($Condition as $rows){
//                    $data=array(
//                        'packageDescID'=>$packageDescID,
//                        'condition_id'=>$rows->condition_code
//                    );
//                    DB::table('condition_in_package_details')->insert($data);
//                }
//            }

        }
        return redirect('package/details');
    }

    public function editDetails($id){
        Session::put('detail','y');
        Session::put('packageDescID',$id);
        $Details=DB::table('package_details')->where('packageDescID',$id)->first();
        $Detail_sub=DB::table('package_details_sub')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Details->packageID)
            ->first();

       // dd($Details);
        $Additional=DB::table('package_additional_services')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();

        $promotion=DB::table('package_promotion')->where('packageDescID',Session::get('packageDescID'))->get();
        if($Details->Country_id!='0'){
            $country=$Details->Country_id;
        }else{
            $country='106';
        }

        $Airline=App\Airline::where('country_id',$country)->active()->get();
        $country=App\Country::active()->get();

//       dd($Details);
        return view('module.packages.setting.edit2-package')
            ->with('promotion',$promotion)
            ->with('country',$country)
            ->with('Package',$Package)
            ->with('Details',$Details)
            ->with('Airline',$Airline)
            ->with('Additional',$Additional)
            ->with('Detail_sub',$Detail_sub);
    }

    public function editPriceDetails($id){

        Session::forget('detail');
        Session::put('packageDescID',$id);
        $Details=DB::table('package_details')->where('packageDescID',$id)->first();
        $Detail_sub=DB::table('package_details_sub')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Details->packageID)
            ->first();

        $Vehicle=DB::table('the_vehicle')->where('language_code',Session::get('language'))->where('status','1')->get();

        $Additional=DB::table('package_additional_services')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();

        $promotion=DB::table('package_promotion')->where('packageDescID',Session::get('packageDescID'))->get();
        if($Details->Country_id!='0'){
            $country=$Details->Country_id;
        }else{
            $country='106';
        }
        $Airline=App\Airline::where('country_id',$country)->active()->get();
        $country=App\Country::active()->get();

       //dd($Details);
        return view('module.packages.setting.edit1-package')
            ->with('promotion',$promotion)
            ->with('Vehicle',$Vehicle)
            ->with('country',$country)
            ->with('Package',$Package)
            ->with('Details',$Details)
            ->with('Airline',$Airline)
            ->with('Additional',$Additional)
            ->with('Detail_sub',$Detail_sub);
    }

    public function editCheckDetails($id){
        Session::put('packageDescID',$id);
        $Details=DB::table('package_details')->where('packageDescID',$id)->first();
        $Detail_sub=DB::table('package_details_sub')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Details->packageID)
            ->first();

       // dd($Details);
        $Additional=DB::table('package_additional_services')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();

        $promotion=DB::table('package_promotion')->where('packageDescID',Session::get('packageDescID'))->get();
        if($Details->Country_id!='0'){
            $country=$Details->Country_id;
        }else{
            $country='106';
        }

        $Airline=App\Airline::where('country_id',$country)->active()->get();
        $country=App\Country::active()->get();

//        dd($Details);
        return view('module.packages.setting.check-edit-package')
            ->with('promotion',$promotion)
            ->with('country',$country)
            ->with('Package',$Package)
            ->with('Details',$Details)
            ->with('Airline',$Airline)
            ->with('Additional',$Additional)
            ->with('Detail_sub',$Detail_sub);
    }

    //************************** Price Details ****************************************

    public function priceDetails($id){
       // App::setLocale(Session::get('language'));
        Session::put('packageDescID',$id);
        $Details=DB::table('package_details')->where('packageDescID',$id)->first();

        $Detail_sub=DB::table('package_details_sub')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();
        $Package=DB::table('package_tour')->where('packageID',$Details->packageID)->first();

        $Additional=DB::table('package_additional_services')
            ->where('packageDescID',$Details->packageDescID)
            ->where('packageID',$Details->packageID)
            ->get();
        //dd($Additional);
        return view('module.packages.setting.price-package')
//            ->with('promotion',$promotion)
            ->with('Package',$Package)
            ->with('Details',$Details)
            ->with('Additional',$Additional)
            ->with('Detail_sub',$Detail_sub);
    }
    
    public function create_priceDetails(){
        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        return view('module.packages.setting.price.create')->with('Package',$Package);
    }

    public function showRateCust(Request $request){
        $Rate=DB::table('package_tour_commission_rate')
            ->where('price_rate_start','<=',$request->price)
            ->where('price_rate_end','>=',$request->price)
            ->first();

        if(!$Rate){
            $Rate=DB::table('package_tour_commission_rate')
                ->orderby('commission_rate','asc')
                ->first();
        }

        return $request->price+($request->price*$Rate->commission_rate/100);

    }

    public function showRateSale(Request $request){
        $Rate=DB::table('package_tour_commission_rate')
            ->where('price_rate_start','<=',$request->price)
            ->where('price_rate_end','>=',$request->price)
            ->first();

        if(!$Rate){
            $Rate=DB::table('package_tour_commission_rate')
                ->orderby('commission_rate','asc')
                ->first();
        }

        return $request->price-($request->price*$Rate->commission_rate/100);

    }

    public function save_priceDetail(Request $request){
        $post=$request->all();
        if(isset($post['price_system_default']) && $post['price_system_default']=='Y'){
            DB::table('package_details_sub')->where('packageDescID', Session::get('packageDescID'))->update(['price_system_default'=>'N']);
        }

        $additional_id=0;
        if(isset($post['need_additional'])){
            if($post['need_additional']=='Y'){
                $data_sub = array(
                    'packageID' => Session::get('package'),
                    'packageDescID' => Session::get('packageDescID'),
                    'additional_service' => $post['additional_service'],
                    'price_service' => intval($post['price_service']),
                    'condition_check_first' => $post['condition_check_first'],
                    'status' => 'Y',
                );
                $additional_id=DB::table('package_additional_services')->insertGetId($data_sub);
            }
        }


        $priceSale=isset($post['PriceSale'])?$post['PriceSale']:'';
        $priceShow=$post['price_show_customer'];
        if(!$priceSale){
            $priceSale=$priceShow;
        }
        $data_sub = array(
                    'packageID' => Session::get('package'),
                    'packageDescID' => Session::get('packageDescID'),
                    'TourType' => $post['TourType'],
                    'additional_id' => $additional_id,
                    'PriceSale' => intval($priceSale),
                    'price_system_fees' => intval($priceShow),
                    'price_include_visa' => isset($post['price_include_visa'])?$post['price_include_visa']:'',
                    'price_include_vat' => isset($post['price_include_vat'])?$post['price_include_vat']:'',
                    'price_for_visa' => $post['price_for_visa'],
                    'price_visa_details' => $post['price_visa_details'],
//                    'price_system_default' => isset($post['price_system_default'])?$post['price_system_default']:'',
                    'Price_by_promotion' => intval($priceSale),
                    'status' => 'Y',
                );
                DB::table('package_details_sub')->insert($data_sub);

        return redirect('package/details/price/'.Session::get('packageDescID'));
    }

    public function update_priceDetail(Request $request){

        $post=$request->all();
        if(isset($post['price_system_default']) && $post['price_system_default']=='Y'){
            DB::table('package_details_sub')->where('packageDescID', Session::get('packageDescID'))->update(['price_system_default'=>'N']);
        }
        $additional_id=0;
        if(isset($post['need_additional'])){
            if($post['need_additional']=='Y') {
                $additional_id = $post['additional_id'] ? $post['additional_id'] : '0';
                $data_sub = array(
                    'packageID' => Session::get('package'),
                    'packageDescID' => Session::get('packageDescID'),
                    'additional_service' => $post['additional_service'],
                    'price_service' => intval($post['price_service']),
                    'condition_check_first' => isset($post['condition_check_first'])?$post['condition_check_first']:'',
                    'status' => 'Y',
                );
                if ($additional_id == 0) {
                    $additional_id = DB::table('package_additional_services')->insertGetId($data_sub);
                } else {
                    DB::table('package_additional_services')->where('id', $additional_id)->update($data_sub);
                }
            }
        }

        $priceSale=isset($post['PriceSale'])?$post['PriceSale']:'';
        $priceShow=$post['price_show_customer'];
        if(!$priceSale){
            $priceSale=$priceShow;
        }
        $data_sub = array(
            'packageID' => Session::get('package'),
            'packageDescID' => Session::get('packageDescID'),
            'TourType' => $post['TourType'],
            'PriceSale' => intval($priceSale),
            'additional_id' => $additional_id,
            'price_system_fees' => intval($priceSale),
            'price_include_visa' => isset($post['price_include_visa'])?$post['price_include_visa']:'',
            'price_include_vat' => isset($post['price_include_vat'])?$post['price_include_vat']:'',
            'price_for_visa' => $post['price_for_visa'],
            'price_visa_details' => $post['price_visa_details'],
//                    'price_system_default' => isset($post['price_system_default'])?$post['price_system_default']:'',
            'Price_by_promotion' => intval($priceSale),
        );
        DB::table('package_details_sub')->where('psub_id',$post['id'])->update($data_sub);

//        return back();
        return redirect('package/details/price/'.Session::get('packageDescID'));
    }


    public function edit_priceDetail($id){
       // App::setLocale(Session::get('language'));
        $Rate=DB::table('package_details_sub')->where('psub_id',$id)->first();
        Session::put('package',$Rate->packageID);
        Session::put('packageDescID',$Rate->packageDescID);

        $Package=DB::table('package_tour')->where('packageID',Session::get('package'))->first();
        $Additional=DB::table('package_additional_services')->where('id',$Rate->additional_id)->first();

        return view('module.packages.setting.price.edit')
            ->with('Rate',$Rate)
            ->with('Additional',$Additional)
            ->with('Package',$Package);
    }

    public function delete_priceDetail($id){

        $Rate=DB::table('package_details_sub')->where('psub_id',$id)->delete();

        return back();
    }






    //************************************************************************

    public function saveDetail(Request $request){
        $post = $request->all();
        // $count = sizeof($post['TourType']);
        // dd($post['TourType'][0]);
        // dd($post['TourType']);
        $v = \Validator::make($request->all(), [
            'package_date_start' => 'required',
            'package_date_end' => 'required',
//            'packageDateEnd' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
//            $d=explode('-',$post['package_date']);
//            $packageDateStart=date('Y-m-d',strtotime($d[0]));
//            $packageDateEnd=date('Y-m-d',strtotime($d[1]));

            $packageDateStart=date('Y-m-d',strtotime($post['package_date_start']));
            $packageDateEnd=date('Y-m-d',strtotime($post['package_date_end']));

            if(isset($post['Airline'])){
                $Airline=$post['Airline'];
            }else{
                $Airline="";
            }

            $season=isset($post['season'])?$post['season']:'';
            $season_name=isset($post['season_name'])?$post['season_name']:'';
            $closing_date = date('Y-m-d', strtotime("-" . $post['number_of_days_the_sale_is_closed'] . " day",strtotime($packageDateEnd)));
            $data = array(
                'packageID' => Session::get('package'),
                'packageDateStart' => $packageDateStart,
                'packageDateEnd' => $packageDateEnd,
                'closing_date' => $closing_date,
                'season' => $season,
                'season_name' => $season_name,
                'travel_by' => isset($post['travel_by'])?$post['travel_by']:'',
                'Flight' => isset($post['Flight'])?$post['Flight']:'',
                'Airline' => $Airline,
                'Country_id' => isset($post['Country_id'])?$post['Country_id']:'',
                'use_airplan' => $post['use_airplan'],
                'NumberOfPeople' => $post['NumberOfPeople'],
                'number_of_days_the_sale_is_closed' => $post['number_of_days_the_sale_is_closed'],
                'Commission' => $post['Commission'],
                'Commission_sell' => $post['Commission_sell'],
                'unit_commission' => $post['unit_commission'],
                'status' => 'Y',
            );
//            DB::table('package_details')->insert($data);
            $packageDescID=DB::table('package_details')->insertGetId($data);


//            if($packageDateStart> date('Y-m-d')){
//                $data=array(
//                    'packageStatus'=>'CP',
//                    'package_close'=>'N',
//                    'LastUpdate'=>date('Y-m-d H:i:s')
//                );
//
//                DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
//            }

            $Condition=DB::table('package_condition as a')
                ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
                ->select('a.*','b.condition_group_title')
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->orderby('a.condition_group_id','asc')
                ->get();

            // ->toSql();
          //  dd($Condition);
//            if(isset($Condition)){
//                foreach ($Condition as $rows){
//                    $data=array(
//                        'packageDescID'=>$packageDescID,
//                        'condition_id'=>$rows->condition_code
//                    );
//                    DB::table('condition_in_package_details')->insert($data);
//                }
//            }
            if(Session::has('checking')) {
                return redirect('/package/details_check/' . Session::get('package'));
            }elseif(Session::get('event')=='details'){
                return redirect('/package/details/'.Session::get('package'));
            }else{
                return redirect('/package/details');
            }
        }
    }

    public function updateDetail(Request $request){
        $post = $request->all();

        $v = \Validator::make($request->all(), [
            'package_date_start' => 'required',
            'package_date_end' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $packageDateStart=date('Y-m-d',strtotime($post['package_date_start']));
            $packageDateEnd=date('Y-m-d',strtotime($post['package_date_end']));
            $season=isset($post['season'])?$post['season']:'';
            $season_name=isset($post['season_name'])?$post['season_name']:'';
            $Filght=isset($post['Flight'])?$post['Flight']:'';
            $Airplan=isset($post['Airline'])?$post['Airline']:'';
            if($post['use_airplan']==1){
                $Filght='';$Airplan='';
            }
            $closing_date = date('Y-m-d', strtotime("-" . $post['number_of_days_the_sale_is_closed'] . " day",strtotime($packageDateEnd)));
            $data = array(
                'packageID' => Session::get('package'),
                'packageDateStart' => $packageDateStart,
                'packageDateEnd' => $packageDateEnd,
                'closing_date' => $closing_date,
                'season' => $season,
                'season_name' => $season_name,
                'travel_by' => isset($post['travel_by'])?$post['travel_by']:'',
                'Flight' => $Filght,
                'Airline' => $Airplan,
                'Country_id' => isset($post['Country_id'])?$post['Country_id']:'',
                'NumberOfPeople' => $post['NumberOfPeople'],
                'number_of_days_the_sale_is_closed' => $post['number_of_days_the_sale_is_closed'],
                'use_airplan' => $post['use_airplan'],
                'Commission' => $post['Commission'],
                'Commission_sell' => $post['Commission_sell'],
                'unit_commission' => $post['unit_commission'],
            );
           // dd($data);
            $packageDescID=DB::table('package_details')->where('packageDescID',$post['packageDescID'])->update($data);

            if($packageDateStart> date('Y-m-d')){
                $data=array(
                    'packageStatus'=>'CP',
                    'package_close'=>'N',
                    'LastUpdate'=>date('Y-m-d H:i:s')
                );
                DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
            }

            if(isset($post['checking'])) {
                return redirect('package/details_check/' . Session::get('package'));
            }elseif(Session::get('detail')=='y'){
                return redirect('package/details/'.Session::get('package'));
            }else{
                return redirect('/package/details');
            }
        }
    }

    public function updatePackageDetail(Request $request){
        $post = $request->all();

        $v = \Validator::make($request->all(), [
            'package_date' => 'required',
//            'packageDateEnd' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $d=explode('-',$post['package_date']);
            $packageDateStart=date('Y-m-d',strtotime($d[0]));
            $packageDateEnd=date('Y-m-d',strtotime($d[1]));

            $data = array(
                'packageID' => Session::get('package'),
                'packageDateStart' => $packageDateStart,
                'packageDateEnd' => $packageDateEnd,
                'Flight' => $post['Flight'],
                'Airline' => $post['Airline'],
                'Country_id' => $post['Country_id'],
                'NumberOfPeople' => $post['NumberOfPeople'],
                'Commission' => $post['Commission'],
                'unit_commission' => $post['unit_commission'],
            );
            $packageDescID=DB::table('package_details')
                ->where('packageDescID',$post['packageDescID'])
                ->update($data);

            if($packageDateStart> date('Y-m-d')){
                $data=array(
                    'packageStatus'=>'CP',
                    'package_close'=>'N',
                    'LastUpdate'=>date('Y-m-d H:i:s')
                );

                DB::table('package_tour')->where('packageID',Session::get('package'))->update($data);
            }


            if(isset($post['TourType'])){
                DB::table('package_details_sub')
                    ->where('packageDescID',$post['packageDescID'])
                    ->where('packageID', Session::get('package'))
                    ->delete();

                $count = sizeof($post['TourType']);

                for ($i=0; $i < $count; $i++) {
                    $data_sub = array(
                        'packageID' => Session::get('package'),
                        'packageDescID' => $post['packageDescID'],
                        'TourType' => $post['TourType'][$i],
                        'PriceSale' => intval($post['PriceSale'][$i]),
                        'Price_by_promotion' => intval($post['PriceSale'][$i]),
//                        'PriceAndTicket' => intval($post['PriceAndTicket'][$i]),
                    );
                  
                    DB::table('package_details_sub')->insert($data_sub);
//                        ->where('packageDescID',$packageDescID)
//                        ->update($data_sub);
                }
            }

            if(isset($post['additional_service'])) {
                DB::table('package_additional_services')
                    ->where('packageDescID', $post['packageDescID'])
                    ->where('packageID', Session::get('package'))
                    ->delete();
                $count = sizeof($post['additional_service']);
                for ($i = 0; $i < $count; $i++) {
                    $data_sub = array(
                        'packageID' => Session::get('package'),
                        'packageDescID' => $post['packageDescID'],
                        'additional_service' => $post['additional_service'][$i],
                        'price_service' => intval($post['price_service'][$i]),
                    );
                   
                    DB::table('package_additional_services')->insert($data_sub);
                }
            }

            return redirect('/package/details');

        }
    }


    public function createRate($id){
        return view('module.packages.rate.create')->with('packageDescID',$id);
    }

    public function editRate(Request $request){
        dd($request->id);
//        return view('module.packages.rate.edit')->with('packageDescID',$id);
    }

    public function saveRate(Request $request){
        $post=$request->all();
        if(isset($post['TourType'])){
            $count = sizeof($post['TourType']);
            for ($i=0; $i < $count; $i++) {
                $data_sub = array(
                    'packageID' => Session::get('package'),
                    'packageDescID' => $post['packageDescID'],
                    'TourType' => $post['TourType'][$i],
                    'PriceSale' => intval($post['PriceSale'][$i]),
                    'PriceAndTicket' => intval($post['PriceAndTicket'][$i]),
                );
                DB::table('package_details_sub')
                    ->insert($data_sub);
            }
        }
        return redirect('package/details');
    }

    public function showRate($id){
        $Rate=DB::table('package_details_sub')->where('packageDescID',$id)->get();
       // dd($Rate);
        return view('module.packages.setting.show-rate')
            ->with('packageDescID',$id)
            ->with('Rate',$Rate);
    }

    public function delRate(Request $request){
        $check=DB::table('package_details_sub')->where('psub_id',$request->psub_id)->delete();
        if($check){
            $Rate=DB::table('package_details_sub')->where('packageDescID',$request->packageDescID)->get();
            $output="";
            foreach ($Rate as $rows) {
                $output.="<tr>
                       <td style=\"width: 12%\">
                           <a data-id=\"".$rows->psub_id."\" class=\"btn btn-sm btn-danger delete-rate\"><i class=\"fa fa-trash\"></i> </a>
                       </td>
                       <td>".$rows->TourType."</td>
                       <td><strong>".number_format($rows->PriceSale)."</strong></td>
                       <td><strong>".number_format($rows->PriceAndTicket)."</strong></td>
                   </tr>";
            }
        }
        return Response($output);
    }

    public function Settype(Request $request)
    {
        if ($request->ajax()) {
            $check = DB::table('tour_category_sub1')
                ->where('LanguageCode', Auth::user()->language)
                ->where('TourCategoryID', $request->type)
                ->get();
            //  dd($check);
            $data = "";
            if (isset($check)) {
                foreach ($check as $rows) {
                    $data .= "<promotion value=\"$rows->groupID\">$rows->TourCategorySub1Name</promotion>";
                }
            }
            return Response($data);
        }
    }

    public function del_details($id){
        $i=DB::table('package_details')->where('packageDescID',$id)->delete();
        DB::table('package_details_sub')->where('packageDescID',$id)->delete();
        DB::table('package_promotion')->where('packageDescID',$id)->delete();
        DB::table('package_additional_services')->where('packageDescID',$id)->delete();
        //DB::table('condition_in_package_details')->where('packageDescID',$id)->delete();
        if($i>0){
            return redirect('/package/details');
        }
    }

    public function delete($id){

        $data=array('packageStatus'=>'X');
        $i=DB::table('package_tour')->where('packageID',$id)->update($data);
        if($i>0){
            return redirect('/package/manage');
        }
    }

    public function set_credit_image($id){
        $Image=DB::table('media')->where('id',$id)->first();

        return view('module.packages.program.credit')->with('Image',$Image);
    }

    public function updateCredit(Request $request){
         $data=array(
             'photo_credit_type'=>'1',
             'photo_credit_name'=>$request->photo_credit_name,
             'photo_credit_url'=>$request->photo_credit_url,
         );
         DB::table('media')->where('id',$request->id)->update($data);
         return redirect('package/program/highlight/'.Session::get('program_id'));
    }


    public function highlight($id){
        ini_set('memory_limit', '-1');
        Session::put('program_id',$id);

        $Category=DB::table('package_tour_category')->where('packageID',Session::get('package'))->groupby('TourCategoryID')->get();
        foreach ($Category as $rows){
            $category[]=$rows->TourCategoryID;
        }

        $locations=DB::table('locations')
            ->whereIn('category_id',$category)
            //->where('timeline_id',Session::get('timeline_id'))
            ->get();
       // dd($locations);
        $mylocations = Location::where('active', 1)

            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->latest()
                    ->where('active','1');
            })
            ->whereIn('id',function ($query){
                $query->select('LocationID')->from('highlight_in_schedule')
                ->where('programID',Session::get('program_id'));
            })

            ->groupby('timeline_id')
            ->skip(15)->take(0)
            ->paginate(15,['*'],'mylocations');

       // dd($mylocations);

        $locations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->whereIn('category_id',$category)
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')
            // ->get();
            ->paginate(15,['*'],'locations');

       // dd($locations);
        return view('module.packages.program.highlight')
                ->with('mylocations',$mylocations)
                ->with('locations',$locations);
    }

    public function search_location_highlight(Request $request){

        $Category=DB::table('package_tour_category')->where('packageID',Session::get('package'))->get();

        foreach ($Category as $rows){
            $category[]=$rows->TourCategoryID;
        }
        $search=$request->txtsearch;
      
        $locations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->whereIn('timeline_id',function ($query) use($search){
                $query->select('timeline_id')->from('timeline_info')
                    ->where('name','like','%'.$search.'%')
                    ->orwhere('username','like','%'.$search.'%');
            })
            ->OrwhereIn('timeline_id',function ($query) use($search){
                $query->select('id')->from('timelines')
                    ->where('name','like','%'.$search.'%')
                    ->orwhere('username','like','%'.$search.'%');
            })
           // ->whereIn('category_id',$category)
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')
            // ->get();
            ->paginate(15,['*'],'locations');

      //  dd($locations);

        $mylocations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->latest()
                    ->where('active','1');
            })
            ->whereIn('id',function ($query){
                $query->select('LocationID')->from('highlight_in_schedule')
                    ->where('programID',Session::get('program_id'));
            })
            ->groupby('timeline_id')
            ->skip(15)->take(0)
            ->paginate(15,['*'],'mylocations');
      //  dd($locations);
        return view('module.packages.program.highlight')
            ->with('mylocations',$mylocations)
            ->with('locations',$locations);
    }
    
}


