<?php

namespace App\Http\Controllers\Package;

use App\City;
use App\CitySub1;
use App\Country;
use App\Media;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use Validator;
use Image;
use Response;

use Illuminate\Support\Facades\Input;


class UploadController extends Controller
{

    public function Upload_location(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $file = $request->file("file");
        if($file) {
            $path1 = storage_path() . '/uploads/locations/avatars/mid/';
            $path2 = storage_path() . '/uploads/locations/avatars/small/';
            $path3 = storage_path() . '/uploads/locations/avatars/x-small/';


            $imageName = md5(rand(11111, 99999)) . '_' . time() . '.jpg';

            $path_1 = $path1 . $imageName;
            $path_2 = $path2 . $imageName;
            $path_3 = $path3 . $imageName;


            list($ori_w, $ori_h) = getimagesize($file);
            if ($ori_w >= 900) {
                if ($ori_w >= $ori_h) {
                    $new_w = 900;
                    $new_h = round(($new_w / $ori_w) * $ori_h);
                } else {
                    $new_h = 650;
                    $new_w = round(($new_h / $ori_h) * $ori_w);
                }
            } else {
                $new_w = $ori_w;
                $new_h = $ori_h;
            }

            Image::make($file)->resize($new_w, $new_h)->save($path_1);

            if ($ori_w >= 650) {
                if ($ori_w >= $ori_h) {
                    $new_w = 650;
                    $new_h = round(($new_w / $ori_w) * $ori_h);
                } else {
                    $new_h = 480;
                    $new_w = round(($new_h / $ori_h) * $ori_w);
                }
            } else {
                $new_w = $ori_w;
                $new_h = $ori_h;
            }
            Image::make($file)->resize($new_w, $new_h)->save($path_2);

            if ($ori_w >= 480) {
                if ($ori_w >= $ori_h) {
                    $new_w = 480;
                    $new_h = round(($new_w / $ori_w) * $ori_h);
                } else {
                    $new_h = 350;
                    $new_w = round(($new_h / $ori_h) * $ori_w);
                }
            } else {
                $new_w = $ori_w;
                $new_h = $ori_h;
            }
            Image::make($file)->resize($new_w, $new_h)->save($path_3);

            $data['result'] = true;
            $data['filename'] = $imageName;
            header('Access-Control-Allow-Origin: *');
            header('Content-type: application/json');
            echo json_encode($data);
        }else{
            $data['filename'] = 'No image uploaded';
            header('Access-Control-Allow-Origin: *');
            header('Content-type: application/json');
            echo json_encode($data);
        }
    }

    public function delete_image_location(Request $request){
        ini_set('memory_limit', '-1');
        $Image=Media::where('id',$request->media_id)->first();
        if($Image){
            $path1 = storage_path().'/uploads/locations/avatars/mid/'.$Image->source;
            $path2 = storage_path().'/uploads/locations/avatars/small/'.$Image->source;
            $path3 = storage_path().'/uploads/locations/avatars/x-small/'.$Image->source;
            if(file_exists($path1)){
                unlink($path1);
            }
            if(file_exists($path2)){
                unlink($path2);
            }
            if(file_exists($path3)){
                unlink($path3);
            }
            DB::table('media')->where('id',$Image->id)->delete();
        }
    }


    public function Upload_post(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $file = $request->file("file");
        if($file){



        $path1 = storage_path().'/uploads/users/gallery/mid/';
        $path2 = storage_path().'/uploads/users/gallery/small/';
        $path3 = storage_path().'/uploads/users/gallery/x-small/';

        $imageName = md5(rand(11111, 99999)).'_' .time().'.jpg';

        $path_1 = $path1.$imageName;
        $path_2 = $path2.$imageName;
        $path_3 = $path3.$imageName;


        list($ori_w, $ori_h) = getimagesize($file);
        if ($ori_w >= 900) {
            if ($ori_w >= $ori_h) {
                $new_w = 900;
                $new_h = round(($new_w / $ori_w) * $ori_h);
            } else {
                $new_h = 650;
                $new_w = round(($new_h / $ori_h) * $ori_w);
            }
        } else {
            $new_w = $ori_w;
            $new_h = $ori_h;
        }



        Image::make($file)->resize($new_w, $new_h)->save($path_1);

        if ($ori_w >= 650) {
            if ($ori_w >= $ori_h) {
                $new_w = 650;
                $new_h = round(($new_w / $ori_w) * $ori_h);
            } else {
                $new_h = 480;
                $new_w = round(($new_h / $ori_h) * $ori_w);
            }
        } else {
            $new_w = $ori_w;
            $new_h = $ori_h;
        }
        Image::make($file)->resize($new_w, $new_h)->save($path_2);

        if ($ori_w >= 480) {
            if ($ori_w >= $ori_h) {
                $new_w = 480;
                $new_h = round(($new_w / $ori_w) * $ori_h);
            } else {
                $new_h = 350;
                $new_w = round(($new_h / $ori_h) * $ori_w);
            }
        } else {
            $new_w = $ori_w;
            $new_h = $ori_h;
        }
        Image::make($file)->resize($new_w, $new_h)->save($path_3);

            $data['result'] = true;
            $data['filename'] = $imageName;
            header('Access-Control-Allow-Origin: *');
            header('Content-type: application/json');
            echo json_encode($data);
        }else{
            $data['filename'] = 'No image uploaded';
            header('Access-Control-Allow-Origin: *');
            header('Content-type: application/json');
            echo json_encode($data);
        }
    }

    public function delete_image_post(Request $request){
        ini_set('memory_limit', '-1');
        $Image=Media::where('id',$request->media_id)->first();
        if($Image){
            $path1 = storage_path().'/uploads/users/gallery/mid/'.$Image->source;
            $path2 = storage_path().'/uploads/users/gallery/small/'.$Image->source;
            $path3 = storage_path().'/uploads/users/gallery/x-small/'.$Image->source;
            if(file_exists($path1)){
               unlink($path1);
            }
            if(file_exists($path2)){
                unlink($path2);
            }
            if(file_exists($path3)){
                unlink($path3);
            }
            DB::table('media')->where('id',$Image->id)->delete();
        }
    }

    //
    public function document_list($id=null){
        if($id){
            Session::put('passport_id',$id) ;
        }
        $Docs=DB::table('users_passport_files')
                ->where('passport_id',Session::get('passport_id'))
                ->get();
//        Session::put('mode','member');
        return view('packages.document.list')
            ->with('Docs',$Docs);
    }

    public function create(){
        $Docs=DB::table('users_passport_files')->where('passport_id',Session::get('passport_id'))->get();
        return view('packages.document.create-file')->with('Docs',$Docs);
    }

    public function upload(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
//        dd($request->all());
//        $input = Input::all();
//        $input = $request->all();


        $rules = array(
            'file' => 'image|max:30000',
        );
        //$dir=base_path() . '/images/member/docs/';
        $dir=public_path('/images/member/docs/') ;

//        $validation = Validator::make($input, $rules);
//
//        if ($validation->fails()) {
//            return Response::make($validation->errors->first(), 400);
//        }
        $type='image';

//        $source=Input::file('file')->getClientOriginalName();
//        dd($input->originalName);

//        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
        $extension=$request->file('file')->getClientOriginalExtension();
        // dd($extension);
        $fileName = date('YmdHi').rand(110, 999) . '.' . $extension; // renameing image
        if($extension=='pdf' || $extension=='doc' || $extension=='xls' || $extension=='docx' || $extension=='xlsx') {
               $type='file';
        }

        if(Session::get('passport_id')){
            $UserID=Session::get('passport_id');
        }else{
            $Check=DB::table('users_passport')->orderby('passport_id','asc')->first();
            $UserID=$Check->passport_id;
            Session::put('passport_id',$UserID);
        }
            $upload_success = $request->file('file')->move($dir, $fileName); //

        if ($upload_success) {
            $data=array(
                'passport_id'=>$UserID,
                'created_at'=>date('Y-m-d H:i:s'),
                'file_name'=>$fileName,
                'file_type'=>$type,
            );
            DB::table('users_passport_files')->insert($data);

            $Docs=DB::table('users_passport_files')
                ->where('passport_id',Session::get('passport_id'))
                ->get();

            return view('packages.document.create-file')->with('Docs',$Docs);
        } else {
            return Response::json('error', 400);
        }

    }


    public function memberDocs() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $input = Input::all();

        $rules = array(
            'file' => 'image|max:30000',
        );
        //$dir=base_path() . '/images/member/docs/';
        $dir=public_path('/images/member/docs/') ;

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }
        $source=Input::file('file')->getClientOriginalName();
        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
       // dd($extension);
        $fileName = date('YmdHi').rand(110, 999) . '.' . $extension; // renameing image
        $Pics=Input::file('file');
        if($extension=='pdf' || $extension=='doc' || $extension=='xls' || $extension=='docx' || $extension=='xlsx'){
            $upload_success = Input::file('file')->move($dir, $fileName); // uploading file to given path
            $type='file';
        }else{
            list($ori_w, $ori_h) = getimagesize($Pics);

            if ($ori_w >= 1000) {
                if ($ori_w >= $ori_h) {
                    $new_w = 1000;
                    $new_h = round(($new_w / $ori_w) * $ori_h);
                } else {
                    $new_h = 650;
                    $new_w = round(($new_h / $ori_h) * $ori_w);
                }
            } else {
                $new_w = $ori_w;
                $new_h = $ori_h;
            }
            $upload_success=Image::make($Pics)->resize($new_w, $new_h)->save($dir. $fileName);
            $type='image';
        }


        if(Session::get('passport_id')){
            $UserID=Session::get('passport_id');
        }else{
            $Check=DB::table('users_passport')->orderby('passport_id','asc')->first();
            $UserID=$Check->passport_id;
            Session::put('passport_id',$UserID);
        }

        if ($upload_success) {
            $data=array(
                'passport_id'=>$UserID,
                'created_at'=>date('Y-m-d H:i:s'),
                'file_name'=>$fileName,
                'source'=>$source,
                'file_type'=>$type,
            );
            DB::table('users_passport_files')->insert($data);
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function delete($id){
        $check=DB::table('users_passport_files')->where('id',$id)->first();
       // dd($check);
        if($check){
            $dir=public_path('/images/member/docs/'.$check->file_name) ;
            if(file_exists($dir)){
                unlink($dir);
            }
            DB::table('users_passport_files')->where('id',$id)->delete();
        }

        $Docs=DB::table('users_passport_files')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        return view('packages.document.list')
            ->with('Docs',$Docs);

    }


    public function remove(Request $request){
        $check=DB::table('users_passport_files')->where('source',$request->name)->first();
        if($check){
            DB::table('users_passport_files')->where('source',$request->name)->delete();
            $dir=public_path('/images/member/docs/'.$check->source) ;
            unlink($dir);
            return Response::json('success', 200);
        }else{
            return Response::json('error', 400);
        }
    }


}
