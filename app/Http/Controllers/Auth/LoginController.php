<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Setting;
use App\Timeline;
use Teepluss\Theme\Facades\Theme;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Location;

use Session;
use App\Category;
use App\Country;
use App\CategorySub1;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function administrator(){
        if(Auth::check()){
            return redirect('member/manage/all');
        }else{
            return view('auth.login-admin');
        }
    }

    public function getLogin()
    {
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');
        $theme->setTitle(trans('auth.login').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        //dd($theme);
        return $theme->scope('landing',compact('user',''))->render();
    }


    public function locationFeed(Request $request)
    {

        return redirect('/');
        if(!Session::get('language')){
            Session::put('language',\App::getLocale());
        }

        $mode = "showfeed";
        $user_post = 'group';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');

        $locations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')
            ->paginate(12,['*'],'locations');

        if ($request->ajax) {
            $responseHtml = '';
            foreach ($locations as $location) {
                $timeline=$location->timeline()->first();
                $post=Post::where('timeline_id',$location->timeline_id)->latest()->first();
                $media=Media::where('id',$timeline->avatar_id)->first();
                $responseHtml .= $theme->partial('location', ['post' => $post, 'timeline' => $timeline,'media' => $media, 'next_page_url' => $locations->appends(['ajax' => true])->nextPageUrl()]);
            }
            return $responseHtml;
        }

        $Category=Category::where('language_code',Session::get('language'))->where('category_type','0')->orderby('name','asc')->pluck('name', 'category_id')->all();
        if(!$Category){
            $Category=Category::where('language_code','en')->where('category_type','0')->orderby('name','asc')->pluck('name', 'category_id')->all();
        }

        $Category_sub=CategorySub1::where('language_code',Session::get('language'))->pluck('sub1_name', 'category_sub1_id')->all();
        if(!$Category_sub){
            $Category_sub=CategorySub1::where('language_code','en')->pluck('sub1_name', 'category_sub1_id')->all();
        }

        $Country=Country::where('language_code',Session::get('language'))->pluck('country', 'country_id')->all();
        if(!$Country){
            $Country=Country::where('language_code','en')->pluck('country', 'country_id')->all();

        }

        $category_options = ['' => 'Select Category'] + $Category;
        $category_sub1_options = ['' => 'Select Category'] + $Category_sub;
        $country_options = ['' => 'Select Country'] + $Country;

        //dd($category_options);
        //dd(Auth::user()->username);
      //  dd('test'.Session::get('language'));

        $next_page_url = url('ajax/get-location-more-feed?page=2&ajax=true');
        $theme->setTitle('Locations | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('landing', compact('locations','timeline','category_options','category_sub1_options','country_options', 'next_page_url', 'mode', 'user_post'))
            ->render();
    }

    public function login(Request $request)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if (!$validate->passes()) {
            return response()->json(['status' => '201', 'message' => trans('auth.login_failed')]);
        }else{
            $user = '';
            $nameoremail = '';
            $canLogin = false;
            $remember = ($request->remember ? true : false);

            if (filter_var(($request->email), FILTER_VALIDATE_EMAIL)  == true) {
                $nameoremail = $request->email;
                $user = DB::table('users')->where('email', $request->email)->first();
            } else {
                $timeline = DB::table('timelines')->where('username', $request->email)->first();
                if ($timeline != null) {
                    $user = DB::table('users')->where('timeline_id', $timeline->id)->first();
                    if ($user) {
                        $nameoremail = $user->email;
                    }

                }
            }

            if (Setting::get('mail_verification') == 'off') {
                $canLogin = true;
            } else {
                if ($user != null) {
                    if ($user->email_verified == 1) {
                        $canLogin = true;
                    } else {
                       return response()->json(['status' => '201', 'message' => trans('messages.verify_mail')]);
                    }
                }
            }
        }

        if ($canLogin && Auth::attempt(['email' => $nameoremail, 'password' => $request->password], $remember)) {

            if($request->package=='Y'){
                return redirect('home/package/14');
            }

            return response()->json(['status' => '200', 'message' => trans('auth.login_success')]);
        } else {
            return response()->json(['status' => '201', 'message' => trans('auth.login_failed')]);
        }
    }

    public function login2(Request $request)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if (!$validate->passes()) {

            return response()->json(['status' => '201', 'message' => trans('auth.login_failed')]);
        } else {
            $user = '';
            $nameoremail = '';
            $canLogin = false;
            $remember = ($request->remember ? true : false);

            if (filter_var(($request->email), FILTER_VALIDATE_EMAIL)  == true) {
                $nameoremail = $request->email;
                $user = DB::table('users')->where('email', $request->email)->first();
            } else {
                $timeline = DB::table('timelines')->where('username', $request->email)->first();
                if ($timeline != null) {
                    $user = DB::table('users')->where('timeline_id', $timeline->id)->first();
                    if ($user) {
                        $nameoremail = $user->email;
                    }
                }
            }

            if (Setting::get('mail_verification') == 'off') {
                $canLogin = true;
            } else {
                if ($user != null) {
                    if ($user->email_verified == 1) {
                        $canLogin = true;
                    } else {
                       return response()->json(['status' => '201', 'message' => trans('messages.verify_mail')]);
                    }
                }
            }
        }

        if ($canLogin && Auth::attempt(['email' => $nameoremail, 'password' => $request->password], $remember)) {
            //->json(['status' => '200', 'message' => trans('auth.login_success')]);\
            DB::table('package_booking_cart')
                ->where('session_id',Session::get('cart_session_id'))
                ->where('auth_id','0')
                ->update(['auth_id'=>Auth::user()->id]);
            if(Session::get('url_back')){
                return redirect(Session::get('url_back'));
            }

            if(Session::get('from_email')=='yes') {
                return redirect('booking/show/invoice/' . Session::get('invoice_booking_id') . '/1');
            }

            if($request->login_type=='order' || $request->package=='Y') {
                return redirect('booking/u/none');

            }else{
                return back();
            }

        } else {
            Session::flash('message','กรุณาตรวจสอบ อีเมล์หรือรหัสผ่านไม่ถูกต้อง');
            return redirect()->back();
//            return response()->json(['status' => '201', 'message' => trans('auth.login_failed')]);
        }
    }

    public function login_admin(Request $request)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if (!$validate->passes()) {

            return response()->json(['status' => '201', 'message' => trans('auth.login_failed')]);
        } else {
            $user = '';
            $nameoremail = '';
            $canLogin = false;
            $remember = ($request->remember ? true : false);

            if (filter_var(($request->email), FILTER_VALIDATE_EMAIL)  == true) {
                $nameoremail = $request->email;
                $user = DB::table('users')->where('email', $request->email)->first();
            } else {
                $timeline = DB::table('timelines')->where('username', $request->email)->first();
                if ($timeline != null) {
                    $user = DB::table('users')->where('timeline_id', $timeline->id)->first();
                    if ($user) {
                        $nameoremail = $user->email;
                    }
                }
            }

            if (Setting::get('mail_verification') == 'off') {
                $canLogin = true;
            } else {
                if ($user != null) {
                    if ($user->email_verified == 1) {
                        $canLogin = true;
                    } else {
                       return response()->json(['status' => '201', 'message' => trans('messages.verify_mail')]);
                    }
                }
            }
        }

        if ($canLogin && Auth::attempt(['email' => $nameoremail, 'password' => $request->password], $remember)) {
            //->json(['status' => '200', 'message' => trans('auth.login_success')]);\
            return redirect('member/manage/all');

        } else {
            Session::flash('message','กรุณาตรวจสอบ อีเมล์หรือรหัสผ่านไม่ถูกต้อง');
            return redirect()->back();

        }
    }


}
