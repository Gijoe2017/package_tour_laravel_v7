<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Timeline;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use File;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'emails', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }


    protected function validator2(array $data, $captcha = null)
    {
        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];
        $rules = [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6',
//            'username'  => 'required|max:25|min:5|alpha_num|unique:timelines|no_admin',
//            'affiliate' => 'exists:timelines,username',
        ];

        if ($captcha) {
            $messages = ['g-recaptcha-response.required' => trans('messages.captcha_required')];
            $rules['g-recaptcha-response'] = 'required';
        }

        return Validator::make($data, $rules, $messages);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register()
    {
        if(Auth::check()){
            return Redirect::to('/');
        }
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');
        $theme->setTitle(trans('auth.register').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('landing_backup')->render();
    }


    public function register2()
    {
        if(Auth::check()){
            return Redirect::to('/');
        }


        return view('auth.login_order');
    }


    public function check_email(Request $request){
        $check=DB::table('users')->where('email',$request->email)->first();
        if($check){
            return "Yes";
        }else{
            return "No";
        }
    }


    protected function registerUser2(Request $request, $socialLogin = false)
    {
      //  dd(Session::get('cart_session_id'));
        if (Setting::get('captcha') == 'on' && !$socialLogin) {
            $validator = $this->validator2($request->all(), true);
        } else {
            $validator = $this->validator2($request->all());
        }
          //dd($validator);
        if ($validator->fails()) {
            $err_result=$validator->errors()->toArray();

            Session::flash('err_result',$err_result);
            return \redirect('register/2');
//            return response()->json(['status' => '201', 'err_result' => $validator->errors()->toArray()]);
        }

        if ($request->affiliate) {
            $timeline = Timeline::where('username', $request->affiliate)->first();
            $affiliate_id = $timeline->user->id;
        } else {
            $affiliate_id = null;
        }

        //Create timeline record for the user
        $timeline = Timeline::create([
            'username' => $request->username,
            'name'     => $request->name,
            'type'     => 'user',
            'about'    => 'write something about yourself'
        ]);
        if(Setting::get('mail_verification') == 'off')
        {
            $mail_verification = 1;
        }
        else
        {
            $mail_verification = null;
        }

        //Create user record
        $email=$request->email;
        $remember=str_random(10);
        $user = User::create([
            'email'             => $email,
            'name'              => $request->username,
            'password'          => bcrypt($request->password),
            'timeline_id'       => $timeline->id,
            'gender'            => 'male',
            'language'          => 'th',
            'affiliate_id'      => $affiliate_id,
            'verification_code' => str_random(30),
            'remember_token'    => $remember,
            'email_verified'    => $mail_verification
        ]);

        if (Setting::get('birthday') == 'on' && $request->birthday != '') {
            $user->birthday = date('Y-m-d', strtotime($request->birthday));
            $user->save();
        }

        if (Setting::get('city') == 'on' && $request->city != '') {
            $user->city = $request->city;
            $user->save();
        }
//        $user->email = $request->email;
//        $user->save();
        //saving default settings to user settings

        $user_settings = [
            'user_id'               => $user->id,
            'confirm_follow'        => Setting::get('confirm_follow'),
            'follow_privacy'        => Setting::get('follow_privacy'),
            'comment_privacy'       => Setting::get('comment_privacy'),
            'timeline_post_privacy' => Setting::get('user_timeline_post_privacy'),
            'post_privacy'          => Setting::get('post_privacy'),
            'message_privacy'       => Setting::get('user_message_privacy'), ];

        //Create a record in user settings table.
        $userSettings = DB::table('user_settings')->insert($user_settings);

//        $user->name = $timeline->name;
//        $user->email = $request->email;


        if ($user) {
            $data=array(
                'UserID'=>$user->id,
                'FirstName'=>$request->name,
                'LastName'=>'-',
                'LanguageCode'=>'th'
            );
            DB::table('users_info')->insert($data);

            if ($socialLogin) {
                return $timeline;
            } else {
                $chk = '';
                if (Setting::get('mail_verification') == 'on') {
                    $chk = 'on';
                    Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user) {
                        $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                        $m->to($user->email, $user->name)->subject('Welcome to '.Setting::get('site_name'));
                    });
                }

                Auth::login($user);

                DB::table('package_booking_cart')
                    ->where('session_id',Session::get('cart_session_id'))
                    ->where('auth_id','0')
                    ->update(['auth_id'=>Auth::user()->id]);



                    return redirect('booking/continuous/step2');
//                }else{
//                    return redirect('/register/2')->with(['message' => trans('common.registered'), 'status' => 'success']);
//                }



//                return response()->json(['status' => '200', 'message' => trans('auth.verify_email'), 'emailnotify' => $chk]);
//                 return response()->json(['status' => '200', 'err_result' => trans('common.registered')]);
            }
        }


    }

}
