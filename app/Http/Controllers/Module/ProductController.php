<?php

namespace App\Http\Controllers\Module;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    //
    public function index(){
        $Products=Products::where('CreateByID',Auth::user()->id)->all();
        return view('module.ecommerce.product.list')
            ->with('Products',$Products);
    }
}
