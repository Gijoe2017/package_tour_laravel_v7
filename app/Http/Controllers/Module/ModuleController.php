<?php

namespace App\Http\Controllers\Module;
use App\Http\Controllers\Controller;

use App\Http\Middleware\Language;
use App\Location;
use App\ModuleVerified;
use App\User;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;

class ModuleController extends Controller
{
    //
    public function register($id){
        Session::put('module',$id);
        $country=DB::table('countries')->where('language_code',Auth::user()->language)->get();
        $banktype=DB::table('account_type')->where('language_code',Auth::user()->language)->get();
        $Business_type=DB::table('business_type')->where('language_code',Auth::user()->language)->get();
        $banks=DB::table('banks')->where('language_code',Auth::user()->language)->get();

        return view('module.register')
            ->with('banks',$banks)
            ->with('banktype',$banktype)
            ->with('Business_type',$Business_type)
            ->with('country',$country);
    }

    public function documents($id){
        $Docs=DB::table('business_verified_documents')->where('timeline_id',$id)->get();
        return view('module.document');
    }


    public function store_register(Request $request){
        $post=$request->all();
        $type_id=$post['business_type_id'];
        if(isset($post['business_type'])){
            $check=DB::table('business_type')->orderby('business_type_id','desc')->first();
            if($check){
                $type_id=$check->business_type_id+1;
            }
            $data=array(
                'business_type_id'=>$type_id,
                'business_type'=>$post['business_type'],
                'language_code'=>Auth::user()->language,
            );
            DB::table('business_type')->insert($data);
        }

        $data=array(
            'timeline_id'=>$post['timeline_id'],
            'tax_id'=>$post['tax_id'],
            'business_type'=>$type_id,
            'country_id'=>$post['country_id'],
            'state_id'=>$post['state_id'],
            'city_id'=>$post['city_id'],
            'city_sub1_id'=>$post['city_sub1_id'],
            'zip_code'=>$post['zip_code'],
            'created_user_id'=>Auth::user()->id,
            'created_site_id'=>'1',
        );
        DB::table('business_verified_info1')->insert($data);

        $data=array(
            'timeline_id'=>$post['timeline_id'],
            'legal_name'=>$post['legal_name'],
            'address'=>$post['address'],
            'phone'=>$post['phone'],
            'email'=>$post['email'],
            'additional'=>$post['additional'],
            'language_code'=>Auth::user()->language,
            'created_user_id'=>Auth::user()->id,
            'created_site_id'=>'1',
        );
        DB::table('business_verified_info2')->insert($data);
        $bank_code=$post['bank_code'];
        if(isset($post['bank_name'])){
            $check=DB::table('banks')->orderby('bank_code','desc')->first();
            if($check){
                $bank_code=sprintf('%03d',$check->bank_code+1);
            }
            $data=array(
                'bank_name'=>$post['bank_name'],
                'bank_code'=>$bank_code,
                'swift_code'=>'',
                'language_code'=>Auth::user()->language
            );
            DB::table('banks')->insert($data);
        }

        $data=array(
            'timeline_id'=>$post['timeline_id'],
            'bank_account_number'=>$post['bank_account_number'],
            'account_name'=>$post['account_name'],
            'bank_id'=>$bank_code,
            'bank_name'=>$post['bank_name'],
            'country_id'=>$post['bank_country_id'],
            'bank_type'=>$post['bank_type'],
            'created_user_id'=>Auth::user()->id,
            'created_site_id'=>'1',
        );
        DB::table('business_verified_bank')->insert($data);

        $data=array(
            'timeline_id'=>$post['timeline_id'],
            'module_type_id'=>Session::get('module'),
            'business_verified_status'=>'peddling',
            'created_user_id'=>Auth::user()->id,
            'created_site_id'=>'1',
        );
        $i=DB::table('business_verified_modules')->insert($data);

        if($i>0){
            Session::flash('message','Successful');
        }
        return back();

    }

    public function store_document(Request $request){

    }

    public function list_users($id){
        $Users=DB::table('users as a')
            ->join('users_info as b','b.UserID','=','a.id')
            ->where('b.LanguageCode',Session::get('language'))
            ->where('active','1')->get();
        $location_id=Location::where('timeline_id',$id)->first()->id;
//        $Users=DB::table('users as a')
//            ->join('users_info as b','b.UserID','=','a.id')
//            ->where('b.LanguageCode',Session::get('language'))
//            ->whereIn('a.id',function ($query) use($location_id){
//                $query->select('user_id')->from('location_user')->where('location_id',$location_id);
//            })
//            ->get();
        return view('module.users.list')->with('Users',$Users)->with('location_id',$location_id);
    }


    public function add_users($id,$location_id){
        $data=array(
            'location_id'=>$location_id,
            'user_id'=>$id,
            'role_id'=>'1',
            'owner'=>'0',
            'active'=>'1',
            'created_at'=>date('Y-m-d H:i:s')
        );
        DB::table('location_user')->insert($data);

        return back();
    }

    public function module_list(){
        $Module=DB::table('business_verified_modules as a')
            ->join('business_verified_modules_type as b','b.module_type_id','=','a.module_type_id')
            ->join('timelines as c','c.id','=','a.timeline_id')
            ->where('b.language_code','th')
            ->get();

        return view('module.list')->with('Module',$Module);
    }

    public function module_edit($id){
        $Business=DB::table('business_verified_info2')->where('timeline_id',$id)->first();
        $BankInfo=DB::table('business_verified_bank')->where('timeline_id',$id)->first();
        $Module=DB::table('business_verified_modules')->where('timeline_id',$id)->first();
        $country=DB::table('countries')->where('language_code',Auth::user()->language)->get();
        $banktype=DB::table('account_type')->where('language_code',Auth::user()->language)->get();
        $banks=DB::table('banks')->where('language_code',Auth::user()->language)->get();
        return view('module.module_edit')
            ->with('country',$country)
            ->with('banktype',$banktype)
            ->with('banks',$banks)
            ->with('Business',$Business)
            ->with('BankInfo',$BankInfo)
            ->with('Module',$Module);
    }


    public function manage($id){

        Session::put('module_id',$id);

        if($id=='1') {
            return redirect('/package/manage');
        }else if($id=='3'){
            return redirect('module/ecommerce/product/list');
        }else{
            return view('module.manage');
        }

    }
    

}
