<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use App\Album;
use App\Announcement;
use App\Category;
use App\CategorySub1;
use App\CategorySub2;
use App\Comment;
use App\Country;
use App\State;
use App\City;
use App\CitySub1;
use App\Event;
use App\Group;
use App\Hashtag;
use App\Http\Requests\CreateTimelineRequest;
use App\Http\Requests\UpdateTimelineRequest;
use App\Media;
use App\Notification;
use App\Location;
use App\PublicAlbum;
use App\Page;
use App\Post;
use App\Repositories\TimelineRepository;
use App\Role;
use App\Setting;
use App\Timeline;
use App\User;
use App\Wallpaper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use DB;
use Flash;
use Flavy;
use URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use LinkPreview\LinkPreview;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use Session;
use Teepluss\Theme\Facades\Theme;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Date;
use Jenssegers\Agent\Agent;


class TimelineController extends AppBaseController
{
    /** @var TimelineRepository */
    private $timelineRepository;

    public function __construct(TimelineRepository $timelineRepo, Request $request)
    {
        $this->timelineRepository = $timelineRepo;
        $this->watchEventExpires();
        $this->request = $request;
//        $this->checkCensored();

    }

    public function star_rating(Request $request){
      //  $timeline = Timeline::where('username', Auth::user()->username)->first();

        $check=DB::table('star_rating')
            ->where('timeline_id',$request->timeline_id)
            ->where('user_id',Auth::user()->id)
            ->where('rate_group',$request->group)
            ->count();

        if($check>0){
            $data=array(
                'timeline_id'=>$request->timeline_id,
                'rate_group'=>$request->group,
                'star_rating'=>$request->id,
                'user_id'=>Auth::user()->id,
//                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            );
            DB::table('star_rating')
                ->where('timeline_id',$request->timeline_id)
                ->where('user_id',Auth::user()->id)
                ->where('rate_group',$request->group)
                ->update($data);
        }else{
            $data=array(
                'timeline_id'=>$request->timeline_id,
                'rate_group'=>$request->group,
                'star_rating'=>$request->id,
                'user_id'=>Auth::user()->id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            );
            DB::table('star_rating')
                ->insert($data);
        }
        return "success!";
    }


   public function GetLocation(){
        $Locations = Timeline::where('type','location')
           ->where('language_code',Auth::user()->location)
           ->get();
        return Response($Locations);
//       foreach ($Locations as $rows){
//            $data[]=$rows->name;
//       }
//       return Response($data);

   }


    public function CheckLocation(Request $request){
        $Locations = Timeline::where('type','location')
            ->where('location_id',$request->location_id)
            ->get();
    }


    public function FindLocation(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return Response::json([]);
        }

        $Locations = Timeline::where('type','location')
//            ->where('language_code',Auth::user()->location)
            ->where('name','Like','%'.$term.'%')
            ->get();

        // $formatted_tags = [];

        $i=0;$j=0;

        foreach ($Locations as $Location) {

            $city=null;$state=null;
            $timeline=Timeline::where('id',$Location->id)->first();
            $country=$timeline->country()->where('language_code',$Location->language_code)->first();
//            $country=Country::where('language_code',Auth::user()->language)
//                ->where('country_id',$Location->country_id)
//                ->first();
            $address=null;
            if($country){

                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',$Location->language_code)->first();
                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',$Location->language_code)->first();

                $address=$state->state.' '.$country->country;
            }
            
            $formatted_tags[] = ['id' => $Location->location_id, 'text' => $Location->name.' '.$address ];
            if($i==$j) {
                $formatted_tags[] = ['id' => $term, 'text' => $term];
            }
            $j++;
        }

        //dd($formatted_tags);

        if(!count($Locations)){
            $keep=$term;
            $term='+Add new location "'.$term.'"';
            $formatted_tags[] = ['id' => $keep, 'text' => $term];
        }
        return Response::json($formatted_tags);
    }



    protected function checkCensored()
    {
        $messages['not_contains'] = 'The :attribute must not contain banned words';
        if($this->request->method() == 'POST') {
            // Adjust the rules as needed
            $data=array(
                'name'          => 'not_contains',
                'about'         => 'not_contains',
                'title'         => 'not_contains',
                'description'   => 'not_contains',
                'tag'           => 'not_contains',
                'email'         => 'email not_contains',
                'body'          => 'not_contains',
                'link'          => 'not_contains',
                'address'       => 'not_contains',
                'website'       => 'not_contains',
                'display_name'  => 'not_contains',
                'key'           => 'not_contains',
                'value'         => 'not_contains',
                'subject'       => 'not_contains',
                'username'      => 'not_contains'
            );
            $this->validate($this->request,$data,$messages);
        }
    }

    public function watchEventExpires()
    {   
        if(Auth::user())
        {
            $events = Event::where('user_id',Auth::user()->id)->get();

            if($events)
            {
                foreach ($events as $event) {
                    if(strtotime($event->end_date) < strtotime('-2 week'))
                    {
                        //Deleting Events
                        
                         $event->users()->detach();
                         $event->pages()->detach();
                            // Deleting event posts
                                $event_posts = $event->timeline()->with('posts')->first();
                        
                                if(count($event_posts->posts) != 0)
                                {
                                    foreach($event_posts->posts as $post)
                                    {
                                        $post->deleteMe();
                                    }
                                }
                                //Deleting event notifications
                                $timeline_alerts = $event->timeline()->with('notifications')->first();

                                if(count($timeline_alerts->notifications) != 0)
                                {
                                    foreach($timeline_alerts->notifications as $notification)
                                    {
                                        $notification->delete();
                                    }
                                }

                            $event_timeline = $event->timeline();
                            $event->delete();
                            $event_timeline->delete();

                    }
                }

            }
        } 
        
    }

    /**
     * Display a listing of the Timeline.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->timelineRepository->pushCriteria(new RequestCriteria($request));
        $timelines = $this->timelineRepository->all();

        return view('timelines.index')
            ->with('timelines', $timelines);
    }


    /**
     * Show the form for creating a new Timeline.
     *
     * @return Response
     */
    public function create()
    {
        return view('timelines.create');
    }

    /**
     * Store a newly created Timeline in storage.
     *
     * @param CreateTimelineRequest $request
     *
     * @return Response
     */
    public function store(CreateTimelineRequest $request)
    {
        $input = $request->all();

        $timeline = $this->timelineRepository->create($input);

        Flash::success('Timeline saved successfully.');

        return redirect(route('timelines.index'));
    }

     /**
      * Display the specified Timeline.
      *
      * @param  int $id
      *
      * @return Response
      */
    public function homeTimeline($username)
    {

        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $posts = [];
        $timeline = Timeline::where('username', $username)->first();

        $user_post = '';
        if ($timeline == null) {
            return redirect('/');
        }
     
        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
       // dd($timeline);
        foreach ($timeline_posts as $timeline_post) {
            //This is for filtering reported(flag) posts, displaying non flag posts
            if ($timeline_post->check_reports($timeline_post->id) == false) {
                array_push($posts, $timeline_post);
            }
        }
        
          //  dd($timeline->country()->first()->country);
        //dd($timeline->type);
        if ($timeline->type == 'user') {
            $follow_user_status = '';
            $timeline_post_privacy = '';
            $timeline_post = '';

            $user = User::where('timeline_id', $timeline['id'])->first();
            $own_pages = $user->own_pages();
            $own_locations = $user->own_locations();

            $own_groups = $user->own_groups();
            $liked_pages = $user->pageLikes()->get();
            $liked_locations = $user->locationLikes()->get();
            $joined_groups = $user->groups()->get();
            $joined_groups_count = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get()->count();
            $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
            $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
            $followRequests = $user->followers()->where('status', '=', 'pending')->get();
            $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
            $guest_events = $user->getEvents();

            $follow_user_status = DB::table('followers')->where('follower_id', '=', Auth::user()->id)
                               ->where('leader_id', '=', $user->id)->first();

            if ($follow_user_status) {
                $follow_user_status = $follow_user_status->status;
            }

            $confirm_follow_setting = $user->getUserSettings(Auth::user()->id);
            $follow_confirm = $confirm_follow_setting->confirm_follow;

           //get user settings
            $live_user_settings = $user->getUserPrivacySettings(Auth::user()->id, $user->id);
           // dd($live_user_settings);
            $privacy_settings = explode('-', $live_user_settings);
            $timeline_post = $privacy_settings[0];
            $user_post = $privacy_settings[1];

        } elseif ($timeline->type == 'page') {
            $page = Page::where('timeline_id', '=', $timeline->id)->first();
            $page_members = $page->members();
            $user_post = 'page';
        } elseif ($timeline->type == 'public_album') {
            $public_album = PublicAlbum::where('timeline_id', '=', $timeline->id)->first();
            //dd($public_album);
            $public_album_members = $public_album->members();
            $user_post = 'public_album';

        } elseif ($timeline->type == 'location') {
            $location = Location::where('timeline_id', '=', $timeline->id)->first();
            $location_members = $location->members();
            $user_post = 'location';

        } elseif ($timeline->type == 'group') {
            $group = Group::where('timeline_id', '=', $timeline->id)->first();
            $group_members = $group->members();
            $group_events = $group->getEvents($group->id);
            $ongoing_events = $group->getOnGoingEvents($group->id);
            $upcoming_events = $group->getUpcomingEvents($group->id);
            $user_post = 'group';

        } elseif ($timeline->type == 'event') {
            $user_post = 'event';
            $event = Event::where('timeline_id', '=', $timeline->id)->first();
        }
        $city=null;$state=null;
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        if($timeline->state_id){
            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        }


       // dd($city);
       // dd($state);
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        //dd($location);
        return $theme->scope('users/timeline', compact('user', 'timeline', 'posts', 'liked_pages','liked_locations', 'timeline_type', 'page','public_album','location','country','city','state', 'group', 'next_page_url', 'joined_groups', 'follow_user_status', 'followRequests', 'following_count', 'followers_count', 'timeline_post', 'user_post', 'follow_confirm', 'joined_groups_count', 'own_pages','own_locations', 'own_groups', 'group_members', 'page_members','public_album_members', 'location_members','event', 'user_events', 'guest_events', 'username', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    public function locationHomeGroup(Request $request,$group)
    {
        $mode = "showfeed";
        $user_post = 'group';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');

        if($group=='other'){
            $cate=['30','14','23','29'];
            $locations = Location::where('active', 1)
                ->whereIn('timeline_id',function ($query){
                    $query->select('timeline_id')->from('posts')
                        ->where('active','1');
                })
                ->whereNotIn('category_id',$cate)
                ->orderby('last_posted_at','desc')
                ->groupby('timeline_id')
                // ->get();
                ->paginate(12,['*'],'locations');
        }else{
            $locations = Location::where('active', 1)
                ->whereIn('timeline_id',function ($query){
                    $query->select('timeline_id')->from('posts')
                        ->where('active','1');
                })
                ->where('category_id',$group)
                ->orderby('last_posted_at','desc')
                ->groupby('timeline_id')
                // ->get();
                ->paginate(12,['*'],'locations');
        }

//        $locations = Location::where('active', 1)
//            ->whereIn('timeline_id',function ($query){
//                $query->select('timeline_id')->from('posts')
//                    ->where('active','1');
//            })
//            ->orderby('last_posted_at','desc')
//            ->groupby('timeline_id')
//            ->paginate(12,['*'],'locations');

      //  dd($locations);


        if(!Session::get('language')){
            Session::put('language',\App::getLocale());
        }

        if ($request->ajax) {
            $responseHtml = '';
            foreach ($locations as $location) {
                $timeline=$location->timeline()->first();
                $post=Post::where('timeline_id',$location->timeline_id)->latest()->first();
                $media=Media::where('id',$timeline->avatar_id)->first();
                $responseHtml .= $theme->partial('location', ['post' => $post, 'timeline' => $timeline,'media' => $media, 'next_page_url' => $locations->appends(['ajax' => true])->nextPageUrl()]);
            }
            return $responseHtml;
        }


        $category_options = ['' => 'Select Category'] + Category::where('language_code',Session::get('language'))->where('category_type','0')->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Session::get('language'))->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Session::get('language'))->pluck('country', 'country_id')->all();

        //dd(Auth::user()->username);
        //dd($user_post);

        $next_page_url = url('ajax/get-location-more-feed?page=2&ajax=true');
        $theme->setTitle('Locations | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        // dd($theme);
        return $theme->scope('landing', compact('locations','timeline','category_options','category_sub1_options','country_options', 'next_page_url', 'mode', 'user_post'))
            ->render();
    }



    public function showHomeTimeline($username)
    {
        if(!Session::get('language')){
            Session::put('language',\App::getLocale());
        }


        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $posts = [];
        $timeline = Timeline::where('username', $username)->first();

        Session::put('username',$username);

        $user_post = '';
        if ($timeline == null) {
            return redirect('/');
        }

        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
       // dd($timeline);
        foreach ($timeline_posts as $timeline_post) {
            //This is for filtering reported(flag) posts, displaying non flag posts
            if ($timeline_post->check_reports($timeline_post->id) == false) {
                array_push($posts, $timeline_post);
            }
        }

          //  dd($timeline->country()->first()->country);
        //dd($timeline->type);
        if ($timeline->type == 'user') {
            $follow_user_status = '';
            $timeline_post_privacy = '';
            $timeline_post = '';

            $user = User::where('timeline_id', $timeline->id)->first();

            $own_pages = $user->own_pages();
            $own_locations = $user->own_locations();

            $own_groups = $user->own_groups();

            $liked_pages = $user->pageLikes()->get();
            $liked_locations = $user->locationLikes()->get();
            $joined_groups = $user->groups()->get();

            $joined_groups_count = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get()->count();
            $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
            $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
            $followRequests = $user->followers()->where('status', '=', 'pending')->get();
            $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
            //dd($user);
           // $guest_events = $user->getEvents();

            $follow_user_status = DB::table('followers')->where('follower_id', '=', $user->id)
                               ->where('leader_id', '=', $user->id)->first();

            if ($follow_user_status) {
                $follow_user_status = $follow_user_status->status;
            }
            $confirm_follow_setting = $user->getUserSettings($user->id);
            $follow_confirm = $confirm_follow_setting->confirm_follow;

           //get user settings
            $live_user_settings = $user->getUserPrivacySettings($user->id, $user->id);
           // dd($live_user_settings);
            $privacy_settings = explode('-', $live_user_settings);
            $timeline_post = $privacy_settings[0];
            $user_post = $privacy_settings[1];

        } elseif ($timeline->type == 'page') {
            $page = Page::where('timeline_id', '=', $timeline->id)->first();
            $page_members = $page->members();
            $user_post = 'page';
        } elseif ($timeline->type == 'public_album') {
            $public_album = PublicAlbum::where('timeline_id', '=', $timeline->id)->first();
            //dd($public_album);
            $public_album_members = $public_album->members();
            $user_post = 'public_album';

        } elseif ($timeline->type == 'location') {
            $location = Location::where('timeline_id', '=', $timeline->id)->first();
            $location_members = $location->members();
            $user_post = 'location';

        } elseif ($timeline->type == 'group') {
            $group = Group::where('timeline_id', '=', $timeline->id)->first();
            $group_members = $group->members();
            $group_events = $group->getEvents($group->id);
            $ongoing_events = $group->getOnGoingEvents($group->id);
            $upcoming_events = $group->getUpcomingEvents($group->id);
            $user_post = 'group';

        } elseif ($timeline->type == 'event') {
            $user_post = 'event';
            $event = Event::where('timeline_id', '=', $timeline->id)->first();
        }
        $city=null;$state=null;
        $country=$timeline->country()->where('language_code',Session::get('language'))->first();
        if($timeline->state_id){
            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
        }
        if(!$country){
            $country=$timeline->country()->where('language_code','en')->first();
        }


        //dd($city);
       // dd($state);
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
       // dd($timeline);
        return $theme->scope('users/timeline-home', compact('user', 'timeline', 'posts', 'liked_pages','liked_locations', 'timeline_type', 'page','public_album','location','country','city','state', 'group', 'next_page_url', 'joined_groups', 'follow_user_status', 'followRequests', 'following_count', 'followers_count', 'timeline_post', 'user_post', 'follow_confirm', 'joined_groups_count', 'own_pages','own_locations', 'own_groups', 'group_members', 'page_members','public_album_members', 'location_members','event', 'user_events', 'guest_events', 'username', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }



    public function showTimeline($username)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();

        $posts = [];
        $timeline = Timeline::where('username', $username)->first();
        //dd($timeline);
        $user_post = '';
        if ($timeline == null) {
//            return redirect('/');
        }

        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));

       // dd($timeline);
        foreach ($timeline_posts as $timeline_post) {
            //This is for filtering reported(flag) posts, displaying non flag posts
            if ($timeline_post->check_reports($timeline_post->id) == false) {
                array_push($posts, $timeline_post);
            }
        }

        if ($timeline->type == 'user') {
            $follow_user_status = '';
            $timeline_post_privacy = '';
            $timeline_post = '';

            $user = User::where('timeline_id', $timeline['id'])->first();

            $own_pages = $user->own_pages();
            $own_locations = $user->own_locations();

            $own_groups = $user->own_groups();
            $liked_pages = $user->pageLikes()->get();
            $liked_locations = $user->locationLikes()->get();
            $joined_groups = $user->groups()->get();
            $joined_groups_count = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get()->count();
            $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
            $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
            $followRequests = $user->followers()->where('status', '=', 'pending')->get();
            $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
            $guest_events = $user->getEvents();

            $follow_user_status = DB::table('followers')->where('follower_id', '=', Auth::user()->id)
                               ->where('leader_id', '=', $user->id)->first();

            if ($follow_user_status) {
                $follow_user_status = $follow_user_status->status;
            }

            $confirm_follow_setting = $user->getUserSettings(Auth::user()->id);
            $follow_confirm = $confirm_follow_setting->confirm_follow;

           //get user settings
            $live_user_settings = $user->getUserPrivacySettings(Auth::user()->id, $user->id);
           // dd($live_user_settings);
            $privacy_settings = explode('-', $live_user_settings);
            $timeline_post = $privacy_settings[0];
            $user_post = $privacy_settings[1];

        } elseif ($timeline->type == 'page') {
            $page = Page::where('timeline_id', '=', $timeline->id)->first();
            $page_members = $page->members();
            $user_post = 'page';
        } elseif ($timeline->type == 'public_album') {
            $public_album = PublicAlbum::where('timeline_id', '=', $timeline->id)->first();
            //dd($public_album);
            $public_album_members = $public_album->members();
            $user_post = 'public_album';

        } elseif ($timeline->type == 'location') {
            $location = Location::where('timeline_id', '=', $timeline->id)->first();
            $location_members = $location->members();
            $user_post = 'location';

        } elseif ($timeline->type == 'group') {
            $group = Group::where('timeline_id', '=', $timeline->id)->first();
            $group_members = $group->members();
            $group_events = $group->getEvents($group->id);
            $ongoing_events = $group->getOnGoingEvents($group->id);
            $upcoming_events = $group->getUpcomingEvents($group->id);
            $user_post = 'group';

        } elseif ($timeline->type == 'event') {
            $user_post = 'event';
            $event = Event::where('timeline_id', '=', $timeline->id)->first();
        }
        $city=null;$state=null;
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        if($timeline->state_id){
            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        }

       // dd($city);
      // dd($state);
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        //dd($location);
        return $theme->scope('users/timeline', compact('user', 'timeline', 'posts', 'liked_pages','liked_locations', 'timeline_type', 'page','public_album','location','country','city','state', 'group', 'next_page_url', 'joined_groups', 'follow_user_status', 'followRequests', 'following_count', 'followers_count', 'timeline_post', 'user_post', 'follow_confirm', 'joined_groups_count', 'own_pages','own_locations', 'own_groups', 'group_members', 'page_members','public_album_members', 'location_members','event', 'user_events', 'guest_events', 'username', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }




    public function showTimelinePopup($username)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $posts = [];
        $timeline = Timeline::where('username', $username)->first();

        $user_post = '';
        if ($timeline == null) {
            return redirect('/');
        }

        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
        //dd($timeline);
        foreach ($timeline_posts as $timeline_post) {
            //This is for filtering reported(flag) posts, displaying non flag posts
            if ($timeline_post->check_reports($timeline_post->id) == false) {
                array_push($posts, $timeline_post);
            }
        }

          //  dd($timeline->country()->first()->country);
          //  dd($timeline->type);

        if ($timeline->type == 'user') {
            $follow_user_status = '';
            $timeline_post_privacy = '';
            $timeline_post = '';

            $user = User::where('timeline_id', $timeline['id'])->first();
            $own_pages = $user->own_pages();
            $own_locations = $user->own_locations();

            $own_groups = $user->own_groups();
            $liked_pages = $user->pageLikes()->get();
            $liked_locations = $user->locationLikes()->get();
            $joined_groups = $user->groups()->get();
            $joined_groups_count = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get()->count();
            $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
            $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
            $followRequests = $user->followers()->where('status', '=', 'pending')->get();
            $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
            $guest_events = $user->getEvents();

            $follow_user_status = DB::table('followers')->where('follower_id', '=', Auth::user()->id)
                               ->where('leader_id', '=', $user->id)->first();

            if ($follow_user_status) {
                $follow_user_status = $follow_user_status->status;
            }

            $confirm_follow_setting = $user->getUserSettings(Auth::user()->id);
            $follow_confirm = $confirm_follow_setting->confirm_follow;

           //get user settings
            $live_user_settings = $user->getUserPrivacySettings(Auth::user()->id, $user->id);
           // dd($live_user_settings);
            $privacy_settings = explode('-', $live_user_settings);
            $timeline_post = $privacy_settings[0];
            $user_post = $privacy_settings[1];

        } elseif ($timeline->type == 'page') {
            $page = Page::where('timeline_id', '=', $timeline->id)->first();
            $page_members = $page->members();
            $user_post = 'page';
        } elseif ($timeline->type == 'public_album') {
            $public_album = PublicAlbum::where('timeline_id', '=', $timeline->id)->first();
            //dd($public_album);
            $public_album_members = $public_album->members();
            $user_post = 'public_album';

        } elseif ($timeline->type == 'location') {
            $location = Location::where('timeline_id', '=', $timeline->id)->first();
            $location_members = $location->members();
            $user_post = 'location';

        } elseif ($timeline->type == 'group') {
            $group = Group::where('timeline_id', '=', $timeline->id)->first();
            $group_members = $group->members();
            $group_events = $group->getEvents($group->id);
            $ongoing_events = $group->getOnGoingEvents($group->id);
            $upcoming_events = $group->getUpcomingEvents($group->id);
            $user_post = 'group';

        } elseif ($timeline->type == 'event') {
            $user_post = 'event';
            $event = Event::where('timeline_id', '=', $timeline->id)->first();
        }
        $city=null;$state=null;
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        if($timeline->state_id){
            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        }


       // dd($city);
       // dd($state);
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        //dd($location);
        return $theme->scope('users/timeline-popup', compact('user', 'timeline', 'posts', 'liked_pages','liked_locations', 'timeline_type', 'page','public_album','location','country','city','state', 'group', 'next_page_url', 'joined_groups', 'follow_user_status', 'followRequests', 'following_count', 'followers_count', 'timeline_post', 'user_post', 'follow_confirm', 'joined_groups_count', 'own_pages','own_locations', 'own_groups', 'group_members', 'page_members','public_album_members', 'location_members','event', 'user_events', 'guest_events', 'username', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    public function getMorePosts(Request $request)
    {
        $timeline = Timeline::where('username', $request->username)->first();
        $user = User::where('timeline_id', $timeline['id'])->first();
       // dd($timeline);
       // $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
        $posts = $user->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
        $theme = Theme::uses('default')->layout('default');
        $responseHtml = '';
        foreach ($posts as $post) {
            $responseHtml .= $theme->partial('post', ['post' => $post, 'timeline' => $timeline, 'next_page_url' => $posts->appends(['username' => $request->username])->nextPageUrl()]);
        }
        return $responseHtml;
    }

    public function getMoreReview(Request $request)
    {
        $timeline = Timeline::where('username', $request->username)->first();
        $user = User::where('timeline_id', $timeline['id'])->first();
//        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
        $rate_group=$request->rate_group;

//        $Rating=DB::table('star_rating')
//            ->where('timeline_id',$timeline->id)
//            ->where('user_id',Auth::user()->id)
//            ->where('rate_group',$rate_group)
//            ->first();
//
//
//        if($rate_group=='1') {
//            $rate_group_title = trans('common.over_all_rating');
//        }else if($rate_group=='2'){
//            $rate_group_title=trans('common.inside_rating');
//        }else if($rate_group=='3'){
//            $rate_group_title=trans('common.outside_rating');
//        }else if($rate_group=='4'){
//            $rate_group_title=trans('common.toilet_rating');
//        }else if($rate_group=='5'){
//            $rate_group_title=trans('common.parking_rating');
//        }else if($rate_group=='6'){
//            $rate_group_title=trans('common.cleanliness');
//        }else if($rate_group=='7'){
//            $rate_group_title=trans('common.normal_parking');
//        }else if($rate_group=='8'){
//            $rate_group_title=trans('common.normal_toilet');
//        }


        $posts = $user->posts()->where('active', 1)
            ->where('rate_group',$rate_group)
            ->orderBy('created_at', 'desc')
            ->with('comments')->paginate(Setting::get('items_page'));
       // dd($posts);

        $theme = Theme::uses('default')->layout('default');
        $responseHtml = '';
        foreach ($posts as $post) {
            $responseHtml .= $theme->partial('post', ['post' => $post, 'timeline' => $timeline, 'next_page_url' => $posts->appends(['username' => $request->username])->nextPageUrl()]);
        }
        return $responseHtml;
    }

    public function showFeed(Request $request)
    {
        ini_set('memory_limit', '-1');
        $mode = "showfeed";
        $user_post = 'showfeed';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
       // dd($user_post);
        $timeline = Timeline::where('username', Auth::user()->username)->first();
        //dd($timeline);
        $id = Auth::id();
      //  dd($timeline);
        $trending_tags = trendingTags();

        $suggested_users = suggestedUsers();

        $suggested_groups = suggestedGroups();

        $suggested_pages = suggestedPages();
       // dd($trending_tags);
        $suggested_locations = suggestedLocations();
        $suggested_public_albums = suggestedPublicAlbums();


       // dd($suggested_public_albums);
        // Check for hashtag
        if ($request->hashtag) {

            $hashtag = '#'.$request->hashtag;
            $posts = Post::where('description', 'like', "%{$hashtag}%")->where('active', 1)->latest()->paginate(Setting::get('items_page'));

        } else { // else show the normal feed
            //dd($trending_tags);
            $posts = Post::whereIn('user_id', function ($query) use ($id) {
                $query->select('leader_id')
                    ->from('followers')
                    ->where('follower_id', $id);
            })->orWhere('user_id', $id)->where('active', 1)->latest()->paginate(Setting::get('items_page'));
        }

        if ($request->ajax) {
            $responseHtml = '';
            foreach ($posts as $post) {
                $responseHtml .= $theme->partial('post', ['post' => $post, 'timeline' => $timeline, 'next_page_url' => $posts->appends(['ajax' => true, 'hashtag' => $request->hashtag])->nextPageUrl()]);
            }
            return $responseHtml;
        }

        $announcement = Announcement::find(Setting::get('announcement'));
        if ($announcement != null) {
            $chk_isExpire = $announcement->chkAnnouncementExpire($announcement->id);
            if ($chk_isExpire == 'notexpired') {
                $active_announcement = $announcement;
                if (!$announcement->users->contains(Auth::user()->id)) {
                    $announcement->users()->attach(Auth::user()->id);
                }
            }
        }
       // dd($posts);
       // dd($announcement);
        // dd(Auth::user()->username);
        $next_page_url = url('ajax/get-more-feed?page=2&ajax=true&hashtag='.$request->hashtag.'&username='.Auth::user()->username);
        // dd($user_post);
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('home', compact('timeline', 'posts', 'next_page_url', 'trending_tags', 'suggested_users', 'active_announcement', 'suggested_groups', 'suggested_pages','suggested_public_albums','suggested_locations', 'mode', 'user_post'))
        ->render();
    }



    public function locationGroup(Request $request,$group)
    {
        $mode = "showfeed";
        $user_post = 'group';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $timeline = Timeline::where('username', Auth::user()->username)->first();
        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums = suggestedPublicAlbums();

        if($group=='other'){
            $cate=['30','14','23','29'];
            $locations = Location::where('active', 1)
                ->whereIn('timeline_id',function ($query){
                    $query->select('timeline_id')->from('posts')
                        ->where('active','1');
                })
                ->whereNotIn('category_id',$cate)
                ->orderby('last_posted_at','desc')
                ->groupby('timeline_id')
                // ->get();
                ->paginate(12,['*'],'locations');
        }else{
            $locations = Location::where('active', 1)
                ->whereIn('timeline_id',function ($query){
                    $query->select('timeline_id')->from('posts')
                        ->where('active','1');
                })
                ->where('category_id',$group)
                ->orderby('last_posted_at','desc')
                ->groupby('timeline_id')
                // ->get();
                ->paginate(12,['*'],'locations');
        }



        $firstLocation = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')->first();
        // dd($firstLocation);
        Session::put('timeline_id',$firstLocation->timeline_id);


//      ->paginate(Setting::get('items_page'));
        $locations2 = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->latest()
                    ->where('active','1');
            })

//            ->latest()
            ->groupby('timeline_id')
            ->skip(15)->take(0)
            ->paginate(15,['*'],'locations');
//            ->paginate(Setting::get('items_page'));
        
        ///dd($locations);
        if ($request->ajax) {
            $responseHtml = '';

            foreach ($locations as $location) {
                $timeline=$location->timeline()->first();
                $post=Post::where('timeline_id',$location->timeline_id)->latest()->first();
                $media=Media::where('id',$timeline->avatar_id)->first();
                $responseHtml .= $theme->partial('location', ['post' => $post, 'timeline' => $timeline,'media' => $media, 'next_page_url' => $locations->appends(['ajax' => true])->nextPageUrl()]);
            }
            return $responseHtml;
        }

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Auth::user()->language)->active()->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();


        //dd(Auth::user()->username);
        //dd($user_post);
        $next_page_url = url('ajax/get-location-more-feed?page=2&ajax=true&username='.Auth::user()->username);
        $theme->setTitle('Locations | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('location-feed', compact('locations','timeline','locations2','category_options','country_options', 'next_page_url',  'suggested_users', 'trending_tags', 'suggested_groups', 'suggested_pages','suggested_public_albums','suggested_locations', 'mode', 'user_post'))
        ->render();
    }

    public function locationFeed(Request $request)
    {
        ini_set('memory_limit', '-1');
        $mode = "showfeed";
        $user_post = 'group';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $timeline = Timeline::where('username', Auth::user()->username)->first();
        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums = suggestedPublicAlbums();

        $locations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')
           // ->get();
            ->paginate(12,['*'],'locations');


        $firstLocation = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')->first();
        // dd($firstLocation);
        Session::put('timeline_id',$firstLocation->timeline_id);


//      ->paginate(Setting::get('items_page'));
        $locations2 = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->latest()
                    ->where('active','1');
            })
//            ->latest()
            ->groupby('timeline_id')
            ->skip(15)->take(0)
            ->paginate(15,['*'],'locations');
//            ->paginate(Setting::get('items_page'));

       // dd($locations2);
        if ($request->ajax) {
            $responseHtml = '';

            foreach ($locations as $location) {
                $timeline=$location->timeline()->first();
                $post=Post::where('timeline_id',$location->timeline_id)->latest()->first();
                $media=Media::where('id',$timeline->avatar_id)->first();
                $responseHtml .= $theme->partial('location', ['post' => $post, 'timeline' => $timeline,'media' => $media, 'next_page_url' => $locations->appends(['ajax' => true])->nextPageUrl()]);
            }
            return $responseHtml;
        }

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Auth::user()->language)->active()->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();

        //dd(Auth::user()->username);
        //dd($user_post);
        $next_page_url = url('ajax/get-location-more-feed?page=2&ajax=true&username='.Auth::user()->username);
        $theme->setTitle('Locations | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('location-feed', compact('locations','timeline','locations2','category_options','country_options', 'next_page_url',  'suggested_users', 'trending_tags', 'suggested_groups', 'suggested_pages','suggested_public_albums','suggested_locations', 'mode', 'user_post'))
        ->render();
    }

    public function locationHighlight(Request $request)
    {
        ini_set('memory_limit', '-1');
        $mode = "showfeed";
        $user_post = 'group';
        Session::put('head-highlight','yes');
//        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('guest');
        $timeline = Timeline::where('username', Auth::user()->username)->first();
//        $trending_tags = trendingTags();
//        $suggested_users = suggestedUsers();
//        $suggested_groups = suggestedGroups();
//        $suggested_pages = suggestedPages();
//        $suggested_locations = suggestedLocations();
//        $suggested_public_albums = suggestedPublicAlbums();

        $locations = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')
           // ->get();
            ->paginate(12,['*'],'locations');


        $firstLocation = Location::where('active', 1)
            ->whereIn('timeline_id',function ($query){
                $query->select('timeline_id')->from('posts')
                    ->where('active','1');
            })
            ->orderby('last_posted_at','desc')
            ->groupby('timeline_id')->first();
        // dd($firstLocation);
        Session::put('timeline_id',$firstLocation->timeline_id);

       // dd($firstLocation);
        if ($request->ajax) {
            $responseHtml = '';

            foreach ($locations as $location) {
                $timeline=$location->timeline()->first();
                $post=Post::where('timeline_id',$location->timeline_id)->latest()->first();
                $media=Media::where('id',$timeline->avatar_id)->first();
//                $responseHtml .= $theme->partial('location', ['post' => $post, 'timeline' => $timeline,'media' => $media, 'next_page_url' => $locations->appends(['ajax' => true])->nextPageUrl()]);
            }
            return $responseHtml;
        }

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Auth::user()->language)->active()->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();


        //dd(Auth::user()->username);
        //dd($user_post);
        $next_page_url = url('ajax/get-location-more-feed?page=2&ajax=true&username='.Auth::user()->username);
//        $theme->setTitle('Locations | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
//        return $theme->scope('location-highlight', compact('firstLocation','timeline','category_options','country_options',  'suggested_users', 'trending_tags', 'suggested_groups', 'suggested_pages','suggested_public_albums','suggested_locations', 'mode', 'user_post'))
//        ->render();
        return view('module.packages.highlight.add-highlight')
            ->with('user_post',$user_post)
            ->with('firstLocation',$firstLocation)
            ->with('category_options',$category_options)
            ->with('category_sub1_options',$category_sub1_options)
            ->with('country_options',$country_options)
            ->with('mode',$mode);
    }





    public function showGlobalFeed(Request $request)
    {
        $mode = 'globalfeed';
        $user_post = 'globalfeed';
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');

        $timeline = Timeline::where('username', Auth::user()->username)->first();

        $id = Auth::id();

        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums = suggestedPublicAlbums();

        // Check for hashtag
        if ($request->hashtag) {
            $hashtag = '#'.$request->hashtag;

            $posts = Post::where('description', 'like', "%{$hashtag}%")->where('active', 1)->latest()->paginate(Setting::get('items_page'));
        } // else show the normal feed
        else {
            $posts = Post::orderBy('created_at', 'desc')->where('active', 1)->paginate(Setting::get('items_page'));
        }

        if ($request->ajax) {
            $responseHtml = '';
            foreach ($posts as $post) {
                $responseHtml .= $theme->partial('post', ['post' => $post, 'timeline' => $timeline, 'next_page_url' => $posts->appends(['ajax' => true, 'hashtag' => $request->hashtag])->nextPageUrl()]);
            }

            return $responseHtml;
        }

        $announcement = Announcement::find(Setting::get('announcement'));
        if ($announcement != null) {
            $chk_isExpire = $announcement->chkAnnouncementExpire($announcement->id);

            if ($chk_isExpire == 'notexpired') {
                $active_announcement = $announcement;
                if (!$announcement->users->contains(Auth::user()->id)) {
                    $announcement->users()->attach(Auth::user()->id);
                }
            }
        }

        $next_page_url = url('ajax/get-global-feed?page=2&ajax=true&hashtag='.$request->hashtag.'&username='.Auth::user()->username);

        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('home', compact('timeline', 'posts', 'next_page_url', 'trending_tags', 'suggested_users', 'active_announcement', 'suggested_groups', 'suggested_pages','suggested_locations','suggested_public_albums', 'mode', 'user_post'))
        ->render();
    }

    public function changeAvatar(Request $request)
    {
        if (Config::get('app.env') == 'demo' && Auth::user()->username == 'bootstrapguru') {
            return response()->json(['status' => '201', 'message' => trans('common.disabled_on_demo')]);
        }
        $timeline = Timeline::where('id', $request->timeline_id)->first();
        //dd($request->timeline_type);
        if (($request->timeline_type == 'user' && $request->timeline_id == Auth::user()->timeline_id) ||
            ($request->timeline_type == 'location' && $timeline->locations->is_admin(Auth::user()->id) == true) ||
            ($request->timeline_type == 'page' && $timeline->page->is_admin(Auth::user()->id) == true) ||
            ($request->timeline_type == 'public_album' && $timeline->public_album->is_admin(Auth::user()->id) == true) ||
            ($request->timeline_type == 'group' && $timeline->groups->is_admin(Auth::user()->id) == true)
        ) {
            if ($request->hasFile('change_avatar')) {

                $timeline_type = $request->timeline_type;
                $change_avatar = $request->file('change_avatar');
                $strippedName = str_replace(' ', '', $change_avatar->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/'.$timeline_type.'s/avatars/';
                // Lets resize the image to the square with dimensions of either width or height , which ever is smaller.
                list($width, $height) = getimagesize($change_avatar->getRealPath());

                $avatar = Image::make($change_avatar->getRealPath());

                if ($width > $height) {
                    $avatar->crop($height, $height);
                } else {
                    $avatar->crop($width, $width);
                }

                $avatar->save(storage_path().'/uploads/'.$timeline_type.'s/avatars/'.$photoName, 60);
                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 400) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 400;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 300;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);

                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);


                $media = Media::create([
                        'title'  => $photoName,
                        'type'   => 'image',
                        'posted_position'   => 'avatar',
                        'source' => $photoName,
                        'user_id' => Auth::user()->id,
                        'timeline_id' => $request->timeline_id,
                        'from_site' => url('/'),
                        'language_code' => Auth::user()->language,
                    ]);

                $timeline->avatar_id = $media->id;
               // dd($media);
                if ($timeline->save()) {
                    return response()->json(['status' => '200', 'avatar_url' => url($timeline_type.'/avatar/'.$photoName), 'message' => trans('messages.update_avatar_success')]);
                }
            } else {
                return response()->json(['status' => '201', 'message' => trans('messages.update_avatar_failed')]);
            }
        }
    }

    public function changeCover(Request $request)
    {
        if (Config::get('app.env') == 'demo' && Auth::user()->username == 'bootstrapguru') {
            return response()->json(['status' => '201', 'message' => trans('common.disabled_on_demo')]);
        }
        if ($request->hasFile('change_cover')) {
            $timeline_type = $request->timeline_type;

            $change_avatar = $request->file('change_cover');
            $strippedName = str_replace(' ', '', $change_avatar->getClientOriginalName());
            $photoName = date('Y-m-d-H-i-s').$strippedName;
            $avatar = Image::make($change_avatar->getRealPath());
            $avatar->save(storage_path().'/uploads/'.$timeline_type.'s/covers/'.$photoName, 60);

            $media = Media::create([
                'title'  => $photoName,
                'type'   => 'image',
                'posted_position'   => 'cover',
                'source' => $photoName,
                'user_id' => Auth::user()->id,
                'timeline_id' => $request->timeline_id,
                'from_site' => url('/'),
                'language_code' => Auth::user()->language,
              ]);

            $timeline = Timeline::where('id', $request->timeline_id)->first();
            $timeline->cover_id = $media->id;

            if ($timeline->save()) {
                return response()->json(['status' => '200', 'cover_url' => url($timeline_type.'/cover/'.$photoName), 'message' => trans('messages.update_cover_success')]);
            }
        } else {
            return response()->json(['status' => '201', 'message' => trans('messages.update_cover_failed')]);
        }
    }

    public function createNewLocation(Request $request)
    {
        $input = $request->all();
        dd($input);
        $input['user_id'] = Auth::user()->id;
        
        $post = Post::create($input);
        $post->notifications_user()->sync([Auth::user()->id], true);

        if ($request->file('location_images_upload_modified')) {
            foreach ($request->file('location_images_upload_modified') as $postImage) {
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/users/gallery/';
                $avatar = Image::make($postImage->getRealPath());
                $avatar->save(storage_path().'/uploads/users/gallery/'.$photoName, 60);

                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 550) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 550;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        if($ori_h>=600){
                            $new_h = 600;
                        }else{
                            $new_h = $ori_h;
                        }

                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);
                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);

                $media = Media::create([
                      'title'  => $photoName,
                      'type'   => 'image',
                      'posted_position'   => 'post',
                      'source' => $photoName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

                $post->images()->attach($media);
            }
        }

        if ($request->hasFile('post_video_upload')) {
            $uploadedFile = $request->file('post_video_upload');
            $s3 = Storage::disk('uploads');

            $timestamp = date('Y-m-d-H-i-s');

            $strippedName = $timestamp.str_replace(' ', '', $uploadedFile->getClientOriginalName());

            $s3->put('users/gallery/'.$strippedName, file_get_contents($uploadedFile));

            $basename = $timestamp.basename($request->file('post_video_upload')->getClientOriginalName(), '.'.$request->file('post_video_upload')->getClientOriginalExtension());

            Flavy::thumbnail(storage_path().'/uploads/users/gallery/'.$strippedName, storage_path().'/uploads/users/gallery/'.$basename.'.jpg', 1); //returns array with file info

            $media = Media::create([
                      'title'  => $basename,
                      'type'   => 'video',
                      'posted_position'   => 'post',
                      'source' => $strippedName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

            $post->images()->attach($media);
        }
        if ($post) {
            // Check for any mentions and notify them
            preg_match_all('/(^|\s)(@\w+)/', $request->description, $usernames);
            foreach ($usernames[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $notification = Notification::create(['user_id' => $timeline->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.mentioned_you_in_post'), 'type' => 'mention', 'link' => 'post/'.$post->id]);
            }
            $timeline = Timeline::where('id', $request->timeline_id)->first();

            //Notify the user when someone posts on his timeline/page/group

            if ($timeline->type == 'page') {
                $notify_users = $timeline->page->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this page';
            } elseif ($timeline->type == 'group') {
                $notify_users = $timeline->groups->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this group';
            } else {
                $notify_users = $timeline->user()->whereNotIn('id', [Auth::user()->id])->get();
                $notify_message = 'posted on your timeline';
            }

            foreach ($notify_users as $notify_user) {
                Notification::create(['user_id' => $notify_user->id, 'timeline_id' => $request->timeline_id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $timeline->type, 'link' => $timeline->username]);
            }


            // Check for any hashtags and save them
            preg_match_all('/(^|\s)(#\w+)/', $request->description, $hashtags);
            foreach ($hashtags[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $hashtag = Hashtag::where('tag', str_replace('#', '', $value))->first();
                if ($hashtag) {
                    $hashtag->count = $hashtag->count + 1;
                    $hashtag->save();
                } else {
                    Hashtag::create(['tag' => str_replace('#', '', $value), 'count' => 1]);
                }
            }

            // Let us tag the post friends :)
            if ($request->user_tags != null) {
                $post->users_tagged()->sync(explode(',', $request->user_tags));
            }
        }

        // $post->users_tagged = $post->users_tagged();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->scope('timeline/post', compact('post', 'timeline'))->render();
        
        return response()->json(['status' => '200', 'data' => $postHtml]);
    }


    public function createPostNewLocation(Request $request)
    {
        $input = $request->all();

        if($input['name']){
//            $check=Timeline::where('name',$input['name'])->first();
//            if(!$check){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                //Create timeline record for userpage
                $username=str_replace(' ','-',$input['name']).'-'.$id.'-'.$user->language;
                //dd($username);
                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $input['name'],
                    'about'                 => $input['description'],
                    'country_id'            => $input['country_id'],
                    'state_id'              => "",
                    'city_id'               => "",
                    'language_code'         => $user->language,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $input['category_id'],
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                //below code inserting record in to page_user table
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $input['timeline_id'] = $timeline->id;
//            }else{
//                $input['timeline_id'] = $check->id;
//            }
        }else{
            $check=Timeline::where('name',$input['q'])->first();
            $input['timeline_id'] = $check->id;
        }
      

        $input['user_id'] = Auth::user()->id;

        $post = Post::create($input);
         
        $data=array('last_posted_at'=>date('Y-m-d H:i:s'));
        DB::table('locations')->where('timeline_id',$input['timeline_id'])->update($data);
//        $UpdateLocation=Location::where('timeline_id',$input['timeline_id']);
//        $UpdateLocation->last_posted_at=date('Y-m-d H:i:s');
//        $UpdateLocation->save();

       // Session::put('timeline_id','');
        $post->notifications_user()->sync([Auth::user()->id], true);

        if ($request->file('post_images_upload_modified')) {
            foreach ($request->file('post_images_upload_modified') as $postImage) {
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/users/gallery/';
                $avatar = Image::make($postImage->getRealPath());
                $avatar->save(storage_path().'/uploads/users/gallery/'.$photoName, 60);

                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 550) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 550;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        if($ori_h>=600){
                            $new_h = 600;
                        }else{
                            $new_h = $ori_h;
                        }

                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);
                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);

                $media = Media::create([
                      'title'  => $photoName,
                      'type'   => 'image',
                      'posted_position'   => 'post',
                      'source' => $photoName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

                $post->images()->attach($media);
            }
        }

        if ($request->hasFile('post_video_upload')) {
            $uploadedFile = $request->file('post_video_upload');
            $s3 = Storage::disk('uploads');

            $timestamp = date('Y-m-d-H-i-s');

            $strippedName = $timestamp.str_replace(' ', '', $uploadedFile->getClientOriginalName());

            $s3->put('users/gallery/'.$strippedName, file_get_contents($uploadedFile));

            $basename = $timestamp.basename($request->file('post_video_upload')->getClientOriginalName(), '.'.$request->file('post_video_upload')->getClientOriginalExtension());

            Flavy::thumbnail(storage_path().'/uploads/users/gallery/'.$strippedName, storage_path().'/uploads/users/gallery/'.$basename.'.jpg', 1); //returns array with file info

            $media = Media::create([
                      'title'  => $basename,
                      'type'   => 'video',
                      'posted_position'   => 'post',
                      'source' => $strippedName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

            $post->images()->attach($media);
        }
        if ($post) {
            // Check for any mentions and notify them
            preg_match_all('/(^|\s)(@\w+)/', $request->description, $usernames);
            foreach ($usernames[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $notification = Notification::create(['user_id' => $timeline->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.mentioned_you_in_post'), 'type' => 'mention', 'link' => 'post/'.$post->id]);
            }
            $timeline = Timeline::where('id', $request->timeline_id)->first();

            //Notify the user when someone posts on his timeline/page/group

            if ($timeline->type == 'page') {
                $notify_users = $timeline->page->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this page';
            } elseif ($timeline->type == 'group') {
                $notify_users = $timeline->groups->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this group';
            } else {
                $notify_users = $timeline->user()->whereNotIn('id', [Auth::user()->id])->get();
                $notify_message = 'posted on your timeline';
            }

            foreach ($notify_users as $notify_user) {
                Notification::create(['user_id' => $notify_user->id, 'timeline_id' => $request->timeline_id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $timeline->type, 'link' => $timeline->username]);
            }

            // Check for any hashtags and save them
            preg_match_all('/(^|\s)(#\w+)/', $request->description, $hashtags);
            foreach ($hashtags[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $hashtag = Hashtag::where('tag', str_replace('#', '', $value))->first();
                if ($hashtag) {
                    $hashtag->count = $hashtag->count + 1;
                    $hashtag->save();
                } else {
                    Hashtag::create(['tag' => str_replace('#', '', $value), 'count' => 1]);
                }
            }
            // Let us tag the post friends :)
            if ($request->user_tags != null) {
                $post->users_tagged()->sync(explode(',', $request->user_tags));
            }
        }

        $post->users_tagged = $post->users_tagged();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->scope('timeline/post', compact('post', 'timeline'))->render();

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }

    public function createPostNewLocationHighlight(Request $request)
    {
        $input = $request->all();

        if($input['name']){

                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                //Create timeline record for userpage
                $username=str_replace(' ','-',$input['name']).'-'.$id.'-'.$user->language;
                //dd($username);
                $state_id='';$city_id='';$city_sub1_id='';
                if(isset($input['state_id'])){
                    $state_id=$input['state_id'];
                }
                if(isset($input['city_id'])){
                    $city_id=$input['city_id'];
                }
                if(isset($input['city_sub1_id'])){
                    $city_sub1_id=$input['city_sub1_id'];
                }

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $input['name'],
                    'about'                 => $input['description'],
                    'country_id'            => $input['country_id'],
                    'state_id'              => $state_id,
                    'city_id'               => $city_id,
                    'city_sub1_id'          => $city_sub1_id,
                    'language_code'         => $user->language,
                    'type'                  => 'location',
                    'created_user_id'       => Auth::user()->id,
                ]);

                $category_sub1_id='';$category_sub2_id='';
                if(isset($input['category_sub1_id'])){
                    $category_sub1_id=$input['category_sub1_id'];
                }
                if(isset($input['category_sub2_id'])){
                    $category_sub2_id=$input['category_sub2_id'];
                }

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $input['category_id'],
                    'category_sub1_id'      => $category_sub1_id,
                    'category_sub2_id'      => $category_sub2_id,
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $data=array(
                    'location_id'=>$location->id,
                    'user_id'=>'67',
                    'role_id'=>'1',
                    'owner'=>'0',
                    'active'=>'1',
                );

                DB::table('location_user')->insert($data);

                $check=DB::table('highlight_in_schedule')
                    ->where('packageID',Session::get('package'))
                    ->where('programID',Session::get('program_id'))
                    ->orderby('OrderBy','desc')
                    ->first();
                $orderby=0;
                if($check){
                    $orderby=$check->OrderBy+1;
                }
                $data=array(
                    'packageID'=>Session::get('package'),
                    'LocationID'=>$location->id,
                    'programID'=>Session::get('program_id'),
                    'makeSlideshow'=>'Y',
                    'OrderBy'=>  $orderby,
                );

                DB::table('highlight_in_schedule')->insert($data);

                $role = Role::where('name', '=', 'Admin')->first();
                //below code inserting record in to page_user table
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $input['timeline_id'] = $timeline->id;
        }else{

            $check=Timeline::where('name',$input['q'])->first();
            $input['timeline_id'] = $check->id;
            $location=Location::where('timeline_id',$check->id)->first();
            if($location){
                $check=DB::table('highlight_in_schedule')
                    ->where('packageID',Session::get('package'))
                    ->where('programID',Session::get('program_id'))
                    ->orderby('OrderBy','desc')
                    ->first();
                $orderby=0;
                if($check){
                    $orderby=$check->OrderBy+1;
                }
                $data=array(
                    'packageID'=>Session::get('package'),
                    'LocationID'=>$location->id,
                    'programID'=>Session::get('program_id'),
                    'makeSlideshow'=>'Y',
                    'OrderBy'=>  $orderby,
                );

                DB::table('highlight_in_schedule')->insert($data);
            }

        }

        $input['user_id'] = Auth::user()->id;
        $input['represent_timeline_id'] = Session::get('represent_timeline_id');

        $post = Post::create($input);

        $data=array('last_posted_at'=>date('Y-m-d H:i:s'));
        DB::table('locations')->where('timeline_id',$input['timeline_id'])->update($data);
//        $UpdateLocation=Location::where('timeline_id',$input['timeline_id']);
//        $UpdateLocation->last_posted_at=date('Y-m-d H:i:s');
//        $UpdateLocation->save();

       // Session::put('timeline_id','');
        $post->notifications_user()->sync([Auth::user()->id], true);

        if ($request->file('post_images_upload_modified')) {
            foreach ($request->file('post_images_upload_modified') as $postImage) {
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/users/gallery/';
                $avatar = Image::make($postImage->getRealPath());
                $avatar->save(storage_path().'/uploads/users/gallery/'.$photoName, 60);

                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 550) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 550;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        if($ori_h>=600){
                            $new_h = 600;
                        }else{
                            $new_h = $ori_h;
                        }

                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);
                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);

                $media = Media::create([
                      'title'  => $photoName,
                      'type'   => 'image',
                      'posted_position'   => 'post',
                      'source' => $photoName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

                $post->images()->attach($media);
            }
        }

        if ($request->hasFile('post_video_upload')) {
            $uploadedFile = $request->file('post_video_upload');
            $s3 = Storage::disk('uploads');

            $timestamp = date('Y-m-d-H-i-s');

            $strippedName = $timestamp.str_replace(' ', '', $uploadedFile->getClientOriginalName());

            $s3->put('users/gallery/'.$strippedName, file_get_contents($uploadedFile));

            $basename = $timestamp.basename($request->file('post_video_upload')->getClientOriginalName(), '.'.$request->file('post_video_upload')->getClientOriginalExtension());

            Flavy::thumbnail(storage_path().'/uploads/users/gallery/'.$strippedName, storage_path().'/uploads/users/gallery/'.$basename.'.jpg', 1); //returns array with file info

            $media = Media::create([
                      'title'  => $basename,
                      'type'   => 'video',
                      'posted_position'   => 'post',
                      'source' => $strippedName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

            $post->images()->attach($media);
        }

        if ($post) {
            // Check for any mentions and notify them
            preg_match_all('/(^|\s)(@\w+)/', $request->description, $usernames);
            foreach ($usernames[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $notification = Notification::create(['user_id' => $timeline->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.mentioned_you_in_post'), 'type' => 'mention', 'link' => 'post/'.$post->id]);
            }
            $timeline = Timeline::where('id', $request->timeline_id)->first();

            //Notify the user when someone posts on his timeline/page/group

            if ($timeline->type == 'page') {
                $notify_users = $timeline->page->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this page';
            } elseif ($timeline->type == 'group') {
                $notify_users = $timeline->groups->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this group';
            } else {
                $notify_users = $timeline->user()->whereNotIn('id', [Auth::user()->id])->get();
                $notify_message = 'posted on your timeline';
            }

            foreach ($notify_users as $notify_user) {
                Notification::create(['user_id' => $notify_user->id, 'timeline_id' => $request->timeline_id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $timeline->type, 'link' => $timeline->username]);
            }

            // Check for any hashtags and save them
            preg_match_all('/(^|\s)(#\w+)/', $request->description, $hashtags);
            foreach ($hashtags[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $hashtag = Hashtag::where('tag', str_replace('#', '', $value))->first();
                if ($hashtag) {
                    $hashtag->count = $hashtag->count + 1;
                    $hashtag->save();
                } else {
                    Hashtag::create(['tag' => str_replace('#', '', $value), 'count' => 1]);
                }
            }
            // Let us tag the post friends :)
            if ($request->user_tags != null) {
                $post->users_tagged()->sync(explode(',', $request->user_tags));
            }
        }

        $post->users_tagged = $post->users_tagged();
//        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
//        $postHtml = $theme->scope('timeline/post', compact('post', 'timeline'))->render();
        $postHtml='';
        return response()->json(['status' => '200', 'data' => $postHtml]);
    }



    public function createReview(Request $request){
        $input = $request->all();
     //   dd($input);
//        $input['rating_id'] = Auth::user()->id;
        $input['user_id'] = Auth::user()->id;
        $post = Post::create($input);
        $post->notifications_user()->sync([Auth::user()->id], true);
        $rate_group='1';
        if ($request->file('post_images_upload_modified')) {
            foreach ($request->file('post_images_upload_modified') as $postImage) {
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/users/gallery/';
                $avatar = Image::make($postImage->getRealPath());
                $avatar->save(storage_path().'/uploads/users/gallery/'.$photoName, 60);

                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 550) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 550;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        if($ori_h>=600){
                            $new_h = 600;
                        }else{
                            $new_h = $ori_h;
                        }

                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);
                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);

                $media = Media::create([
                      'title'  => $photoName,
                      'type'   => 'image',
                      'rate_group'   => $rate_group,
                      'posted_position'   => 'post',
                      'source' => $photoName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);
                $post->images()->attach($media);
            }
        }

        if ($request->hasFile('post_video_upload')) {
            $uploadedFile = $request->file('post_video_upload');
            $s3 = Storage::disk('uploads');

            $timestamp = date('Y-m-d-H-i-s');

            $strippedName = $timestamp.str_replace(' ', '', $uploadedFile->getClientOriginalName());

            $s3->put('users/gallery/'.$strippedName, file_get_contents($uploadedFile));

            $basename = $timestamp.basename($request->file('post_video_upload')->getClientOriginalName(), '.'.$request->file('post_video_upload')->getClientOriginalExtension());

            Flavy::thumbnail(storage_path().'/uploads/users/gallery/'.$strippedName, storage_path().'/uploads/users/gallery/'.$basename.'.jpg', 1); //returns array with file info

            $media = Media::create([
                      'title'  => $basename,
                      'type'   => 'video',
                      'posted_position'   => 'post',
                      'source' => $strippedName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

            $post->images()->attach($media);
        }

        if ($post) {
            // Check for any mentions and notify them
            preg_match_all('/(^|\s)(@\w+)/', $request->description, $usernames);
            foreach ($usernames[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $notification = Notification::create(['user_id' => $timeline->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.mentioned_you_in_post'), 'type' => 'mention', 'link' => 'post/'.$post->id]);
            }
            $timeline = Timeline::where('id', $request->timeline_id)->first();

            //Notify the user when someone posts on his timeline/page/group

            if ($timeline->type == 'page') {
                $notify_users = $timeline->page->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this page';
            } elseif ($timeline->type == 'group') {
                $notify_users = $timeline->groups->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this group';
            } else {
                $notify_users = $timeline->user()->whereNotIn('id', [Auth::user()->id])->get();
                $notify_message = 'posted on your timeline';
            }

            foreach ($notify_users as $notify_user) {
                Notification::create(['user_id' => $notify_user->id, 'timeline_id' => $request->timeline_id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $timeline->type, 'link' => $timeline->username]);
            }


            // Check for any hashtags and save them
            preg_match_all('/(^|\s)(#\w+)/', $request->description, $hashtags);
            foreach ($hashtags[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $hashtag = Hashtag::where('tag', str_replace('#', '', $value))->first();
                if ($hashtag) {
                    $hashtag->count = $hashtag->count + 1;
                    $hashtag->save();
                } else {
                    Hashtag::create(['tag' => str_replace('#', '', $value), 'count' => 1]);
                }
            }

            // Let us tag the post friends :)
            if ($request->user_tags != null) {
                $post->users_tagged()->sync(explode(',', $request->user_tags));
            }
        }
        // $post->users_tagged = $post->users_tagged();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->scope('timeline/review', compact('post', 'timeline'))->render();

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }

    public function createPost(Request $request)
    {
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $post = Post::create($input);
        $post->notifications_user()->sync([Auth::user()->id], true);

        if ($request->file('post_images_upload_modified')) {

            foreach ($request->file('post_images_upload_modified') as $postImage) {
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $dir=storage_path().'/uploads/users/gallery/';
                $avatar = Image::make($postImage->getRealPath());
                $avatar->save(storage_path().'/uploads/users/gallery/'.$photoName, 60);

                list($ori_w, $ori_h) = getimagesize($dir . $photoName);
                // mid;
                if ($ori_w >= 550) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 550;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        if($ori_h>=600){
                            $new_h = 600;
                        }else{
                            $new_h = $ori_h;
                        }

                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                $check=Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/mid/' . $photoName);

                // small;
                if ($ori_w >= 150) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 150;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 100;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/small/' . $photoName);
                // x-small;
                if ($ori_w >= 50) {
                    if ($ori_w >= $ori_h) {
                        $new_w = 50;
                        $new_h = round(($new_w / $ori_w) * $ori_h);
                    } else {
                        $new_h = 50;
                        $new_w = round(($new_h / $ori_h) * $ori_w);
                    }
                } else {
                    $new_w = $ori_w;
                    $new_h = $ori_h;
                }
                Image::make($dir . $photoName)->resize($new_w, $new_h)->save($dir . '/x-small/' . $photoName);


                $media = Media::create([
                      'title'  => $photoName,
                      'type'   => 'image',
                      'posted_position'   => 'post',
                      'source' => $photoName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

                $post->images()->attach($media);
            }
        }

        if ($request->hasFile('post_video_upload')) {
            $uploadedFile = $request->file('post_video_upload');
            $s3 = Storage::disk('uploads');

            $timestamp = date('Y-m-d-H-i-s');

            $strippedName = $timestamp.str_replace(' ', '', $uploadedFile->getClientOriginalName());

            $s3->put('users/gallery/'.$strippedName, file_get_contents($uploadedFile));

            $basename = $timestamp.basename($request->file('post_video_upload')->getClientOriginalName(), '.'.$request->file('post_video_upload')->getClientOriginalExtension());

            Flavy::thumbnail(storage_path().'/uploads/users/gallery/'.$strippedName, storage_path().'/uploads/users/gallery/'.$basename.'.jpg', 1); //returns array with file info

            $media = Media::create([
                      'title'  => $basename,
                      'type'   => 'video',
                      'posted_position'   => 'post',
                      'source' => $strippedName,
                      'user_id' => Auth::user()->id,
                      'timeline_id' => $request->timeline_id,
                      'from_site' => url('/'),
                      'language_code' => Auth::user()->language,
                    ]);

            $post->images()->attach($media);
        }
        if ($post) {
            // Check for any mentions and notify them
            preg_match_all('/(^|\s)(@\w+)/', $request->description, $usernames);
            foreach ($usernames[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $notification = Notification::create(['user_id' => $timeline->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.mentioned_you_in_post'), 'type' => 'mention', 'link' => 'post/'.$post->id]);
            }
            $timeline = Timeline::where('id', $request->timeline_id)->first();

            //Notify the user when someone posts on his timeline/page/group

            if ($timeline->type == 'page') {
                $notify_users = $timeline->page->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this page';
            } elseif ($timeline->type == 'group') {
                $notify_users = $timeline->groups->users()->whereNotIn('user_id', [Auth::user()->id])->get();
                $notify_message = 'posted on this group';
            } else {
                $notify_users = $timeline->user()->whereNotIn('id', [Auth::user()->id])->get();
                $notify_message = 'posted on your timeline';
            }

            foreach ($notify_users as $notify_user) {
                Notification::create(['user_id' => $notify_user->id, 'timeline_id' => $request->timeline_id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $timeline->type, 'link' => $timeline->username]);
            }


            // Check for any hashtags and save them
            preg_match_all('/(^|\s)(#\w+)/', $request->description, $hashtags);
            foreach ($hashtags[2] as $value) {
                $timeline = Timeline::where('username', str_replace('@', '', $value))->first();
                $hashtag = Hashtag::where('tag', str_replace('#', '', $value))->first();
                if ($hashtag) {
                    $hashtag->count = $hashtag->count + 1;
                    $hashtag->save();
                } else {
                    Hashtag::create(['tag' => str_replace('#', '', $value), 'count' => 1]);
                }
            }

            // Let us tag the post friends :)
            if ($request->user_tags != null) {
                $post->users_tagged()->sync(explode(',', $request->user_tags));
            }
        }

        // $post->users_tagged = $post->users_tagged();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->scope('timeline/post', compact('post', 'timeline'))->render();

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }


    public function editPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->with('user')->first();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->partial('edit-post', compact('post'));

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }

    public function loadEmoji()
    {
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
        $postHtml = $theme->partial('emoji');

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }

    public function updatePost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post->user->id == Auth::user()->id) {
            $post->description = $request->description;
            $post->save();
        }

        return redirect('post/'.$post->id);
    }

    public function getSoundCloudResults(Request $request)
    {
        $soundcloudJson = file_get_contents('http://api.soundcloud.com/tracks.json?client_id='.env('SOUNDCLOUD_CLIENT_ID').'&q='.$request->q);

        return response()->json(['status' => '200', 'data' => $soundcloudJson]);
    }

    public function postComment(Request $request)
    {
        $comment = Comment::create([
                    'post_id'     => $request->post_id,
                    'description' => $request->description,
                    'user_id'     => Auth::user()->id,
                    'parent_id'   => $request->comment_id,
                  ]);

        $post = Post::where('id', $request->post_id)->first();
        $posted_user = $post->user;

        if ($comment) {
            if (Auth::user()->id != $post->user_id) {
                //Notify the user for comment on his/her post
                Notification::create(['user_id' => $post->user_id, 'post_id' => $request->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.commented_on_your_post'), 'type' => 'comment_post']);
            }

            $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
            if ($request->comment_id) {
                $reply = $comment;
                $main_comment = Comment::find($reply->parent_id);
                $main_comment_user = $main_comment->user;

                $user = User::find(Auth::user()->id);
                $user_settings = $user->getUserSettings($main_comment_user->id);
                if ($user_settings && $user_settings->email_reply_comment == 'yes') {
                    Mail::send('emails.commentreply_mail', ['user' => $user, 'main_comment_user' => $main_comment_user], function ($m) use ($user, $main_comment_user) {
                        $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                        $m->to($main_comment_user->email, $main_comment_user->name)->subject('New reply to your comment');
                    });
                }
                $postHtml = $theme->scope('timeline/reply', compact('reply', 'post'))->render();
            } else {
                $user = User::find(Auth::user()->id);
                $user_settings = $user->getUserSettings($posted_user->id);
                if ($user_settings && $user_settings->email_comment_post == 'yes') {
                    Mail::send('emails.commentmail', ['user' => $user, 'posted_user' => $posted_user], function ($m) use ($user, $posted_user) {
                        $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                        $m->to($posted_user->email, $posted_user->name)->subject('New comment to your post');
                    });
                }

                $postHtml = $theme->scope('timeline/comment', compact('comment', 'post'))->render();
            }
        }

        return response()->json(['status' => '200', 'comment_id' => $comment->id, 'data' => $postHtml]);
    }

    public function likePost(Request $request)
    {
        $post = Post::findOrFail($request->post_id);
        $posted_user = $post->user;
        $like_count = $post->users_liked()->count();

        //Like the post
        if (!$post->users_liked->contains(Auth::user()->id)) {
            $post->users_liked()->attach(Auth::user()->id, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            $post->notifications_user()->attach(Auth::user()->id);

            $user = User::find(Auth::user()->id);
            $user_settings = $user->getUserSettings($posted_user->id);
            if ($user_settings && $user_settings->email_like_post == 'yes') {
                Mail::send('emails.postlikemail', ['user' => $user, 'posted_user' => $posted_user], function ($m) use ($posted_user, $user) {
                    $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                    $m->to($posted_user->email, $posted_user->name)->subject($user->name.' '.'liked your post');
                });
            }

            //Notify the user for post like
            $notify_message = 'liked your post';
            $notify_type = 'like_post';
            $status_message = 'successfully liked';

            if ($post->user->id != Auth::user()->id) {
                Notification::create(['user_id' => $post->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $notify_type]);
            }

            return response()->json(['status' => '200', 'liked' => true, 'message' => $status_message, 'likecount' => $like_count]);
        } //Unlike the post
        else {
            $post->users_liked()->detach([Auth::user()->id]);
            $post->notifications_user()->detach([Auth::user()->id]);

            //Notify the user for post unlike
            $notify_message = 'unliked your post';
            $notify_type = 'unlike_post';
            $status_message = 'successfully unliked';

            if ($post->user->id != Auth::user()->id) {
                Notification::create(['user_id' => $post->user->id, 'post_id' => $post->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.$notify_message, 'type' => $notify_type]);
            }

            return response()->json(['status' => '200', 'liked' => false, 'message' => $status_message, 'likecount' => $like_count]);
        }

        if ($post) {
            $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('ajax');
            $postHtml = $theme->scope('timeline/post', compact('post'))->render();
        }

        return response()->json(['status' => '200', 'data' => $postHtml]);
    }

    public function likeComment(Request $request)
    {
        $comment = Comment::findOrFail($request->comment_id);
        $comment_user = $comment->user;

        if (!$comment->comments_liked->contains(Auth::user()->id)) {
            $comment->comments_liked()->attach(Auth::user()->id);
            $comment_likes = $comment->comments_liked()->get();
            $like_count = $comment_likes->count();

            //sending email notification
            $user = User::find(Auth::user()->id);
            $user_settings = $user->getUserSettings($comment_user->id);
            if ($user_settings && $user_settings->email_like_comment == 'yes') {
                Mail::send('emails.commentlikemail', ['user' => $user, 'comment_user' => $comment_user], function ($m) use ($user, $comment_user) {
                    $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                    $m->to($comment_user->email, $comment_user->name)->subject($user->name.' '.'likes your comment');
                });
            }

            //Notify the user for comment like
            if ($comment->user->id != Auth::user()->id) {
                Notification::create(['user_id' => $comment->user_id, 'post_id' => $comment->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.liked_your_comment'), 'type' => 'like_comment']);
            }

            return response()->json(['status' => '200', 'liked' => true, 'message' => 'successfully liked', 'likecount' => $like_count]);
        } else {
            $comment->comments_liked()->detach([Auth::user()->id]);
            $comment_likes = $comment->comments_liked()->get();
            $like_count = $comment_likes->count();

            //Notify the user for comment unlike
            if ($comment->user->id != Auth::user()->id) {
                Notification::create(['user_id' => $comment->user_id, 'post_id' => $comment->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unliked_your_comment'), 'type' => 'unlike_comment']);
            }

            return response()->json(['status' => '200', 'unliked' => false, 'message' => 'successfully unliked', 'likecount' => $like_count]);
        }
    }

    public function sharePost(Request $request)
    {
        $post = Post::findOrFail($request->post_id);
        $posted_user = $post->user;


        if (!$post->users_shared->contains(Auth::user()->id)) {
            $post->users_shared()->attach(Auth::user()->id, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            $post_share_count = $post->users_shared()->get()->count();
            // we need to insert the shared post into the timeline of the person who shared
            $input['user_id'] = Auth::user()->id;
            $post = Post::create([
                'timeline_id' => Auth::user()->timeline->id,
                'user_id' => Auth::user()->id,
                'shared_post_id' => $request->post_id,
            ]);


            if ($post->user_id != Auth::user()->id) {
                //Notify the user for post share
                Notification::create(['user_id' => $post->user_id, 'post_id' => $request->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.shared_your_post'), 'type' => 'share_post', 'link' => '/'.Auth::user()->username]);

                $user = User::find(Auth::user()->id);
                $user_settings = $user->getUserSettings($posted_user->id);

                if ($user_settings && $user_settings->email_post_share == 'yes') {
                    Mail::send('emails.postsharemail', ['user' => $user, 'posted_user' => $posted_user], function ($m) use ($user, $posted_user) {
                        $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                        $m->to($posted_user->email, $posted_user->name)->subject($user->name.' '.'shared your post');
                    });
                }
            }

            return response()->json(['status' => '200', 'shared' => true, 'message' => 'successfully shared', 'share_count' => $post_share_count]);
        } else {
            $post->users_shared()->detach([Auth::user()->id]);
            $post_share_count = $post->users_shared()->get()->count();

            $sharedPost = Post::where('shared_post_id', $post->id)->delete();

            if ($post->user_id != Auth::user()->id) {
                //Notify the user for post share
                Notification::create(['user_id' => $post->user_id, 'post_id' => $request->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unshared_your_post'), 'type' => 'unshare_post', 'link' => '/'.Auth::user()->username]);
            }

            return response()->json(['status' => '200', 'unshared' => false, 'message' => 'Successfully unshared', 'share_count' => $post_share_count]);
        }
    }

    public function locationLiked(Request $request)
    {
        $location = Location::where('timeline_id', '=', $request->timeline_id)->first();

        if ($location->likes->contains(Auth::user()->id)) {
            $location->likes()->detach([Auth::user()->id]);
            return response()->json(['status' => '200', 'like' => true, 'message' => 'successfully unliked']);
        }
    }

    public function pageLiked(Request $request)
    {
        $page = Page::where('timeline_id', '=', $request->timeline_id)->first();

        if ($page->likes->contains(Auth::user()->id)) {
            $page->likes()->detach([Auth::user()->id]);

            return response()->json(['status' => '200', 'like' => true, 'message' => 'successfully unliked']);
        }
    }



    public function pageReport(Request $request)
    {
        $timeline = Timeline::where('id', '=', $request->timeline_id)->first();

        if ($timeline->type == 'page') {
            $admins = $timeline->page->admins();
            $report_type = 'page_report';
        }
        if ($timeline->type == 'group') {
            $admins = $timeline->groups->admins();
            $report_type = 'group_report';
        }


        if (!$timeline->reports->contains(Auth::user()->id)) {
            $timeline->reports()->attach(Auth::user()->id, ['status' => 'pending']);

            if ($timeline->type == 'user') {
                Notification::create(['user_id' => $timeline->user->id, 'timeline_id' => $timeline->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.reported_you'), 'type' => 'user_report']);
            } else {
                foreach ($admins as $admin) {
                    Notification::create(['user_id' => $admin->id, 'timeline_id' => $timeline->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' reported your '.$timeline->type, 'type' => $report_type]);
                }
            }


            return response()->json(['status' => '200', 'reported' => true, 'message' => 'successfully reported']);
        } else {
            $timeline->reports()->detach([Auth::user()->id]);

            if ($timeline->type == 'user') {
                Notification::create(['user_id' => $timeline->user->id, 'timeline_id' => $timeline->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unreported_you'), 'type' => 'user_report']);
            } else {
                foreach ($admins as $admin) {
                    Notification::create(['user_id' => $admin->id, 'timeline_id' => $timeline->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unreported_your_page'), 'type' => 'page_report']);
                }
            }

            return response()->json(['status' => '200', 'reported' => false, 'message' => 'successfully unreport']);
        }
    }

    public function timelineGroups(Request $request)
    {
        $group = Group::where('timeline_id', '=', $request->timeline_id)->first();

        if ($group->users->contains(Auth::user()->id)) {
            $group->users()->detach([Auth::user()->id]);

            return response()->json(['status' => '200', 'join' => true, 'message' => 'successfully unjoined']);
        }
    }

    public function getYoutubeVideo(Request $request)
    {
        $videoId = Youtube::parseVidFromURL($request->youtube_source);

        $video = Youtube::getVideoInfo($videoId);

        $videoData = [
                        'id'     => $video->id,
                        'title'  => $video->snippet->title,
                        'iframe' => $video->player->embedHtml,
                      ];

        return response()->json(['status' => '200', 'message' => $videoData]);
    }

    public function show($id)
    {
        $timeline = $this->timelineRepository->findWithoutFail($id);

        if (empty($timeline)) {
            Flash::error('Timeline not found');

            return redirect(route('timelines.index'));
        }

        return view('timelines.show')->with('timeline', $timeline);
    }

    /**
     * Show the form for editing the specified Timeline.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $timeline = $this->timelineRepository->findWithoutFail($id);

        if (empty($timeline)) {
            Flash::error('Timeline not found');

            return redirect(route('timelines.index'));
        }

        return view('timelines.edit')->with('timeline', $timeline);
    }

    /**
     * Update the specified Timeline in storage.
     *
     * @param int                   $id
     * @param UpdateTimelineRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTimelineRequest $request)
    {
        $timeline = $this->timelineRepository->findWithoutFail($id);

        if (empty($timeline)) {
            Flash::error('Timeline not found');

            return redirect(route('timelines.index'));
        }

        $timeline = $this->timelineRepository->update($request->all(), $id);

        Flash::success('Timeline updated successfully.');

        return redirect(route('timelines.index'));
    }

    /**
     * Remove the specified Timeline from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $timeline = $this->timelineRepository->findWithoutFail($id);

        if (empty($timeline)) {
            Flash::error('Timeline not found');

            return redirect(route('timelines.index'));
        }

        $this->timelineRepository->delete($id);

        Flash::success('Timeline deleted successfully.');

        return redirect(route('timelines.index'));
    }

    public function follow(Request $request)
    {
        $follow = User::where('timeline_id', '=', $request->timeline_id)->first();
       // dd($follow);
        if (!$follow->followers->contains(Auth::user()->id)) {
            $follow->followers()->attach(Auth::user()->id, ['status' => 'approved']);

            $user = User::find(Auth::user()->id);
            $user_settings = $user->getUserSettings($follow->id);

            if ($user_settings && $user_settings->email_follow == 'yes') {
                Mail::send('emails.followmail', ['user' => $user, 'follow' => $follow], function ($m) use ($user, $follow) {
                    $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                    $m->to($follow->email, $follow->name)->subject($user->name.' '.trans('common.follows_you'));
                });
            }

            //Notify the user for follow
            Notification::create(['user_id' => $follow->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.is_following_you'), 'type' => 'follow']);

            return response()->json(['status' => '200', 'followed' => true, 'message' => 'successfully followed']);
        } else {
            $follow->followers()->detach([Auth::user()->id]);

            //Notify the user for follow
            Notification::create(['user_id' => $follow->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.is_unfollowing_you'), 'type' => 'unfollow']);

            return response()->json(['status' => '200', 'followed' => false, 'message' => 'successfully unFollowed']);
        }
    }

    public function joiningGroup(Request $request)
    {
        $user_role_id = Role::where('name', '=', 'user')->first();
        $group = Group::where('timeline_id', '=', $request->timeline_id)->first();
        $group_timeline = $group->timeline;

        $users = $group->users()->get();

        if (!$group->users->contains(Auth::user()->id)) {
            $group->users()->attach(Auth::user()->id, ['role_id' => $user_role_id->id, 'status' => 'approved']);


            foreach ($users as $user) {
                if ($user->id != Auth::user()->id) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.joined_your_group'), 'type' => 'join_group']);
                }

                if ($group->is_admin($user->id)) {
                    $group_admin = User::find($user->id);
                    $user = User::find(Auth::user()->id);
                    $user_settings = $user->getUserSettings($group_admin->id);
                    if ($user_settings && $user_settings->email_join_group == 'yes') {
                        Mail::send('emails.groupjoinmail', ['user' => $user, 'group_timeline' => $group_timeline], function ($m) use ($user, $group_admin, $group_timeline) {
                            $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                            $m->to($group_admin->email)->subject($user->name.' '.trans('common.joined_your_group'));
                        });
                    }
                }
            }

            return response()->json(['status' => '200', 'joined' => true, 'message' => 'successfully joined']);
        } else {
            $group->users()->detach([Auth::user()->id]);

            foreach ($users as $user) {
                if ($user->id != Auth::user()->id) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unjoined_your_group'), 'type' => 'unjoin_group']);
                }
            }

            return response()->json(['status' => '200', 'joined' => false, 'message' => 'successfully unjoined']);
        }
    }

    public function joiningEvent(Request $request)
    {
        $event = Event::where('timeline_id', '=', $request->timeline_id)->first();
        $users = $event->users()->get();

        if (!$event->users->contains(Auth::user()->id)) {
            $event->users()->attach(Auth::user()->id);

            foreach ($users as $user) {
                if ($user->id != Auth::user()->id) {
                    //Notify the user for event join
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.attending_your_event'), 'type' => 'join_event']);
                }
            }
            return response()->json(['status' => '200', 'joined' => true, 'message' => 'successfully joined']);
        } else {
            $event->users()->detach([Auth::user()->id]);
            foreach ($users as $user) {
                if ($user->id != Auth::user()->id) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.quit_attending_your_event'), 'type' => 'unjoin_event']);
                }
            }
            return response()->json(['status' => '200', 'joined' => false, 'message' => 'successfully unjoined']);
        }
    }

    public function joiningClosedGroup(Request $request)
    {
        $user_role_id = Role::where('name', '=', 'user')->first();
        $group = Group::where('timeline_id', '=', $request->timeline_id)->first();

        if (!$group->users->contains(Auth::user()->id)) {
            $group->users()->attach(Auth::user()->id, ['role_id' => $user_role_id->id, 'status' => 'pending']);


            $users = $group->users()->get();
            foreach ($users as $user) {
                if (Auth::user()->id != $user->id) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.request_join_group'), 'type' => 'group_join_request']);
                }
            }

            return response()->json(['status' => '200', 'joinrequest' => true, 'message' => 'successfully sent group join request']);
        } else {
            $checkStatus = $group->chkGroupUser($group->id, Auth::user()->id);

            if ($checkStatus && $checkStatus->status == 'approved') {
                $group->users()->detach([Auth::user()->id]);

                return response()->json(['status' => '200', 'join' => true, 'message' => 'unsuccessfully request']);
            } else {
                $group->users()->detach([Auth::user()->id]);

                return response()->json(['status' => '200', 'joinrequest' => false, 'message' => 'unsuccessfully request']);
            }
        }
    }

    public function userFollowRequest(Request $request)
    {
        $user = User::where('timeline_id', '=', $request->timeline_id)->first();

        if (!$user->followers->contains(Auth::user()->id)) {
            $user->followers()->attach(Auth::user()->id, ['status' => 'pending']);

            //Notify the user for page like
            Notification::create(['user_id' => $user->id, 'timeline_id' => Auth::user()->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.request_follow'), 'type' => 'follow_requested']);

            return response()->json(['status' => '200', 'followrequest' => true, 'message' => 'successfully sent user follow request']);
        } else {
            if ($request->follow_status == 'approved') {
                $user->followers()->detach([Auth::user()->id]);

                return response()->json(['status' => '200', 'unfollow' => true, 'message' => 'unfollowed successfully']);
            } else {
                $user->followers()->detach([Auth::user()->id]);

                return response()->json(['status' => '200', 'followrequest' => false, 'message' => 'unsuccessfully request']);
            }
        }
    }

    public function locationLike(Request $request)
    {
        $location = Location::where('timeline_id', '=', $request->timeline_id)->first();
        $location_timeline = $location->timeline;

        if (!$location->likes->contains(Auth::user()->id)) {
            $location->likes()->attach(Auth::user()->id);
            if (!$location->users->contains(Auth::user()->id)) {
                $users = $location->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.liked_your_location'), 'type' => 'like_location']);

                    if ($location->is_admin($user->id)) {
                        $location_admin = User::find($user->id);
                        $user = User::find(Auth::user()->id);
                        $user_settings = $user->getUserSettings($location_admin->id);
                        if ($user_settings && $user_settings->email_like_location == 'yes') {
                            Mail::send('emails.locationlikemail', ['user' => $user, 'location_timeline' => $location_timeline], function ($m) use ($user, $location_admin, $location_timeline) {
                                $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                                $m->to($location_admin->email)->subject($user->name.' '.'liked your location');
                            });
                        }
                    }
                }
            }
            return response()->json(['status' => '200', 'liked' => true, 'message' => 'Location successfully liked']);
        } else {
            $location->likes()->detach([Auth::user()->id]);
            if (!$location->users->contains(Auth::user()->id)) {
                $users = $location->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page unlike
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unliked_your_location'), 'type' => 'unlike_location']);
                }
            }

            return response()->json(['status' => '200', 'liked' => false, 'message' => 'Location successfully unliked']);
        }
    }

    public function publicAlbumLike(Request $request)
    {
        $public_album = PublicAlbum::where('timeline_id', '=', $request->timeline_id)->first();
        $public_album_timeline = $public_album->timeline;

        if (!$public_album->likes->contains(Auth::user()->id)) {
            $public_album->likes()->attach(Auth::user()->id);

            if (!$public_album->users->contains(Auth::user()->id)) {
                $users = $public_album->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.liked_your_page'), 'type' => 'like_public_album']);

                    if ($public_album->is_admin($user->id)) {
                        $page_admin = User::find($user->id);
                        $user = User::find(Auth::user()->id);
                        $user_settings = $user->getUserSettings($page_admin->id);
                        if ($user_settings && $user_settings->email_like_page == 'yes') {
                            Mail::send('emails.public-album-likemail', ['user' => $user, 'public_album_timeline' => $public_album_timeline], function ($m) use ($user, $page_admin, $page_timeline) {
                                $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                                $m->to($page_admin->email)->subject($user->name.' '.'liked your public album');
                            });
                        }
                    }
                }
            }

            return response()->json(['status' => '200', 'liked' => true, 'message' => 'Page successfully liked']);
        } else {
            $public_album->likes()->detach([Auth::user()->id]);

            if (!$public_album->users->contains(Auth::user()->id)) {
                $users = $public_album->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page unlike
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unliked_your_page'), 'type' => 'unlike_public_album']);
                }
            }

            return response()->json(['status' => '200', 'liked' => false, 'message' => 'Page successfully unliked']);
        }
    }

    public function pageLike(Request $request)
    {
        $page = Page::where('timeline_id', '=', $request->timeline_id)->first();
        $page_timeline = $page->timeline;

        if (!$page->likes->contains(Auth::user()->id)) {
            $page->likes()->attach(Auth::user()->id);

            if (!$page->users->contains(Auth::user()->id)) {
                $users = $page->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page like
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.liked_your_page'), 'type' => 'like_page']);

                    if ($page->is_admin($user->id)) {
                        $page_admin = User::find($user->id);
                        $user = User::find(Auth::user()->id);
                        $user_settings = $user->getUserSettings($page_admin->id);
                        if ($user_settings && $user_settings->email_like_page == 'yes') {
                            Mail::send('emails.pagelikemail', ['user' => $user, 'page_timeline' => $page_timeline], function ($m) use ($user, $page_admin, $page_timeline) {
                                $m->from(Setting::get('noreply_email'), Setting::get('site_name'));
                                $m->to($page_admin->email)->subject($user->name.' '.'liked your page');
                            });
                        }
                    }
                }
            }

            return response()->json(['status' => '200', 'liked' => true, 'message' => 'Page successfully liked']);
        } else {
            $page->likes()->detach([Auth::user()->id]);

            if (!$page->users->contains(Auth::user()->id)) {
                $users = $page->users()->get();
                foreach ($users as $user) {
                    //Notify the user for page unlike
                    Notification::create(['user_id' => $user->id, 'timeline_id' => $request->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.unliked_your_page'), 'type' => 'unlike_page']);
                }
            }

            return response()->json(['status' => '200', 'liked' => false, 'message' => 'Page successfully unliked']);
        }
    }

    public function getNotifications(Request $request)
    {
        $post = Post::findOrFail($request->post_id);

        if (!$post->notifications_user->contains(Auth::user()->id)) {
            $post->notifications_user()->attach(Auth::user()->id);

            return response()->json(['status' => '200', 'notified' => true, 'message' => 'Successfull']);
        } else {
            $post->notifications_user()->detach([Auth::user()->id]);

            return response()->json(['status' => '200', 'unnotify' => false, 'message' => 'UnSuccessfull']);
        }
    }

    public function addLocation($username)
    {
//        $category_options = ['' => 'Select Category'] + Category::active()->pluck('name', 'id')->all();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Auth::user()->language)->active()->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.create_location').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/create-location', compact('username', 'category_options','category_sub1_options','country_options'))->render();
    }
    
    public function addLocationHighligth($username)
    {
//      $category_options = ['' => 'Select Category'] + Category::active()->pluck('name', 'id')->all();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $category_sub1_options = ['' => 'Select Category'] + CategorySub1::where('language_code',Auth::user()->language)->active()->pluck('sub1_name', 'category_sub1_id')->all();
        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.create_location').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('timeline/create-location-highlight', compact('username', 'category_options','category_sub1_options','country_options'))->render();
    }
    
    
    
    public function addPublicAlbum($username)
    {
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->active()->orderby('name','asc')->pluck('name', 'category_id')->all();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.create_public').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/create-public-album', compact('username', 'category_options'))->render();
    }
    
    public function addPage($username)
    {
//        $category_options = ['' => 'Select Category'] + Category::active()->pluck('name', 'id')->all();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');

        $theme->setTitle(trans('common.create_page').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/create-page', compact('username', 'category_options'))->render();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name'     => 'required|max:30|min:5',
            'category' => 'required',
            'username' => 'required|max:26|min:5|alpha_num|unique:timelines|no_admin'
        ];

        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];

        return Validator::make($data, $rules, $messages);
    }

    protected function location_validator(array $data)
    {
        $rules = [
            'name'     => 'required|max:100|min:5',
            'category_id' => 'required',
            'category_sub1_id' => 'required',
            'country_id' => 'required',
            //'state_id' => 'required',
            'username' => 'required|max:150|no_admin'
//            'username' => 'required|max:26|min:5|alpha_num|alpha_dash|unique:timelines|no_admin'
        ];

        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];

        return Validator::make($data, $rules, $messages);
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }

    public function createLocationHighlight(Request $request)
    {
        $validator = $this->location_validator($request->all());

        if ($validator->fails()) {
         //   dd($validator);
           return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        $user = User::where('id', '=', Auth::user()->id)->first();
        //Create timeline record for userpage
        $timeline = Timeline::create([
            'username'              => $request->username,
            'name'                  => $request->name,
            'about'                 => $request->about,
            'country_id'            => $request->country_id,
            'state_id'              => $request->state_id,
            'city_id'               => $request->city_id,
            'language_code'         => $user->language,
            'type'                  => 'location',
            ]);

        $location = Location::create([
            'timeline_id'           => $timeline->id,
            'category_id'           => $request->category_id,
            'category_sub1_id'      => $request->category_sub1_id,
            'member_privacy'        => Setting::get('location_member_privacy'),
            'message_privacy'       => Setting::get('location_message_privacy'),
            'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
            ]);

        $data=array(
            'name'=>$request->name,
            'username'=>$request->username,
            'about'=>$timeline->name.' '.$timeline->about,
            'timeline_id'=>$timeline->id,
            'language_code'=>Auth::user()->language
        );
        DB::table('timeline_info')->insert($data);

        $data=array(
            'rate_group'=>'1',
            'star_rating'=>'2',
            'description'=>$timeline->name.' '.$timeline->about,
            'timeline_id'=>$timeline->id,
            'user_id'=>Auth::user()->id,
            'active'=>'1',
            'created_at'=>date('Y-m-d H:i:s'),
            'language_code'=>Auth::user()->language
        );
        DB::table('posts')->insert($data);

        $data=array(
            'packageID'=>Session::get('package'),
            'programID'=>Session::get('program_id'),
            'makeSlideshow'=>'',
            'LocationID'=>$location->id
        );
        DB::table('highlight_in_schedule')->insert($data);
        // dd($request);
        $role = Role::where('name', '=', 'Admin')->first();
        //below code inserting record in to page_user table
        $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
        $message = 'Location created successfully';
        $username = $request->username;

        return redirect('/package/program/highlight/'.Session::get('program_id'));
    }

    public function createLocation(Request $request)
    {
        $validator = $this->location_validator($request->all());

        if ($validator->fails()) {
         //   dd($validator);
           return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        $user = User::where('id', '=', Auth::user()->id)->first();
        //Create timeline record for userpage
        $timeline = Timeline::create([
            'username'              => $request->username,
            'name'                  => $request->name,
            'about'                 => $request->about,
            'country_id'            => $request->country_id,
            'state_id'              => $request->state_id,
            'city_id'               => $request->city_id,
            'language_code'         => $user->language,
            'type'                  => 'location',
            ]);

        $location = Location::create([
            'timeline_id'           => $timeline->id,
            'category_id'           => $request->category_id,
            'category_sub1_id'      => $request->category_sub1_id,
            'member_privacy'        => Setting::get('location_member_privacy'),
            'message_privacy'       => Setting::get('location_message_privacy'),
            'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
            ]);

        // dd($request);
        $role = Role::where('name', '=', 'Admin')->first();
        //below code inserting record in to page_user table
        $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
        $message = 'Location created successfully';
        $username = $request->username;

        return redirect('/'.$timeline->username);
    }

    
    
    public function createPublicAlbum(Request $request)
    {
        $validator = $this->validator($request->all());

//        $category=Category::where('category_id',$request->category)->where('language_code',Auth::user()->language)->first();
       // dd($category);
        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        //Create timeline record for userpage
        $timeline = Timeline::create([
            'username' => $request->username,
            'name'     => $request->name,
            'about'    => $request->about,
            'type'     => 'public_album',
            ]);

        $public_album = PublicAlbum::create([
            'timeline_id'           => $timeline->id,
            'category_id'           => $request->category,
            'member_privacy'        => Setting::get('public_album_member_privacy'),
            'message_privacy'       => Setting::get('public_album_message_privacy'),
            'timeline_post_privacy' => Setting::get('public_album_timeline_post_privacy'),
            ]);

        $role = Role::where('name', '=', 'Admin')->first();
        //below code inserting record in to public_album_user table
        $public_album->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
        $message = 'Public album created successfully';
        $username = $request->username;

        return redirect('/'.$timeline->username);
    }

    public function createPage(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        //Create timeline record for userpage
        $timeline = Timeline::create([
            'username' => $request->username,
            'name'     => $request->name,
            'about'    => $request->about,
            'type'     => 'page',
            ]);

        $page = Page::create([
            'timeline_id'           => $timeline->id,
            'category_id'           => $request->category,
            'member_privacy'        => Setting::get('page_member_privacy'),
            'message_privacy'       => Setting::get('page_message_privacy'),
            'timeline_post_privacy' => Setting::get('page_timeline_post_privacy'),
            ]);

        $role = Role::where('name', '=', 'Admin')->first();
        //below code inserting record in to page_user table
        $page->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
        //dd($page);
        $message = 'Page created successfully';
        $username = $request->username;

        return redirect('/'.$timeline->username);
    }

    public function addGroup($username)
    {
        
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.create_group').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/create-group', compact('username'))->render();
    }

    public function posts($username)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $timeline = Timeline::where('username', $username)->first();

        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));

        if ($timeline->type == 'user') {
            $follow_user_status = '';
            $user = User::where('timeline_id', $timeline['id'])->first();
            $followRequests = $user->followers()->where('status', '=', 'pending')->get();
            $liked_pages = $user->pageLikes()->get();
            $joined_groups = $user->groups()->get();
            $own_pages = $user->own_pages();
            $own_locations = $user->own_locations();
            $own_groups = $user->own_groups();
            $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
            $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
            $joined_groups_count = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get()->count();
            $follow_user_status = DB::table('followers')->where('follower_id', '=', Auth::user()->id)
                                ->where('leader_id', '=', $user->id)->first();
            $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
            $guest_events = $user->getEvents();


            if ($follow_user_status) {
                $follow_user_status = $follow_user_status->status;
            }

            $confirm_follow_setting = $user->getUserSettings(Auth::user()->id);
            $follow_confirm = $confirm_follow_setting->confirm_follow;

            $live_user_settings = $user->getUserPrivacySettings(Auth::user()->id, $user->id);
            $privacy_settings = explode('-', $live_user_settings);
            $timeline_post = $privacy_settings[0];
            $user_post = $privacy_settings[1];
        } else {
//            $user = User::where('id', Auth::user()->id)->first();
            $user = User::where('timeline_id', $timeline['id'])->first();
        }

        $posts = $user->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));

        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));


        return $theme->scope('timeline/posts', compact('timeline', 'user', 'posts', 'liked_pages', 'followRequests', 'joined_groups', 'own_pages', 'own_groups','own_locations', 'follow_user_status', 'following_count', 'followers_count', 'follow_confirm', 'user_post', 'timeline_post', 'joined_groups_count', 'next_page_url', 'user_events', 'guest_events'))->render();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function groupPageValidator(array $data)
    {
        $rules = [
            'name'     => 'required',
            'username' => 'required|max:16|min:5|alpha_num|unique:timelines|no_admin'
        ];
        
        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function createGroupPage(Request $request)
    {
        $validator = $this->groupPageValidator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->errors());
        }

        //Create timeline record for userpage
        $timeline = Timeline::create([
            'username' => $request->username,
            'name'     => $request->name,
            'about'    => $request->about,
            'type'     => 'group',
            ]);

        if ($request->type == 'open') {
            $group = Group::create([
            'timeline_id'    => $timeline->id,
            'type'           => $request->type,
            'active'         => 1,
            'member_privacy' => 'everyone',
            'post_privacy'   => 'members',
            'event_privacy'  => 'only_admins',
            ]);
        } else {
            $group = Group::create([
                'timeline_id'    => $timeline->id,
                'type'           => $request->type,
                'active'         => 1,
                'member_privacy' => Setting::get('group_member_privacy'),
                'post_privacy'   => Setting::get('group_timeline_post_privacy'),
                'event_privacy'  => Setting::get('group_event_privacy'),
                ]);
        }
        $role = Role::where('name', '=', 'Admin')->first();
        //below code inserting record in to page_user table
        if ($request->type == 'open' || $request->type == 'closed' || $request->type == 'secret') {
            $group->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'status' => 'approved']);
        } else {
            $group->users()->attach(Auth::user()->id, ['role_id' => $role->id]);
        }

        $message = trans('messages.create_page_success');
        $username = $request->username;

        return redirect('/'.$timeline->username);
    }

    public function pagesGroups($username)
    {
        
        $timeline = Timeline::where('username', $username)->with('user')->first();
        if ($timeline == null) {
            return redirect('/');
        }
        if ($timeline->id == Auth::user()->timeline_id) {
            $user = $timeline->user;
            $userPages = $user->own_pages();
            $groupPages = $user->own_groups();
            $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
            $theme->setTitle('Pages & Groups | '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
            return $theme->scope('timeline/pages-groups', compact('username', 'userPages', 'groupPages'))->render();
        } else {
            return redirect($timeline->username);
        }
    }

    public function generalPublicAlbumSettings($username)
    {
        //dd($username);
        $timeline = Timeline::where('username', $username)->with('public_album')->first();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.general_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('public_album/settings/general', compact('timeline', 'username'))->render();
    }

    public function generalLocationSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations')->first();

        $location=$timeline->locations()->first();

        $category=Category::where('language_code',Auth::user()->language)->where('category_type',0)->active()->orderBy('name', 'asc')->pluck('name', 'category_id')->all();
        $sub1_category=CategorySub1::where('category_id',$location->category_id)->where('language_code',Auth::user()->language)->active()->orderBy('sub1_name', 'asc')->pluck('sub1_name', 'category_sub1_id')->all();
        $sub2_category=CategorySub2::where('category_sub1_id',$location->category_sub1_id)->where('language_code',Auth::user()->language)->active()->orderBy('category_sub2_name', 'asc')->pluck('category_sub2_name', 'category_sub2_id')->all();

//       d($category);
        $category_options = ['' => 'Select Category'] + $category;
        $category_sub1_options = ['' => 'Select Sub1Category'] + $sub1_category;
        $category_sub2_options = ['' => 'Select Sub2Category'] + $sub2_category;
        $zipcode=$timeline->zip_code;

        if($timeline->city_sub1_id && !$timeline->zip_code){
            $citySub1=CitySub1::where('city_sub1_id',$timeline->city_sub1_id)->first();
            $code=$citySub1->zip_code()->first();
            if(isset($code)){
                $zipcode=$code->zip_code;
            }
        }

      //  dd($category_options);

        $country=Country::where('language_code',Auth::user()->language)->orderBy('country', 'asc')->pluck('country', 'country_id')->all();
        $state=State::where('language_code',Auth::user()->language)->where('country_id',$timeline->country_id)->orderBy('state', 'asc')->pluck('state', 'state_id')->all();
        $city=City::where('language_code',Auth::user()->language)
            ->where('country_id',$timeline->country_id)
            ->where('state_id',$timeline->state_id)
            ->orderBy('city', 'asc')
            ->pluck('city', 'city_id')->all();

        $city_sub=CitySub1::where('language_code',Auth::user()->language)
            ->where('country_id',$timeline->country_id)
            ->where('state_id',$timeline->state_id)
            ->where('city_id',$timeline->city_id)
            ->orderBy('city_sub1_name', 'asc')
            ->pluck('city_sub1_name', 'city_sub1_id')->all();

        $country_options = ['' => 'Select Country'] + $country;
        $state_options = ['' => 'Select State'] + $state;
        $city_options = ['' => 'Select City'] + $city;
        $city_sub_options = ['' => 'Select Sub City'] + $city_sub;
      
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.general_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/general', compact('timeline', 'username','location','category_options','category_sub1_options','category_sub2_options','country_options','state_options','city_options','city_sub_options','zipcode'))->render();
    }

    public function generalPageSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('page')->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.general_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('page/settings/general', compact('timeline', 'username'))->render();
    }




    public function updateGeneralPublicAlbumSettings(Request $request)
    {
        $validator = $this->groupPageSettingsValidator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }
        $timeline = Timeline::where('username', $request->username)->first();
        $timeline_values = $request->only('username', 'name', 'about');
        $update_timeline = $timeline->update($timeline_values);

        $public_album = PublicAlbum::where('timeline_id', $timeline->id)->first();
        $public_album_values = $request->only('address', 'phone', 'website');
        $update_public_album = $public_album->update($public_album_values);

        Flash::success(trans('messages.update_Settings_success'));

        return redirect()->back();
    }



    public function updateGeneralLocationSettings(Request $request)
    {
        $validator = $this->groupPageSettingsValidator($request->all());
        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }
        $timeline = Timeline::where('username', $request->username)->first();
        $timeline_values = $request->only('username', 'name', 'about','country_id','state_id','city_id','city_sub1_id','zip_code');
       // dd($timeline_values);
        $update_timeline = $timeline->update($timeline_values);

        $location = Location::where('timeline_id', $timeline->id)->first();
        $location_values = $request->only('address', 'phone', 'website','category_id','category_sub1_id','category_sub2_id');
        $update_page = $location->update($location_values);

        Flash::success(trans('messages.update_Settings_success'));

        return redirect()->back();
    }

    public function updateMapLocationSettings(Request $request)
    {

        $timeline = Timeline::where('username', $request->username)->first();
        $timeline_values = $request->only('latitude','longitude','map_verified');
        $update_timeline = $timeline->update($timeline_values);

        Flash::success(trans('messages.update_Settings_success'));

        return redirect()->back();
    }

    public function updateGeneralPageSettings(Request $request)
    {
        $validator = $this->groupPageSettingsValidator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }
        $timeline = Timeline::where('username', $request->username)->first();
        $timeline_values = $request->only('username', 'name', 'about');
        $update_timeline = $timeline->update($timeline_values);

        $page = Page::where('timeline_id', $timeline->id)->first();
        $page_values = $request->only('address', 'phone', 'website');
        $update_page = $page->update($page_values);


        Flash::success(trans('messages.update_Settings_success'));

        return redirect()->back();
    }




    public function privacyPublicAlbumSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $public_album_details = $timeline->public_album()->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.privacy_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('public_album/settings/privacy', compact('timeline', 'username', 'public_album_details'))->render();
    }

    public function privacyLocationSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $location_details = $timeline->locations()->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.privacy_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/privacy', compact('timeline', 'username', 'location_details'))->render();
    }


    public function privacyPageSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $page_details = $timeline->page()->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.privacy_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('page/settings/privacy', compact('timeline', 'username', 'page_details'))->render();
    }




    public function updatePrivacyPublicAlbumSettings(Request $request)
    {
        $timeline = Timeline::where('username', $request->username)->first();
        $public_album = PublicAlbum::where('timeline_id', $timeline->id)->first();
        $public_album->timeline_post_privacy = $request->timeline_post_privacy;
        $public_album->member_privacy = $request->member_privacy;
        $public_album->save();

        Flash::success(trans('messages.update_privacy_success'));

        return redirect()->back();
    }


    public function updatePrivacyLocationSettings(Request $request)
    {
        $timeline = Timeline::where('username', $request->username)->first();
        $location = Location::where('timeline_id', $timeline->id)->first();
        $location->timeline_post_privacy = $request->timeline_post_privacy;
        $location->member_privacy = $request->member_privacy;
        $location->save();

        Flash::success(trans('messages.update_privacy_success'));

        return redirect()->back();
    }

    public function updatePrivacyPageSettings(Request $request)
    {
        $timeline = Timeline::where('username', $request->username)->first();
        $page = Page::where('timeline_id', $timeline->id)->first();
        $page->timeline_post_privacy = $request->timeline_post_privacy;
        $page->member_privacy = $request->member_privacy;
        $page->save();

        Flash::success(trans('messages.update_privacy_success'));

        return redirect()->back();
    }

    public function rolesPublicAlbumSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $public_album = $timeline->public_album;
        $public_album_members = $public_album->members();
        $roles = Role::pluck('name', 'id');
        $theme = Theme::uses('default')->layout('default');
        $theme->setTitle(trans('common.manage_roles').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('public_album/settings/roles', compact('timeline', 'public_album_members', 'roles', 'public_album'))->render();
    }


    public function rolesLocationSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $location = $timeline->locations;
        $location_members = $location->members();
        $roles = Role::pluck('name', 'id');
        $theme = Theme::uses('default')->layout('default');
        $theme->setTitle(trans('common.manage_roles').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/roles', compact('timeline', 'location_members', 'roles', 'location'))->render();
    }

    public function rolesPageSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $page = $timeline->page;
        $page_members = $page->members();
        $roles = Role::pluck('name', 'id');
        $theme = Theme::uses('default')->layout('default');
        $theme->setTitle(trans('common.manage_roles').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('page/settings/roles', compact('timeline', 'page_members', 'roles', 'page'))->render();
    }



    public function likesPublicAlbumSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('public_album')->first();
        $public_album_likes = $timeline->public_album->likes()->where('user_id', '!=', Auth::user()->id)->get();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.page_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('public_album/settings/likes', compact('timeline', 'public_album_likes'))->render();
    }

    public function likesLocationSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations')->first();
        $location_likes = $timeline->locations->likes()->where('user_id', '!=', Auth::user()->id)->get();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.location_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/likes', compact('timeline', 'location_likes'))->render();
    }

    public function mapLocationSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations')->first();
        $location_likes = $timeline->locations->likes()->where('user_id', '!=', Auth::user()->id)->get();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.location_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/location-map', compact('timeline', 'username','location_likes'))->render();
    }



    
    public function likesPageSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('page')->first();
        $page_likes = $timeline->page->likes()->where('user_id', '!=', Auth::user()->id)->get();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.page_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('page/settings/likes', compact('timeline', 'page_likes'))->render();
    }



    //Getting group members
    public function getGroupMember($username, $group_id)
    {
        $timeline = Timeline::where('username', $username)->with('groups')->first();
        $group = $timeline->groups;
        $group_members = $group->members();
        $group_events = $group->getEvents($group->id);
        $ongoing_events = $group->getOnGoingEvents($group->id);
        $upcoming_events = $group->getUpcomingEvents($group->id);

        $member_role_options = Role::pluck('name', 'id');

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.members').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/members', compact('timeline', 'group_members', 'group', 'group_id', 'member_role_options', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    //Displaying group admins
    public function getAdminMember($username, $group_id)
    {
        $timeline = Timeline::where('username', $username)->with('groups')->first();
        $group = $timeline->groups;
        $group_admins = $group->admins();
        $group_members = $group->members();
        $member_role_options = Role::pluck('name', 'id');
        $group_events = $group->getEvents($group->id);
        $ongoing_events = $group->getOnGoingEvents($group->id);
        $upcoming_events = $group->getUpcomingEvents($group->id);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.admins').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/admin-group-member', compact('timeline', 'group', 'group_id', 'group_admins', 'member_role_options', 'group_members', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    //Displaying group members posts
    public function getGroupPosts($username, $group_id)
    {
        $user_post = 'group';
        $timeline = Timeline::where('username', $username)->with('groups')->first();
        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
        $group = $timeline->groups;
        $group_members = $group->members();
        $group_events = $group->getEvents($group->id);
        $ongoing_events = $group->getOnGoingEvents($group->id);
        $upcoming_events = $group->getUpcomingEvents($group->id);
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/groupposts', compact('timeline', 'group', 'posts', 'group_members', 'next_page_url', 'user_post', 'username', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    public function getJoinRequests($username, $group_id)
    {
        $group = Group::findOrFail($group_id);
        $requestedUsers = $group->pending_members();
        $timeline = Timeline::where('username', $username)->first();
        $group_members = $group->members();
        $group_events = $group->getEvents($group->id);
        $ongoing_events = $group->getOnGoingEvents($group->id);
        $upcoming_events = $group->getUpcomingEvents($group->id);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.join_requests').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/joinrequests', compact('timeline', 'username', 'requestedUsers', 'group_id', 'group', 'group_members', 'group_events', 'ongoing_events', 'upcoming_events'))->render();
    }

    //Getting public album members with count whose status approved
    public function getPublicAlbumMember($username)
    {
        $timeline = Timeline::where('username', $username)->with('public_album')->first();
        $public_album = $timeline->public_album;
        $public_album_members = $public_album->members();
        $roles = Role::pluck('name', 'id');

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.members').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/public-album-members', compact('timeline', 'public_album', 'roles', 'public_album_members'))->render();
    }

    //Getting location members with count whose status approved
    public function getLocationMember($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations')->first();
        $location = $timeline->locations;
        $location_members = $location->members();
        $roles = Role::pluck('name', 'id');
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.members').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/location-members', compact('timeline', 'location', 'roles', 'location_members','country','state','city'))->render();
    }



 //Getting page members with count whose status approved
    public function getPageMember($username)
    {
        $timeline = Timeline::where('username', $username)->with('page')->first();
        $page = $timeline->page;
        $page_members = $page->members();
        $roles = Role::pluck('name', 'id');

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.members').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/pagemembers', compact('timeline', 'page', 'roles', 'page_members'))->render();
    }



    //Displaying admin of the public album
    public function getPublicAlbumAdmins($username)
    {
        $timeline = Timeline::where('username', $username)->with('page')->first();
        $public_album = $timeline->public_album;
        $public_album_admins = $public_album->admins();
        $public_album_members = $public_album->members();
        $roles = Role::pluck('name', 'id');

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.admins').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/public-album-admins', compact('timeline', 'public_album', 'public_album_admins', 'roles', 'public_album_members'))->render();
    }


    //Displaying admin of the page
    public function getLocationAdmins($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations')->first();
        $location = $timeline->locations;
        $location_admins = $location->admins();
        $location_members = $location->members();
        $roles = Role::pluck('name', 'id');
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.admins').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/location-admins', compact('timeline', 'location', 'location_admins', 'roles', 'location_members','country','state','city'))->render();
    }

  //Displaying admin of the page
    public function getPageAdmins($username)
    {
        $timeline = Timeline::where('username', $username)->with('page')->first();
        $page = $timeline->page;
        $page_admins = $page->admins();
        $page_members = $page->members();
        $roles = Role::pluck('name', 'id');

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.admins').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('users/pageadmins', compact('timeline', 'page', 'page_admins', 'roles', 'page_members'))->render();
    }




    // Displaying public album likes
    public function getPublicAlbumLikes($username)
    {
        $timeline = Timeline::where('username', $username)->with('public_album', 'public_album.likes', 'public_album.users')->first();
        $public_album = $timeline->public_album;
        $public_album_likes = $public_album->likes()->get();
        $public_album_members = $public_album->members();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.page_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/public-album-likes', compact('timeline', 'public_album', 'public_album_likes', 'public_album_members'))->render();
    }

    // Displaying page likes
    public function getLocationLikes($username)
    {
        $timeline = Timeline::where('username', $username)->with('locations', 'locations.likes', 'locations.users')->first();
        $location = $timeline->locations;
        $location_likes = $location->likes()->get();
        $location_members = $location->members();

        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.location_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/location_likes', compact('timeline', 'location', 'location_likes', 'location_members','country','state','city'))->render();
    }

   // Displaying page likes
    public function getPageLikes($username)
    {
        $timeline = Timeline::where('username', $username)->with('page', 'page.likes', 'page.users')->first();
        $page = $timeline->page;
        $page_likes = $page->likes()->get();
        $page_members = $page->members();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.page_likes').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/page_likes', compact('timeline', 'page', 'page_likes', 'page_members'))->render();
    }

    //Displaying public album members posts
    public function getPublicAlbumPosts($username)
    {
        $user_post = 'public_album';
        $public_album_user_id = '';
        $timeline = Timeline::where('username', $username)->with('public_album', 'public_album.likes', 'public_album.users')->first();
        $public_album = $timeline->public_album;
        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
        $public_album_members = $public_album->members();
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/public-album-posts', compact('timeline', 'posts', 'public_album', 'public_album_user_id', 'public_album_members', 'next_page_url', 'user_post'))->render();
    }

      //Displaying page members posts
    public function getLocationPosts($username)
    {
        $user_post = 'location';
        $location_user_id = '';
        $timeline = Timeline::where('username', $username)->with('locations', 'locations.likes', 'locations.users')->first();
        $location = $timeline->locations;
        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
        $location_members = $location->members();
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/location-posts', compact('timeline', 'posts', 'location', 'location_user_id', 'location_members', 'next_page_url', 'user_post','country','state','city'))->render();
    }

    //Displaying page members posts
    public function getPagePosts($username)
    {
        $user_post = 'page';
        $page_user_id = '';
        $timeline = Timeline::where('username', $username)->with('page', 'page.likes', 'page.users')->first();
        $page = $timeline->page;
        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
        $page_members = $page->members();
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/pageposts', compact('timeline', 'posts', 'page', 'page_user_id', 'page_members', 'next_page_url', 'user_post'))->render();
    }



    //Assigning role for a member in group
    public function assignMemberRole(Request $request)
    {
        $chkUser_exists = '';
        $group = Group::findOrFail($request->group_id);
        $chkUser_exists = $group->chkGroupUser($request->group_id, $request->user_id);
        if ($chkUser_exists) {
            $result = $group->updateMemberRole($request->member_role, $request->group_id, $request->user_id);
            if ($result) {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            } else {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            }
        }
    }

    //Assigning role for a member in page
    public function assignPublicAlbumMemberRole(Request $request)
    {
        $chkUser_exists = '';
       //dd($request->public_album_id);
        $public_album = PublicAlbum::findOrFail($request->public_album_id);
        $chkUser_exists = $public_album->chkPublicUser($request->public_album_id, $request->user_id);
       // dd($public_album);
        if ($chkUser_exists) {
            $result = $public_album->updatePublicMemberRole($request->member_role, $request->public_album_id, $request->user_id);
            if ($result) {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            } else {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            }
        }
    }



    //Assigning role for a member in page
    public function assignLocationMemberRole(Request $request)
    {
        $chkUser_exists = '';

        $location = Location::findOrFail($request->location_id);
        $chkUser_exists = $location->chkLocationUser($request->location_id, $request->user_id);

        if ($chkUser_exists) {
            $result = $location->updateLocationMemberRole($request->member_role, $request->location_id, $request->user_id);
            if ($result) {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            } else {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            }
        }
    }

    //Assigning role for a member in page
    public function assignPageMemberRole(Request $request)
    {
        $chkUser_exists = '';
        $page = Page::findOrFail($request->page_id);

        $chkUser_exists = $page->chkPageUser($request->page_id, $request->user_id);

        if ($chkUser_exists) {
            $result = $page->updatePageMemberRole($request->member_role, $request->page_id, $request->user_id);
            if ($result) {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            } else {
                Flash::success(trans('messages.assign_role_success'));
                return redirect()->back();
            }
        }
    }



    //Removing member from group
    public function removeGroupMember(Request $request)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $chkUser_exists = '';
        $group = Group::findOrFail($request->group_id);

        $group_admins = $group->users()->where('group_id', $group->id)->where('role_id', '=', $admin_role_id->id)->get()->count();
        $group_members = $group->users()->where('group_id', $group->id)->where('user_id', '=', $request->user_id)->first();

        if ($group_members->pivot->role_id == $admin_role_id->id && $group_admins > 1) {
            $chkUser_exists = $group->removeMember($request->group_id, $request->user_id);
        } elseif ($group_members->pivot->role_id != $admin_role_id->id) {
            $chkUser_exists = $group->removeMember($request->group_id, $request->user_id);
        }

        if ($chkUser_exists) {
            if (Auth::user()->id != $request->user_id) {
                //Notify the user for accepting group's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $group->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.removed_from_group'), 'type' => 'remove_group_member']);
            }
            return response()->json(['status' => '200', 'deleted' => true, 'message' => trans('messages.remove_member_group_success')]);
        } else {
            return response()->json(['status' => '200', 'deleted' => false, 'message' => trans('messages.assign_admin_role_remove')]);
        }
    }

    //Removing member from public album
    public function removeLocationMember(Request $request)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $chkUser_exists = '';
        $location = Location::findOrFail($request->page_id);

        $location_admins = $location->users()->where('location_id', $location->id)->where('role_id', '=', $admin_role_id->id)->get()->count();
        $location_members = $location->users()->where('location_id', $location->id)->where('user_id', '=', $request->user_id)->first();

        if ($location_members->pivot->role_id == $admin_role_id->id && $location_admins > 1) {
            $chkUser_exists = $location->removeLocationMember($request->page_id, $request->user_id);
        } elseif ($location_members->pivot->role_id != $admin_role_id->id) {
            $chkUser_exists = $location->removeLocationMember($request->page_id, $request->user_id);
        }
          // else{
          //     return response()->json(['status' => '200','deleted' => false,'message'=>'Assign admin role for member and remove']);
          // }

        if ($chkUser_exists) {
            if (Auth::user()->id != $request->user_id) {
                //Notify the user for accepting page's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $location->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.removed_from_location'), 'type' => 'remove_location_member']);
            }
            return response()->json(['status' => '200', 'deleted' => true, 'message' => trans('messages.remove_member_location_success')]);
        } else {
            return response()->json(['status' => '200', 'deleted' => false, 'message' => trans('messages.assign_admin_role_remove')]);
        }
    }

      //Removing member from public album
    public function removePublicAlbumMember(Request $request)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();

        $chkUser_exists = '';
        $public_album = PublicAlbum::findOrFail($request->page_id);

        $page_admins = $public_album->users()->where('public_album_id', $public_album->id)->where('role_id', '=', $admin_role_id->id)->get()->count();
        $page_members = $public_album->users()->where('public_album_id', $public_album->id)->where('user_id', '=', $request->user_id)->first();

        if ($page_members->pivot->role_id == $admin_role_id->id && $page_admins > 1) {
            $chkUser_exists = $public_album->removePublicMember($request->page_id, $request->user_id);
        } elseif ($page_members->pivot->role_id != $admin_role_id->id) {
            $chkUser_exists = $public_album->removePublicMember($request->page_id, $request->user_id);
        }
          // else{
          //     return response()->json(['status' => '200','deleted' => false,'message'=>'Assign admin role for member and remove']);
          // }

        if ($chkUser_exists) {
            if (Auth::user()->id != $request->user_id) {
                //Notify the user for accepting page's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $public_album->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.removed_from_page'), 'type' => 'remove_page_member']);
            }

            return response()->json(['status' => '200', 'deleted' => true, 'message' => trans('messages.remove_member_page_success')]);
        } else {
            return response()->json(['status' => '200', 'deleted' => false, 'message' => trans('messages.assign_admin_role_remove')]);
        }
    }




    //Removing member from page
    public function removePageMember(Request $request)
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $chkUser_exists = '';
        $page = Page::findOrFail($request->page_id);

        $page_admins = $page->users()->where('page_id', $page->id)->where('role_id', '=', $admin_role_id->id)->get()->count();
        $page_members = $page->users()->where('page_id', $page->id)->where('user_id', '=', $request->user_id)->first();

        if ($page_members->pivot->role_id == $admin_role_id->id && $page_admins > 1) {
            $chkUser_exists = $page->removePageMember($request->page_id, $request->user_id);
        } elseif ($page_members->pivot->role_id != $admin_role_id->id) {
            $chkUser_exists = $page->removePageMember($request->page_id, $request->user_id);
        }
          // else{
          //     return response()->json(['status' => '200','deleted' => false,'message'=>'Assign admin role for member and remove']);
          // }

        if ($chkUser_exists) {
            if (Auth::user()->id != $request->user_id) {
                //Notify the user for accepting page's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $page->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.removed_from_page'), 'type' => 'remove_page_member']);
            }

            return response()->json(['status' => '200', 'deleted' => true, 'message' => trans('messages.remove_member_page_success')]);
        } else {
            return response()->json(['status' => '200', 'deleted' => false, 'message' => trans('messages.assign_admin_role_remove')]);
        }
    }

    public function acceptJoinRequest(Request $request)
    {
        $group = Group::findOrFail($request->group_id);

        $chkUser = $group->chkGroupUser($request->group_id, $request->user_id);


        if ($chkUser) {
            $group_user = $group->updateStatus($chkUser->id);

            if ($group_user) {
                //Notify the user for accepting group's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $group->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.accepted_join_request'), 'type' => 'accept_group_join']);
            }

            Flash::success('Request Accepted');

            return response()->json(['status' => '200', 'accepted' => true, 'message' => trans('messages.join_request_accept')]);
        }
    }

    public function rejectJoinRequest(Request $request)
    {
        $group = Group::findOrFail($request->group_id);
        $chkUser = $group->chkGroupUser($request->group_id, $request->user_id);

        if ($chkUser) {
            $group_user = $group->decilneRequest($chkUser->id);
            if ($group_user) {
              //Notify the user for rejected group's join request
                Notification::create(['user_id' => $request->user_id, 'timeline_id' => $group->timeline_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.rejected_join_request'), 'type' => 'reject_group_join']);
            }

            Flash::success('Request Rejected');

            return response()->json(['status' => '200', 'rejected' => true, 'message' => trans('messages.join_request_reject')]);
        }
    }

    public function updateUserGroupSettings(Request $request, $username)
    {
        $validator = $this->groupPageSettingsValidator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        $timeline = Timeline::where('username', $username)->first();
        $timeline->username = $username;
        $timeline->name = $request->name;
        $timeline->about = $request->about;
        $timeline->save();

        $group = Group::where('timeline_id', $timeline->id)->first();
        $group->type = $request->type;
        $group->member_privacy = $request->member_privacy;
        $group->post_privacy = $request->post_privacy;
        $group->event_privacy = $request->event_privacy;
        $group->save();

        Flash::success(trans('messages.update_group_settings'));

        return redirect()->back();
    }

    public function deleteComment(Request $request)
    {
        $comment = Comment::find($request->comment_id);

        if($comment->parent_id != null)
        {
            $parent_comment = Comment::find($comment->parent_id);
            $comment->update(['parent_id' => null]);
            $parent_comment->comments_liked()->detach();
            $parent_comment->delete();
        }
        else
        {
            $comment->comments_liked()->detach();
            $comment->delete();
        }
        if (Auth::user()->id != $comment->user_id) {
            //Notify the user for comment delete
            Notification::create(['user_id' => $comment->user->id, 'post_id' => $comment->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.deleted_your_comment'), 'type' => 'delete_comment']);
        }
        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Comment successfully deleted']);
    }

    public function deleteLocation(Request $request)
    {
        $location = Location::where('id', '=', $request->location_id)->first();
        $location->timeline->reports()->detach();
        $location->users()->detach();
        $location->likes()->detach();

        //Deleting locations notifications
        $timeline_alerts = $location->timeline()->with('notifications')->first();

        if (count($timeline_alerts->notifications) != 0) {
            foreach ($timeline_alerts->notifications as $notification) {
                $notification->delete();
            }
        }

        //Deleting locations posts
        $timeline_posts = $location->timeline()->with('posts')->first();
        if (count($timeline_posts->posts) != 0) {
            foreach ($timeline_posts->posts as $post) {
                $post->deleteMe();
            }
        }

        $location_timeline = $location->timeline();
        $location->delete();
        $location_timeline->delete();

        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Location successfully deleted']);
    }

    public function deletePublicAlbum(Request $request)
    {
        $public_album = PublicAlbum::where('id', '=', $request->public_album_id)->first();
        $public_album->timeline->reports()->detach();
        $public_album->users()->detach();
        $public_album->likes()->detach();

        //Deleting locations notifications
        $timeline_alerts = $public_album->timeline()->with('notifications')->first();

        if (count($timeline_alerts->notifications) != 0) {
            foreach ($timeline_alerts->notifications as $notification) {
                $notification->delete();
            }
        }

        //Deleting locations posts
        $timeline_posts = $public_album->timeline()->with('posts')->first();
        if (count($timeline_posts->posts) != 0) {
            foreach ($timeline_posts->posts as $post) {
                $post->deleteMe();
            }
        }

        $public_album_timeline = $public_album->timeline();
        $public_album->delete();
        $public_album_timeline->delete();

        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Public album successfully deleted']);
    }

    public function deletePage(Request $request)
    {
        // $page = Page::where('id', '=', $request->page_id)->first();

        // if ($page->delete()) {
        //     $users = $page->users()->get();
        //     foreach ($users as $user) {
        //         if ($user->id != Auth::user()->id) {
        //             //Notify the user for page delete
        //         Notification::create(['user_id' => $user->id, 'timeline_id' => $page->timeline->id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' deleted your page', 'type' => 'delete_page']);
        //         }
        //     }

        //     return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Page successfully deleted']);
        // } else {
        //     return response()->json(['status' => '200', 'notdeleted' => false, 'message' => 'Unsuccessful']);
        // }

        $page = Page::where('id', '=', $request->page_id)->first();

        $page->timeline->reports()->detach();
        $page->users()->detach();
        $page->likes()->detach();

        //Deleting page notifications
        $timeline_alerts = $page->timeline()->with('notifications')->first();
        if (count($timeline_alerts->notifications) != 0) {
            foreach ($timeline_alerts->notifications as $notification) {
                $notification->delete();
            }
        }

        //Deleting page posts
        $timeline_posts = $page->timeline()->with('posts')->first();
        if (count($timeline_posts->posts) != 0) {
            foreach ($timeline_posts->posts as $post) {
                $post->deleteMe();
            }
        }



        $page_timeline = $page->timeline();
        $page->delete();
        $page_timeline->delete();

        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Page successfully deleted']);
    }

    public function deletePost(Request $request)
    {
        $post = Post::find($request->post_id);
        
        if ($post->user->id == Auth::user()->id) {
            $post->deleteMe();
        }
        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Post successfully deleted']);
    }

    public function reportPost(Request $request)
    {
        $post = Post::where('id', '=', $request->post_id)->first();
        $reported = $post->managePostReport($request->post_id, Auth::user()->id);

        if ($reported) {
            //Notify the user for reporting his post
            Notification::create(['user_id' => $post->user_id, 'post_id' => $request->post_id, 'notified_by' => Auth::user()->id, 'description' => Auth::user()->name.' '.trans('common.reported_your_post'), 'type' => 'report_post']);

            return response()->json(['status' => '200', 'reported' => true, 'message' => 'Post successfully reported']);
        }
    }

    public function singlePost($post_id)
    {
        $mode = 'posts';
        $post = Post::where('id', '=', $post_id)->first();
        $timeline = Auth::user()->timeline;

        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums  = suggestedPublicAlbums();

        //Redirect to home page if post doesn't exist
        if ($post == null) {
            return redirect('/');
        }
        $theme = Theme::uses('default')->layout('default');
        $theme->setTitle(trans('common.post').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/single-post', compact('post', 'timeline', 'suggested_users', 'trending_tags', 'suggested_groups', 'suggested_pages', 'suggested_locations','suggested_public_albums','mode'))->render();
    }

    public function eventsList(Request $request, $username)
    {
        $mode = "eventlist";

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        
        $user_events = Event::where('user_id', Auth::user()->id)->get();
        $id = Auth::id();

        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums  = suggestedPublicAlbums();

        $next_page_url = url('ajax/get-more-feed?page=2&ajax=true&hashtag='.$request->hashtag.'&username='.$username);

        $theme->setTitle(trans('common.events').' | '.Setting::get('site_title').' | '.Setting::get('site_tagline'));

        return $theme->scope('home', compact('next_page_url', 'trending_tags', 'suggested_users', 'suggested_groups', 'suggested_pages','suggested_locations','suggested_public_albums', 'mode', 'user_events', 'username'))
        ->render();
    }

    public function addEvent($username, $group_id = null)
    {
        $timeline_name = '';
        if ($group_id) {
            $group = Group::find($group_id);
            $timeline_name = $group->timeline->name;
        }

        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums  = suggestedPublicAlbums();

        $country_options = ['' => 'Select Country'] + Country::where('language_code',Auth::user()->language)->pluck('country', 'country_id')->all();
//dd($country_options);
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        return $theme->scope('event-create', compact('suggested_users', 'suggested_groups', 'suggested_pages','suggested_locations','suggested_public_albums', 'username', 'group_id', 'timeline_name','country_options'))
            ->render();
    }

     /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateEventPage(array $data)
    {
        return Validator::make($data, [
            'name'        => 'required|max:30|min:5',
            'start_date'  => 'required',
            'end_date'    => 'required',
            'address'    => 'required',
            'country_id'    => 'required',
            'state_id'    => 'required',
            'type'        => 'required',
        ]);
    }

    public function createEvent($username, Request $request)
    {
        //dd($request->all());
        $validator = $this->validateEventPage($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }


        $start_date = date('Y-m-d H:i', strtotime($request->start_date));
        $end_date  = date('Y-m-d H:i', strtotime($request->end_date));


        if(isset($request->city_sub1_id)){
            $city_sub1_id=$request->city_sub1_id;
        }
        if(isset($request->zip_code)){
            $zip_code=$request->zip_code;
        }
        
        if ($start_date >= date('Y-m-d', strtotime(Carbon::now())) && $end_date >= $start_date) {
            $user_timeline = Timeline::where('username', $username)->first();
            $timeline = Timeline::create([
                'username'  => $user_timeline->gen_num(),
                'name'      => $request->name,
                'about'     => $request->about,
                'type'      => 'event',
                ]);

            $event = Event::create([
                'timeline_id' => $timeline->id,
                'type'        => $request->type,
                'user_id'     => Auth::user()->id,
//                'locations'    => $request->location,
                'city_sub1_id'    => $city_sub1_id,
                'zip_code'    => $zip_code,
                'address'    => $request->address,
                'country_id'    => $request->country_id,
                'state_id'    => $request->state_id,
                'start_date'  => date('Y-m-d H:i', strtotime($request->start_date)),
                'end_date'    => date('Y-m-d H:i', strtotime($request->end_date)),
                'invite_privacy'        => Setting::get('invite_privacy'),
                'timeline_post_privacy' => Setting::get('event_timeline_post_privacy'),
                ]);

            if ($request->group_id) {
                $event->group_id = $request->group_id;
                $event->save();
            }

            $event->users()->attach(Auth::user()->id);
            Flash::success(trans('messages.create_event_success'));
            return redirect('/'.$timeline->username);
        } else {
            $message = 'Invalid date selection';
            return redirect()->back()->with('message', trans('messages.invalid_date_selection'));
        }
    }

    //Displaying event posts
    public function getEventPosts($username)
    {
        $user_post = 'event';
        $timeline = Timeline::where('username', $username)->with('event', 'event.users')->first();
        $event = $timeline->event;

        if (!$event->is_eventadmin(Auth::user()->id, $event->id) &&  !$event->users->contains(Auth::user()->id)) {
            return redirect($username);
        }

        $posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
      
        $next_page_url = url('ajax/get-more-posts?page=2&username='.$username);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.posts').' | '.Setting::get('site_title').' | '.Setting::get('site_tagline'));

        return $theme->scope('timeline/eventposts', compact('timeline', 'posts', 'event', 'next_page_url', 'user_post'))->render();
    }

     //Displaying event guests
    public function displayGuests($username)
    {
        $timeline = Timeline::where('username', $username)->with('event')->first();
        $event = $timeline->event;
        $event_guests = $event->guests($event->user_id);
        
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.invitemembers').' | '.Setting::get('site_title').' | '.Setting::get('site_tagline'));

        return $theme->scope('users/eventguests', compact('timeline', 'event', 'event_guests'))->render();
    }

    public function generalEventSettings($username)
    {
        $timeline = Timeline::where('username', $username)->with('event')->first();

        $event_details = $timeline->event()->first();
       // dd($event_details);

        $country=Country::where('language_code',Auth::user()->language)->orderBy('country', 'asc')->pluck('country', 'country_id')->all();
        $state=State::where('language_code',Auth::user()->language)->where('country_id',$event_details->country_id)->orderBy('state', 'asc')->pluck('state', 'state_id')->all();
        $city=City::where('language_code',Auth::user()->language)
            ->where('country_id',$event_details->country_id)
            ->where('state_id',$event_details->state_id)
            ->orderBy('city', 'asc')
            ->pluck('city', 'city_id')->all();

        $city_sub=CitySub1::where('language_code',Auth::user()->language)
            ->where('country_id',$event_details->country_id)
            ->where('state_id',$event_details->state_id)
            ->where('city_id',$event_details->city_id)
            ->orderBy('city_sub1_name', 'asc')
            ->pluck('city_sub1_name', 'city_sub1_id')->all();

        $country_options = ['' => 'Select Country'] + $country;
        $state_options = ['' => 'Select State'] + $state;
        $city_options = ['' => 'Select City'] + $city;
        $city_sub_options = ['' => 'Select Sub City'] + $city_sub;

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.general_settings').' | '.Setting::get('site_title').' | '.Setting::get('site_tagline'));

        return $theme->scope('event/settings', compact('timeline', 'username', 'event_details','country_options','state_options','city_options','city_sub_options'))->render();
    }

    public function updateUserEventSettings($username, Request $request)
    {
        $validator = $this->validateEventPage($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }

        $start_date = date('Y-m-d H:i', strtotime($request->start_date));
        $end_date  = date('Y-m-d H:i', strtotime($request->end_date));
        
        if ($start_date >= date('Y-m-d', strtotime(Carbon::now())) && $end_date >= $start_date) {
            $timeline = Timeline::where('username', $username)->first();
            $timeline_values = $request->only('name', 'about');
            $update_timeline = $timeline->update($timeline_values);

            $event = Event::where('timeline_id', $timeline->id)->first();
            $event_values = $request->only('type', 'locations', 'invite_privacy', 'timeline_post_privacy');
            $event_values['start_date'] = date('Y-m-d H:i', strtotime($request->start_date));
            $event_values['end_date'] = date('Y-m-d H:i', strtotime($request->end_date));
            $update_values = $event->update($event_values);

            if ($request->group_id) {
                $event->group_id = $request->group_id;
                $event->save();
            }

            Flash::success(trans('messages.update_event_Settings'));
            return redirect()->back();
        } else {
            Flash::error(trans('messages.invalid_date_selection'));
            return redirect()->back();
        }
    }

    public function deleteEvent(Request $request)
    {
        $event = Event::find($request->event_id);
        
        //Deleting Events
        $event->users()->detach();

        // Deleting event posts
        $event_posts = $event->timeline()->with('posts')->first();
        
        if (count($event_posts->posts) != 0) {
            foreach ($event_posts->posts as $post) {
                $post->deleteMe();
            }
        }

        //Deleting event notifications
        $timeline_alerts = $event->timeline()->with('notifications')->first();

        if (count($timeline_alerts->notifications) != 0) {
            foreach ($timeline_alerts->notifications as $notification) {
                $notification->delete();
            }
        }

        $event_timeline = $event->timeline();
        $event->delete();
        $event_timeline->delete();
        
        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Event successfully deleted']);
    }

    public function allNotifications()
    {
        $mode = 'notifications';
        $notifications = Notification::where('user_id', Auth::user()->id)->with('notified_from')->latest()->paginate(Setting::get('items_page', 10));
        
        $trending_tags = trendingTags();
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums  = suggestedPublicAlbums();


        if ($notifications == null) {
            return redirect('/');
        }

        $theme = Theme::uses('default')->layout('default');
        $theme->setTitle(trans('common.notifications').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('timeline/single-post', compact('notifications', 'suggested_users', 'trending_tags', 'suggested_groups', 'suggested_pages','suggested_locations', 'suggested_public_albums', 'mode'))->render();
    }

    public function deleteNotification(Request $request)
    {
        $notification = Notification::find($request->notification_id);
        if ($notification->delete()) {
            Flash::success(trans('messages.notification_deleted_success'));

            return response()->json(['status' => '200', 'notify' => true, 'message' => 'Notification deleted successfully']);
        }
    }

    public function deleteAllNotifications(Request $request)
    {
        $notifications = Notification::where('user_id', Auth::user()->id)->get();

        if ($notifications) {
            foreach ($notifications as $notification) {
                $notification->delete();
            }

            Flash::success(trans('messages.notifications_deleted_success'));
            return response()->json(['status' => '200', 'allnotify' => true, 'message' => 'Notifications deleted successfully']);
        }
    }
    
    public function hidePost(Request $request)
    {
        $post = Post::where('id', '=', $request->post_id)->first();

        if ($post->user->id == Auth::user()->id) {
            $post->active = 0;
            $post->save();

            return response()->json(['status' => '200', 'hide' => true, 'message' => 'Post is hidden successfully']);
        } else {
            return response()->json(['status' => '200', 'unhide' => false, 'message' => 'Unsuccessful']);
        }
    }

    public function linkPreview()
    {
        $linkPreview = new LinkPreview('http://github.com');
        $parsed = $linkPreview->getParsed();
        foreach ($parsed as $parserName => $link) {
            echo $parserName. '<br>' ;
            echo $link->getUrl() . PHP_EOL;
            echo $link->getRealUrl() . PHP_EOL;
            echo $link->getTitle() . PHP_EOL;
            echo $link->getDescription() . PHP_EOL;
            echo $link->getImage() . PHP_EOL;
            print_r($link->getPictures());
            dd();
        }
    }

    public function deleteGroup(Request $request)
    {
        $group = Group::where('id', '=', $request->group_id)->first();
        
        $group->timeline->reports()->detach();
        
        //Deleting events in a group
        if (count($group->getEvents()) != 0) {
            foreach ($group->getEvents() as $event) {
                $event->users()->detach();

                // Deleting event posts
                $event_posts = $event->timeline()->with('posts')->first();

                if (count($event_posts->posts) != 0) {
                    foreach ($event_posts->posts as $post) {
                        $post->deleteMe();
                    }
                }

                //Deleting event notifications
                $timeline_alerts = $event->timeline()->with('notifications')->first();

                if (count($timeline_alerts->notifications) != 0) {
                    foreach ($timeline_alerts->notifications as $notification) {
                        $notification->delete();
                    }
                }

                $event_timeline = $event->timeline();
                $event->delete();
                $event_timeline->delete();
            }
        }
        $group->users()->detach();
        
        $timeline_alerts = $group->timeline()->with('notifications')->first();

        if (count($timeline_alerts->notifications) != 0) {
            foreach ($timeline_alerts->notifications as $notification) {
                $notification->delete();
            }
        }
        $timeline_posts = $group->timeline()->with('posts')->first();
        
        if (count($timeline_posts->posts) != 0) {
            foreach ($timeline_posts->posts as $post) {
                $post->deleteMe();
            }
        }
        $group_timeline = $group->timeline();
        $group->delete();
        $group_timeline->delete();

        return response()->json(['status' => '200', 'deleted' => true, 'message' => 'Group successfully deleted']);
    }

    public function allAlbums($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $albums = $timeline->albums()->with('photos')->get();

        $trending_tags = trendingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(Auth::user()->name.' '.Setting::get('title_seperator').' '.trans('common.albums').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/index', compact('timeline', 'albums', 'trending_tags'))->render();
    }

    public function allPhotos($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $albums = $timeline->albums()->get();

        if (count($albums) > 0) {
            foreach ($albums as $album) {
                $photos[] = $album->photos()->where('type', 'image')->get();
            }
            foreach ($photos as $photo) {
                foreach ($photo as $image) {
                    $images[] = $image;
                }
            }
        }
        $trending_tags = trendingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(Auth::user()->name.' '.Setting::get('title_seperator').' '.trans('common.photos').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/photos', compact('timeline', 'images', 'trending_tags'))->render();
    }

    public function allVideos($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        if (Setting::get('announcement') != null) {
            $election = Announcement::find(Setting::get('announcement'));
        }

        $albums = $timeline->albums()->get();
        
        if (count($albums) > 0) {
            foreach ($albums as $album) {
                $photos[] = $album->photos()->where('type', 'youtube')->get();
            }
            foreach ($photos as $photo) {
                foreach ($photo as $video) {
                    $videos[] = $video;
                }
            }
        }
        
        $trending_tags = trendingTags();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(Auth::user()->name.' '.Setting::get('title_seperator').' '.trans('common.photos').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/videos', compact('timeline', 'videos', 'trending_tags', 'election'))->render();
    }

    public function viewAlbum($username, $id)
    {
        $timeline = Timeline::where('username', $username)->first();
        $album = Album::where('id', $id)->with('photos')->first();

        $trending_tags = trendingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($album->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/show', compact('timeline', 'album', 'trending_tags'))->render();
    }

    public function viewPhotos($username)
    {
        $timeline = Timeline::where('username', $username)->first();

        $keepuser=$timeline->user()->first();
      //  dd($timeline);
        $photos_location=$keepuser->Medias()->where('posted_position','location')->get();
        $photos_post=$keepuser->Medias()->where('posted_position','post')->get();
        $photos_avatar=$keepuser->Medias()->where('posted_position','avatar')->get();
        $photos_cover=$keepuser->Medias()->where('posted_position','cover')->get();
      //  dd($photos_avatar);
        $trending_tags = trendingTags();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($timeline->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('users/show', compact('timeline', 'photos_cover','photos_location','photos_post','photos_avatar', 'trending_tags'))->render();
    }



    public function albumPhotoEdit(Request $request)
    {
        $media = Media::find($request->media_id);
        if ($media->source) {
            return response()->json(['status' => '200', 'photo_src' => true, 'pic_source' => $media->source]);
        } else {
            return response()->json(['status' => '200', 'photo_src' => false]);
        }
    }

    public function createAlbum($username)
    {
        $suggested_users = suggestedUsers();
        $suggested_groups = suggestedGroups();
        $suggested_pages = suggestedPages();
        $suggested_locations = suggestedLocations();
        $suggested_public_albums  = suggestedPublicAlbums();

        $timeline = Timeline::where('username', Auth::user()->username)->first();
        
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.create_album').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/create', compact('suggested_users', 'suggested_groups', 'suggested_pages','suggested_locations','suggested_public_albums', 'timeline'))->render();
    }

    protected function albumValidator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:30|min:5',
            'privacy'  => 'required'
          ]);
    }

    public function saveAlbum(Request $request, $username)
    {
        // $validator = $this->albumValidator($request->only('name','privacy'));

        // if ($validator->fails()) {
        //     return redirect()->back()
        //           ->withInput($request->all())
        //           ->withErrors($validator->errors());
        // }

        if ($request->album_photos[0] == null || $request->name == null || $request->privacy == null) {
            Flash::error(trans('messages.album_validation_error'));
            return redirect()->back();
        }

        $input = $request->except('_token', 'album_photos');
        $input['timeline_id'] = Timeline::where('username', $username)->first()->id;
        $album = Album::create($input);

        foreach ($request->album_photos as $album_photo) {
            $strippedName = str_replace(' ', '', $album_photo->getClientOriginalName());
            $photoName = date('Y-m-d-H-i-s').$strippedName;
            $photo = Image::make($album_photo->getRealPath());
            $photo->save(storage_path().'/uploads/albums/'.$photoName, 60);

            $media = Media::create([
              'title'  => $album_photo->getClientOriginalName(),
              'type'   => 'image',
              'source' => $photoName,
            ]);

            $album->photos()->attach($media->id);
        }

        if ($request->album_videos[0] != null) {
            foreach ($request->album_videos as $album_video) {
                $match;
                if (preg_match("/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/", $album_video, $match)) {
                    if ($match[2] != null) {
                        $videoId = Youtube::parseVidFromURL($album_video);
                        $video = Youtube::getVideoInfo($videoId);
                
                        $video = Media::create([
                        'title'  => $video->snippet->title,
                        'type'   => 'youtube',
                        'source' => $videoId,
                        ]);
                        $album->photos()->attach($video->id);
                    } else {
                        Flash::error(trans('messages.not_valid_url'));
                        return redirect()->back();
                    }
                } else {
                    Flash::error(trans('messages.not_valid_url'));
                    return redirect()->back();
                }
            }
        }

        if ($album) {
            Flash::success(trans('messages.create_album_success'));
            return redirect('/'.$username.'/album/show/'.$album->id);
        } else {
            Flash::error(trans('messages.create_album_error'));
        }
        return redirect()->back();
    }

    public function editAlbum($username, $id)
    {
        $album = Album::where('id', $id)->with('photos')->first();

        $trending_tags = trendingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle($album->name.' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('albums/edit', compact('album', 'trending_tags'))->render();
    }

    public function updateAlbum($username, $id, Request $request)
    {
        // $validator = $this->albumValidator($request->all());

        // if ($validator->fails()) {
        //     return redirect()->back()
        //           ->withInput($request->all())
        //           ->withErrors($validator->errors());
        // }
        if ($request->name == null || $request->privacy == null) {
            Flash::error(trans('messages.album_validation_error'));
            return redirect()->back();
        }

        $album = Album::findOrFail($id);
        $input = $request->except('_token', 'album_photos');
        $album->update($input);
        
        if ($request->album_photos[0] != null) {
            foreach ($request->album_photos as $album_photo) {
                $strippedName = str_replace(' ', '', $album_photo->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;
                $photo = Image::make($album_photo->getRealPath());
                $photo->save(storage_path().'/uploads/albums/'.$photoName, 60);

                $media = Media::create([
                  'title'  => $album_photo->getClientOriginalName(),
                  'type'   => 'image',
                  'source' => $photoName,
                ]);

                $album->photos()->attach($media->id);
            }
        }

        if ($request->album_videos[0] != null) {
            foreach ($request->album_videos as $album_video) {
                $match;
                if (preg_match("/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/", $album_video, $match)) {
                    if ($match[2] != null) {
                        $videoId = Youtube::parseVidFromURL($album_video);
                        $video = Youtube::getVideoInfo($videoId);
                
                        $video = Media::create([
                        'title'  => $video->snippet->title,
                        'type'   => 'youtube',
                        'source' => $videoId,
                        ]);
                        $album->photos()->attach($video->id);
                    } else {
                        Flash::error(trans('messages.not_valid_url'));
                        return redirect()->back();
                    }
                } else {
                    Flash::error(trans('messages.not_valid_url'));
                    return redirect()->back();
                }
            }
        }

        if ($album) {
            Flash::success(trans('messages.update_album_success'));
            return redirect('/'.$username.'/album/show/'.$album->id);
        } else {
            Flash::error(trans('messages.update_album_error'));
        }
        return redirect()->back();
    }

    public function deleteAlbum($username, $photo_id)
    {
        $album = Album::findOrFail($photo_id);
        $album->photos()->detach();
        if ($album->delete()) {
            Flash::success(trans('messages.delete_album_success'));
        } else {
            Flash::error(trans('messages.delete_album_error'));
        }
        return redirect('/'.$username.'/albums');
    }

    public function addPreview($username, $id, $photo_id)
    {
        $album = Album::findOrFail($id);
        $album->preview_id = $photo_id;
        if ($album->save()) {
            Flash::success(trans('messages.update_preview_success'));
        } else {
            Flash::error(trans('messages.update_preview_error'));
        }
        return redirect()->back();
    }

    public function deleteMedia($username, $photo_id)
    {
        $media = Media::find($photo_id);
        $media->albums()->where('preview_id', $media->id)->update(['albums.preview_id' => null]);
        $media->albums()->detach();
      
        if ($media->delete()) {
            Flash::success(trans('messages.delete_media_success'));
        } else {
            Flash::error(trans('messages.delete_media_error'));
        }
        return redirect()->back();
    }
    
    public function unjoinPage(Request $request)
    {
        $page = Page::where('timeline_id', '=', $request->timeline_id)->first();

        if ($page->users->contains(Auth::user()->id)) {
            $page->users()->detach([Auth::user()->id]);

            return response()->json(['status' => '200', 'join' => true, 'username'=> Auth::user()->username, 'message' => 'successfully unjoined']);
        }
    }
    public function saveWallpaperSettings($username, Request $request)
    {
        if($request->wallpaper == null)
        {
            Flash::error(trans('messages.no_file_added'));
            return redirect()->back();
        }

        $timeline = Timeline::where('username', $username)->first();
        $result = $timeline->saveWallpaper($request->wallpaper);
        if($result)
        {
            Flash::success(trans('messages.wallpaper_added_activated'));
            return redirect()->back();
        }
    }

    public function toggleWallpaper($username,$action, Media $media)
    {
        $timeline = Timeline::where('username', $username)->first();
        
        $result = $timeline->toggleWallpaper($action, $media);

        if($result == 'activate')
        {
            Flash::success(trans('messages.activate_wallpaper_success'));
        }
        if($result == 'deactivate')
        {
            Flash::success(trans('messages.deactivate_wallpaper_success'));
        }
        return Redirect::back();
    }

    public function PublicAlbumWallpaperSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();
        $wallpapers = Wallpaper::all();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.wallpaper_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));
        return $theme->scope('public_album/settings/wallpaper', compact('timeline', 'wallpapers'))->render();
    }

    public function locationWallpaperSettings($username)
    {
      $timeline = Timeline::where('username', $username)->first();
      $wallpapers = Wallpaper::all();

      $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.wallpaper_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('location/settings/wallpaper', compact('timeline', 'wallpapers'))->render();
    }

    public function pageWallpaperSettings($username)
    {
      $timeline = Timeline::where('username', $username)->first();
      $wallpapers = Wallpaper::all();

      $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
      $theme->setTitle(trans('common.wallpaper_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

      return $theme->scope('page/settings/wallpaper', compact('timeline', 'wallpapers'))->render();
    }



    public function groupGeneralSettings($username)
    {
        $timeline = Timeline::where('username', $username)->first();

        $group_details = $timeline->groups()->first();

        $group = Group::where('timeline_id', '=', $timeline->id)->first();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.group_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('group/settings/general', compact('timeline', 'username', 'group_details'))->render();
    }

    public function groupWallpaperSettings($username)
    {
      $timeline = Timeline::where('username', $username)->first();
      $wallpapers = Wallpaper::all();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle(trans('common.wallpaper_settings').' '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('group/settings/wallpaper', compact('timeline', 'wallpapers'))->render();
    }

    public function saveTimeline(Request $request)
    {
        $timeline = Timeline::find($request->timeline_id);
        if($timeline == null)
        {
           return response()->json(['status' => '201', 'message' => 'Invalid Timeline']); 
        }
        if(Auth::user()->timelinesSaved()->where('timeline_id',$request->timeline_id)->where('saved_timelines.type', $timeline->type)->get()->isEmpty())
        {
            Auth::user()->timelinesSaved()->attach($timeline->id, ['type' => $timeline->type]);
            return response()->json(['status' => '200', 'message' => $timeline->type.' saved successfully']);
        }
        else
        {
            Auth::user()->timelinesSaved()->detach($timeline->id);
            return response()->json(['status' => '200', 'message' => $timeline->type.' unsaved successfully']);
        }
    }

    public function savePost(Request $request)
    {
        $post = Post::find($request->post_id);
        if($post == null)
        {
           return response()->json(['status' => '201', 'message' => 'Invalid Post']); 
        }
        if(Auth::user()->postsSaved()->where('post_id',$request->post_id)->get()->isEmpty())
        {
            Auth::user()->postsSaved()->attach($post->id);
            return response()->json(['status' => '200', 'type' => 'save', 'message' => 'Post saved successfully']);
        }
        else
        {
            Auth::user()->postsSaved()->detach($post->id);
            return response()->json(['status' => '200', 'type' => 'unsave', 'message' => 'Post unsaved successfully']);
        }
    }

    public function switchLanguage(Request $request)
    {
        Auth::user()->update(['language' => $request->language]);
        App::setLocale($request->language);

        return response()->json(['status' => '200', 'message' => 'Switched language to '.$request->language]);
    }

    public function switchLanguage_guest(Request $request)
    {
        // Auth::user()->update(['language' => $request->language]);
        Session::put('language',$request->language);
        App::setLocale($request->language);
        Setting::set('language', $request->language);
        // dd(App::getLocale());
        return response()->json(['status' => '200', 'message' => 'Switched language to '.$request->language]);
    }
}
