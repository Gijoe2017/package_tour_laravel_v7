<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class PolicyController extends Controller
{
    //
    public function show_list(){
        $Policy=DB::table('privacy_policys')
            ->where('language_code',Session::get('language'))
            ->orderby('order_by','asc')
            ->get();
        return view('module.policy.list')->with('Policy',$Policy);
    }

    public function create(){
        return view('module.policy.create');
    }
    public function edit($id){
        $Policy=DB::table('privacy_policys')->where('id',$id)->first();
        return view('module.policy.edit')->with('Policy',$Policy);
    }


    public function show_details(Request $request){
        $Policy=DB::table('privacy_policys')->where('id',$request->id)->first();

        return view('module.policy.show-details')->with('Policy',$Policy);
    }


    public function store(Request $request){
    $check=DB::table('privacy_policys')->orderby('order_by','desc')->first();
    if($check){
        $order=$check->order_by+1;
    }else{
        $order=0;
    }
    $data=array(
        'title'=>$request->title,
        'details'=>$request->details,
        'order_by'=>$order,
        'language_code'=>Session::get('language'),
        'updated_at'=>date('Y-m-d h:i:s'),
        'update_by'=>Auth::user()->id,
    );
    DB::table('privacy_policys')->insert($data);

    return redirect('policy/list');
}

    public function update(Request $request){
        $check=DB::table('privacy_policys')
            ->where('language_code',Session::get('language'))
            ->orderby('order_by','asc')
            ->get();
        $order=$request->order_by;
        foreach ($check as $rows){
            if($rows->order_by>=$request->order_by){
                $order++;
                DB::table('privacy_policys')->where('id',$rows->id)->update(['order_by'=> $order]);
            }
        }

        $data=array(
            'title'=>$request->title,
            'details'=>$request->details,
            'order_by'=>$request->order_by,
            'language_code'=>Session::get('language'),
            'updated_at'=>date('Y-m-d h:i:s'),
            'update_by'=>Auth::user()->id,
        );
        DB::table('privacy_policys')->where('id',$request->id)->update($data);

        return redirect('policy/list');
    }



    public function delete($id){
        DB::table('privacy_policys')->where('id',$id)->delete();
        return redirect('policy/list');
    }
}
