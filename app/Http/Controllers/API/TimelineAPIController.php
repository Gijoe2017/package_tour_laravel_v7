<?php

namespace App\Http\Controllers\API;

use App\Group;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateTimelineAPIRequest;
use App\Http\Requests\API\UpdateTimelineAPIRequest;
use App\Location;
use App\Media;
use App\Page;
use App\Post;
use App\PublicAlbum;
use App\Repositories\TimelineRepository;
use App\Setting;
use App\Timeline;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use DB;

/**
 * Class TimelineController.
 */
class TimelineAPIController extends AppBaseController
{
    /** @var TimelineRepository */
    private $timelineRepository;

    public function __construct(TimelineRepository $timelineRepo)
    {
        $this->timelineRepository = $timelineRepo;
    }


   // *********************** API *****************
    public function timeline(){
        $timeline = DB::table('timelines')->where('id', '37844')->get();
        return $timeline;
    }

    public function timeline_posts(){
        $timeline = Timeline::where('id', '37844')->first();
        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->get();
//        $timeline_posts = $timeline->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->paginate(Setting::get('items_page'));
        $posts=array();
        foreach ($timeline_posts as $timeline_post) {
            //This is for filtering reported(flag) posts, displaying non flag posts
            if ($timeline_post->check_reports($timeline_post->id) == false) {
                array_push($posts, $timeline_post);
            }
        }
    }

    public function media_post(){
        $media=DB::table('media as a')->join('post_media as b','b.media_id','a.id')
            ->select('a.*','b.post_id')
            ->where('a.posted_position','post')
            ->get();
        return $media;
    }

    public function media_timeline(){
        $media=DB::table('media')
            ->where('posted_position','avatar')
            ->orwhere('posted_position','cover')
            ->get();

        return $media;
    }


    public function posts(){
        $id = Auth::id();
        $posts="";
            $posts = Post::whereIn('user_id', function ($query) use ($id) {
                $query->select('leader_id')
                    ->from('followers')
                    ->where('follower_id', $id);
            })->orWhere('user_id', $id)->where('active', 1)->latest()->get();
        return $posts;
    }
    public function post_hashtag(Request $request){
        $posts="";
        if ($request->hashtag) {
            $hashtag = '#'.$request->hashtag;
            $posts = Post::where('description', 'like', "%{$hashtag}%")->where('active', 1)->latest()->get();

        }
        return $posts;
    }


    public function suggested_locations(){

        $suggested_locations = Location::whereNotIn('id', Auth::user()->locationLikes()->pluck('location_id'))->whereNotIn('id', Auth::user()->locations()->pluck('location_id'))->get();
       // dd($suggested_locations);
        if (count($suggested_locations) > 0) {
            if (count($suggested_locations) > (int) Setting::get('min_items_page', 3)) {
                $suggested_locations = $suggested_locations->random((int) Setting::get('min_items_page', 3));
            }
        } else {
            $suggested_locations = '';
        }

        return $suggested_locations;
    }

    public function suggested_pages(){


        $suggested_pages = Page::whereNotIn('id', Auth::user()->pageLikes()->pluck('page_id'))->whereNotIn('id', Auth::user()->pages()->pluck('page_id'))->get();

        if (count($suggested_pages) > 0) {
            if (count($suggested_pages) > (int) Setting::get('min_items_page', 3)) {
                $suggested_pages = $suggested_pages->random((int) Setting::get('min_items_page', 3));
            }
        } else {
            $suggested_pages = '';
        }

        return $suggested_pages;
    }

    public function suggested_public_album(){


        $suggested_publice_albums = PublicAlbum::whereNotIn('id', Auth::user()->publicLikes()->pluck('public_album_id'))->whereNotIn('id', Auth::user()->publicalbums()->pluck('public_album_id'))->get();

        if (count($suggested_publice_albums) > 0) {
            if (count($suggested_publice_albums) > (int) Setting::get('min_items_page', 3)) {
                $suggested_publice_albums = $suggested_publice_albums->random((int) Setting::get('min_items_page', 3));
            }
        } else {
            $suggested_publice_albums = '';
        }

        return $suggested_publice_albums;
    }

    public function suggested_groups(){
        $suggested_groups = Group::whereNotIn('id', Auth::user()->groups()->pluck('group_id'))->where('type', 'open')->get();

        if (count($suggested_groups) > 0) {
            if (count($suggested_groups) > (int) Setting::get('min_items_page', 3)) {
                $suggested_groups = $suggested_groups->random((int) Setting::get('min_items_page', 3));
            }
        } else {
            $suggested_groups = '';
        }

        return $suggested_groups;
    }


    /**
     * @param Request $request
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/timelines",
     *      summary="Get a listing of the Timelines.",
     *      tags={"Timeline"},
     *      description="Get all Timelines",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Timeline")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->timelineRepository->pushCriteria(new RequestCriteria($request));
        $this->timelineRepository->pushCriteria(new LimitOffsetCriteria($request));
        $timelines = $this->timelineRepository->all();

        return $this->sendResponse($timelines->toArray(), 'Timelines retrieved successfully');
    }

    /**
     * @param CreateTimelineAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Post(
     *      path="/timelines",
     *      summary="Store a newly created Timeline in storage",
     *      tags={"Timeline"},
     *      description="Store Timeline",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Timeline that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Timeline")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Timeline"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTimelineAPIRequest $request)
    {
        $input = $request->all();

        $timelines = $this->timelineRepository->create($input);

        return $this->sendResponse($timelines->toArray(), 'Timeline saved successfully');
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/timelines/{id}",
     *      summary="Display the specified Timeline",
     *      tags={"Timeline"},
     *      description="Get Timeline",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Timeline",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Timeline"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Timeline $timeline */
        $timeline = $this->timelineRepository->find($id);
      
        if (empty($timeline)) {
            return Response::json(ResponseUtil::makeError('Timeline not found'), 400);
        }
        return $this->sendResponse($timeline->toArray(), 'Timeline retrieved successfully');
    }

    /**
     * @param int                      $id
     * @param UpdateTimelineAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Put(
     *      path="/timelines/{id}",
     *      summary="Update the specified Timeline in storage",
     *      tags={"Timeline"},
     *      description="Update Timeline",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Timeline",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Timeline that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Timeline")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Timeline"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTimelineAPIRequest $request)
    {
        $input = $request->all();

        /** @var Timeline $timeline */
        $timeline = $this->timelineRepository->find($id);

        if (empty($timeline)) {
            return Response::json(ResponseUtil::makeError('Timeline not found'), 400);
        }

        $timeline = $this->timelineRepository->update($input, $id);

        return $this->sendResponse($timeline->toArray(), 'Timeline updated successfully');
    }





    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Delete(
     *      path="/timelines/{id}",
     *      summary="Remove the specified Timeline from storage",
     *      tags={"Timeline"},
     *      description="Delete Timeline",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Timeline",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Timeline $timeline */
        $timeline = $this->timelineRepository->find($id);

        if (empty($timeline)) {
            return Response::json(ResponseUtil::makeError('Timeline not found'), 400);
        }

        $timeline->delete();

        return $this->sendResponse($id, 'Timeline deleted successfully');
    }
}
