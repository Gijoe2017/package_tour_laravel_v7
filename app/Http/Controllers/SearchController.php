<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timeline;
use DB;
use Auth;
use App\Country;
use App\State;
use App\City;
use Session;

class SearchController extends Controller
{
    //
    public function search_country(){
       return view('data.country');
    }

    public function search_location_home(Request $request){
        ini_set('memory_limit', '-1');

        $strQuery = DB::table('timelines as a')
            ->join('locations as b', 'b.timeline_id', '=', 'a.id')
            ->select('a.id')
            ->where('a.language_code', \App::getLocale())
            ->where('a.type', 'location');
        if($request->country_id){
            $strQuery->where('a.country_id', $request->country_id);
        }
        if($request->category_id){
            $strQuery->where('b.category_id', $request->category_id);
        }
        if($request->country_id){
            $strQuery->where('a.name', 'like', '%' . $request->name . '%');
        }

        $Location=$strQuery->paginate(15,['*'],'locations');

//
//        if($request->name && $request->country_id && $request->category_id) {
//            $Location = DB::table('timelines as a')
//                ->join('locations as b', 'b.timeline_id', '=', 'a.id')
//                ->select('a.id')
//                ->where('a.language_code', \App::getLocale())
//                ->where('a.type', 'location')
//                ->where('a.country_id', $request->country_id)
//                ->where('b.category_id', $request->category_id)
//                ->where('a.name', 'like', '%' . $request->name . '%')
//                ->paginate(15,['*'],'locations');
//        }else if($request->country_id && $request->category_id){
//                $Location=DB::table('timelines as a')
//                    ->join('locations as b','b.timeline_id','=','a.id')
//                    ->select('a.id')
//                    ->where('a.language_code',\App::getLocale())
//                    ->where('a.type','location')
//                    ->where('a.country_id',$request->country_id)
//                    ->where('b.category_id',$request->category_id)
//                    ->paginate(15,['*'],'locations');
//        }else if($request->name && $request->category_id){
//            $Location=DB::table('timelines as a')
//                ->join('locations as b','b.timeline_id','=','a.id')
//                ->select('a.id')
//                ->where('a.language_code',\App::getLocale())
//                ->where('a.type','location')
//                ->where('a.name','like','%'.$request->name.'%')
//                ->where('b.category_id',$request->category_id)
//                ->paginate(15,['*'],'locations');
//        }else if($request->country_id){
//
//            $Location=Timeline::where('country_id', $request->country_id)
//                ->where('language_code',\App::getLocale())
//                ->paginate(15,['*'],'locations');
//            if(!$Location){
//                $Location=Timeline::where('country_id', $request->country_id)
//                    ->select('a.id')
//                    ->where('language_code','th')
//                    ->paginate(15,['*'],'locations');
//            }
//
//        }else if($request->category_id){
//
//            $Location=DB::table('timelines as a')
//                ->join('locations as b','b.timeline_id','=','a.id')
//                ->select('a.id')
//                ->where('a.language_code',\App::getLocale())
//                ->where('b.category_id',$request->category_id)
//                ->paginate(15,['*'],'locations');
//        }else{
//
//            $Location=Timeline::where('country_id', $request->country_id)
//                ->select('a.id')
//                ->where('language_code',\App::getLocale())
//                ->where('a.name','like','%'.$request->name.'%')
//                ->paginate(15,['*'],'locations');
//            if(!$Location){
//                $Location=Timeline::where('country_id', $request->country_id)
//                    ->select('a.id')
//                    ->where('language_code','th')
//                    ->where('a.name','like','%'.$request->name.'%')
//                    ->paginate(15,['*'],'locations');
//            }
//
//        }
//
//        dd($Location);

        Session::put('search_location',$Location);
        
        return "Success!";
    }

    public function search_location(){

        ini_set('memory_limit', '-1');
        $keep=DB::table('timelines')->where('language_code',Session::get('language'))->where('type','location')->get();
        // dd($keep->toArray());

        foreach ($keep as $rows){
//            $country=null;$state=null;
//
//            $country=DB::table('countries')->where('language_code',Auth::user()->language)
//                ->where('country_id',$rows->country_id)
//                ->first();
//
//            if($country){
////                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
////                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
//
//                $addr=$country->country;
//            }
           // dd($addr);
            $locations[]=$rows->name;
        }
     //   dd($locations);
        return view('data.location')->with('locations',$locations);
    }


}
