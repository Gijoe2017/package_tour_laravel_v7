<?php

namespace App\Http\Controllers;

use App\Airline;
use App\CitySub1;
use App\Zipcode;
use Illuminate\Http\Request;
use App\Category;
use App\CategorySub1;
use App\CategorySub2;

use App\State;
use App\City;
use Auth;
use DB;
use Session;

class CommonController extends Controller
{
    //

    public function getSubcategory(Request $request){

        $option=null;
        $Category=DB::table('tour_category_sub1')
            ->where('TourCategoryID',$request->group)
            ->where('Status','Y')
            ->where('LanguageCode',Auth::user()->language)
            ->get();

        if($Category!=null){
            foreach ($Category as $cate){
                $option.="<option value='".$cate->groupID."'>".$cate->TourCategorySub1Name."</option>";
            }
        }
        // dd($option);
        return Response($option);
    }

    public function getAirline(Request $request){
        $option=null;

        $Airline=Airline::where('country_id',$request->country_id)->active()->get();

        if($Airline!=null){
            foreach ($Airline as $cate){
                if($cate->iata && $cate->icao){
                    $code='('.$cate->iata.', '.$cate->icao.')';
                }else if($cate->icao){
                    $code='('.$cate->icao.')';
                }else if($cate->iata){
                    $code='('.$cate->iata.')';
                }
                $option.="<option value='".$cate->airline."'>".$cate->airline_name.' '.$code."</option>";
            }
        }
        return Response($option);
    }


    public function getSubCate(Request $request){
        $option=null;
        $subCate=CategorySub1::where('category_id',$request->category_id)->active()->get();

        if($subCate!=null){
            foreach ($subCate as $cate){
                $option.="<option value='".$cate->category_sub1_id."'>".$cate->sub1_name."</option>";
            }
        }
        return Response($option);
    }

    public function getSub2Cate(Request $request){
        $option=null;
        $sub2Cate=CategorySub2::where('category_sub1_id',$request->category_sub1_id)->active()->get();
        if($sub2Cate!=null){
            foreach ($sub2Cate as $cate){
                $option.="<option value='".$cate->category_sub2_id."'>".$cate->category_sub2_name."</option>";
            }
        }
        return Response($option);
    }



    public function getState(Request $request){
        $option=null;
        $States=State::where('country_id',$request->country_id)->active()->get();

//        $option="";
        $option="<li value=''>".trans('profile.Choose')."</li>";
        if($States!=null){
            foreach ($States as $state){
//                $option.="<li value='".$state->state_id."'>".$state->state."</li>";
                $option.="<option value='".$state->state_id."'>".$state->state."</option>";
            }
        }
        return Response($option);
    }

    public function set_visacountry(Request $request){
        $option=null;
        $Country=DB::table('visa_type')->where('country_id',$request->country_id)->get();
//        $option="";
        $option="<li value=''>".trans('profile.Choose')."</li>";
        if($Country!=null){
            foreach ($Country as $rows){
//                $option.="<li value='".$state->state_id."'>".$state->state."</li>";
                $option.="<option value='".$rows->visa_type_id."'>".$rows->visa_name."</option>";
            }
        }
        return Response($option);
    }

    public function getCountryPartner(Request $request){
        $option=null;
        $Country=DB::table('package_tour as a')
            ->join('package_details as b','b.packageID','=','a.packageID')
            ->join('countries as c','c.country_id','=','b.Country_id')
            ->select('c.country_id','c.country')
            ->where('a.owner_timeline_id',$request->partner)
            ->where('c.language_code',Session::get('language'))
            ->groupby('c.country_id')
            ->get();
        //dd($Country);
        if($Country!=null){
            foreach ($Country as $rows){
                $option.="<option value='".$rows->country_id."'>".$rows->country."</option>";
            }
        }
        return Response($option);

    }






    public function getCity(Request $request){
        $option=null;
        $Coties=City::where('state_id',$request->state_id)->where('country_id',$request->country_id)->active()->get();
        $option="<option value=''>".trans('profile.Choose')."</option>";
        if($Coties!=null){
            foreach ($Coties as $coty){
//                $option.="<li value='".$coty->city_id."'>".$coty->city."</li>";
             $option.="<option value='".$coty->city_id."'>".$coty->city."</option>";
            }
        }
       // dd($option);
        return Response($option);
    }
    
    public function getCitySub(Request $request){
        $option=null;
        $CotySub=CitySub1::where('state_id',$request->state_id)
            ->where('city_id',$request->city_id)
            ->where('country_id',$request->country_id)
            ->where('language_code',Auth::user()->language)
            ->get();
        $option="<option value=''>".trans('profile.Choose')."</option>";
        if($CotySub!=null){
            foreach ($CotySub as $cotysub){
                $option.="<option value='".$cotysub->city_sub1_id."'>".$cotysub->city_sub1_name."</option>";
            }
        }
     
        return Response($option);
    }

    public function getZipcode(Request $request){
        $option=null;
        $Zipcode=Zipcode::where('city_id',$request->city_id)
            ->where('country_id',$request->country_id)
            ->where('city_sub1_id',$request->city_sub_id)
            ->first();


        return Response($Zipcode->zip_code);
    }
}
