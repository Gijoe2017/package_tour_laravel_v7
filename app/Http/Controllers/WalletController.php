<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class WalletController extends Controller
{
    //
    public function bank_account(){
        $BankAccount=DB::table('business_verified_bank as a')
            ->join('banks as b','b.bank_code','=','a.bank_id')
            ->join('country as c','c.country_id','=','a.country_id')
            ->select('a.*','c.country','b.bank_name')
            ->where('a.timeline_id',Session::get('timeline_id'))
            ->where('b.language_code',Session::get('language'))
            ->where('c.language_code',Session::get('language'))
            ->get();
        if(!$BankAccount){
            $BankAccount=DB::table('business_verified_bank as a')
                ->join('banks as b','b.bank_code','=','a.bank_id')
                ->join('country as c','c.country_id','=','a.country_id')
                ->select('a.*','c.country','b.bank_name')
                ->where('a.timeline_id',Session::get('timeline_id'))
                ->where('b.language_code','en')
                ->where('c.language_code','en')
                ->get();
        }
       // dd($BankAccount);
        return view('module.wallet.list')->with('BankAccount',$BankAccount);
    }


    public function seller_balance(){

        Session::put('currentUrl',url()->current());
        $Seller_balance=DB::table('package_payments as a')
            ->select('a.*','b.invoice_amount','b.invoice_booking_id','b.currency_symbol')
            ->join('package_invoice as b','b.invoice_id','=','a.invoice_id')
            ->where('b.invoice_timeline_id',Session('timeline_id'))
            ->where('b.invoice_status','>','1')
            ->groupby('b.invoice_id')
            ->get();



       // dd($Seller_balance);
        return view('module.wallet.seller_balance')->with('Seller_balance',$Seller_balance);
    }

    public function seller_balance_list(Request $request){

        $date=str_replace(' ','',$request->date);

        $dateArr=explode('-',$date);
        $st=date('Y-m-d',strtotime($dateArr[0]));
        $end=date('Y-m-d',strtotime($dateArr[1]));



        Session::put('currentUrl',url()->current());

        if($request->type=='all'){
            $str=DB::table('package_payments as a')
                ->join('package_invoice as b','b.invoice_id','=','a.invoice_id')
                ->where('b.invoice_timeline_id',Session('timeline_id'))
                ->where('b.invoice_status','>','1');
                if($st!=$end){
                    $str->where('a.payment_date','>=',$st)
                        ->where('a.payment_date','<=',$end);
                }
            $Seller_balance=$str->groupby('a.invoice_id')->get();
            // dd($Seller_balance);
            return view('module.wallet.ajax_seller_balance')->with('Seller_balance',$Seller_balance);
        }elseif($request->type=='order'){
            $str=DB::table('package_payments as a')
                ->join('package_invoice as b','b.invoice_id','=','a.invoice_id')
                ->where('b.invoice_timeline_id',Session('timeline_id'))
                ->where('b.invoice_status','>','1');
            if($st!=$end){
                $str->where('a.payment_date','>=',$st)
                    ->where('a.payment_date','<=',$end);
            }
            $Seller_balance=$str->groupby('a.invoice_id')->get();
            // dd($Seller_balance);
            return view('module.wallet.ajax_order_balance')->with('Seller_balance',$Seller_balance);
        }

    }





    public function bank_create(){
        $Banks=DB::table('banks')->where('language_code',Session::get('language'))->get();
        if(!$Banks){
            $Banks=DB::table('banks')->where('language_code','en')->get();
        }

        $Country=DB::table('country')->where('language_code',Session::get('language'))->orderby('country','asc')->get();
        if(!$Country){
            $Country=DB::table('country')->where('language_code','en')->orderby('country','asc')->get();
        }

        return view('module.wallet.bank-create')
            ->with('Country',$Country)
            ->with('Banks',$Banks);
    }

    public function bank_edit($id){
        $Banks=DB::table('banks')->where('language_code',Session::get('language'))->get();
        if(!$Banks){
            $Banks=DB::table('banks')->where('language_code','en')->get();
        }

        $Country=DB::table('country')->where('language_code',Session::get('language'))->orderby('country','asc')->get();
        if(!$Country){
            $Country=DB::table('country')->where('language_code','en')->orderby('country','asc')->get();
        }

        $Bank=DB::table('business_verified_bank')->where('id',$id)->first();
       // dd($Bank);
        return view('module.wallet.bank-edit')
            ->with('Banks',$Banks)
            ->with('Bank',$Bank)
            ->with('Country',$Country);
    }




    public function bank_store(Request $request){

        $account_id_card=str_replace('-','',$request->account_id_card);
        $data=array(
            'timeline_id'=>Session::get('timeline_id'),
            'bank_account_number'=>$request->bank_account_number,
            'account_name'=>$request->account_name,
            'bank_name'=>$request->bank_name,
            'sub_bank'=>$request->sub_bank,
            'country_id'=>$request->country,
            'bank_id'=>$request->bank_id,
            'bank_type'=>'1',
            'account_id_card'=>$account_id_card,
            'bank_default'=>isset($request->bank_default)?$request->bank_default:0,
            'created_at'=>date('Y-m-d h:i:s'),
            'created_user_id'=>Auth::user()->id,
        );
        DB::table('business_verified_bank')->insert($data);
        return redirect('wallet/bank_account');
    }

    public function bank_update(Request $request){
        $account_id_card=str_replace('-','',$request->account_id_card);
        $data=array(
            'timeline_id'=>Session::get('timeline_id'),
            'bank_account_number'=>$request->bank_account_number,
            'account_name'=>$request->account_name,
            'bank_name'=>$request->bank_name,
            'sub_bank'=>$request->sub_bank,
            'country_id'=>$request->country,
            'bank_id'=>$request->bank_id,
            'bank_type'=>'1',
            'account_id_card'=>$account_id_card,
            'bank_default'=>isset($request->bank_default)?$request->bank_default:0,
            'updated_at'=>date('Y-m-d h:i:s'),
            'updated_user_id'=>Auth::user()->id,
        );
        DB::table('business_verified_bank')->where('id',$request->id)->update($data);

        return redirect('wallet/bank_account');
    }



    public function delete($id){
        DB::table('privacy_policys')->where('id',$id)->delete();
        return redirect('policy/list');
    }
}
