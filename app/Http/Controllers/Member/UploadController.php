<?php

namespace App\Http\Controllers\Member;

use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use Illuminate\Support\Facades\Input;
use Validator;
use Image;
use Response;

class UploadController extends Controller
{
    //
    public function document_list(){
        $Docs=DB::table('users_passport_files')
                ->where('passport_id',Session::get('passport_id'))
                ->get();
//        Session::put('mode','member');
        return view('member.document.list')
            ->with('Docs',$Docs);
    }

    public function create(){
        $Docs=DB::table('users_passport_files')->where('passport_id',Session::get('passport_id'))->get();
        return view('member.document.create-file')->with('Docs',$Docs);
    }

    public function upload(){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $input = Input::all();
       // dd($input);
        $rules = array(
            'file' => 'image|max:30000',
        );
        //$dir=base_path() . '/images/member/docs/';
        $dir=public_path('/images/member/docs/') ;

//        $validation = Validator::make($input, $rules);
//
//        if ($validation->fails()) {
//            return Response::make($validation->errors->first(), 400);
//        }
        $type='image';
//        $source=Input::file('file')->getClientOriginalName();
        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
        // dd($extension);
        $fileName = date('YmdHi').rand(110, 999) . '.' . $extension; // renameing image
        if($extension=='pdf' || $extension=='doc' || $extension=='xls' || $extension=='docx' || $extension=='xlsx') {
               $type='file';
        }

        if(Session::get('passport_id')){
            $UserID=Session::get('passport_id');
        }else{
            $Check=DB::table('users_passport')->orderby('passport_id','asc')->first();
            $UserID=$Check->passport_id;
            Session::put('passport_id',$UserID);
        }

            $upload_success = Input::file('file')->move($dir, $fileName); //
        if ($upload_success) {
            $data=array(
                'passport_id'=>$UserID,
                'created_at'=>date('Y-m-d H:i:s'),
                'file_name'=>$fileName,
                'file_type'=>$type,
            );
            DB::table('users_passport_files')->insert($data);

            $Docs=DB::table('users_passport_files')
                ->where('passport_id',Session::get('passport_id'))
                ->get();

            return view('member.document.create-file')->with('Docs',$Docs);
        } else {
            return Response::json('error', 400);
        }

    }


    public function memberDocs() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $input = Input::all();

        $rules = array(
            'file' => 'image|max:30000',
        );
        //$dir=base_path() . '/images/member/docs/';
        $dir=public_path('/images/member/docs/') ;

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }
        $source=Input::file('file')->getClientOriginalName();
        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
       // dd($extension);
        $fileName = date('YmdHi').rand(110, 999) . '.' . $extension; // renameing image
        $Pics=Input::file('file');
        if($extension=='pdf' || $extension=='doc' || $extension=='xls' || $extension=='docx' || $extension=='xlsx'){
            $upload_success = Input::file('file')->move($dir, $fileName); // uploading file to given path
            $type='file';
        }else{
            list($ori_w, $ori_h) = getimagesize($Pics);

            if ($ori_w >= 1000) {
                if ($ori_w >= $ori_h) {
                    $new_w = 1000;
                    $new_h = round(($new_w / $ori_w) * $ori_h);
                } else {
                    $new_h = 650;
                    $new_w = round(($new_h / $ori_h) * $ori_w);
                }
            } else {
                $new_w = $ori_w;
                $new_h = $ori_h;
            }
            $upload_success=Image::make($Pics)->resize($new_w, $new_h)->save($dir. $fileName);
            $type='image';
        }


        if(Session::get('passport_id')){
            $UserID=Session::get('passport_id');
        }else{
            $Check=DB::table('users_passport')->orderby('passport_id','asc')->first();
            $UserID=$Check->passport_id;
            Session::put('passport_id',$UserID);
        }

        if ($upload_success) {
            $data=array(
                'passport_id'=>$UserID,
                'created_at'=>date('Y-m-d H:i:s'),
                'file_name'=>$fileName,
                'source'=>$source,
                'file_type'=>$type,
            );
            DB::table('users_passport_files')->insert($data);
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function delete($id){
        $check=DB::table('users_passport_files')->where('id',$id)->first();
       // dd($check);
        if($check){
            $dir=public_path('/images/member/docs/'.$check->file_name) ;
            if(file_exists($dir)){
                unlink($dir);
            }
            DB::table('users_passport_files')->where('id',$id)->delete();
        }

        $Docs=DB::table('users_passport_files')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        return view('member.document.create-file')->with('Docs',$Docs);

    }


    public function remove(Request $request){
        $check=DB::table('users_passport_files')->where('source',$request->name)->first();
        if($check){
            DB::table('users_passport_files')->where('source',$request->name)->delete();
            $dir=public_path('/images/member/docs/'.$check->source) ;
            unlink($dir);
            return Response::json('success', 200);
        }else{
            return Response::json('error', 400);
        }
    }


}
