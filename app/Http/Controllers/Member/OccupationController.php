<?php

namespace App\Http\Controllers\Member;

use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use App\Category;
use App\User;
use App\Timeline;
use App\Role;
use App\Location;
use App\Setting;

class OccupationController extends Controller
{
    //
    public function occupation_list(){
        $Occupation=DB::table('users_passport_occupation as a')
            ->join('users_occupation as b','b.occupation_id','=','a.occupation_id')
            ->join('timelines as c','c.id','=','a.timeline_id')
            ->join('locations as d','d.timeline_id','=','a.timeline_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('c.language_code',Auth::user()->language)
            ->groupby('a.occupation_id')
            ->get();
        Session::put('mode_lang','occupation');
        return view('member.occupation.list')
            ->with('Occupation',$Occupation);

    }



    public function create(){
        $Phones=DB::table('users_passport_occupation_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->select('a.*','b.country','b.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        return view('member.occupation.create')
            ->with('category_options',$category_options)
            ->with('Phones',$Phones);
    }

    public function edit($id){
        $Phones=DB::table('users_passport_occupation_phone as a')
            ->join('countries as b','b.country_id','=','a.country_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->get();

        $Occupation=DB::table('users_passport_occupation as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.occupation_id',$id)
            ->first();

      //  dd($Occupation);
        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Occupation->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Occupation->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Occupation->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Occupation->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Occupation->country_id)
                ->where('state_id',$Occupation->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Occupation->country_id)
                    ->where('state_id',$Occupation->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Occupation->country_id)
                    ->where('state_id',$Occupation->state_id)
                    ->where('city_id',$Occupation->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Occupation->country_id)
                        ->where('state_id',$Occupation->state_id)
                        ->where('city_id',$Occupation->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }
        //dd($Reference);
        return view('member.occupation.edit')
            ->with('Occupation',$Occupation)
            ->with('state',$state)
            ->with('country',$country)
            ->with('city',$city)
            ->with('city_sub',$city_sub)
            ->with('Phones',$Phones);
    }

    public function store(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'occupation_id' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;

                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;

            }

          //  dd($timeline_id);



            $data = array(
                'passport_id' => Session::get('passport_id'),
                'occupation_id' => $post['occupation_id'],
                'occupation_level_id' => $post['occupation_level_id'],
                'date_work' => $post['date_work'],
                'date_work_end' => $post['date_work_end'],
                'timeline_id' => $timeline_id,
//                'state_id' => $state,
//                'city_id' => $city,
//                'city_sub1_id' => $citySub,
//                'zip_code' => $zip_code,
                'created_at' => date('Y-m-d H:i:s'),
                'created_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,

            );

            DB::table('users_passport_occupation')->insert($data);
          //  dd($data);

//            $data = array(
//                'occupation_group_id' => $occupation_group_id,
//                'passport_id' => Session::get('passport_id'),
//                'employer_name_business' => $post['employer_name_business'],
//                'bussiness_address' => $post['bussiness_address'],
//                'created_at' => date('Y-m-d H:i:s'),
//                'created_user_id' => Auth::user()->id,
//                'updated_user_id' => Auth::user()->id,
//                'language_code' => Auth::user()->language,
//            );
//            // dd($data);
//            DB::table('users_passport_occupation2')->insert($data);

            if($request->occupation_phone_number){
                $check=DB::table('users_passport_occupation_phone')
                    ->where('phone_number',$request->occupation_phone_number)
                    ->where('passport_id',Session::get('passport_id'))
                    ->first();
                // dd($check);
                if(!$check){
                    $data=array(
                        'passport_id'=>Session::get('passport_id'),
                        'country_id'=>$request->phone_country_id,
                        'phone_country_code'=>$request->phone_country_code,
                        'phone_number'=>$request->occupation_phone_number,
                        'phone_type'=>$request->phone_type,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_user_id'=>Auth::user()->id,
                        'updated_user_id'=>Auth::user()->id,
                        'active'=>'1',
                    );
                    DB::table('users_passport_occupation_phone')->insert($data);
                    // dd($data);
                }
            }


            return redirect('/member/occupation');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
            'occupation_id' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            $data = array(
                'passport_id' => Session::get('passport_id'),
                'occupation_id' => $post['occupation_id'],
                'occupation_level_id' => $post['occupation_level_id'],
                'date_work' => $post['date_work'],
                'date_work_end' => $post['date_work_end'],
                'updated_user_id' => Auth::user()->id,

            );
            // dd($data);
            DB::table('users_passport_occupation')
                ->where('occupation_id',$post['occupation_id'])
                ->update($data);

            $check=DB::table('users_passport_occupation')->where('occupation_id',$post['occupation_id'])->first();
                    $data=array(
                        'country_id' => $post['country_id'],
                        'state_id' => $state,
                        'city_id' => $city,
                        'city_sub1_id' => $citySub,
                        'zip_code' => $zip_code,
                    );
                    DB::table('timelines')->where('id',$check->timeline_id)->update($data);

            $data=array(
                'address' => $post['address'],
            );
            DB::table('locations')->where('timeline_id',$check->timeline_id)->update($data);


//            $data = array(
//                'occupation_group_id' => $post['occupation_group_id'],
//                'passport_id' => Session::get('passport_id'),
//                'employer_name_business' => $post['employer_name_business'],
//                'bussiness_address' => $post['bussiness_address'],
//                'created_at' => date('Y-m-d H:i:s'),
//                'created_user_id' => Auth::user()->id,
//                'updated_user_id' => Auth::user()->id,
//                'language_code' => Auth::user()->language,
//            );
//            // dd($data);
//            $check=DB::table('users_passport_occupation2')
//                ->where('occupation_group_id',$post['occupation_group_id'])
//                ->first();
//            if(isset($check)){
//                DB::table('users_passport_occupation2')
//                    ->where('occupation_group_id',$post['occupation_group_id'])
//                    ->update($data);
//            }else{
//                DB::table('users_passport_occupation2')->insert($data);
//            }

            if($request->occupation_phone_number) {
                $check = DB::table('users_passport_occupation_phone')
                    ->where('phone_number', $request->occupation_phone_number)
                    ->where('passport_id', Session::get('passport_id'))
                    ->first();
                // dd($check);
                if (!$check) {
                    $data = array(
                        'passport_id' => Session::get('passport_id'),
                        'country_id' => $request->phone_country_id,
                        'phone_country_code' => $request->phone_country_code,
                        'phone_number' => $request->occupation_phone_number,
                        'phone_type' => $request->phone_type,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_user_id' => Auth::user()->id,
                        'updated_user_id' => Auth::user()->id,
                        'active' => '1',
                    );
                    DB::table('users_passport_occupation_phone')->insert($data);
                    // dd($data);
                }
            }
            return redirect('/member/occupation');
        }
    }



    public function delete($id){

        $check=DB::table('users_passport_occupation')
            ->where('occupation_group_id',$id)
            ->first();

        DB::table('users_passport_occupation')
            ->where('occupation_id',$id)
            ->delete();

        DB::table('users_passport_occupation_phone')
            ->where('passport_id',$check->passport_id)
            ->delete();
        return redirect()->back();
    }

    public function save_phone(Request $request){
        if($request->ajax()){
            $data=array(
                'passport_id'=>Session::get('passport_id'),
                'country_id'=>$request->phone_country_id,
                'phone_country_code'=>$request->phone_country_code,
                'phone_number'=>$request->phone_number,
                'phone_type'=>$request->phone_type,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_user_id'=>Auth::user()->id,
                'updated_user_id'=>Auth::user()->id,
                'active'=>'1',
            );
            DB::table('users_passport_occupation_phone')->insert($data);

//            $Phones=DB::table('users_passport_phone')->where('passport_id',Session::get('passport_id'))->get();
            $Phones=DB::table('users_passport_occupation_phone as a')
                ->join('countries as b','b.country_id','=','a.country_id')
                ->where('a.passport_id',Session::get('passport_id'))
                ->where('b.language_code',Auth::user()->language)
                ->get();
         //   dd($Phones);
            $output=" <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <th>".trans('LProfile.Country')."</th>
                                <th>".trans('LProfile.phone_code')."</th>
                                <th>".trans('LProfile.phone_type')."</th>
                                <th>".trans('LProfile.phone_name')."</th>
                                <th>".trans('LProfile.action')."</th>
                            </tr>
                            </thead>
                            <tbody>";
            foreach ($Phones as $phone){
                $output.="<tr>
                        <td>$phone->country</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                         <td align='center'><button class=\"btn btn-danger btn-sm occupation-phone-delete\"  data-id=\"".$phone->id."\"  ><i class=\"fa fa-trash\"></i> ".trans('LProfile.Delete')." </button></td>
                     </tr>";
            }
            $output.=" </tbody></table>";
            return Response($output);
        }
    }

    public function delete_phone(Request $request){

        DB::table('users_passport_occupation_phone')->where('id',$request->id)->delete();
        $Phones=DB::table('users_passport_occupation_phone')
            ->where('passport_id',Session::get('passport_id'))
            ->get();

        $output=" <table class=\"table table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>".trans('LProfile.Country')."</th>
                                        <th>".trans('LProfile.phone_code')."</th>
                                        <th>".trans('LProfile.phone_type')."</th>
                                        <th>".trans('LProfile.phone_name')."</th>
                                        <th>".trans('LProfile.action')."</th>
                                    </tr>
                                    </thead>
                                    <tbody>";
        foreach ($Phones as $phone){
            $output.="<tr>
                        <td>$phone->country_id</td>
                        <td>$phone->phone_country_code</td>
                        <td>$phone->phone_type</td>
                        <td>$phone->phone_number</td>
                        <td align='center'><button class=\"btn btn-danger btn-sm occupation-phone-delete\"  data-id=\"".$phone->id."\" ><i class=\"fa fa-trash\"></i> ".trans('LProfile.Delete')." </button></td>
                     </tr>";

        }
        $output.=" </tbody></table>";
        return Response($output);
    }
}
