<?php

namespace App\Http\Controllers\Member;
use App\Timeline;
use App\City;
use App\CitySub1;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use URL;
use App\Category;
use App\Setting;
use App\User;
use App\Location;
use App\Role;

class VisitedController extends Controller
{
    //
    public function visited_test(){
        return view('member.visited.test');
    }

    public function visited_list(){
        ini_set('memory_limit', '-1');

        $Visited=DB::table('users_passport_country_visited as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.passport_id',Session::get('passport_id'))
//            ->where('b.language_code',Auth::user()->language)
            ->where('a.visa_doe','<',date('Y-m-d'))
            ->groupby('a.visited_id')
            ->get();

        $Visiting=DB::table('users_passport_country_visited as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('a.visa_doe','>=',date('Y-m-d'))
            ->where('a.visa_doi','<=',date('Y-m-d'))
            ->groupby('a.visited_id')
            ->get();

        $Visit=DB::table('users_passport_country_visited as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.passport_id',Session::get('passport_id'))
            ->where('b.language_code',Auth::user()->language)
            ->where('a.visa_doi','>',date('Y-m-d'))
            ->groupby('a.visited_id')
            ->get();
//        dd($Visit);

//        $check=\App\models\LanguageVisited::where('passport_id',Session::get('passport_id'))->first();
//        dd($check->visit_language());
//        dd($Visited);
        Session::put('mode_lang','visited');
        return view('member.visited.list')
            ->with('Visited',$Visited)
            ->with('Visiting',$Visiting)
            ->with('Visit',$Visit);
    }

//    public function change_language($id){
//        $Visited=DB::table('users_passport_country_visited2')
//            ->where('country_visited_group_id',$id)
//            ->groupby('language_code')
//            ->get();
//        // dd($MemberLang);
//        return view('member.visited.visited-show-lang')
//            ->with('Visited',$Visited)
//            ->with('country_visited_group_id',$id);
//    }


    public function create(){
        ini_set('memory_limit', '-1');
        $Place = Timeline::where('type','location')->get();
        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();

        $VisaType=\App\VisaType::where('language_code',Auth::user()->language)->get();
        if(!count($VisaType)){
            $VisaType=\App\VisaType::where('language_code','en')->get();
        }

       // dd($VisaType);
        return view('member.visited.create')
            ->with('category_options',$category_options)
            ->with('VisaType',$VisaType)
            ->with('Place',$Place);
    }

    public function edit($id){

        $Visited=DB::table('users_passport_country_visited as a')
            ->join('timelines as b','b.id','=','a.timeline_id')
            ->join('locations as c','c.timeline_id','=','a.timeline_id')
            ->where('a.visited_id',$id)
            ->first();

//        $VisitedInfo=DB::table('users_passport_country_visited2')
//            ->where('language_code',Auth::user()->language)
//            ->where('country_visited_group_id',$id)
//            ->first();
        //dd($ReferenceInfo);

        $VisaType=\App\VisaType::where('language_code',Auth::user()->language)->get();
        if(!count($VisaType)){
            $VisaType=\App\VisaType::where('language_code','en')->get();
        }

        $city_sub=null;$state=null;$city=null;

        $country=Country::where('country_id',$Visited->country_id)->active()->get();

        if(count($country)==0){
            $country=Country::where('country_id',$Visited->country_id)->where('language_code','en')->get();
        }

        $state=State::where('country_id',$Visited->country_id)->where('language_code','en')->get();
        if(count($state)==0){
            $state=State::where('country_id',$Visited->country_id)->active()->get();
        }
        if($state){
            $city=City::where('country_id',$Visited->country_id)
                ->where('state_id',$Visited->state_id)
                ->active()
                ->get();
            if(count($city)==0){
                $city=City::where('country_id',$Visited->country_id)
                    ->where('state_id',$Visited->state_id)
                    ->where('language_code','en')
                    ->get();
            }
            if($city){
                $city_sub=CitySub1::where('country_id',$Visited->country_id)
                    ->where('state_id',$Visited->state_id)
                    ->where('city_id',$Visited->city_id)
                    ->active()
                    ->get();
                if(count($city_sub)==0){
                    $city_sub=CitySub1::where('country_id',$Visited->country_id)
                        ->where('state_id',$Visited->state_id)
                        ->where('city_id',$Visited->city_id)
                        ->where('language_code','en')
                        ->get();
                }
            }
        }

        $category_options = ['' => 'Select Category'] + Category::where('language_code',Auth::user()->language)->where('category_type','0')->active()->orderby('name','asc')->pluck('name', 'category_id')->all();


        // dd($Visited);
        return view('member.visited.edit')
            ->with('category_options',$category_options)
            ->with('Visited',$Visited)
            ->with('state',$state)
            ->with('country',$country)
            ->with('city',$city)
            ->with('VisaType',$VisaType)
            ->with('city_sub',$city_sub);
    }

    public function store(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
//            'visa_issued_place_name' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            $country_id=$request->country_id;
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;
                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;

            }

           // dd($timeline_id);
            $data = array(
                'passport_id' => Session::get('passport_id'),
                'timeline_id' => $timeline_id,
                'visa_type_id' => $post['visa_type_id'],
                'visa_issued_country_id' => $post['visa_issued_country_id'],
                'visa_number' => $post['visa_number'],
                'visa_doi' => date('Y-m-d',strtotime($post['visa_doi'])),
                'visa_doe' => date('Y-m-d',strtotime($post['visa_doe'])),
                'created_at' => date('Y-m-d H:i:s'),
                'created_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,
            );
           // dd($data);
            DB::table('users_passport_country_visited')->insert($data);

            return redirect('/member/visited');
        }
    }

    public function update(Request $request){
        $post = $request->all();
        $v = \Validator::make($request->all(), [
        //   'visa_issued_place_name' => 'required',+
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $state = "";
            $city = "";
            $citySub = "";
            $zip_code = '';
            if (isset($request->state_id)) {
                $state = $request->state_id;
            }
            if (isset($request->city_id)) {
                $city = $request->city_id;
            }
            if (isset($request->city_sub1_id)) {
                $citySub = $request->city_sub1_id;
                $zip_code = $request->zip_code;
            }

            if($request->name){
                $user = User::where('id', '=', Auth::user()->id)->first();
                $getId=Timeline::orderby('id','desc')->first();
                $id=$getId->id+1;
                $username=str_replace(' ','-',$request->name).'-'.$id.'-'.$user->language;
                $timeline = Timeline::create([
                    'username'              => $username,
                    'name'                  => $request->name,
                    'about'                 => "",
                    'country_id'            => $request->country_id,
                    'state_id'              => $state,
                    'city_id'               => $city,
                    'city_sub1_id'          => $citySub,
                    'zip_code'              => $zip_code,
                    'language_code'         => Auth::user()->language,
                    'created_user_id'       => Auth::user()->id,
                    'type'                  => 'location',
                ]);

                $location = Location::create([
                    'timeline_id'           => $timeline->id,
                    'category_id'           => $request->category_id,
                    'address'               => $request->address,
                    'category_sub1_id'      => "",
                    'member_privacy'        => Setting::get('location_member_privacy'),
                    'message_privacy'       => Setting::get('location_message_privacy'),
                    'timeline_post_privacy' => Setting::get('location_timeline_post_privacy'),
                ]);

                $role = Role::where('name', '=', 'Admin')->first();
                $location->users()->attach(Auth::user()->id, ['role_id' => $role->id, 'active' => 1]);
                $timeline_id = $timeline->id;

            }else{
                $check=Timeline::where('name',$request->q)->first();
                $timeline_id = $check->id;

            }
//            $check=DB::table('users_passport_country_visited')->where('visited_id',$post['visited_id'])->first();
//            $data=array(
//                'country_id' => $post['country_id'],
//                'state_id' => $state,
//                'city_id' => $city,
//                'city_sub1_id' => $citySub,
//                'zip_code' => $zip_code,
//                'name' => $post['name'],
//            );
//
//            DB::table('timelines')->where('id',$check->timeline_id)->update($data);
//
//            $data=array(
//                'address' => $post['address'],
//            );
//            DB::table('locations')->where('timeline_id',$check->timeline_id)->update($data);


            $data = array(
                'passport_id' => Session::get('passport_id'),
                'timeline_id' => $timeline_id,
                'visa_type_id' => $post['visa_type_id'],
                'visa_issued_country_id' => $post['visa_issued_country_id'],
                'visa_number' => $post['visa_number'],
                'visa_doi' => date('Y-m-d',strtotime($post['visa_doi'])),
                'visa_doe' => date('Y-m-d',strtotime($post['visa_doe'])),
                'created_at' => date('Y-m-d H:i:s'),
                'created_user_id' => Auth::user()->id,
                'updated_user_id' => Auth::user()->id,
            );
           //  dd($data);
            DB::table('users_passport_country_visited')->where('visited_id',$post['visited_id'])->update($data);

//            $data = array(
//                'country_visited_group_id' => $post['country_visited_group_id'],
//                'passport_id' => Session::get('passport_id'),
//                'visa_issued_place_name' => $post['visa_issued_place_name'],
//                'stayed_address' => $post['stayed_address'],
//                'updated_user_id' => Auth::user()->id,
//                'language_code' => Auth::user()->language
//            );
//            // dd($data);
//            $check=DB::table('users_passport_country_visited2')
//                ->where('country_visited_group_id',$post['country_visited_group_id'])
//                ->where('language_code',Auth::user()->language)
//                ->first();
//            if($check){
//                DB::table('users_passport_country_visited2')
//                    ->where('country_visited_group_id',$post['country_visited_group_id'])
//                    ->where('language_code',Auth::user()->language)
//                    ->update($data);
//            }else{
//                DB::table('users_passport_country_visited2')->insert($data);
//            }

            return redirect('/member/visited');
        }
    }

    public function delete($id){
        DB::table('users_passport_country_visited')
            ->where('visited_id',$id)
            ->delete();


        return redirect()->back();
    }

}
