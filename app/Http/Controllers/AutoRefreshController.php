<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
//use App\Mail\SendMailable;
use Session;
use DB;
use Auth;
use App;
use Carbon\Carbon;

class AutoRefreshController extends Controller
{
    //

    public function sendmail(){

        $name = 'Narong';
        Session::put('name',$name);
//
//        $Invoice=DB::table('package_invoice')
//            ->where('invoice_id','10')
//            ->first();
//
//        $Booking=DB::table('package_bookings')
//            ->where('booking_id',$Invoice->invoice_booking_id)
//            ->first();
//
//        $Booking_detail=DB::table('package_booking_details')
//            ->where('booking_id',$Invoice->invoice_booking_id)
////            ->where('package_id',$package)
//            ->get();

        $user=array(
            'email'=>'rong_2220@hotmail.com',
            'name'=>'Narong Mungtom',
            'booking_id'=>'1'
        );

       // dd($user);

        Mail::send('emails.tpl', $user, function ($m) use ($user) {
            $m->from('info@toechok.com', 'Toechok');
            $m->to($user['email'], $user['name'])->subject('Your Reminder!');
        });

//        Mail::to('narongmungtom@gmail.com')->send(new SendMailable($name));

        return 'Email was sent';
    }

    public function set_date_close(){
        $Details=DB::table('package_details')
            ->where('status','Y')
            ->get();

            $i=0;$j=0;
            foreach ($Details as $rows){
                $check=DB::table('condition_in_package_details as a')
                    ->join('condition_deposit_operation as b','b.condition_code','=','a.condition_id')
                    ->where('b.operation_sub_id','3')
                    ->where('a.packageID',$rows->packageID)
                    ->first();
                if($check){
                    $i++;
                    if($check->number_of_day==0){
                        $number_of_day=15;
                    }else{
                        $number_of_day=$check->number_of_day;
                    }
                    $closing_date = date('Y-m-d', strtotime("-" . $number_of_day . " day",strtotime($rows->packageDateStart)));
                    DB::table('package_details')->where('packageDescID',$rows->packageDescID)->update(['number_of_days_the_sale_is_closed'=>$number_of_day,'closing_date'=>$closing_date]);
                }else{
                    $j++;
                    $closing_date = date('Y-m-d', strtotime("-15 day",strtotime($rows->packageDateStart)));
                    DB::table('package_details')->where('packageDescID',$rows->packageDescID)->update(['number_of_days_the_sale_is_closed'=>15,'closing_date'=>$closing_date]);
                }
            }

       return 'Success!'.$i.'=30'.$j.'=15';
    }

    public function dateDiv($t1,$t2){
        $secondsDifference=strtotime($t1)-strtotime($t2);
        return $secondsDifference/86400;
    }

    public function refresh_cancel(){
        if(Auth::check()){
            App::setLocale(Auth::user()->language);
        }else{
            App::setLocale('th');
        }
        $invoiceText="";
        $checkCancel=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->join('users as c','c.id','=','b.booking_by')
            ->select('a.*','c.name','c.email')
            ->where('a.invoice_type','1')
          //  ->where('a.invoice_package_id','130')
            ->where('a.invoice_status','1')
            ->get();

        //dd($checkCancel);

        foreach ($checkCancel as $rows){
            $max_person=DB::table('package_details')
                ->where('packageDescID',$rows->invoice_package_detail_id)
                ->sum('NumberOfPeople');
            //echo $max_person;
            $number_booking=DB::table('package_booking_details')
//                ->where('booking_id',$rows->invoice_booking_id)
                ->where('package_id',$rows->invoice_package_id)
                ->where('package_detail_id',$rows->invoice_package_detail_id)
                ->where('booking_detail_status','>','1')
                ->where('booking_detail_status','!=','6')
                ->sum('number_of_person');

            if($max_person<=$number_booking){
                $message_cancel=trans('common.message_cancel_number_tourist_fully').'<br>'.trans('common.order_id').':#'.$rows->invoice_booking_id.'<BR>'.trans('common.invoice_no').':'.$rows->invoice_id;
                DB::table('package_invoice')->where('invoice_id',$rows->invoice_id)->update(['invoice_status'=>'6']);
                DB::table('package_booking_details')
                    ->where('package_detail_id', $rows->invoice_package_detail_id)
                    ->where('booking_id', $rows->invoice_booking_id)
                    ->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.cancel_by_system'),'updated_at'=>date('Y-m-d H:i:s')]);

                $Package=DB::table('package_booking_details')
                    ->where('booking_id',$rows->invoice_booking_id)
                    ->where('package_detail_id',$rows->invoice_package_detail_id)
                    ->first();
                $date=\Date::parse(date('Y-m-d'));
                $dataMail=array(
                    'email'=>$rows->email,
                    'name'=>$rows->name,
                    'subject'=>trans('email.cancellation').trans('common.order_id').':#'.$rows->invoice_booking_id,
                    'message_details'=>trans('common.this_email_is_to_inform_you_that_your_tour_package_has_been_canceled').'<br>'.trans('common.cancel_notification_in').' '.$date->format('d F, Y').'<br><br>'.$message_cancel,
                    'package_title'=>$Package->package_detail_title,
                    'booking_id'=>$rows->invoice_booking_id,
                    'invoice_id'=>$rows->invoice_id,
                );
                //dd($dataMail) ;
                Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
                    $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                    $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
                });

                $invoiceText.=$rows->invoice_id.', ';
            }
        }

        return 'Seccess!'.$invoiceText;

    }


    public function refresh(){
        if(!Session::has('language')){
            Session::put('language','th');
        }

        $checkCancel_deposit=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->join('users as c','c.id','=','b.booking_by')
            ->select('a.*','c.name','c.email')
            ->where('a.invoice_type','1')
            ->where('a.invoice_status','1')
            ->where('a.invoice_payment_date','<',date('Y-m-d'))
//            ->where('a.invoice_payment_date','Like','%'.date('Y-m-d').'%')
            ->get();

        $checkCancel_tour=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->join('users as c','c.id','=','b.booking_by')
            ->select('a.*','c.name','c.email')
            ->where('a.invoice_type','2')
            ->where('a.invoice_status','1')
            ->where('a.invoice_payment_date','<',date('Y-m-d'))
            ->get();

        //dd($checkCancel_deposit);
        foreach ($checkCancel_deposit as $rows){
//            if($rows->invoice_booking_id=='26'){
//                dd($rows);
//            }

            $message_cancel='';
            $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
            $dateStart=$Detail->packageDateStart;
            $dateCancel=$rows->invoice_payment_date;

            $dateStart=\Date::parse($dateStart);
            $dateCancel=\Date::parse($dateCancel);

            $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
            $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';

//            $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
//            $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';
            $days=$this->dateDiv($dateStart,$dateCancel);
            //dd($rows);
            $package=$rows->invoice_package_id;
            $condition=DB::table('package_condition as a')
                ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                ->whereIn('a.condition_code',function ($query) use($package){
                    $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                })
                ->where('a.condition_group_id','2')
                ->where('a.timeline_id',$rows->invoice_timeline_id)
                ->where('a.formula_id','!=','0')
                ->where('language_code',Session::get('language'))
                ->get();

          //  dd($condition);
            $value_tour=0;$remark='';$message_cancel2='';
            foreach ($condition as $rowC){
                if($rowC->operator_code=='Between'){
                    if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                        $remark=$rowC->condition_title;
                    }
                }else if($rowC->operator_code=='<'){
                    if($days<$rowC->condition_left){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                    }
                    $remark=$rowC->condition_title;
                }else{
                    if($days>$rowC->condition_left){
                        if($days<$rowC->condition_left){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                        }
                    }
                    $remark=$rowC->condition_title;
                }

                $remark?$message_cancel2.=$remark:'';
            }

            if($message_cancel2==''){
                $message_cancel.= trans('common.no_condition_cancel').'<hr>';
            }else{
                $message_cancel.=$message_cancel2;
            }

            $data=array(
                'payment_refund'=>$rows->invoice_amount-$value_tour,
                'payment_remark'=>$remark
            );

            DB::table('package_payments')->where('invoice_id',$rows->invoice_id)->update($data);
            DB::table('package_invoice')->where('invoice_id',$rows->invoice_id)->update(['invoice_status'=>'6']);
            DB::table('package_booking_details')
                ->where('booking_id',$rows->invoice_booking_id)
                ->where('package_detail_id',$rows->invoice_package_detail_id)
                ->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.cancel_by_system'),'updated_at'=>date('Y-m-d H:i:s')]);

            $Package=DB::table('package_booking_details')
                ->where('booking_id',$rows->invoice_booking_id)
                ->where('package_detail_id',$rows->invoice_package_detail_id)
                ->first();

            $dataMail=array(
                'email'=>$rows->email,
                'name'=>$rows->name,
                'subject'=>trans('email.cancellation').trans('common.order_id').':#'.$rows->invoice_booking_id,
                'message_details'=>trans('common.this_email_is_to_inform_you_that_your_tour_package_has_been_canceled').'<br>'.$message_cancel,
                'package_title'=>$Package->package_detail_title,
                'booking_id'=>$rows->invoice_booking_id,
                'invoice_id'=>$rows->invoice_id,
            );
            Mail::send('emails.cancellation', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });

        }

       //dd($checkCancel_tour);

        foreach ($checkCancel_tour as $rows){
            $message_cancel="";
            $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
            $dateStart=$Detail->packageDateStart;
            $dateCancel=$rows->invoice_payment_date;
            $dateStart=\Date::parse($dateStart);
            $dateCancel=\Date::parse($dateCancel);

            $message_cancel=trans('common.this_package_departs_on').' '.$dateStart->format('d F, Y').'<BR>';
            $message_cancel.=trans('common.cancel_notification_in').' '.$dateCancel->format('d F, Y').'<hr>';

//            $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
//            $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';
            $days=$this->dateDiv($dateStart,$dateCancel);

            $refund=""; $remark="";
            $package=$rows->invoice_package_id;
            $condition=DB::table('package_condition as a')
                ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
                ->whereIn('a.condition_code',function ($query) use($package){
                    $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
                })
                ->where('a.condition_group_id','2')
                ->where('a.timeline_id',$rows->invoice_timeline_id)
                ->where('a.formula_id','!=','0')
                ->where('language_code',Session::get('language'))
                ->get();

            $value_tour=0;$remark='';
            foreach ($condition as $rowC){

                if($rowC->operator_code=='Between'){
                    if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                    }

                    $remark=$rowC->condition_title;

                }else if($rowC->operator_code=='<'){
                    if($days<$rowC->condition_left){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                    }
                    $remark=$rowC->condition_title;

                }else{

                    if($days>$rowC->condition_left){
                        if($days<$rowC->condition_left){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                        }
                    }
                    $remark=$rowC->condition_title;
                }
                // dd($value_tour.'='.$remark);
            }

            $data=array(
                'payment_refund'=>$rows->invoice_amount-$value_tour,
                'payment_remark'=>$remark
            );

            DB::table('package_payments')->where('invoice_id',$rows->invoice_id)->update($data);
            DB::table('package_invoice')->where('invoice_id',$rows->invoice_id)->update(['invoice_status'=>'6']);

            $Details=DB::table('package_booking_details')
                ->where('booking_id',$rows->invoice_booking_id)
                ->where('package_id',$rows->invoice_package_id)
                ->where('package_detail_id',$rows->invoice_package_detail_id)
                ->where('booking_detail_status','!=','6')
                ->get();

            foreach ($Details  as $rows){
                DB::table('package_details')->where('packageDescID',$rows->package_detail_id)->increment('NumberOfPeople', $rows->number_of_person);
                DB::table('package_booking_details')
                    ->where('package_detail_id', $rows->package_detail_id)
                    ->where('booking_id', $rows->booking_id)
                    ->decrement('number_of_person', $rows->number_of_person);
                DB::table('package_booking_details')
                    ->where('booking_id', $rows->booking_id)
                    ->where('package_detail_id', $rows->package_detail_id)
                    ->update(['booking_detail_status'=>'6','booking_remark'=>trans('common.cancel_by_system'),'updated_at'=>date('Y-m-d H:i:s')]);
            }

        }

        return 'Success!';
    }

    public function notification_payment(){
        $date_pay[] = date("Y-m-d", strtotime("+" . 14 . " day"));
        $date_pay[] = date("Y-m-d", strtotime("+" . 7 . " day"));
        $date_pay[] = date("Y-m-d", strtotime("+" . 1 . " day"));

        $checkPayment=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->join('users as c','c.id','=','b.booking_by')
            ->select('a.*','c.name','c.email')
            ->where('a.invoice_type','1')
            ->where('a.invoice_status','1')
            ->whereIn('a.invoice_payment_date',$date_pay)
            ->get();

      //  dd($checkPayment);
        foreach ($checkPayment as $rows){
            $dataMail=array(
                'email'=>$rows->email,
                'name'=>$rows->name,
                'subject'=>trans('email.notify_deposit_payment'),
                'message_details'=>trans('common.please_pay_the_deposit_before').date('d/m/Y',strtotime($rows->invoice_payment_date)).'<BR>'.trans('common.invoice_no').'#'.$rows->invoice_id.'<Br>'.trans('email.this_is_an_automatic_system_notification').trans('common.tour_deposit').'<BR>'.trans('email.sorry_if_you_have_already_paid'),
                'booking_id'=>$rows->invoice_booking_id,
                'invoice_id'=>$rows->invoice_id,
            );
            Mail::send('emails.notification-to-payment', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }

        $checkPayment=DB::table('package_invoice as a')
            ->join('package_bookings as b','b.booking_id','=','a.invoice_booking_id')
            ->join('users as c','c.id','=','b.booking_by')
            ->select('a.*','c.name','c.email')
            ->where('a.invoice_type','2')
            ->where('a.invoice_status','1')
            ->whereIn('a.invoice_payment_date',$date_pay)
            ->get();
 // dd($checkPayment);

        foreach ($checkPayment as $rows){
            $dataMail=array(
                'email'=>$rows->email,
                'name'=>$rows->name,
                'subject'=>trans('email.notify_balance_payment'),
                'message_details'=>trans('common.please_pay_the_remaining_amount_before').date('d/m/Y',strtotime($rows->invoice_payment_date)).'<BR>'.trans('common.invoice_no').'#'.$rows->invoice_id.'<Br>'.trans('email.this_is_an_automatic_system_notification').trans('common.tour_balance').'<BR>'.trans('email.sorry_if_you_have_already_paid'),
                'booking_id'=>$rows->invoice_booking_id,
                'invoice_id'=>$rows->invoice_id,
            );
            Mail::send('emails.notification-to-payment', $dataMail, function ($m) use ($dataMail) {
                $m->from('info@toechok.com', 'Toechok Co.,ltd.');
                $m->to($dataMail['email'], $dataMail['name'])->subject($dataMail['subject']);
            });
        }

        return 'Success!';
    }

}
