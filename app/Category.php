<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id','description', 'parent_id', 'active','language_code'
    ];

    public function categories()
    {
          return $this->belongsTo('App\Page');
//        return $this->belongsTo('App\Public_album');
    }

    public function subcategory_home()
    {
        return $this->belongsTo('App\CategorySub1','category_id','category_id')->where('language_code',\App::getLocale());
//        return $this->belongsTo('App\Public_album');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\CategorySub1','category_id','category_id')->where('language_code',Auth::user()->language);
//        return $this->belongsTo('App\Public_album');
    }


    public function scopeActive($query)
    {
        $user = User::where('id', '=', Auth::user()->id)->first();
      //  dd($user);
        return $query->where('active', 1)->where('language_code',$user->language);
    }
}
