<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CitySub1 extends Model
{
    //
    public $table='cities_sub1';

    protected $fillable = [
        'city_id', 'city_sub1_id', 'city_sub1_name','state_id','language_code'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function zip_code()
    {
        return $this->belongsTo('App\Zipcode','city_sub1_id','city_sub1_id');
    }

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
