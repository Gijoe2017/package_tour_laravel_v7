<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    public $table='package_tour';
    protected $fillable = ['timeline_id', 'packageDays', 'packageNight', 'packageBy','packageStatus'];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
    'name' => 'required',
    ];




    /**
     * Get the user's  name.
     *
     * @param string $value
     *
     * @return string
     */
    public function getNameAttribute($value)
    {
        return $this->timeline->name;
    }

    /**
     * Get the user's  username.
     *
     * @param string $value
     *
     * @return string
     */
    public function getUsernameAttribute($value)
    {
        return $this->timeline->username;
    }

    /**
     * Get the user's  avatar.
     *
     * @param string $value
     *
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return $this->timeline->avatar ? $this->timeline->avatar->source : null;
    }

    /**
     * Get the user's  cover.
     *
     * @param string $value
     *
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return $this->timeline->cover ? $this->timeline->cover->source : null;
    }

    /**
     * Get the user's  about.
     *
     * @param string $value
     *
     * @return string
     */
    public function getAboutAttribute($value)
    {
        return $this->timeline->about ? $this->timeline->about : null;
    }

    public function toArray()
    {
        $array = parent::toArray();

        $timeline = $this->timeline->toArray();

        foreach ($timeline as $key => $value) {
            if ($key != 'id') {
                $array[$key] = $value;
            }
        }

        return $array;
    }

    public function timeline()
    {
        return $this->belongsTo('App\Timeline');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'package_user', 'package_id', 'user_id')->withPivot('role_id', 'active');
    }

    public function likes()
    {
        return $this->belongsToMany('App\User', 'package_likes', 'package_id', 'user_id');
    }

    public function is_admin($user_id)
    {
        $admin_role_id = Role::where('name', 'admin')->first();
        $pageUser = $this->users()->where('user_id', '=', $user_id)->where('role_id', '=', $admin_role_id->id)->where('package_user.active', 1)->first();
       // dd($pageUser);
        $result = $pageUser ? true : false;

        return $result;
    }

    public function members()
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $members = $this->users()->where('role_id', '!=', $admin_role_id->id)->where('package_user.active', 1)->get();

        $result = $members ? $members : false;

        return $result;
    }

    public function admins()
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $admins = $this->users()->where('role_id', $admin_role_id->id)->where('package_user.active', 1)->get();

        $result = $admins ? $admins : false;

        return $result;
    }

    public function chkPackageUser($package_id, $user_id)
    {
        $package_user = DB::table('package_user')->where('package_id', '=', $package_id)->where('user_id', '=', $user_id)->first();
        $result = $package_user ? $package_user : false;

        return $result;
    }

    public function updateStatus($package_user_id)
    {
        $package_user = DB::table('package_user')->where('id', $package_user_id)->update(['active' => 1]);
        $result = $package_user ? true : false;

        return $result;
    }

    public function updatePackageMemberRole($member_role, $package_id, $user_id)
    {
        $package_user = DB::table('page_user')->where('package_id', $package_id)->where('user_id', $user_id)->update(['role_id' => $member_role]);
        $result = $package_user ? true : false;

        return $result;
    }

    public function removePackageMember($package_id, $user_id)
    {
        $package_user = DB::table('page_user')->where('package_id', '=', $package_id)->where('user_id', '=', $user_id)->delete();

        $result = $package_user ? true : false;

        return $result;
    }


}
