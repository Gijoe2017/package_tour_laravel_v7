<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;


class Airline extends Model
{
    //
    public $table="airline";

    public function scopeActive($query)
    {
        return $query->where('language_code','en');
    }
}
