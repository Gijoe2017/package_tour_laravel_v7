<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PackagePromotion extends Model
{
    //
    public $table='package_promotion';

    public function scopeActive($query)
    {
       return $query->where('promotion_status', 'Y');
    }

}
