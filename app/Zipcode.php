<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
    //
    public $table='zipcodes';

    protected $fillable = [
        'city_id', 'city_sub1_id','zip_code', 'state_id'
    ];

    public function citysub1()
    {
        return $this->belongsTo('App\CitySub1');
    }


}
