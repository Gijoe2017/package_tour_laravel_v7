<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OccupationLevel extends Model
{
    //
    public $table="users_occupation_level";

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
