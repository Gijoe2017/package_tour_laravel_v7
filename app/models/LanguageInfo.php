<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguageInfo extends Model
{
    //
    protected $fillable = ['passport_id', 'language_code'];
    public $table="users_passport_info";

}
