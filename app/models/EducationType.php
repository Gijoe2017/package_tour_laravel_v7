<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class EducationType extends Model
{
    //
    public $table='education_type';
}
