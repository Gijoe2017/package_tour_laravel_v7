<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class LanguageVisited extends Model
{
    //
    protected $fillable = ['country_visited_group_id', 'language_code'];
    public $table="users_passport_country_visited";

    public function visit_language(){
        $check=DB::table('users_passport_country_visited as a')
                ->join('timelines as b','b.id','=','a.timeline_id')
                ->select('language_code')
                ->where('a.passport_id',Session::get('passport_id'))
                ->groupby('language_code')
                ->get();
        return $check;
    }

}
