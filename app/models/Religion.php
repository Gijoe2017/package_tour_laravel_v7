<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Religion extends Model
{
    //
    public $table='users_religion_status';

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
