<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PhoneCode extends Model
{
    //
    public $table='country_phonecode';
}
