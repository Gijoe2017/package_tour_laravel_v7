<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguageEducation extends Model
{
    //
    protected $fillable = ['education_id', 'language_code'];
    public $table="users_passport_education_info";

}
