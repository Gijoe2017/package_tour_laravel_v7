<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguagePlace extends Model
{
    //
    protected $fillable = ['address_stay_place_group_id', 'language_code'];
    public $table="users_passport_address_place_stay2";

}
