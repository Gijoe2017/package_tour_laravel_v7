<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguageRef extends Model
{
    //
    protected $fillable = ['reference_id', 'language_code'];
    public $table="users_passport_reference2";

}
