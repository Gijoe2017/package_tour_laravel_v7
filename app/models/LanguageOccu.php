<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguageOccu extends Model
{
    //
    protected $fillable = ['occupation_group_id', 'language_code'];
    public $table="users_passport_occupation2";

}
