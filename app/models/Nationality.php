<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Nationality extends Model
{
    //
    public $table='users_nationality';

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
