<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Title extends Model
{
    //
    public $table='users_title';

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
