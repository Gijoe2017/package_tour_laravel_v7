<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LanguageInfoFamily extends Model
{
    //
    protected $fillable = ['relation_id', 'language_code'];
    public $table="users_passport_family";

}
