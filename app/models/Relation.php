<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Relation extends Model
{
    //
    public $table="users_relation_type";

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
