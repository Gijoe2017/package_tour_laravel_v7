<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class City extends Model
{
    //
    protected $fillable = [
        'city', 'country_id', 'state_id','language_code'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City')->withDefault();
    }
    public function scopeActive($query)
    {
       // $user = User::where('id', '=', Auth::user()->id)->first();
        //  dd($user);
        return $query->where('language_code',Auth::user()->language);
    }
}
