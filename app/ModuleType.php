<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;


class ModuleType extends Model
{
    //
    public $table="business_verified_modules_type";

    public function module_open(){
        return $this->belongsTo('App\ModuleVerified','module_type_id','module_type_id');
    }

    public function scopeActive($query)
    {
        return $query->where('language_code',Auth::user()->language);
    }
}
