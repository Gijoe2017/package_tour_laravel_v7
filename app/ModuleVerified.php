<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleVerified extends Model
{
    //
    public $table="business_verified_modules";


    public function scopeActive($query)
    {
        return $query->where('business_verified_status','verified');
    }
}
