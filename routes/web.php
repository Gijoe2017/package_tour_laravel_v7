<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    return '<h1>Clear cache cleared</h1>';
});
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::post('ajax/switch-language', 'Package\PackageTourController@switchLanguage');
Route::post('ajax/switch-language_home', 'Package\PackageTourController@switchLanguage');
Route::post('ajax/switch-language-guest', 'TimelineController@switchLanguage_guest');

Route::get('setdate/close','AutoRefreshController@set_date_close');
//Route::get('/aomsin', 'HomeController@Aomsin');
//Route::get('/package/{group}', 'Package\PackageTourController@list_tour');
Route::group(['middleware' => ['web']], function () {
    Route::post('/upload_location', 'Package\UploadController@upload_location');
    Route::post('/upload_post', 'Package\UploadController@upload_post');
    Route::post('/delete_image_post', 'Package\UploadController@delete_image_post');
    Route::post('/delete_image_location', 'Package\UploadController@delete_image_location');

    Route::get('/ajax/policy/show/details', 'PolicyController@show_details');

    Route::get('/admin', 'Auth\LoginController@administrator');
    Route::get('/auto/refresh', 'AutoRefreshController@refresh');
    Route::get('/auto/refresh/cancel', 'AutoRefreshController@refresh_cancel');
    Route::get('/auto/notification/payment', 'AutoRefreshController@notification_payment');
    Route::get('/contact', 'HomeController@support');
    Route::get('/support', 'HomeController@support');
    Route::get('/send/email', 'AutoRefreshController@sendmail');
    Route::get('/', 'Package\PackageTourController@list_tour');
    Route::get('home/location/{group}', 'TimelineController@locationHomeGroup');
    Route::post('home/package/search', 'Package\PackageTourController@search_tour');
//    Route::post('home/package/search', 'HomeController@search_tour');
    Route::post('home/package/sort_by_country', 'Package\PackageController@sort_by_country');
    Route::get('home/package/sort_by_country', 'Package\PackageController@sort_by_country');
    Route::get('home/package/{group}', 'Package\PackageTourController@list_tour');
    Route::get('home/package_order/more', 'Package\PackageTourController@list_package_more');
    Route::get('home/details/{id}', 'Package\PackageTourController@View_details');
    Route::get('home/package/country/{id}', 'Package\PackageTourController@list_package_country');
    Route::get('home/package/agent/{id}', 'Package\PackageTourController@list_package_agent');
    Route::get('home/package/common/{type}', 'Package\PackageTourController@common_member_info');
    Route::get('home/package/category/{cate}', 'Package\PackageTourController@list_package_cate');
    Route::get('home/package/sub_cate/{subcate}', 'Package\PackageTourController@list_package_sub_cate');
    Route::get('home/package/wish/list', 'Package\PackageTourController@with_list');
    Route::get('ajax/show/price/selected', 'Package\PackageTourController@price_select');
    Route::get('check/email/member', 'Auth\LoginController@check_email');
    Route::get('ajax/check/email', 'Auth\RegisterController@check_email');

    Route::get('home/print/invoice1/{id}', 'Package\OrderTourController@print_invoice1');
    Route::get('home/print/invoice2/{id}', 'Package\OrderTourController@print_invoice2');

//******************************* Booking Back end **********************************/

    Route::get('booking/backend/show/invoice/{id}', 'Package\OrderTourController@invoice_detail');
    Route::get('booking/backend/show/invoice/{id}/all', 'Package\OrderTourController@invoice_detail_all');

    Route::get('booking/backend/show_update/invoice/{id}', 'Package\OrderTourController@status_booking');
    Route::get('booking/backend/update/invoice/{id}/{booking}', 'Package\OrderTourController@update_status_booking');

    Route::get('booking/backend/list', 'Package\OrderTourController@booking_list');
    Route::get('booking/backend/tour/info/{id}/{type}', 'Package\OrderMemberController@tourist');
    Route::get('booking/backend/payment/list', 'Package\OrderTourController@payment_list');
    Route::get('booking/backend/waiting/confirm', 'Package\OrderTourController@waiting_confirm_list');
    Route::get('booking/backend/main', 'Package\OrderTourController@main');

    Route::get('booking/backend_main/list', 'Package\OrderTourController@main_list');
    Route::get('booking/backend/add/{id}', 'Package\OrderTourController@package');
    Route::get('booking/backend/check/{id}', 'Package\OrderTourController@check_package');
    Route::get('booking/backend/popup/check', 'Package\OrderTourController@check_seat');
    Route::get('booking/backend/add_cart', 'Package\OrderTourController@add_cart');
    Route::get('booking/backend/check_out', 'Package\OrderTourController@check_out');

    Route::get('booking/backend/confirm/seat/{id}/{booking}', 'Package\OrderTourController@confirm_seat');
    Route::get('booking/backend/cancel/seat/{id}/{booking}', 'Package\OrderTourController@cancel_seat');

    Route::get('booking/{id}', 'Package\OrderTourController@create_order');
    Route::get('booking/c/{id}', 'Package\OrderTourController@booking');
    Route::get('booking/u/{id}', 'Package\OrderTourController@cart_tour');
    Route::get('booking/continuous/step2', 'Package\OrderTourController@continuous');
    Route::get('booking/continuous/payment', 'Package\OrderTourController@payment');
    Route::get('booking/continuous/invoice', 'Package\OrderTourController@check_invoice');

    Route::get('booking/ajax/set/address', 'Package\OrderMemberController@set_address_book');
    Route::get('booking/show/address', 'Package\OrderMemberController@show_address_book');
    Route::get('booking/show/invoice_address', 'Package\OrderMemberController@show_invoice_address_book');
    Route::get('booking/add/more/address', 'Package\OrderMemberController@more_address_book');
    Route::get('booking/add/more/invoice_address', 'Package\OrderMemberController@more_invoice_address_book');
    Route::get('booking/add/address', 'Package\OrderMemberController@address_book');
    Route::get('booking/edit/address/{id}', 'Package\OrderMemberController@edit_address_book');
    Route::get('booking/edit/invoice_address/{id}', 'Package\OrderMemberController@edit_invoice_address_book');
    Route::post('booking/edit/invoice_address', 'Package\OrderMemberController@booking_invoice_address_update');
    Route::post('booking/store/address', 'Package\OrderMemberController@booking_address_store');
    Route::post('booking/store/more/address', 'Package\OrderMemberController@booking_more_address_store');
    Route::post('booking/store/more/invoice_address', 'Package\OrderMemberController@booking_more_invoice_address_store');
    Route::post('booking/update/address', 'Package\OrderMemberController@booking_address_update');

    Route::get('booking/show/invoice/{id}/{type}', 'Package\OrderTourController@show_invoice');
    Route::get('booking/view/invoice/{id}/{type}', 'Package\OrderTourController@view_invoice');
    Route::get('booking/view/invoice/{id}/{detail_id}/{type}', 'Package\OrderTourController@view_invoice_all');
    Route::get('booking/view/invoice/{id}', 'Package\OrderTourController@view_invoice_by');

    Route::get('booking/problem/invoice/{id}', 'Package\ProblemTourController@problem_invoice');


    Route::get('booking/show/form_notification/{id}/{type}', 'Package\OrderTourController@show_form_notification');
    Route::get('booking/show/form_notification_id/{id}', 'Package\OrderTourController@show_form_notification_by_id');

    Route::get('booking/download/invoice/pdf/{id}/{package}/{type}', 'Package\OrderTourController@make_pdf_invoice');
    Route::get('booking/download/invoice/pdf/{id}/{type}', 'Package\OrderTourController@make_pdf_invoice');
    Route::get('booking/download/invoice/pdf/{id}', 'Package\OrderTourController@download_pdf_invoice');
    Route::get('booking/download/one/invoice/pdf/{id}', 'Package\OrderTourController@download_pdf_one_invoice');

    Route::get('booking/invoice/show_all/{id}', 'Package\OrderTourController@show_all_invoice');

    Route::post('booking/store', 'Package\OrderTourController@store_order');
    Route::post('booking/store_cart', 'Package\OrderTourController@store_cart');
    Route::get('my/bookings', 'Package\OrderTourController@my_bookings');
    Route::get('ajax/mybooking/status', 'Package\OrderTourController@search_my_bookings');

    Route::get('booking/cancel/invoice/{id}', 'Package\OrderTourController@cancel_bookings');
    Route::get('booking/cancel/option', 'Package\OrderTourController@cancel_option');
    Route::get('booking/cancel/package', 'Package\OrderTourController@cancel_package');
    Route::get('booking/cancel/person', 'Package\OrderTourController@cancel_person');
    Route::get('booking/confirm_cancel/package', 'Package\OrderTourController@confirm_cancel_package');
    Route::get('booking/confirm_cancel/person', 'Package\OrderTourController@confirm_cancel_person');
    Route::get('booking/confirm/cancel/{id}/{option}', 'Package\OrderTourController@cancel_confirm');

    Route::get('confirm/booking', 'Package\OrderTourController@booking_confirm');
    Route::post('confirm/booking', 'Package\OrderTourController@booking_confirm');

    Route::get('my/account', 'Member\MemberController@my_account');
    Route::post('my/account', 'Member\MemberController@change_password');
    Route::post('my/account/name', 'Member\MemberController@change_name');
    Route::post('my/account/picture', 'Member\MemberController@change_picture');

    /************************* Payment *****************************/
    Route::get('payment/notification/{id}/{type}', 'Package\PaymentController@Notification');
    Route::get('ajax/select/invoice', 'Package\PaymentController@SetNotification');
    Route::post('payment/omise/store', 'Package\PaymentController@OmisePayment');
    Route::post('payment/omise/store/all', 'Package\PaymentController@OmisePayment_All_invoice');


// ********** Agency **********************

    Route::get('home/partner/form/{id}', 'Package\AgencyController@partner_form');
    Route::get('home/agency/list', 'Package\AgencyController@list_agency');
    Route::get('home/request/partner', 'Package\AgencyController@list_partner');
    Route::get('home/partner/accept/{id}', 'Package\AgencyController@accept_partner');



    Route::get('ajax/contact/supplier/{id}', 'Package\PackageTourController@contat_supplier');
    Route::post('ajax/contact/supplier', 'Package\PackageTourController@send_contact_supplier');
    Route::get('/ajax/add/wishlist', 'Package\OrderTourController@add_withlish');
    Route::get('/ajax/delete/wishlist', 'Package\OrderTourController@delete_withlish');
    Route::get('/ajax/delete/addition_cart', 'Package\OrderTourController@addition_cart_delete');
    Route::get('/ajax/addition/cart', 'Package\OrderTourController@addition_cart_detail');
    Route::get('/ajax/set/cart_detail', 'Package\OrderTourController@set_cart_detail');
    Route::get('/ajax/set/cart_detail/all', 'Package\OrderTourController@set_cart_detail_all');

    Route::get('/ajax/remove/cart_detail', 'Package\OrderTourController@delete_cart_detail');
    Route::get('/ajax/set/payment', 'Package\OrderTourController@set_payment_option');
    Route::get('/ajax/update/cart', 'Package\OrderTourController@update_cart');
    Route::get('/ajax/remove/cart', 'Package\OrderTourController@delete_cart');
    Route::get('/ajax/set/someone_share', 'Package\OrderTourController@someone_share');
    Route::get('ajax/count/item/cart', 'Package\OrderTourController@count_item_cart');



    Route::get('/ajax/login/{group}/{id}', 'HomeController@login_form');
//    Route::get('/ajax/login_wishlist', 'HomeController@login_form2');
    Route::get('/ajax/login_wishlist', 'Package\PackageTourController@login_form2');
    Route::post('/ajax/switch-currency', 'HomeController@switch_currency');

//   ************ Settings *********************
    Route::get('/setting/title/create', 'SettingsController@create');
    Route::get('/setting/title/edit/{id}', 'SettingsController@edit');
    Route::post('/setting/title/store', 'SettingsController@store');
    Route::post('/setting/title/update', 'SettingsController@update');
    Route::get('/setting/title', 'SettingsController@title_list');


    Route::get('/setting/relation/create', 'SettingsController@relation_create');
    Route::get('/setting/relation/edit/{id}', 'SettingsController@relation_edit');
    Route::post('/setting/relation/store', 'SettingsController@relation_store');
    Route::post('/setting/relation/update', 'SettingsController@relation_update');
    Route::get('/setting/relation', 'SettingsController@relation_list');

    Route::get('/copy/package/{id}', 'SupervisorController@copy_package');
    // Route::get('register/2', 'Package\OrderTourController@continuous');
    Auth::routes();
});

// Login
Route::get('/alert_login', 'HomeController@alert_login');
Route::get('/user/register', 'HomeController@register');
Route::get('/login', 'Auth\LoginController@locationFeed');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/login2', 'Auth\LoginController@login2');
Route::post('/login/admin', 'Auth\LoginController@login_admin');
Route::post('login', 'LoginController@do')->name('login');

// Register
Route::get('/register', 'Auth\RegisterController@register');
Route::get('/register/2', 'Auth\RegisterController@register2');
Route::post('/register2', 'Auth\RegisterController@registerUser2');
Route::post('/register', 'Auth\RegisterController@registerUser');

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix' => '/module', 'middleware' => ['auth','editmodule']], function ($username) {
    ///******************** Manage *************************************/
    Route::get('/register/{id}', 'Module\ModuleController@register');
    Route::post('/register', 'Module\ModuleController@store_register');
    Route::get('/show_users/{id}', 'Module\ModuleController@list_users');
    Route::get('/add_users/{id}/{location}', 'Module\ModuleController@add_users');
    Route::get('/manage/{id}', 'Module\ModuleController@manage');
    Route::get('/manage_list', 'Module\ModuleController@module_list');
    Route::get('/manage_edit/{id}', 'Module\ModuleController@module_edit');
    Route::get('/documents/{id}', 'Module\ModuleController@documents');
    Route::post('/documents/store', 'Module\ModuleController@store_document');


    ///******************** Ecommerce *************************/
    Route::get('/ecommerce/product/list', 'Module\ProductController@index');

});

Route::group(['prefix' => '/member', 'middleware' => ['auth','editmember']], function ($username) {
    ///******************** Manage *************************************/
    Route::get('/dashboard/{id}', 'Member\MemberController@dashboard');
    Route::post('/create-location', 'Member\MemberController@create_location');
    Route::get('/change-language/{id}','Member\MemberController@change_language');
    Route::post('/set-timeline', 'Member\MemberController@setTimeline');
    Route::get('/show/docs', 'Member\MemberController@showImage');
    Route::get('/visa-country', 'CommonController@set_visacountry');


    //*******Admin Package Tour***********//
    //    Route::get('/package/manage','Package\PackageController@PackageList');


    Route::post('/store', 'Member\MemberController@store');


    /**************************** Member - Passport **************************************/

    Route::get('/manage/{id}', 'Member\MemberController@dashboard');
    Route::get('/list', 'Member\MemberController@MemberList');
    Route::get('/moreInfo/{id}', 'Member\MemberController@MemberInfo');
    Route::get('/edit/{id}', 'Member\MemberController@edit');
    Route::get('/delete/{id}', 'Member\MemberController@delete');

    Route::get('/create', 'Member\MemberController@Create');
    Route::post('/update', 'Member\MemberController@update');
    Route::post('/save', 'Member\MemberController@store');


    Route::post('/step1', 'Member\MemberController@save_step1');
    Route::get('/edit-step1', 'Member\MemberController@EditStep1');
    Route::post('/update/step1', 'Member\MemberController@update_step1');

    Route::get('/step2', 'Member\MemberController@create_step2');
    Route::post('/step2', 'Member\MemberController@save_step2');
    Route::get('/edit-step2', 'Member\MemberController@EditStep2');
    Route::post('/update/step2', 'Member\MemberController@update_step2');

    Route::get('/step3', 'Member\MemberController@create_step3');
    Route::post('/step3', 'Member\MemberController@save_step3');
    Route::get('/edit-step3', 'Member\MemberController@EditStep3');
    Route::get('/edit-address/{id}', 'Member\MemberController@EditAddress');
    Route::get('/contact', 'Member\MemberController@contact');
    Route::post('/update/step3', 'Member\MemberController@update_step3');
    Route::post('/save-phone', 'Member\MemberController@save_phone');
    Route::post('/save-email', 'Member\MemberController@save_email');
    Route::get('/delete-phone', 'Member\MemberController@delete_phone');
    Route::get('/delete-email', 'Member\MemberController@delete_email');


    /************************ Contact *********************************/
    Route::post('/ajax-save-address', 'Member\MemberController@save_address');
    Route::get('/delete-address', 'Member\MemberController@delete_address');
//    Route::get('/edit-address/{id}', 'Member\MemberController@edit_address');
    Route::post('/edit-address', 'Member\MemberController@update_address');
    Route::get('/address/edit', 'Member\MemberController@address_edit');
    Route::post('/address/add', 'Member\MemberController@add_address');

    /******************** Family ****************************/

    Route::get('/step4', 'Member\FamilyController@family_list');
    Route::get('/family/create', 'Member\FamilyController@create');
    Route::post('/family/save', 'Member\FamilyController@save');
    Route::post('/family/update', 'Member\FamilyController@update');
    Route::get('/family/edit/{id}', 'Member\FamilyController@edit');
    Route::get('/family/delete/{id}', 'Member\FamilyController@delete');
    Route::get('/family/change-language/{id}','Member\FamilyController@change_language');

    /************************ Documents *********************************/
    Route::get('/documents', 'Member\UploadController@document_list');
    Route::get('/document/create', 'Member\UploadController@create');
    Route::post('/upload/docs/remove', 'Member\UploadController@remove');
    Route::get('/document/delete/{id}', 'Member\UploadController@delete');
    Route::post('/upload/docs', 'Member\UploadController@memberDocs');
    Route::post('/upload', 'Member\UploadController@upload');



    /******************** Education **************************/

    Route::get('/education', 'Member\EducationController@education_list');
    Route::get('/education/create', 'Member\EducationController@create');
    Route::post('/education/save', 'Member\EducationController@store');
    Route::post('/education/update', 'Member\EducationController@update');
    Route::get('/education/edit/{id}', 'Member\EducationController@edit');
    Route::get('/education/delete/{id}', 'Member\EducationController@delete');
    Route::get('/education/change-language/{id}','Member\EducationController@change_language');

    /*************************** Referrence*************************************/

    Route::get('/reference', 'Member\ReferenceController@reference_list');
    Route::get('/reference/create', 'Member\ReferenceController@create');
    Route::get('/reference/edit/{id}', 'Member\ReferenceController@edit');
    Route::get('/reference/delete/{id}', 'Member\ReferenceController@delete');
    Route::post('/reference/save', 'Member\ReferenceController@store');
    Route::post('/reference/update', 'Member\ReferenceController@update');
    Route::post('/save-ref-phone', 'Member\ReferenceController@save_phone');
    Route::get('/delete-ref-phone', 'Member\ReferenceController@delete_phone');
    Route::get('/reference/change-language/{id}','Member\ReferenceController@change_language');

    /*************************** Occupation *************************************/

    Route::get('/occupation', 'Member\OccupationController@occupation_list');
    Route::get('/occupation/create', 'Member\OccupationController@create');
    Route::get('/occupation/edit/{id}', 'Member\OccupationController@edit');
    Route::get('/occupation/delete/{id}', 'Member\OccupationController@delete');
    Route::post('/occupation/save', 'Member\OccupationController@store');
    Route::post('/occupation/update', 'Member\OccupationController@update');
    Route::post('/save-occupation-phone', 'Member\OccupationController@save_phone');
    Route::get('/delete-occupation-phone', 'Member\OccupationController@delete_phone');
    Route::get('/occupation/change-language/{id}','Member\OccupationController@change_language');

    /*************************** Place Stay *************************************/

    Route::get('/place', 'Member\PlaceController@place_list');
    Route::get('/place/create', 'Member\PlaceController@create');
    Route::get('/place/edit/{id}', 'Member\PlaceController@edit');
    Route::get('/place/delete/{id}', 'Member\PlaceController@delete');
    Route::post('/place/save', 'Member\PlaceController@store');
    Route::post('/place/update', 'Member\PlaceController@update');
    Route::post('/save-place-phone', 'Member\PlaceController@save_phone');
    Route::get('/delete-place-phone', 'Member\PlaceController@delete_phone');
    Route::get('/place/change-language/{id}','Member\PlaceController@change_language');

    /*************************** Visited *************************************/
    Route::get('/visited/test', 'Member\VisitedController@visited_test');
    Route::get('/visited', 'Member\VisitedController@visited_list');
    Route::get('/visited/create', 'Member\VisitedController@create');
    Route::get('/visited/edit/{id}', 'Member\VisitedController@edit');
    Route::get('/visited/delete/{id}', 'Member\VisitedController@delete');
    Route::post('/visited/save', 'Member\VisitedController@store');
    Route::post('/visited/update', 'Member\VisitedController@update');
    Route::get('/visited/change-language/{id}','Member\VisitedController@change_language');

});

Route::group(['prefix' => '/package', 'middleware' => ['auth','editpackage']], function ($username) {

    /************************** Start Tour Information *************************/

    Route::get('ajax/invoice/amount', 'Package\OrderTourController@invoice_amount');
    Route::get('ajax/invoice/tax_address', 'Package\OrderTourController@tax_address');
    Route::post('form/payment/notification', 'Package\OrderTourController@payment_notification');

    Route::get('/add/tour/info/{id}', 'Package\OrderMemberController@member_list');
    Route::get('/member/list/{id}/{type}', 'Package\OrderMemberController@member_list_by_timeline');
    Route::get('/member/edit/{id}', 'Package\OrderMemberController@edit');
    Route::get('/member/create_start/{id}', 'Package\OrderMemberController@create');
    Route::get('/member/create/{id}', 'Package\OrderMemberController@create');
    Route::post('/member/save1', 'Package\OrderMemberController@save_step1');
    Route::post('/member/save_step2', 'Package\OrderMemberController@save_step2');
    Route::get('/member/step2', 'Package\OrderMemberController@create_step2');
    Route::get('/member/contact', 'Package\OrderMemberController@contact');
    Route::post('/member/address','Package\OrderMemberController@add_address');
    Route::get('/member/edit-address/{id}', 'Package\OrderMemberController@EditAddress');
    Route::get('/member/edit-step1', 'Package\OrderMemberController@EditStep1');
    Route::get('/member/edit-step2', 'Package\OrderMemberController@EditStep2');
    Route::get('/member/edit-step2/{id}', 'Package\OrderMemberController@EditStep2');
    Route::post('/update/step2', 'Package\OrderMemberController@update_step2');
    Route::post('/update/step1', 'Package\OrderMemberController@update_step1');

    Route::get('/member/step3', 'Package\OrderMemberController@create_step3');
    Route::post('/member/step3', 'Package\OrderMemberController@save_step3');
    Route::get('/member/edit-step3', 'Package\OrderMemberController@EditStep3');
    Route::get('/member/edit-step3/{id}', 'Package\OrderMemberController@EditStep3');
    Route::get('/member/edit-address/{id}', 'Package\OrderMemberController@EditAddress');
    Route::post('/edit-address', 'Package\OrderMemberController@update_address');
    Route::get('/member/delete-address', 'Package\OrderMemberController@delete_address');
    Route::get('/member/delete-email', 'Package\OrderMemberController@delete_email');

    Route::post('/member/save-email', 'Package\OrderMemberController@save_email');
    Route::post('/member/save-phone', 'Package\OrderMemberController@save_phone');

    Route::get('/member/delete-phone', 'Package\OrderMemberController@delete_phone');
    Route::get('/member/delete/{id}', 'Package\OrderMemberController@delete_member');

/******************** Family ****************************/

    Route::get('/member/step4', 'Package\FamilyController@family_list');
    Route::get('/member/step4/{id}', 'Package\FamilyController@family_list');
    Route::get('/member/family/create', 'Package\FamilyController@create');
    Route::post('/member/family/save', 'Package\FamilyController@save');
    Route::post('/member/family/update', 'Package\FamilyController@update');
    Route::get('/member/family/edit/{id}', 'Package\FamilyController@edit');
    Route::get('/member/family/delete/{id}', 'Package\FamilyController@delete');
    Route::get('/member/family/change-language/{id}','Package\FamilyController@change_language');

    /******************** Education **************************/

    Route::get('/education/list/{id}', 'Package\EducationController@education_list');
    Route::get('/education/list', 'Package\EducationController@education_list');
    Route::get('/education/create', 'Package\EducationController@create');
    Route::post('/education/save', 'Package\EducationController@store');
    Route::post('/education/update', 'Package\EducationController@update');
    Route::get('/education/edit/{id}', 'Package\EducationController@edit');
    Route::get('/education/delete/{id}', 'Package\EducationController@delete');
    Route::get('/education/change-language/{id}','Package\EducationController@change_language');


    /************************ Documents *********************************/
    Route::get('/document/list', 'Package\UploadController@document_list');
    Route::get('/document/list/{id}', 'Package\UploadController@document_list');
    Route::get('/document/create', 'Package\UploadController@create');
    Route::post('/upload/docs/remove', 'Package\UploadController@remove');
    Route::get('/document/delete/{id}', 'Package\UploadController@delete');
    Route::post('/upload/docs', 'Package\UploadController@memberDocs');
    Route::post('/upload', 'Package\UploadController@upload');

    /*************************** Referrence*************************************/

    Route::get('/reference/list', 'Package\ReferenceController@reference_list');
    Route::get('/reference/list/{id}', 'Package\ReferenceController@reference_list');
    Route::get('/reference/create', 'Package\ReferenceController@create');
    Route::get('/reference/edit/{id}', 'Package\ReferenceController@edit');
    Route::get('/reference/delete/{id}', 'Package\ReferenceController@delete');
    Route::post('/reference/save', 'Package\ReferenceController@store');
    Route::post('/reference/update', 'Package\ReferenceController@update');
    Route::post('/save-ref-phone', 'Package\ReferenceController@save_phone');
    Route::get('/delete-ref-phone', 'Package\ReferenceController@delete_phone');
    Route::get('/reference/change-language/{id}','Package\ReferenceController@change_language');

    /*************************** Occupation *************************************/

    Route::get('/occupation/list', 'Package\OccupationController@occupation_list');
    Route::get('/occupation/list/{id}', 'Package\OccupationController@occupation_list');
    Route::get('/occupation/create', 'Package\OccupationController@create');
    Route::get('/occupation/edit/{id}', 'Package\OccupationController@edit');
    Route::get('/occupation/delete/{id}', 'Package\OccupationController@delete');
    Route::post('/occupation/save', 'Package\OccupationController@store');
    Route::post('/occupation/update', 'Package\OccupationController@update');
    Route::post('/save-occupation-phone', 'Package\OccupationController@save_phone');
    Route::get('/delete-occupation-phone', 'Package\OccupationController@delete_phone');
    Route::get('/occupation/change-language/{id}','Package\OccupationController@change_language');

    /*************************** Place Stay *************************************/

    Route::get('/place/list', 'Package\PlaceController@place_list');
    Route::get('/place/list/{id}', 'Package\PlaceController@place_list');
    Route::get('/place/create', 'Package\PlaceController@create');
    Route::get('/place/edit/{id}', 'Package\PlaceController@edit');
    Route::get('/place/delete/{id}', 'Package\PlaceController@delete');
    Route::post('/place/save', 'Package\PlaceController@store');
    Route::post('/place/update', 'Package\PlaceController@update');
    Route::post('/save-place-phone', 'Package\PlaceController@save_phone');
    Route::get('/delete-place-phone', 'Package\PlaceController@delete_phone');
    Route::get('/place/change-language/{id}','Package\PlaceController@change_language');

    /*************************** Visited *************************************/
    Route::get('/visited/list', 'Package\VisitedController@visited_list');
    Route::get('/visited/list/{id}', 'Package\VisitedController@visited_list');
    Route::get('/visited/create', 'Package\VisitedController@create');
    Route::get('/visited/edit/{id}', 'Package\VisitedController@edit');
    Route::get('/visited/delete/{id}', 'Package\VisitedController@delete');
    Route::post('/visited/save', 'Package\VisitedController@store');
    Route::post('/visited/update', 'Package\VisitedController@update');
    Route::get('/visited/change-language/{id}','Package\VisitedController@change_language');

    /************************** End Tour Information *************************/

    Route::get('/approve/public', 'Package\PackageController@approve_public');
    Route::get('/manage','Package\PackageController@PackageList');
    Route::get('request/partner','Package\AgencyController@send_request');
    Route::get('highlight/image/credit/{id}','Package\PackageController@set_credit_image');
    Route::post('highlight/image/credit/','Package\PackageController@updateCredit');

    Route::get('/details/details/set_status','Package\PromotionController@set_status');
    Route::get('/detail/promotion','Package\PromotionController@promotion_list');
    Route::get('/detail/promotion/create','Package\PromotionController@create');
    Route::get('/promotion/edit/{id}','Package\PromotionController@edit');
    Route::get('/promotion/delete/{id}','Package\PromotionController@delete');
    Route::post('/detail/promotion/save','Package\PromotionController@save');
    Route::post('/detail/promotion/set','Package\PromotionController@setPromotion');
    Route::post('/detail/promotion/update','Package\PromotionController@update');

    /******************* Package tour Route *****************/
    Route::get('/list','Package\PackageController@PackageList');
    Route::get('/list_search','Package\PackageController@getPackageBySearch');
    Route::get('/clear/search','Package\PackageController@clearSearch');
    Route::get('/partner','Package\PackageController@partner_list');
    Route::get('/partner/packages/{id}','Package\PackageController@PackagePartnerList');
    Route::get('/create','Package\PackageController@create');
    Route::get('/edit/{id}','Package\PackageController@edit');
    Route::get('/delete/{id}','Package\PackageController@delete');
    Route::post('/update','Package\PackageController@update');

    Route::get('/details','Package\PackageController@details');
    Route::get('/partner/details/{id}','Package\PackageController@partner_details');


    Route::post('/store','Package\PackageController@store');
    Route::post('/upload/file','Package\PackageController@upload_package_file');

    Route::get('/info','Package\PackageController@info');
    Route::get('/settype', 'Package\PackageController@Settype');


    Route::post('/SaveInfo','Package\PackageController@saveInfo');
    Route::post('/SavePackageDetail','Package\PackageController@saveDetail');


//    Route::get('/popup/edit/{id}', 'Package\PackageController@popup_edit_details');
    Route::post('/update/details', 'Package\PackageController@updatePackageDetail');
    Route::post('/update/details1', 'Package\PackageController@updateDetail');

    Route::get('/program/highlight/{id}', 'Package\PackageController@highlight');
    Route::post('/program/highlight/search', 'Package\PackageController@search_location_highlight');
//    Route::get('/schedule', 'Package\PackageTourController@schedule');
//    Route::get('/edit/{id}', 'Package\PackageTourController@edit_Setting');
    /*************************  Schedule *************************************/
    Route::get('/schedule','Package\ScheduleController@schedule');
    Route::get('program/Images/sortable', 'Package\ScheduleController@ImageSort');
    Route::get('/program/sortby_days','Package\ScheduleController@sortDay');
    Route::post('schedule/save','Package\ScheduleController@store');
    Route::get('/program/copy/{id}', 'Package\ScheduleController@copyProgram');
    Route::get('/program/edit/{id}', 'Package\ScheduleController@editProgram');
    Route::get('/program/del/{id}', 'Package\ScheduleController@delProgram');
    Route::post('/program/update', 'Package\ScheduleController@updateProgram');
    Route::post('dropzone/uploadFiles', 'Upload\DropzoneController@uploadFiles');
    Route::post('dropzone/programImage', 'Upload\DropzoneController@programImage');
    Route::post('dropzone/highlightImage', 'Upload\DropzoneController@highlightImage');
    Route::get('/program/showImage', 'Package\ScheduleController@showImage');
    Route::get('/program/gettime', 'Package\ScheduleController@getTime');


    Route::get('/highlight/mycreate', 'Location\LocationController@highlight2');
    Route::get('/highlight/other', 'Location\LocationController@LocationList');
    Route::get('/highlight/items', 'Location\LocationController@ItemList');
    Route::get('/location/create', 'Location\LocationController@LocationCreate');
    Route::get('/item/create', 'Location\LocationController@ItemCreate');
    Route::get('/location/showImage', 'Location\LocationController@showImage');
    Route::get('Highlight/edit/{id}', 'Location\LocationController@edit');



    //*************************  Details *************************************/
    Route::get('/detail/edit/{id}', 'Package\PackageController@editPriceDetails');
    Route::get('/detail/create', 'Package\PackageController@createDetails');
    Route::get('/details/copy/{id}', 'Package\PackageController@copy_details');
    Route::get('ajax/get-airline', 'CommonController@getAirline');
    Route::get('ajax/detail/create', 'Package\PackageController@createDetails');
    Route::get('ajax/detail/edit/{id}', 'Package\PackageController@editDetails');
    Route::get('check/detail/create', 'Package\PackageController@createCheckDetails');
    Route::get('check/detail/edit/{id}', 'Package\PackageController@editCheckDetails');
    Route::get('ajax/details/set_status', 'Package\PackageController@statusDetails');
    Route::get('ajax/rate/set_status', 'Package\PackageController@statusRateDetails');
    Route::get('ajax/promotion/set_status', 'Package\PackageController@statusPromotionDetails');
    Route::get('ajax/additional/set_status', 'Package\PackageController@statusAdditionalDetails');
    Route::get('ajax/program/set_highlight', 'Package\PackageController@setProgramHighlight');
    Route::get('ajax/list/user_approve/{id}', 'Package\PackageController@listUserApprove');

    //************************ Price *******************************/
    Route::get('/details/price/{id}', 'Package\PackageController@priceDetails');
    Route::get('/details/create/price', 'Package\PackageController@create_priceDetails');
    Route::post('/details/create/price', 'Package\PackageController@save_priceDetail');
    Route::get('/details/price/edit/{id}', 'Package\PackageController@edit_priceDetail');
    Route::get('/details/price/delete/{id}', 'Package\PackageController@delete_priceDetail');
    Route::post('/details/price/update', 'Package\PackageController@update_priceDetail');

    //********************************** Promotion ************************************/

    Route::get('/details/promotion/create/{id}', 'Package\PromotionController@create1');
    Route::get('/details/promotion/delete/{id}', 'Package\PromotionController@delete');
    Route::get('/details/promotion/edit/{id}', 'Package\PromotionController@edit1');


    //************************************ Additional ******************************************/
    Route::get('/details/additional/delete/{id}', 'Package\PromotionController@delete_additional');
    Route::get('/details/additional/edit/{id}', 'Package\PromotionController@edit_additional');
    Route::get('/details/create/additional', 'Package\PromotionController@create_additional');
    Route::post('/details/create/additional', 'Package\PromotionController@save_additionalDetail');
    Route::post('/details/update/additional', 'Package\PromotionController@update_additionalDetail');




    Route::get('/set-price-package', 'Package\PackageController@showRateCust');
    Route::get('/set-price-package_back', 'Package\PackageController@showRateSale');
    Route::get('ajax/rate/create/{id}', 'Package\PackageController@createRate');
    Route::post('/rate/create', 'Package\PackageController@saveRate');

    Route::get('search-partner-package', 'Package\PackageController@getPackageByPartner');

    Route::get('booking/search-partner-package', 'Package\PackageController@getPackageByPartnerForBooking');
    // Route::post('search-partner-package', 'Package\PackageController@getPackageByPartner');

    Route::get('ajax/rate/{id}', 'Package\PackageController@showRate');
    Route::get('/ajax/rate/edit/{id}', 'Package\PackageController@editRates');
    Route::get('/ajax/rate_del', 'Package\PackageController@delRate');
    Route::get('/details/del/{id}', 'Package\PackageController@del_details');
    Route::get('/checking/{id}','Package\PackageTourController@checking');
    Route::get('/details/{id}', 'Package\PackageTourController@view_package');
    Route::get('/details_check/{id}', 'Package\PackageTourController@view_package_check');
    Route::get('/copy/{id}', 'Package\PackageTourController@copyPackage');
    Route::get('/users/{id}', 'Package\PackageTourController@package_member');

//    *************************** Condition **********************************

    Route::post('/ajax/condition/group', 'Package\ConditionController@setOperatorGroup');
    Route::get('/ajax/condition/deposit/sub', 'Package\ConditionController@setOperatorSub');
    Route::get('/condition/list', 'Package\ConditionController@ListCondition');
    Route::get('/condition/list/{id}', 'Package\ConditionController@ListConditions');
    Route::get('/condition/view/{id}', 'Package\ConditionController@ViewCondition');
    Route::get('/condition/more/{id}', 'Package\ConditionController@MoreCondition');
    Route::get('/condition/create/{id}', 'Package\ConditionController@createCondition_new');
    Route::get('/condition/create', 'Package\ConditionController@createCondition');
    Route::get('/condition/delete/{id}', 'Package\ConditionController@deleteCondition');
    Route::get('/condition/begin_delete/{id}', 'Package\ConditionController@BegindeleteCondition');
    Route::get('/condition/delete_from_list/{id}', 'Package\ConditionController@deleteFromList');
    Route::get('/condition/edit/{id}', 'Package\ConditionController@editCondition');
    Route::get('/condition/begin_edit/{id}', 'Package\ConditionController@BegineditCondition');
    Route::get('/condition/list_more/{id}', 'Package\ConditionController@ListMoreCondition');

    Route::get('/ajax/condition/status', 'Package\ConditionController@UpdateStatusCondition');
    Route::get('/ajax/condition/status/all', 'Package\ConditionController@UpdateStatusAllCondition');


    Route::get('/ajax/condition/price_visa', 'Package\PackageController@set_price_visa');
    Route::get('/ajax/condition/price_default', 'Package\PackageController@set_price_default');

    Route::post('/condition/update', 'Package\ConditionController@updateCondition');
    Route::post('/condition/set', 'Package\ConditionController@setCondition');

    Route::get('/condition/form/{id}', 'Package\ConditionController@FormCondition');

//    Route::get('/condition/edit/{id}', 'Package\ConditionController@FormCondition');
//    Route::post('/condition/update', 'Package\ConditionController@SaveFormCondition');

    Route::post('/condition/save', 'Package\ConditionController@saveCondition');
    Route::post('/condition/save_other', 'Package\ConditionController@saveConditionOther');
    Route::get('/condition/delOther/{id}', 'Package\ConditionController@DelConditionOther');
    Route::get('/condition/editOther/{id}', 'Package\ConditionController@EditConditionOther');
    Route::get('/condition/updateOther', 'Package\ConditionController@UpdateConditionOther');

    /*************************  Highlight *************************************/

    Route::get('ajax/forms/add', 'Package\HighlightOfDayController@FormAddHighlight');
    Route::get('ajax/forms/edit/{id}', 'Package\HighlightOfDayController@FormEditHighlight');
    Route::get('editHighlight/{id}', 'Package\HighlightOfDayController@edit');
    Route::post('updateHighlight', 'Package\HighlightOfDayController@update');
    Route::post('/saveHighlight', 'Package\HighlightOfDayController@store');
    Route::get('highlight/Images/sortable', 'Package\ScheduleController@ImageSort');
    Route::get('highlight/delImage', 'Package\HighlightOfDayController@deleteImage');

    Route::post('ajax/program/sub_category', 'CommonController@getSubcategory');
    Route::get('ajax/program/create', 'Package\ScheduleController@createProgram');
    Route::get('ajax/program/edit/{id}', 'Package\ScheduleController@editProgram');

});


Route::group(['middleware' => ['auth']], function () {
    Route::get('/policy/show/details', 'PolicyController@show_details');
    Route::get('/policy/list', 'PolicyController@show_list');
    Route::get('/policy/create', 'PolicyController@create');
    Route::get('/policy/edit/{id}', 'PolicyController@edit');
    Route::get('/policy/delete/{id}', 'PolicyController@delete');
    Route::get('/policy/language/{id}', 'PolicyController@language');
    Route::post('/policy/store', 'PolicyController@store');
    Route::post('/policy/update', 'PolicyController@update');



    //********** Wallet ******************/
    Route::get('/wallet/bank_account', 'WalletController@bank_account');
    Route::get('/wallet/bank/create', 'WalletController@bank_create');
    Route::get('/wallet/bank/edit/{id}', 'WalletController@bank_edit');
    Route::post('/wallet/bank/store', 'WalletController@bank_store');
    Route::post('/wallet/bank/update', 'WalletController@bank_update');

    Route::get('/wallet/seller_balance', 'WalletController@seller_balance');
    Route::get('/ajax/seller_balance/list', 'WalletController@seller_balance_list');

//    Route::get('/', 'TimelineController@showFeed');
    Route::get('/browse', 'TimelineController@showGlobalFeed');
    //********** Trip able ******************/
    Route::get('/tripable', 'TimelineController@locationFeed');
    Route::get('/tripable/{group}', 'TimelineController@locationGroup');

    Route::get('/add/location/highlight', 'TimelineController@locationHighlight');

});


//Route::group(['prefix' => 'ajax', 'middleware' => ['auth']], function () {
////    ****************** 2018-09-25 ********************************
//Route::post('get-state', 'CommonController@getState');
//Route::post('get-city', 'CommonController@getCity');
//Route::post('get-city-sub', 'CommonController@getCitySub');
//Route::post('get-zipcode', 'CommonController@getZipcode');
//
//Route::post('get-sub-category', 'CommonController@getSubCate');
//Route::post('get-sub2-category', 'CommonController@getSub2Cate');
//Route::get('get-country-partner', 'CommonController@getCountryPartner');
//Route::get('booking/get-country-partner', 'CommonController@getCountryPartner');
//
//
//});


Route::group(['prefix' => 'ajax', 'middleware' => ['auth']], function () {
    //************************* Star rating *********************************/
    Route::get('booking/sortbystatus', 'Package\OrderTourController@sort_by_status');
    Route::get('add/member/booking', 'Package\OrderMemberController@add_to_booking');
    Route::get('star_rating/data', 'TimelineController@star_rating');

    Route::get('data/item', 'TimelineController@CheckLocation');
    Route::get('location/find', 'TimelineController@FindLocation');
    Route::post('create-post', 'TimelineController@createPost');
    Route::post('create-review', 'TimelineController@createReview');
    Route::post('create-post-newlocation', 'TimelineController@createPostNewLocation');
    Route::post('create-post-newlocation-highlight', 'TimelineController@createPostNewLocationHighlight');
    Route::post('create-location', 'TimelineController@createNewLocation');

    Route::post('get-youtube-video', 'TimelineController@getYoutubeVideo');
    Route::post('like-post', 'TimelineController@likePost');
    Route::post('follow-post', 'TimelineController@follow');
    Route::post('notify-user', 'TimelineController@getNotifications');
    Route::post('post-comment', 'TimelineController@postComment');
    Route::post('public-album-likes', 'TimelineController@publicAlbumLike');
    Route::post('page-like', 'TimelineController@pageLike');
    Route::post('location-like', 'TimelineController@locationLike');

    Route::post('change-avatar', 'TimelineController@changeAvatar');
    Route::post('change-cover', 'TimelineController@changeCover');
    Route::post('comment-like', 'TimelineController@likeComment');
    Route::post('comment-delete', 'TimelineController@deleteComment');
    Route::post('post-delete', 'TimelineController@deletePost');
    Route::post('page-delete', 'TimelineController@deletePage');
    Route::post('location-delete', 'TimelineController@deleteLocation');
    Route::post('public-album-delete', 'TimelineController@deletePublicAlbum');
    Route::post('share-post', 'TimelineController@sharePost');
    Route::post('location-liked', 'TimelineController@locationLiked');
    Route::post('page-liked', 'TimelineController@pageLiked');
    Route::post('get-soundcloud-results', 'TimelineController@getSoundCloudResults');
    Route::post('join-group', 'TimelineController@joiningGroup');
    Route::post('join-close-group', 'TimelineController@joiningClosedGroup');
    Route::post('join-accept', 'TimelineController@acceptJoinRequest');
    Route::post('join-reject', 'TimelineController@rejectJoinRequest');
    Route::post('follow-accept', 'UserController@acceptFollowRequest');
    Route::post('follow-reject', 'UserController@rejectFollowRequest');
    Route::get('get-more-posts', 'TimelineController@getMorePosts');
    Route::get('get-more-review', 'TimelineController@getMoreReview');
    Route::get('get-more-feed', 'TimelineController@showFeed');
    Route::get('get-location-more-feed', 'TimelineController@locationFeed');
    Route::get('get-global-feed', 'TimelineController@showGlobalFeed');
    Route::post('add-memberGroup', 'UserController@addingMembersGroup');
    Route::post('get-users', 'UserController@getUsersJoin');
    Route::get('get-users-mentions', 'UserController@getUsersMentions');
    Route::post('groupmember-remove', 'TimelineController@removeGroupMember');
    Route::post('group-join', 'TimelineController@timelineGroups');
    Route::post('report-post', 'TimelineController@reportPost');
    Route::post('follow-user-confirm', 'TimelineController@userFollowRequest');
    Route::post('post-message/{id}', 'MessageController@update');
    Route::post('create-message', 'MessageController@store');
    Route::post('page-report', 'TimelineController@pageReport');
    Route::post('get-notifications', 'UserController@getNotifications');
    Route::post('get-unread-notifications', 'UserController@getUnreadNotifications');
    Route::post('get-messages', 'MessageController@getMessages');
    Route::post('get-message/{id}', 'MessageController@getMessage');
    Route::post('get-conversation/{id}', 'MessageController@show');
    Route::post('get-private-conversation/{userId}', 'MessageController@getPrivateConversation');
    Route::post('get-unread-message', 'UserController@getUnreadMessage');
    Route::post('get-unread-messages', 'MessageController@getUnreadMessages');
    Route::post('public-album-member-remove', 'TimelineController@removePublicAlbumMember');
    Route::post('location-member-remove', 'TimelineController@removeLocationMember');
    Route::post('pagemember-remove', 'TimelineController@removePageMember');
    Route::post('get-users-modal', 'UserController@getUsersModal');
    Route::post('edit-post', 'TimelineController@editPost');
    Route::get('load-emoji', 'TimelineController@loadEmoji');
    Route::post('update-post', 'TimelineController@updatePost');
    Route::post('/mark-all-notifications', 'NotificationController@markAllRead');

    Route::post('add-public-album-members', 'UserController@addingMembersPublicAlbum');
    Route::post('add-page-members', 'UserController@addingMembersPage');
    Route::post('add-location-members', 'UserController@addingMembersLocation');
    Route::post('get-members-join', 'UserController@getMembersJoin');
    Route::post('get-members-package', 'Package\PackageTourController@getMembers');
    Route::post('announce-delete', 'AdminController@removeAnnouncement');
    Route::post('category-delete', 'AdminController@removeCategory');
    Route::post('get-members-invite', 'UserController@getMembersInvite');
    Route::post('add-event-members', 'UserController@addingEventMembers');
    Route::post('join-event', 'TimelineController@joiningEvent');
    Route::post('event-delete', 'TimelineController@deleteEvent');
    Route::post('notification-delete', 'TimelineController@deleteNotification');
    Route::post('allnotifications-delete', 'TimelineController@deleteAllNotifications');
    Route::post('post-hide', 'TimelineController@hidePost');
    Route::post('group-delete', 'TimelineController@deleteGroup');
    Route::post('media-edit', 'TimelineController@albumPhotoEdit');
    Route::post('unjoinPage', 'TimelineController@unjoinPage');
    Route::post('save-timeline', 'TimelineController@saveTimeline');

//    ****************** 2018-09-25 ********************************
    Route::post('get-state', 'CommonController@getState');
    Route::post('get-city', 'CommonController@getCity');
    Route::post('get-city-sub', 'CommonController@getCitySub');
    Route::post('get-zipcode', 'CommonController@getZipcode');

    Route::post('get-sub-category', 'CommonController@getSubCate');
    Route::post('get-sub2-category', 'CommonController@getSub2Cate');
    Route::get('get-country-partner', 'CommonController@getCountryPartner');
    Route::get('booking/get-country-partner', 'CommonController@getCountryPartner');

});





//*********** For search *****************/
Route::get('/data/country', 'SearchController@search_country');
Route::get('/data/location', 'SearchController@search_location');
Route::get('search/location', 'SearchController@search_location_home');

//*********** End For search *****************/


use Intervention\Image\Facades\Image;

Route::get('user/gallery/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/gallery/small/'.$filename)->response();
});
Route::get('user/gallery/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/gallery/small/x-small/'.$filename)->response();
});

Route::get('user/gallery/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/gallery/mid/'.$filename)->response();
});

Route::get('user/gallery/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/gallery/'.$filename)->response();
});

Route::get('user/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/avatars/small/'.$filename)->response();
});
Route::get('user/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/avatars/'.$filename)->response();
});

Route::get('page/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/avatars/'.$filename)->response();
});

Route::get('page/avatar/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/avatars/mid/'.$filename)->response();
});

Route::get('page/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/avatars/small/'.$filename)->response();
});

Route::get('page/avatar/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/avatars/x-small/'.$filename)->response();
});


Route::get('page/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/covers/'.$filename)->response();
});

Route::get('public_album/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/public_albums/avatars/'.$filename)->response();
});

Route::get('public_album/avatar/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/public_albums/avatars/mid/'.$filename)->response();
});

Route::get('public_album/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/public_albums/avatars/small/'.$filename)->response();
});

Route::get('public_album/avatar/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/public_albums/avatars/x-small/'.$filename)->response();
});

Route::get('public_album/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/public_albums/covers/'.$filename)->response();
});

Route::get('location/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/locations/avatars/'.$filename)->response();
});

Route::get('location/avatar/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/locations/avatars/mid/'.$filename)->response();
});

Route::get('location/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/locations/avatars/small/'.$filename)->response();
});

Route::get('location/avatar/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/locations/avatars/x-small/'.$filename)->response();
});

Route::get('location/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/locations/covers/'.$filename)->response();
});

Route::get('group/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/avatars/'.$filename)->response();
});
Route::get('group/avatar/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/avatars/mid/'.$filename)->response();
});
Route::get('group/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/avatars/small/'.$filename)->response();
});

Route::get('group/avatar/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/avatars/x-small/'.$filename)->response();
});

Route::get('group/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/covers/'.$filename)->response();
});

Route::get('setting/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/settings/'.$filename)->response();
});

Route::get('event/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/events/covers/'.$filename)->response();
});

Route::get('event/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/events/avatars/'.$filename)->response();
});
Route::get('event/avatar/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/events/avatars/mid/'.$filename)->response();
});
Route::get('event/avatar/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/events/avatars/small/'.$filename)->response();
});
Route::get('event/avatar/x-small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/events/avatars/x-small/'.$filename)->response();
});

Route::get('/page/{pagename}', 'PageController@page');

Route::get('album/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/albums/'.$filename)->response();
});

Route::get('wallpaper/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/wallpapers/'.$filename)->response();
});

Route::get('package/tour/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/package-tour/'.$filename)->response();
});
Route::get('package/tour/mid/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/package-tour/mid/'.$filename)->response();
});

Route::get('package/tour/small/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/package-tour/small/'.$filename)->response();
});

Route::get('package/tour/xsmall/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/package-tour/x-small/'.$filename)->response();
});

Route::get('package/tour/docs/{file_name}', function($file_name = null)
{
    $path = storage_path().'/'.'uploads'.'/package-tour/docs/'.$file_name;
    if (file_exists($path)) {
        return Response::download($path);
    }
});

//Route::get('package/tour/docs/{filename}', function ($filename) {
//    return storage_path().'/uploads/package-tour/docs/'.$filename;
//});