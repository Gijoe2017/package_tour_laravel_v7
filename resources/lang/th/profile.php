<?php
return[
    //Code here
    'back_to_list'=>'กลับไปที่รายการ',
    'booking_is_complete'=>'ข้อมูลลูกทัวร์ในการจองนี้ครบแล้ว',
    'member_list_timeline'=>'รายการลูกทัวร์เก่า',
    'add_tourist'=>'+เพิ่มข้อมูลลูกทัวร์',
    'show_list_tourist'=>'แสดงข้อมูลลูกทัวร์มีในระบบ',
    'member_list'=>'รายการลูกทัวร์',
    'AcademicNameAndDetails' => 'ชื่องานวิชาการและรายละเอียด',
    'AcademicType' => 'ประเภทงานวิชาการ',
    'AcademicWork' => 'งานวิชาการ',
    'Add' => 'เพิ่ม',
    'AddressType' => 'ประเภทที่อยู่',
    'Zip_code' => 'รหัสไปรษณีย์',
    'Address' => 'ที่อยู่',
    'Additional' => 'ที่อยู่เพิ่มเติม',
    'AddYourSchool' => 'เพิ่มชื่อโรงเรียน/สถาบัน/มหาลัยของท่าน',
    'AlreadyMember' => 'เป็นสมาชิกแล้ว',
    'Award' => 'รางวัลเกียรติคุณ',
    'AwardNameandDetails' => 'ชื่อรางวัล และรายละเอียด',
    'AwardReceiveDate' => 'วันที่รับรางวัล',
    'Cancel' => 'ยกเลิก',
    'Choose' => 'เลือก',
    'ChooseYourMainLanguage' => 'เลือกภาษาหลักของคุณ',
    'City' => 'เมือง',
    'CitySub' => 'เมือง',
    'State' => 'จังหวัด/รัฐ',
    'Close' => 'ปิด',
    'ZipCode' => 'รหัสไปรษณีย์',
    'ConfirmPassword' => 'ยืนยันรหัสผ่าน',
    'Country' => 'ประเทศ',
    'CoverPhoto' => 'ภาพหน้าเพจ',
    'DateOfBirth' => 'วันเกิด',
    'Delete' => 'ลบ',
    'Edit' => 'แก้ไข',
    'Education' => 'การศึกษา :',
    'EducationBackground' => 'ประวัติการศึกษา :',
    'EducationLevel' => 'ระดับการศึกษา',
    'Email' => 'อีเมล์',
    'EndAcademicDate' => 'งานวิชาการสิ้นสุดวันที่',
    'EndExhibitionDate' => 'งานจบวันที่',
    'EndWorkDate' => 'วันที่สิ้นสุดการทำงาน',
    'ExhibitionName' => 'ชื่อนิทรรศการ',
    'ExhibitionShortDetail' => 'รายละเอียดงานโดยย่อ',
    'ExhibitionType' => 'ประเภทงาน',
    'ExhibitionUntillToday' => 'งานจัดต่อเนื่องและยังไม่กำหนดวันสิ้นสุดงาน',
    'Facebook' => 'เฟสบุค',
    'FirstName' => 'ชื่อ',
    'FullName' => 'ชื่อ นามสกุล',
    'GraduationYear' => 'ปีจบการศึกษา',
    'IAmUserType' => 'ฉันเป็น (คุณสามารถเลือกได้มากกว่าหนึ่ง)',
    'LastName' => 'สกุล',
    'Location' => 'สถานที่',
    'LostYourPassword' => 'ลืมรหัสผ่าน',
    'NoSchool/Institute/University' => 'ไม่มีชื่อโรงเรียน/สถาบัน/มหาลัย ในรายการ',
    'NotHereBefore' => 'ไม่เคยเข้าสู่ระบบ?',
    'Password' => 'รหัสผ่าน',
    'Phone' => 'เบอร์โทรศัพท์',
    'ProfilePhoto' => 'รูปโปรไฟล์',
    'ProfileURL' => 'โปรไฟล์ URL',
    'ProgramOfStudy' => 'สาขา/คณะ',
    'Register' => 'สมัครสมาชิก',
    'ResetPassword' => 'ตั้งค่ารหัสผ่าน',
    'Save' => 'บันทีก',
    'School/Institute/University' => 'โรงเรียน/สถาบัน/มหาลัย',
    'SelectPicture' => 'เลือกรูปภาพ',
    'SelectWorkType' => 'เลือกประเภทงาน',
    'SendPasswordResetLink' => 'ส่งลิงค์ตั้งค่ารหัสผ่าน',
    'SignIn' => 'เข้าสู่ระบบ',
    'SignUp' => 'สร้างบัญชีใหม่',
    'StartAcademicDate' => 'งานวิชาการเริ่มวันที่',
    'StartExhibitionDate' => 'งานเริ่มวันที่',
    'StartWorkDate' => 'วันที่เริ่มทำงาน',
    'Studying' => 'กำลังศึกษาอยู่',
    'TelephoneNumber' => 'หมายเลขโทรศัพท์',
    'ThisEmailIsAlreadyInSystem' => 'ขออภัยคะ อีเมล์นี้ได้ลงทะเบียนในระบบแล้ว กรุณาใส่อีเมล์อื่น',
    'UpdateProfile/Academic' => 'อัพเดทข้อมูล / งานวิชาการ',
    'UpdateProfile/Award' => 'อัพเดทข้อมูล / รางวัลเกียรติคุณ',
    'UpdateProfile/Contact' => 'อัพเดทข้อมูล / ติดต่อ',
    'UpdateProfile/Education' => 'อัพเดทข้อมูล / การศึกษา',
    'UpdateProfile/Exhibition' => 'อัพเดทข้อมูล / นิทรรศการ',
    'UpdateProfile/Works' => 'อัพเดทข้อมูล / การทำงาน',
    'Upload' => 'อัพโหลด',
    'Username' => 'ชื่อผู้ใช้',
    'Website' => 'เว็บไซต์',
    'WorkDetails' => 'รายละเอียดงานที่ทำ',
    'WorkExperience' => 'ประวัติการทำงาน :',
    'WorkType' => 'ประเภทงาน',
    'WorkUntillToday' => 'ทำงานจนถึงปัจจุบัน',
    'confirmDelete' =>'ยืนยันการลบรายการนี้',
    'SiteName'=>'ชื่อไซต์',
    'Backupemail'=>'อีเมล์สำรอง',
    'Category'=>'ประเภทสถานที่',
    'CategorySub1'=>'หมวดย่อยที่ 1',
    'CategorySub2'=>'หมวดย่อยที่ 2',
	'PhotoAlbumType' => 'หมวดอัลบั้ม',
    'action'=>'ดำเนินการ',
    'relation'=>'ความสัมพันธุ์',
    'add_new'=>'เพิ่ม',
    'mobile' => 'มือถือ',
    'phone' => 'โทรศัพท์',
    'phone_code' => 'รหัสประเทศ',
    'phone_name' => 'เบอร์โทรศัพท์',
    'phone_type' => 'ประเภทโทรศัพท์',
    'select'=>'เลือกใช้คนนี้',
    'selected'=>'เลือกใช้คนนี้แล้ว',

	'place_of_birth'=>'สถานที่เกิด',
	'passport_birthdate_coutry'=>'ประเทศที่เกิด',
	'title'=>'คํานําหน้าชื่อ',
//    'id_card'=>'ID Card',
    'name'=>'ชื่อ',
	'n_name'=>'ชื่อเล่น',
	'f_name'=>'ชื่อ',
	'l_name'=>'นามสกุล',
	'm_name'=>'ชื่อกลาง',
	'identification_id'=>'เลขประจำตัวประชาชน',
	'age'=>'อายุ',
    'gender'=>'เพศ',
    'status'=>'สถานะ',
    'citizenship'=>'สัญชาติ',
    'religion'=>'ศาสนา',
    'nationality'=>'สัญชาติโดยกำเนิด',
    'ethnicity'=>'เชื้อชาติ',
    'relation'=>'ความสัมพันธ์',
    'date_of_birth'=> 'วันเกิด',
    'place_of_birth'=>'สถานที่เกิด',
    'career'=>'Career',
    'income'=>'Income',
    'remark'=>'Remark',
    'naturalization'=>'การโอนสัญชาติ',
    'add_tourist_information'=>'+ เพิ่มข้อมูลการท่องเที่ยว',

    /****************** Occupation *************************/
    'occupation_information'=>'ข้อมูลอาชีพ',
    'occupation'=>'อาชีพ',
    'occupation_level'=>'ตำแหน่งงาน',
    'work_date_start'=>'วันเริ่มทำงาน',
    'work_date_end'=>'วันสิ้นสุดการทำงาน',
    'business_name'=>'ชื่อสถานที่ทำงาน',
    'occupation_list'=>'รายการอาชีพ',


    /****************** Passport *************************/
    'passport'=>'หนังสือเดินทาง',
	'passport_info'=>'ข้อมูลหนังสือเดินทาง',
	'passport_number'=>'หมายเลขหนังสือเดินทาง',
	'passport_coutry'=>'หนังสือเดินทางประเทศ',
	'passport_DOE'=>'วันที่หมดอายุหนังสือเดินทาง',
	'passport_DOI'=>'วันที่ออกหนังสือเดินทาง',
	'passport_picture'=>'ภาพหนังสือเดินทาง',
	'passport_birthdate_coutry'=>'เกิดประเทศ',
	'current_nationality'=>'สัญชาติปัจจุบัน',
	'previous_nationality'=>'สัญชาติที่ผ่านมา (ถ้ามี)',
	'general_information'=>'ข้อมูลทั่วไป',
	'save_and_continue' => 'บันทึกและทำขั้นตอนต่อไป',
	'update_and_continue' => 'อัปเดทและทำขั้นตอนต่อไป',
	'photo_request_for_visa'=>'ภาพถ่ายเพื่อยื่นขอทำวีซ่า',

    /****************** Education *************************/
    'education'=>'การศึกษา',
    'education_information'=>'ข้อมูลการศึกษา',
    'education_list'=>'รายการการศึกษา',
    'education_type'=>'ระดับการศึกษา',
    'education_program'=>'หลักสูตรที่จบ',
    'date_graduate'=>'วันจบการศึกษา',
    'graduated_from'=>'จบการศึกษาจาก',
    'university'=>'มหาวิทยาลัย',
    'update'=>'อัปเดท',
    'language'=>'ข้อมูลภาษา',
    'View'=>'เปิดดูไฟล์',
	'citizenship'=>'เชื่อชาติ',

    /****************** General *************************/
	'general'=> 'ทั่วไป',
	'general_information'=> 'ข้อมูลทั่วไป',
	'personal_informaltion'=> 'ข้อมูลส่วนบุคคล',
	'step'=> 'ขั้นตอน',
	'previous'=> 'ย้อนกลับ',
	'continue'=> 'ต่อไป',
	'information'=> 'ข้อมูล',
	'no_data'=> 'ไม่มีข้อมูล',

	/****************** Contact *************************/
	'contact'=> 'ติดต่อ',
	'contact_informaition'=> 'ข้อมูลติดต่อ',

	/****************** Family *************************/
    'family_informaition'=> 'ข้อมูลครอบครัว',
    'family'=> 'ครอบครัว',

    /****************** Documents *************************/
     'document_files'=> 'ไฟล์เอกสาร',
     'date_upload'=> 'วันที่อัพโหลด',
     'file_name'=> 'ชื่อไฟล์',
     'file_type'=> 'ประเภทไฟล์',
     'add_document_file'=> 'เพิ่มไฟล์เอกสาร',
     'upload'=> 'อัพโหลด',

    /****************** Reference *************************/
    'reference'=> 'อ้างอิง',
    'reference_information'=> 'ข้อมูลอ้างอิง',
    'reference_name'=> 'ชื่อผู้อ้างอิง',
    'reference_list'=> 'รายการอ้างอิง',

    /****************** Place Stay *************************/
    'place_stay'=> 'ที่พัก',
    'accommodation_information'=> 'ข้อมูลที่พัก',
    'accommodation_name'=> 'ชื่อสถานที่พัก',
    'accommodation_list'=> 'รายการสถานที่พัก',

    /****************** Visited Country *************************/
    'country_will_travel'=> 'ประเทศจะเดินทาง',
    'country_destination_visited'=> 'ประเทศที่กำลังจะไป หรือ ประเทศที่เคยไปมาแล้ว',
    'country_visited'=> 'ประเทศที่เคยไปมาแล้ว',
    'will_visit_country'=> 'ประเทศที่กำลังจะเดินทางไป',
    'now_in_country'=> 'กำลังอยู่ในประเทศ',
    'location_will_or_visited'=> 'ชื่อสถานที่ที่กำลังจะไป หรือ ชื่อสถานที่ที่เคยไปมาแล้ว',
    'travel_history'=> 'ประวัติการเดินทาง',
    'list_travel_history'=> 'รายการประวัติการเดินทาง',
    'visa_type'=> 'ประเภทวีซ่า',
    'visa_number'=> 'หมายเลขวีซ่า',
    'visa_country'=> 'วีซ่าประเทศ',
    'visa_doi'=> 'วันออกวีซ่า',
    'visa_doe'=> 'วันหมดอายุวีซ่า',
];