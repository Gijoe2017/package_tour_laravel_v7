<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    //==================================== Translations ====================================//
    'sign_in_join'              => 'เข้าสู่ระบบ | ร่วม',
    'have_account_sign_up'              => 'มีบัญชีไหม? | ลงชื่อ',
    'affiliate_code'            => 'Affiliate code',
    'already_have_an_account'   => 'Already have an account',
    'confirm_password'          => 'ยืนยันรหัสผ่าน',
    'dont_have_an_account_yet'  => 'Don\'t have an account yet?',
    'email_address'             => 'ที่อยู่อีเมล',
    'enter_email_or_username'   => 'Enter E-mail or username',
    'forgot_password'           => 'ลืมรหัสผ่าน',
    'get_started'               => 'get started',
    'login_failed'              => 'Invalid Login credentials.',
    'login_success'             => 'Login Success',
    'login_via_social_networks' => 'Login via social networks',
    'login_welcome_heading'     => 'Welcome back! Please login.',
    'reset_welcome_heading'     => 'Reset your password.',
    'member_login'                 => 'เข้าสู่ระบบสมาชิก',
    'my_account'                 => 'บัญชีของฉัน',
    'user_name'                 => 'ชื่อผู้ใช้',
    'name'                      => 'ชื่อ',
    'member'                    => 'สมาชิก',
    'password'                  => 'รหัสผ่าน',
    'register'                  => 'สมัครสมาชิก',
    'login'                     => 'เข้าสู่ระบบ',
    'registered_verify_email'   => 'You have successfully registered!<br>Please verify your email before logging in.',
    'reset_password'            => 'รีเซ็ตรหัสผ่าน',
    'reset_your_password'       => 'รีเซ็ตรหัสผ่านของคุณ',
    'select_gender'             => 'Select gender',
    'send_password_reset_link'  => 'Send Password Reset Link',
    'sign_in'                   => 'เข้าสู่ระบบ',
    'sign_out'                   => 'ออกจากระบบ',
    'signin_to_dashboard'       => 'Sign in to Dashboard',
    'signup_to_dashboard'       => 'Sign up to Dashboard',
    'verify_email'              => 'Please verify your email.',
    'welcome_to'                => 'Welcome To',
    'subject'                   => 'Subject',
    'submit'                    => 'Submit',
    'email_verify'              => 'Please verify your email before logging in',
    'email_not_found'              => 'ไม่พบอีเมล์นี้ในระบบ',
    'password_not_match'              => 'รหัสผ่านในการยืนยันไม่ตรงกัน',
];
