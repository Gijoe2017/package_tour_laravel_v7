<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'no_followers'              => 'ยังไม่มีผู้ติดตาม!',
    'no_following'              => 'ไม่ติดตามใคร!',
    'no_likes'                  => 'ยังไม่ได้รับชอบ!',
    'no_posts'                  => 'ไม่พบกระทู้ใด ๆ ...',
    'no_tags'                   => 'ยังไม่มีแท็ก!',
    'no_suggested_users'        => 'ไม่พบผู้ใช้ที่แนะนำ!',
    'no_suggested_groups'       => 'ไม่พบกลุ่มที่แนะนำ!',
    'no_suggested_pages'        => 'ไม่พบหน้าแนะนำ!',
    'search_people_placeholder' => 'ค้นหาคน',
    'search_placeholder'        => 'ค้นหาสถานที่ อีเวนท์ ข่าวสาร โปรไฟล์ เพจและกลุ่ม',
    'post-placeholder'          => 'เขียนอะไรบางอย่าง.....#แฮชแท็ก @กล่าวถึง',
    'view-previous-messages'    => 'ดูข้อความก่อนหน้า',
    'whats-going-on'            => 'เกิดอะไรขึ้น ',
    'click-earn-revenue'        => 'คลิกปุ่มเหล่านี้และสร้างรายได้เพิ่ม!',
    'pages-manage'              => 'หน้าที่คุณจัดการ',
    'location-manage'           => 'สถานที่คุณจัดการ',
    'groups-manage'             => 'กลุ่มที่คุณจัดการ',
    'no-groups'                 => 'คุณไม่มีกลุ่มที่จะจัดการ',
    'no-liked-pages'            => 'ยังไม่ชอบหน้าใด ๆ !',
    'no-joined-goups'           => 'ยังไม่ได้เข้าร่วมกลุ่มใด ๆ !',
    'no_requests'               => 'ยังไม่มีคำขอ!',
    'no_notifications'          => 'คุณไม่ได้รับการแจ้งเตือนใด ๆ',
    'no_messages'               => 'คุณไม่มีข้อความใด ๆ',
    'no_members'                => 'ยังไม่มีสมาชิก!',
    'no_users'                  => 'ยังไม่มีผู้ใช้เพิ่ม!',
    'no_pages'                  => 'ยังไม่มีการเพิ่มหน้า!',
    'no_locations'               => 'ยังไม่มีการเพิ่มสถานที่!',
    'no_groups'                 => 'ยังไม่มีกลุ่มเพิ่ม!',
    'no_reports'                => 'ไม่มีรายงานที่จะจัดการ!',
    'no_affliates'              => 'ยังไม่ได้มีส่วนร่วม!',
    'no_description'            => 'ไม่มีคำอธิบายเพิ่ม!',
    'comment_placeholder'       => 'เขียนความคิดเห็น ... กด Enter เพื่อโพสต์',
    'get_more_posts'            => 'รับโพสต์เพิ่มเติม',
    'create_page_placeholder'   => 'เขียนเกี่ยวกับหน้าเว็บของคุณ ...',
    'create_group_placeholder'  => 'เขียนเกี่ยวกับกลุ่มของคุณ ...',
    'about_page_heading1'       => 'สร้างเพจ',
    'about_page_content1'       => 'ให้แบรนด์ธุรกิจของคุณหรือทำให้เกิดเสียงใน socialite และเชื่อมต่อกับคนที่สำคัญกับคุณ. ตั้งค่าได้ฟรี. เพียงเลือกหมวดหมู่เพจเพื่อเริ่มต้นใช้งาน.',
    'about_page_heading2'       => 'เชื่อมต่อกัน',
    'about_page_content2'       => 'เชื่อมต่อกับผู้ติดตามของคุณโดยการโพสต์ในไทม์ไลน์ของหน้า',

    'about_group_heading1'        => 'สนทนา',
    'about_group_heading2'        => 'วางแผน',
    'about_group_heading3'        => 'เชื่อมต่อกัน',
    'about_group_content1'        => 'สมาชิกสามารถแสดงความคิดเห็นและชอบโพสต์ได้, ดังนั้นจึงเป็นเรื่องง่ายที่จะพูดคุยกับทุกคนที่คุณต้องการ.',
    'about_group_content2'        => 'ไม่ว่าจะเป็นการเดินทาง,งานปาร์ตี้ตรีy, หรือรายการข้อมูลคอนเสิร์ตของคุณ, กลุ่มทำให้ง่ายต่อการประสานงานกับเพื่อนที่อยู่ใกล้และไกล.',
    'about_group_content3'        => 'กลุ่มช่วยให้คุณติดต่อสื่อสารได้ในพื้นที่ที่สามารถค้นหาได้หนึ่งช่อง.',
    'radio_open_group'            => 'ทุกคนสามารถดูและเข้าร่วมกลุ่มได้.',
    'radio_closed_group'          => 'ทุกคนสามารถดูและขอเข้าร่วมกลุ่มได้. คำขอสามารถได้รับการยอมรับหรือปฏิเสธโดยผู้ดูแลระบบ.',
    'radio_secret_group'          => 'เฉพาะสมาชิกเท่านั้นที่สามารถเข้าถึงกลุ่มได้.',
    'learn_more_about_groups'     => 'เรียนรู้เพิ่มเติมเกี่ยวกับกลุ่ม',
    'about_user_placeholder'      => 'ป้อนคำอธิบายเกี่ยวกับตัวคุณ',
    'enter_old_password'          => 'ป้อนรหัสผ่านเก่า',
    'enter_new_password'          => 'ใส่รหัสผ่านใหม่',
    'confirm_deactivate_question' => 'คุณแน่ใจหรือไม่ว่าต้องการปิดใช้งานบัญชีของคุณ?',
    'yes_deactivate'              => 'ใช่ปิดการใช้งาน',
    'menu_message_general'        => 'คุณสามารถเปลี่ยนการตั้งค่าทั่วไปได้',
    'menu_message_privacy'        => 'เปลี่ยนการตั้งค่าความเป็นส่วนตัวที่นี่',
    'menu_message_notifications'  => 'จัดการการแจ้งเตือนทางอีเมลของคุณ',
    'menu_message_affiliates'     => 'รายชื่อ บริษัท ในเครือของคุณ',
    'menu_message_connections'    => 'คุณสามารถเชื่อมต่อกับบริการได้',
    'menu_message_deactivate'     => 'คุณสามารถปิดใช้งานบัญชีของคุณได้',
    'menu_message_admin_roles'    => 'จัดการบทบาทที่นี่',
    'menu_message_messages'       => 'รายการข้อความ',
    'menu_message_page_likes'     => 'รายชื่อผู้ใช้ที่ชอบ',
    'no_admin'                    => 'ยังไม่มีผู้ดูแลระบบ!',
    'who_are_you_with'            => 'คุณอยู่กับใคร?',
    'what_are_you_watching'       => 'คุณกำลังดูอะไร?',
    'what_are_you_listening_to'   => 'คุณกำลังฟังอะไรอยู่?',
    'where_are_you'               => 'คุณอยู่ที่ไหน?',
    'no_announcements'            => 'ยังไม่มีประกาศ!',
    'mark_all_read'               => 'ทำเครื่องหมายว่าอ่านแล้วทั้งหมด',
    'private_posts'               => 'นี่เป็นกิจกรรมส่วนตัวและคุณไม่ได้รับอนุญาตให้ดูข้อมูลใด ๆ เกี่ยวกับเหตุการณ์นี้',
    'no_guests'                   => 'ยังไม่มีผู้เยี่ยมชม!',
    'no_guest_events'             => 'ยังไม่มีกิจกรรมสำหรับแขก',

    /****** Album form ******/
    'create_a_new_album'       => 'สร้างอัลบั้มใหม่',
    'create_a_new_message'     => 'สร้างข้อความใหม่',
    'enter_album_name'         => 'ป้อนชื่ออัลบั้ม',
    'name_of_the_album'        => 'ชื่ออัลบั้ม',
    'description_about_album'  => 'คำอธิบายเกี่ยวกับอัลบั้ม',
    'location_of_the_album'    => 'ตำแหน่งที่ตั้งของอัลบั้ม',
    'click_below_to_upload'    => 'คลิกด้านล่างเพื่ออัปโหลด',
    'only_jpeg_upto_20mb_each' => 'เฉพาะภาพ JPEG สูงสุด 20 MB',
    'get_notification_text'    => 'คุณจะได้รับการแจ้งเตือนเรื่องชอบความคิดเห็นและหุ้น',
    'stop_notification_text'   => 'คุณจะไม่ได้รับแจ้ง',
    'edit_text'                => 'คุณสามารถแก้ไขโพสต์ได้',
    'delete_text'              => 'โพสต์นี้จะถูกลบออก',
    'report_text'              => 'โพสต์นี้จะได้รับการรายงาน',
    'manage_roles_text'        => 'จัดการบทบาทให้กับสมาชิกเพจของคุณ',
    'no_members_to_admin'      => 'ไม่มีสมาชิกเพิ่มเป็น Admin!',
    'name_placeholder'         => 'ใส่ชื่อของคุณ',
    'email_placeholder'        => 'ป้อน E-mail ของแท้',
    'subject_placeholder'      => 'บรรทัดหัวเรื่องหลัก',
    'message_placeholder'      => 'ใส่ข้อความของคุณ',

    'are_you_sure'                        => 'คุณแน่ใจไหม?',
    'role_assigned_success'               => 'มอบหมายบทบาทสำเร็จแล้ว',
    'role_assigned_failure'               => 'ปัญหาในการกำหนดบทบาท!',
    'request_accepted'                    => 'ยอมรับคำขอแล้ว',
    'request_rejected'                    => 'คำขอถูกปฏิเสธ',
    'page_created_success'                => 'สร้างหน้าเรียบร้อยแล้ว',
    'page_updated_success'                => 'อัปเดตหน้าสำเร็จแล้ว',
    'page_deleted_success'                => 'ลบหน้าเรียบร้อยแล้ว',
    'group_created_success'               => 'สร้างกลุ่มเรียบร้อยแล้ว',
    'group_updated_success'               => 'อัปเดตกลุ่มสำเร็จแล้ว',
    'group_deleted_success'               => 'ลบกลุ่มเรียบร้อยแล้ว',
    'settings_updated_success'            => 'อัปเดตสำเร็จแล้ว',
    'user_settings_updated_success'       => 'อัปเดตการตั้งค่าของผู้ใช้เรียบร้อยแล้ว',
    'user_updated_success'                => 'อัปเดตผู้ใช้เรียบร้อยแล้ว',
    'user_deleted_success'                => 'ผู้ใช้ลบเรียบร้อยแล้ว',
    'password_updated_success'            => 'อัปเดตรหัสผ่านสำเร็จแล้ว',
    'page_settings_updated_success'       => 'การตั้งค่าหน้าเว็บสำเร็จแล้ว',
    'group_settings_updated_success'      => 'การตั้งค่ากลุ่มสำเร็จแล้ว',
    'announcement_updated_success'        => 'อัปเดตประกาศเรียบร้อยแล้ว',
    'announcement_deleted_success'        => 'ยกเลิกการประกาศเรียบร้อยแล้ว',
    'announcement_activated_success'      => 'เปิดใช้งานการประกาศแล้ว',
    'new_announcement_added'              => 'เพิ่มประกาศใหม่แล้ว',
    'general_settings_updated_success'    => 'การตั้งค่าทั่วไปสำเร็จแล้ว',
    'privacy_settings_updated_success'    => 'อัปเดตการตั้งค่าความเป็นส่วนตัวเรียบร้อยแล้ว',
    'new_password_updated_success'        => 'รหัสผ่านใหม่ของคุณได้รับการอัปเดตเรียบร้อยแล้ว',
    'password_no_match'                   => 'รหัสผ่านใหม่ไม่ตรงกับรหัสผ่านปัจจุบัน',
    'old_password_no_match'               => 'รหัสผ่านเดิมไม่ตรงกับ',
    'email_notifications_updated_success' => 'อัปเดตการแจ้งเตือนทางอีเมลสำเร็จแล้ว',
    'timeline_saved_success'              => 'บันทึกเส้นเวลาสำเร็จแล้ว',
    'timeline_deleted_success'            => 'ไทม์ไลน์ถูกลบเรียบร้อยแล้ว',
    'report_mark_safe'                    => 'รายงานที่ทำเครื่องหมายว่าปลอดภัย',
    'report_deleted_success'              => 'รายงานถูกลบแล้ว',
    'ads_updated_success'                 => 'การอัปเดตโฆษณาสำเร็จแล้ว',
    'page_active'                         => 'ต้องการทำให้หน้านี้ใช้งานได้ดีหรือไม่?',
    'new_category_added'                  => 'เพิ่มหมวดหมู่ใหม่แล้ว',
    'category_updated_success'            => 'อัปเดตหมวดหมู่เรียบร้อยแล้ว',
    'category_deleted_success'            => 'หมวดหมู่ถูกลบแล้ว',
    'events-manage'                       => 'กิจกรรมที่คุณจัดการ',
    'no_events'                           => 'ยังไม่มีกิจกรรมเพิ่ม!',
    'event_settings_updated_success'      => 'อัปเดตการตั้งค่ากิจกรรมเรียบร้อยแล้ว',
    'event_updated_success'               => 'อัปเดตกิจกรรมสำเร็จแล้ว',
    'event_deleted_success'               => 'ลบกิจกรรมเรียบร้อยแล้ว', 
    'notification_deleted_success'        => 'การแจ้งเตือนถูกลบแล้ว',        
    'notifications_deleted_success'       => 'การแจ้งเตือนถูกลบแล้ว',
    'create_album_placeholder'            => 'เขียนอะไรเกี่ยวกับอัลบั้มของคุณ',
    'create_album_success'                => 'สร้างอัลบั้มเรียบร้อยแล้ว',
    'create_album_error'                  => 'การสร้างอัลบั้มล้มเหลว',
    'no_photo_error'                      => 'เพิ่มรูปภาพหนึ่งรูปไปที่อัลบั้มหนึ่ง ๆ',
    'update_preview_success'              => 'อัปเดตรูปภาพสำเร็จแล้ว',
    'update_preview_error'                => 'การอัปเดตภาพตัวอย่างไม่สำเร็จ',
    'delete_media_success'                => 'รูปภาพถูกลบเรียบร้อยแล้ว',
    'delete_media_error'                  => 'เกิดข้อผิดพลาดในการลบภาพ',
    'update_album_success'                => 'อัปเดตอัลบั้มสำเร็จแล้ว',
    'update_album_error'                  => 'เกิดข้อผิดพลาดในการอัปเดตอัลบั้ม',
    'hide_notification_text'              => 'ซ่อนโพสต์ในไทม์ไลน์และหน้าแรกของคุณ', 
    'update_avatar_success'               => 'คุณอัปเดตภาพประจำตัวเรียบร้อยแล้ว',
    'update_avatar_failed'                => 'การอัปเดตรูปประจำตัวของคุณล้มเหลว',
    'update_cover_success'                => 'คุณได้ปรับปรุงหน้าปกของคุณเรียบร้อยแล้ว',
    'update_cover_failed'                 => 'การอัปเดตปกของคุณล้มเหลว',
    'create_page_success'                 => 'สร้างหน้าเรียบร้อยแล้ว',
    'update_Settings_success'             => 'การตั้งค่าทั่วไปสำเร็จแล้ว.',
    'update_privacy_success'              => 'อัปเดตการตั้งค่าความเป็นส่วนตัวเรียบร้อยแล้ว.',
    'assign_role_success'                 => 'มอบหมายบทบาทสำเร็จแล้ว',
    'remove_member_group_success'         => 'สมาชิกนำออกจากกลุ่มเรียบร้อยแล้ว',
    'assign_admin_role_remove'            => 'กำหนดบทบาทผู้ดูแลระบบสำหรับสมาชิกและนำออก',
    'remove_member_page_success'          => 'สมาชิกนำออกจากหน้าเรียบร้อยแล้ว',
    'join_request_accept'                 => 'เข้าร่วมคำขอเรียบร้อยแล้วที่ยอมรับ',
    'join_request_reject'                 => 'เข้าร่วมคำขอถูกปฏิเสธเรียบร้อยแล้ว',  
    'update_group_settings'               => 'การตั้งค่ากลุ่มสำเร็จแล้ว.',
    'create_event_success'                => 'สร้างกิจกรรมสำเร็จแล้ว.',
    'invalid_date_selection'              => 'การเลือกวันที่ไม่ถูกต้อง',
    'update_event_Settings'               => 'อัปเดตการตั้งค่ากิจกรรมเรียบร้อยแล้ว',
    'create_a_new_album'                  => 'สร้างอัลบั้มใหม่',
    'create_a_new_message'                => 'สร้างข้อความใหม่',
    'enter_album_name'                    => 'ป้อนชื่ออัลบั้ม',
    'name_of_the_album'                   => 'ชื่ออัลบั้ม',
    'description_about_album'             => 'คำอธิบายเกี่ยวกับอัลบั้ม',
    'location_of_the_album'               => 'ตำแหน่งที่ตั้งของอัลบั้ม',
    'click_below_to_upload'               => 'คลิกด้านล่างเพื่ออัปโหลด',
    'only_jpeg_upto_20mb_each'            => 'เฉพาะภาพ JPEG สูงสุด 20 MB',
    'create_album_placeholder'            => 'เขียนอะไรเกี่ยวกับอัลบั้มของคุณ',
    'create_album_success'                => 'สร้างอัลบั้มเรียบร้อยแล้ว',
    'create_album_error'                  => 'การสร้างอัลบั้มล้มเหลว',
    'no_photo_error'                      => 'เพิ่มรูปภาพลงในอัลบั้มอย่างน้อยหนึ่งภาพ',
    'update_preview_success'              => 'อัปเดตรูปภาพสำเร็จแล้ว',
    'update_preview_error'                => 'การอัปเดตภาพตัวอย่างไม่สำเร็จ',
    'delete_media_success'                => 'รูปภาพถูกลบเรียบร้อยแล้ว',
    'delete_media_error'                  => 'เกิดข้อผิดพลาดในการลบภาพ',
    'update_album_success'                => 'อัปเดตอัลบั้มสำเร็จแล้ว',
    'update_album_error'                  => 'เกิดข้อผิดพลาดในการอัปเดตอัลบั้ม',
    'delete_album_success'                => 'ลบอัลบั้มสำเร็จแล้ว',        
    'delete_album_error'                  => 'เกิดปัญหาในการลบอัลบั้ม',
    'no_albums'                           => 'ยังไม่ได้เพิ่มอัลบั้ม!',
    'no_photos'                           => 'ยังไม่มีการเพิ่มภาพถ่าย!',
    'no_videos'                           => 'ยังไม่ได้เพิ่มวิดีโอ!',
    'enter_youtube_url'                   => 'ป้อน URL ของ Youtube',
    'preview_image_delete_error'          => 'ไม่สามารถลบภาพตัวอย่างได้!',
    'media_only_image'                    => 'สื่อที่เลือกควรเป็นภาพ',
    'album_validation_error'              => 'ชื่ออัลบั้มความเป็นส่วนตัวและภาพอย่างน้อยหนึ่งภาพมีผลบังคับใช้',
    'invalid_link'                        => 'แก้ไขลิงก์ที่ไม่ถูกต้อง (:link)',
    'no_pending_updates'                  => 'ไม่มีการอัปเดตที่รอดำเนินการ',
    'menu_message_wallpaper'              => 'ตั้งวอลล์เปเปอร์ของคุณ',
    'activate_wallpaper_success'          => 'เปิดใช้วอลเปเปอร์ใหม่เรียบร้อยแล้ว',
    'deactivate_wallpaper_success'        => 'ปิดใช้งานวอลเปเปอร์เรียบร้อยแล้ว',
    'wallpaper_added_activated'           => 'เพิ่มและเปิดใช้งานวอลเปเปอร์ของคุณแล้ว',
    'no_file_added'                       => 'ไม่มีไฟล์แนบ!',
    'create_wallpapers_success'           => 'วอลเปเปอร์อัปโหลดเรียบร้อยแล้ว',
    'wallpaper_delete_success'            => 'ลบวอลเปเปอร์เรียบร้อยแล้ว',
    'no_wallpapers'                       => 'ยังไม่มีการเพิ่มวอลเปเปอร์!',
    'noty_category_delete_error'          => 'หมวดหมู่ไม่สามารถลบออกเนื่องจากใช้งานเพจได้',
    'noty_category_not_found'             => 'ไม่พบหมวด',
    'noty_category_delete_error_subcategory' => 'หมวดหมู่ไม่สามารถลบออกได้เนื่องจากมีประเภทย่อยอยู่',
    'no_saved_pages'                      => 'คุณยังไม่ได้บันทึกหน้าใด ๆ !',
    'no_saved_groups'                     => 'คุณยังไม่ได้บันทึกกลุ่มใด ๆ !',
    'no_saved_events'                     => 'คุณยังไม่ได้บันทึกเหตุการณ์ใด ๆ !',
    'post_save_text'                      => 'โพสต์นี้จะได้รับการบันทึก',
    'no_saved_posts'                      => 'คุณยังไม่ได้บันทึกข้อความใด ๆ !',
    'post_unsave_text'                    => 'โพสต์จะถูกนำออกจากรายการที่บันทึกไว้',
    'homepage_settings_success'           => 'การอัปเดตหน้าแรกสำเร็จแล้ว',
    
    /*
    *
    *
    * Authentication translations
    */
    'captcha_required'                   => 'ต้องกรอกข้อมูลในฟิลด์ captcha',
    'verify_mail'                        => 'โปรดยืนยันอีเมลของคุณ',
    'verified_mail'                      => 'คุณได้ยืนยันอีเมลของคุณแล้ว',
    'verified_mail_success'              => 'คุณยืนยันอีเมลเรียบร้อยแล้ว กรุณาเข้าสู่ระบบทันที',
    'invalid_verification'               => 'รหัสยืนยันหรือคำขอไม่ถูกต้อง',
    'change_username_facebook'           => 'Facebook ไม่ได้ระบุชื่อผู้ใช้,โปรดเปลี่ยนชื่อผู้ใช้ชั่วคราวของคุณ',
    'user_login_failed'                  => 'ปัญหาการตรวจสอบผู้ใช้',
    'change_username_google'             => 'Google ไม่ได้ระบุชื่อผู้ใช้, โปรดเปลี่ยนชื่อผู้ใช้ชั่วคราวของคุณ',
    'change_username_twitter'            => 'Twitter ไม่ได้ให้อีเมล,โปรดเปลี่ยนอีเมลชั่วคราวของคุณ',
    'change_username_linkedin'           => 'linkedin ไม่มีชื่อผู้ใช้,โปรดเปลี่ยนชื่อผู้ใช้ชั่วคราวของคุณ',


    /*
     *
     * Shared translations.
     *
     */
    'title'   => 'ติดตั้ง Laravel',
    'next'    => 'ขั้นตอนต่อไป',
    'install' => 'ติดตั้ง',


    /*
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'ยินดีต้อนรับสู่ตัวติดตั้ง',
        'message' => 'ยินดีต้อนรับสู่วิซาร์ดการตั้งค่า.',
    ],


    /*
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'ความต้องการ',
    ],


    /*
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'สิทธิ์',
    ],


    /*
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title'   => 'การตั้งค่าสภาพแวดล้อม',
        'save'    => 'บันทึก. env',
        'success' => 'การตั้งค่าไฟล์. env ของคุณได้รับการบันทึกแล้ว ',
        'errors'  => 'ไม่สามารถบันทึกไฟล์. env ได้โปรดสร้างด้วยตนเอง',
    ],


    /*
     *
     * Final page translations.
     *
     */
    'final' => [
        'title'    => 'เสร็จ',
        'finished' => 'ติดตั้งแอ็พพลิเคชันเรียบร้อยแล้ว',
        'exit'     => 'คลิกที่นี่เพื่อออก',
    ], 


];