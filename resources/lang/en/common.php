<?php



return [



    /*

    |--------------------------------------------------------------------------

    | Common Language Lines

    |--------------------------------------------------------------------------

    |

    | The following language lines are used for various

    | messages that we need to display to the user. You are free to modify

    | these language lines according to your application's requirements.

    |

    */
//  *************  Package Details **********************
    'wait_for_the_tour_operator_to_find_a_guest_to_share'=>'Wait for the tour operator to find a guest to share so they don\'t have to pay additional service fees.',
    'let_the_system_notify_the_tour_guide_to_wait_for_a_match'=>'Let the system notify the tour guide to wait for a match.',
'if_able_to_find_a_partner_tour'=>'If able to find a partner Tour operators are not required to pay this additional service. But if unable to find it, the tourists need to buy this additional service',
    'if_wanting_to_take_tours_from_other_companies'=>'(If wanting to take tours from other companies to get permission from those companies first by requesting a systematic request)',
    'needing_additional_options_for_buyers_of_this_type'  =>'Needing additional options for buyers of this type of tour To force to choose the case of traveling alone.',
'message_waiting_confirm'  =>'Please wait for the availability check from the tour vendor. If checking that there is space, the system will send an email informing you to pay again. Sorry for the inconvenience.',
    'awaiting_confirm_seats'  =>'Wait to check the available seats',
    'amount_seat_available'    =>  'Number of seats available',
    'confirm_seat_avalable'    =>  'Confirm seat avalable',
    'back_to_payment_list'  =>'Back to Payment list',
    'number_of_days'     =>'Number of days',
    'no_seat_avalable'     =>'No seats available',
    'back_to_mybooking'     =>'Back to mybooking',
    'pay_via_credit_card'           =>'Pay via credit card',
    'buy_more_packages'             =>'Buy more packages',
    'check_seats_available'         =>'Check seats available',
    'bank_details'                  =>'Bank_details',
    'update_status'                 =>'Update status',
    'social_network'                =>'Social network',
    'manage_module'                 => 'Manage Module',
    'my_account'                    => 'My account',
    'wallet'                        => 'Wallet',
    'my_income'                     => 'My income',
    'seller_balance'                => 'Seller balance',
    'bank_account'                  => 'Bank account',
    'bank_default'                  => 'Bank default',
    'account_id_card'               => 'Identification',
    'cancel_booking'                => 'Cancel booking',
    'message_cancel_number_tourist_fully'                => 'Canceled due to the number of tour tourists already filling in amount',
    'submenu_package'                => 'Sub menu package',
    'print_a_tour_receipt'          =>'Print a tour receipt',
    'thank_you_for_paying_for_the_tour'          =>'Thank you for paying for the tour',
    'cancel_individually'           => 'Cancel individually',
    'cancel_some_package'           => 'Cancel some package',
    'canceled'                      => 'Canceled',
    'cancel_notification_in'              => 'Cancel notification in',
    'cancel_by_system'              => 'Cancel by system',
    'canceled_by_myself'              => 'Canceled by myself',
    'notice_of_cancellation_from'              => 'Notice of cancellation from',
    'no_condition_cancel'=>'No cancellation conditions were found in the system. Please check the seller again.',
    'this_email_is_to_inform_you_that_you_have_canceled_the_tour_package'=>'This email is to inform you that you have canceled the tour package',
    'this_email_is_to_inform_you_that_your_tour_package_has_been_canceled'=>'This email is to inform you that your tour package has been canceled.',
    'choose_the_package_that_you_want_to_cancel'              => 'Choose the package that you want to cancel',
    'choose_the_number_of_people_you_want_to_cancel'              => 'Choose the number of people you want to cancel',
    'choose_package_category'              => 'Choose Package Category',
    'choose_package_type'              => 'Choose Package Type',
    'package_tour_banner'              => 'Package Tour Banner',

    'this_package_departs_on'              => 'This package departs on',
    'need_to_insert_airline'               => 'Need to insert Airline',
    'need_to_insert_flight'               => 'Need to insert Flight',
    'travel_by_plane'                     => 'Travel by plane',
    'do_not_travel_by_plane'              => 'Do not travel by plane',
    'days_off_the_sale_before'              => 'Specify the number of days off the sale before the date of departure',
    'confirm_cancel_booking'           => 'Confirm cancel this booking',
    'cancel_successful'                => 'Cancel successful',
    'closing_date'                            => 'Closing date',
    'confirm_cancel_booking_package'           => 'Confirm cancellation of this package reservation.',
    'message_cancel_all'           => 'When clicking the cancel booking button The system will cancel all items in the list from now. Confirm to cancel. Click the cancel button immediately.',
    'all'                           =>'All',
    'donot_want'                   =>'Don\'t want',
    'this_additional_service_is_tied_to'                   =>'This additional service is tied to',
    'automatic'                   =>'Automatic',
    'number_of_people_who_need_a_visa'                         =>'Number of people who need a visa',
    'can_share'                     =>'I am happy to share it with other people that the seller provides.',
    'member_login'                  =>'Member Login',
    'my_bookings'                   =>'My bookings',
    'this_price_include_vat'=>'This price include VAT.',
    'this_price_not_include_vat'=>'This price does not include VAT.',

    'payment_methods'=>'Payment Methods',
    'payment_details'=>'Payment Details',
    'accept_payment'=>'Accept payment',
    'payer_details'=>'Payer details',
    'remark'=>'Remark',
    'service_fee_upon_receipt'=>'Service fee upon receipt',
    'paid'=>'Paid',
    'difference'=>'Difference',
    'member_list'=>'Member List.',
    'manage_booking'=>'Manage',
    'normal_price'=>'Normal price',
    'no_invoice'=>'No invoice line item found for this reservation',
    'balance'=>'ฺBalance',
    'sale'=>'Sale',
    'start_price'=>'Starting price',
    'price_system_default_N'=>'No',
    'price_system_default_Y'=>'Yes',
    'price_include_visa_N'=>'No',
    'price_include_visa_Y'=>'Yes',
    'your_name'=>'Your name',
    'your_email'=>'Your email',
    'your_message'=>'Your message',
    'your_message_here'=>'Please enter your message here...',
    'send_message_to_supplier'=>'Send message to supplier',
    'cart'=>'Cart',
    'pay_deposit'=>'Deposit paid',
    'deposit_paid'=>'Deposit paid',
    'balance_paid'=>'Balance Paid',
    'pay_all'=>'Pay All',
    'pay_balance'=>'Pay Balance',
    'show_invoice'=>'Invoice',
    'invoice_no'=>'Invoice No',
    'transfer'=>'Transfer',
    'account_name'=>'Account name',
    'account_number'=>'Account No.',
    'branch'=>'Branch',
    'change_address'=>'Change address',
    'check_invoice'=>'Check Invoice',
    'confirm_order'=>'Confirm Order',
    'confirm_payment'=>'Confirm payment',
    'modify'=>'Modify',
    'booking_detail'=>'Booking Details',
    'booking_id'=>'Booking No.',
    'booking_date'=>'Booking Date',
    'booking_now'=>'Booking Now',
    'pending_payment'=>'Pending payment',
    'notify_deposit_payment'=>'Notify deposit payment',
    'notify_tour_payment'=>'Notify tour payment',
    'inform_payment_of_the_rest_of_the_tour'=>'Inform payment of the rest of the tour',
    'payment_notification'=>'Payment notification',
    'paid'=>'Paid',
    'transfer_amount'=>'Transfer amount',
    'please_pay_the_deposit_before'=>'Please pay the deposit before',
    'please_pay_the_remaining_amount_before'=>'Please pay the remaining amount before',
    'please_pay_for_the_tour_before_the_date'=>'Please pay for the tour before the date',
    'please_select_the_transfer_bank'=>'Please select the transfer bank.',
    'awaiting_confirmation'=>'Awaiting confirmation',
    'waiting_for_payment'=>'Waiting for payment',
    'pay_deposit'=>'Pay deposit',
    'manage_tourist'=>'Manage tourist',
    'print_invoice'=>'Print invoice',
    'not_check'=>'Packages that I have not checked',
    'my_checking'=>'Package I have checked',
    'refund'=>'Refund',
    'Service_fee_has_not_been_paid_yet'=>'Service fee has not been paid yet',

    'wait_pay_deposit'=>'Waiting for deposit payment',
    'wait_pay_all'=>'Waiting for all tour fees',
    'wait_pay_balance'=>'Wait to pay for the rest of the tour',

    'receipt'=>'Receipt',
    'invoice_balance'=>'Invoice balance',
    'total_deposit'=>'Total Deposit',
    'total_balance'=>'Total Balance',
    'invoice_deposit'=>'Invoice deposit',
    'invoice_tour'=>'Invoice tour',
    'invoice_tour_all'=>'Invoice tour all',

    'invoice_balance_notification'=>'Notify balance payment',
    'invoice_deposit_notification'=>'Notify deposit payment',
    'invoice_tour_notification'=>'Notify tour payment',
    'invoice_tour_notification_already'=>'Payment notification',
    'system_fee'=>'System fee',
    'add_to_wishlist'=>'Add to wishlist',

'user_name'=>'User name',
'billing_date'=>'Billing Date/Time ',
'deposit_payment'=>'Deposit payment :',
'deposit_invoice'=>'Deposit Invoice :',


'receipt_deposit'=>'Deposit receipt',
'receipt_balance'=>'Receipt of the remaining tour fee',
'receipt_tour'=>'Tour receipt',

'remaining_balance'=>'The remaining balance :',
'reference'          => 'Reference',
'continuous'=>'Continuous',
'choose_payment'=>'Choose payment',
'confirm_edit_condition'=>'This condition has already been applied to below package(s). If you edit this condition, the below package(s) will also be modified. Confirm to edit press "Confirm edit" or create a new condition press "Create new condition"',
'confirm_delete_condition'=>'This condition has already been applied to package(s) below. If you click "Delete", this condition will also be deleted from the package(s) below. Confirm to delete press "Confirm delete" Do not want to delete press "Return main item"',
'warning_before_edit'=>'Warning before edit',
'warning_before_delete'=>'Warning before delete',
'warning'=>'Warning',
'terms_and_conditions'=>'Terms & Conditions',
'package_condition_details'=>'Package Condition Detail',
'condition_auto_formula'=>'This condition, the automatic calculation system',
    'confirm_delete'=>'Confirm delete',
    'confirm_edit'=>'Confirm edit',
    'create_new_condition'=>'Create new condition',
    'enter_the_formula'=>'Set the system to automatically calculate',
    'source_code_from_the_tour_company'=>'Source code from the tour company',
    'season'=>'Season',
    'season_name'=>'Season_name',
    'condition_pay_on_season'=>'During the festival, please pay before departure x days',
    'top'=>'Top',
    'inspector'=>'Inspector',
    'schedule'=>'Schedule',
    'add_schedule'=>'Add Schedule',
    'food_for_today'=>'Food for to day:',
    'breakfast'=>'Breakfast',
    'lunch'=>'Lunch',
    'dinner'=>'Dinner',
    'highlight'=>'Highlight',
    'action'=>'Action',
    'details'=>'Details',
    'detail_booking'=>'Booking detail',
    'create_new_package'=>'Create new package',
    'package_partner'=>'Package Partner',
    'package_list'=>'Package List',
    'package_manager'=>'Package manage',
    'add_package_details'=>'Add package details',
    'extra_service_rate'=>'Extra service rate',
    'tour_buyer_type'=>'Tour buyer type',
    'tour_fee_rate'=>'Tour fee rate',
    'tour_information'=>'Tour Info.',
    'highlight_in_schedule'=>'Highlight in schedule',
    'schedule_travel_price'     => 'Schedule travel / price',
    'set_the_departure_date_and_end_date_of_travel'          =>'(Set the departure date and end date of travel)',
    'details'=>'Details',
    'package_edit'=>'Edit Package',
    'information'=>'Information',
    'passport'=>'Passport',
    'recently_opened_tour_packages'=>'Recently opened tour packages',
    'list_of_tour_packages'=>'List of tour package',
    'list_of_booking'=>'List of booking',
    'list_of_tourist'=>'List of tourist',
    'list_payment_notification'=>'List of Payment notification',
    'search_by_name'=>'Search by name',
    'no_have_data'=>'!No Information',
    'the_package_i_checked'=>'The package I checked',
    'packages_that_i_havenot_checked'=>'Packages that I haven\'t checked',
    'filter'=>'Filter',
    'buy_more'=>'Buy more',
    'open_package'=>'Open package',
    'close_package'=>'Closed package for sale',
    'clear_filter'=>'Clear filter',
    'clear_option'=>'Clear option',
    'search'=>'Search',
    'show_all_list'=>'Show all list',
    'selectall'=>'Select all',
    'un_selectall'=>'Un select all',
    'date_start'=>'Date Start',
    'date_end'=>'Date End',
    'flight'=>'Flight',
    'airline'=>'Airline',
    'number_of_people'=>'Number Of People',
    'number_of_tourist'=>'Number Of Tourist',
    'number_of_people_booking'=>'Number Of People Booking',
    'number_of_people_available'=>'Number Of People Available',

    'price_sale'=>'PriceSale',
    'commission'=>'Commission',
    'schedule_travel'=>'Schedule travel',
    'add_rate'=>'Add rate',
    'original_package_file'=>'Original package file',
    'original_package_code'=>'Original package code',
    'package_details'=>'Package details',
    'description'=>'Description',
    'code'=>'Code',
    'fax'=>'Faxsignin',
    'back_to_list'=>'Back to list',
    'checking'=>'Check',
    'status_D'=>'Draft',
    'status_P'=>'Waiting for examination',
    'status_CP'=>'Open for sale',
    'status_Y'=>'Sold out',
    'help'=>'Help',
    'this_tour_package_closed_for_sale'     => 'This tour package is closed for sale',
    'value_added_service'               => 'Value added service',
    'verify_now'                        =>'Verify Now',
    'totals'                            => 'Totals',
    'have_account_sign_up'              => 'Have account? Sign up',
    'password'              => 'Password',
    'forgot_password'              => 'Forgot your password?',
    'have_no_data'=>'Have No Data!',
    'checkout'=>'checkout',
    'items'=>'Item',
    'description'=>'Description',
    'unit_price'=>'Unit Price',
    'unit_total'=>'Unit Total',
    'include_tax'=>'Include Tax',
    'subtotal'=>'Subtotal',
    'shipping'=>'Shipping',
    'tax'=>'Tax',
    'total_amount'=>'Total Amount',
    'tour_deposit'=>'Tour deposit',
    'tour_balance'=>'Tour balance',
    'travel_preparation'=>'Travel preparation',
    'order_summary'=>'Order summary',
    'package_not_found'=>'Package not found!',
    'payment_methods'=>'Payment Methods',
    'payment'=>'Payment',
//    'button_public'=>'Info check and publish to sell',
    'button_public'=>'Confirmation of verification',
    'button_already_public'=>'This package has been confirmed.',
    'list_all'=>'List all',
    'mall'=>'Mall',
    'confirm'=>'Confirm',
    'information_for_receipt_or_tax_invoice'=>'Information for receipt / or tax invoice',
    'information_for_invoice'=>'Information for showing the receipt',
    'name_company_for_receipt_tax_invoice'=>'Name / company for receipt / tax invoice :',
    'id_card_number'=>'เลขประจำตัวผู้เสียภาษี 13 หลัก/เลขที่บัตร ปชช.:',
    'food_options_for_today'=>'Food options for today',
    'option_cancel'=>'Option to cancel',
    'use_the_same_address'=>'Use the same address',
    'add_new_address'=>'Add new address',
    'need_tax_invoice'=>'Need tax invoice',
    'no_need_tax_invoice'=>'No need tax invoice',
	'send_request_partner'=>'Send request partner',
	'program_suggest'=>'Program Suggest',
	'destination'=>'Destination',
	'search_city_country'=>'Search City Country',
    'low'=>'Low',
    'high'=>'High',
    'hour'=>'Hour',
    'minute'=>'Minute',
    'seconds'=>'Seconds',
    'last_one'=>'Last one',
    'search_program'=>'Filter Program tour',
    'program_latest'=>'Latest Program tour',
    'price_title_down'=>'Saving',
	'price_title_up'=>'Price increase',
    'keep_value_deposit'=>'Deposit',
    'return_value_deposit'=>'Return the deposit',
	'value_deposit'=>'Deposit',
	'return_value_deposit'=>'Return the deposit',
    'keep_value_tour'=>'Keep tour',
    'payment_date'=>'Data Payment',
    'payment_before'=>'Payment before',
    'payment_detail'=>'Payment detail',
    'value_tour'=>'Transportation cost',
    'return_value_tour'=>'Return the tour',
    'keep_all_costs'=>'All costs',
    'return_all_costs'=>'Return all expenses',

    'keep_all_deposit'=>'All deposit',
    'return_all_deposit'=>'Return all deposit',

    'total_price'=>'Total price',
    'discount'=>'Discount',
    'agency'=>'Agency',
	'person'=>'person',
	'partner'=>'Partner',
	'preview'=>'Preview',
	'add_partner'=>'Add Partner',
	'more_info'=>'more info',
	'request_partner'=>'Request Partner',
	'select_timeline'=>'Choose Timeline',
    //********* Home page ********/
	
	'your_review_info'               => 'Your review information in this location',
	'your_review_last_date'               => 'Your latest review',
	'your_point_review'               => 'Your point review',
	'reviewed_on'             => 'reviewed ',
	'posted_on'             => 'posted ',
	'Y'               => ' Enabled',
    'N'               => ' Pause',
    'home'               => 'Home',
    'create'             => 'Create',
    'where_did_you_go'      => 'Where did you go?',
    'create_an_account'     => 'Create an Account',
    'registered'            => 'Your registration is completed.',
    'time_zone'             => 'Time zone:',
    'time_zones'             => 'Time zone',
	'travel_time'           => 'Travel time:',
	'saving'                => 'Saving',
	'addtocart'             => 'Add to cart',
	'program'               => 'program',
	'add_highlight'         => 'Add this to highlight program',
	'seller_commission'     => 'Seller commission',
	'company_commission'    => 'Company commission',

    'delete_photo'         => 'Delete photo',
    'package'              => 'Package',
    'admin'                => 'admin',
    'wish'                 => 'Wish',
    'list'                 => 'List',
    'wishlist'             => 'Wishlist',
    'Language'              =>'Language',
    'package_by'           => 'Tour operation by',
    'sell_by'              => 'Sell by',

    'followers'          => 'Followers',

    'follow'             => 'follow',

    'following'          => 'Following',

    'view_more'          => 'View more',

    'data'             => 'Search',

    'delete'             => 'Delete',

    'messages'           => 'Messages',

    'message'            => 'Message',

    'create_album'       => 'Create album',

    'create_location'        => 'Create location',

    'create_page'        => 'Create page',

    'create_group'       => 'Create group',

    'create_message'     => 'Create message',

    'post'               => 'post',

    'post_filters'       => 'post filters',

    'all'                => 'all',

    'texts'              => 'texts',

    'photos'             => 'photos',

    'videos'             => 'videos',

    'music'              => 'music',

    'places'             => 'places',

    'suggestions'        => 'suggestions',

    'trending'           => 'trending',

    'like'               => 'like',

    'liked'              => 'liked',

    'share'              => 'Share',

    'shares'             => 'Shares',
    'select_category_sub1'             => 'Select Category Sub',
    'category_sub1'             => 'Category Sub',
    'category_sub2'             => 'Category Sub 2',
    'city_sub'             => 'City sub',
    'select_city_sub'             => 'Select City sub',
    'unshare'            => 'unshare',

    'get_notifications'  => 'Get notifications',

    'stop_notifications' => 'Stop notifications',

    'unlike'             => 'unlike',

    'unliked'            => 'unliked',

    'change_avatar'      => 'Change avatar',

    'edit_profile'       => 'Edit profile',

    'change_cover'       => 'Change cover',

    'reposition_cover'   => 'Reposition cover',

    'posts'              => 'Posts',

    'likes'              => 'Likes',

    'groups'             => 'Groups',

    'group'              => 'Group',

    'pages'              => 'Pages',

    'albums'             => 'Albums',

    'comment'            => 'comment',

    'comments'           => 'comments',

    'create_event'       => 'Create Event',

    'guest-event'        => 'Guest Events',

    'avatar'             => 'Avatar',

    'create_event_in'    => 'Create Event in',

    'delete_all'         => 'Delete all', 

    'hide_notifications' => 'Hide post',  

    //********* Footer ********/


    'complete'             => 'Complete',

    'confirm'             => 'Confirm',

    'about'               => 'About',
    'about_tax'           => 'About Tax',

    'invoice'             => 'Invoice',

    'terms'               => 'Terms',

    'contact'             => 'Contact',

    'privacy_policy'      => 'Privacy policy',

    'terms_of_use'        => 'Terms of use',

    'disclaimer'          => 'Disclaimer',

    'languages'           => 'Languages',

    'language'            => 'Language',

    'address'             => 'Address',
    'address_for_invoice'             => 'Address for invoice / tax invoice',
    'pay_via_payment_gateway'=>'Pay via Payment Gateway',
    'phone'               => 'Phone',

    'website'             => 'Website',

    'copyright'           => 'Copyright',

    'all_rights_reserved' => 'All Rights Reserved',



    /********* User Settings ********/



    'menu'                              => 'menu',

    'general_settings'                  => 'General Settings',
    'upload_original_file'                  => 'Upload original file',

    'settings'                          => 'settings',

    'privacy_settings'                  => 'Privacy Settings',

    'my_affiliates'                     => 'my affiliates',

    'my_profile'                        => 'my profile',

    'email_notifications'               => 'Email Notifications',

    'connected_services'                => 'connected services',

    'deactivate_account'                => 'Deactivate Account',

    'my_connections'                    => 'My Connections',

    'username'                          => 'Username',

    'fullname'                          => 'Full name',

    'emails'                             => 'E-mail',
    'email'                             => 'E-mail',

    'birthday'                          => 'Birthday',

    'gender'                            => 'Gender',

    'male'                              => 'Male',

    'female'                            => 'Female',

    'none'                              => 'None',

    'current_city'                      => 'Current city',

    'country'                           => 'Country',

    'timezone'                          => 'Timezone',
    'duration'                  		=> 'Duration',
	'seat'                  			=> 'seat',
	'per_person'                  		=> 'per person',

    'save_changes'                      => 'Save changes',

    'saved_changes'                     => 'Changes are updated successfully',

    'label_confirm_request'             => 'Confirm request when someone follows you',

    'label_follow_privacy'              => 'Who can follow you',

    'label_message_privacy'             => 'Who can message you',

    'label_comment_privacy'             => 'Who can comment on your posts',

    'label_timline_post_privacy'        => 'Who can post on your timeline',

    'label_post_privacy'                => 'Who can see your posts',

    'yes'                               => 'Yes',

    'no'                                => 'No',

    'everyone'                          => 'Everyone',

    'people_i_follow'                   => 'People I follow',

    'no_one'                            => 'No one',

    'upload'                            => 'Upload',

    'current_password'                  => 'Current password',

    'new_password'                      => 'New password',

    'confirm_password'                  => 'Confirm password',

    'confirm_password_text'             => 'Confirm password for user',

    'new_password_text'                 => 'New password for user',

    'affiliate_link'                    => 'Affiliate link',

    'rewards_balance'                   => 'Rewards Balance',

    'spend_rewards'                     => 'Spend rewards',

    'transaction_id'                    => 'Transaction Id',

    'referral_id'                       => 'Referral Id',

    'type'                              => 'Type',

    'amount'                            => 'Amount',

    'date'                              => 'Date',

    'processed_on'                      => 'Processed On',

    'status'                            => 'Status',

    'join'                              => 'Join',

    'joined'                            => 'Joined',

    'join_requested'                    => 'Join requested',

    'privacy'                           => 'Privacy',

    'members'                           => 'Members',

    'admins'                            => 'Admins',

    'people_like_this'                  => 'People like this',

    'pages_liked'                       => 'Pages liked',

    'category'                          => 'Category',

    'select_category'                   => 'Select category',

    'no_posts'                          => 'No posts were found!',

    'accept'                            => 'Accept',

    'decline'                           => 'Decline',

    'remove'                            => 'Remove',

    'addmembers'                        => 'Add Members',

    'add'                               => 'Add',

    'groups_joined'                     => 'Groups Joined',

    'requests_follow'                   => 'Follow requests',

    'join_requests'                     => 'Join Requests',

    'update_profile'                    => 'update profile',

    'picture'                           => 'picture',

    'requested'                         => 'requested',

    'lives_in'                          => 'Lives in',

    'from'                              => 'From',

    'born_on'                           => 'Born on',

    'with'                              => 'with',

    'and'                               => 'and',

    'others'                            => 'others',

    'other'                             => 'other',

    'reply'                             => 'reply',

    'replies'                           => 'replies',

    'report'                            => 'Report',

    'reported'                          => 'Reported',

    'page_likes'                        => 'Page Likes',

    'name_of_your_page'                 => 'Name of your page',

    'address_of_your_page'              => 'Address of your page',

    'name_of_your_group'                => 'Name of your group',

    'open_group'                        => 'open group',

    'closed_group'                      => 'closed group',

    'secret_group'                      => 'secret group',

    'update_group'                      => 'update group',

    'update_password'                   => 'update password',

    'update_picture'                    => 'Update picture',

    'current_password'                  => 'current password',

    'save_password'                     => 'save password',

    'label_email_follow'                => 'When someone follows me',

    'label_email_comment'               => 'When someone comments my post',

    'label_email_post_share'            => 'When someone share my post',

    'label_email_reply_comment'         => 'When someone replies to my comment',

    'label_email_like_post'             => 'When someone likes my post',

    'label_email_like_comment'          => 'When someone likes my comment',

    'label_email_join_group'            => 'When someone joins my group',

    'label_email_like_page'             => 'When someone likes my page',

    'linked_in'                         => 'Linked In',

    'google'                            => 'Google',

    'facebook'                          => 'Facebook',

    'twitter'                           => 'Twitter',

    'connect'                           => 'connect',

    'label_group_member_privacy'        => 'Who can add members to this group',

    'label_group_timeline_post_privacy' => 'Who can post on this group',

    'label_page_timeline_post_privacy'  => 'Who can post on this page',

    'label_page_message_privacy'        => 'Who can message to this page',

    'update_page'                       => 'Update page',

    'admin_roles'                       => 'Admin roles',

    'admin'                             => 'Admin',

    'editor'                            => 'editor',

    'add_admins'                        => 'add admins',

    'label_page_member_privacy'         => 'Who can add members to this page',

    'date_time'                         => 'Date / Time',

    'public'                            => 'Public',

    'private'                           => 'Private',

    'name_of_your_event'                => 'Name of your event',

    'only_guests'                       => 'Guests',

    'invitemembers'                     => 'Invite Members',

    'guests'                            => 'Guests',

    'update_event'                      => 'Update Event',

    'label_event_invite_privacy'        => 'Who can invite guests to this event',

    'label_event_timeline_post_privacy' => 'Who can post on this event',

    'want_to_go'                        => 'I Want To Go',

    'iam_going'                         => 'I\'m Going',

    'guest_asc'                         => 'Guests asc',

    'guest_desc'                        => 'Guests desc',

    'member_asc'                        => 'Members asc',

    'member_desc'                       => 'Menbers desc',

    'label_group_timeline_event_privacy' => 'Who can add events to the group',

    'likes_asc'                         => 'Likes asc',

    'likes_desc'                        => 'Likes desc',


	/********* Location ********/
	'locations'                        => 'Destination', 
	'suggested_location'			   => 'Suggested Destination',
	'suggested_public_albums'		   => 'Suggested Photo Album',
	'map'			                   => 'Map',
	'name'			                   => 'Name',
	'location_likes'			       => 'Like',
	'update_location'			       => 'Update Location',
	'state'			                   => 'State',
	'select_state'			           => 'Select State',
	'city'			                   => 'City',
	'select_city'			           => 'Select City',
	'select_country'			           => 'Select Country',
	'ratings_reviews'			           => 'Ratings & Reviews',
	'by'			                    => 'By',


	/********* Public Albums ********/
	'public_album'                        => 'Public Album',
	'create_public_album'                 => 'Create Public Album',
	'joined_publicalbum'                 => 'Joined Public Album',

	/********* Post Not User ********/
	'post_message_not_user'                 => 'Post messages, photo or video about',


	/********* Admin panel ********/
	
    'module_manage'            => 'Module Manage',
    'dashboard'                => 'Dashboard',

    'application_statistics'   => 'Application statistics',

    'website_settings'         => 'Website settings',

    'general_website_settings' => 'General website settings',

    'user_settings'            => 'User settings',

    'user_settings_text'       => 'User authentication settings',

    'page_settings'            => 'Page settings',

    'page_settings_text'       => 'Manage categories & settings',

    'group_settings'           => 'Group settings',

    'group_settings_text'      => 'manage group authentication',

    'announcements'            => 'Announcements',

    'announcements_text'       => 'Make an announcement',

    'themes'                   => 'Themes',

    'themes_text'              => 'Give a new look',

    'newsletter'               => 'Newsletter',

    'newsletter_text'          => 'Surprise your users',

    'manage_users'             => 'Manage users',

    'manage_users_text'        => 'List of all users',

    'manage_pages'             => 'Manage pages',

    'manage_pages_text'        => 'List of all the pages',

    'manage_groups'            => 'Manage groups',

    'manage_groups_text'       => 'List of all groups',

    'manage_reports'           => 'Manage reports',

    'manage_reports_text'      => 'list of reports',

    'manage_ads'               => 'Manage Ads',

    'manage_ads_text'          => 'Post an ad here',

    'utility_pages'            => 'Utility pages',

    'utility_pages_text'       => 'Manage utility pages here',

    'custom_pages'             => 'Custom pages',

    'custom_pages_text'        => 'Manage custom pages here',

    'photo_s_selected'         => 'photo(s) selected',

    'description'              => 'Description',

    'location'                 => 'Location',

    'cancel'                   => 'Cancel',

    'liked_pages'              => 'Liked Pages',

    'joined_groups'            => 'Joined Groups',

    'show_all'                 => 'Show all',

    'see_all'                  => 'See all',

    'notifications'            => 'Notifications',

    'logout'                   => 'Logout',

    'my_pages_groups'          => 'My Pages &amp; Groups',

    'edit'                     => 'Edit',

    'bio'                      => 'Bio',

    'manage_roles'             => 'Manage roles',

    'follow_requests'          => 'Follow requests',

    'most_trending'            => 'Most Trending',

    'suggested_people'         => 'Suggested People',

    'suggested_groups'         => 'Suggested Groups',

    'suggested_pages'          => 'Suggested Pages',

    'users'                    => 'Users',

    'user'                     => 'User',

    'timeago'                  => 'timeago',

    'with_people'              => 'with People',

    'at'                       => 'at',

    'assign'                   => 'Assign',

    'new_messages'             => 'New messages',

    'available_languages'      => 'Available languages',

    'environment_settings'     => 'Environment Settings',

    'edit_on_risk'             => 'Please do not change these, unless you know what you are doing',

    'disabled_on_demo'         => 'Some features are disabled on demo!',

    'selected'                 => 'Selected',

    'author'                   => 'Author',

    'browse'                   => 'Latest',

    'tweet'                    => 'Tweet',

    'copy_embed_post'          => 'Copy the below text to embed the post',

    'close'                    => 'Close',

    'be_social'                => 'Be Social',

    'add_members'              => 'Add Members',

    'affiliates'               => 'Affiliates',

    'reset_password'           => 'Reset Password',

    'designation'              => 'Designation',

    'your_qualification'       => 'Your qualification',

    'hobbies'                  => 'Hobbies',

    'interests'                => 'Interests',

    'add_your_interests'       => 'Add your interests',

    'mention_your_hobbies'     => 'Mention your hobbies',

    'personal'                 => 'Personal',

    'events'                   => 'Events',

    'enter_location'           => 'Enter location',

    'event_settings'           => 'Event settings',

    'event_settings_text'      => 'manage event authentication',

    'hosted_by'                => 'Hosted by',

    'starts_on'                => 'Starts On',

    'ends_on'                  => 'Ends On',

    'event'                    => 'Event',

    'manage_events'            => 'Manage Events',

    'manage_events_text'       => 'List of all events',

    'ongoing'                  => 'Ongoing',

    'upcoming'                 => 'Upcoming',

    'expired'                  => 'Expired',

    'allnotifications'         => 'All Notifications',

    'notification'             => 'Notification',

    'rtl_version'              => 'RTL Version',

    'name_of_the_album'        => 'Name of the Album',

    'upload_photos'            => 'Upload Photos',

    'all_albums'               => 'All Albums',

    'last_updated'             => 'Last updated',

    'edit_album'               => 'Edit Album',

    'view_album'               => 'View Album',

    'privacy_type'             => 'Privacy Type',

    'set_as_preview'           => 'Set as preview',

    'add_more_photos'          => 'Add more photos',

    'update_album'             => 'Update Album',

    'show_more'                => 'Show more',

    'show_less'                => 'Show less',

    'create_album'             => 'Create album',

    'albums'                   => 'Albums',

    'name_of_the_album'        => 'Name of the Album',

    'upload_photos'            => 'Upload Photos',

    'all_albums'               => 'All Albums',

    'last_updated'             => 'Last updated',

    'edit_album'               => 'Edit Album',

    'view_album'               => 'View Album',

    'privacy_type'             => 'Privacy Type',

    'set_as_preview'           => 'Set as preview',

    'add_more_photos'          => 'Add more photos',

    'update_album'             => 'Update Album',

    'show_more'                => 'Show more',

    'show_less'                => 'Show less',

    'delete_album'             => 'Delete Album',

    'youtube_links'            => 'Add youtube links',

    'copy_paste_youtube_link'  => 'Copy and paste the youtube link/URL',

    'one_more'                 => 'One more',

    'view_image'               => 'View Image',

    'all_photos'               => 'All Photos',

    'albums_photos'            => 'Albums & Photos',

    'youtube_videos'           => 'Youtube Videos',

    'play_video'               => 'play video',

    'my_albums'                => 'My Albums',

    'is_following_you'         => 'is following you',

    'is_unfollowing_you'       => 'is unfollowing you',

    'follows_you'              => 'follows you',

    'joined_your_group'        => 'joined your group',

    'unjoined_your_group'      => 'unjoined your group',

    'attending_your_event'     => 'attending your event',

    'quit_attending_your_event'=> 'quit attending your event',

    'request_join_group'       => 'request to join your group',

    'request_follow'           => 'requested you to follow',

    'liked_your_page'          => 'liked your page',

    'unliked_your_page'        => 'unliked your page',

    'removed_from_group'       => 'removed you from the group',

    'removed_from_page'        => 'removed you from the page',

    'accepted_join_request'    => 'accepted your join request',

    'rejected_join_request'    => 'rejected your join request',

    'deleted_your_comment'     => 'deleted your comment',

    'deleted_your_wishlist'     => 'Delete your wishlist',

    'reported_your_post'       => 'reported your post',

    'mentioned_you_in_post'    => 'mentioned you in their post',

    'commented_on_your_post'   => 'commented on your post',

    'liked_your_comment'       => 'liked your comment',

    'unliked_your_comment'     => 'unliked your comment',

    'shared_your_post'         => 'shared your post',

    'unshared_your_post'       => 'unshared your post',

    'reported_you'             => 'reported you',

    'unreported_you'           => 'unreported you',

    'unreported_your_page'     => 'unreported your page',

    'accepted_follow_request'  => 'accepted your follow request',

    'rejected_follow_request'  => 'rejected your follow request',

    'update_now'               => 'Update Now',

    'my_pages'                 => 'My pages',

    'my_groups'                => 'My groups',

    'my_events'                => 'My events',

    'joined_pages'             => 'Joined pages',

    'joined_location'          => 'Joined location',

    'joined_groups'            => 'Joined groups',

    'wallpaper_settings'       => 'Wallapaper settings',

    'upload_new'               => 'Upload custom wallpaper',

    'wallpaper_settings'       => 'Wallpaper Settings',

    'no_existing_wallpapers'   => 'No existing wallpapers! Please select a custom one.',

    'select_from_existing'     => 'Select from existing wallpapers',

    'active'                   => 'Active',

    'activate'                 => 'Activate',

    'activated'                => 'Activated',

    'no_wallpaper'             => 'No wallpaper',

    'active_wallpaper'         => 'Active Wallpaper',

    'set_wallpaper'            => 'Set Wallpaper',

    'wallpapers'               => 'Wallpapers',

    'wallpapers_text'          => 'upload custom wallpapers',

    'save'                     => 'Save',

    'unsave'                   => 'Unsave',

    'saved_items'              => 'Saved Items',

    'saved'                    => 'Saved',

    'unsave'                   => 'Unsave',

    'save_post'                => 'Save Post',

    'unsave_post'              => 'Unsave Post',

    'saved_items'              => 'Saved Items',

    'email_address'            => 'E-Mail Address',

    'reset_password'           => 'Reset Password',
    'register'           		=> 'Register',

    'send_password_reset_link' => 'Send Password Reset Link',

    'enter_mail'               => 'Enter the registered E-mail address',

    'signin'                   => 'Sign In',

    'general'                  => 'General',

    'homepage'                 => 'Homepage',

    'home_welcome_message_text'   => 'Enter welcome message on home page',

    'home_welcome_message'        => 'Welcome message',

    'home_widget_one'          => 'Widget One',

    'home_widget_one_text'     => 'Enter description for first widget',

    'home_widget_two'          => 'Widget Two',

    'home_widget_two_text'     => 'Enter description for second widget',

    'home_widget_three'          => 'Widget Three',

    'home_widget_three_text'     => 'Enter description for third widget',

    'home_list_heading'         => 'Features list heading',

    'home_list_heading_text'    => 'Enter heading for features list',

    'home_feature_one_icon'     => 'Feature 1 Icon',

    'home_feature_one_icon_text'  => 'Feature 1 Icon code',

    'home_feature_one'           => 'Feature 1 Text',

    'home_feature_one_text'      => 'Enter text for Feature 1',

    'home_feature_two_icon'     => 'Feature 2 Icon',

    'home_feature_two_icon_text'  => 'Feature 2 Icon code',

    'home_feature_two'           => 'Feature 2 Text',

    'home_feature_two_text'      => 'Enter text for Feature 2',

    'home_feature_three_icon'     => 'Feature 3 Icon',

    'home_feature_three_icon_text'  => 'Feature 3 Icon code',

    'home_feature_three'           => 'Feature 3 Text',

    'home_feature_three_text'      => 'Enter text for Feature 3',

    'home_feature_four_icon'     => 'Feature 4 Icon',

    'home_feature_four_icon_text'  => 'Feature 4 Icon code',

    'home_feature_four'           => 'Feature 4 Text',

    'home_feature_four_text'      => 'Enter text for Feature 4',

    'home_feature_five_icon'     => 'Feature 5 Icon',

    'home_feature_five_icon_text'  => 'Feature 5 Icon code',

    'home_feature_five'           => 'Feature 5 Text',

    'home_feature_five_text'      => 'Enter text for Feature 5',

    'home_feature_six_icon'     => 'Feature 6 Icon',

    'home_feature_six_icon_text'  => 'Feature 6 Icon code',

    'home_feature_six'           => 'Feature 6 Text',

    'home_feature_six_text'      => 'Enter text for Feature 6',

    'home_feature_seven_icon'     => 'Feature 7 Icon',

    'home_feature_seven_icon_text'  => 'Feature 7 Icon code',

    'home_feature_seven'           => 'Feature 7 Text',

    'home_feature_seven_text'      => 'Enter text for Feature 7',

    'home_feature_eight_icon'     => 'Feature 8 Icon',

    'home_feature_eight_icon_text'  => 'Feature 8 Icon code',

    'home_feature_eight'           => 'Feature 8 Text',

    'home_feature_eight_text'      => 'Enter text for Feature 8',

    'home_feature_nine_icon'     => 'Feature 9 Icon',

    'home_feature_nine_icon_text'  => 'Feature 9 Icon code',

    'home_feature_nine'           => 'Feature 9 Text',

    'home_feature_nine_text'      => 'Enter text for Feature 9',

    'home_feature_ten_icon'     => 'Feature 10 Icon',

    'home_feature_ten_icon_text'  => 'Feature 10 Icon code',

    'home_feature_ten'           => 'Feature 10 Text',
    'home_feature_ten_text'      => 'Enter text for Feature 10',
    'tour_management'      => 'Tour Management',
    'check_rate'      => 'Check Rate',
    'copy'      => 'Copy',
    'to'      => ' to ',
    'condition'      => 'Condition',
    'condition_term'      => 'Condition & Terms',
    'recommended_before_traveling'      => 'Recommended before traveling',

    'deposit_all'      => 'Diposit all',
    'deposit'      => 'Diposit',
    'discount'      => 'Discount',
	'day'      => ' Day ',
    'not_deposit_all'      => 'Return Diposit all',
    'service_charge_all'      => 'service charge all',
    'return_service_charge_all'      => 'Return service charge all',
    'service_charge'      => 'service_charge',
    'condition_is_used'      => 'This condition is used.',
    'promotion_setting'      => 'Promotion Setting',
	'promotion'      => 'Promotion',
	'date_start'      => 'Date Start',
	'date_end'      => 'Date End',
	'process'      => 'Process',
	'unit'      => 'Unit',
	'up'      => 'Up',
	'down'      => 'Down',
	'booking'      => 'Booking',
	'every_booking'      => 'Every Booking',
	'price'      => 'Price',
	'travel'      => 'Travel',
    'search_in'      => 'Search in',

	'times'      => 'Time',
	'time_start'      => 'Time Start',
	'time_end'      => 'Time End',
	'additional'      => 'Additional',
    'append'      => 'Append',
	'choose'      => 'Choose',
	'star_rating'      => 'Star rating',
	'click_to_review'      => 'Click to review',
	'click_report_a_problem_to_check'      => 'Click report a problem to check',
    'report_a_problem_with_no_payment_notification_from_this_booking'      => 'Report a problem with no payment notification from this booking.',
    'the_problem_has_been_submitted_to_the_system_inspector'      => 'The problem has been submitted to the system inspector.',
    'over_all_rating'      => 'Location',
	'over_all_rating_detail'      => 'ความประทับใจในทำเลที่ตั้ง',
    'cleanliness'      => 'Cleanliness',
	'cleanliness_detail'      => 'ความประทับใจในการรักษาความสะอาดของ',
    'normal_parking'      => 'Parking',
	'normal_parking_detail'      => 'ความประทับใจในสถานที่จอดรถ',
    'normal_toilet'      => 'Toilet',
	'normal_toilet_detail'      => 'ความประทับใจในห้องน้ำ',
    'inside_rating'      => 'Inside Accessibility',
	'inside_rating_detail'      => 'ความประทับใจในสิ่งอำนวยความสะดวกภายในสถานที่/ภายในอาคารสำหรับผู้พิการ',
    'outside_rating'      => 'Outside Accessibility',
	'outside_rating_detail'      => 'ความประทับใจในสิ่งอำนวยความสะดวกรอบๆสถานที่/ภายนอกอาคารสำหรับผู้พิการ',
    'toilet_rating'      => 'PWD Toilet',
	'toilet_rating_detail'      => 'ความประทับใจในห้องน้ำสำหรับคนพิการ',
    'parking_rating'      => 'PWD Parking',
	'parking_rating_detail'      => 'ความประทับใจในสถานที่จอดรถสำหรับคนพิการ',
    'disabled_facilities'      => 'PWD Accessibility ',
    'elevator'      => 'Elevator',
    'review'      => 'Review',
	'date_visit_location'      => 'visited location',
    'place_to_eat'      => 'Eat',
	'travel_destination'      => 'Travel',
	'place_to_shop'      => 'Shop',
	'place_to_stay'      => 'Stay',
	'other_place'      => 'Other',
	'my_location'      => 'For you',
	'my_visited_location'      => 'Place you visited',
	'my_wanted_location'      => 'Place you want to go',
	'my_member_location'      => 'Place you are member',
	'my_admin_location'      => 'Place you are admin',
	'all_visiters_reviewed'      => 'Review from visitors',
	'latest_reviwer'      => 'Latest reviwer',
	'tripable'      => 'TripAble',
	'package_tour'      => 'Package Tour',
	'add_photo'      => 'Add photo',
	'program_tour'      => 'Program Tour',
	'package_tour_by'      => 'Package tour by',
	'this_price'      => 'This Price',
	'only_last_place'      => 'Only last place',
	'this_price_is_only'      => 'This price is only',
	'traveling_date'      => 'Traveling date',
	'tour_type'      => 'Tour type',

	'customer_services'      => 'Customer Services',
	'help_center'      => 'Help center',
	'money_refund'      => 'Money refund',
	'terms_and_policy'      => 'Terms and Policy',
	'open_dispute'      => 'Open Dispute',
	'my_account'      => 'My Account',
	'login'      => 'Login',
	'user_login'      => 'User Login',
	'user_register'      => 'User register',
	'account_setting'      => 'Account Setting',
	'my_orders'      => 'My Orders',
	'my_wishlist'      => 'My Wishlist',
	'partnership'      => 'Partnership',
	'advertice'      => 'Advertice',
	'delivery_and_payment'      => 'Delivery and payment',
	'how_to_buy'                => 'How to buy',
	'our_history'      => 'Our history',
    'history'      => 'History',
    'history_buy'      => 'History Buy',
    'history_return'      => 'History Return',
    'manage'      => 'Manage',

    //********* Invoice ***************
    'account'=>'Account',
    'deposit_receipt'=>'Deposit Receipt',
    'list_of_payment_tour_deposit_payment'=>'List of payment tour deposit payment',
    'list_of_payment_tour_balance_payment'=>'List of payment tour balance payment',
    'receipt'=>'Receipt',
    'receipt_datetime'=>'Receipt date time',
    'order_id'=>'Order ID',
    'invoice'=>'Invoice',
    'reference'=>'Reference',
    'payment_due_date'=>'Payment Due Date',
    'billing_from'=>'Billing From',
    'billing_to'=>'Billing To',
    'tax_id'=>'Tax ID',
    'business_name'=>'Business name',
    'business_type'=>'Business type',
    'bank_type'=>'Business name',
    'bank_account_number'=>'Bank account number',
    'bank_name'=>'Bank name',
    'bank'=>'Bank',
    'transfer_bank'=>'Tranfer bank:',
    'bank_transter'=>'Bank transter:',
    'state'=>'State',
    'slip'=>'Evidence of money transfer / transfer slip',
    'city_sub'=>'City Sub',
    'zipcode'=>'Zip code',

];

