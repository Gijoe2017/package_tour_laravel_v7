<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    //==================================== Translations ====================================//
    'sign_in_join'              => 'Sign in | Join',
    'have_account_sign_up'              => 'Have account? Sign up',
    'affiliate_code'            => 'Affiliate code',
    'already_have_an_account'   => 'Already have an account',
    'confirm_password'          => 'Confirm password',
    'dont_have_an_account_yet'  => 'Don\'t have an account yet?',
    'email_address'             => 'E-mail address',
    'enter_email_or_username'   => 'Enter E-mail or username',
    'forgot_password'           => 'Forgot password',
    'get_started'               => 'get started',
    'login_failed'              => 'Invalid Login credentials.',
    'login_success'             => 'Login Success',
    'login_via_social_networks' => 'Login via social networks',
    'login_welcome_heading'     => 'Welcome back! Please login.',
    'reset_welcome_heading'     => 'Reset your password.',
    'user_name'                 => 'User Name',
    'name'                      => 'Name',
    'member'                    => 'Member',
    'member_login'                 => 'Member Login',
    'my_account'                 => 'My account',
    'password'                  => 'Password',
    'register'                  => 'Register',
    'login'                     => 'Login',
    'registered_verify_email'   => 'You have successfully registered!<br>Please verify your emails before logging in.',
    'reset_password'            => 'Reset Password',
    'reset_your_password'       => 'Reset Your Password.',
    'select_gender'             => 'Select gender',
    'send_password_reset_link'  => 'Send Password Reset Link',
    'sign_in'                   => 'Sign In',
    'sign_out'                   => 'Sign out',
    'signin_to_dashboard'       => 'Sign in to Dashboard',
    'signup_to_dashboard'       => 'Sign up to Dashboard',
    'verify_email'              => 'Please verify your emails.',
    'welcome_to'                => 'Welcome To',
    'subject'                   => 'Subject',
    'submit'                    => 'Submit',
    'email_verify'              => 'Please verify your emails before logging in',
    'email_not_found'              => 'This emails cannot be found in the system.',
    'password_not_match'              => 'The verification password does not match.',
];
