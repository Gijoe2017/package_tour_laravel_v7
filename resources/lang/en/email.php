<?php

return [

    'deposit_payment_notification' => 'Deposit payment notification. Number:',
    'thank you_for_your_booking_with_us' => 'Thank you for your booking with us. Number:',
    'thank_you_for_paying_deposit_number' => 'Thank you for paying deposit. Number:',
    'thank_you_for_paying_the_rest_of_the_tour_fee' => 'Thank you for paying the rest of the tour fee. Number:',
    'thank_you_for_paying_for_the_tour' => 'Thank you for paying for the tour Number',
    'print_out_the_deposit_payment_notification' => 'Print out the deposit payment notification.',
    'print_a_tour_receipt' => 'Print a tour receipt',
    'confirm_booking_date' => 'Confirm Booking. Date:',
    'thanks_for_payment' => 'Thanks for payment',
    'received_payment_notification_from' => 'Received payment notification from ',
    'received_payment_notification' => 'Received payment notification. Order number ',
    'notify_deposit_payment_from' => 'Notify deposit payment from',
     'tour_payment_notification_from' => 'Tour payment notification from',
     'payment_of_the_remaining_tour_payment_from' => 'Payment of the remaining tour payment from',
    'payment_confirmation' => 'Payment confirmation ',
    'this_is_an_automatic_system_notification'=>'This is an automatic system notification.',
    'sorry_if_you_have_already_paid'=>'Sorry if you made a payment',
    'notification_cancel_package'=>'Notification cancel package',
    'cancellation'=>'Cancellation',
    'package_canceled_due_to_payment_termination'=>'Package canceled due to payment termination.',
    'this_email_confirms_seat_availability'=>'This email notifies to confirm seats.',
    'this_email_notifies_to_cancel'=>'This email notifies to cancel this reservation.',
];
