<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['id','timeline_id', 'category_id','category_sub1_id','category_sub2_id', 'message_privacy', 'timeline_post_privacy', 'member_privacy', 'address', 'active', 'phone', 'website', 'verified'];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];


    public function locationfollow()
    {
        return $this->belongsTo('App\LocationFollow');
    }

    public function getNameAttribute($value)
    {
        return $this->timeline->name;
    }

    /**
     * Get the user's  username.
     *
     * @param string $value
     *
     * @return string
     */
    public function getUsernameAttribute($value)
    {
        return $this->timeline->username;
    }

    /**
     * Get the user's  avatar.
     *
     * @param string $value
     *
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return $this->timeline->avatar ? $this->timeline->avatar->source : null;
    }

    /**
     * Get the user's  cover.
     *
     * @param string $value
     *
     * @return string
     */
    public function getCoverAttribute($value)
    {
        return $this->timeline->cover ? $this->timeline->cover->source : null;
    }

    /**
     * Get the user's  about.
     *
     * @param string $value
     *
     * @return string
     */
    public function getAboutAttribute($value)
    {
        return $this->timeline->about ? $this->timeline->about : null;
    }

    public function toArray()
    {
        $array = parent::toArray();
        $timeline = $this->timeline->toArray();
        foreach ($timeline as $key => $value) {
            if ($key != 'id') {
                $array[$key] = $value;
            }
        }
        return $array;
    }

    public function timelines()
    {
        return $this->hasMany('App\Timeline','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

//    public function categorysub1()
//    {
//       return $this->belongsToMany('App\Category', 'categories_sub1', 'categories_sub1.category_id', 'category_id')->withPivot('language_code', 'active');
//    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'location_user', 'location_id', 'user_id')->withPivot('role_id', 'active');
    }

    public function likes()
    {
        return $this->belongsToMany('App\User', 'location_likes', 'location_id', 'user_id');
    }

    public function is_admin($user_id)
    {
        $admin_role_id = Role::where('name', 'admin')->first();
        $locationUser = $this->users()->where('user_id', '=', $user_id)->where('role_id', '=', $admin_role_id->id)->where('location_user.active', 1)->first();
        //dd($locationUser);
        $result = $locationUser ? true : false;
        return $result;
    }

    public function members()
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
       // dd($admin_role_id);
        $members = $this->users()->where('role_id', '!=', $admin_role_id->id)->where('location_user.active', 1)->get();
        $result = $members ? $members : false;
        return $result;
    }

    public function admins()
    {
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $admins = $this->users()->where('role_id', $admin_role_id->id)->where('location_user.active', 1)->get();

        $result = $admins ? $admins : false;

        return $result;
    }

    public function chkLocationUser($location_id, $user_id)
    {
        $location_user = DB::table('location_user')->where('location_id', '=', $location_id)->where('user_id', '=', $user_id)->first();
        $result = $location_user ? $location_user : false;

        return $result;
    }

    public function updateStatus($location_user_id)
    {
        $location_user = DB::table('location_user')->where('id', $location_user_id)->update(['active' => 1]);
        $result = $location_user ? true : false;

        return $result;
    }

    public function updateLocationMemberRole($member_role, $location_id, $user_id)
    {
        $location_user = DB::table('location_user')->where('location_id', $location_id)->where('user_id', $user_id)->update(['role_id' => $member_role]);
        $result = $location_user ? true : false;

        return $result;
    }

    public function removeLocationMember($location_id, $user_id)
    {
        $location_user = DB::table('location_user')->where('location_id', '=', $location_id)->where('user_id', '=', $user_id)->delete();

        $result = $location_user ? true : false;

        return $result;
    }

    public function getLocationCount($location_id)
    {
        $users = DB::table('location_likes')
            ->select(DB::raw('count(*) as user_count, location_id'))
            ->where('location_id', $location_id)
            ->groupBy('location_id')
            ->first();

        $result = $users ? $users : false;
        return $result;
    }


    public function posts()
    {
        return $this->belongsTo('App\Post','timeline_id','timeline_id');
    }
}
