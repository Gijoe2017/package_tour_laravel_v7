<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Toechok co.,ltd.</title>
    <style media="all" type="text/css">
        @media only screen and (max-width: 640px) {
            .span-2,
            .span-3 {
                float: none !important;
                max-width: none !important;
                width: 100% !important;
            }
            .span-2 > table,
            .span-3 > table {
                max-width: 100% !important;
                width: 100% !important;
            }
        }

        @media all {
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        @media all {
            .btn-secondary a:hover {
                border-color: #34495e !important;
                color: #34495e !important;
            }
        }

        @media only screen and (max-width: 640px) {
            h1 {
                font-size: 36px !important;
                margin-bottom: 16px !important;
            }
            h2 {
                font-size: 28px !important;
                margin-bottom: 8px !important;
            }
            h3 {
                font-size: 22px !important;
                margin-bottom: 8px !important;
            }
            .main p,
            .main ul,
            .main ol,
            .main td,
            .main span {
                font-size: 16px !important;
            }
            .wrapper {
                padding: 8px !important;
            }
            .article {
                padding-left: 8px !important;
                padding-right: 8px !important;
            }
            .content {
                padding: 0 !important;
            }
            .container {
                padding: 0 !important;
                padding-top: 8px !important;
                width: 100% !important;
            }
            .header {
                margin-bottom: 8px !important;
                margin-top: 0 !important;
            }
            .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            .btn table {
                max-width: 100% !important;
                width: 100% !important;
            }
            .btn a {
                font-size: 16px !important;
                max-width: 100% !important;
                width: 100% !important;
            }
            .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
            .alert td {
                border-radius: 0 !important;
                font-size: 16px !important;
                padding-bottom: 16px !important;
                padding-left: 8px !important;
                padding-right: 8px !important;
                padding-top: 16px !important;
            }
            .receipt,
            .receipt-container {
                width: 100% !important;
            }
            .hr tr:first-of-type td,
            .hr tr:last-of-type td {
                height: 16px !important;
                line-height: 16px !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>




<body style="font-family: Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6">
    <tr>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; margin: 0 auto !important; max-width: 600px; padding: 0; padding-top: 24px; width: 600px;" width="600" valign="top">
            <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 600px; padding: 0;">

                <!-- START HEADER -->
                <div class="header" style="margin-bottom: 24px; margin-top: 0; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                        <tr>
                            <td class="align-center" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; text-align: center;" valign="top" align="center">
                                <a href="{{url('/')}}" target="_blank" style="color: #3498db; text-decoration: underline;"><img src="{{asset('/images/logo-toechok-invoice.png')}}" width="70" height="41" alt="Logo" align="center" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;"></a>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- END HEADER -->
                <table border="0" cellpadding="0" cellspacing="0" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 4px;" width="100%">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <h1 style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 300; line-height: 1.4; margin: 0; font-size: 36px; margin-bottom: 24px; text-align: center; text-transform: capitalize;">{{trans('email.received_payment_notification').$booking_id}}</h1>

                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;
                                                    <h3>{{$message_admin}}</h3>
                                                </td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                                <td class="receipt-container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; width: 80%;" width="80%" valign="top">
                                                    <p style="font-family: Helvetica, sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 16px;">Notice something wrong? <a href="{{url('/contact')}}" target="_blank" style="color: #3498db; text-decoration: underline;">Contact our support team</a> and we'll be happy to help.</p>
                                                </td>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td class="wrapper" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <?php
                                        $date=\Date::parse($invoice_date);
                                        $payment_date=$date->format(' F, d Y');
                                        ?>
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="center" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 16px;" valign="top">
                                                    <h2 style="color:red">{{trans('common.date').$payment_date}}</h2>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 4px; text-align: center;" valign="top" bgcolor="#3498db" align="center">
                                                                    <a href="{{url('/register/2')}}" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 2px #3498db; border-radius: 4px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 24px; text-transform: capitalize; border-color: #3498db;"> {{trans('common.verify_now')}}</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="align-center" style="font-family: Helvetica, sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 16px; text-align: center;">Thanks for being a great customer.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- START FOOTER -->
                <div class="footer" style="clear: both; padding-top: 24px; text-align: center; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                        <tr>
                            <td class="content-block" style="font-family: Helvetica, sans-serif; vertical-align: top; padding-top: 0; padding-bottom: 24px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
                                <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Toechok co.,ltd. 287/1, Phutabucha, Bangmod, Bangkok, 10140, Thailand</span>
                                {{--<br> Don't like these emails? <a href="http://htmlemail.io/blog" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>.--}}
                            </td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="content-block powered-by" style="font-family: Helvetica, sans-serif; vertical-align: top; padding-top: 0; padding-bottom: 24px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">--}}
                                {{--Powered by <a href="http://htmlemail.io" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">HTMLemail</a>.--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    </table>
                </div>
                <!-- END FOOTER -->
                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
    </tr>
</table>
</body>
</html>