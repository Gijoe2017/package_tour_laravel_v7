<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Toechok co.,ltd.</title>
    <style media="all" type="text/css">
        @media only screen and (max-width: 640px) {
            .span-2,
            .span-3 {
                float: none !important;
                max-width: none !important;
                width: 100% !important;
            }
            .span-2 > table,
            .span-3 > table {
                max-width: 100% !important;
                width: 100% !important;
            }
        }

        @media all {
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        @media all {
            .btn-secondary a:hover {
                border-color: #34495e !important;
                color: #34495e !important;
            }
        }

        @media only screen and (max-width: 640px) {
            h1 {
                font-size: 36px !important;
                margin-bottom: 16px !important;
            }
            h2 {
                font-size: 28px !important;
                margin-bottom: 8px !important;
            }
            h3 {
                font-size: 22px !important;
                margin-bottom: 8px !important;
            }
            .main p,
            .main ul,
            .main ol,
            .main td,
            .main span {
                font-size: 16px !important;
            }
            .wrapper {
                padding: 8px !important;
            }
            .article {
                padding-left: 8px !important;
                padding-right: 8px !important;
            }
            .content {
                padding: 0 !important;
            }
            .container {
                padding: 0 !important;
                padding-top: 8px !important;
                width: 100% !important;
            }
            .header {
                margin-bottom: 8px !important;
                margin-top: 0 !important;
            }
            .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            .btn table {
                max-width: 100% !important;
                width: 100% !important;
            }
            .btn a {
                font-size: 16px !important;
                max-width: 100% !important;
                width: 100% !important;
            }
            .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
            .alert td {
                border-radius: 0 !important;
                font-size: 16px !important;
                padding-bottom: 16px !important;
                padding-left: 8px !important;
                padding-right: 8px !important;
                padding-top: 16px !important;
            }
            .receipt,
            .receipt-container {
                width: 100% !important;
            }
            .hr tr:first-of-type td,
            .hr tr:last-of-type td {
                height: 16px !important;
                line-height: 16px !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>
<?php
function encode($string,$key) {
    $j=0;$hash='';
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}

$Invoice=DB::table('package_invoice')
    ->where('invoice_id',$invoice_id)
    ->first();

$Package=DB::table('package_tour as a')
    ->join('package_tour_info as b','b.packageID','=','a.packageID')
    ->where('a.packageID',$Invoice->invoice_package_id)
    ->first();

$current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

$Timeline=\App\Timeline::where('id','37850')->first();

$media=\App\Media::where('id',$Timeline->avatar_id)->first();
// dd($media);
$BankInfo=DB::table('business_verified_bank')
    ->where('timeline_id',$Timeline->id)
    ->first();
$BusinessInfo=DB::table('business_verified_info1')
    ->where('timeline_id',$Timeline->id)
    ->first();
$BusinessInfo1=DB::table('business_verified_info2')
    ->where('language_code',Auth::user()->language)
    ->where('timeline_id',$Timeline->id)
    ->first();
// dd($BusinessInfo1);
//$country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
//if(!$country){
//    $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
//}
//
//$states=DB::table('states')
//    ->where('country_id',$BusinessInfo->country_id)
//    ->where('state_id',$BusinessInfo->state_id)
//    ->where('language_code',Auth::user()->langauge)
//    ->first();
//
//if(!$states){
//    $states=DB::table('states')
//        ->where('country_id',$BusinessInfo->country_id)
//        ->where('state_id',$BusinessInfo->state_id)
//        ->where('language_code','en')
//        ->first();
//}
//
//$city=DB::table('cities')
//    ->where('country_id',$BusinessInfo->country_id)
//    ->where('state_id',$BusinessInfo->state_id)
//    ->where('city_id',$BusinessInfo->city_id)
//    ->where('language_code',Auth::user()->langauge)
//    ->first();
//
//if(!$city){
//    $city=DB::table('cities')
//        ->where('country_id',$BusinessInfo->country_id)
//        ->where('state_id',$BusinessInfo->state_id)
//        ->where('city_id',$BusinessInfo->city_id)
//        ->where('language_code','en')
//        ->first();
//}

$AddressBook=DB::table('address_book as a')
    ->join('countries as b','b.country_id','=','a.entry_country_id')
    ->where('a.timeline_id',Auth::user()->timeline_id)
    ->where('a.default_address','1')
    ->first();

$Details=DB::table('package_booking_details as a')
//    ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
    ->where('a.booking_id',$Invoice->invoice_booking_id)
    ->where('a.timeline_id',$Invoice->invoice_timeline_id)
    ->get();
//dd($Details);
?>



<body style="font-family: Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6">
    <tr>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; margin: 0 auto !important; max-width: 600px; padding: 0; padding-top: 24px; width: 600px;" width="600" valign="top">
            <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 600px; padding: 0;">
                <!-- START HEADER -->
                <div class="header" style="margin-bottom: 24px; margin-top: 0; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                        <tr>
                            <td class="align-center" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; text-align: center;" valign="top" align="center">
                                <a href="https://toechok.com" target="_blank" style="color: #3498db; text-decoration: underline;"><img src="{{asset('/images/logo-toechok-invoice.png')}}" width="70" height="41" alt="Logo" align="center" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;"></a>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- END HEADER -->
                <table border="0" cellpadding="0" cellspacing="0" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 4px;" width="100%">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <h1 style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 300; line-height: 1.4; margin: 0; font-size: 36px; margin-bottom: 24px; text-align: center; text-transform: capitalize;">{{trans('email.thank_you_for_paying_deposit_number')}}</h1>
                                        <h2 class="align-center" style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 28px; margin-bottom: 16px; text-align: center;">{{trans('common.order_id')}}.: #{{$Invoice->invoice_booking_id}}</h2>
                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                                <td class="receipt-container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; width: 80%;" width="80%" valign="top">
                                                    <table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-bottom: 24px;" width="100%">
                                                        <tr class="receipt-subtle" style="color: #aaa;">
                                                            <?php
                                                            $date=\Date::parse($Invoice->payment_date);
                                                            $booking_date=$date->format(' F, d Y');
                                                            ?>
                                                            <td colspan="2" class="align-center" style="font-family: Helvetica, sans-serif; font-size: 16px; vertical-align: top; text-align: center; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top" align="center">{{$booking_date}}</td>
                                                        </tr>

                                                        <?php $i=1;$TotalsAll=0;$Tax=0;$discount=0;$price_include_vat=''; $Deposit=0;$AdditionalPrice=0;$Totals=0;?>

                                                        @foreach($Details as $Detail)
                                                            <?php

//                                                            $Timeline=\App\Timeline::where('id',$Detail->timeline_id)->first();
//
//                                                            $PackageDetailsOne=DB::table('package_details')
//                                                                ->where('packageDescID',$Detail->package_detail_id)
//                                                                ->first();
//
//                                                            if($PackageDetailsOne->season=='Y'){
//                                                                $order_by="desc";
//                                                            }else{
//                                                                $order_by="asc";
//                                                            }
//
//                                                            $Condition=DB::table('condition_in_package_details as a')
//                                                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
//                                                                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
//                                                                ->where('b.condition_group_id','1')
//                                                                ->where('b.formula_id','>',0)
//                                                                ->where('a.packageID',$Detail->package_id)
//                                                                ->orderby('c.value_deposit',$order_by)
//                                                                ->first();

//                                                            if($Condition){
//                                                                $Deposit_title=$Condition->value_deposit;
//                                                                $Deposit+=$Condition->value_deposit*$Detail->number_of_person;
//                                                            }

                                                            $Deposit_title=$Detail->deposit_price;
                                                            $Deposit+=$Detail->deposit_price*$Detail->number_of_person;

//                                                            $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();
//
//                                                            $st=explode('-',$Detail->packageDateStart);
//                                                            $end=explode('-',$Detail->packageDateEnd);
//
//                                                            if($st[1]==$end[1]){
//                                                                $date=\Date::parse($Detail->packageDateStart);
//                                                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
//                                                                // dd($end[0]);
//                                                            }else{
//                                                                $date=\Date::parse($Detail->packageDateStart);
//                                                                $date1=\Date::parse($Detail->packageDateEnd);
//                                                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
//                                                            }
//
//                                                            $promotion_title=null;$every_booking=0;
//                                                            $promotion=\App\PackagePromotion::where('packageDescID',$Detail->package_detail_id)->active()
//                                                                ->orderby('promotion_date_start','asc')
//                                                                ->first();
//
//                                                            $data_target=null;
//                                                            if($promotion && $promotion->promotion_operator!='Mod'){
//                                                                $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
//                                                            }
                                                            $Totals+=$Deposit
                                                            ?>

                                                            <tr>
                                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">
                                                                   <strong>{!! $Detail->package_detail_title !!}</strong>
                                                                    <span class="text-danger"> {{trans('common.deposit')}} {{$Detail->TourType}}: {{$Invoice->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}</span>
                                                                </td>
                                                                <td class="receipt-figure" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px; text-align: right;" valign="top" align="right">{{$Invoice->currency_symbol.number_format($Deposit)}}</td>
                                                            </tr>
                                                        @endforeach

                                                        <tr class="receipt-bold">
                                                            <td style="font-family: Helvetica, sans-serif; vertical-align: top; margin: 0; padding: 8px; font-size: 18px; border-bottom: 2px solid #333; border-top: 2px solid #333; font-weight: 600;" valign="top">{{trans('common.total_amount')}}</td>
                                                            <td class="receipt-figure" style="font-family: Helvetica, sans-serif; vertical-align: top; margin: 0; padding: 8px; font-size: 18px; border-bottom: 2px solid #333; text-align: right; border-top: 2px solid #333; font-weight: 600;" valign="top" align="right">{{$Invoice->currency_symbol.number_format($Totals)}}</td>
                                                        </tr>
                                                    </table>
                                                    <h2 class="align-center" style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 28px; margin-bottom: 16px; text-align: center;">Your details</h2>

                                                    <table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-bottom: 24px;" width="100%">

                                                        {{--<tr>--}}
                                                            {{--<td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">Shipping to</td>--}}
                                                            {{--@if($AddressBook)--}}
                                                                {{--<td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">--}}
                                                                    {{--{{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>--}}
                                                                    {{--{{$AddressBook->entry_street_address}}<br>--}}
                                                                    {{--{{$AddressBook->entry_city}}, {{$AddressBook->country}} {{$AddressBook->entry_postcode}}<br>--}}
                                                                    {{--{{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>--}}
                                                                    {{--{{trans('common.emails')}}: {{$BusinessInfo1->email}}--}}
                                                                {{--</td>--}}
                                                            {{--@endif--}}
                                                        {{--</tr>--}}
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">Billed to</td>
                                                            @if($AddressBook)
                                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">
                                                                    {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                                                    {{$AddressBook->address_show}}<br>

                                                                    {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                                                    {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    </table>

                                                    <p style="font-family: Helvetica, sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 16px;">Notice something wrong? <a href="{{url('/contact')}}" target="_blank" style="color: #3498db; text-decoration: underline;">Contact our support team</a> and we'll be happy to help.</p>
                                                </td>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td class="wrapper" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <?php
                                        $date=\Date::parse($Invoice->invoice_payment_date);
                                        $diposit_payment_date=$date->format(' F, d Y');
                                        ?>
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;" width="100%">
                                            <tbody>

                                            <?php
                                            $data=url('/home/print/invoice1/').'/'.encode($Invoice->invoice_id.'/'.$Invoice->invoice_package_detail_id.'/'.Auth::user()->id,'Invoice No.');
//                                            $data2=url('/home/print/invoice2/').'/'.encode($Invoice->invoice_booking_id.'/'.$Invoice->invoice_package_detail_id.'/'.Auth::user()->id,'Invoice No.');
                                            //  dd($data2);
                                            ?>
                                            <tr>
                                                <td align="center" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 16px;" valign="top">
                                                    <h2 style="color:red">{{trans('email.thank_you_for_paying_for_the_tour')}}</h2>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 4px; text-align: center;" valign="top" bgcolor="#3498db" align="center">
                                                                <a href="{{$data}}" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 2px #3498db; border-radius: 4px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 24px; text-transform: capitalize; border-color: #3498db;"> {{trans('common.print_a_tour_receipt')}}</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="align-center" style="font-family: Helvetica, sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 16px; text-align: center;">Thanks for being a great customer.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- START FOOTER -->
                <div class="footer" style="clear: both; padding-top: 24px; text-align: center; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                        <tr>
                            <td class="content-block" style="font-family: Helvetica, sans-serif; vertical-align: top; padding-top: 0; padding-bottom: 24px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
                                <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">

                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}<br>

                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                                </span>
                                {{--<br> Don't like these emails? <a href="http://htmlemail.io/blog" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>.--}}
                            </td>
                        </tr>

                    </table>
                </div>
                <!-- END FOOTER -->
                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
    </tr>
</table>
</body>
</html>