<!doctype html>

<html>

<head>

    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>Toechok co.,ltd.</title>

    <style>

        /* -------------------------------------

            GLOBAL RESETS

        ------------------------------------- */



        /*All the styling goes here*/



        img {

            border: none;

            -ms-interpolation-mode: bicubic;

            max-width: 100%;

        }

        body {

            background-color: #f6f6f6;

            font-family: sans-serif;

            -webkit-font-smoothing: antialiased;

            font-size: 14px;

            line-height: 1.4;

            margin: 0;

            padding: 0;

            -ms-text-size-adjust: 100%;

            -webkit-text-size-adjust: 100%;

        }

        table {

            border-collapse: separate;

            mso-table-lspace: 0pt;

            mso-table-rspace: 0pt;

            width: 100%; }

        table td {

            font-family: sans-serif;

            font-size: 14px;

            vertical-align: top;

        }

        /* -------------------------------------

            BODY & CONTAINER

        ------------------------------------- */

        .body {

            background-color: #f6f6f6;

            width: 100%;

        }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */

        .container {

            display: block;

            margin: 0 auto !important;

            /* makes it centered */

            max-width: 900px;

            padding: 10px;

            width: 900px;

        }

        /* This should also be a block element, so that it will fill 100% of the .container */

        .content {

            box-sizing: border-box;

            display: block;

            margin: 0 auto;

            max-width: 900px;

            padding: 10px;

        }

        /* -------------------------------------

            HEADER, FOOTER, MAIN

        ------------------------------------- */

        .main {

            background: #ffffff;

            border-radius: 3px;

            width: 100%;

        }

        .wrapper {

            box-sizing: border-box;

            padding: 20px;

        }

        .content-block {

            padding-bottom: 10px;

            padding-top: 10px;

        }

        .footer {

            clear: both;

            margin-top: 10px;

            text-align: center;

            width: 100%;

        }

        .footer td,

        .footer p,

        .footer span,

        .footer a {

            color: #999999;

            font-size: 12px;

            text-align: center;

        }

        /* -------------------------------------

            TYPOGRAPHY

        ------------------------------------- */

        h1,

        h2,

        h3,

        h4 {

            color: #000000;

            font-family: sans-serif;

            font-weight: 400;

            line-height: 1.4;

            margin: 0;

            margin-bottom: 30px;

        }

        h1 {

            font-size: 35px;

            font-weight: 300;

            text-align: center;

            text-transform: capitalize;

        }

        p,

        ul,

        ol {

            font-family: sans-serif;

            font-size: 14px;

            font-weight: normal;

            margin: 0;

            margin-bottom: 15px;

        }

        p li,

        ul li,

        ol li {

            list-style-position: inside;

            margin-left: 5px;

        }

        a {

            color: #3498db;

            text-decoration: underline;

        }

        /* -------------------------------------

            BUTTONS

        ------------------------------------- */

        .btn {

            box-sizing: border-box;

            width: 100%; }

        .btn > tbody > tr > td {

            padding-bottom: 15px; }

        .btn table {

            width: auto;

        }

        .btn table td {

            background-color: #ffffff;

            border-radius: 5px;

            text-align: center;

        }

        .btn a {

            background-color: #ffffff;

            border: solid 1px #3498db;

            border-radius: 5px;

            box-sizing: border-box;

            color: #3498db;

            cursor: pointer;

            display: inline-block;

            font-size: 14px;

            font-weight: bold;

            margin: 0;

            padding: 12px 25px;

            text-decoration: none;

            text-transform: capitalize;

        }

        .btn-primary table td {

            background-color: #3498db;

        }

        .btn-primary a {

            background-color: #3498db;

            border-color: #3498db;

            color: #ffffff;

        }

        /* -------------------------------------

            OTHER STYLES THAT MIGHT BE USEFUL

        ------------------------------------- */

        .last {

            margin-bottom: 0;

        }

        .first {

            margin-top: 0;

        }

        .align-center {

            text-align: center;

        }

        .align-right {

            text-align: right;

        }

        .align-left {

            text-align: left;

        }

        .clear {

            clear: both;

        }

        .mt0 {

            margin-top: 0;

        }

        .mb0 {

            margin-bottom: 0;

        }

        .preheader {

            color: transparent;

            display: none;

            height: 0;

            max-height: 0;

            max-width: 0;

            opacity: 0;

            overflow: hidden;

            mso-hide: all;

            visibility: hidden;

            width: 0;

        }

        .powered-by a {

            text-decoration: none;

        }

        hr {

            border: 0;

            border-bottom: 1px solid #f6f6f6;

            margin: 20px 0;

        }

        /* -------------------------------------

            RESPONSIVE AND MOBILE FRIENDLY STYLES

        ------------------------------------- */

        @media only screen and (max-width: 620px) {

            table[class=body] h1 {

                font-size: 28px !important;

                margin-bottom: 10px !important;

            }

            table[class=body] p,

            table[class=body] ul,

            table[class=body] ol,

            table[class=body] td,

            table[class=body] span,

            table[class=body] a {

                font-size: 16px !important;

            }

            table[class=body] .wrapper,

            table[class=body] .article {

                padding: 10px !important;

            }

            table[class=body] .content {

                padding: 0 !important;

            }

            table[class=body] .container {

                padding: 0 !important;

                width: 100% !important;

            }

            table[class=body] .main {

                border-left-width: 0 !important;

                border-radius: 0 !important;

                border-right-width: 0 !important;

            }

            table[class=body] .btn table {

                width: 100% !important;

            }

            table[class=body] .btn a {

                width: 100% !important;

            }

            table[class=body] .img-responsive {

                height: auto !important;

                max-width: 100% !important;

                width: auto !important;

            }

        }

        /* -------------------------------------

            PRESERVE THESE STYLES IN THE HEAD

        ------------------------------------- */

        @media all {

            .ExternalClass {

                width: 100%;

            }

            .ExternalClass,

            .ExternalClass p,

            .ExternalClass span,

            .ExternalClass font,

            .ExternalClass td,

            .ExternalClass div {

                line-height: 100%;

            }

            .apple-link a {

                color: inherit !important;

                font-family: inherit !important;

                font-size: inherit !important;

                font-weight: inherit !important;

                line-height: inherit !important;

                text-decoration: none !important;

            }

            .btn-primary table td:hover {

                background-color: #34495e !important;

            }

            .btn-primary a:hover {

                background-color: #34495e !important;

                border-color: #34495e !important;

            }

        }

    </style>

</head>

<?php
function encode($string,$key) {
    $j=0;$hash='';
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}
?>

<body class="">

<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>

<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">

    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">
                <?php
                $Booking=DB::table('package_booking_details')->where('booking_id',$booking_id)->first();
                $Invoices=DB::table('package_invoice')
                    ->where('invoice_booking_id',$booking_id)
                    ->get();
                //dd($Invoices);
                foreach($Invoices as $Invoice){
                $Package=DB::table('package_tour as a')
                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
                    ->where('a.packageID',$Invoice->invoice_package_id)
                    ->first();
                //  dd($Package);
                $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();
                $Timeline=\App\Timeline::where('id','37850')->first();

                $media=\App\Media::where('id',$Timeline->avatar_id)->first();
                // dd($media);
                $BankInfo=DB::table('business_verified_bank')
                    ->where('timeline_id',$Timeline->id)
                    ->first();
                $BusinessInfo=DB::table('business_verified_info1')
                    ->where('timeline_id',$Timeline->id)
                    ->first();
                $BusinessInfo1=DB::table('business_verified_info2')
                    ->where('language_code',Auth::user()->language)
                    ->where('timeline_id',$Timeline->id)
                    ->first();
                // dd($BusinessInfo1);
                $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
                if(!$country){
                    $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
                }

                $states=DB::table('states')
                    ->where('country_id',$BusinessInfo->country_id)
                    ->where('state_id',$BusinessInfo->state_id)
                    ->where('language_code',Auth::user()->langauge)
                    ->first();
                if(!$states){
                    $states=DB::table('states')
                        ->where('country_id',$BusinessInfo->country_id)
                        ->where('state_id',$BusinessInfo->state_id)
                        ->where('language_code','en')
                        ->first();
                }
                $city=DB::table('cities')
                    ->where('country_id',$BusinessInfo->country_id)
                    ->where('state_id',$BusinessInfo->state_id)
                    ->where('city_id',$BusinessInfo->city_id)
                    ->where('language_code',Auth::user()->langauge)
                    ->first();
                if(!$city){
                    $city=DB::table('cities')
                        ->where('country_id',$BusinessInfo->country_id)
                        ->where('state_id',$BusinessInfo->state_id)
                        ->where('city_id',$BusinessInfo->city_id)
                        ->where('language_code','en')
                        ->first();
                }


                $AddressBook=DB::table('address_book as a')
                    ->join('countries as b','b.country_id','=','a.entry_country_id')
                    ->where('a.timeline_id',Auth::user()->timeline_id)
                    ->where('a.default_address','1')
                    ->first();



                $Detail=DB::table('package_booking_details as a')
                    ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                    ->where('a.booking_id',$Invoice->invoice_booking_id)
                    ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                    ->first();
                // dd($Detail);
                $Deposit=0;

                $PackageDetailsOne=DB::table('package_details')
                    ->where('packageDescID',$Detail->package_detail_id)
                    ->first();

                if($PackageDetailsOne->season=='Y'){
                    $order_by="desc";
                }else{
                    $order_by="asc";
                }
                $Condition=DB::table('condition_in_package_details as a')
                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
                    ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                    ->where('b.condition_group_id','1')
                    ->where('b.formula_id','>',0)
                    ->where('a.packageID',$Detail->package_id)
                    ->orderby('c.value_deposit',$order_by)
                    ->first();

                if($Condition){
                    $Deposit_title=$Condition->value_deposit;
                    $Deposit+=$Condition->value_deposit*$Detail->number_of_person;
                }


                $Totals=DB::table('package_booking_details as a')
                    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                    ->where('a.package_id',$Invoice->invoice_package_id)
                    ->sum('a.booking_normal_price');

                $InvoiceNo=sprintf('%09d',$Invoice->invoice_id);
                $OrderID='TC'.sprintf('%09d',$Invoice->invoice_booking_id);
                ?>

                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                    <tr>
                        <td colspan="3" >
                            <h2 class="page-header">
                                @if($Invoice->invoice_type==1)
                                    {{trans('common.deposit_invoice')}}
                                @else
                                    {{trans('common.invoice_balance')}}
                                @endif
                                <small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y',strtotime($Invoice->invoice_date))}}</small>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; width: 60%" valign="top">
                                        @if($media!=null)
                                            <img class="logo-invoice" style="height: 110px" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                        @else
                                            <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                        @endif
                                    </td>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <b>{{trans('common.invoice_no')}}: #{{$InvoiceNo}}</b><br>
                                        <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i')}}<br>
                                        <b>{{trans('common.order_id')}}:</b> {{$OrderID}}<br>
                                        <b>{{trans('common.reference')}}:</b> ACB11<br>
                                        <?php
                                        $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                                        ?>
                                        <strong>
                                            {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}}<BR>
                                            <small>
                                                @if($Invoice->invoice_type=='1' && $Invoice->invoice_status=='2')
                                                    {{trans('common.payment_date').':'.$Invoice->payment_date.' '.$Invoice->payment_time}}
                                                @elseif($Invoice->invoice_type=='2' && $Invoice->invoice_status=='4')
                                                    {{trans('common.payment_date').':'.$Invoice->payment_date.' '.$Invoice->payment_time}}
                                                @else
                                                    {{trans('common.payment_due_date').':'.$Invoice->invoice_payment_date}}
                                                @endif
                                            </small>
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; width:50%" valign="top">
                                        <strong>Billing From/จาก:  </strong><br>
                                        <address>
                                            {{$BusinessInfo1->legal_name}}<br>
                                            {{$BusinessInfo1->address}}, {{$city->city}}<br>
                                            {{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>
                                            {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                            {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                                        </address>
                                    </td>

                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; " valign="top">
                                        <strong>Billing To/ถึง:</strong>
                                        @if($AddressBook)
                                            <address>
                                                {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                                {{$AddressBook->entry_street_address}}<br>
                                                {{$AddressBook->entry_city}}, {{$AddressBook->country}} {{$AddressBook->entry_postcode}}<br>
                                                {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                                {{trans('common.emails')}}: {{Auth::user()->email}}
                                            </address>
                                        @endif
                                    </td>
                                </tr></table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                                <thead>
                                <tr>
                                    <th style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; " valign="top">{{trans('common.items')}}</th>
                                    <th style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; " valign="top">{{trans('common.description')}}</th>
                                    <th style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; " valign="top">{{trans('common.unit_price')}}</th>
                                    <th style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; " valign="top">{{trans('common.unit')}}</th>
                                    <th align="right">{{trans('common.unit_total')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;$TotalsAll=0;$Tax=0;$discount=0;$price_include_vat=''; $Deposit=0;$AdditionalPrice=0;?>
                                <?php
                                $Timeline=\App\Timeline::where('id',$Detail->timeline_id)->first();
                                // dd($Timeline);
                                $PackageDetailsOne=DB::table('package_details')
                                    ->where('packageDescID',$Detail->package_detail_id)
                                    ->first();

                                if($PackageDetailsOne->season=='Y'){
                                    $order_by="desc";
                                }else{
                                    $order_by="asc";
                                }

                                $Condition=DB::table('condition_in_package_details as a')
                                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                    ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                    ->where('b.condition_group_id','1')
                                    ->where('b.formula_id','>',0)
                                    ->where('a.packageID',$Detail->package_id)
                                    ->orderby('c.value_deposit',$order_by)
                                    ->first();

                                if($Condition){
                                    $Deposit_title=$Condition->value_deposit;
                                    $Deposit+=$Condition->value_deposit*$Detail->number_of_person;
                                }

                                $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();

                                $st=explode('-',$Detail->packageDateStart);
                                $end=explode('-',$Detail->packageDateEnd);

                                if($st[1]==$end[1]){
                                    $date=\Date::parse($Detail->packageDateStart);
                                    $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                    // dd($end[0]);
                                }else{
                                    $date=\Date::parse($Detail->packageDateStart);
                                    $date1=\Date::parse($Detail->packageDateEnd);
                                    $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                }


                                $promotion_title=null;$every_booking=0;

                                $promotion=\App\PackagePromotion::where('packageDescID',$Detail->package_detail_id)->active()
                                    ->orderby('promotion_date_start','asc')
                                    ->first();

                                $data_target=null;
                                if($promotion && $promotion->promotion_operator!='Mod'){
                                    $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                }
                                //   dd($promotion);
                                ?>
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: center; " valign="top">{{$i++}}</td>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: center; ">
                                        <strong> {{$Detail->TourType}}: {{trans('common.traveling_date')}} {{$package_date}}</strong><br>
                                        {{$Package->packageName}}<BR>
                                        {{trans('common.sell_by').':'. $Timeline->username}} <BR>
                                        <span class="text-danger"> {{trans('common.deposit')}} {{$Detail->TourType}}: {{$Invoice->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}</span>
                                    </td>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: center; ">{{$Invoice->currency_symbol.number_format($Detail->booking_normal_price)}}</td>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: center; ">{{$Detail->number_of_person}}</td>
                                    <td style="text-align: right">
                                        {{$Invoice->currency_symbol.number_format($Detail->booking_normal_price*$Detail->number_of_person)}}
                                    </td>
                                </tr>
                                @if($Additional)
                                    @foreach($Additional as $rowA)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                            <td>{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                            <td align="center">1</td>
                                            <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                        </tr>
                                        <?php
                                        $AdditionalPrice+=$rowA->price_service;
                                        ?>
                                    @endforeach
                                @endif

                                <?php
                                if($promotion){
                                    if($Detail->booking_normal_price!=$Detail->booking_realtime_price){
                                        $discount+=($Detail->booking_normal_price-$Detail->booking_realtime_price);
                                    }
                                }
                                $TotalsAll+=round($Detail->booking_normal_price*$Detail->number_of_person);
                                $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                                if($Detail->price_include_vat=='Y'){
                                    $price_include_vat=$Detail->price_include_vat;
                                }
                                ?>

                                <?php
                                $Totals=$TotalsAll+$AdditionalPrice;
                                ?>
                                <tr>
                                    <td colspan="4" style="text-align: right">
                                        @if($price_include_vat=='Y')
                                            {{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)
                                        @else
                                            {{trans('common.subtotal')}}
                                        @endif
                                    </td>

                                    <td  style="text-align: right">{{$Invoice->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}}</td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($discount)}}</td>
                                </tr>
                                {{--<tr>--}}
                                {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                                {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                                {{--</tr>--}}

                                @if($Invoice->invoice_type=='2')

                                    <tr>
                                        <td colspan="4" style="text-align: right"><strong>{{trans('common.deposit')}}:</strong></td>
                                        <td style="text-align: right"><strong>-{{$Invoice->currency_symbol.number_format($Deposit)}}</strong></td>
                                    </tr>

                                    @if($price_include_vat!='Y')
                                        <?php $Tax=$Totals*.07;?>
                                        <tr>
                                            <td colspan="4" style="text-align: right"><strong>({{trans('common.include_tax')}} 7%):</strong></td>
                                            <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Tax)}} </strong></td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td colspan="4" style="text-align: right"><strong>{{trans('common.total_amount')}}:</strong></td>
                                        <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($TotalsAll+$Tax+$AdditionalPrice-$discount-$Deposit)}} </strong></td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="4" style="text-align: right"><strong>{{trans('common.total_deposit')}}:</strong></td>
                                        <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Deposit)}}</strong></td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <?php
                    $data=url('/home/print/invoice1/').'/'.encode($Invoice->invoice_id.'/'.$Invoice->invoice_package_detail_id.'/'.Auth::user()->id,'Invoice No.');
                    $data2=url('/home/print/invoice2/').'/'.encode($Invoice->invoice_booking_id.'/'.$Invoice->invoice_package_detail_id.'/'.Auth::user()->id,'Invoice No.');

                    ?>

                    <tr>
                        <td colspan="3">
                            <div class="text-center">
                                @if($Invoice->invoice_type==1)
                                    <a href="{{$data}}">{{trans('email.print_out_the_deposit_payment_notification')}}</a>
                                @else
                                    <a href="{{$data2}}">{{trans('email.print_out_the_deposit_payment_notification')}}</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                </table>

            <?php }?>


            <!-- START FOOTER -->

                <div class="footer">

                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">

                        <tr>

                            <td class="content-block">

                                <span class="apple-link">Toechok co.,ltd. 287/1, Phutabucha, Bangmod, Bangkok, 10140, Thailand</span>

                                {{--<br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>.--}}

                            </td>

                        </tr>


                    </table>

                </div>

                <!-- END FOOTER -->



            </div>

        </td>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>

