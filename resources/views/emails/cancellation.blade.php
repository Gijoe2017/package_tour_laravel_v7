<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Toechok co.,ltd.</title>


    <style media="all" type="text/css">
        @media only screen and (max-width: 640px) {
            .span-2,
            .span-3 {
                float: none !important;
                max-width: none !important;
                width: 100% !important;
            }
            .span-2 > table,
            .span-3 > table {
                max-width: 100% !important;
                width: 100% !important;
            }
        }

        @media all {
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        @media all {
            .btn-secondary a:hover {
                border-color: #34495e !important;
                color: #34495e !important;
            }
        }

        @media only screen and (max-width: 640px) {
            h1 {
                font-size: 36px !important;
                margin-bottom: 16px !important;
            }
            h2 {
                font-size: 28px !important;
                margin-bottom: 8px !important;
            }
            h3 {
                font-size: 22px !important;
                margin-bottom: 8px !important;
            }
            .main p,
            .main ul,
            .main ol,
            .main td,
            .main span {
                font-size: 16px !important;
            }
            .wrapper {
                padding: 8px !important;
            }
            .article {
                padding-left: 8px !important;
                padding-right: 8px !important;
            }
            .content {
                padding: 0 !important;
            }
            .container {
                padding: 0 !important;
                padding-top: 8px !important;
                width: 100% !important;
            }
            .header {
                margin-bottom: 8px !important;
                margin-top: 0 !important;
            }
            .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            .btn table {
                max-width: 100% !important;
                width: 100% !important;
            }
            .btn a {
                font-size: 16px !important;
                max-width: 100% !important;
                width: 100% !important;
            }
            .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
            .alert td {
                border-radius: 0 !important;
                font-size: 16px !important;
                padding-bottom: 16px !important;
                padding-left: 8px !important;
                padding-right: 8px !important;
                padding-top: 16px !important;
            }
            .receipt,
            .receipt-container {
                width: 100% !important;
            }
            .hr tr:first-of-type td,
            .hr tr:last-of-type td {
                height: 16px !important;
                line-height: 16px !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>

<body class="">
<?php
//function encode($string,$key) {
//    $j=0;$hash='';
//    $key = sha1($key);
//    $strLen = strlen($string);
//    $keyLen = strlen($key);
//    for ($i = 0; $i < $strLen; $i++) {
//        $ordStr = ord(substr($string,$i,1));
//        if ($j == $keyLen) { $j = 0; }
//        $ordKey = ord(substr($key,$j,1));
//        $j++;
//        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
//    }
//    return $hash;
//}

$Timeline=\App\Timeline::where('id','37850')->first();

$media=\App\Media::where('id',$Timeline->avatar_id)->first();

$BusinessInfo=DB::table('business_verified_info1')
    ->where('timeline_id',$Timeline->id)
    ->first();

if(Auth::check()){
    $language=Auth::user()->language;
}else{
    $language='th';
}

$BusinessInfo1=DB::table('business_verified_info2')
    ->where('language_code',$language)
    ->where('timeline_id',$Timeline->id)
    ->first();


?>

<span class="preheader">{{trans('common.notice_of_cancellation_from')}} Toechok co.,ltd.</span>

<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">

    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">

                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                    <tr>
                        <td>
                            {{trans('common.order_id')}}:#{{$booking_id}}<BR>
                            {!! $package_title !!}
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="align-center" style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 18px; margin-bottom: 16px; text-align: center;">{!! $message_details !!}</strong>
                        </td>
                    </tr>
                </table>

                <!-- START FOOTER -->

                <div class="footer">
<hr>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="content-block">

                                <span class="apple-link">
                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}<br>

                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.emails')}}: {{$BusinessInfo1->email}}</span>
                                {{--<br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>.--}}
                            </td>
                        </tr>
                        <tr>
                            {{--<td class="content-block powered-by">--}}

                                {{--Powered by <a href="http://htmlemail.io">HTMLemail</a>.--}}

                            {{--</td>--}}

                        </tr>

                    </table>

                </div>

                <!-- END FOOTER -->



            </div>

        </td>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>

