@extends('layouts.member.layout_master_new')

@section('content')

    <div class="box box-danger">
        <div class="box-header with-border">
            <div class="col-md-3"><h2>Users </h2></div>
            <div class="col-md-9 text-right">
            <a href="{{url('module/manage_list')}}" class="btn btn-default"><i class="fa fa-reply"></i> Back </a>
            </div>
        </div>
    <div class="row">

            <div class="col-md-12">
                <div class="box-body">

               <table id="example1" class="table table-bordered">
                   <thead>
                   <tr>
                       <th>User ID</th>
                       <th>Timeline</th>
                       <th>Name</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($Users as $rows)
                       <?php
                        $check=DB::table('location_user')->where('user_id',$rows->id)->where('location_id',$location_id)->first();
                       ?>

                   <tr>
                       <td>{{$rows->id}}</td>
                       <td>{{$rows->timeline_id}}</td>
                       <td>{{$rows->FirstName.' '.$rows->LastName}}</td>
                       <td>
                           @if(!$check)
                               <a href="{{url('module/add_users/'.$rows->id.'/'.$location_id)}}" class="btn btn-info  btn-sm"><i class="fa fa-user"></i> Add User</a>
                           @else
                               <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user"></i> Add User</a>
                           @endif
                       </td>
                   </tr>

                       @endforeach
                   </tbody>
               </table>
                </div>
            </div>

    </div>


    </div>
@endsection