@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-exclamation-triangle"></i> Privacy policy</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('member/dashboard/'.Session::get('timeline'))}}"><i class="fa fa-dashboard"></i> Home Package</a></li>
            <li><a href="{{url('policy/list')}}"> Privacy policy list.</a></li>
            <li><a href="#"> Privacy policy Create.</a></li>
        </ol>
    </section>
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="panel">
            <div class="panel-heading ">
                <h2>Form Edit Provacy policy.</h2>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <!-- The time line -->
                <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('PolicyController@update')}}" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{$Policy->id}}">
                    <div class="box box-info">
                        <div class="box-body">
                                @if(!isset($errors))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            <div class="row">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label><strong>Title<span class="text-red">*</span></strong></label><BR>
                                            <input type="text" class="form-control" name="title" id="title" value="{{$Policy->title}}" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label><strong>Order <span class="text-red">*</span></strong></label><BR>
                                            <input type="number" class="form-control" name="order_by" id="order_by" value="{{$Policy->order_by}}" required="required">
                                        </div>
                                    </div>
                                <div class="col-lg-12">
                                <div class="form-group">
                                    <label><strong>Details<span class="text-red">*</span></strong></label><BR>
                                    <textarea type="text" class="form-control summernote" name="details" id="details" >{{$Policy->details}}</textarea>
                                </div>
                                </div>



                                <div class="col-lg-12">

                                    <a href="{{url('policy/list')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Update')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection