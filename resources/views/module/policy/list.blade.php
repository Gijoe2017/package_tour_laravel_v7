@extends('layouts.member.layout_master_new')


@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-exclamation-triangle"></i> Privacy policy</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('member/dashboard/'.Session::get('timeline'))}}"><i class="fa fa-dashboard"></i> Home Package</a></li>
            <li><a href="#"> Privacy policy List.</a></li>
        </ol>
    </section>
@endsection


@section('content')
    <section class="content">
        <div class="row">
            <div class="panel">
            <div class="panel-heading ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right"><a href="{{url('policy/create')}}" class="btn btn-default"><i class="fa fa-plus"></i> Create new </a> </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <!-- The time line -->
                <div class="box-body">
                    @if($Policy->count())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Details</th>
                            <th>Order</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Policy as $rows)
                            <tr>
                                <td>{{$rows->title}}</td>
                                <td>{!! str_limit($rows->details,150) !!} <button data-id="{{$rows->id}}" data-toggle="modal" data-target="#pupupForm" class="get-details">more.</button> </td>
                                <td>{{$rows->order_by}}</td>
                                <td>
                                    <div class="btn-group btn-xs">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            {{--<li><a href="{{url('policy/language/'.$rows->id)}}"><i class="fa fa-flag"></i> {{trans('package.add_language')}}</a></li>--}}
                                            <li><a href="{{url('policy/edit/'.$rows->id)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a></li>
                                            <li><a href="{{url('policy/delete/'.$rows->id)}}" onclick="confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <h2 class="text-center">Have no data!</h2>
                    @endif
                </div>
            </div>
            </div>
            </div>
            <!-- /.col -->
        </div>
    </section>

    <script>
        $('body').on('click','.get-details',function(){

            var title = "Privacy Policy ";
            var body = "Welcome to Shop system";
            $.ajax({
                type:'get',
                url:SP_source() +'policy/show/details',
                data:{'id':$(this).data('id')},
                success:function(data){
                    $("#popupForm .modal-title").html(title);
                    $("#popupForm .modal-body").html(data);
                    $("#popupForm").modal("show");
                }
            });

        });
    </script>
@endsection