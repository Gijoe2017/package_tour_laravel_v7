@extends('layouts.member.layout_master_new')

@section('content')
    <h2>Register Module {{Session::get('module_id')}}</h2>
    <form role="form"  action="{{action('Module\ModuleController@store_document')}}" enctype="multipart/form-data" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="timeline_id" value="{{Session::get('timeline_id')}}">

    <div class="row">
        @if(Session::has('message'))
            <div class="col-md-12">
                <div class="alert alert-success">{{Session::get('message')}}</div>
            </div>
        @else
            <div class="col-md-12">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Documents</h3>

                        {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}
                        <!-- /.box-tools -->
                    </div>
                <!-- /.box-header -->
                    <div class="box-body">

                        @foreach(\App\DocumentType::where('language_code',Auth::user()->language)->get() as $rows)
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{$rows->document_name}} <span class="text-danger">*</span> </label>
                                <input type="file" class="form-control" id="file{{$rows->id}}" name="file{{$rows->id}}" >
                            </div>
                            <hr>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="col-md-12">
                <button type="submit" class="btn btn-success btn-lg pull-right"> <i class="fa fa-save"></i> {{trans('common.save')}}</button>
            </div>
        @endif
    </div>
    </form>


        </div>
    </div>
@endsection