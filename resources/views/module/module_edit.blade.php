@extends('layouts.member.layout_master_new')

@section('content')
    <h2><i class="fa fa-edit"></i> Register Module {{Session::get('module_id')}}</h2>
    <form role="form"  action="{{action('Module\ModuleController@store_register')}}" enctype="multipart/form-data" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="timeline_id" value="{{Session::get('timeline_id')}}">

    <div class="row">
        @if(Session::has('message'))
            <div class="col-md-12">
                <div class="alert alert-success">{{Session::get('message')}}</div>
            </div>
        @else
        <div class="col-md-4">

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-info"></i> Business Info.</h3>

                    {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                    {{--</div>--}}
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>{{trans('common.business_name')}} <span class="text-danger">*</span> </label>
                        <input type="text" class="form-control" id="legal_name" name="legal_name" value="{{$Business->legal_name}}" required>

                    </div>
                    <div class="form-group">
                        <label>{{trans('common.business_type')}} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="business_type" name="business_type" value="{{$Business->legal_name}}" required>

                    </div>
                    <div class="form-group">
                        <label>{{trans('common.tax_id')}} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="tax_id" name="tax_id" required >

                    </div>
                    <div class="form-group">
                        <label>{{trans('common.phone')}} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="phone" name="phone" required>

                    </div>

                    <div class="form-group">
                        <label>{{trans('common.emails')}}</label>
                        <input type="email" class="form-control" id="email" name="email" required>

                    </div>
                    <div class="form-group">
                        <label>{{trans('common.append')}}</label>
                        <textarea  class="form-control" id="additional" name="additional"></textarea>

                    </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>


        </div>

        <div class="col-md-4">

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-address-book"></i> Business Address</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="col-md-12">


                        <div class="form-group">
                            <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-danger">*</span></label>
                            <select class="form-control select2" style="width: 100%" name="country_id" id="country_id">
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                @foreach($country as $rows)
                                    <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group box-state">
                            <label for="state_id" class="form-label required">{{trans('common.state')}}</label>
                            <select  class="form-control select2" style="width: 100%" name="state_id" id="state_id">
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                {{--@foreach($state as $rows)--}}
                                {{--<promotion value="{{$rows->state_id}}" >{{$rows->state}}</promotion>--}}
                                {{--@endforeach--}}
                            </select>

                        </div>

                        <div class="form-group  box-state city_id">
                            <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                            <select class="form-control select2" style="width: 100%" name="city_id" id="city_id">
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                {{--@foreach($city as $rows)--}}
                                {{--<promotion value="{{$rows->city_id}}" >{{$rows->city}}</promotion>--}}
                                {{--@endforeach--}}
                            </select>

                        </div>

                        <div class="form-group box-state city_sub1_id">

                            <label for="city_sub1_id" class="form-label required">{{trans('common.city_sub')}}</label>
                            <select class="form-control select2" style="width: 100%" name="city_sub1_id" id="city_sub1_id">
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                {{--@foreach($city_sub as $rows)--}}
                                {{--<promotion value="{{$rows->city_sub1_id}}" >{{$rows->city_sub1_name}}</promotion>--}}
                                {{--@endforeach--}}
                            </select>

                        </div>

                        <div class="form-group city_sub1_id">
                            <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                            <input class="form-control" type="text" name="zip_code" id="zip_code" readonly />
                        </div>

                        <div class="form-group">
                            <label class="form-label">{{trans('common.address')}} <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="address" id="address" required />
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>


        </div>


        <div class="col-md-4">

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bank"></i> Business Bank.</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">

                        <div class="form-group">
                            <label  class="form-label">{{trans('common.bank_account_number')}} <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="bank_account_number" id="bank_account_number"  required />
                        </div>
                        <div class="form-group">
                            <label  class="form-label">{{trans('common.account_name')}} <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="account_name" id="account_name"  required />
                        </div>
                        <div class="form-group">
                            <label for="country_id" class="form-label required">{{trans('common.bank_name')}} <span class="text-danger">*</span></label>
                            <select class="form-control select2" style="width: 100%" name="bank_id" id="bank_id" required>
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                @foreach($banks as $rows)
                                    <option value="{{$rows->bank_code}}" >{{$rows->bank_name}}</option>
                                @endforeach
                                <option value="">-- {{trans('common.other')}} --</option>
                            </select>
                        </div>
                        <div style="display: none" class="form-group bank">
                            <label  class="form-label">{{trans('common.add').trans('common.bank_name')}} <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="bank_name" id="bank_name"/>
                        </div>

                        <div class="form-group">
                            <label  class="form-label">{{trans('common.sub_bank')}} <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="sub_bank" id="sub_bank"/>
                        </div>

                        <div class="form-group">
                            <label for="country_id" class="form-label required">{{trans('common.bank_type')}} <span class="text-danger">*</span></label>
                            <select class="form-control select2" style="width: 100%" name="bank_type" id="bank_type" required>
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                @foreach($banktype as $rows)
                                    <option value="{{$rows->account_type_id}}" >{{$rows->account_type}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-danger">*</span></label>
                            <select class="form-control select2" style="width: 100%" name="bank_country_id" id="bank_country_id" required>
                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                @foreach($country as $rows)
                                    <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>


        </div>


        <div class="col-md-12">
            <button type="submit" class="btn btn-success btn-lg pull-right"> <i class="fa fa-save"></i> {{trans('common.save')}}</button>
        </div>
        @endif
    </div>
    </form>


        </div>
    </div>

    <script>
        $('body').on('change','#bank_id',function (e) {
            if(e.target.value==''){
                $('.bank').show();
                $("#bank_name").prop('readonly',false);
            }else{
                $("#bank_name").prop('readonly',true);
                $('.bank').hide();
            }

        });
    </script>

@endsection