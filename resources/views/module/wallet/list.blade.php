@extends('layouts.member.layout_master_new')


@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-bank"></i> {{trans('common.bank_account')}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('member/dashboard/'.Session::get('timeline'))}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"> Bank List.</a></li>
        </ol>
    </section>
@endsection


@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 35px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 35px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 20px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 20px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
        hr.gray {
            border-top: 1px dashed  #5a7b8c;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="panel">
            <div class="panel-heading ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right"><a href="{{url('wallet/bank/create')}}" class="btn btn-default"><i class="fa fa-plus"></i> Add new bank account </a> </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <!-- The time line -->
                <div class="box-body">
                    @if($BankAccount->count())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('common.bank_account_number')}}</th>
                            <th>{{trans('common.account_name')}}</th>
                            <th>{{trans('common.bank_name')}}</th>
                            <th>{{trans('common.sub_bank')}}</th>
                            {{--<th>{{trans('common.bank_type')}}</th>--}}
                            <th>{{trans('common.country')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($BankAccount as $rows)
                            <tr>
                                <td>
                                    <label class="radio-inline">
                                        <div class="material-switch">
                                            <input class="switch-rate"  id="bank_default" name="bank_default" type="checkbox" {{$rows->bank_default==1?'checked':''}} value="1" />
                                            <label for="bank_default" class="label-info"></label>
                                            <span id="bank_default" style="padding-left: 15px">{{$rows->bank_account_number}}</span>
                                        </div>
                                    </label>
                                </td>
                                <td>{{$rows->account_name}}</td>
                                <td>{{$rows->bank_name}}</td>
                                <td>{{$rows->sub_bank}}</td>
                                {{--<td>{{$rows->bank_type}}</td>--}}
                                <td>{{$rows->country}}</td>
                                <td>
                                    <div class="btn-group btn-xs">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            {{--<li><a href="{{url('policy/language/'.$rows->id)}}"><i class="fa fa-flag"></i> {{trans('package.add_language')}}</a></li>--}}
                                            <li><a href="{{url('wallet/bank/edit/'.$rows->id)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a></li>
                                            <li><a href="{{url('wallet/bank/delete/'.$rows->id)}}" onclick="confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <h2 class="text-center">Have no data!</h2>
                    @endif
                </div>
            </div>
            </div>
            </div>
            <!-- /.col -->
        </div>
    </section>

    <script>
        $('body').on('click','.get-details',function(){

            var title = "Privacy Policy ";
            var body = "Welcome to Shop system";
            $.ajax({
                type:'get',
                url:SP_source() +'policy/show/details',
                data:{'id':$(this).data('id')},
                success:function(data){
                    $("#popupForm .modal-title").html(title);
                    $("#popupForm .modal-body").html(data);
                    $("#popupForm").modal("show");
                }
            });

        });
    </script>
@endsection