@extends('layouts.member.layout_master_new')

@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-money"></i> {{trans('common.seller_balance')}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('member/dashboard/'.Session::get('timeline'))}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('wallet/bank_account')}}"> Seller balance</a></li>
        </ol>
    </section>
@endsection


@section('content')
<style>
    .nav-tabs-custom > .nav-tabs > li.active{
        padding: 2px;
    }
    .tab-content > .active{
        padding: 8px;
    }
</style>


    <section class="content">
        <div class="row">
            <div class="panel">
            <div class="panel-heading ">
                <h2>การทำรายการที่ผ่านมา</h2>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <input type="hidden" id="tabCurrent" value="#tab-1">
                <input type="hidden" id="tabType" value="#tab-1">
                <div class="nav-tabs-custom">
                    <div class="col-md-7">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1"  data-toggle="tab">ทั้งหมด</a></li>
                        <li><a href="#tab_2" data-toggle="tab">รายการจากคำสั่งซื้อ</a></li>
                        <li><a href="#tab_3" data-toggle="tab">การถอนเงิน</a></li>
                        <li><a href="#tab_4" data-toggle="tab">การคืนเงินจากคำสั่งซื้อ</a></li>
                        <li><a href="#tab_5" data-toggle="tab">จ่ายด้วย Seller Balance</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-file-excel-o"></i>Export to excel</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-file-pdf-o"></i>Export to pdf</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-file-excel-o"></i>Export to excel</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                            </ul>
                        </li>
                        {{--<li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>--}}
                    </ul>
                    </div>
                    <div class="col-md-2 text-right">
                        <label style="margin-top: 8px"><strong>{{trans('common.choose')}}{{trans('common.date')}} : </strong></label>
                    </div>
                    <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="reservation" name="sort_by_date">
                                </div>
                                <!-- /.input group -->
                            </div>
                    </div>
                    <div class="tab-content">

                        <div id="preload2" style="display: none; position: absolute; z-index: 100; top:10%; margin-left: 20%" class="preload" align="center">
                            <div id="loader-wrapper" class="loader"></div>
                            <p style="color:#7B8BB3"><img src="{{asset('images/preload.gif')}}" width="150"></p>
                        </div>

                        <div class="tab-pane active" id="tab_1"><BR>
                            <div id="show-all">
                                @if($Seller_balance)
                                    <table id="table-data" class="table table-hover">
                                   <thead>
                                       <tr>
                                           <th>{{trans('common.date')}}</th>
                                           <th>{{trans('common.description')}}</th>
                                           <th>{{trans('common.service_fee_upon_receipt')}}</th>
                                           <th>{{trans('common.accept_payment')}}</th>
                                           {{--<th>{{trans('common.payment_methods')}}</th>--}}
                                           <th>{{trans('common.refund')}}</th>
                                           <th>{{trans('common.difference')}}</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                   <?php $totals=0;$refund=0;$payment_amount=0;$diferenttotal=0;?>
                                       @foreach($Seller_balance as $rows)
                                           <?php
                                                   $diferent=0;
                                                   if($rows->payment_amount>$rows->payment_refund){
                                                       $diferent=$rows->payment_amount-$rows->payment_refund;
                                                   }

                                           ?>
                                           <tr>
                                               <td>{{date('m/d/Y',strtotime($rows->payment_date))}} {{$rows->payment_time}}</td>
                                               <td>
                                                   <strong>{{$rows->invoice_type=='1'?trans('common.tour_deposit'):trans('common.tour_balance')}}</strong><BR>
                                                   <a href="{{url('booking/backend/show/invoice/'.$rows->invoice_booking_id.'/all')}}" target="_blank"> {{trans('common.order_id')}}:#{{$rows->invoice_booking_id}} </a> |
                                                   <a href="{{url('booking/backend/show/invoice/'.$rows->invoice_id)}}" target="_blank"> {{trans('common.invoice_no')}}:#{{$rows->invoice_id}}</a>
                                               </td>
                                               <td><strong>{{$rows->invoice_amount>0?$rows->currency_symbol.number_format($rows->invoice_amount):'-'}}</strong></td>
                                               <td><strong>{{$rows->payment_amount>0?$rows->currency_symbol.number_format($rows->payment_amount):'-'}}</strong></td>
                                               {{--<td>{{$rows->payment_option=='1'?'Credit Card':'Bank'}}</td>--}}
                                               <td><strong>{{$rows->payment_refund>0?$rows->currency_symbol.number_format($rows->payment_refund):'-'}}</strong></td>
                                               <td><strong>{{$diferent>0?$rows->currency_symbol.number_format($diferent):'-'}}</strong></td>

                                           </tr>
                                           <?php
                                            $totals+=$rows->invoice_amount;
                                            $payment_amount+=$rows->payment_amount;
                                            $refund+=$rows->payment_refund;
                                            $diferenttotal+=$diferent;
                                           ?>
                                       @endforeach
                                   </tbody>
                                   <tfoot>
                                   <tr class="bg-success">
                                       <th colspan="2"><strong>{{trans('common.totals')}}</strong></th>
                                       <th><strong>{{$rows->currency_symbol}}{{number_format($totals)}}</strong></th>
                                       <th><strong>{{$rows->currency_symbol}}{{number_format($payment_amount)}}</strong></th>
                                       <th><strong>{{$rows->currency_symbol}}{{number_format($refund)}}</strong></th>
                                       <th><strong>{{$rows->currency_symbol}}{{number_format($diferenttotal)}}</strong></th>
                                   </tr>
                                   </tfoot>
                               </table>
                                @endif
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <?php $Order_balance=array()?>
                        <div class="tab-pane" id="tab_2">

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <table id="table-data" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>{{trans('common.date')}}</th>
                                    <th>{{trans('common.description')}}</th>

                                    <th>{{trans('common.withdraw_money')}}</th>
                                    {{--<th>{{trans('common.payment_methods')}}</th>--}}
                                    <th>{{trans('common.refund')}}</th>

                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                            like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                        <div class="tab-pane" id="tab_5">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                            like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>

            </div>
            </div>
            </div>
            <!-- /.col -->
        </div>
    </section>

    <script>

//        $('body').on('click','.tab-pane',function () {
//            alert('test');
//        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            var date=$('#reservation').val();
            $('#tabCurrent').val(target);

            $('#preload2').show();

            if(target=='#tab_1') {
                $('#tabType').val('all');
                $.ajax({
                    url:'{{URL::to('ajax/seller_balance/list')}}',
                    type:'get',
                    data:{'date':date,'type':'all'},
                    success:function (data) {
                        $(target).html(data);
                        $('#preload2').hide();
                    }
                });
            }else if(target=='#tab_2'){
                $('#tabType').val('order');
                $.ajax({
                    url:'{{URL::to('ajax/seller_balance/list')}}',
                    type:'get',
                    data:{'date':date,'type':'order'},
                    success:function (data) {
                        $(target).html(data);
                        $('#preload2').hide();
                    }
                });
            }
        });

        $('body').on('apply.daterangepicker','#reservation',function (e) {
            $.ajax({
                url:'{{URL::to('ajax/seller_balance/list')}}',
                type:'get',
                data:{'date':$(this).val(),'type':$('#tabType').val()},
                success:function (data) {
                    $($('#tabCurrent').val()).html(data);
                    $('#preload2').hide();
                }
            });
        });
    </script>

@endsection