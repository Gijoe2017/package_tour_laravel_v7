@extends('layouts.member.layout_master_new')

@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-bank"></i> {{trans('common.bank_account')}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('member/dashboard/'.Session::get('timeline'))}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('wallet/bank_account')}}"> Bank List.</a></li>
            <li><a href="#"> Bank Create.</a></li>
        </ol>
    </section>
@endsection


@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 35px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 35px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 20px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 20px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
        hr.gray {
            border-top: 1px dashed  #5a7b8c;
        }
    </style>


    <section class="content">
        <div class="row">
            <div class="panel">
            <div class="panel-heading ">
                <h2>Form create bank account.</h2>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <!-- The time line -->
                <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('WalletController@bank_store')}}" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="box box-info">
                        <div class="box-body">
                                @if(!isset($errors))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><strong>{{trans('common.country')}} <span class="text-red">*</span></strong></label><BR>
                                        <select class="form-control select2" name="country" required>
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($Country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>{{trans('common.bank_name')}} <span class="text-red">*</span></strong></label><BR>
                                            <select class="form-control select2" name="bank_id" required>
                                                <option value="">{{trans('common.choose')}}</option>
                                                @foreach($Banks as $rows)
                                                    <option value="{{$rows->bank_code}}">{{$rows->bank_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>{{trans('common.bank_account_number')}} <span class="text-red">*</span></strong></label><BR>
                                            <input type="text" class="form-control" name="bank_account_number" id="bank_account_number" required="required">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>{{trans('common.sub_bank')}} <span class="text-red">*</span></strong></label><BR>
                                            <input type="text" class="form-control" name="sub_bank" id="sub_bank" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>{{trans('common.account_name')}} <span class="text-red">*</span></strong></label><BR>
                                            <input type="text" class="form-control" name="account_name" id="account_name" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>{{trans('common.account_id_card')}} <span class="text-red">*</span></strong></label><BR>
                                            <input type="text" class="form-control" name="account_id_card" id="account_id_card" data-inputmask='"mask": "9-9999-99999-99-9"' data-mask required="required">
                                            <span class="text-danger" id="error_id_card"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><BR></label><BR>
                                            <label class="radio-inline">
                                                <div class="material-switch">
                                                    <input class="switch-rate"  id="bank_default" name="bank_default" type="checkbox" value="1" />
                                                    <label for="bank_default" class="label-info"></label>
                                                    <span id="bank_default" style="padding-left: 15px"><strong>{{trans('common.bank_default')}}</strong></span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>


                                <div class="col-lg-12">
                                    <hr>
                                    <a href="{{url('wallet/bank_account')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                    <button type="submit" id="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
            </div>
            <!-- /.col -->
        </div>
    </section>

    <script>
        function validateThaiCitizenID(id) {
            if(
                id.length != 13 ||
                id.charAt(0).match(/[09]/)
            ) return false;

            var sum = 0;
            for( i=0; i < 12; i++ ){
                sum += parseInt(id.charAt(i))*(13-i);
            }

            if( (11 - sum%11 )%10 != parseInt(id.charAt(12)) ){
                return false;
            }
            return true;
        }

        $('body').on('keypress','#account_id_card',function () {
            var Thisnumber=$(this).val();
                iPID = Thisnumber.replace(/-/g, "");
                data=validateThaiCitizenID(iPID);
                $('#error_id_card').html('');
                $('#submit').prop('disabled', false);
                if(data==false){
                    $('#error_id_card').html('ระบุหมายเลขประจำตัวประชาชนไม่ถูกต้อง');
                    $('#submit').prop('disabled', true);
                }
        });

        $('body').on('blur','#account_id_card',function () {
                var Thisnumber=$(this).val();
                iPID = Thisnumber.replace(/-/g, "");
                data=validateThaiCitizenID(iPID);
                $('#error_id_card').html('');
                $('#submit').prop('disabled', false);
                if(data==false){
                    $('#error_id_card').html('ระบุหมายเลขประจำตัวประชาชนไม่ถูกต้อง');
                    $(this).select();
                    $('#submit').prop('disabled', true);
                }
        });
    </script>

@endsection