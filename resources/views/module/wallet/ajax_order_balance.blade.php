
    @if($Seller_balance)
        <table id="table-data" class="table table-hover">
            <thead>
            <tr>
                <th>{{trans('common.date')}}</th>
                <th>{{trans('common.description')}}</th>
                <th>{{trans('common.service_fee_upon_receipt')}}</th>
                <th>{{trans('common.accept_payment')}}</th>
                {{--<th>{{trans('common.payment_methods')}}</th>--}}
                <th>{{trans('common.refund')}}</th>
                <th>{{trans('common.difference')}}</th>
            </tr>
            </thead>
            <tbody>
            <?php $totals=0;$refund=0;$payment_amount=0;$diferenttotal=0;?>
            @foreach($Seller_balance as $rows)
                <?php
                $diferent=0;
                if($rows->payment_amount>$rows->payment_refund){
                    $diferent=$rows->payment_amount-$rows->payment_refund;
                }
                ?>
                <tr>
                    <td>{{date('m/d/Y',strtotime($rows->payment_date))}} {{$rows->payment_time}}</td>
                    <td>
                        <strong>{{$rows->invoice_type=='1'?trans('common.tour_deposit'):trans('common.tour_balance')}}</strong><BR>
                        <a href="{{url('booking/backend/show/invoice/'.$rows->invoice_booking_id.'/all')}}" target="_blank"> {{trans('common.order_id')}}:#{{$rows->invoice_booking_id}} </a> |
                        <a href="{{url('booking/backend/show/invoice/'.$rows->invoice_id)}}" target="_blank"> {{trans('common.invoice_no')}}:#{{$rows->invoice_id}}</a>
                    </td>
                    <td><strong>{{$rows->invoice_amount>0?$rows->currency_symbol.number_format($rows->invoice_amount):'-'}}</strong></td>
                    <td><strong>{{$rows->payment_amount>0?$rows->currency_symbol.number_format($rows->payment_amount):'-'}}</strong></td>
                    {{--<td>{{$rows->payment_option=='1'?'Credit Card':'Bank'}}</td>--}}
                    <td><strong>{{$rows->payment_refund>0?$rows->currency_symbol.number_format($rows->payment_refund):'-'}}</strong></td>
                    <td><strong>{{$diferent>0?$rows->currency_symbol.number_format($diferent):'-'}}</strong></td>

                </tr>
                <?php
                $totals+=$rows->invoice_amount;
                $payment_amount+=$rows->payment_amount;
                $refund+=$rows->payment_refund;
                $diferenttotal+=$diferent;
                ?>
            @endforeach
            </tbody>
            <tfoot>
            <tr class="bg-success">
                <th colspan="2"><strong>{{trans('common.totals')}}</strong></th>
                <th><strong>{{$rows->currency_symbol}}{{number_format($totals)}}</strong></th>
                <th><strong>{{$rows->currency_symbol}}{{number_format($payment_amount)}}</strong></th>
                <th><strong>{{$rows->currency_symbol}}{{number_format($refund)}}</strong></th>
                <th><strong>{{$rows->currency_symbol}}{{number_format($diferenttotal)}}</strong></th>
            </tr>
            </tfoot>
        </table>
    @endif
