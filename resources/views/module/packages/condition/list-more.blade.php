@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 text-center-xs">
            <h3><span><i class="fa fa-paint-brush"></i> {{$Package->packageID.':'.$Package->packageName}} </span></h3>
        </div>

    </section>
@endsection

@section('content')

    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        #show_status {
            position:absolute;
            top: 25%;
            left: 50%;

            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
            z-index: 100;
        }

    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                    <div class="box-body">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <form role="form"  action="{{action('Package\ConditionController@setCondition')}}" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                                        <div class="row">
                                            <div class="col-md-8">
                                                <span style="font-size: 2em; margin-left: 15px"> {{$Condition_group->condition_group_title}}</span>
                                            </div>
                                            <div class="col-md-4" align="right">
                                                <a href="{{url('package/condition/create/'.$Condition_group->condition_group_id)}}" class="btn btn-info"><i class="fa fa-plus"></i> {{trans('common.create_new_condition')}} </a>
                                            </div>
                                        </div>
                                        <div class="content40"></div>

                                        <div id='show_status' style="display:none; " class="alert alert-success">Update Success</div>
                                        @if($Condition->count() || $ConditionIn->count())

                                            <table class="table">
                                                <tbody> <?php $i=1;$group=''?>
                                                <tr class="bg-info">
                                                    <td>
                                                        <div class="material-switch pull-left">
                                                        <input type="checkbox" class="anchor-from" id="anchor-from"/>
                                                            <label for="anchor-from" class="label-success"></label>
                                                        </div>
                                                    </td>
                                                    <td colspan="4">
                                                        <span id="checkbox-title">{{trans('common.selectall')}}</span>
                                                    </td>
                                                </tr>

                                                @foreach($ConditionIn as $rows)
                                                    <input type="hidden" id="timeline_id" value="{{$rows->timeline_id}}">
                                                    <?php
                                                    $CheckUse1=DB::table('condition_in_package_details')->where('condition_id',$rows->condition_code)->count();
                                                    $CheckUse=DB::table('condition_in_package_details')
                                                        ->where('condition_id',$rows->condition_code)
                                                        ->where('packageID',Session::get('package'))
                                                        ->count();

                                                    $Sub=DB::table('condition_deposit_operation as a')
                                                        ->join('operation_sub as b','b.operation_sub_id','=','a.operation_sub_id')
                                                        ->where('a.condition_code',$rows->condition_code)
                                                        ->where('b.language_code',Auth::user()->language)
                                                        ->get();
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <div class="material-switch pull-left">
                                                                <input type="checkbox" id="chk{{$rows->condition_code}}" class="checkall chk_condition" name="conidtion_id[]" data-id="{{$rows->condition_code}}" value="{{$rows->condition_code}}" {{$CheckUse>0?'checked':''}}>
                                                                <label for="chk{{$rows->condition_code}}" class="label-success"></label>
                                                            </div>
                                                        </td>
                                                        <td><div class="text-left">
                                                                {!! $rows->condition_title !!}<BR>
                                                                <span class="text-blue">

                                                                @foreach ($Sub as $rowSub)
                                                                <?php $day=$rowSub->number_of_day;
                                                                $operator_title=str_replace('x',$day,$rowSub->operator_title);
                                                                ?>
                                                                @if($day>0)
                                                                {{$operator_title}} </br>
                                                                    @endif
                                                                    @endforeach
                                                                    @if($rows->formula_id!='' && $rows->formula_id>0 )
                                                                        <small class="text-red"> {{trans('common.condition_auto_formula')}}</small>
                                                                    @endif
                                                                       </span>

                                                            </div></td>
                                                        <td align="right" style="min-width: 180px">
                                                            {{--@if($rows->use_operator=='Y')--}}
                                                            {{--<a class="btn btn-sm btn-info" href="#" class="btn btn-sm"><i class="fa fa-calculator"></i> Formura</a>--}}
                                                            {{--@endif--}}
                                                            {{--@if($CheckUse1==0)--}}
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/condition/begin_edit/'.$rows->condition_code)}}" class="btn btn-sm"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                            <a class="btn btn-sm btn-danger" href="{{url('package/condition/begin_delete/'.$rows->condition_code)}}" class="btn btn-sm"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                            {{--@else--}}
                                                            {{--<label class="text-red">{{trans('common.condition_is_used')}}</label>--}}
                                                            {{--@endif--}}
                                                        </td>
                                                    </tr>
                                                    <?php $group=$rows->condition_group_id;?>
                                                @endforeach


                                                @foreach($Condition as $rows)
                                                    <input type="hidden" id="timeline_id" value="{{$rows->timeline_id}}">
                                                    <?php
                                                        $CheckUse1=DB::table('condition_in_package_details')->where('condition_id',$rows->condition_code)->count();
                                                        $CheckUse=DB::table('condition_in_package_details')
                                                            ->where('condition_id',$rows->condition_code)
                                                            ->where('packageID',Session::get('package'))
                                                            ->count();

                                                        $Sub=DB::table('condition_deposit_operation as a')
                                                            ->join('operation_sub as b','b.operation_sub_id','=','a.operation_sub_id')
                                                            ->where('a.condition_code',$rows->condition_code)
                                                            ->where('b.language_code',Auth::user()->language)
                                                            ->get();
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <div class="material-switch pull-left">
                                                                <input type="checkbox" id="chk{{$rows->condition_code}}" class="checkall chk_condition" name="conidtion_id[]" data-id="{{$rows->condition_code}}" value="{{$rows->condition_code}}" {{$CheckUse>0?'checked':''}}>
                                                                <label for="chk{{$rows->condition_code}}" class="label-success"></label>
                                                            </div>
                                                        </td>
                                                        <td><div class="text-left">
                                                                {!! $rows->condition_title !!}<BR>
                                                                <span class="text-blue">

                                                                @foreach ($Sub as $rowSub)
                                                                    <?php $day=$rowSub->number_of_day;
                                                                    $operator_title=str_replace('x',$day,$rowSub->operator_title);
                                                                    ?>
                                                                        @if($day>0)
                                                                    {{$operator_title}} </br>
                                                                    @endif
                                                               @endforeach
                                                                    @if($rows->formula_id!='')
                                                                       <small class="text-red"> {{trans('common.condition_auto_formula')}}</small>
                                                                    @endif
                                                                       </span>

                                                            </div></td>
                                                        <td align="right" style="min-width: 180px">
                                                            {{--@if($rows->use_operator=='Y')--}}
                                                            {{--<a class="btn btn-sm btn-info" href="#" class="btn btn-sm"><i class="fa fa-calculator"></i> Formura</a>--}}
                                                            {{--@endif--}}
                                                            {{--@if($CheckUse1==0)--}}
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/condition/begin_edit/'.$rows->condition_code)}}" class="btn btn-sm"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                            <a class="btn btn-sm btn-danger" href="{{url('package/condition/begin_delete/'.$rows->condition_code)}}" class="btn btn-sm"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                            {{--@else--}}
                                                                {{--<label class="text-red">{{trans('common.condition_is_used')}}</label>--}}
                                                            {{--@endif--}}
                                                        </td>
                                                    </tr>
                                                    <?php $group=$rows->condition_group_id;?>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning">!ยังไม่มีการเขียนเงื่อนไข</div>
                                        @endif

                                    </form>
                                    </div>
                                <br>
                                <div class="col-md-12">
                                    <hr>
                                    @if(Session::get('event')=='details')
                                        <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default"><i  class="fa fa-arrow-circle-left"></i> {{trans('common.back_to_list')}}</a>
                                    @elseif(Session::has('checking'))
                                        <a href="{{url('package/details_check/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                                    @else
                                    <a href="{{url('package/condition/list')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                                @endif
                                </div>
                            </div>
                           </div>
                        </div>
            </div>
        </div>
    </div>
    <!-- include summernote -->
    {{--<script>--}}
        {{--$( function() {--}}
            {{--$( "#sortable" ).sortable();--}}
        {{--//    $( "#sortable" ).disableSelection();--}}
        {{--} );--}}
    {{--</script>--}}
    <script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>
    <script>
        $("#anchor-from").change(function(){

            if($('#anchor-from').is(':checked')){
                var status='y';
                $('#checkbox-title').html('{{trans('common.un_selectall')}}');
                $(".checkall").attr("disabled", false);
                $(".checkall").prop("checked", true);

            }else{
                var status='n';
                $('#checkbox-title').html('{{trans('common.selectall')}}');
                $(".checkall").attr("disabled", true);
                $(".checkall").prop("checked", false);
            }
            var timeline_id=$('#timeline_id').val();
           // alert(timeline_id);
            $.ajax({
                type:'get',
                url:SP_source() + 'package/ajax/condition/status/all',
                data:{'status':status,'timeline_id':timeline_id},
                success:function (data) {
                    $('#show_status').html(data);
                    $('#show_status').show();
                    $("#show_status").fadeOut(2000);
                }
            });
        });
    </script>
@stop()


