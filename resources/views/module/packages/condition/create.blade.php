@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Package Condition. </span></h2>
        </div>
    </section>
@endsection
@section('content')

    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <form role="form"  action="{{action('Package\ConditionController@saveCondition')}}" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-heading">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>{{trans('package.Condition_group')}} * </label><BR>
                                                <select class="form-control select2" name="condition_group_id" id="condition_group_id" required>
                                                    <option value="">{{trans('LProfile.Choose')}}</option>
                                                    @foreach($Condition_group as $rows)
                                                        <option value="{{$rows->condition_group_id}}">{{$rows->condition_group_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="material-switch pull-left">
                                                <input id="use_operator" name="use_operator" type="checkbox" value="Y"/>
                                                <label for="use_operator" class="label-success"></label> <span><strong>&nbsp; เลือกต้องการใส่สูตร</strong></span>
                                            </div>
                                            {{--<div class="checkbox-inline" style="font-size: 16px">--}}
                                                {{--<input type="checkbox"  name="use_operator" id="use_operator" value="Y"> <label>&nbsp; เลือกต้องการใส่สูตร</label>--}}
                                            {{--</div>--}}
                                        </div>
                                        </div>
                                        <div class="col-lg-12">
                                         <hr>
                                        <div class="form-group not-use-operator">
                                            <label><strong>{{trans('package.Condition')}} *</strong> </label><BR>
                                            <textarea class="form-control summernote" name="condition_title" id="condition_title" ></textarea>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <!-- /.modal-content -->
                                    </div>

                                    <div class="col-lg-12 use-operator" style="display: none">
                                        <label><i class="fa fa-calculator"></i> สูตรทางคณิตศาสตร์ *</label><hr>
                                        <div class="form-group col-md-6">
                                            <label><strong>{{trans('package.Condition')}} *</strong> </label><BR>
                                            <select class="form-control" style="width: 100%" name="operator_code" id="operator_code" >
                                                <option value="">{{trans('LProfile.Choose')}}</option>
                                                @foreach($Formula as $rows)
                                                    <option value="{{$rows->operator_code}}">{{$rows->operator_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="deposit form-group col-md-3">
                                            <label><strong>ค่ามัดจำ *</strong> </label><BR>
                                            <input type="text" class="form-control" name="deposit" id="deposit"  >
                                        </div>
                                        <div class="visa form-group col-md-3">
                                            <label id="visa_expire"><strong>วัน *</strong> </label><BR>
                                            <input type="text" class="form-control" name="visa_expiry_date" id="visa_expiry_date"  >
                                        </div>

                                        <div class="not-deposit">
                                        <div class="form-group col-md-3">
                                            <label id='date_start'><strong>วันเริ่ม *</strong> </label><BR>
                                            <input type="number" class="form-control" name="condition_left" id="condition_left"   >
                                        </div>

                                        <div class="form-group col-md-3 between" style="display: none">
                                            <label id='date_end'><strong>วันสิ้นสุด *</strong> </label><BR>
                                            <input type="number" class="form-control" name="condition_right" id="condition_right"   >
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="material-switch pull-left">
                                                    <input type="checkbox"  name="keep_value_deposit" id="keep_value_deposit" value="Y" checked >
                                                    <label for="keep_value_deposit" class="label-info"></label><span><strong>&nbsp; {{trans('common.keep_value_deposit')}}</strong> </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="material-switch pull-left">
                                                    <input type="checkbox" name="keep_value_tour" id="keep_value_tour" value="Y"  >
                                                    <label for="keep_value_tour" class="label-info"></label> <span><strong>&nbsp;{{trans('common.keep_value_tour')}}</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="material-switch pull-left">
                                                <input type="checkbox"  name="keep_all_costs" id="keep_all_costs" value="Y"  >
                                                    <label for="keep_all_costs" class="label-success"></label> <span><strong>&nbsp;{{trans('common.keep_all_costs')}}</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="keep_value_deposit">
                                                <div class="form-group col-md-6">
                                                    <label><strong>ค่ามัดจำ *</strong> </label><BR>
                                                    <input type="number" class="form-control" name="value_deposit" id="value_deposit"  value="{{isset($Deposit->value_deposit)?$Deposit->value_deposit:''}}" >
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label><strong>หน่วย *</strong> </label><BR>
                                                    <select class="form-control select2" style="width: 100%" name="condition_unit" id="condition_unit" >
                                                        <option value="">{{trans('LProfile.Choose')}}</option>
                                                        <option value="%" {{isset($Deposit->unit_deposit)=='%'?'selected':''}}>%</option>
                                                        <option value="{{$Package->packageCurrency}}" {{isset($Deposit->unit_deposit)==$Package->packageCurrency?'selected':''}}>{{$Package->packageCurrency}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="keep_value_tour" style="display: none">
                                                <div class="form-group col-md-6">
                                                    <label><strong>ค่าเดินทาง *</strong> </label><BR>
                                                    <input type="number" class="form-control" name="value_tour" id="value_tour"  >
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label><strong>หน่วย *</strong> </label><BR>
                                                    <select class="form-control select2" style="width: 100%" name="unit_value_tour" id="unit_value_tour" >
                                                        <option value="">{{trans('LProfile.Choose')}}</option>
                                                        <option value="%">%</option>
                                                        <option value="{{$Package->packageCurrency}}">{{$Package->packageCurrency}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

                                    </div>
                                    <div class="col-lg-12">
                                     <hr>
                                     <a href="{{url('package/condition/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                                     <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                    </div>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>


@stop()
