@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Package Detail.</span></h2>
        </div>
    </section>
@endsection

@section('content')
    {{--formhelpers--}}
    <link href="{{asset('public/assets/css/bootstrap-formhelpers.min.css')}}" rel='stylesheet' type='text/css' />
    <script src="{{asset('public/assets/js/bootstrap-formhelpers.min.js')}}"></script>

    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                    <div class="box-body">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <form role="form"  action="{{action('Package\ConditionController@setCondition')}}" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                                        <div class="row">
                                            <div class="col-md-8">
                                                <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-question"></i> เงื่อนไขโปรแกรมทัวร์</span>
                                            </div>
                                            <div class="col-md-4" align="right">
                                                <a href="{{url('package/condition/create')}}" class="btn btn-info"><i class="fa fa-plus"></i>  Add Condition </a>
                                            </div>
                                        </div>
                                        <div class="content40"></div>
                                        @if(isset($Condition))
                                            <table class="table">

                                                <tbody> <?php $i=1;$group=''?>
                                                @foreach($Condition as $rows)
                                                    <?php
                                                  //  dd($rows);
                                                    $CheckUse1=DB::table('condition_in_package_details')->where('condition_id',$rows->condition_code)->count();
                                                        $CheckUse=DB::table('condition_in_package_details')
                                                            ->where('condition_id',$rows->condition_code)
                                                            ->where('packageID',Session::get('package'))
                                                            ->count();

                                                    ?>
                                                    @if($rows->condition_group_id!=$group)
                                                    <tr><th colspan="4">{{$rows->condition_group_title}}</th></tr>
                                                    @endif
                                                    <tr>
                                                        <td><input type="checkbox" name="conidtion_id[]" value="{{$rows->condition_code}}" {{$CheckUse>0?'checked':''}}></td>
                                                        <td><div class="text-left"> {!! $rows->condition_title !!}</div></td>

                                                        <td align="right" style="min-width: 180px">
                                                            {{--@if($rows->use_operator=='Y')--}}
                                                            {{--<a class="btn btn-sm btn-info" href="#" class="btn btn-sm"><i class="fa fa-calculator"></i> Formura</a>--}}
                                                            {{--@endif--}}
                                                            {{--@if($CheckUse1==0)--}}
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/condition/begin_edit/'.$rows->id)}}" class="btn btn-sm"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                            <a class="btn btn-sm btn-danger" href="{{url('package/condition/begin_delete/'.$rows->id)}}" class="btn btn-sm"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                            {{--@else--}}
                                                                {{--<label class="text-red">{{trans('common.condition_is_used')}}</label>--}}
                                                            {{--@endif--}}
                                                        </td>
                                                    </tr>
                                                    <?php $group=$rows->condition_group_id;?>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>!ยังไม่มีการเขียนเงื่อนไข</h2>
                                        @endif



                                    <hr>
                                        <div class="row">
                                             @if(Session::has('message'))
                                                <div class="alert alert-success">
                                                    {{Session::get('message')}}
                                                </div>
                                             @endif
                                            @if($Condition->count()>0)
                                    <div class="col-md-9">
                                    <label>เลือก <input type="checkbox" checked> หน้าเงื่อนไขที่ต้องการใช้ในแพ็คเกจทัวร์นี้แล้วกดปุ่ม ยืนยันการเซ็ตเงื่อนไข </label>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> ยืนยันการเซ็ตเงื่อนไข </button>
                                    </div>
                                                @endif
                                        </div>
                                    </form>
                                    </div>

                                <br>
                                <div class="col-md-12">
                                    <hr>
                                    <a href="{{url('package/details')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                                </div>
                            </div>
                           </div>
                        </div>
            </div>
        </div>
    </div>





@stop()


