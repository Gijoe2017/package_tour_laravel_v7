@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 text-center-xs">
            <h3><span><i class="fa fa-paint-brush"></i> {{$Package->packageID.':'.$Package->packageName}} </span></h3>
        </div>
    </section>
@endsection


@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        #show_status {
            position:absolute;
            top: 25%;
            left: 50%;

            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
           z-index: 100;
        }

    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                    <div class="box-body">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span style="font-size: 2em; margin-left: 15px"> {{trans('common.package_condition_details')}}.</span>
                                        </div>
                                    </div>
                                    <div id='show_status' style="display:none; " class="alert alert-success">Update Success</div>
                                    <table class="table">
                                    <?php $i=1?>

                                    @foreach($Condition_group as $rows)
                                        <tr class="bg-info">

                                            <th><h4>{{$i++}}. {{$rows->condition_group_title}}</h4></th>
                                            <th>
                                                <a class="btn  btn-success pull-right" href="{{url('/package/condition/list_more/'.$rows->condition_group_id)}}" ><i class="fa fa-check-circle"></i> {{trans('common.add').' / '.trans('common.edit')}}</a>
                                            </th>
                                        </tr>
                                        <?php
                                        $packageID=$Package->packageID;
                                        $Condition=DB::table('package_condition as a')
                                            ->join('condition_group as b','b.condition_group_id','=','a.condition_group_id')
                                            ->join('condition_in_package_details as c','c.condition_id','=','a.condition_code')
                                            ->select('a.*','b.condition_group_title','c.status as conditionStatus','c.id as condition_id')
                                            ->where('a.condition_group_id',$rows->condition_group_id)
                                            ->whereIn('a.condition_code',function ($query) use($packageID){
                                                $query->select('condition_id')->from('condition_in_package_details')
                                                    ->where('packageID',$packageID);
                                            })
                                            ->where('a.timeline_id',$timeline_id)
                                            ->where('a.language_code',Auth::user()->language)
                                            ->where('b.language_code',Auth::user()->language)
                                            ->groupby('id')
                                            ->orderby('a.condition_group_id','asc')
                                            ->get();
                                      //  echo $timeline_id;
                                       // dd($Condition);
                                        ?>
                                        @if($Condition->count())
                                        <tr>
                                            <td colspan="2"><ul>
                                                @foreach($Condition as $rowCon)
                                                    <li>
                                                        <?php
                                                        $Sub=DB::table('condition_deposit_operation as a')
                                                            ->join('operation_sub as b','b.operation_sub_id','=','a.operation_sub_id')
                                                            ->where('a.condition_code',$rowCon->condition_code)
                                                            ->where('b.language_code',Auth::user()->language)
                                                            ->get();
                                                        ?>
                                                            {!!$rowCon->condition_title !!}
                                                            <span class="text-blue">
                                                                @foreach ($Sub as $rowSub)
                                                                    <?php $day=$rowSub->number_of_day;
                                                                    $operator_title=str_replace('x',$day,$rowSub->operator_title);
                                                                    ?>
                                                                    @if($day>0)
                                                                      {{$operator_title}} </br>
                                                                    @endif
                                                                @endforeach
                                                                @if($rowCon->formula_id!='' && $rowCon->formula_id>0 )
                                                                      <small class="text-red"> {{trans('common.condition_auto_formula')}}</small>
                                                                @endif
                                                            </span>
                                                    </li>
                                                @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </table>
                                </div>

                                <br>
                                <div class="col-md-12">
                                    <hr>
                                    <a href="{{url('package/details')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                                </div>
                            </div>
                           </div>
                        </div>
            </div>
        </div>
    </div>





@stop()


