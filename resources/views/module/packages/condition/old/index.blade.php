@extends('layouts.member.layout_master_new')

@section('content')
    {{--formhelpers--}}
    <link href="{{asset('public/assets/css/bootstrap-formhelpers.min.css')}}" rel='stylesheet' type='text/css' />
    <script src="{{asset('public/assets/js/bootstrap-formhelpers.min.js')}}"></script>

    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">

                    <div class="box-body">


                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="row">


                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-question"></i> เงื่อนไขโปรแกรมทัวร์</span>
                                                    </div>
                                                    <div class="col-md-4" align="right">
                                                        <a data-toggle="modal" data-target="#modal-review" class="btn btn-danger"><i class="fa fa-plus"></i>  Add Package Condition </a>
                                                    </div>
                                                </div>


                                        <div class="content40"><hr></div>
                                        @if(isset($Condition))
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    {{--<th>รหัสเงื่อนไข</th>--}}
                                                    <th>เงื่อนไข</th>
                                                    <th>รายละเอียด</th>
                                                    <th><div align="center">ดำเนินการ</div> </th>
                                                </tr>
                                                </thead>
                                                <tbody> <?php $i=1?>
                                                @foreach($Condition as $rows)
                                                    <tr>
                                                        {{--<td>{{$rows->conditionCode}}</td>--}}
                                                        <td>{{$rows->packageCondition}}</td>
                                                        <td>
                                                            {!! str_limit($rows->packageConditionDesc,90) !!}
                                                        </td>
                                                        <td align="right">
                                                            <a class="btn btn-sm btn-info" href="{{url('package/condition/copy/'.$rows->ID)}}" class="btn btn-sm"><i class="fa fa-copy"></i> Copy</a>
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/condition/edit/'.$rows->ID)}}" class="btn btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                            <a class="btn btn-sm btn-danger" href="{{url('package/condition/del/'.$rows->ID)}}" class="btn btn-sm" onclick="return confirmDel()"><i class="fa fa-trash"></i> Delete</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>!ยังไม่มีการเขียนเงื่อนไข</h2>
                                        @endif


                                        </div>
                                    </div>
                                <br>
                                <div class="col-md-12">
                                    <a href="{{url('package/details')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                                </div>
                            </div>
                           </div>
                        </div>
            </div>
        </div>
    </div>



    <!-- Modal review start -->
    <div class="modal  fade" id="modal-review" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <form role="form"  action="{{action('Package\ConditionController@saveCondition')}}" enctype="multipart/form-data" method="post" novalidate>
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
                        <h3 class="modal-title-site text-center"> เงื่อนไขโปรแกรมทัวร์  </h3>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label><strong>{{trans('package.Condition')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <input type="text" class="form-control" name="packageCondition" id="packageCondition" >
                            </div>
                            <div class="form-group">
                                <label><strong>{{trans('package.ConditionDesc')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <textarea type="text" class="form-control" name="packageConditionDesc" id="packageConditionDesc" ></textarea>
                            </div>
                        </div>
                        {{--<div class="col-lg-4">--}}

                            {{--<div class="text-center">--}}
                                {{--<img src="{{asset('public/images/package-tour/mid/default-user.png')}}" id="blah" class="img-thumbnail">--}}
                            {{--</div>--}}

                            {{--<BR>--}}
                            {{--<div class="form-group">--}}
                                {{--<input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                    </div>


                </div>
                <!-- /.modal-content -->
            </form>
        </div>

        <!-- /.modal-dialog -->

    </div>
    <!-- /.Modal review -->


@stop()


