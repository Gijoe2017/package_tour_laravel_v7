@extends('layouts.package.master-package-info')
@section('contents')
    <!-- Placed js at the end of the document so the pages load faster -->

    <style type="text/css">
        .condition input[type="text"], input[type="password"], input[type="email"] {
            border-color: #DDDDDD;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            border-style: solid;
            border-width: 1px;
            color: #888888;
            font-size: 14px;
            /* margin-bottom: 10px; */
            /* height: 36px; */
        }
        div.savestatus{ /* Style for the "Saving Form Contents" DIV that is shown at the top of the form */
            width:200px;
            padding:6px 10px;
            /*border:1px solid gray;*/
            background:#27CCE4;
            /*-webkit-box-shadow: 0 0 8px #818181 ;*/
            /*box-shadow: 0 0 8px #818181;*/
            color:#FFF;
            font-size: small;
            position:absolute;
            top:100px;
            margin-left: 30%;
        }


        form#AutoForm div{ /*CSS used by demo form*/
            margin-bottom:9px;
        }
    </style>

    <div class="row">
        <div class="2">
            <div class="box box-info">
                <div class="box-body">
                    @if (isset($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form"  id="AutoForm" action="{{action('Package\ConditionController@SaveFormCondition')}}" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="row" style="margin-top: 60px">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
                                            <h1 class="section-title-inner" style="margin-top: 12px;"><span><a href="{{Session::get('userURL')?url('work-of-art/'.Session::get('userURL')):url('/'.Auth::user()->id)}}"><i class="fa fa-paint-brush"></i> PackageTour </a></span></h1>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-6 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
                                            @if(isset($Default))
                                                @if($Default->packageStatus=='D')
                                                    <h4 class="caps"> <a href="{{url('editWork-Of-Art/'.$Default->packageID.'/'.$Default->packageBy)}}"><lable class="text-danger"><i class="fa fa-edit"></i>{{trans('package.IncompleteInformation')}}</lable></a>
                                                        | <button type="submit" class="btn btn-info pull-right">{{trans('package.PostWorkOfArtNow')}}</button>
                                                    </h4>
                                                @else
                                                    <h4 class="caps"><a href="{{url('editWork-Of-Art/'.$Default->packageID.'/'.$Default->packageBy)}}"><i class="fa fa-edit"></i> {{$Default->packageName}}</a> |
                                                        <a href="{{url('Work-Of-Art-Info/'.$Default->packageID.'/'.$Default->packageBy.'/'.Session::get('Language'))}}"><lable class="text-success"><i class="fa fa-info-circle"></i> {{trans('package.InformationIsDisplayed')}}</lable></a>
                                                    </h4>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="w100 clearfix">
                                            <ul class="orderStep orderStepLook2">
                                                <li ><a href="{{url('package/setting/'.Session::get('package'))}}"> <i class="fa fa-pencil-square-o"></i> <span> {{trans('package.SettingPackage')}}</span> </a></li>
                                                <li><a href="{{url('package/info')}}"> <i class="fa fa-picture-o"></i> <span> {{trans('package.PackageInfo')}} </span></a></li>
                                                <li ><a href="{{url('package/schedule')}}"> <i class="fa fa-newspaper-o"></i> <span> {{trans('package.PackageSchedule')}} </span></a></li>
                                                <li class="active"><a href="{{url('package/details')}}"> <i class="fa fa-object-group"></i> <span> {{trans('package.PackageDetails')}} </span></a></li>
                                            </ul>
                                            <!--/.orderStep end-->
                                        </div>

                                    </div>


                                    <div class="col-lg-12">
                                        <h3>เงื่อนไขการสำรองที่นั่งและจ่ายเงิน ddd</h3>
                                            <label style="font-size: 13px">* กรุณาจองล่วงหน้าก่อนวันเดินทางอย่างน้อย   &nbsp;<input type="text" style="text-align: center" name="BookingBefore_travel" id="BookingBefore_travel"  >&nbsp; วัน </label><br>
                                            <label style="font-size: 13px">* เงินมัดจำสำรองที่นั่งคนละ  &nbsp;<input type="text" style="text-align: center" name="BookingDeposit" id="BookingDeposit"  >&nbsp; บาท ที่นั่งจะยืนยันเมื่อได้รับเงินมัดจำแล้วเท่านั้น</label><BR>
                                            <label style="font-size: 13px">* ชำระค่าทัวร์ส่วนที่เหลือก่อนเดินทางมิฉะนั้นจะถือว่าท่านยกเลิกการเดินทางโดยอัตโนมัติไม่น้อยกว่า &nbsp;<input type="text" style="text-align: center" name="PaymentBefore" id="PaymentBefore" >&nbsp; วัน</label>
                                            <?php
                                                $SubCondition=DB::table('package_sub_condition as a')
                                                        ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                                                        ->where('b.LanguageCode',$Default->packageLanguage)
                                                        ->where('a.groupCode','1')
                                                        ->where('a.packageDescID',Session::get('packageDescID'))->get();

                                             ?>
                                            @if(isset($SubCondition))
                                                @foreach($SubCondition as $rows)
                                                    <div class="alert">
                                                        <h3>{{$rows->packageCondition}}
                                                            <span style="font-size: small" class="pull-right">
                                                                <a href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a> |
                                                                <a href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </h3>
                                                    <p>{{$rows->packageConditionDesc}}</p>
                                                    </div>
                                                    @endforeach

                                            @endif
                                            <div  align="right">
                                                <a data-toggle="modal" id="Addgroup1" data-id="1" data-target="#modal-review" class="btn btn-info"><i class="fa fa-plus"></i> เงื่อนไขการสำรองที่นั่งและจ่ายเงิน (เพิ่มเต็ม) </a>
                                            </div>

                                    </div>

                                        <div class="col-lg-12">
                                            <hr>
                                            <h3>เงื่อนไขการยกเลิกการสำรองที่นั่ง</h3>
                                            <label style="font-size: 13px">* แจ้งยกเลิกก่อนเดินทาง  &nbsp; <input type="text" style="text-align: center" name="CancelBefore" id="CancelBefore" >&nbsp; วัน คืนค่าใช้จ่ายทั้งหมด</label><br>
                                            <label style="font-size: 13px">* แจ้งยกเลิกก่อนเดินทาง  &nbsp; <input type="text" style="text-align: center" name="Cancellation" id="Cancellation"  >&nbsp; วัน เก็บค่าใช้จ่ายท่านละ <input type="text" style="text-align: center" name="CancellationPrice" id="CancellationPrice" value="{{$Condition->CancellationPrice}}" > บาท</label><BR>
                                            <label style="font-size: 13px">* แจ้งยกเลิกน้อยกว่า  &nbsp; <input type="text" style="text-align: center" name="CancelSmallthan" id="CancelSmallthan" >&nbsp; วัน ก่อนการเดินทางทางบริษัทฯ ขอสงวนสิทธิ์เก็บค่าใช้จ่ายทั้งหมด</label>

                                            <?php
                                            $SubCondition=DB::table('package_sub_condition as a')
                                                    ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                                                    ->where('b.LanguageCode',$Default->packageLanguage)
                                                    ->where('a.groupCode','2')
                                                    ->where('a.packageDescID',Session::get('packageDescID'))->get();

                                            ?>
                                            @if(isset($SubCondition))
                                                @foreach($SubCondition as $rows)
                                                    <div class="alert">
                                                        <h3>{{$rows->packageCondition}}
                                                            <span style="font-size: small" class="pull-right">
                                                                <a href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a> |
                                                                <a href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </h3>
                                                        <p>{{$rows->packageConditionDesc}}</p>
                                                    </div>
                                                @endforeach

                                            @endif

                                            <div  align="right">
                                                <a data-toggle="modal" id="Addgroup2" data-id="2" data-target="#modal-review" class="btn btn-info"><i class="fa fa-plus"></i> เงื่อนไขการยกเลิกการสำรองที่นั่ง (เพิ่มเต็ม) </a>
                                            </div>

                                        </div>
                                        <div class="col-lg-12">
                                            <hr>
                                            <h3>ข้อมูลเอกสารในการของีซ่า </h3>
                                            <label style="font-size: 13px">* พาสปอร์ตจะต้องมีอายุก่อนการเดินทางไม่น้อยกว่า   &nbsp; <input type="text" style="text-align: center" name="Agepassport" id="Agepassport"  > &nbsp; วัน</label><br>

                                            <?php
                                            $SubCondition=DB::table('package_sub_condition as a')
                                                    ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                                                    ->where('b.LanguageCode',$Default->packageLanguage)
                                                    ->where('a.groupCode','3')
                                                    ->where('a.packageDescID',Session::get('packageDescID'))->get();

                                            ?>
                                            @if(isset($SubCondition))
                                                @foreach($SubCondition as $rows)
                                                    <div class="alert">
                                                        <h3>{{$rows->packageCondition}}
                                                            <span style="font-size: small" class="pull-right">
                                                                <a href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a> |
                                                                <a href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                                            </span>
                                                        </h3>
                                                        <p>{{$rows->packageConditionDesc}}</p>
                                                    </div>
                                                @endforeach

                                            @endif
                                            <div  align="right">
                                                <a data-toggle="modal" id="Addgroup3" data-id="3" data-target="#modal-review" class="btn btn-info"><i class="fa fa-plus"></i> ข้อมูลเอกสารในการของีซ่า (เพิ่มเต็ม) </a>
                                            </div>

                                        </div>


                                    <div class="clearfix"></div>
                                 <!-- /.modal-content -->
                                 </div>
                                 <hr>
                                 <a href="{{url('package/condition/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                 <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.SaveCondition')}}</button>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal review start -->
    <div class="modal  fade" id="modal-review" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form role="form"  action="{{action('Package\ConditionController@saveConditionOther')}}" enctype="multipart/form-data" method="post" novalidate>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="groupCode" id="groupCode">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
                        <h3 id="conditionTitle" class="modal-title-site text-center"> เงื่อนไขการสำรองที่นั่งและจ่ายเงิน </h3>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label><strong>{{trans('package.Condition')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <input type="text" class="form-control" name="packageCondition" id="packageCondition" >
                            </div>
                            <div class="form-group">
                                <label><strong>{{trans('package.ConditionDesc')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <textarea type="text" class="form-control" name="packageConditionDesc" id="packageConditionDesc" ></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="text-center">
                                <img src="{{asset('images/package-tour/mid/default-user.png')}}" id="blah" class="img-thumbnail">
                            </div>
                            <BR>
                            <div class="form-group">
                                <input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                    </div>


                </div>
                <!-- /.modal-content -->
            </form>
        </div>

        <!-- /.modal-dialog -->

    </div>
    <!-- /.Modal review -->


{{--<script src="{{asset('assets/js/bootstrap-filestyle.min.js')}}"></script>--}}

    <script language="javascript">


        $(document).on('click', '#Addgroup1', function(e){

            e.preventDefault();

            var groupCode = $(this).data('id');
            document.getElementById('groupCode').value=groupCode;

            document.getElementById('conditionTitle').innerHTML='เงื่อนไขการสำรองที่นั่งและจ่ายเงิน';



        });
        $(document).on('click', '#Addgroup2', function(e){

            e.preventDefault();

            var groupCode = $(this).data('id');
            document.getElementById('groupCode').value=groupCode;

            document.getElementById('conditionTitle').innerHTML='เงื่อนไขการยกเลิกการสำรองที่นั่ง';



        });
        $(document).on('click', '#Addgroup3', function(e){

            e.preventDefault();

            var groupCode = $(this).data('id');
            document.getElementById('groupCode').value=groupCode;

            document.getElementById('conditionTitle').innerHTML='ข้อมูลเอกสารในการขอวีซ่า';



        });



        $('#moreImage').on('click',function () {
            document.getElementById('showAddmore').style.display='';
        });

        var simplemde = new SimpleMDE(
                { element: document.getElementById("packageConditionDesc"),
                    toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
                });
    </script>

@stop()
