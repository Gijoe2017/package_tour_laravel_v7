@extends('layouts.package.master-package-info')

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">


                <div class="box-body">
                    @if (isset($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form"  action="{{action('Package\ConditionController@UpdateCondition')}}" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" id="id" value="{{$Condition->ID}}" >
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12 text-center-xs" style="margin-top: 60px">
                                            <h1 class="section-title-inner" style="margin-top: 12px;"><span><a href="{{Session::get('userURL')?url('work-of-art/'.Session::get('userURL')):url('/'.Auth::user()->id)}}"><i class="fa fa-paint-brush"></i> PackageTour </a></span></h1>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-6 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
                                            @if(isset($Default))
                                                @if($Default->packageStatus=='D')
                                                    <h4 class="caps"> <a href="{{url('editWork-Of-Art/'.$Default->packageID.'/'.$Default->packageBy)}}"><lable class="text-danger"><i class="fa fa-edit"></i>{{trans('package.IncompleteInformation')}}</lable></a>
                                                        | <button type="submit" class="btn btn-info pull-right">{{trans('package.PostWorkOfArtNow')}}</button>
                                                    </h4>
                                                @else
                                                    <h4 class="caps"><a href="{{url('editWork-Of-Art/'.$Default->packageID.'/'.$Default->packageBy)}}"><i class="fa fa-edit"></i> {{$Default->packageName}}</a> |
                                                        <a href="{{url('Work-Of-Art-Info/'.$Default->packageID.'/'.$Default->packageBy.'/'.Session::get('Language'))}}"><lable class="text-success"><i class="fa fa-info-circle"></i> {{trans('package.InformationIsDisplayed')}}</lable></a>
                                                    </h4>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="w100 clearfix">
                                            <ul class="orderStep orderStepLook2">
                                                <li ><a href="{{url('package/setting/'.Session::get('package'))}}"> <i class="fa fa-pencil-square-o"></i> <span> {{trans('package.SettingPackage')}}</span> </a></li>
                                                <li><a href="{{url('package/info')}}"> <i class="fa fa-picture-o"></i> <span> {{trans('package.PackageInfo')}} </span></a></li>
                                                <li ><a href="{{url('package/schedule')}}"> <i class="fa fa-newspaper-o"></i> <span> {{trans('package.PackageSchedule')}} </span></a></li>
                                                <li class="active"><a href="{{url('package/details')}}"> <i class="fa fa-object-group"></i> <span> {{trans('package.PackageDetails')}} </span></a></li>
                                            </ul>
                                            <!--/.orderStep end-->
                                        </div>

                                    </div>


                                    <div class="col-lg-8">
                                        @if(isset($Condition))
                                        <div class="form-group">
                                            <label><strong>{{trans('package.Condition')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                            <input type="text" class="form-control" name="packageCondition" id="packageCondition" value="{{$Condition->packageCondition}}" >
                                        </div>
                                        <div class="form-group">
                                            <label><strong>{{trans('package.ConditionDesc')}}*</strong></label><BR>
                                            <textarea class="form-control" name="packageConditionDesc" id="packageConditionDesc"  required >{{$Condition->packageConditionDesc}}</textarea>
                                        </div>
                                            @else
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Condition')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                                <input type="text" class="form-control" name="packageCondition" id="packageCondition"  >
                                            </div>
                                            <div class="form-group">
                                                <label><strong>{{trans('package.ConditionDesc')}}*</strong></label><BR>
                                                <textarea class="form-control" name="packageConditionDesc" id="packageConditionDesc"  required ></textarea>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-lg-4">
                                        @if(isset($ConditionImage->Image))
                                            <div class="text-center">
                                                <img src="{{asset('public/images/package-tour/small/'.$ConditionImage->Image)}}" id="blah" class="img-thumbnail">
                                            </div>
                                        @else
                                            <div class="text-center">
                                                <img src="{{asset('public/images/package-tour/mid/default-user.png')}}" id="blah" class="img-thumbnail">
                                            </div>
                                        @endif
                                        <BR>
                                        <div class="form-group">
                                            <input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">
                                        </div>

                                    </div>

                                    <div class="clearfix"></div>
                                 <!-- /.modal-content -->
                                 </div>
                                 <hr>
                                 <a href="{{url('package/condition/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Update')}}</button>

                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>
    <script language="javascript">

        $('#moreImage').on('click',function () {
            document.getElementById('showAddmore').style.display='';
        });

        var simplemde = new SimpleMDE(
                { element: document.getElementById("packageConditionDesc"),
                    toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
                });
    </script>

@stop()
