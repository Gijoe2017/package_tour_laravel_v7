@extends('layouts.member.layout_master_new')

@section('content')
    <div class="box box-info">
        <div class="box-body">
    <form role="form"  action="{{action('Package\ConditionController@SaveFormCondition')}}" enctype="multipart/form-data" method="post" novalidate>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="panel-heading">
                    <div class="col-lg-12">
                        <h3>เงื่อนไขการสำรองที่นั่งและจ่ายเงิน</h3>

                        <label style="font-size: 15px">* กรุณาจองล่วงหน้าก่อนวันเดินทางอย่างน้อย   <input type="text" style="text-align: center; width: 50px" name="BookingBefore_travel" id="BookingBefore_travel" value="{{$Condition->BookingBefore_travel}}"> วัน </label><br>
                        <label style="font-size: 15px">* เงินมัดจำสำรองที่นั่งคนละ  <input type="text" style="text-align: center;width: 150px" name="BookingDeposit" id="BookingDeposit" value="{{$Condition->BookingDeposit}}" > บาท ที่นั่งจะยืนยันเมื่อได้รับเงินมัดจำแล้วเท่านั้น</label><BR>
                        <label style="font-size: 15px">* ชำระค่าทัวร์ส่วนที่เหลือก่อนเดินทางมิฉะนั้นจะถือว่าท่านยกเลิกการเดินทางโดยอัตโนมัติไม่น้อยกว่า <input type="text" style="text-align: center;width: 50px" name="PaymentBefore" id="PaymentBefore" value="{{$Condition->PaymentBefore}}"> วัน</label>

                        <?php

                        $SubCondition=DB::table('package_sub_condition as a')
                            ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                            ->where('b.LanguageCode',$Default->packageLanguage)
                            ->where('b.groupCode','1')
                            ->where('a.packageDescID',Session::get('packageDescID'))->get();

                        ?>

                        @if(isset($SubCondition))
                            @foreach($SubCondition as $rows)
                                <div class="alert">
                                    <h3>{{$rows->packageCondition}}
                                        <span class="pull-right">
                                            <a class="btn btn-xs btn-info" href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a>
                                            <a class="btn btn-xs btn-danger" href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                        </span>
                                    </h3>
                                    <p>{{$rows->packageConditionDesc}}</p>
                                </div>
                            @endforeach

                        @endif
                        <div  align="right">
                            <a data-toggle="modal" id="Addgroup1" data-id="1" data-target="#modal-review" class="btn btn-info"><i class="fa fa-plus"></i> เงื่อนไขการสำรองที่นั่งและจ่ายเงิน (เพิ่มเต็ม) </a>
                        </div>

                    </div>

                    <div class="col-lg-12">
                        <hr>
                        <h3>เงื่อนไขการยกเลิกการสำรองที่นั่ง</h3>
                        <label style="font-size: 15px">* แจ้งยกเลิกก่อนเดินทาง   <input type="text" style="text-align: center;width: 50px" name="CancelBefore" id="CancelBefore" value="{{$Condition->PaymentBefore}}" > วัน คืนค่าใช้จ่ายทั้งหมด</label><br>
                        <label style="font-size: 15px">* แจ้งยกเลิกก่อนเดินทาง  <input type="text" style="text-align: center;width: 50px" name="Cancellation" id="Cancellation" value="{{$Condition->Cancellation}}" > วัน เก็บค่าใช้จ่ายท่านละ <input type="text" style="text-align: center;width: 150px" name="CancellationPrice" id="CancellationPrice" value="{{$Condition->CancellationPrice}}" > บาท</label><BR>
                        <label style="font-size: 15px">* แจ้งยกเลิกน้อยกว่า  <input type="text" style="text-align: center;width: 50px" name="CancelSmallthan" id="CancelSmallthan" value="{{$Condition->CancelSmallthan}}" > วัน ก่อนการเดินทางทางบริษัทฯ ขอสงวนสิทธิ์เก็บค่าใช้จ่ายทั้งหมด</label>
                        <?php
                        $SubCondition1=DB::table('package_sub_condition as a')
                            ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                            ->where('b.LanguageCode',$Default->packageLanguage)
                            ->where('b.groupCode','2')
                            ->where('a.packageDescID',Session::get('packageDescID'))->get();
                        ?>
                        @if(isset($SubCondition1))
                            @foreach($SubCondition1 as $rows)
                                <div class="alert">
                                    <h3>{{$rows->packageCondition}}
                                        <span  class="pull-right">
                                            <a class="btn btn-xs btn-info" href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a>
                                            <a class="btn btn-xs btn-danger" href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                        </span>
                                    </h3>
                                    <p>{{$rows->packageConditionDesc}}</p>
                                </div>
                            @endforeach

                        @endif

                        <div  align="right">
                            <a data-toggle="modal" id="Addgroup2" data-id="2" data-target="#modal-review" class="btn btn-info"><i class="fa fa-plus"></i> เงื่อนไขการยกเลิกการสำรองที่นั่ง (เพิ่มเต็ม) </a>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <hr>
                        <h3>ข้อมูลเอกสารในการของีซ่า </h3>
                        <label style="font-size: 15px">* พาสปอร์ตจะต้องมีอายุก่อนการเดินทางไม่น้อยกว่า   <input type="text" style="text-align: center;width: 50px" name="Agepassport" id="Agepassport" value="{{$Condition->Agepassport}}" > วัน</label><br>

                        <?php
                        $SubCondition2=DB::table('package_sub_condition as a')
                            ->join('package_condition as b','b.conditionCode','=','a.conditionCode')
                            ->where('b.LanguageCode',$Default->packageLanguage)
                            ->where('b.groupCode','3')
                            ->where('a.packageDescID',Session::get('packageDescID'))->get();

                        ?>
                        @if(isset($SubCondition2))
                            @foreach($SubCondition2 as $rows)
                                <div class="alert">
                                    <h3>{{$rows->packageCondition}}
                                        <span  class="pull-right">
                                            <a class="btn btn-xs btn-info" href="{{url('package/condition/editOther/'.$rows->conditionCode)}}"> <i class="fa fa-edit"></i> Edit </a>
                                            <a class="btn btn-xs btn-danger" href="{{url('package/condition/delOther/'.$rows->conditionCode)}}" onclick="return confirmDel()"><i class="fa fa-minus-circle"></i> Delete</a>
                                        </span>
                                    </h3>
                                    <p>{{$rows->packageConditionDesc}}</p>
                                </div>
                            @endforeach

                        @endif
                        <div  align="right">
                            <a data-toggle="modal" id="Addgroup3" data-id="3" data-target="#modal-review" class="group btn btn-info"><i class="fa fa-plus"></i> ข้อมูลเอกสารในการของีซ่า (เพิ่มเต็ม) </a>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <!-- /.modal-content -->
                </div>
                <hr>
                <a href="{{url('package/condition/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>

                @if($Condition->BookingBefore_travel>0)
                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.UpdateCondition')}}</button>

                    <a href="{{url('package/condition/list/'.Session::get('packageDescID'))}}" class="btn btn-danger"><i class="fa fa-plus"></i> {{trans('package.MoreCondition')}}</a>
                @else
                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.SaveCondition')}}</button>
                @endif
            </div>

        </div>
    </form>
        </div>
    </div>


    <!-- Modal review start -->
    <div class="modal  fade" id="modal-review" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <form role="form"  action="{{action('Package\ConditionController@saveConditionOther')}}" enctype="multipart/form-data" method="post" novalidate>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="groupCode" id="groupCode">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
                        <h3 id="conditionTitle" class="modal-title-site text-center"> เงื่อนไขการสำรองที่นั่งและจ่ายเงิน </h3>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label><strong>{{trans('package.Condition')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <input type="text" class="form-control" name="packageCondition" id="packageCondition" >
                            </div>
                            <div class="form-group">
                                <label><strong>{{trans('package.ConditionDesc')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                <textarea type="text" class="form-control" name="packageConditionDesc" id="packageConditionDesc" ></textarea>
                            </div>
                        </div>
                        {{--<div class="col-lg-4">--}}

                            {{--<div class="text-center">--}}
                                {{--<img src="{{asset('public/images/package-tour/mid/default-user.png')}}" id="blah" class="img-thumbnail">--}}
                            {{--</div>--}}

                            {{--<BR>--}}
                            {{--<div class="form-group">--}}
                                {{--<input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                    </div>


                </div>
                <!-- /.modal-content -->
            </form>
        </div>

        <!-- /.modal-dialog -->

    </div>
    <!-- /.Modal review -->
    <!-- jQuery 3 -->
    <script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
    <script language="javascript">

        $(document).on('click', '#Addgroup1', function(e){
            e.preventDefault();
            var groupCode = $(this).data('id');

            document.getElementById('groupCode').value=groupCode;
            document.getElementById('conditionTitle').innerHTML='เงื่อนไขการสำรองที่นั่งและจ่ายเงิน';
        });
        $(document).on('click', '#Addgroup2', function(e){
            e.preventDefault();
            var groupCode = $(this).data('id');
            document.getElementById('groupCode').value=groupCode;
            document.getElementById('conditionTitle').innerHTML='เงื่อนไขการยกเลิกการสำรองที่นั่ง';
        });
        $(document).on('click', '#Addgroup3', function(e){
            e.preventDefault();
            var groupCode = $(this).data('id');
            document.getElementById('groupCode').value=groupCode;
            document.getElementById('conditionTitle').innerHTML='ข้อมูลเอกสารในการขอวีซ่า';
        });



        $('#moreImage').on('click',function () {
            document.getElementById('showAddmore').style.display='';
        });

//        var simplemde = new SimpleMDE(
//                { element: document.getElementById("packageConditionDesc"),
//                    toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
//                });
    </script>

@stop()
