@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> {{$Condition_group->condition_group_title}} </span></h2>
        </div>
    </section>
@endsection
@section('content')

    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
        .col-lg-7{
            padding-left: 2px;
            padding-right: 2px;
        }
        .col-lg-5{
            padding-left: 2px;
            padding-right: 2px;
        }
        .col-md-4{
            padding-left: 2px;
            padding-right: 2px;
        }
        .col-md-6{
            padding-left: 2px;
            padding-right: 2px;
        }

        .form-control-sm{padding-right:0;padding-left:0; bottom: 2px}.form-control-sm{height:calc(1.5em + .5rem + 2px);padding:.25rem .7rem;font-size:1em;line-height:2;border-radius:.2rem; }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <form role="form" id="ConditionForm"  action="{{action('Package\ConditionController@saveCondition')}}" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="condition_group_id" value="{{Session::get('condition_group')}}">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-heading">
                                    @if($Formula->count())
                                        <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="material-switch pull-left">
                                                <input id="use_operator" name="use_operator" type="checkbox" value="Y"/>
                                                <label for="use_operator" class="label-success"></label> <span><strong>&nbsp; {{trans('common.enter_the_formula')}}</strong></span>
                                            </div>
                                            {{--<div class="checkbox-inline" style="font-size: 16px">--}}
                                                {{--<input type="checkbox"  name="use_operator" id="use_operator" value="Y"> <label>&nbsp; เลือกต้องการใส่สูตร</label>--}}
                                            {{--</div>--}}
                                        </div>
                                            <hr>
                                        </div>
                                    @else
                                        <input type="hidden" name="use_operator" value="">
                                    @endif
                                        <div class="col-lg-12">
                                        <div class="form-group not-use-operator">
                                            <label><strong>{{trans('package.Condition')}} *</strong> </label><BR>
                                            <textarea class="form-control summernote" name="condition_title" id="condition_title" ></textarea>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <!-- /.modal-content -->
                                    </div>

                                    <div class="col-lg-12 use-operator" style="display: none">

                                        <div class="form-group col-md-6">
                                            <label><strong>{{trans('package.Condition')}} *</strong> </label><BR>
                                            <select class="form-control" style="width: 100%" name="operator_code" id="operator_code" >
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach($Formula as $rows)
                                                    <option value="{{$rows->operator_code}}">{{$rows->operator_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="deposit form-group col-md-4">
                                            <label><strong>{{trans('common.deposit')}} *</strong> </label><BR>
                                            <input type="text" class="form-control" name="deposit" id="deposit">
                                        </div>
                                        <div class="visa form-group col-md-3">
                                            <label id="visa_expire"><strong>{{trans('common.day')}} *</strong> </label><BR>
                                            <input type="text" class="form-control" name="visa_expiry_date" id="visa_expiry_date"  >
                                        </div>
                                        <div style="display: none" id="sub_opertor">Operator</div>


                                        <div class="not-deposit">
                                        <div class="form-group col-md-3">
                                            <label id='date_start'><strong>{{trans('common.date_start')}} *</strong> </label><BR>
                                            <input type="number" class="form-control" name="condition_left" id="condition_left"   >
                                        </div>

                                        <div class="form-group col-md-3 between" style="display: none">
                                            <label id='date_end'><strong>{{trans('common.date_end')}} *</strong> </label><BR>
                                            <input type="number" class="form-control" name="condition_right" id="condition_right"   >
                                        </div>


                                        <div class="col-md-12">
                                            <div class="col-md-4" style="min-height: 40px">
                                                <div class="material-switch pull-left">
                                                    <input type="checkbox"  name="keep_value_deposit" id="keep_value_deposit" value="Y"  >
                                                    <label for="keep_value_deposit" class="label-success"></label> <span><strong>&nbsp;{{trans('common.value_deposit')}}</strong></span>
                                                </div>
                                            </div>
                                            <div class="keep_value_deposit">
                                            <div class="col-md-4">
                                               <label class="radio-inline">
                                                    <input type="radio"  name="deposit_type" id="deposit_type" value="K" checked >
                                                    <strong>&nbsp; {{trans('common.keep_value_deposit')}}</strong>
                                               </label>
                                                <label class="radio-inline">
                                                    <input type="radio"  name="deposit_type" id="deposit_type" value="R" >
                                                    <strong>&nbsp; {{trans('common.return_value_deposit')}}</strong>
                                                </label>
                                            </div>

                                            <div class="col-md-4" >
                                                <div class="form-group col-lg-7">
                                                    <input type="number" class="form-control" name="value_deposit" id="value_deposit" placeholder="{{trans('common.value_deposit')}}*"  >
                                                </div>
                                                <div class="form-group col-lg-5">
                                                    <select class="form-control select2" style="width: 100%" name="unit_deposit" id="unit_deposit" >
                                                        <option value="">{{trans('LProfile.Choose')}}{{trans('common.unit')}}</option>
                                                        <option value="%" >%</option>
                                                        <option value="{{$Package->packageCurrency}}" >{{$Package->packageCurrency}}</option>
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                        </div>

                                            <div class="col-md-12">
                                                <div class="col-md-4"  style="min-height: 40px">
                                                    <div class="material-switch pull-left">
                                                        <input type="checkbox"  name="keep_value_tour" id="keep_value_tour" value="Y"  >
                                                        <label for="keep_value_tour" class="label-success"></label> <span><strong>&nbsp;{{trans('common.value_tour')}}</strong></span>
                                                    </div>
                                                </div>
                                                <div class="keep_value_tour">
                                                <div class="col-md-4">
                                                    <label class="radio-inline">
                                                        <input type="radio"  name="tour_type" id="tour_type" value="K" checked >
                                                        <strong>&nbsp; {{trans('common.keep_value_tour')}}</strong>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio"  name="tour_type" id="tour_type" value="R" >
                                                        <strong>&nbsp; {{trans('common.return_value_tour')}}</strong>
                                                    </label>
                                                    {{--<div class="material-switch pull-left">--}}
                                                        {{--<input type="checkbox" name="keep_value_tour" id="keep_value_tour" value="Y"  >--}}
                                                        {{--<label for="keep_value_tour" class="label-info"></label> <span><strong>&nbsp;{{trans('common.keep_value_tour')}}</strong></span>--}}
                                                    {{--</div>--}}
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group col-lg-7">
                                                        <input type="number" class="form-control" name="value_tour" id="value_tour" placeholder="{{trans('common.value_tour')}}"  >
                                                    </div>
                                                    <div class="form-group col-lg-5">
                                                        <select class="form-control select2" style="width: 100%" name="unit_value_tour" id="unit_value_tour" >
                                                            <option value="">{{trans('profile.Choose')}}{{trans('common.unit')}}</option>
                                                            <option value="%">%</option>
                                                            <option value="{{$Package->packageCurrency}}">{{$Package->packageCurrency}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="material-switch pull-left">
                                                        <input type="checkbox"  name="keep_all_deposit" id="keep_all_deposit" value="Y"  >
                                                        <label for="keep_all_deposit" class="label-success"></label> <span><strong>&nbsp;{{trans('common.keep_all_deposit')}}</strong></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="material-switch pull-left">
                                                        <input type="checkbox"  name="return_all_deposit" id="return_all_deposit" value="Y"  >
                                                        <label for="return_all_deposit" class="label-success"></label> <span><strong>&nbsp;{{trans('common.return_all_deposit')}}</strong></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="material-switch pull-left">
                                                    <input type="checkbox"  name="keep_all_costs" id="keep_all_costs" value="Y"  >
                                                        <label for="keep_all_costs" class="label-success"></label> <span><strong>&nbsp;{{trans('common.keep_all_costs')}}</strong></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="material-switch pull-left">
                                                        <input type="checkbox"  name="return_all_costs" id="return_all_costs" value="Y"  >
                                                        <label for="return_all_costs" class="label-success"></label> <span><strong>&nbsp;{{trans('common.return_all_costs')}}</strong></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-12">
                                     <hr>
                                        @if(Session::get('event')=='details')
                                            <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default"><i  class="fa fa-arrow-circle-left"></i> {{trans('common.back_to_list')}}</a>

                                        @elseif(Session::get('checking'))
                                            <a href="{{url('package/condition/list_more/'.Session::get('condition_group'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                        @else
                                            <a href="{{url('package/condition/list/')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                        @endif

                                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                    </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $('.not-deposit').hide();
    });
</script>

@stop()
