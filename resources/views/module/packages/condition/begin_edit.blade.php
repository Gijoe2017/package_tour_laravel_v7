@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> {{trans('common.warning_before_edit')}} </span></h2>
        </div>
    </section>
@endsection
@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
        .confirm_title{
            line-height: 25px;
        }
    </style>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box box-header">
                        <h4 class="confirm_title"> {{trans('common.confirm_edit_condition')}}</h4>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <?php $i=0;?>
                            @foreach($Condition as $rows)
                                <?php $i++;?>
                                <p>{{$rows->packageID}} ) {{$rows->packageName}}</p>
                            @endforeach
<hr>
                            <div class="form-group">
                                <div class="col-md-2">
                                    @if(Session::get('event')=='details')
                                        <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default"><i  class="fa fa-arrow-circle-left"></i> {{trans('common.back_to_list')}}</a>
                                    @else
                                        <a href="{{url('/package/condition/list_more/'.Session::get('condition_group'))}}" class="btn btn-default"><i  class="fa fa-arrow-circle-left"></i> {{trans('common.back_to_list')}}</a>
                                    @endif
                                    {{--<a href="{{url('/package/condition/list_more/'.Session::get('condition_group'))}}" class="btn btn-default"><i  class="fa fa-arrow-circle-left"></i> {{trans('common.back_to_list')}}</a>--}}
                                </div>

                                    <div class="col-md-8">
                                    <a href="{{url('package/condition/create/'.Session::get('condition_group'))}}" class="btn btn-info pull-right"><i  class="fa fa-plus"></i> {{trans('common.create_new_condition')}}</a>
                            </div>
                                <div class="col-md-2">
                                    <a href="{{url('package/condition/edit/'.$id)}}" class="btn btn-danger pull-right"><i  class="fa fa-arrow-circle-right"></i> {{trans('common.confirm_edit')}}</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    @stop()
