<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload();"> &times; </button>
        <h3 class="modal-title-site text-center"> กำหนดอัตราค่าบริการ </h3>
    </div>
    <form role="form" id="AutoForm"  action="{{action('Package\PackageController@updateRate')}}" method="post" novalidate>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="packageDescID" value="{{$packageDescID}}">
        <input type="hidden" name="id" value="{{$id}}">

        <div class="modal-body">

            <div class="row">

                    <div class="col-md-12">
    <div class="form-group">
        <strong>ประเภทผู้ซื้อทัวร์ #1 *</strong>
        <input type="text" class="form-control" name="TourType[]" id="TourType1"  required>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label><strong>{{trans('package.PriceSale')}} *</strong></label>
        <input type="number" class="form-control" name="PriceSale[]" id="PriceSale1" required>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label><strong>{{trans('package.PriceAndTicket')}} *</strong></label>
        <input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket1"  required>
    </div>
</div>



</div>

        </div>


        <div class="modal-footer">
            <a class="btn  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
            <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </form>
</div>

