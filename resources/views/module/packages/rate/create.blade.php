<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload();"> &times; </button>
        <h3 class="modal-title-site text-center"> กำหนดอัตราค่าบริการ </h3>
    </div>
    <form role="form" id="AutoForm"  action="{{action('Package\PackageController@saveRate')}}" method="post" novalidate>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="packageDescID" value="{{$packageDescID}}">

        <div class="modal-body">

            <div class="row">

                    <div class="col-md-12">
    <div class="form-group">
        <strong>ประเภทผู้ซื้อทัวร์ #1 *</strong>
        <input type="text" class="form-control" name="TourType[]" id="TourType1"  required>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label><strong>{{trans('package.PriceSale')}} *</strong></label>
        <input type="number" class="form-control price_sale" name="PriceSale[]" id="PriceSale1" required>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label><strong>{{trans('package.PriceAndTicket')}} *</strong></label>
        <input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket1"  required>
    </div>
</div>

<div id="add-txt"></div>
<div class="col-md-6" style="padding-top: 5px">
    <button id="add" type="button" class="btn btn-default">Add</button>
    <button id="btn-remove" type="button" class="btn btn-default">Remove</button>
</div>

</div>

        </div>


        <div class="modal-footer">
            <a class="btn  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
            <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </form>
</div>
<!-- jQuery 3 -->
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<!-- date-range-picker -->
<script type="text/javascript">

    $(document).ready(function(){
        var counter = 2;
        $("#add").click(function () {
            if(counter>10){
                alert("Only 10 textboxes allow");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
            newTextBoxDiv.after().html('<div class="col-md-12"><hr></div>' +
                    '<div class="col-md-12">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><label><strong>{{trans("package.PriceSale")}}*</strong></label><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div><div class="col-md-6"><label><strong>{{trans("package.PriceAndTicket")}}*</strong></label><input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket' + counter + '"  required></div></div>');
            newTextBoxDiv.appendTo("#add-txt");
            counter++;
        });

        $("#btn-remove").click(function () {
            if(counter==2){
                alert("No more textbox to remove");
                return false;
            }
            counter--;
            $("#TextBoxDiv" + counter).remove();
        });
    });
</script>
