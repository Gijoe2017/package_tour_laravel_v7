<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf_token" content="{!! csrf_token() !!}"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />

<!-- main-section -->    <!-- <div class="main-content"> -->{{--<link media="all" type="text/css" rel="stylesheet" href="{{asset('/themes/default/assets/css/bootstrap.min.css')}}">--}}
<link rel="stylesheet" href="{{asset('themes/defaults/assets/css/style.d843a54abab569eaede4ed3a69a9b943.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/defaults/assets/js/popup/jquery-ui.complete.min.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/defaults/assets/js/popup/jquery.jspanel.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/defaults/assets/select1/jquery.typeahead.css')}}">
<link rel="stylesheet" href="{{asset('themes/defaults/assets/smartphoto/css/smartphoto.min.css')}}">

{{--<script src="{{asset('themes/defaults/assets/js/jquery.jscroll.min.js')}}"></script>--}}
<script src="{{asset('themes/defaults/assets/js/main.7e099de1c2d4b4d95065cb1d66b3cb74.js')}}"></script>
<script src="{{asset('themes/defaults/assets/js/popup/jquery-ui-complete.min.js')}}"></script>
{{--<script src="{{asset('themes/defaults/assets/js/popup/jquery.ui.touch-punch.min.js')}}"></script>--}}
<script src="{{asset('themes/defaults/assets/js/popup/jquery.jspanel.min.js')}}"></script>
<script src="{{asset('themes/defaults/assets/select1/jquery.typeahead.js')}}"></script>
<script src="{{asset('themes/defaults/assets/smartphoto/js/smartphoto.min.js')}}"></script>
<script src="{{asset('themes/defaults/assets/js/socket.io.min.js')}}"></script>
{{--<script src="{{asset('themes/defaults/assets/js/notifications.js')}}"></script>--}}
{{--<script src="{{asset('themes/defaults/assets/js/chatboxes.js')}}"></script>--}}


<script>
    var base_url = "{{ url('/') }}/";
    var theme_url = "{!! url('/themes/defaults/assets/') !!}";
</script>


<style type="text/css">
    .container {
        width: 98%;
        margin-top: 20px;
    }

    .logo {
        border-radius: 50%;
    }

    .text-muted a {
        color: #989898;
    }

    .text-muted a:hover {
        color: grey;
    }

    .fa a:active, a:hover, a:visited {
        text-decoration: none;
    }

    /* Tool Colors */
    .general {
        background-color: #0095af;
    }

    .general .card-title {
        color: #f3f3f3;
    }

    .general .card-text {
        color: #f3f3f3;
    }

    .card-img {
        display: block;
        width: 100%;
        height: auto;
    }

    .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .2s ease;
        background-color: #008CBA;
    }

    .card-pin:hover .overlay {
        opacity: .5;
        border: 5px solid #f3f3f3;
        transition: ease .5s;
        background-color: #000000;
        cursor: zoom-in;
    }

    .more {
        color: white;
        font-size: 20px;
        position: absolute;
        bottom: 0;
        right: 0;
        text-transform: uppercase;
        transform: translate(-20%, -20%);
        -ms-transform: translate(-50%, -50%);
    }

    .download {
        color: white;
        font-size: 20px;
        position: absolute;
        bottom: 0;
        left: 0;
        margin-left: 20px;
        text-transform: uppercase;
        transform: translate(-20%, -20%);
        -ms-transform: translate(-50%, -50%);
    }

    .card-pin:hover .card-title {
        color: #ffffff;
        margin-top: 10px;
        text-align: center;
        font-size: 1.2em;
    }

    .card-pin:hover .more a {
        text-decoration: none;
        color: #ffffff;
    }

    .card-pin:hover .download a {
        text-decoration: none;
        color: #ffffff;
    }

    .social {
        position: relative;
        transform: translateY(-50%);
    }

    .social .fa {
        margin: 0 3px;
    }

    #post-image-holder img {
        width: 270px !important;
        height: auto !important;
        margin-right: 5px;
        padding-left: 13px;
    }

    html {
        font-size: 13px;
    }

</style>
<div class="container">
    <div class="row">


            <div class="col-md-12 first">
                @if (Session::has('message'))
                    <div class="alert alert-{{ Session::get('status') }}" role="alert">
                        {!! Session::get('message') !!}
                    </div>
                @endif
                @if(isset($active_announcement))
                    <div class="announcement alert alert-info">
                        <a href="#" class="close" data-dismiss="alert"  aria-label="close">&times;</a>
                        <h3>{{ $active_announcement->title }}</h3>
                        <p>{{ $active_announcement->description }}</p>
                    </div>
                @endif


                    <div class="row">
                        {{--@if($locations->count() > 0)--}}
                        {{--@foreach($locations as $location)--}}
                        <?php
                        $timeline = $firstLocation->timeline()->first();
                        $post = \App\Post::where('timeline_id', $firstLocation->timeline_id)->orderby('created_at','desc')->first();
                        $media = \App\Media::where('id', $timeline->avatar_id)->first();
                        $city = null;$state = null;
                        $country = $timeline->country()->where('language_code', Auth::user()->language)->first();
                        if ($timeline->state_id) {
                            $state = $timeline->country->state()->where('state_id', $timeline->state_id)->where('language_code', Auth::user()->language)->first();
                            $city = $timeline->country->city()->where('city_id', $timeline->city_id)->where('language_code', Auth::user()->language)->first();
                        }
                        ?>
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="postmessage"></span>
                                    <form action="{{ url('') }}" method="post" class="create-post-form-highlight">
                                        {{ csrf_field() }}
                                        <label class="text-info" style="font-size: 14px"> {{trans('common.where_did_you_go')}}</label>
                                        <div class="search-location">
                                            <input type="hidden" id="username" name="username" value="{{Auth::user()->name}}">
                                            {{--<div class="js-result-container"></div>--}}
                                            <div class="typeahead__container">
                                                <div class="typeahead__field">
                                                    <div class="typeahead__query">
                                                        <input class="js-typeahead" id="q" name="q" type="search" autofocus autocomplete="off" required >
                                                    </div>
                                                    <div class="typeahead__button">
                                                        <button type="submit"><i class="fa fa-map-marker"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-location" style="display: none">

                                            <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                                {{ Form::label('name', trans('common.name'), ['class' => 'control-label']) }}
                                                {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                     </span>
                                                @endif
                                            </fieldset>
                                            <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                                {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                                {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                                @if ($errors->has('category_id'))
                                                    <span class="help-block">
										{{ $errors->first('category_id') }}
									</span>
                                                @endif
                                            </fieldset>

                                            <fieldset style="display: none" class="form-group category_sub1 required {{ $errors->has('category_sub1_id') ? ' has-error' : '' }}">
                                                {{ Form::label('category_sub1_id', trans('common.category_sub1'), ['class' => 'control-label']) }}
                                                {{ Form::select('category_sub1_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                                @if ($errors->has('category_sub1_id'))
                                                    <span class="help-block">
                                    {{ $errors->first('category_sub1_id') }}
                                </span>
                                                @endif
                                            </fieldset>

                                            <fieldset style="display: none"  class="form-group category_sub2 required {{ $errors->has('category_sub2_id') ? ' has-error' : '' }}">
                                                {{ Form::label('category_sub2_id', trans('common.category_sub2'), ['class' => 'control-label']) }}
                                                {{ Form::select('category_sub2_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                                @if ($errors->has('category_sub2_id'))
                                                    <span class="help-block">
                                            {{ $errors->first('category_sub2_id') }}
                                          </span>
                                                @endif
                                            </fieldset>

                                            <fieldset class="form-group required {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                                {{ Form::label('country_id', trans('common.country'), ['class' => 'control-label']) }}
                                                {{ Form::select('country_id', array('' => trans('common.select_country'))+ $country_options, '', array('class' => 'form-control')) }}
                                                @if ($errors->has('country_id'))
                                                    <span class="help-block">
										    {{ $errors->first('country_id') }}
									  </span>
                                                @endif
                                            </fieldset>

                                            <fieldset id="box-state" class="form-group required {{ $errors->has('state_id') ? ' has-error' : '' }}">
                                                {{ Form::label('state_id', trans('common.state'), ['class' => 'control-label']) }}
                                                {{ Form::select('state_id', array('' => trans('common.select_state')), '', array('class' => 'form-control')) }}
                                                @if ($errors->has('state_id'))
                                                    <span class="help-block">
                                        {{ $errors->first('state_id') }}
                                  </span>
                                                @endif
                                            </fieldset>

                                            <fieldset style="display: none" class="form-group city_id required {{ $errors->has('city_id') ? ' has-error' : '' }}">
                                                {{ Form::label('city_id', trans('common.city'), ['class' => 'control-label']) }}
                                                {{ Form::select('city_id', array('' => trans('common.select_city')), '', array('class' => 'form-control')) }}
                                                @if ($errors->has('city_id'))
                                                    <span class="help-block">
										    {{ $errors->first('city_id') }}
									  </span>
                                                @endif
                                            </fieldset>

                                            <fieldset style="display: none" class="form-group city_sub1_id required{{ $errors->has('city_sub1_id') ? ' has-error' : '' }}">
                                                {{ Form::label('city_sub1_id', trans('common.city_sub'), ['class' => 'control-label']) }}
                                                {{ Form::select('city_sub1_id', array('' => trans('common.select_city_sub')), '', array('class' => 'form-control')) }}
                                                @if ($errors->has('city_sub1_id'))
                                                    <span class="help-block">
										    {{ $errors->first('city_sub1_id') }}
									  </span>
                                                @endif
                                            </fieldset>

                                        </div>
                                        <BR>


                                        <div class="panel panel-default panel-create"> <!-- panel-create -->
                                            <div class="panel-heading">
                                                <div class="heading-text">
                                                    {{ trans('common.post_message_not_user')}}

                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <textarea name="description" class="form-control createpost-form comment" cols="30" rows="3" id="createPost" cols="30" rows="2" placeholder="{{ trans('messages.post-placeholder') }}"></textarea>
                                                <div class="user-tags-added" style="display:none">
                                                    &nbsp; -- {{ trans('common.with') }}
                                                    <div class="user-tag-names"></div>
                                                </div>
                                                <div class="user-tags-addon post-addon" style="display: none">
                                                    <span class="post-addon-icon"><i class="fa fa-user-plus"></i></span>
                                                    <div class="form-group">
                                                        <input type="text" id="userTags" class="form-control user-tags youtube-text" placeholder="{{ trans('messages.who_are_you_with') }}" autocomplete="off" value="" >
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="users-results-wrapper"></div>
                                                <div class="youtube-iframe"></div>

                                                <div class="video-addon post-addon" style="display: none">
                                                    <span class="post-addon-icon"><i class="fa fa-film"></i></span>
                                                    <div class="form-group">
                                                        <input type="text" name="youtubeText" id="youtubeText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_watching') }}"  value="" >
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                @if((env('SOUNDCLOUD_CLIENT_ID') != "" || (env('SOUNDCLOUD_CLIENT_ID') != null)))
                                                    <div class="music-addon post-addon" style="display: none">
                                                        <span class="post-addon-icon"><i class="fa fa-music" aria-hidden="true"></i></span>
                                                        <div class="form-group">
                                                            <input type="text" name="soundCloudText" autocomplete="off" id ="soundCloudText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_listening_to') }}"  value="" >
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="soundcloud-results-wrapper"></div>
                                                @endif

                                                <div class="location-addon post-addon" style="display: none">
                                                    <span class="post-addon-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                                    <div class="form-group">
                                                        <input type="text" name="location" id="pac-input" class="form-control" placeholder="{{ trans('messages.where_are_you') }}"  autocomplete="off" value="" onKeyPress="return initMap(event)"><div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="emoticons-wrapper  post-addon" style="display:none">

                                                </div>
                                                <div class="images-selected post-images-selected" style="display:none">
                                                    <span>3</span> {{ trans('common.photo_s_selected') }}
                                                </div>
                                                <div class="images-selected post-video-selected" style="display:none">
                                                    <span>3</span>
                                                </div>
                                                <!-- Hidden elements  -->
                                                <input type="hidden" name="timeline_id" value="{{ $timeline->id }}">
                                                <input type="hidden" name="youtube_title" value="">
                                                <input type="hidden" name="youtube_video_id" value="">
                                                <input type="hidden" name="locatio" value="">
                                                <input type="hidden" name="soundcloud_id" value="">
                                                <input type="hidden" name="user_tags" value="">
                                                <input type="hidden" name="soundcloud_title" value="">
                                                <input type="file"   class="post-images-upload hidden" multiple="multiple"  accept="image/jpeg,image/png,image/gif" name="post_images_upload[]" id="post_images_upload[]">
                                                <input type="file" class="post-video-upload hidden"  accept="video/mp4" name="post_video_upload" >
                                                <div id="post-image-holder"></div>
                                            </div><!-- panel-body -->

                                            <div class="panel-footer">
                                                <ul class="list-inline left-list">
                                                    <li><a href="#" id="addUserTags"><i class="fa fa-user-plus"></i></a></li>
                                                    <li><a href="#" id="imageUpload"><i class="fa fa-camera-retro"></i></a></li>
                                                    {{-- <li><a href="#" id="selfVideoUpload"><i class="fa fa-film"></i></a></li> --}}
                                                    @if((env('SOUNDCLOUD_CLIENT_ID') != "" || (env('SOUNDCLOUD_CLIENT_ID') != null)))
                                                        <li><a href="#" id="musicUpload"><i class="fa fa-music"></i></a></li>
                                                    @endif
                                                    <li><a href="#" id="videoUpload"><i class="fa fa-youtube"></i></a></li>
                                                    <li><a href="#" id="locationUpload"><i class="fa fa-map-marker"></i></a></li>
                                                    <li><a href="#" id="emoticons"><i class="fa fa-smile-o"></i></a></li>
                                                </ul>
                                                <ul class="list-inline right-list">
                                                    {{--@if($user_post == 'group' && Auth::user()->is_groupAdmin(Auth::user()->id, $timeline->groups->id) || $user_post == 'group' && $timeline->groups->event_privacy == 'members' && Auth::user()->is_groupMember(Auth::user()->id, $timeline->groups->id))--}}
                                                    {{--<li><a href="{!! url($username.'/groupevent/'.$timeline->groups->id) !!}" class="btn btn-default">{{ trans('common.create_event') }}</a></li>--}}
                                                    {{--@endif--}}
                                                    <li><button type="submit" class="btn btn-submit btn-success">{{ trans('common.post') }}</button></li>
                                                </ul>

                                                <div class="clearfix"></div>
                                            </div>

                                        </div>

                                        <div><a class="btn btn-default" href="{{url('/package/program/highlight/'.Session::get('program_id'))}}"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a> </div>
                                        @if(Setting::get('postcontent_ad') != NULL)
                                            <div id="link_other" class="page-image">
                                                {!! htmlspecialchars_decode(Setting::get('postcontent_ad')) !!}
                                            </div>
                                        @endif


                                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_vuWi_hzMDDeenNYwaNAj0PHzzS2GAx8&libraries=places&callback=initMap"
                                                async defer></script>

                                        <script>
                                            function initMap(event)
                                            {
                                                var key;
                                                var map = new google.maps.Map(document.getElementById('pac-input'), {
                                                });

                                                var input = /** @type {!HTMLInputElement} */(
                                                    document.getElementById('pac-input'));

                                                if(window.event)
                                                {
                                                    key = window.event.keyCode;

                                                }
                                                else
                                                {
                                                    if(event)
                                                        key = event.which;
                                                }

                                                if(key == 13){
                                                    //do nothing
                                                    return false;
                                                    //otherwise
                                                } else {
                                                    var autocomplete = new google.maps.places.Autocomplete(input);
                                                    autocomplete.bindTo('bounds', map);

                                                    //continue as normal (allow the key press for keys other than "enter")
                                                    return true;
                                                }
                                            }
                                        </script>






                                    </form>
                                </div>

                            </div>
                        </div>
                        {{--@endforeach--}}

                        {{--@endif--}}
                    </div>
                        <script>
                            function SP_source() {
                                return "{{url('/')}}/";
                            }


                        </script>


                        <script src="{{asset('themes/defaults/assets/js/app.js')}}"></script>
                        <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>
            </div>




    </div>    <!-- </div> --><!-- /main-section -->
</div>


