@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <link rel=stylesheet href={{asset('member/waterfull/styles/main.css')}}>
    @if($Packages->count()==0)
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>Have no package.</h2>
                    </div>

                </div>
            </div>
        </div>
    @else
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-2 text-right">
                        <label>Filter By:</label>
                    </div>
                <div class="col-md-4">
               <form id="form-set-country"  method="post" action="{{action('Package\PackageController@sort_by_country')}}">
                   {{csrf_field()}}
                   <div class="form-group" >
                       <select name="country" id="country" class="form-control">
                           <option value="">Choose Country</option>
                           <option value="all" {{isset($country)?$country=='all'?'Selected':'':''}}>All Country</option>
                           @foreach($PackageCountry as $rows)
                               <option value="{{$rows->country_id}}" {{isset($country)?$country==$rows->country_id?'Selected':'':''}}>{{$rows->country}}</option>
                           @endforeach
                       </select>
                   </div>
               </form>
                </div>
                </div>
            </div>
        </div>
        <div class=container>
            <div class="row" style="margin-left: -25px">
                <div class=waterfall></div>
            </div>
    </div>
    @endif

    <script src={{asset('member/waterfull/scripts/vendor.js')}}></script>
    <script src={{asset('member/waterfull/bootstrap-waterfall.js')}}></script>

    <script id=waterfall-template type=text/template>
        @foreach($Packages as $rows)
        <?php
        $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
        $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
        $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
        $check_copy=DB::table('package_tour_info')
            ->where('show_in_timeline',Session::get('timeline_id'))
            ->where('packageID',$rows->packageID)
            ->count();
        ?>
        <ul class="list-group">

            <li class="list-group-item text-center">
                <div class="panel-body">{{$rows->packageName}}</div>  <br>
                <a href="javascript:;">
                    <img class="img-thumbnail" src="{{asset('images/package-tour/mid/'.$rows->Image)}}">
                </a>
            </li>
            <li class="list-group-item text-center">
                {{--@if($check>0)--}}
                {{--<a href="{{url('package/users/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Members</a>--}}
                <a href="{{url('package/partner/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Preview</a>
                {{--@endif--}}
                @if($check_copy==0)
                    <a href="{{url('package/copy/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-edit"></i> Copy</a>
                @endif
            </li>

        </ul>
        @endforeach

    </script>


    <script>
        $('.waterfall')
            .data('bootstrap-waterfall-template', $('#waterfall-template').html())
            .on('finishing.mystist.waterfall', function () {
                setTimeout(function () { // simulate ajax
                    $('.waterfall').data('mystist.waterfall').addPins($($('#another-template').html()))
                }, 500);
            })
            .waterfall()

        $('#country').on('change',function (e) {
            if(e.target.value){
               $('#form-set-country').submit();
            }
//            $.ajax({
//                url:SP_source()+'/ajax/sort-country',
//                type:'get',
//                data:{'country':e.target.value},
//                success:function (data) {
//                    alert('dd');
//                }
//            });
        });
    </script>


@endsection
