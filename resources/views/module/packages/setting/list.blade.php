@extends('layouts.member.layout_master_new')
@section('header')
    <section class="content-header">
        <h1>
            Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- The time line -->
                    <ul class="timeline">
                        @foreach($Packages as $rows)

                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-red">
                            {{date('F d, Y',strtotime($rows->packageCreated))}}
                          </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-edit bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {{date('H:i',strtotime($rows->packageCreated))}}</span>
                                <h3 class="timeline-header"><a href="#">{{$rows->packageName}}</a> <span>dddd</span></h3>
                                <div class="timeline-body">
                                    <div style="padding: 15px">
                                    <img src="{{url('/package/tour/x-small/'.$rows->Image)}}" style="width: 20%" class="img-circle pull-left">
                                    {!! $rows->packageHighlight !!}
                                    </div>
                                </div>
                                <div class="timeline-footer  text-right">
                                    <?php
                                    $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                                    $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                                    $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                                    $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                                    ?>
                                    {{--@if($check>0)--}}
                                    {{--<a href="{{url('package/users/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Members</a>--}}
                                    <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Preview</a>
                                    {{--@endif--}}
                                    <a href="{{url('package/edit/'.$rows->packageID)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    @if($check_del==0 && $check_del2==0 && $check_owner==1)
                                        <a href="{{url('package/delete/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                                    @endif
                                    {{--<a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Preview</a>--}}
                                    {{--<a href="{{url('package/edit/'.$rows->packageID)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>--}}
                                    {{--<a href="{{url('package/delete/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>--}}
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        @endforeach
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div>
                <!-- /.col -->
            </div>
        </section>
@endsection