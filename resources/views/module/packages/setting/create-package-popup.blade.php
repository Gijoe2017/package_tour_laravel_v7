<!-- daterange picker -->
{{--<link rel="stylesheet" href="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.css')}}">--}}

{{--<link rel="stylesheet" href="{{asset('member/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">--}}

@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> {{trans('common.package_edit')}} </span></h2>
        </div>
    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                <form role="form" id="AutoForm"  action="{{action('Package\PackageController@saveDetail')}}" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="Days" id="Days" value="{{Session::get('days')}}" >

                    <div class="panel-content">
                    <div class="panel-header">
                    <h3 class="panel-title-site text-center"> {{trans('common.schedule_travel')}} </h3>
                        {{--<p class="text-center">{{$PackageInfo->packageName}}</p>--}}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                        <div class="col-lg-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <div id="sandbox-container" class="form-group">
                                    <label><strong>{{trans('package.Daterange')}} <span class="text-red">*</span> </strong></label> <small class="text-gray">{{trans('common.set_the_departure_date_and_end_date_of_travel')}}</small> {{trans('LPackageTour.PackageInfo').' '.$Package->packageDays .' '.trans('LPackageTour.Days')}} {{$Package->packageNight .' '.trans('LPackageTour.Night')}}<BR>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input type="text" class="form-control"  name="package_date_start" autocomplete="off" required />
                                            <span class="input-group-addon">{{trans('common.to')}}</span>
                                            <input type="text" class="form-control"  name="package_date_end" autocomplete="off" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>&nbsp;</label><BR>
                                    <input type="checkbox" id="season"  name="season" value="Y"><strong> {{trans('common.season')}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-4 season_name" style="display: none">
                                    <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="text" class="form-control" id="season_name" name="season_name" placeholder="{{trans('common.season_name')}}" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><strong>{{trans('common.days_off_the_sale_before')}}</strong>  <span class="text-red">*</span></label><BR>
                                        <input type="text" class="form-control" id="number_of_days_the_sale_is_closed" name="number_of_days_the_sale_is_closed" required placeholder="{{trans('common.number_of_days')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                                <div class="col-md-12 alert">
                                    <label class="radio-inline">
                                        <input type="radio" class="use-airplan" name="use_airplan" value="0" checked><strong>{{trans('common.travel_by_plane')}}</strong>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="use-airplan" name="use_airplan" value="1" > <strong>{{trans('common.do_not_travel_by_plane')}}</strong>
                                    </label>
                                </div>


                                <div class="col-md-12 use-car" style="display: none">
                                    <div class="form-group">
                                        <label><strong>{{trans('package.travel_by')}} <span class="text-red">*</span></strong> </label><BR>
                                        <select class="form-control selectpicker" name="travel_by" id="travel_by"  >
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($Vehicle as $rows)
                                                <option value="{{$rows->id}}">{{$rows->vehicle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4 use-plan">
                                    <div class="form-group">
                                        <label><strong>{{trans('package.Country_Airport')}} <span class="text-red">*</span></strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                        <select class="form-control selectpicker" name="Country_id" id="Country_id" required >
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 use-plan">
                                    <div class="form-group">
                                        <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">{{trans('common.need_to_insert_airline')}}</span></label><BR>
                                        <select class="form-control" name="Airline" id="Airline" >
                                            <option value="">{{trans('common.choose')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 use-plan">
                                    <div class="form-group">
                                        <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">{{trans('common.need_to_insert_flight')}}</span></label><BR>
                                        <input type="text" class="form-control" name="Flight" id="Flight" >
                                    </div>
                                </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                                    <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople"  required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                                    <input type="number" class="form-control" name="Commission" id="Commission"  required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>{{trans('package.Commission_sell')}} *</strong></label><BR>
                                    <input type="number"  class="form-control" name="Commission_sell" id="Commission_sell"  required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                                    <select class="form-control select2" style="width: 100%" name="unit_commission" id="unit_commission" required >
                                        <option value="">{{trans('profile.Choose')}}</option>
                                        <option value="%">%</option>
                                        <option value="{{$Package->packageCurrency}}" >{{$Package->packageCurrency}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                    <a href="{{url('/package/details')}}" class="btn btn-default  pull-left" class="close"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                    <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                    </div>
    </div>
    <!-- /.modal-content -->
                </form>
                </div>
            </div>
        </div>
    </section>
{{--<script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>--}}
{{--<script src="{{asset('member/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}}
{{--<script src="{{asset('member/bootstrap-datepicker/locales/bootstrap-datepicker.'.App::getLocale().'.min.js')}}"></script>--}}

<script>

    $('.use-airplan').on('click',function () {

        var radioValue = $("input[name='use_airplan']:checked").val();
        if(radioValue==0){
            $('.use-plan').show();
            $('.use-car').hide();
            $('#Country_id').prop('required',true);
            $('#travel_by').prop('required',false);
        }else{
            $('#Country_id').prop('required',false);
            $('#travel_by').prop('required',true);
            $('.use-plan').hide();
            $('.use-car').show();
        }
    });

    $('#season').on('click',function () {
        if($(this).prop('checked')==true){
            $('.season_name').show();
            $('#season_name').prop('required',true);
        }else{
            $('.season_name').hide();
            $('#season_name').prop('required',false);

        }
    });

    $('#Country_id').on('change',function (e) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/ajax/get-airline',
            data:{'country_id':e.target.value},
            success:function(data){
                $('#Airline').empty();
                $('#Airline').append("<option value=''>{{trans('common.choose')}}</option>");
                $('#Airline').append(data);
            }
        });
    });

    var today=new Date();
    $('#sandbox-container .input-daterange').datepicker({
//        maxViewMode: 0,
        startDate: today,
        forceParse: false,
//        calendarWeeks: true,
        autoclose: true,
//        todayHighlight: true,
//        format: "yyyy-mm-dd",
        language: "{{Session::get('language')}}",

    });



//
//    function addDays(date, days) {
//        var result = new Date(date);
//        result.setDate(result.getDate() + days);
//        return result;
//    }

</script>

@endsection

