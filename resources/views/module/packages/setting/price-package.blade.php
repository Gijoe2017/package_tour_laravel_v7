@extends('layouts.member.layout_master_new')
@section('header')
    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
        #show_price_status {
            position:absolute;
            top: 25%;
            left: 50%;

            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
            z-index: 100;
        }
    </style>

    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Price Rate Package Detail. </span></h2>
        </div>
    </section>
@endsection
        @section('content')
            <style type="text/css">
                .material-switch > input[type="checkbox"] {
                    display: none;
                }
                .material-switch > label {
                    cursor: pointer;
                    height: 0px;
                    position: relative;
                    width: 35px;
                }
                .material-switch > label::before {
                    background: rgb(0, 0, 0);
                    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
                    border-radius: 8px;
                    content: '';
                    height: 16px;
                    margin-top: -8px;
                    position:absolute;
                    opacity: 0.3;
                    transition: all 0.4s ease-in-out;
                    width: 35px;
                }
                .material-switch > label::after {
                    background: rgb(255, 255, 255);
                    border-radius: 16px;
                    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
                    content: '';
                    height: 20px;
                    left: -4px;
                    margin-top: -8px;
                    position: absolute;
                    top: -4px;
                    transition: all 0.3s ease-in-out;
                    width: 20px;
                }
                .material-switch > input[type="checkbox"]:checked + label::before {
                    background: inherit;
                    opacity: 0.5;
                }
                .material-switch > input[type="checkbox"]:checked + label::after {
                    background: inherit;
                    left: 20px;
                }
            </style>
        <section class="content">
                <div class="row">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div  class="alert alert-success">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-calendar"></i> {{trans('package.service_rate')}} </span>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<form role="form" id="AutoForm"  action="{{action('Package\PackageController@updatePackageDetail')}}" enctype="multipart/form-data" method="post" novalidate>--}}
                                                {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
                                                {{--<input type="hidden" name="packageDescID" id="packageDescID" value="{{$Details->packageDescID}}" >--}}
                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <h3>{{trans('common.tour_fee_rate')}}
                                                                    <span class="col-md-2 pull-right">
                                                                        <a  class=" btn btn-block btn-sm btn-warning" href="{{url('package/details/create/price')}}">
                                                                            <i class="fa fa-plus"></i> {{trans('common.add')}}
                                                                        </a>

                                                                        {{--<a type="button" data-toggle="modal" data-target="#popupForm" class=" btn btn-block btn-sm btn-warning" href="{{url('package/details/create/price')}}">--}}
                                                                            {{--<i class="fa fa-plus"></i> {{trans('common.add')}}--}}
                                                                        {{--</a>--}}
                                                                    </span>
                                                                </h3>
                                                            </div>

                                                            <?php $i=1;?>
                                                            <div class="col-md-12">

                                                                <table id="table-rate" class="table table-bordered table">
                                                                    <thead>
                                                                    <tr><th>{{trans('common.status')}}</th>
                                                                        <th>{{trans('common.tour_buyer_type')}} </th>
                                                                        <th>{{trans('package.PriceSale')}} </th>
                                                                        <th>{{trans('package.PriceSale_show')}} </th>
                                                                        <th>{{trans('package.Price_include_visa')}} </th>
                                                                        <th>{{trans('package.Price_include_vat')}} </th>
                                                                        <th> </th>

                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php
                                                                        $CurrencyCode=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

                                                                    ?>
                                                                    @foreach($Detail_sub as $rows)

                                                                        <tr style="background-color: #eef2f5">
                                                                            <td>
                                                                                <div class="material-switch">
                                                                                    <input class="switch-rate" data-id="{{$rows->psub_id}}" id="status-{{$rows->psub_id}}" name="status-{{$rows->psub_id}}" type="checkbox" value="Y"  {{$rows->status=='Y'?'checked':''}}/>
                                                                                    <label for="status-{{$rows->psub_id}}" class="label-info"></label><span id="span-{{$rows->psub_id}}" style="padding-left: 15px">{{$rows->status==''?trans('common.N'):trans('common.'.$rows->status)}}</span>
                                                                                </div>
                                                                            </td>
                                                                            <td>{{$rows->TourType}}</td>
                                                                            <td>{{number_format((float)$rows->PriceSale)}}</td>
                                                                            <td>{{number_format((float)$rows->price_system_fees)}}</td>
                                                                            <td>
                                                                                 <div class="material-switch">
                                                                                    <input class="switch-price_visa_condition" data-id="{{$rows->psub_id}}" id="price_include_visa-{{$rows->psub_id}}" name="price_include_visa-{{$rows->psub_id}}" type="checkbox" value="Y"  {{$rows->price_include_visa=='Y'?'checked':''}}/>
                                                                                    <label for="price_include_visa-{{$rows->psub_id}}" class="label-info"></label>
                                                                                    <span id="span-price_include_visa-{{$rows->psub_id}}" style="padding-left: 15px">{{$rows->price_include_visa==''?trans('common.price_include_visa_N'):trans('common.price_include_visa_'.$rows->price_include_visa)}}</span>
                                                                                 </div>
                                                                                 <div class="form-group text-red">
                                                                                     @if($rows->price_for_visa>0)
                                                                                     {{trans('package.Price_visa')}} {{$CurrencyCode->currency_symbol.number_format($rows->price_for_visa)}}</br> {{$rows->price_visa_details}}
                                                                                     @endif
                                                                                 </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="material-switch">
                                                                                    <input class="switch-price_default_condition" data-id="{{$rows->psub_id}}" id="price_system_vat-{{$rows->psub_id}}" name="price_system_vat-{{$rows->psub_id}}" type="checkbox" value="Y" {{$rows->price_include_vat=='Y'?'checked':''}}/>
                                                                                    <label for="price_system_vat-{{$rows->psub_id}}" class="label-info"></label>
                                                                                    <span class="span-price_default" id="span-price_system_vat-{{$rows->psub_id}}" style="padding-left: 15px">{{$rows->price_include_vat==''?trans('common.price_system_default_N'):trans('common.price_system_default_'.$rows->price_include_vat)}}</span>
                                                                                </div>
                                                                            </td>
                                                                            <td style="width: 20%" align="right">
                                                                                <div class="btn-group">
                                                                                    <button type="button" class="btn btn-default">{{trans('common.action')}}</button>
                                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                                        <span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu" role="menu">
                                                                                        <li><a   href="{{url('package/details/promotion/create/'.$rows->psub_id)}}"><i class="fa fa-plus"></i> {{trans('common.promotion')}}</a></li>
                                                                                        <li><a  href="{{url('package/details/price/edit/'.$rows->psub_id)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a></li>
                                                                                        <li><a  onclick="return confirm_delete()" href="{{url('package/details/price/delete/'.$rows->psub_id)}}"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $promotion=DB::table('package_promotion')->where('psub_id',$rows->psub_id)->get();
                                                                       // dd($promotion);
                                                                        ?>
                                                                        @if($promotion)
                                                                            @foreach($promotion as $rowp)
                                                                                <tr class="promotion-{{$rowp->psub_id}}" style="display: {{$rows->status=='Y'?'':'none'}}">
                                                                                    <td></td>
                                                                                    <td colspan="6">
                                                                                        <div class="col-md-2">
                                                                                            <div class="material-switch">
                                                                                                <input class="switch-promotion {{$rowp->promotion_operator.'-'.$rows->psub_id}}" data-id="{{$rowp->promotion_id}}" id="status-{{$rowp->promotion_id}}" name="status-{{$rowp->promotion_id}}" type="checkbox" value="Y"  {{$rowp->promotion_status=='Y'?'checked':''}}/>
                                                                                                <label for="status-{{$rowp->promotion_id}}" class="label-info"></label><span id="span-{{$rowp->promotion_id}}" style="padding-left: 15px">{{trans('common.'.$rowp->promotion_status)}}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-8"> {{$rowp->promotion_title}}</div>
                                                                                        <div class="col-md-2" align="right">
                                                                                            <div class="btn-group">
                                                                                                <button type="button" class="btn btn-default">{{trans('common.action')}}</button>
                                                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                                                    <span class="caret"></span>
                                                                                                </button>
                                                                                                <ul class="dropdown-menu" role="menu">
                                                                                                    <li><a href="{{url('package/details/promotion/edit/'.$rowp->promotion_id)}}" ><i class="fa fa-edit"></i> {{trans('common.edit')}}</a></li>
                                                                                                    <li><a href="{{url('package/details/promotion/delete/'.$rowp->promotion_id)}}" ><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @endif

                                                                    @endforeach
                                                                    </tbody>
                                                                </table>

                                                            </div>

                                                            <div class="col-md-12">
                                                                <hr>
                                                                <h3>{{trans('common.extra_service_rate')}}
                                                                    <span class="col-md-2 pull-right">
                                                                        <a class=" btn btn-block btn-sm btn-warning" href="{{url('package/details/create/additional')}}">
                                                                            <i class="fa fa-plus"></i> {{trans('common.add')}}
                                                                        </a>

                                                                          {{--<a type="button" data-toggle="modal" data-target="#popupForm" class=" btn btn-block btn-sm btn-warning" href="{{url('package/details/create/additional')}}">--}}
                                                                            {{--<i class="fa fa-plus"></i> {{trans('common.add')}}--}}
                                                                        {{--</a>--}}
                                                                    </span>
                                                                </h3>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <table id="table-rate" class="table table-bordered table">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>{{trans('common.status')}}</th>
                                                                        <th>{{trans('common.additional')}} </th>
                                                                        <th>{{trans('package.PriceSale')}} </th>
                                                                        <th>{{trans('common.remark')}}</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($Additional as $rows)
                                                                        <?php
                                                                            $check=DB::table('package_details_sub')->where('additional_id',$rows->id)->first();
                                                                        ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="material-switch">
                                                                                    <input class="switch-additional" data-id="{{$rows->id}}" id="status-{{$rows->id}}" name="status-{{$rows->id}}" type="checkbox" value="Y"  {{$rows->status=='Y'?'checked':''}}/>
                                                                                    <label for="status-{{$rows->id}}" class="label-success"></label><span id="span-{{$rows->id}}" style="padding-left: 15px">{{$rows->status==''?trans('common.N'):trans('common.'.$rows->status)}}</span>
                                                                                </div>
                                                                            </td>
                                                                            <td>{{$rows->additional_service}}</td>
                                                                            <td>{{number_format($rows->price_service)}}</td>
                                                                            @if($check)
                                                                                <td>{{trans('common.this_additional_service_is_tied_to')}} <span class="text-danger"> {{$check->TourType.' '.trans('common.automatic')}}</span></td>
                                                                            @else
                                                                                <td>-</td>
                                                                            @endif
                                                                            <td  align="right">
                                                                                <div class="btn-group">
                                                                                    <button type="button" class="btn btn-default">{{trans('common.action')}}</button>
                                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                                        <span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu" role="menu">
                                                                                        <li><a  href="{{url('package/details/additional/edit/'.$rows->id)}}"><i class="fa fa-plus"></i> {{trans('common.edit')}}</a></li>
                                                                                        <li><a  onclick="return confirm_delete()" href="{{url('package/details/additional/delete/'.$rows->id)}}"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                                                                    </ul>
                                                                                </div>
                                                                                {{--<a class="btn btn-sm btn-default" data-toggle="modal" data-target="#popupForm" href="{{url('package/details/promotion/create/'.$rows->psub_id)}}"><i class="fa fa-plus"></i> {{trans('common.promotion')}}</a>--}}
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="content40"><hr></div>
                                                            @if(Session::has('checking'))
                                                                <a href="{{url('/package/details_check/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                                            @elseif(Session::get('event')=='details')
                                                                <a href="{{url('/package/details/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                                            @else
                                                            <a href="{{url('package/details')}}" class="btn btn-lg btn-default  pull-left"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                                            @endif
                                                            {{--<button type="submit"  class="btn btn-lg btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Update')}}</button>--}}
                                                        </div>
                                                <!-- /.modal-content -->

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>


<!-- jQuery 3 -->
            <!-- jQuery 3 -->
            <script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>

            <script type="text/javascript">

                $(document).ready(function () {
                    $('.modal').on('hidden.bs.modal',function () {
                        alert('test');
                        $(this).removeData();
                    });
                });

                $(document).ready(function(){
                    var counter = '<?php echo $Detail_sub->count()+1?>';
                    var counter2 = '<?php echo $Additional->count()+1?>';
                    $("#add").click(function () {
                        if(counter>10){
                            alert("Only 10 textboxes allow");
                            return false;
                        }
                        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
                        newTextBoxDiv.after().html(
                            '<div class="col-md-6">' +
                            '<div class="form-group">' +
                            '<label><strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong></label>' +
                            '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                            '</div></div>' +
                            '<div class="col-md-6">' +
                            '<div class="form-group"><label><strong>{{trans("package.PriceSale")}}*</strong></label><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div></div>');
                        newTextBoxDiv.appendTo("#add-txt");
                        counter++;
                    });

                    $("#btn-remove").click(function () {
                        if(counter==1){
                            alert("No more textbox to remove");
                            return false;
                        }
                        counter--;
                        $("#TextBoxDiv" + counter).remove();
                    });



                    $("#add2").click(function () {
                        if(counter2>10){
                            alert("Only 10 textboxes allow");
                            return false;
                        }
                        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter2);

                        newTextBoxDiv.after().html(
                            '<div class="col-md-6">' +
                            '<div class="form-group">' +
                            '<label><strong>ประเภทผู้ซื้อทัวร์ #' + counter2 + ' *</strong></label>' +
                            '<input type="text" class="form-control" name="additional_service[]" id="additional_service' + counter2 + '"  required>' +
                            '</div></div>' +
                            '<div class="col-md-6">' +
                            '<div class="form-group"><label><strong>{{trans("package.Price")}}*</strong></label><input type="number" class="form-control" name="price_service[]" id="price_service' + counter + '"  required></div></div>');


                        newTextBoxDiv.appendTo("#add-txt2");
                        counter2++;
                    });

                    $("#btn-remove2").click(function () {
                        if(counter2==2){
                            alert("No more textbox to remove");
                            return false;
                        }
                        counter2--;
                        $("#TextBoxDiv" + counter2).remove();
                    });

                });


                function newDayAdd(inputDate,addDay){
                    var d = new Date(inputDate);
                    d.setDate(d.getDate()+addDay);
                    mkMonth=d.getMonth()+1;
                    mkMonth=new String(mkMonth);
                    if(mkMonth.length==1){
                        mkMonth="0"+mkMonth;
                    }
                    mkDay=d.getDate();
                    mkDay=new String(mkDay);
                    if(mkDay.length==1){
                        mkDay="0"+mkDay;
                    }
                    mkYear=d.getFullYear();
                    return mkYear+"-"+mkMonth+"-"+mkDay;
                    //  return mkMonth+"/"+mkDay+"/"+mkYear; // แสดงผลลัพธ์ในรูปแบบ เดือน/วัน/ปี
                }
                function sortValue(elm){
                    document.getElementById('Days').value=elm.value;
                    $.ajax({
                        url : "{{URL::to('package/program/sortby_days')}}",
                        type: "get",
                        data: {'id':elm.value},
                        success: function(data) {
                            $('tbody').html(data);
                        }
                    });
                }

                $('.set_status').on('click',function (e) {

                    if($(this).is(":checked")){
                        var status='Y';
                    }else{
                        var status="N";
                    }
                    $.ajax({
                        url : "{{URL::to('package/details/set_status')}}",
                        type: "get",
                        data: {'status':status,'id':e.target.value},
                        success: function(data) {
                            location.reload();
                        }
                    });

                });

            </script>

@endsection