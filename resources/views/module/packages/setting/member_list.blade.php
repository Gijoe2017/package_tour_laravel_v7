@extends('layouts.member.layout_master_new')
@section('header')
    <section class="content-header">
        <h1>
            Package Member List.
            <small>Preview</small>
        </h1>

    </section>
@endsection

@section('content')
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- The time line -->

                        <div class="panel panel-default">
                            <div class="panel-heading no-bg panel-settings">
                                <h3 class="panel-title">
                                    {{ trans('common.addmembers') }}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form class="navbar-form sp-navbar-form" role="search">
                                    <div class="form-group">
                                        <input type="text" data-url="{{ URL::to('api/v1/timelines') }}" class="form-control" id="add-members-package"  data-package-id="{{ $id }}" placeholder="Search for people">
                                    </div>
                                </form>
                                <div class="package-suggested-users"></div>
                            </div>


                        </div><!-- /panel -->

                </div>
                <!-- /.col -->
            </div>
        </section>
@endsection