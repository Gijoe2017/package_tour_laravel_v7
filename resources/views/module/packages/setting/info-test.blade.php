@extends('layouts.member.layout_master_editor')
@section('header')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h3><span><i class="fa fa-paint-brush"></i> Package Info. </span></h3>
        </div>
    </section>
@endsection

@section('content')






    <style type="text/css">
        .dropzone .dz-message{
            margin: 0;
        }
        .col-lg-2 {
            padding-right: 5px;
            padding-left: 5px;
        }
        .col-lg-6 {
            padding-right: 8px;
            padding-left: 8px;
        }
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            overflow: visible;
            margin: 3px 3px 25px 0;
            padding: 1px;
            float: left;
            width: 150px;
            height: 100px;

            text-align: center;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                {{--<form id="checkout-form"></form>--}}
                <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('Package\PackageController@saveInfo')}}"  method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{Session::get('package')}}">
                    <div class="box box-info">
                        <div class="box-body" style="padding: 0px;">
                            <div class="panel panel-white">
                                <div class="panel-body" style="padding-bottom: 0px;">
                                    <div class="panel-heading">
                                        <div class="col-md-12">
                                            <div class="summernote"><p>Hello World</p></div>
                                            <div class="form-group">
                                                <label for="contents">Contents</label>
                                                <textarea name="text" class="summernote" id="contents" title="Contents"></textarea>
                                            </div>

                                        <!-- <div class="col-lg-12">
                                                <div class="form-group" align="center"><hr>
                                                    <button class="btn btn-danger"><i class="fa fa-youtube"></i> Video Youtube</button>
                                                </div>
                                            </div>

                                            <div class="content40"><hr></div> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-12">
                                <a href="{{url('/package/list')}}" class="btn btn-default"><i class="fa fa-list"></i> {{trans('package.BackToList')}}</a>
                                <a href="{{url('/package/edit/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                                <button type="submit" class="btn btn-success pull-right" style="padding-right: 20px"><i class="fa fa-save"></i> {{trans('package.SaveandContinue')}}</button>
                            </div>
                        </div>

                     </div>
                </form>
            </div>
        </div>
    </section>





@stop()

