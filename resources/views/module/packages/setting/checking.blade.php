<link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('member/assets/font-awesome/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('member/assets/Ionicons/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('member/assets/dist/css/bootstrap-formhelpers.css')}}">
{{--<script src="{{asset('member/assets/bootstrap/3.3/js/jquery.js')}}"></script>--}}
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('member/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('member/assets/dist/js/bootstrap-formhelpers.js')}}"></script>
<script src="{{asset('member/assets/dist/js/bootstrap-filestyle.js')}}"></script>
    <style type="text/css">
        html, body, iframe {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
        .col-lg-6{
            padding-right: 4px;
            padding-left: 4px;
        }
        .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover{
            background-color: #29a209;
            color: #fff7ee;
        }
    </style>

    <section class="content">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class="nav navbar-nav pull-left">
                    <li><a href="{{url('package/list')}}"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a></li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="active bg-success"><a href="{{url('package/approve/public')}}"><i class="fa fa-check"></i> {{trans('common.button_public')}}</a></li>
                </ul>
            </div>
        </nav>
        <div class="row">
                @if(Session::has('message'))
                    <div class="col-lg-12"><div class="alert alert-success text-center">{{Session::get('message')}}</div> </div>
                @endif
                <div class="col-lg-6">
                    <h3 class="text-center">{{trans('common.original_package_file')}}</h3>

                    @if($PackageTourOne->original_file)
                        <iframe src="https://docs.google.com/gview?url=https://toechok.com/images/package-tour/docs/{{$PackageTourOne->original_file}}&embedded=true" frameborder="0"></iframe>
                        {{--<iframe src="https://view.officeapps.live.com/op/embed.aspx?url=https://view.officeapps.live.com/op/view.aspx?src=https%3A%2F%2Ftoechok.com%2Fimages%2Fpackage-tour%2Fdocs%2Fdocs-207.doc" frameborder="0"></iframe>--}}
                    @else
                        <div class="row">
                            <div class="col-lg-12">
                                <hr>
                            <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('Package\PackageController@upload_package_file')}}" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{Session::get('package')}}">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <label><strong>{{trans('common.upload_original_file')}}</strong></label>
                                        <input type="file" name="file"  class="filestyle" data-buttonName="btn-primary" >
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-lg-6">
                        <h3 class="text-center">{{trans('common.package_details')}}</h3>
                        <iframe src="{{url('package/details_check/'.$PackageTourOne->packageID)}}" frameborder="0">
                        </iframe>
                </div>
        </div>

    </section>
