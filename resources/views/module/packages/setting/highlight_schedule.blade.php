@extends('layouts.manage.master_full')

@section('contents')
    <link rel="stylesheet" href="{{asset('assets/css/build.css')}}" type="text/css" />
    <style type="text/css">

        .dropzone .dz-message{
            margin: 0;
        }

        .col-lg-2 {
            padding-right: 5px;
            padding-left: 5px;
            margin-bottom: 10px;
            min-height: 200px;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            overflow: visible;
            margin: 3px 3px 25px 0;
            padding: 1px;
            float: left;
            width: 150px;
            height: 100px;
            font-size: 4em;
            text-align: center;
        }
        .orderStep li{
            width: 50%;
        }

        .boximage{
            height: 100px;
            overflow: visible;
        }
        div.card:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;

        }
    </style>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                        <div class="box-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <form role="form"  action="{{action('Package\ScheduleController@updateProgram')}}" enctype="multipart/form-data" method="post" novalidate>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="Days" id="Days" value="{{isset($Program->packageDays)?$Program->packageDays:''}}" >
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="panel-heading" style="margin-top: 0px">
                                        <div class="col-md-12">
                                            <h3><i class="fa fa-calendar"></i> {{str_limit($Program->packageDetails,115)}}</h3>
                                        </div>
                                        <div class="row">
                                            <div class="w100 clearfix">
                                                <ul class="orderStep orderStepLook2">
                                                    <li class="active"><a href="{{url(Session::get('Language').'/package/highlight/mycreate')}}"> <i class="fa fa-th-list" aria-hidden="true"></i> <span> {{trans('package.HighlightsFromMyList')}} </span></a>
                                                    </li>
                                                    <li ><a href="{{url(Session::get('Language').'/package/highlight/other')}}"> <i class="fa fa-map-marker" aria-hidden="true"></i> <span> {{trans('package.HighlightsFromOtherList')}}</span> </a>
                                                    </li>
                                                </ul>
                                                <!--/.orderStep end-->
                                            </div>
                                        </div>
                                                    <div id="locationSelect" class="col-lg-12">
                                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.LocationTour')}}
                                                            <label class="checkbox-inline checkbox checkbox-info checkbox-circle" style="font-size: small"> คลิ๊ก &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

                                                                    <input id="check8" class="styled" type="checkbox" checked>
                                                                    <label for="check8"> เลือกสถานที่เพื่อเป็นไฮไลท์ของกำหนดการนี้ </label>

                                                            </label>
                                                            <span class="pull-right"><a href="{{url(Session::get('Language').'/package/location/create')}}" class="btn">+ {{trans('package.AddLocation')}}</a></span></h3>
                                                            @if(count($MyLocation))
                                                                @foreach($MyLocation as $rows)
                                                                    <?php $check=DB::table('highlight_in_schedule')
                                                                        ->where('LocationID',$rows->LocationID)
                                                                        ->where('programID',Session::get('programID'))
                                                                        ->first();
                                                                        $checked='';
                                                                        if(count($check)){
                                                                            $checked='checked';
                                                                        }
                                                                    ?>
                                                                    <div class="col-lg-2 card " align="center" >

                                                                        <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class=" boximage hoverable img-rounded img-responsive">
                                                                        <p>
                                                                            @if($rows->LocationName)
                                                                                {{str_limit($rows->LocationName,25)}} <span class="pull-right"><a href="{{url(Session::get('Language').'/package/location/edit/'.$rows->LocationID)}}"><i class="fa fa-edit"></i> </a> </span>
                                                                            <div class="checkbox checkbox-info checkbox-circle">
                                                                            <input class="styled" id="Highlight{{$rows->LocationID}}" name="Highlight" onclick="UpdateHighlight(this,'{{$rows->LocationID}}')" type="checkbox" value="{{$rows->LocationID}}" {{$checked}} ><label for="check8">เลือกเป็นไฮไลท์ </label>
                                                                            </div>
                                                                            @else
                                                                                <a href="{{url(Session::get('Language').'/package/location/edit/'.$rows->LocationID)}}"><span style="color: red">Update Information</span> </a>
                                                                            @endif
                                                                        </p>
                                                                    </div>

                                                                @endforeach
                                                            @else
                                                                <h3 class="text-center">Have No Data.</h3>
                                                            @endif

                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <hr>

                                                    <div id="HighlightUse" style="display:{{count($HighlightSchedule)>0?'':'none'}} " class="col-lg-12">
                                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightOfSchedule')}}
                                                            <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/location')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                                        </h3>
                                                        <div  id="ShowHighlightUse">
                                                            @foreach($HighlightSchedule as $rows)
                                                                <div class="col-lg-2 card boximage">
                                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                    <p>{{str_limit($rows->LocationName,25)}}
                                                                        <span class="pull-right"><a onclick="deleteHighlight('{{$rows->LocationID}}')" href="#"><i class="fa fa-trash"></i></a></span>
>>>>>>> b04e084aab52e43fd5985107c93b06ee48406a87
                                                                    </p>
                                                                </div>
                                                            @endforeach
<<<<<<< HEAD
                                                        @else
                                                            <h3 class="text-center">Have No Data.</h3>
                                                        @endif

                                                </div>
                                                <div class="clearfix"></div>
                                                <hr>

                                                <div id="HighlightUse" style="display:{{isset($HighlightSchedule)>0?'':'none'}} " class="col-lg-12">
                                                    <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightOfSchedule')}}
                                                        <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/location')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                                    </h3>
                                                    <div  id="ShowHighlightUse">
                                                        @foreach($HighlightSchedule as $rows)
                                                            <div class="col-lg-2 card boximage">
                                                                <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                <p>{{str_limit($rows->LocationName,25)}}
                                                                    <span class="pull-right"><a onclick="deleteHighlight('{{$rows->LocationID}}')" href="#"><i class="fa fa-trash"></i></a></span>
                                                                </p>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <hr>
                                                <div id="itemSelect" class="col-lg-12">
                                                    <h3><i class="fa fa-picture-o"></i> {{trans('LItem.itemsAll')}}
                                                        <span style="font-size: small">* เลือก{{trans('LItem.itemsAll')}}เพื่อเป็นไฮไลท์ของกำหนดการนี้</span>
                                                        <span class="pull-right"><a href="{{url('Item/create')}}" class="btn  ">+ {{trans('LItem.AddNewItem')}}</a></span> </h3>
                                                        @if(isset($MyItems))
                                                            @foreach($MyItems as $rows)
                                                            <?php $check=DB::table('highlight_item_in_schedule')
                                                                    ->where('ItemID',$rows->ItemID)
                                                                    ->where('programID',Session::get('programID'))
                                                                    ->first();
                                                            $checked='';
                                                            if(isset($check)){
                                                                $checked='checked';
                                                            }
                                                            ?>
                                                            <div class=" col-lg-2 card" align="center">
                                                                <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                <p>{{str_limit($rows->Title,25)}}
                                                                <div class="checkbox checkbox-info checkbox-circle">
                                                                    <input class="styled" id="Highlight{{$rows->ItemID}}" name="Highlight" onclick="UpdateHighlightItem(this,'{{$rows->ItemID}}')" type="checkbox" value="{{$rows->ItemID}}" {{$checked}} ><label for="check8">เลือกเป็นไฮไลท์ </label>
                                                                </div>
                                                                </p>
                                                                </div>
                                                             @endforeach
                                                        @else
                                                            <h3 class="text-center">Have No Data.</h3>
                                                        @endif

                                                </div>
                                                <div class="clearfix"></div><hr>

                                                <div id="HighlightItemUse" style="display:{{isset($HighlightItemSchedule)>0?'':'none'}} " class="col-lg-12">
                                                    <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightItemOfSchedule')}}
                                                        <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/item')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                                    </h3>
                                                    <div  id="ShowHighlightItemUse">
                                                        @foreach($HighlightItemSchedule as $rows)
                                                            <div class="col-lg-2 card" align="center">
                                                                <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                <p>{{str_limit($rows->Title,25)}}
                                                                    <span class="pull-right"><a onclick="deleteHighlightItem('{{$rows->ItemID}}')" href="#"><i class="fa fa-trash"></i></a></span>
                                                                </p>
                                                            </div>
                                                        @endforeach
=======
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <hr>
                                                    <div id="itemSelect" class="col-lg-12">
                                                        <h3><i class="fa fa-picture-o"></i> {{trans('LItem.itemsAll')}}
                                                            <span style="font-size: small">* เลือก{{trans('LItem.itemsAll')}}เพื่อเป็นไฮไลท์ของกำหนดการนี้</span>
                                                            <span class="pull-right"><a href="{{url('Item/create')}}" class="btn  ">+ {{trans('LItem.AddNewItem')}}</a></span> </h3>
                                                            @if(count($MyItems))
                                                                @foreach($MyItems as $rows)
                                                                <?php $check=DB::table('highlight_item_in_schedule')
                                                                        ->where('ItemID',$rows->ItemID)
                                                                        ->where('programID',Session::get('programID'))
                                                                        ->first();
                                                                $checked='';
                                                                if(count($check)){
                                                                    $checked='checked';
                                                                }
                                                                ?>
                                                                <div class=" col-lg-2 card" align="center">
                                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                    <p>{{str_limit($rows->Title,25)}}
                                                                    <div class="checkbox checkbox-info checkbox-circle">
                                                                        <input class="styled" id="Highlight{{$rows->ItemID}}" name="Highlight" onclick="UpdateHighlightItem(this,'{{$rows->ItemID}}')" type="checkbox" value="{{$rows->ItemID}}" {{$checked}} ><label for="check8">เลือกเป็นไฮไลท์ </label>
                                                                    </div>
                                                                    </p>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <h3 class="text-center">Have No Data.</h3>
                                                            @endif

                                                    </div>
                                                    <div class="clearfix"></div><hr>

                                                    <div id="HighlightItemUse" style="display:{{count($HighlightItemSchedule)>0?'':'none'}} " class="col-lg-12">
                                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightItemOfSchedule')}}
                                                            <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/item')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                                        </h3>
                                                        <div  id="ShowHighlightItemUse">
                                                            @foreach($HighlightItemSchedule as $rows)
                                                                <div class="col-lg-2 card" align="center">
                                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                                    <p>{{str_limit($rows->Title,25)}}
                                                                        <span class="pull-right"><a onclick="deleteHighlightItem('{{$rows->ItemID}}')" href="#"><i class="fa fa-trash"></i></a></span>
                                                                    </p>
                                                                </div>
                                                            @endforeach
                                                        </div>
>>>>>>> b04e084aab52e43fd5985107c93b06ee48406a87
                                                    </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                        <a href="{{url('package/schedule')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>


                                </div>

                            </div>

                                </form>

                        </div>


                </div>
            </div>
        </div>
    </section>


<script language="javascript">
    function deleteHighlight(id) {
        if (confirm('Do you want to delete this!')) {
            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/delschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightUse').show();
                    }else{
                        $('#HighlightUse').hide();
                    }
                    $('#ShowHighlightUse').html(data);
                    $("#locationSelect").load(location.href + " #locationSelect");
                }
            });
        }
    }
    function deleteHighlightItem(id) {
        if (confirm('Do you want to delete this!')) {
            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/Item/delschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightItemUse').show();
                    }else{
                        $('#HighlightItemUse').hide();
                    }
                    $('#ShowHighlightItemUse').html(data);
                    $("#itemSelect").load(location.href + " #itemSelect");
                }
            });
        }
    }

    function UpdateHighlight(elm,id){

        if(elm.checked){
            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/addschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightUse').show();
                    }else{
                        $('#HighlightUse').hide();
                    }
                    $('#ShowHighlightUse').html(data);
                }
            });
        }else{

            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/delschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightUse').show();
                    }else{
                        $('#HighlightUse').hide();
                    }
                    $('#ShowHighlightUse').html(data);
                }
            });
        }
    }

    function UpdateHighlightItem(elm,id){

        if(elm.checked){

            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/Item/addschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightItemUse').show();
                    }else{
                        $('#HighlightItemUse').hide();
                    }
                    $('#ShowHighlightItemUse').html(data);
                }
            });
        }else{

            $.ajax({
                type: 'get',
                url: '{{URL::to('ajax/Highlight/Item/delschedule')}}',
                data: {'id': id},
                success: function (data) {
                    if(data!=""){
                        $('#HighlightItemUse').show();
                    }else{
                        $('#HighlightItemUse').hide();
                    }
                    $('#ShowHighlightItemUse').html(data);
                }
            });
        }
    }


    function showImage(id) {
        $.ajax({
            type:'get',
            url : '{{URL::to('package/picture/showImage')}}',
            data : {'picID':id},
            success:function(data){
                $('#showPics').html(data);
                $('#dropzoneFileUpload').css({'width':'170px'});
                $('#dropzoneFileUpload').css({'height':'130px'});
                $('#dropzoneFileUpload').html(' ');
                $('#dropzoneFileUpload').css("background","url({{asset('images/default-add-sm.png')}})");

                $("#Manage-image").load(location.href + " #showPics");
            }

        });


    }

    $(function () {
        $('#sortable').sortable({
            update: function (event, ui) {
                var data1 = $(this).sortable('serialize');
                $.ajax({
                    data: data1,
                    type: 'get',
                    url : '{{URL::to('program/Images/sortable')}}',
                    success: function (data) {
                        $('#blah').attr('src',data);
                        $("#Manage-image").load(location.href + " #showPics");
                    }
                });
            }
        });
//            $( "#sortable" ).sortable();
        $("#sortable").disableSelection();
    });


    var baseUrl = "{{ url('dropzone/programImage') }}";
    var token = "{{ Session::token() }}";

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzoneFileUpload", {
        dictDefaultMessage: "<img src='{{asset('images/default-add.jpg')}}' width='100%'>",
        url: baseUrl ,
        params: {
            _token: token
        },
        success: function(file, response){
            console.log('WE NEVER REACH THIS POINT.');
            var id='show';
            document.getElementById('dropzoneFileUpload').style.backgroundColor = 'write';

            showImage(id);
            $("#sortable").disableSelection();

        },
    });
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        addRemoveLinks: true,
        accept: function(file, done) {

        },

    };

    var simplemde = new SimpleMDE(
            { element: document.getElementById("packageDetails"),
                toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
            });
</script>

@stop()
