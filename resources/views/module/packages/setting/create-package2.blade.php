<!-- daterange picker -->
{{--<link rel="stylesheet" href="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.css')}}">--}}
@extends('layouts.member.layout_master_2019')

@section('content')
    <link rel="stylesheet" href="{{asset('member/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>
    <script src="{{asset('member/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('member/bootstrap-datepicker/locales/bootstrap-datepicker.'.App::getLocale().'.min.js')}}"></script>


    <section class="content">
<form role="form" id="AutoForm"  action="{{action('Package\PackageController@saveDetail')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="Days" id="Days" value="{{Session::get('days')}}" >

        <div class="panel panel-default">
        <div class="panel-header">
       <h3 class="modal-title-site text-center"> กำหนดตารางการเดินทาง / ราคา </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<label><strong>{{trans('package.DateStart')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>--}}
                        {{--<input type="text" class="form-control"  name="packageDateStart" id="datetimepicker4" required>--}}
                        {{--<script type="text/javascript">--}}
                            {{--$(function () {--}}
                                {{--$('#datetimepicker4').datetimepicker({--}}
                                    {{--//viewMode: 'years',--}}
                                    {{--format: 'YYYY-MM-DD',--}}
                                {{--});--}}
                            {{--});--}}
                        {{--</script>--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<label><strong>{{trans('package.DateEnd')}}*</strong></label><BR>--}}
                        {{--<input type="text" class="form-control" name="packageDateEnd" id="packageDateEnd" readonly required>--}}
                    {{--</div>--}}
                {{--</div>--}}
<div class="col-lg-12">
    <div class="row">
                <div class="col-md-8">
                    <div id="sandbox-container" class="form-group">
                    <label><strong>{{trans('package.Daterange')}} * (ex. YYYY-MM-DD)</strong></label><BR>
                    {{--<div class="input-group">--}}
                        {{--<div class="input-group-addon">--}}
                            {{--<i class="fa fa-calendar"></i>--}}
                        {{--</div>--}}
                        {{--<input type="text" class="form-control pull-right" name="package_date" id="reservation">--}}
                    {{--</div>--}}
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="form-control" name="start" />
                            <span class="input-group-addon">{{trans('common.to')}}</span>
                            <input type="text" class="form-control" name="end" />
                        </div>

                    </div>


                </div>
                <div class="col-md-4"></div>
    </div>
</div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Country_Airport')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <select class="form-control selectpicker" name="Country_id" id="Country_id" >
                            <option value="">{{trans('common.choose')}}</option>
                            @foreach($country as $rows)
                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                    <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>
                    <select class="form-control" name="Airline" id="Airline" >
                        <option value="">{{trans('common.choose')}}</option>
                    </select>
                </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <input type="text" class="form-control" name="Flight" id="Flight" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople"  required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="Commission" id="Commission"  required>
                    </div>
                </div>

                <div class="col-md-4">

                    <div class="form-group">
                        <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                        <select class="form-control select2" style="width: 100%" name="unit_commission" id="unit_commission" required >
                            <option value="">{{trans('profile.Choose')}}</option>
                            <option value="%">%</option>
                            <option value="{{$Package->packageCurrency}}" >{{$Package->packageCurrency}}</option>
                        </select>
                    </div>
                </div>
            </div>
          <div class="clearfix"></div>

            <hr>
            <a class="btn btn-default  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
            <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>





    </div>
    <!-- /.modal-content -->
</form>
</section>


<script >
    $('#sandbox-container .input-daterange').datepicker({
//        maxViewMode: 0,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
//        todayHighlight: true,
        format: "yyyy-mm-dd",
        language: "th",
        beforeShowDay: function(date){
            if (date.getMonth() == (new Date()).getMonth())
                switch (date.getDate()){
                    case 4:
                        return {
                            tooltip: 'Example tooltip',
                            classes: 'active'
                        };
                    case 8:
                        return false;
                    case 12:
                        return "green";
                }
        },
        beforeShowMonth: function(date){
            if (date.getMonth() == 8) {
                return false;
            }
        }
    });

</script>




@endsection