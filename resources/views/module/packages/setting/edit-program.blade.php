<link rel="stylesheet" href="{{asset('assets/css/build.css')}}" type="text/css"  />
<link rel="stylesheet" href="{{asset('assets/icon-line-pro/style.css')}}">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload();"> &times; </button>
        <span style="font-size: 1.5em"> <i class="fa fa-calendar"></i>
                  {{trans('package.ProgramTour')}} {{$NameDay->DayName}} sss </span>
    </div>
<form role="form"  action="{{action('Package\ScheduleController@updateProgram')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="id" value="{{$Program->programID}}">
    <input type="hidden" name="Days" id="Days" value="{{$Program->packageDays}}" >
<div class="modal-body">

    <div class="col-lg-12">
        <div class="row">
            @if(isset($Program))
            <div class="col-md-6">
                <div class="form-group">
                    <label><strong>{{trans('package.Time')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                    <input type="text" class="form-control bfh-phone"  name="packageTime" id="packageTime" value="{{$Program->packageTime}}"  maxlength="23:59" data-format="dd:dd" required>
                </div>
            </div>

            <div class="col-md-6">
                <label><strong>{{trans('package.Time')}} *</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                <select class="form-control" name="packageTime2" id="packageTime2">
                    <option value="0"> {{trans('package.Choose')}} </option>
                    @foreach(App\Package\OptionTime::active()->get() as $rows)
                        <option value="{{$rows->TimeCode}}">{{$rows->Time_text}}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <div class="form-group">
                <label><strong>{{trans('package.PackageDetails')}}*</strong></label><BR>
                <textarea type="text" class="form-control summernote" name="packageDetails" id="packageDetails" required>{{$Program->packageDetails}}</textarea>
            </div>
            @else
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.Time')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                        <input type="time" class="form-control bfh-phone"  name="packageTime" id="packageTime"  data-format="dd:dd" required>
                    </div>
                </div>
                <div class="form-group">
                    <label><strong>{{trans('package.PackageHeadDetails')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Head Details.</span></label><BR>
                    <input type="text" class="form-control" name="packageTitle" id="packageTitle"  >
                </div>
                <div class="form-group">
                    <label><strong>{{trans('package.PackageDetails')}}*</strong></label><BR>
                    <textarea type="text" class="form-control summernote" name="packageDetails" id="packageDetails" required></textarea>
                </div>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="modal-footer">
    <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
</div>


    </form>
<script src="{{asset('assets/js/bootstrap-filestyle.min.js')}}"></script>

<script language="javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        content: function(){return '<img src="'+$(this).data('img') + '" />';}
    });
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>