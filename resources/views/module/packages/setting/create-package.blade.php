<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.css')}}">

{{--<link rel="stylesheet" href="{{asset('member/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">--}}



{{--<script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>--}}
{{--<script src="{{asset('member/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}}
{{--<script src="{{asset('member/bootstrap-datepicker/locales/bootstrap-datepicker.'.App::getLocale().'.min.js')}}"></script>--}}



<form role="form" id="AutoForm"  action="{{action('Package\PackageController@saveDetail')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="Days" id="Days" value="{{Session::get('days')}}" >


    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h3 class="modal-title-site text-center"> กำหนดตารางการเดินทาง / ราคา </h3>
        </div>
        <div class="modal-body">
            <div class="col-lg-12">
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<label><strong>{{trans('package.DateStart')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>--}}
                        {{--<input type="text" class="form-control"  name="packageDateStart" id="datetimepicker4" required>--}}
                        {{--<script type="text/javascript">--}}
                            {{--$(function () {--}}
                                {{--$('#datetimepicker4').datetimepicker({--}}
                                    {{--//viewMode: 'years',--}}
                                    {{--format: 'YYYY-MM-DD',--}}
                                {{--});--}}
                            {{--});--}}
                        {{--</script>--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<label><strong>{{trans('package.DateEnd')}}*</strong></label><BR>--}}
                        {{--<input type="text" class="form-control" name="packageDateEnd" id="packageDateEnd" readonly required>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="col-md-12">
                    <div class="form-group">
                    <label><strong>{{trans('package.Daterange')}} * (ex. YYYY-MM-DD)</strong></label><BR>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="package_date" id="reservation">
                    </div>
                    </div>

                    {{--<div class="input-daterange input-group" id="datepicker">--}}
                        {{--<input type="text" class="input-sm form-control" name="start" />--}}
                        {{--<span class="input-group-addon">to</span>--}}
                        {{--<input type="text" class="input-sm form-control" name="end" />--}}
                    {{--</div>--}}
                </div>


                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Country_Airport')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <select class="form-control selectpicker" name="Country_id" id="Country_id" >
                            <option value="">{{trans('common.choose')}}</option>
                            @foreach($country as $rows)
                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                    <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>
                    <select class="form-control" name="Airline" id="Airline" >
                        <option value="">{{trans('common.choose')}}</option>
                    </select>
                </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <input type="text" class="form-control" name="Flight" id="Flight" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople"  required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="Commission" id="Commission"  required>
                    </div>
                </div>

                <div class="col-md-4">

                    <div class="form-group">
                        <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                        <select class="form-control select2" style="width: 100%" name="unit_commission" id="unit_commission" required >
                            <option value="">{{trans('profile.Choose')}}</option>
                            <option value="%">%</option>
                            <option value="{{$Package->packageCurrency}}" >{{$Package->packageCurrency}}</option>
                        </select>
                    </div>
                </div>
            </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
            <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>





    </div>
    <!-- /.modal-content -->
</form>
<!-- jQuery 3 -->
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('member/assets/moment/min/moment.min.js')}}"></script>
<script src="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var counter = 2;
        var counter2 = 2;
        $("#add").click(function () {
            if(counter>10){
                alert("Only 10 textboxes allow");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
            {{--newTextBoxDiv.after().html('<div class="col-md-12"><hr></div>' +--}}
                {{--'<div class="col-md-12">' +--}}
                {{--'<div class="form-group">' +--}}
                {{--'<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +--}}
                {{--'<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +--}}
                {{--'</div></div>' +--}}
                {{--'<div class="col-md-6">' +--}}
                {{--'<div class="form-group"><label><strong>{{trans("package.PriceSale")}}*</strong></label><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div><div class="col-md-6"><label><strong>{{trans("package.PriceAndTicket")}}*</strong></label><input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket' + counter + '"  required></div></div>');--}}

            newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');


            newTextBoxDiv.appendTo("#add-txt");
            counter++;
        });

        $("#btn-remove").click(function () {
            if(counter==2){
                alert("No more textbox to remove");
                return false;
            }
            counter--;
            $("#TextBoxDiv" + counter).remove();
        });


        $("#add2").click(function () {
            if(counter2>10){
                alert("Only 10 textboxes allow");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter2);
            {{--newTextBoxDiv.after().html('<div class="col-md-12"><hr></div>' +--}}
                {{--'<div class="col-md-12">' +--}}
                {{--'<div class="form-group">' +--}}
                {{--'<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +--}}
                {{--'<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +--}}
                {{--'</div></div>' +--}}
                {{--'<div class="col-md-6">' +--}}
                {{--'<div class="form-group"><label><strong>{{trans("package.PriceSale")}}*</strong></label><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div><div class="col-md-6"><label><strong>{{trans("package.PriceAndTicket")}}*</strong></label><input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket' + counter + '"  required></div></div>');--}}

            newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter2 + ' *</strong>' +
                    '<input type="text" class="form-control" name="additional_service[]" id="additional_service' + counter2 + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.Price")}}*</strong><input type="number" class="form-control" name="price_service[]" id="price_service' + counter + '"  required></div></div>');


            newTextBoxDiv.appendTo("#add-txt2");
            counter2++;
        });

        $("#btn-remove2").click(function () {
            if(counter2==2){
                alert("No more textbox to remove");
                return false;
            }
            counter2--;
            $("#TextBoxDiv" + counter2).remove();
        });
    });
</script>

<script language="javascript">

    $('#Country_id').on('change',function (e) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/ajax/get-airline',
            data:{'country_id':e.target.value},
            success:function(data){

                $('#Airline').empty();
                $('#Airline').append(data);
            }
        });
    });

    function newDayAdd(inputDate,addDay){
        var d = new Date(inputDate);
        d.setDate(d.getDate()+addDay);
        mkMonth=d.getMonth()+1;
        mkMonth=new String(mkMonth);
        if(mkMonth.length==1){
            mkMonth="0"+mkMonth;
        }
        mkDay=d.getDate();
        mkDay=new String(mkDay);
        if(mkDay.length==1){
            mkDay="0"+mkDay;
        }
        mkYear=d.getFullYear();
        return mkYear+"-"+mkMonth+"-"+mkDay;
        //  return mkMonth+"/"+mkDay+"/"+mkYear; // แสดงผลลัพธ์ในรูปแบบ เดือน/วัน/ปี
    }

    function sortValue(elm){
        document.getElementById('Days').value=elm.value;
        $.ajax({
            url : "{{URL::to('package/program/sortby_days')}}",
            type: "get",
            data: {'id':elm.value},
            success: function(data) {
                $('tbody').html(data);
            }
        });
    }

    $('#datetimepicker4').on('blur',function(e){
        var txtdate = e.target.value;
        var DateEnd = newDayAdd(txtdate,{{ @$Default->packageDays-1 }});
        document.getElementById('packageDateEnd').value = DateEnd;
    });

    $('#saveSchedule').on('click',function (e) {
        var days=document.getElementById('packageDays').value;
        if(document.getElementById('packageTime').value==''){
            document.getElementById('packageTime').focus();
            document.getElementById('error1').style.display='';
            return false;
        }else  if(document.getElementById('packageTitle').value==''){
            document.getElementById('packageTitle').focus();
            document.getElementById('error2').style.display='';
            return false;
        }else{
            document.getElementById('error1').style.display='none';
            document.getElementById('error2').style.display='none';
            document.getElementById('Days').value=days;
            time=document.getElementById('packageTime').value;
            Order=document.getElementById('packageOrder').value;
            Title=document.getElementById('packageTitle').value;
            Detail=document.getElementById('packageDetails').value;
            $.ajax({
                type:'get',
                url : '{{URL::to('Subj/setofselect')}}',
                data : {'time':time,'Order':Order,'Title':Title,'Detail':Detail},
                success:function(data){

                }
            });
            return true;
        }
    });

//    Date range picker
    $('#reservation').daterangepicker({
        locale: {
        format: 'YYYY/MM/DD'
        }
    });

//    var simplemde = new SimpleMDE(
//        { element: document.getElementById("packageDetails"),
//            toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
//        });
</script>