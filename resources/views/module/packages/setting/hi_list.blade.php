@extends('layouts.manage.master_full')

@section('contents')
    <link rel="stylesheet" href="{{asset('assets/css/build.css')}}" type="text/css" />
    <style type="text/css">

        .dropzone .dz-message{
            margin: 0;
        }

        .col-lg-2 {
            padding-right: 5px;
            padding-left: 5px;
            margin-bottom: 10px;
            min-height: 200px;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            overflow: visible;
            margin: 3px 3px 25px 0;
            padding: 1px;
            float: left;
            width: 150px;
            height: 100px;
            font-size: 4em;
            text-align: center;
        }
        .orderStep li{
            width: 50%;
        }

        .boximage{
            height: 100px;
            overflow: visible;
        }
        div.card:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    @if (isset($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form"  action="{{action('Package\ScheduleController@updateProgram')}}" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="Days" id="Days" value="{{$Program->packageDays}}" >
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading" style="margin-top: 70px">
                                    <div class="col-md-12">
                                        <h3><i class="fa fa-calendar"></i> {{str_limit($Program->packageDetails,115)}}</h3>
                                    </div>
                                    <div class="row">
                                        <div class="w100 clearfix">
                                            <ul class="orderStep orderStepLook2">
                                                <li ><a href="{{url(Session::get('Language').'/package/highlight/mycreate')}}"> <i class="fa fa-th-list" aria-hidden="true"></i> <span> {{trans('package.HighlightsFromMyList')}} </span></a>
                                                </li>
                                                <li class="active"><a href="{{url(Session::get('Language').'/package/highlight/other')}}"> <i class="fa fa-map-marker" aria-hidden="true"></i> <span> {{trans('package.HighlightsFromOtherList')}}</span> </a>
                                                </li>
                                            </ul>
                                            <!--/.orderStep end-->
                                        </div>
                                        <div class="alert alert-info">
                                            <div class="row">
                                        <div class="col-lg-3" style="margin-bottom: 15px">
                                            <select class="form-control" id="Country" name="Country" data-live-search="true" data-style="btn-info">
                                                <option>-- Choose Country --</option>
                                                @foreach($Country as $rows)
                                                    <option value="{{$rows->CountryID}}">{{$rows->Country}}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                            <div class="col-lg-3" style="margin-bottom: 15px">
                                            <select class="form-control" id="State" name="State" data-live-search="true" data-style="btn-info">
                                                <option>-- Choose State --</option>
                                                <option>-- test1 --</option>

                                            </select>
                                                </div>
                                                <div class="col-lg-3" style="margin-bottom: 15px">
                                            <select class="form-control" id="City" data-live-search="true" data-style="btn-info">
                                                <option>-- Choose City --</option>

                                            </select>
                                        </div>

                                            <div class="col-lg-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="textSearch" name="textSearch" placeholder="Search for Location..">
                                                  <span class="input-group-btn">
                                                    <button class="btn btn-info" id="search" type="button">Go!</button>
                                                  </span>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div id="locationSelect" class="col-lg-12">
                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.LocationTour')}}
                                            <span style="font-size: small">* เลือกสถานที่เพื่อเป็นไฮไลท์ของกำหนดการนี้</span></h3>
                                        <div id="showSearch">
                                        @if(isset($Location))
                                            @foreach($Location as $rows)
                                                <?php $check=DB::table('highlight_in_schedule')
                                                        ->where('LocationID',$rows->LocationID)
                                                        ->where('programID',Session::get('programID'))
                                                        ->first();
                                                $checked='';
                                                if(isset($check)){
                                                    $checked='checked';
                                                }
                                                ?>
                                                <div class="col-lg-2 card" align="center" >

                                                        <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive" >

                                                    {{str_limit($rows->LocationName,25)}} <span class="pull-right"><a href="{{url(Session::get('Language').'/package/location/edit/'.$rows->LocationID)}}"><i class="fa fa-edit"></i> </a> </span>
                                                    <div class="checkbox checkbox-info checkbox-circle">
                                                        <input class="styled" id="Highlight{{$rows->LocationID}}" name="Highlight" onclick="UpdateHighlight(this,'{{$rows->LocationID}}')" type="checkbox" value="{{$rows->LocationID}}" {{$checked}} ><label for="check8">เลือกเป็นไฮไลท์ </label>
                                                    </div>


                                                </div>
                                            @endforeach
                                        @else
                                            <h3 class="text-center">Have No Data.</h3>
                                        @endif

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                        </div>

                                    <div id="HighlightUse" style="display:{{isset($HighlightSchedule)>0?'':'none'}} " class="col-lg-12 ">
                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightOfSchedule')}}
                                            <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/location')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                        </h3>
                                        <div  id="ShowHighlightUse">
                                            @foreach($HighlightSchedule as $rows)

                                                <div class="col-lg-2 card" align="center">
                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                    <p class="text-center">{{str_limit($rows->LocationName,25)}}
                                                        <span class="pull-right"><a onclick="deleteHighlight('{{$rows->LocationID}}')" href="#"><i class="fa fa-trash"></i></a></span>
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>


                                    <div class="clearfix"></div>
                                    <hr>
                                    <div id="itemSelect" class="col-lg-12">
                                        <h3><i class="fa fa-picture-o"></i> {{trans('LItem.itemsAll')}}
                                            <span style="font-size: small">* เลือก{{trans('LItem.itemsAll')}}เพื่อเป็นไฮไลท์ของกำหนดการนี้</span></h3>


                                        @if(isset($Items))
                                            @foreach($Items as $rows)
                                                <?php $check=DB::table('highlight_item_in_schedule')
                                                        ->where('ItemID',$rows->ItemID)
                                                        ->where('programID',Session::get('programID'))
                                                        ->first();
                                                $checked='';
                                                if(isset($check)){
                                                    $checked='checked';
                                                }
                                                ?>
                                                <div class="col-lg-2 card" align="center">
                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="boximage img-rounded img-responsive">
                                                    <p align="center">{{str_limit($rows->Title,25)}}
                                                    <div class="checkbox checkbox-info checkbox-circle">
                                                        <input class="styled" id="Highlight{{$rows->ItemID}}" name="Highlight" onclick="UpdateHighlightItem(this,'{{$rows->ItemID}}')" type="checkbox" value="{{$rows->ItemID}}" {{$checked}} ><label for="check8">เลือกเป็นไฮไลท์ </label>
                                                    </div>
                                                    </p>
                                                </div>
                                            @endforeach
                                        @else
                                            <h3 class="text-center">Have No Data.</h3>
                                        @endif

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div><hr>

                                    <div id="HighlightItemUse" style="display:{{isset($HighlightItemSchedule)>0?'':'none'}} " class="col-lg-12">
                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i> {{trans('package.HighlightItemOfSchedule')}}
                                            <span  class="pull-right small"> <a  data-toggle="modal" href="{{url('ajax/swap/item')}}"  data-target="#popupForm"><i class="fa fa-exchange" aria-hidden="true"></i> {{trans('LItem.Swapposition')}}</a></span>
                                        </h3>
                                        <div  id="ShowHighlightItemUse">
                                            @foreach($HighlightItemSchedule as $rows)
                                                <div class="col-lg-2 card" align="center">
                                                    <img src="{{$rows->image_path.'/small/'.$rows->Image}}" class="img-rounded img-responsive">
                                                    <p>{{str_limit($rows->Title,25)}}
                                                        <span class="pull-right"><a onclick="deleteHighlightItem('{{$rows->ItemID}}')" href="#"><i class="fa fa-trash"></i></a></span>
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <a href="{{url(Session::get('Language').'/member/package/schedule')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>


                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>



    <script language="javascript">
        $('#search').on('click',function () {
            var textSearch=$('#textSearch').val();
            $.ajax({
                type:'get',
                url:'{{URL::to('Location/searchBytext')}}',
                data:{'textSearch':textSearch},
                success:function(data){
                    $('#showSearch').html('');
                    $('#showSearch').html(data);
                }
            });
        });

        $('#textSearch').on('blur',function (e) {

            $.ajax({
                type:'get',
                url:'{{URL::to('Location/searchBytext')}}',
                data:{'textSearch':e.target.value},
                success:function(data){
                    $('#showSearch').html('');
                    $('#showSearch').html(data);
                }
            });
        });
        
        function LocationSearch(data1){
            var country=$('#Country').val();
            var state=$('#State').val();
            var city=$('#City').val();
            var txtsearch=$('#textSearch').val();

            $.ajax({
                type:'get',
                url:'{{URL::to('ajex/location/searchByCountry')}}',
                data:{'CountryID':country,'StateID':state,'CityID':city,'txtSearch':txtsearch},
                success:function(data){
                    $('#ShowHighlightUse').html(data);
                }
            });
        }


        $('#Country').on('change',function(e){
            LocationSearch();
            $.ajax({
                type:'get',
                url:'{{URL::to('ajex_city/setstate')}}',
                data:{'id':e.target.value},
                success:function(data){
                    $('#State').empty();
                    $('#City').empty();
                    $('#City').append('<promotion value="">--{{trans('profile.Choose').trans('profile.City')}}--</promotion>');
                    $('#State').append('<promotion value="">--{{trans('profile.Choose').trans('profile.State')}}--</promotion>');
                    $('#State').append(data);

                }
            });


        });


        $('#State').on('change',function(e){
            var country=document.getElementById('Country').value;
            $.ajax({
                type:'get',
                url:'{{URL::to('ajex_city/setcity')}}',
                data:{'id':e.target.value,'country':country},
                success:function(data){
                    $('#City').empty();
                    $('#City').append('<promotion value="">--{{trans('profile.Choose').trans('profile.City')}}--</promotion>');
                    $('#City').append(data);
                }
            });
        });






        function deleteHighlight(id) {
            if (confirm('Do you want to delete this!')) {
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/delschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightUse').show();
                        }else{
                            $('#HighlightUse').hide();
                        }
                        $('#ShowHighlightUse').html(data);
                        $("#locationSelect").load(location.href + " #locationSelect");
                    }
                });
            }
        }
        function deleteHighlightItem(id) {
            if (confirm('Do you want to delete this!')) {
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/Item/delschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightItemUse').show();
                        }else{
                            $('#HighlightItemUse').hide();
                        }
                        $('#ShowHighlightItemUse').html(data);
                        $("#itemSelect").load(location.href + " #itemSelect");

                    }
                });
            }
        }

        function UpdateHighlight(elm,id){

            if(elm.checked){
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/addschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightUse').show();
                        }else{
                            $('#HighlightUse').hide();
                        }
                        $('#ShowHighlightUse').html(data);
                    }
                });
            }else{

                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/delschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightUse').show();
                        }else{
                            $('#HighlightUse').hide();
                        }
                        $('#ShowHighlightUse').html(data);
                    }
                });
            }
        }

        function UpdateHighlightItem(elm,id){

            if(elm.checked){

                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/Item/addschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightItemUse').show();
                        }else{
                            $('#HighlightItemUse').hide();
                        }
                        $('#ShowHighlightItemUse').html(data);
                    }
                });
            }else{

                $.ajax({
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/Item/delschedule')}}',
                    data: {'id': id},
                    success: function (data) {
                        if(data!=""){
                            $('#HighlightItemUse').show();
                        }else{
                            $('#HighlightItemUse').hide();
                        }
                        $('#ShowHighlightItemUse').html(data);
                    }
                });
            }
        }




    </script>

@stop()
