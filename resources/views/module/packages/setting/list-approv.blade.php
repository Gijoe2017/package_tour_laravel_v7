
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h3 class="modal-title-site text-center"> {{trans('common.inspector')}} </h3>
    </div>
    <div class="modal-body">
        <div class="col-md-12">

            <table id="table-rate" class="table table-bordered table">
                <thead>
                <tr>
                    <th>{{trans('common.date')}}</th>
                    <th>{{trans('common.inspector')}}</th>

                </tr>
                </thead>
                <tbody>
                @foreach($Approve as $rows)
                    <tr>
                        <td>{{$rows->package_checked_date}}</td>
                        <td><strong>{{$rows->FirstName }}</strong></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="modal-footer">
        <a class="btn btn-default  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
    </div>
</div>
