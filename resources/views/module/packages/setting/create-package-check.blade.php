@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                <link rel="stylesheet" href="{{asset('member/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
                <form role="form" id="AutoForm"  action="{{action('Package\PackageController@saveDetail')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="Days" id="Days" value="{{Session::get('days')}}" >

    <div class="panel-content">
        <div class="panel-header">
       <h3 class="panel-title-site text-center"> {{trans('common.schedule_travel')}} </h3>
            {{--<p class="text-center">{{$PackageInfo->packageName}}</p>--}}
        </div>
        <div class="panel-body">
            <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-8">
                        <div id="sandbox-container" class="form-group">
                        <label><strong>{{trans('package.Daterange')}} * </strong></label> <small class="text-gray">(กำหนดวันที่เริ่มออกเดินทางและวันที่สิ้นสุดการเดินทาง)</small><BR>
                            <div class="input-daterange input-group" id="datepicker">
                                <input type="text" class="form-control"  name="package_date_start" autocomplete="true" />
                                <span class="input-group-addon">{{trans('common.to')}}</span>
                                <input type="text" class="form-control"  name="package_date_end" autocomplete="true" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Country_Airport')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <select class="form-control selectpicker" name="Country_id" id="Country_id" >
                            <option value="">{{trans('common.choose')}}</option>
                            @foreach($country as $rows)
                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                    <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>
                    <select class="form-control" name="Airline" id="Airline" >
                        <option value="">{{trans('common.choose')}}</option>
                    </select>
                </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                        <input type="text" class="form-control" name="Flight" id="Flight" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople"  required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="Commission" id="Commission"  required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><strong>{{trans('package.Commission_sell')}} *</strong></label><BR>
                        <input type="number" class="form-control" name="Commission_sell" id="Commission_sell"  required>
                    </div>
                </div>

                <div class="col-md-3">

                    <div class="form-group">
                        <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                        <select class="form-control select2" style="width: 100%" name="unit_commission" id="unit_commission" required >
                            <option value="">{{trans('profile.Choose')}}</option>
                            <option value="%">%</option>
                            <option value="{{$Package->packageCurrency}}" >{{$Package->packageCurrency}}</option>
                        </select>
                    </div>
                </div>
            </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            @if(Session::get('event')=='details')
                <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            @elseif(Session::has('checking'))
                <a href="{{url('package/details_check/'.Session::get('package'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            @endif
                <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </div>
    <!-- /.modal-content -->
</form>
                </div>
            </div>
        </div>
    </section>
<script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('member/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('member/bootstrap-datepicker/locales/bootstrap-datepicker.'.App::getLocale().'.min.js')}}"></script>

<script >

    $('#Country_id').on('change',function (e) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/ajax/get-airline',
            data:{'country_id':e.target.value},
            success:function(data){
                $('#Airline').empty();
                $('#Airline').append(data);
            }
        });
    });

    var today=new Date();
    $('#sandbox-container .input-daterange').datepicker({
//        maxViewMode: 0,
        startDate: today,
        forceParse: false,
//        calendarWeeks: true,
        autoclose: true,
//        todayHighlight: true,
//        format: "yyyy-mm-dd",
        language: "{{Session::get('language')}}",

    });


//    $('#package_date_end').on('change',function () {
//        $(this).val('2019-05-20');
//
//    });
//
//    function addDays(date, days) {
//        var result = new Date(date);
//        result.setDate(result.getDate() + days);
//        return result;
//    }

</script>


@stop()
