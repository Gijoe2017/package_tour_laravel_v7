@extends('layouts.member.layout_master_new')

@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 35px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 35px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 20px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 20px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
        hr.gray {
            border-top: 1px dashed  #5a7b8c;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-body">

<form role="form" id="AutoForm"  action="{{action('Package\PackageController@save_priceDetail')}}" enctype="multipart/form-data" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="panel-content">
        <div class="panel-header">
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>--}}
            <h3 class="panel-title-site text-center"> {{trans('package.edit_service_fee_rate')}}  </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.tour_buyer_type')}} <span class="text-red">*</span></strong></label>
                        <input type="text" class="form-control" name="TourType" id="TourType"  required>
                    </div>
                    @if($Package->package_partner=='Yes')
                        <div class="form-group">
                            <label><strong>{{trans('package.PriceSale')}} <span class="text-red">*</span></strong></label>
                            <input type="text" class="form-control price_sale" name="PriceSale" id="PriceSale1"  required>
                        </div>
                        <div class="form-group">
                            <label>
                                <strong>{{trans('package.PriceSale_show')}}</strong>
                            </label>
                            <input type="text" class="form-control price_sale_show text-red" name="price_show_customer" id="price_show_customer">
                            <small class="text-red">{{trans('package.insert_amount_you_need_show_customer')}}</small>
                        </div>
                    @else
                        <div class="form-group">
                            <label><strong>{{trans('package.PriceSale')}} <span class="text-red">*</span></strong></label>
                            <input type="text" class="form-control" name="price_show_customer" id="price_show_customer"  required>
                        </div>
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <hr class="gray">
                        <label class="radio-inline">
                            <div class="material-switch">
                                <input class="switch-rate"  id="price_include_visa" name="price_include_visa" type="checkbox" value="Y" />
                                <label for="price_include_visa" class="label-info"></label>
                                <span id="price_include_visa" style="padding-left: 15px"><strong>{{trans('package.Price_include_visa')}}</strong></span>
                            </div>
                            {{--<input type="checkbox" name="price_include_visa" id="price_include_visa" value="Y"> {{trans('package.Price_include_visa')}}--}}
                        </label>
                        <hr class="gray">

                    </div>

                    <div class="form-group">
                        <label class="radio-inline">
                            <div class="material-switch">
                                <input class="switch-rate"  id="price_include_vat" name="price_include_vat" type="checkbox" checked value="Y" />
                                <label for="price_include_vat" class="label-info"></label>
                                <span id="price_system_default" style="padding-left: 15px"><strong>{{trans('package.Price_include_vat')}}</strong></span>
                            </div>
                        </label>
                        <hr class="gray">
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>{{trans('package.Price_visa')}}</strong><span  class="required text-red">*</span></label>
                        <input type="text" class="form-control" name="price_for_visa" id="price_for_visa" required>
                    </div>
                    <div class="form-group">
                        <label><strong>{{trans('package.Price_visa_details')}}</strong><span  class="required text-red">*</span></label>
                        <input type="text" class="form-control" name="price_visa_details" id="price_visa_details" required>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="radio-inline">
                        <div class="material-switch">
                            <input class="switch-additional"  id="need_additional" name="need_additional" type="checkbox" value="Y" />
                            <label for="need_additional" class="label-info"></label>
                            <span id="lb_need_additional" style="padding-left: 15px"><strong>{{trans('common.needing_additional_options_for_buyers_of_this_type')}}</strong></span>
                        </div>
                    </label>
                </div>
                <div class="col-md-12">
                    <hr>
                    <div class="row addition" style="display: none">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>{{trans('common.additional')}} <span  class="required text-red">*</span></strong></label>
                                <input type="text" class="form-control" name="additional_service" id="additional_service"  >
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label><strong>{{trans('package.PriceSale')}} <span  class="required text-red">*</span></strong></label>
                                <input type="number" class="form-control" name="price_service" id="price_service"  >
                            </div>
                        </div>

                        <div class="col-md-6"><br>
                            <label class="radio-inline">
                                <div class="material-switch">
                                    <input class="switch-additional-check"  id="condition_check_first" name="condition_check_first" type="checkbox" value="Y"  />
                                    <label for="condition_check_first" class="label-info"></label>
                                    <span id="sp_condition_check_first" style="padding-left: 15px"><strong>ให้ระบบแจ้งกับลูกทัวร์ให้รอการหาคู่พักให้ <br> <span class="text-danger"> ** หากสามารถหาคู่ให้ได้ ลูกทัวร์ไม่จำเป็นจ่ายค่าบริการเสริมนี้ แต่หากหาให้ไม่ได้ลูกทัวร์จำเป็นต้องซื้อบริการเสริมนี้</span></strong></span>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default pull-left"  href="{{url('package/details/price/'.Session::get('packageDescID'))}}" aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </div>
</form>
                </div>
            </div>
        </div>
    </section>
    <script>
        
        $('body').on('click','.switch-additional',function () {
            if($(this).is(':checked')){
                $('.addition').show();
                $("#price_for_visa").attr("required", "true");
                $("#price_visa_details").attr("required", "true");
            }else{
                $('.addition').hide();
                $("#price_for_visa").removeAttr( "required" );
                $("#price_visa_details").removeAttr( "required" );
            }

        });


        $('body').on('click','.switch-rate',function () {
            if($(this).is(':checked')){
                $('.required').hide();
                $("#price_for_visa").removeAttr( "required" );
                $("#price_visa_details").removeAttr( "required" );

            }else{
                $('.required').show();
                $("#price_for_visa").attr("required", "true");
                $("#price_visa_details").attr("required", "true");
            }

        });

        $('body').on('keyup','.price_sale',function(){

            $.ajax({
                type:'get',
                url:SP_source() + 'package/set-price-package',
                data:{'price':$(this).val()},
                success:function(data){
                    $('.show_ex_price').show();
                    $('#price_show_customer').val(data);
                }
            });
        });

        $('body').on('keyup','#price_show_customer',function(){
            $.ajax({
                type:'get',
                url:SP_source() + 'package/set-price-package_back',
                data:{'price':$(this).val()},
                success:function(data){
                    $('.show_ex_price').show();

                    $('#PriceSale1').val(data);
                }
            });
        });
    </script>
    @endsection