@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')

    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>


    <style type="text/css">
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
    </style>


        <section class="content">
            <div class="row">



                <div class="col-md-12">

                    <div class="panel">
                        <div class="panel-heading">
                            <form id="form-set-country"  method="post" action="{{action('Package\PackageController@sort_by_country')}}">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-2 text-right">
                                        <label>Filter By:</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" >
                                            <select name="country" id="country" class="form-control">
                                                <option value="">Choose Country</option>
                                                <option value="all" {{isset($country)?$country=='all'?'Selected':'':''}}>All Country</option>
                                                @foreach($PackageCountry as $rows)
                                                    <option value="{{$rows->country_id}}" {{isset($country)?$country==$rows->country_id?'Selected':'':''}}>{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="txtsearch" name="txtsearch" value="{{isset($txtsearch)?$txtsearch:''}}" placeholder="Search by name">
                                    </div>
                                    <div class="col-md-1">
                                        <div class="input-group-append pull-left">
                                            <button class="btn btn-success" type="submit">Go</button>
                                        </div>
                                    </div>

                                    {{--<div class="col-md-4">--}}
                                    {{--<div class="input-group">--}}

                                    {{--<input type="text" name="txtsearch" id="txtsearch" class="form-control" placeholder="Search by name">--}}

                                    {{--<div class="input-group-append">--}}
                                    {{--<span class="input-group-text"><i class="fa fa-search"></i> </span>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="infinite-scroll">
                        <div class="card-columns">
                            @foreach($Packages as $rows)
                                   <div class="card card-pin">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            {{$rows->packageName}}
                                        </div>
                                        <div class="panel-body">
                                            <a href="{{url('package/details/'.$rows->packageID)}}">
                                                <img class="img-thumbnail" src="{{asset('images/package-tour/mid/'.$rows->Image)}}">
                                            </a>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="timeline-footer text-center">
                                                <?php
                                                $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                                                $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                                                $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                                                $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                                                ?>
                                                {{--@if($check>0)--}}
                                                {{--<a href="{{url('package/users/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Members</a>--}}
                                                <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Preview</a>
                                                {{--@endif--}}
                                                <a href="{{url('package/edit/'.$rows->packageID)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                                @if($check_del==0 && $check_del2==0 && $check_owner==1)
                                                    <a href="{{url('package/delete/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                    {{$Packages->render()}}

                </div>
                </div>
                </div>
                <!-- /.col -->
            </div>
        </section>
        {{--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>--}}



@endsection
