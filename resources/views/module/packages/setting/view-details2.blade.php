@extends('layouts.member.layout_master_new')
@section('content')
    <link rel="stylesheet" href="{{asset('package/css/style.d843a54abab569eaede4ed3a69a9b943.css')}}">

    <style>
        .panel-post .panel-heading .post-author .user-avatar {
            width: 40px;
            height: 40px;
            float: left;
            margin-right: 10px;
        }
        .panel-post .panel-heading .post-author .post-options {
            float: right;
            color: #859AB5;
            margin-top: 6px;
        }
        .col-md-1{
            padding-left: 0;
            padding-right: 0;
            text-align: center;
        }
        p{
            font-size: 14px;
        }
        .alert{
            padding: 10px;
            margin-bottom: 5px;
        }
        hr{
            margin-bottom: 8px;
            margin-top: 8px;
        }
        .navigation{

            background-color: rgba(255, 0, 0, 0.8);

            color: rgba(255, 0, 0, 0.8);
            position:absolute;
            bottom:0;
            position: fixed;
            color: white;
            z-index: 100;
            padding: 10px 150px;

        }
    </style>
    <div class="row">
        <div class="col-md-12 navigation" >
            >> <a href="#detail">บรรยายรายละเอียด</a> >> <a href="#schedule">กำหนดตารางการเดินทาง</a> >> <a href="#condition">เงื่อนไข</a>
        </div>
    </div>

            <div class="content">

                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                                <div class="box box-info">
                                <div class="box-body">
                                    <div class="col-sm-12 border-right">
                                        <article class="gallery-wrap">
                                            <div class="img-big-wrap">
                                                <div class="text-center">
                                                    <a target="_blank" href="{{asset('images/package-tour/mid/'.$PackageTourOne->Image)}}">
                                                        <img class="img-responsive" src="{{url('/package/tour/mid/'.$PackageTourOne->Image)}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- slider-product.// -->
                                            {{--<div class="img-small-wrap">--}}
                                                {{--<div class="item-gallery"> <img src="images/banners/slide1-2.jpg"></div>--}}
                                                {{--<div class="item-gallery"> <img src="images/banners/slide1-3.jpg"></div>--}}
                                                {{--<div class="item-gallery"> <img src="images/banners/slide1-2.jpg"></div>--}}
                                                {{--<div class="item-gallery"> <img src="images/banners/slide1-5.jpg"></div>--}}
                                            {{--</div> <!-- slider-nav.// -->--}}
                                        </article>
                                    </div>
                                    <?php
                                    $current=\App\Currency::where('currency_code',$PackageTourOne->packageCurrency)->first();

                                    $Detail=DB::table('package_details')
                                        ->where('packageID',$PackageTourOne->packageID)
                                        ->where('packageDateStart','>',date('Y-m-d'))
                                        ->where('status','Y')
                                        ->orderby('packageDateStart','asc')
                                        ->first();

//                                    $Details=DB::table('package_details')
//                                            ->where('packageID',$PackageTourOne->packageID)
//                                            ->where('packageDateStart','>',date('Y-m-d'))
//                                            ->where('status','Y')
//                                            ->orderby('packageDateStart','asc')
//                                            ->get();
                                    $Details=DB::table('package_details as a')
                                        //->join('airline as b','b.airline','=','a.Airline')
                                        ->join('countries as c','c.country_id','=','a.Country_id')
                                        ->where('a.packageID',$PackageTourOne->packageID)
                                        ->where('c.language_code',Auth::user()->language)
                                        ->where('a.packageDateStart','>',date('Y-m-d'))
                                        ->where('a.status','Y')
                                        ->orderby('a.packageDateStart','asc')
                                        ->get();

                                    //dd($Details);

                                    $Price=DB::table('package_details_sub')
                                            ->where('packageID',$PackageTourOne->packageID)
                                            ->where('status','Y')
                                            ->orderby('PriceSale','desc')
                                            ->first();
    //                                echo $PackageTourOne->packageID;
                                    //dd($Price);
                                    $Price_now=0;
                                    $Price_up=0;
                                    $Price_down=0;
                                    //****************** Update Status if promotion finish **********************//
                                    DB::table('package_promotion')
                                            ->where('promotion_date_end','<',date('Y-m-d'))
                                            ->where('promotion_status','Y')
                                            ->where('promotion_operator','Between')
                                            ->update(['promotion_status'=>'N']);
                                    //****************** Update Status if promotion finish **********************//

                                    $data_target=null;
                                        if($Price){
                                    $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                            //->where('promotion_date_start','>',date('Y-m-d'))
                                            ->orderby('promotion_date_start','asc')
                                            ->first();
                                    $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                                    // dd($pricePro);
                                    $promotion_title=null;$every_booking=0;

                                    if($promotion){
                                        $every_booking=$promotion->every_booking;
                                        $promotion_title='Between';
                                        if($promotion->promotion_operator=='Between'){
                                            if($promotion->promotion_date_start<=date('Y-m-d') && $promotion->promotion_date_end>=date('Y-m-d')){
                                                if($promotion->promotion_operator2=='up'){
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_up=$rowPrice->PriceSale*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale+$Price_up]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_up=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale+$Price_up]);
                                                    }

                                                }

                                            }else{ // promotion down
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_down=$Price->PriceSale*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale-$Price_down]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_down=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale-$Price_down]);
                                                    }
                                                }

                                            }
                                                }
                                            $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                        }else{
                                            $promotion_title='Mod';
                                           // dd($rows->packageID.' '.$Price->packageDescID);
                                            $booking=DB::table('package_bookings as a')
                                                    ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                                                    ->where('b.package_id',$PackageTourOne->packageID)
                                                    ->where('b.package_detail_id',$Price->packageDescID)
                                                    ->sum('b.number_of_person');
                                            // dd($booking);
                                            if($booking>0){ // Check booking
                                                if($booking % $promotion->every_booking==1){ // every booking
                                                    if($booking>$promotion->every_booking){
                                                        if($promotion->promotion_operator2=='up'){
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_up=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                }
                                                            }
                                                            /// $Price_by_promotion+=$Price_up;
                                                        }else{
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_down=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                                                }
                                                            }
                                                        }
                                                    }

                                                }

                                            }
    //                                                    dd($Price_now);
                                        }
                                    }

                                    }



                                    $Price=DB::table('package_details_sub')
                                            ->where('packageID',$PackageTourOne->packageID)
                                            ->where('status','Y')
                                            ->orderby('PriceSale','desc')
                                            ->first();
                                      //  dd($Price);
                                    ?>

                                    <div id="detail" class="col-sm-12">
                                    <article class="card-body">
                                            <!-- short-info-wrap -->
                                            <div class="info-wrap-2">
                                                @if($PackageTourOne->timeline_id==Session::get('timeline_id'))
                                                <span class="pull-right"> <a href="{{url('/package/info/')}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a></span>
                                                @endif
                                                    <?php
                                                    $pacakge_by='';
                                                    if($PackageTourOne->package_owner=='Yes'){
                                                        $timeline=\App\Timeline::where('id',$PackageTourOne->timeline_id)->first();
                                                    }else{
                                                        $timeline=\App\Timeline::where('id',$PackageTourOne->owner_timeline_id)->first();
                                                    }

                                                    if(isset($timeline->name)){
                                                        $pacakge_by=$timeline->name;
                                                    }
                                                    ?>

                                                    <h3 class="title mb-3">{{$PackageTourOne->packageName}} <BR><small>{{trans('common.package_by')}} : {{$pacakge_by}}</small>
                                                        @if($PackageTourOne->original_file)
                                                        | <i class="fa fa-file"></i> <a href="{{asset('images/package-tour/docs/'.$PackageTourOne->original_file)}}" target="_blank">{{$PackageTourOne->original_file}}</a>
                                                        @endif
                                                    </h3>

                                                    @if($PackageTourOne->package_close=='Y')
                                                        <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$PackageTourOne->package_close)}}</p>

                                                    @elseif($PackageTourOne->packageStatus=='CP')
                                                        <?php
                                                        $user=DB::table('users_info')->where('UserID',$PackageTourOne->package_checked_by)->first();
                                                        $date= new Date($PackageTourOne->package_checked_date);
                                                        $check=DB::table('package_tour_checked_log')->where('package_id',$PackageTourOne->packageID)->count();
                                                        ?>
                                                        <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$PackageTourOne->packageStatus)}}
                                                            {{$date->format('d F Y')}} : {{trans('common.by')}} {{isset($user->FirstName)?$user->FirstName:''}}
                                                            @if($check>1)
                                                                <a href="{{url('package/ajax/list/user_approve/'.$PackageTourOne->packageID)}}" data-target="#popupForm" data-toggle="modal"> <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            @endif
                                                        </p>
                                                    @else
                                                        <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$PackageTourOne->packageStatus)}}</p>
                                                    @endif
                                                <dt>{{trans('common.description')}}</dt>
                                                <div>{!! $PackageTourOne->packageHighlight !!}</div>

                                                <hr>
                                            </div>

                                            {{--<div class="price-wrap h5">--}}
                                                {{--<var class="price h3 text-warning">--}}

                                                    {{--<span class="num">{{$current->currency_symbol}}{{number_format($Price->Price_by_promotion)}}</span>--}}
                                                {{--</var>--}}
                                                {{--<span>/ {{trans('common.per_person')}}</span>--}}
                                                {{--@if($Price->Price_by_promotion!=$Price->PriceSale)--}}
                                                {{--<br>--}}
                                                {{--<del class="price-old">{{$current->currency_symbol.number_format($Price->PriceSale)}}</del>--}}
                                                {{--@if($promotion)--}}
                                                {{--<span class="price-save">--}}
                                                    {{--{{trans('common.price_title_'.$promotion->promotion_operator2)}}--}}
                                                    {{--@if($promotion->promotion_operator2=='up')--}}
                                                        {{--{{$current->currency_symbol.number_format($Price->Price_by_promotion-$Price->PriceSale)}}--}}
                                                    {{--@else--}}
                                                        {{--{{$current->currency_symbol.number_format($Price->PriceSale-$Price->Price_by_promotion)}}--}}
                                                    {{--@endif--}}
                                                {{--</span>--}}
                                                {{--@endif--}}
                                                {{--@endif--}}
                                            {{--</div> <!-- price-wrap.// -->--}}

                                @if($Details->count()>0)
                                    <table class="table">
                                                <thead>
                                                {{--<tr>--}}
                                                    {{--<th>{{trans('common.travel')}}</th>--}}
                                                    {{--<th>{{trans('common.price')}}</th>--}}
                                                {{--</tr>--}}
                                                <tr>
                                                    <td><dt>{{trans('common.details')}}</dt></td>
                                                    <td class="text-right"><a class="btn btn-default btn-sm" href="{{url('package/check/detail/create')}}"><i class="fa fa-plus"></i> {{trans('common.add_package_details')}}</a></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                    <?php

                                                    foreach ($Details as $Detail){
                                                    $st=explode('-',$Detail->packageDateStart);
                                                    $end=explode('-',$Detail->packageDateEnd);

                                                    if($st[1]==$end[1]){
                                                        $date=\Date::parse($Detail->packageDateStart);
                                                        $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                        // dd($end[0]);
                                                    }else{
                                                        $date=\Date::parse($Detail->packageDateStart);
                                                        $date1=\Date::parse($Detail->packageDateEnd);
                                                        $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                    }

                                                    $Category=DB::table('package_details_sub')
                                                            ->where('packageID',$PackageTourOne->packageID)
                                                            ->where('packageDescID',$Detail->packageDescID)
                                                            ->get();
                                                    $Additional=DB::table('package_additional_services')
                                                        ->where('packageID',$PackageTourOne->packageID)
                                                        ->where('packageDescID',$Detail->packageDescID)
                                                        ->get();

                                                    $Airport=DB::table('airline')
                                                        ->where('airline',$Detail->Airline)
                                                        ->first();

                                                     if($Airport){
                                                         $airport=$Airport->airline_name;
                                                     }else{
                                                         $airport="";
                                                     }
                                                   // dd($Airport);

                                                    $promotion=\App\PackagePromotion::where('packageDescID',$Detail->packageDescID)->active()
                                                            ->orderby('promotion_date_start','asc')
                                                            ->first();

                                                    $data_target=null;
                                                    if($promotion && $promotion->promotion_operator!='Mod'){
                                                        $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                                    }

                                                    $booking=DB::table('package_booking_details')
                                                            ->where('package_id',$PackageTourOne->packageID)
                                                            ->where('package_detail_id',$Detail->packageDescID)
                                                            ->sum('number_of_person');

                                                    $person_booking=$Detail->NumberOfPeople;

                                                    if($booking){
                                                        $person_booking=$Detail->NumberOfPeople-$booking;
                                                    }
///dd($Detail);
                                                    ?>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p>
                                                                <strong>
                                                                    <u>{{trans('common.travel')}}</u>
                                                                </strong>
                                                                {{$package_date}}<BR>{{$Detail->country.' '.$airport.' '.$Detail->Flight}} |
                                                                {{trans('common.number_people').' '.number_format($Detail->NumberOfPeople).' '.trans('common.person')}} |
                                                                {{trans('common.company_commission').' '.number_format($Detail->Commission).' '.$Detail->unit_commission.' | '.trans('common.seller_commission').' '.number_format($Detail->Commission_sell).' '.$Detail->unit_commission }}
                                                                <span class="pull-right">
                                                                    <a href="{{url('package/check/detail/edit/'.$Detail->packageDescID)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                                    </span>
                                                            </p>
                                                            <p>
                                                                <strong>
                                                                    <u>{{trans('common.price')}}</u>
                                                                </strong>
                                                                @foreach($Category as $rows)
                                                                    {{$rows->TourType}} : {{$current->currency_symbol.number_format($rows->PriceSale)}},
                                                                @endforeach

                                                                @if($Additional->count())
                                                                    <br><u class="text-red">{{trans('common.additional')}}</u>
                                                                    @foreach($Additional as $rows)
                                                                        {{$rows->additional_service}} : {{$current->currency_symbol.number_format($rows->price_service)}},
                                                                    @endforeach
                                                                @endif
                                                                @if($PackageTourOne->timeline_id==Session::get('timeline_id'))
                                                                    <span class="pull-right">
                                                                        <a href="{{url('package/details/price/'.$Detail->packageDescID)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}/+{{trans('common.add')}}</a>
                                                                        </span>
                                                                @endif
                                                            </p>
                                                        </td>
                                                    </tr>

                                                    <?php }?>
                                                </tbody>
                                            </table>
                                @else
                                    <div class="col-md-12"><hr>
                                    <h5 class="text-red">ยังไม่มีการเซ็ตกำหนดการเดินทาง <span class="pull-right"><a class="btn btn-info" href="{{url('/package/details')}}"> <i class="fa fa-plus"></i> เพิ่มกำหนดการเดินทาง </a> </span></h5>
                                    </div>
                                @endif
                                        <!-- short-info-wrap .// -->
                                    </article> <!-- card-body.// -->
                                </div> <!-- col.// -->

                                </div>
                                </div> <!-- row.// -->
                        </div>
                    </div>
                        <!-- PRODUCT DETAIL -->
                    <div class="row">
                            <div class="col-md-12">
                                <div class="panel">
                                <div class="panel-body">

                                    <div class="col-md-12" id="schedule">

                                        <h3> <i class="fa fa-list"></i> {{trans('common.schedule_travel')}}</h3>

                                        @for($day=1;$day<=$PackageTourOne->packageDays;$day++)
                                            <?php
                                            $Programs=DB::table('package_programs as a')
                                                ->join('package_program_info as b','b.programID','=','a.programID')
                                                ->where('a.packageID',$PackageTourOne->packageID)
                                                ->where('b.LanguageCode',Session::get('language'))
                                                ->where('a.packageDays',$day)
                                                ->orderby('a.packageDays','asc')
                                                ->orderby('a.packageID','asc')
//                                                        ->orderby('a.packageTime','asc')
                                                ->get();
                                            //dd($Programs);
                                            $PackageHighlight=DB::table('package_program_highlight as a')
                                                ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
                                                ->where('b.LanguageCode',Session::get('language'))
                                                ->where('a.packageID',$PackageTourOne->packageID)
                                                ->where('a.packageDays',$day)
                                                ->orderby('packageDays','asc')
                                                ->first();
                                            //echo Session::get('language');
                                            // dd($PackageHighlight);
                                            //                                                    $Date=DB::table('package_days')->where('DayCode',$day)->where('LanguageCode',Session::get('language'))->fisrt();
                                            $Date=\App\PackageDay::where('DayCode',$day)->active()->first();

                                            ?>
                                            @if(isset($PackageHighlight))
                                                <div class="alert alert-success">

                                                    <strong>{{$Date->DayName}} {{$day}} : {{$PackageHighlight->Highlight}}</strong><BR>
                                                    <span>{{trans('common.food_for_today')}}
                                                        {!! $PackageHighlight->Breakfast=='Y'?'<i class="fa fa-check "></i>'.trans('common.breakfast'):'<i class="fa fa-close text-yellow"></i> '.trans('common.breakfast') !!},
                                                        {!! $PackageHighlight->Lunch=='Y'?'<i class="fa fa-check "></i>'.trans('common.lunch'):'<i class="fa fa-close text-yellow"></i> '.trans('common.lunch') !!},
                                                        {!! $PackageHighlight->Dinner=='Y'?'<i class="fa fa-check "></i>'.trans('common.dinner'):'<i class="fa fa-close text-yellow"></i> '.trans('common.dinner') !!}
                                                        </span>
                                                    @if($PackageTourOne->timeline_id==Session::get('timeline_id'))
                                                        <span class="pull-right">
                                                           <a  href="{{url('package/ajax/forms/edit/'.$PackageHighlight->HighlightID)}}"> <i class="fa fa-edit"></i> {{trans('common.edit')}} </a>
                                                        </span>
                                                    @endif
                                                </div>
                                            @endif

                                            @if($Programs)
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <a  href="{{url('package/ajax/program/create')}}" ><i class="fa fa-plus"></i>  {{trans('common.add_package_details')}} </a>
                                                        <hr>
                                                    </div>
                                                    @foreach($Programs as $rows)

                                                        <?php
                                                        $time_text=$rows->packageTime;
                                                        if($rows->packageTime2>0){
                                                            $Time=\App\PackageTime::where('TimeCode',$rows->packageTime2)->active()->first();
                                                            //                                                        $Time=DB::table('package_times')->where('TimeCode',$day)->where('LanguageCode',Session::get('language'))->first();
                                                            if($Time){
                                                                $time_text=$Time->Time_text;
                                                            }
                                                        }
                                                        ?>

                                                        <div class="col-md-12">
                                                            <div class="col-md-1" style="margin-bottom: 15px">
                                                                <strong class="text-warning">{{$time_text}} </strong><BR>
                                                            </div>
                                                            <div class="col-md-11">
                                                                {!! $rows->packageDetails !!}

                                                                @if($PackageTourOne->timeline_id==Session::get('timeline_id'))
                                                                    {{--<a  href="{{url('package/program/edit/'.$rows->programID)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>--}}
                                                                    <div class="text-right">
                                                                        <a href="{{url('/package/program/copy/'.$rows->programID)}}"><i class="fa fa-copy"></i> {{trans('common.copy')}}</a> |
                                                                        <a href="{{url('/package/program/highlight/'.$rows->programID)}}"><i class="fa fa-star-half-o" aria-hidden="true"></i> {{trans('common.highlight')}}</a> |
                                                                        <a href="{{url('package/program/edit/'.$rows->programID)}}"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a> |
                                                                        <a href="{{url(Session::get('Language').'/package/program/del/'.$rows->programID)}}" onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                                    </div>
                                                                    <hr>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        {{--<li><strong>23.25 น.</strong> ออกเดินทางสู่ <strong>เมืองเดลลี</strong> โดยสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) <strong>เที่ยวบินที่ TG331 </strong>(ใช้เวลาเดินทางโดยประมาณ  4.25 ชม.)(มีบริการอาหารบนเครื่อง)</li>--}}
                                                        {{--<li><strong>20.25  น. </strong>คณะมาพร้อมกันที่ <strong>สนามบินสุวรรณภูมิ</strong> ผู้โดยสารขาออกชั้น <strong>4</strong> ของสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) โดยมีเจ้าหน้าที่บริษัทและหัวหน้าทัวร์ให้การต้อนรับ และอำนวยความสะดวก</li>--}}
                                                        {{--<li><strong>23.25 น.</strong> ออกเดินทางสู่ <strong>เมืองเดลลี</strong> โดยสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) <strong>เที่ยวบินที่ TG331 </strong>(ใช้เวลาเดินทางโดยประมาณ  4.25 ชม.)(มีบริการอาหารบนเครื่อง)</li>--}}
                                                        <?php
                                                        $HighlightProgram=DB::table('highlight_in_schedule')
                                                            ->where('programID',$rows->programID)
                                                            ->get();
                                                        ?>
                                                        @if($HighlightProgram->count()>0)
                                                            <div class="col-md-12">
                                                                <div class="col-md-12">
                                                                    <h5>{{trans('package.PackageHighlight')}}</h5></div>
                                                                @foreach($HighlightProgram as $rowh)
                                                                    <?php
                                                                    $location=\App\Location::where('id',$rowh->LocationID)->first();
                                                                    $timeline=\App\Timeline::where('id',$location->timeline_id)->first();
                                                                    $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
                                                                    $media=\App\Media::where('id',$timeline->avatar_id)->first();
                                                                    $city=null;$state=null;
                                                                    $country=$timeline->country()->where('language_code',Session::get('language'))->first();
                                                                    if(!$country){
                                                                        $country=$timeline->country()->where('language_code','en')->first();
                                                                    }

                                                                    if($timeline->state_id){
                                                                        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
                                                                        if(!$state){
                                                                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code','en')->first();
                                                                        }
                                                                        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
                                                                        if(!$city){
                                                                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code','en')->first();
                                                                        }
                                                                    }
                                                                    if(!$country){
                                                                        $country=$timeline->country()->where('language_code','en')->first();
                                                                    }
                                                                    ?>

                                                                    <div class="col-md-4">
                                                                        @if($post!=null)
                                                                            <div class="card card-pin">
                                                                                <div class="panel panel-default panel-post animated" id="post{{ $post->id }}">
                                                                                    <div class="panel-heading no-bg">
                                                                                        <div class="post-author">
                                                                                            <?php
                                                                                            $date=\Date::parse($post->created_at);
                                                                                            ?>
                                                                                            <div class="user-avatar">
                                                                                                <a href="{{ url('home/'.$timeline->username) }}">
                                                                                                    @if($media!=null)
                                                                                                        <img src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                                                                    @else
                                                                                                        <img src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                                                                    @endif
                                                                                                </a>
                                                                                                {{--<a href="{{ url($post->user->username) }}"><img src="{{ $post->user->avatar }}" alt="{{ $post->user->name }}" title="{{ $post->user->name }}"></a>--}}
                                                                                                <?php
                                                                                                $star_rating=0;
                                                                                                $RateAll2=DB::table('star_rating')
                                                                                                    ->where('timeline_id',$timeline->id)
                                                                                                    ->whereIn('rate_group',[2,3,4,5])
                                                                                                    ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
                                                                                                    ->first();
                                                                                                if($RateAll2){
                                                                                                    if($RateAll2->star>0){
                                                                                                        $star_rating=round($RateAll2->star/$RateAll2->userCount,1);
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                @if($star_rating>0)
                                                                                                    @if($star_rating>=4)
                                                                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-success" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                                                                    @elseif($star_rating>=3)
                                                                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-warning" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                                                                    @elseif($star_rating<3)
                                                                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-danger" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>

                                                                                                    @endif
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="user-post-details">
                                                                                                <ul class="list-unstyled no-margin">
                                                                                                    <li>
                                                                                                        @if(isset($sharedOwner))
                                                                                                            <a href="{{ url($sharedOwner->user->username) }}" title="{{ '@'.$sharedOwner->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                                                                {{ $sharedOwner->user->name }}
                                                                                                            </a>
                                                                                                            shared
                                                                                                        @endif
                                                                                                        <a href="{{ url('home/'.$timeline->username) }}" title="{{ '@'.$post->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                                                            {{ $timeline->name }}
                                                                                                        </a>
                                                                                                        @if($post->user->verified)
                                                                                                            <span class="verified-badge bg-success">
                                                                                                                    <i class="fa fa-check"></i>
                                                                                                                </span>
                                                                                                        @endif

                                                                                                        @if(isset($sharedOwner))
                                                                                                            's post
                                                                                                        @endif

                                                                                                        @if($post->users_tagged->count() > 0)
                                                                                                            {{ trans('common.with') }}
                                                                                                            <?php $post_tags = $post->users_tagged->pluck('name')->toArray(); ?>
                                                                                                            <?php $post_tags_ids = $post->users_tagged->pluck('id')->toArray(); ?>
                                                                                                            @foreach($post->users_tagged as $key => $user)
                                                                                                                @if($key==1)
                                                                                                                    {{ trans('common.and') }}
                                                                                                                    @if(count($post_tags)==1)
                                                                                                                        <a href="{{ url($user->username) }}">{{ $user->name }}</a>
                                                                                                                    @else
                                                                                                                        <a href="#" data-toggle="tooltip" title="" data-placement="top" class="show-users-modal" data-html="true" data-heading="{{ trans('common.with_people') }}"  data-users="{{ implode(',', $post_tags_ids) }}" data-original-title="{{ implode('<br />', $post_tags) }}"> {{ count($post_tags).' '.trans('common.others') }}</a>
                                                                                                                    @endif
                                                                                                                    @break
                                                                                                                @endif
                                                                                                                @if($post_tags != null)
                                                                                                                    <a href="{{ url('home/'.$user->username) }}" class="user"> {{ array_shift($post_tags) }} </a>
                                                                                                                @endif
                                                                                                            @endforeach

                                                                                                        @endif

                                                                                                    </li>
                                                                                                    <li>
                <span>
                    <?php
                    $category=\App\Category::where('category_id',$location->category_id)->where('language_code',Session::get('language'))->first();
                    if(!$category){
                        $category=\App\Category::where('category_id',$location->category_id)->where('language_code','en')->first();
                    }

                    if($location->category_sub1_id){
                        $sub1category=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                    }

                    ?>
                    @if($location->category_sub2_id>0)
                        <?php
                        $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                            ->where('language_code',Session::get('language'))
                            ->first();
                        if(!$category_sub2){
                            $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                ->where('language_code','en')
                                ->first();
                        }
                        ?>
                        {{$category_sub2->category_sub2_name}},

                    @elseif($location->category_sub1_id>0)
                        {{ $category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first()->sub1_name }},
                    @else
                        @if($location->category_id)
                            <?php
                            $category=\App\Category::where('category_id',$location->category_id)
                                ->where('language_code',Session::get('language'))->first();
                            if($category){
                                $category=\App\Category::where('category_id',$location->category_id)
                                    ->where('language_code','en')->first();
                            }
                            ?>
                            {{$category->name}},
                        @endif
                    @endif
                </span>
                                                                                                        <span>
					  @if($city)
                                                                                                                {{ $city->city }}
                                                                                                            @endif
                                                                                                            @if($state)
                                                                                                                {{$state->state}}
                                                                                                            @endif
                                                                                                            @if($country)
                                                                                                                {{$country->country}}
                                                                                                            @endif
				    </span>
                                                                                                        @if($post->location != NULL && !isset($sharedOwner))
                                                                                                            {{ trans('common.at') }}
                                                                                                            <span class="post-place">
                                  <a target="_blank" href="{{ url('locations'.$post->location) }}">
                                      <i class="fa fa-map-marker"></i> {{ $post->location }}
                                  </a>
                                  </span>
                                                                                                    </li>
                                                                                                    @endif
                                                                                                </ul>

                                                                                            </div>
                                                                                            <div  style="font-size: 12px;padding-left: 50px; color:#859AB5 ">

                                    <span>
                                        <div class="rating-wrap">
                                            <span>
                                                (<a href="{{ url('home/'.$post->user->username) }}" class="user"> {{ $post->user->name }}</a>
                                                @if($post->rate_group!='')
                                                    {{trans('common.reviewed_on')}}
                                                    <?php
                                                    $rate_group=$post->rate_group;
                                                    if($rate_group=='1') {
                                                        $rate_group_title = trans('common.over_all_rating');
                                                    }else if($rate_group=='2'){
                                                        $rate_group_title=trans('common.inside_rating');
                                                    }else if($rate_group=='3'){
                                                        $rate_group_title=trans('common.outside_rating');
                                                    }else if($rate_group=='4'){
                                                        $rate_group_title=trans('common.toilet_rating');
                                                    }else if($rate_group=='5'){
                                                        $rate_group_title=trans('common.parking_rating');
                                                    }else if($rate_group=='6'){
                                                        $rate_group_title=trans('common.cleanliness');
                                                    }else if($rate_group=='7'){
                                                        $rate_group_title=trans('common.normal_parking');
                                                    }else if($rate_group=='8'){
                                                        $rate_group_title=trans('common.normal_toilet');
                                                    }
                                                    ?>
                                                    {{$rate_group_title}}
                                                @else
                                                    {{trans('common.posted_on')}}
                                                @endif
                                            </span>

                                            @if($post->rate_group!='')
                                                <?php
                                                $width=0;$star_rating=0;
                                                if($post->star_rating){
                                                    $width=$post->star_rating*20;
                                                    $star_rating=$post->star_rating;
                                                }
                                                ?>
                                                <ul class="rating-stars">
                                                    <li style="width:{{$width}}%" class="stars-active">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                </ul>
                                            @endif
                                            <span>{{$date->ago()}})</span>
                                        </div>
                                    </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="panel-body">
                                                                                        <div class="text-wrapper">
                                                                                            <?php
                                                                                            $links = preg_match_all("/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $post->description, $matches);

                                                                                            $main_description = $post->description;
                                                                                            ?>
                                                                                            @foreach($matches[0] as $link)
                                                                                                <?php
                                                                                                $linkPreview = new LinkPreview($link);
                                                                                                $parsed = $linkPreview->getParsed();
                                                                                                foreach ($parsed as $parserName => $main_link) {
                                                                                                    $data = '<div class="row link-preview">
                                                                                                                  <div class="col-md-3">
                                                                                                                    <a target="_blank" href="'.$link.'"><img src="'.$main_link->getImage().'"></a>
                                                                                                                  </div>
                                                                                                                  <div class="col-md-9">
                                                                                                                    <a target="_blank" href="'.$link.'">'.$main_link->getTitle().'</a><br>'.substr($main_link->getDescription(), 0, 500). '...'.'
                                                                                                                  </div>
                                                                                                                </div>';
                                                                                                }
                                                                                                $main_description = str_replace($link, $data, $main_description);
                                                                                                ?>
                                                                                            @endforeach

                                                                                            <p class="post-description">
                                                                                                {{--************************ Cannot fig here ****************************--}}
                                                                                                {!! $main_description !!}
                                                                                                {{--{!! clean($main_description) !!}--}}
                                                                                            </p>
                                                                                            <div class="post-image-holder  @if(count($post->images()->get()) == 1) single-image @endif">
                                                                                                @foreach($post->images()->get() as $postImage)
                                                                                                    @if($postImage->type=='image')
                                                                                                        @if(!file_exists(storage_path('user/gallery/mid/'.$postImage->source)))
                                                                                                            <a href="{{ url('user/gallery/'.$postImage->source) }}" data-lightbox="imageGallery.{{ $post->id }}" >
                                                                                                                <img src="{{ url('user/gallery/mid/'.$postImage->source) }}"  title="{{ $post->user->name }}" alt="{{ $post->user->name }}">
                                                                                                                {{--{{ $post->user->name }} |--}}
                                                                                                                {{--<time class="post-time timeago" datetime="{{ $post->created_at }}+00:00" title="{{ $post->created_at }}+00:00">--}}
                                                                                                                {{--{{ $post->created_at }}+00:00--}}
                                                                                                                {{--</time>--}}
                                                                                                            </a>
                                                                                                        @else
                                                                                                            <img src="{{ url('location/avatar/no-image-full.jpg') }}">
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            </div>
                                                                                            <div class="post-v-holder">
                                                                                                @foreach($post->images()->get() as $postImage)
                                                                                                    @if($postImage->type=='video')
                                                                                                        <video width="100%" preload="none" height="auto" poster="{{ url('user/gallery/video/'.$postImage->title) }}.jpg" controls class="video-video-playe">
                                                                                                            <source src="{{ url('user/gallery/video/'.$postImage->source) }}" type="video/mp4">
                                                                                                            <!-- Captions are optional -->
                                                                                                        </video>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                        @if($post->youtube_video_id)
                                                                                            <iframe  src="https://www.youtube.com/embed/{{ $post->youtube_video_id }}" frameborder="0" allowfullscreen></iframe>
                                                                                        @endif
                                                                                        @if($post->soundcloud_id)
                                                                                            <div class="soundcloud-wrapper">
                                                                                                <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $post->soundcloud_id }}&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                                                                            </div>
                                                                                        @endif
                                                                                        <ul class="actions-count list-inline">
                                                                                            @if($post->users_liked()->count() > 0)
                                                                                                <?php
                                                                                                $liked_ids = $post->users_liked->pluck('id')->toArray();
                                                                                                $liked_names = $post->users_liked->pluck('name')->toArray();
                                                                                                ?>
                                                                                                <li>
                                                                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.likes') }}"  data-users="{{ implode(',', $liked_ids) }}" data-original-title="{{ implode('<br />', $liked_names) }}"><span class="count-circle"><i class="fa fa-thumbs-up"></i></span> {{ $post->users_liked->count() }} {{ trans('common.likes') }}</a>
                                                                                                </li>
                                                                                            @endif

                                                                                            @if($post->comments->count() > 0)
                                                                                                <li>
                                                                                                    <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>{{ $post->comments->count() }} {{ trans('common.comments') }}</a>
                                                                                                </li>
                                                                                            @endif

                                                                                            @if($post->shares->count() > 0)
                                                                                                <?php
                                                                                                $shared_ids = $post->shares->pluck('id')->toArray();
                                                                                                $shared_names = $post->shares->pluck('name')->toArray(); ?>
                                                                                                <li>
                                                                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.shares') }}"  data-users="{{ implode(',', $shared_ids) }}" data-original-title="{{ implode('<br />', $shared_names) }}"><span class="count-circle"><i class="fa fa-share"></i></span> {{ $post->shares->count() }} {{ trans('common.shares') }}</a>
                                                                                                </li>
                                                                                            @endif
                                                                                        </ul>
                                                                                        <div class="text-right">
                                                                                            @if($timeline->type == 'location')
                                                                                                @if($post->rate_group>0)
                                                                                                    @if($post->date_visited_location!='0000-00-00')
                                                                                                        <small style="color: #9cc2cb"><i class="fa fa-calendar"></i> {{trans('common.date_visit_location').' '.\Date::parse($post->date_visited_location.'01:01:10')->format('j F Y')}}</small>
                                                                                                    @endif
                                                                                                @endif
                                                                                            @endif
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <!-- col // -->
                                                                @endforeach
                                                            </div>

                                                        @endif

                                                    @endforeach
                                                </div>
                                            @endif
                                        @endfor



                                    </div> <!-- tab-pane.// -->
                                    <div class="col-md-12" id="condition">
                                        <?php  $have='';?>
                                        <h3><i class="fa fa-question"></i> {{trans('common.condition_term')}}</h3>

                                        @if($Detail)
                                            <?php
                                            $ConditionGroup=DB::table('condition_group')
                                                ->orderby('order','asc')
                                                ->where('language_code',Session::get('language'))
                                                ->get();

                                           // dd($ConditionGroup);
                                            ?>

                                            @foreach ($ConditionGroup as $group)
                                                <?php
                                                $Conditions=DB::table('condition_in_package_details as a')
                                                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                                    ->where('a.packageID',$Detail->packageID)
                                                    ->where('b.condition_group_id',$group->condition_group_id)
                                                    ->where('language_code',Session::get('language'))
                                                    ->get();
                                                ?>

                                                    <?php $have='Y'?>
                                                    <h5 style="color: green">{{$group->condition_group_title}} <span class="pull-right">
                                                              <a class="btn  btn-success pull-right" href="{{url('/package/condition/list_more/'.$group->condition_group_id)}}" ><i class="fa fa-check-circle"></i> {{trans('common.add').' / '.trans('common.edit')}}</a>
                                                            {{--<a href="{{url('package/condition/create/'.$group->condition_group_id)}}"><i class="fa fa-plus"></i> {{trans('common.create_new_condition')}}</a> --}}
                                                        </span> </h5>
                                                    @if($Conditions->count())
                                                    <ul class="list">
                                                        @foreach($Conditions as $condition)
                                                            <li>{!! $condition->condition_title !!}
                                                                {{--<span class="pull-right">--}}
                                                                    {{--<a href="{{url('package/condition/begin_edit/'.$condition->condition_code)}}">--}}
                                                                        {{--<i class="fa fa-edit"></i> {{trans('common.edit')}}--}}
                                                                    {{--</a> |--}}
                                                                      {{--<a href="{{url('package/condition/begin_delete/'.$condition->condition_code)}}">--}}
                                                                        {{--<i class="fa fa-trash"></i> {{trans('common.delete')}}--}}
                                                                    {{--</a>--}}
                                                                {{--</span>--}}

                                                            </li>
                                                        @endforeach

                                                    </ul>
                                                     @endif
                                                <hr>

                                            @endforeach
                                        @endif

                                        @if($have!='Y')
                                            <h5 style="color: red">ยังไม่มีการกำหนดเงื่อนไจใด ๆ <span class="pull-right"> <a class="btn btn-info" href="{{url('/package/condition/list')}}"> <i class="fa fa-plus"></i> กำหนดเงื่อนไข</a></span> </h5>
                                        @endif


                                    </div> <!-- tab-pane.// -->




                                </div> <!-- card-body.// -->
                                </div>
                            </div> <!-- card.// -->

                        </div>

                        <!-- card.// -->

                        <!-- PRODUCT DETAIL .// -->


                    <?php
                    //dd($PackageTourOne);
                    if(Session::get('timeline_id')){
                        $timeline=\App\Timeline::where('id','=',$PackageTourOne->timeline_id)->first();
                        $location = \App\Location::where('timeline_id', '=', $PackageTourOne->timeline_id)->first();
                        // dd($timeline);
                        $getCountry=\App\Country::where('country_id',$timeline->country_id)
                            ->where('language_code',Session::get('language'))
                            ->first();
                    }
                    ?>




                <!-- row.// -->

            </div><!-- container // -->


@endsection