@extends('layouts.member.layout_master_new')
@section('header')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h3><span><i class="fa fa-paint-brush"></i> Package Info. </span></h3>
        </div>
    </section>
@endsection

@section('content')

    <style type="text/css">
        .dropzone .dz-message{
            margin: 0;
        }
        .col-lg-2 {
            padding-right: 5px;
            padding-left: 5px;
        }
        .col-lg-6 {
            padding-right: 8px;
            padding-left: 8px;
        }
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            overflow: visible;
            margin: 3px 3px 25px 0;
            padding: 1px;
            float: left;
            width: 150px;
            height: 100px;

            text-align: center;
        }
    </style>


    <section class="content">
        @if(Session::get('event')=='details')
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="box box-info">
                    <div class="box-body" style="padding: 8px;">
                        <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-success"><i class="fa fa-search-plus"></i> {{trans('common.preview')}}</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                {{--<form id="checkout-form"></form>--}}
                <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('Package\PackageController@saveInfo')}}"  method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{Session::get('package')}}">
                <div class="box box-info">
                        <div class="box-body" style="padding: 0px;">
                            <div class="panel panel-white">
                                <div class="panel-body" style="padding-bottom: 0px;">
                                    <div class="panel-heading">
                                        <div class="col-md-12">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>{{trans('common.choose_package_category')}} <span class="text-red">*</span></label>
                                                            <select name="TourCategoryID[]" id="TourCategoryID"   class="select2 form-control" required="required" multiple title="Choose Package Category...">
                                                                @foreach($TourCategory as $rows)
                                                                    <?php
                                                                    $Selected='';
                                                                    $check=DB::table('package_tour_category')
                                                                        ->where('TourCategoryID',$rows->TourCategoryID)
                                                                        ->where('packageID',Session::get('package'))
                                                                        // ->toSql();
                                                                        // dd($check);
                                                                        ->first();
                                                                    if(isset($check)){
                                                                        $Selected='Selected';
                                                                    }
                                                                    ?>
                                                                    <option value="{{$rows->TourCategoryID}}" {{$Selected}}>{{$rows->TourCategoryName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div  class="col-lg-12">

                                                        <div class="form-group">
                                                            <label>{{trans('common.choose_package_type')}} </label>
                                                            <select name="TourSubCateID[]" id="TourSubCateID"  class="select2 form-control" multiple  title="Choose Package Type...">
                                                                @foreach($TourType as $rows)
                                                                    <?php
                                                                    $Selected='';
                                                                    $check=DB::table('package_tour_category as a')
                                                                        ->join('tour_category_sub1 as b','b.groupID','=','a.TourSubCateID')
                                                                        ->where('a.TourSubCateID',$rows->groupID)
                                                                        ->where('b.LanguageCode',Auth::user()->language)
                                                                        ->where('a.packageID',Session::get('package'))
                                                                        // ->toSql();
                                                                        // dd($check);
                                                                        ->first();
                                                                    if(isset($check)){
                                                                        $Selected='Selected';
                                                                        echo Auth::user()->language;
                                                                    }
                                                                    ?>
                                                                    <option value="{{$rows->groupID}}" {{$Selected}}>{{$rows->TourCategorySub1Name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-lg-12"><hr></div> -->
                                                </div>

                                                @if(isset($packageInfo))
                                                    <div class="form-group">
                                                        <label><strong>{{trans('package.PackageName')}}<span class="text-red">*</span>  </strong></label><BR>
                                                        <input type="text" class="form-control" name="packageName" id="packageName" value=" {{$packageInfo->packageName}}" required="required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label><strong>{{trans('package.PackageHighlight')}}<span class="text-red">*</span></strong></label><BR>
                                                        <textarea type="text" class="form-control summernote" name="packageHighlight" id="packageHighlight" >{{$packageInfo->packageHighlight}}</textarea>
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label><strong>{{trans('package.PackageName')}}<span class="text-red">*</span></strong></label><BR>
                                                        <input type="text" class="form-control" name="packageName" id="packageName" required="required">
                                                    </div>

                                                    <div class="form-group">
                                                        <label><strong>{{trans('package.PackageHighlight')}}<span class="text-red">*</span></strong></label><BR>
                                                        <textarea type="text" class="form-control summernote" name="packageHighlight" id="packageHighlight" ></textarea>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="text-center">
                                                    @if(isset($TourImage))
                                                        <img src="{{url('/package/tour/small/'.$TourImage->Image)}}" id="blah" class="img-thumbnail">
                                                    @else
                                                        <img src="{{asset('images/default-add.jpg')}}" id="blah" class="img-thumbnail">
                                                    @endif
                                                </div>

                                                <div class="form-group"><label><Br></BT><strong>{{trans('common.package_tour_banner')}}</strong></label> </div>
                                                <div class="form-group">
                                                    <input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary" >
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <hr>
                                            @if(isset($Photos))
                                                <div class="col-lg-12"><h4><i class="fa fa-image"></i> Make Slide Show</h4></div>

                                                <div class="clearfix"></div>
                                                <div id="Manage-image" class="col-lg-12 col-xs-12 col-sm-12">
                                                    <div class="showPics">
                                                        <ul id="sortable" class="ui-sortable" >
                                                            @foreach($Photos as $rows)
                                                                <li id="img-{{$rows->LocationID}}">
                                                                    <img class="quickview1" style="height: 100%; max-width: 150px" src="{{$rows->image_path.'/small/'.$rows->Image}}" >
                                                                    <div class="checkbox checkbox-info checkbox-circle">
                                                                        <input class="styled" id="Highlight{{$rows->LocationID}}" name="Highlight" onclick="UpdateSlide(this,'{{$rows->LocationID}}')" type="checkbox" value="{{$rows->LocationID}}" {{$rows->makeSlideshow=='Y'?'checked':''}} ><label for="check8">เลือกเป็นสไลด์โชว์</label>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                        @endif

                                        <!-- <div class="col-lg-12">
                                                <div class="form-group" align="center"><hr>
                                                    <button class="btn btn-danger"><i class="fa fa-youtube"></i> Video Youtube</button>
                                                </div>
                                            </div>

                                            <div class="content40"><hr></div> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-12">
                                @if(Session::has('checking'))
                                    <a href="{{url('/package/details_check/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                @else
                                    <a href="{{url('/package/list')}}" class="btn btn-default"><i class="fa fa-list"></i> {{trans('package.BackToList')}}</a>
                                    <a href="{{url('/package/edit/'.Session::get('package'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                                @endif
                               <button type="submit" class="btn btn-success pull-right" style="padding-right: 20px"><i class="fa fa-save"></i> {{trans('package.SaveandContinue')}}</button>
                            </div>
                        </div>

                </div>
                </form>
            </div>
        </div>
    </section>

@stop()

