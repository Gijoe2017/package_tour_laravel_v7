@extends('layouts.member.layout_master_new')
@section('header')
    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
    </style>

    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Edit Package Detail. </span></h2>
        </div>
    </section>
@endsection
        @section('content')

    <section class="content">
                <div class="row">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div  class="alert alert-success">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-calendar"></i> {{trans('package.TravelSchedule')}} </span>
                                                    </div>

                                                </div>
                                            </div>
                                            <form role="form" id="AutoForm"  action="{{action('Package\PackageController@updatePackageDetail')}}" enctype="multipart/form-data" method="post" novalidate>
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="packageDescID" id="packageDescID" value="{{$Details->packageDescID}}" >
                                                        <div class="col-lg-12">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label><strong>{{trans('package.Daterange')}} * (ex. YYYY-MM-DD)</strong></label><BR>
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right" name="package_date" value="{{date('Y/m/d',strtotime($Details->packageDateStart))}}-{{date('Y/m/d',strtotime($Details->packageDateEnd))}}" id="reservation">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                                                    <input type="text" class="form-control" name="Flight" id="Flight" value="{{$Details->Flight}}" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>
                                                                    <input type="text" class="form-control" name="Airline" id="Airline" value="{{$Details->Airline}}" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                                                                    <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople" value="{{$Details->NumberOfPeople}}" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                                                                    <input type="number" class="form-control" name="Commission" id="Commission" value="{{$Details->Commission}}" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12"><hr>
                                                                <h3>อัตราค่าบริการทัวร์</h3>
                                                            </div>
                                                            <?php $i=1;?>
                                                            @if($Detail_sub->count()>0)
                                                            @foreach($Detail_sub as $rows)
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label> <strong>ประเภทผู้ซื้อทัวร์ #{{$i++}} *</strong></label>
                                                                        <input type="text" class="form-control" name="TourType[]" id="TourType1" value="{{$rows->TourType}}" required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label><strong>{{trans('package.PriceSale')}} *</strong></label>
                                                                        <input type="number" class="form-control" name="PriceSale[]" id="PriceSale1" value="{{$rows->PriceSale}}" required>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="col-md-6">--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label><strong>{{trans('package.PriceAndTicket')}} *</strong></label>--}}
                                                                        {{--<input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket1" value="{{$rows->PriceAndTicket}}" required>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            @endforeach
                                                            @else
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label><strong>ประเภทผู้ซื้อทัวร์ #{{$i}} *</strong></label>
                                                                        <input type="text" class="form-control" name="TourType[]" id="TourType1"  required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label><strong>{{trans('package.PriceSale')}} *</strong></label>
                                                                        <input type="number" class="form-control" name="PriceSale[]" id="PriceSale1"  required>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="col-md-6">--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label><strong>{{trans('package.PriceAndTicket')}} *</strong></label>--}}
                                                                        {{--<input type="number" class="form-control" name="PriceAndTicket[]" id="PriceAndTicket1"  required>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            @endif

                                                            <div id="add-txt"></div>

                                                            <div class="col-md-12" style="padding-top: 5px">

                                                             <div class="text-right">
                                                                <button id="add" type="button" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> {{trans('common.add')}}</button>
                                                                <button id="btn-remove" type="button" class="btn btn-sm btn-danger"><i class="fa fa-close"></i> {{trans('common.remove')}}</button>
                                                             </div>
                                                                <hr>
                                                            </div>

                                                            @if(isset($promotion))
                                                                <div class="col-md-12">
                                                                    <h3>{{trans('common.promotion')}}</h3>
                                                                    <table class="table">
                                                                        <tbody> <?php $i=1;$group=''?>
                                                                        @foreach($promotion as $rows)

                                                                            <tr>
                                                                                <td><input class="set_status" type="checkbox" name="promotion_id" value="{{$rows->promotion_id }}" {{$rows->promotion_status=='Y'?'checked':''}}></td>
                                                                                <td><div class="text-left"> {{$rows->promotion_title}}</div></td>
                                                                                <td align="right">
                                                                                    @if($rows->promotion_status=='N')
                                                                                        <a data-toggle="modal" data-target="#popupForm" class="btn btn-sm btn-warning" href="{{url('package/promotion/edit/'.$rows->promotion_id)}}" ><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                                                        <a class="btn btn-sm btn-danger" href="{{url('package/promotion/delete/'.$rows->promotion_id)}}"  onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                                                    @else
                                                                                        <label class="text-red">{{trans('common.condition_is_used')}}</label>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            @endif
                                                            <div class="col-md-12 text-right">
                                                                <a data-toggle="modal" data-target="#popupForm" href="{{url('package/detail/promotion/create')}}" type="button" class="btn btn-info"><i class="fa fa-plus"></i> {{trans('common.promotion_setting')}}</a>
                                                            </div>
                                                            <div class="col-md-12"><hr>
                                                            <h3>อัตราค่าบริการเสริม</h3>
                                                            </div>

                                                            <?php $i=1;?>
                                                            @if($Additional->count()>0)
                                                                @foreach($Additional as $rows)
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label><strong>ประเภทผู้ซื้อทัวร์ #{{$i++}} </strong></label>
                                                                            <input type="text" class="form-control" name="additional_service[]" id="additional_service" value="{{$rows->additional_service}}" required>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label><strong>{{trans('package.Price')}} </strong></label>
                                                                            <input type="number" class="form-control" name="price_service[]" id="price_service" value="{{$rows->price_service}}" required>
                                                                        </div>
                                                                    </div>

                                                                @endforeach
                                                            @else
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label> <strong>บริการประเภท #{{$i}} </strong></label>
                                                                        <input type="text" class="form-control" name="additional_service[]" id="additional_service"  required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label> <strong>{{trans('package.Price')}} </strong></label>
                                                                        <input type="number" class="form-control" name="price_service[]" id="price_service"  required>
                                                                    </div>
                                                                </div>

                                                            @endif

                                                            <div id="add-txt2"></div>
                                                            <div class="col-md-12 text-right" style="padding-top: 5px">
                                                                <button id="add2" type="button" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> {{trans('common.add')}}</button>
                                                                <button id="btn-remove2" type="button" class="btn btn-sm btn-danger"><i class="fa fa-close"></i> {{trans('common.remove')}}</button>
                                                            </div>

                                                        </div>
                                                <div class="col-md-12">
                                                    <div class="content40"><hr></div>
                                                    <a href="{{url('package/details')}}" class="btn btn-lg btn-default  pull-left"><i class="fa fa-reply"></i> Back to list</a>
                                                    <button type="submit"  class="btn btn-lg btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Update')}}</button>
                                                </div>
                                                <!-- /.modal-content -->
                                            </form>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>


<!-- jQuery 3 -->
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var counter = '<?php echo $Detail_sub->count()+1?>';
        var counter2 = '<?php echo $Additional->count()+1?>';
        $("#add").click(function () {
            if(counter>10){
                alert("Only 10 textboxes allow");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
            newTextBoxDiv.after().html(
                '<div class="col-md-6">' +
                '<div class="form-group">' +
                '<label><strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong></label>' +
                '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                '</div></div>' +
                '<div class="col-md-6">' +
                '<div class="form-group"><label><strong>{{trans("package.PriceSale")}}*</strong></label><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div></div>');
            newTextBoxDiv.appendTo("#add-txt");
            counter++;
        });

        $("#btn-remove").click(function () {
            if(counter==1){
                alert("No more textbox to remove");
                return false;
            }
            counter--;
            $("#TextBoxDiv" + counter).remove();
        });



        $("#add2").click(function () {
            if(counter2>10){
                alert("Only 10 textboxes allow");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter2);

            newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<label><strong>ประเภทผู้ซื้อทัวร์ #' + counter2 + ' *</strong></label>' +
                    '<input type="text" class="form-control" name="additional_service[]" id="additional_service' + counter2 + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><label><strong>{{trans("package.Price")}}*</strong></label><input type="number" class="form-control" name="price_service[]" id="price_service' + counter + '"  required></div></div>');


            newTextBoxDiv.appendTo("#add-txt2");
            counter2++;
        });

        $("#btn-remove2").click(function () {
            if(counter2==2){
                alert("No more textbox to remove");
                return false;
            }
            counter2--;
            $("#TextBoxDiv" + counter2).remove();
        });

    });
</script>

<script language="javascript">

    function newDayAdd(inputDate,addDay){
        var d = new Date(inputDate);
        d.setDate(d.getDate()+addDay);
        mkMonth=d.getMonth()+1;
        mkMonth=new String(mkMonth);
        if(mkMonth.length==1){
            mkMonth="0"+mkMonth;
        }
        mkDay=d.getDate();
        mkDay=new String(mkDay);
        if(mkDay.length==1){
            mkDay="0"+mkDay;
        }
        mkYear=d.getFullYear();
        return mkYear+"-"+mkMonth+"-"+mkDay;
        //  return mkMonth+"/"+mkDay+"/"+mkYear; // แสดงผลลัพธ์ในรูปแบบ เดือน/วัน/ปี
    }
    function sortValue(elm){
        document.getElementById('Days').value=elm.value;
        $.ajax({
            url : "{{URL::to('package/program/sortby_days')}}",
            type: "get",
            data: {'id':elm.value},
            success: function(data) {
                $('tbody').html(data);
            }
        });
    }

    $('.set_status').on('click',function (e) {

        if($(this).is(":checked")){
           var status='Y';
        }else{
            var status="N";
        }
        $.ajax({
            url : "{{URL::to('package/details/set_status')}}",
            type: "get",
            data: {'status':status,'id':e.target.value},
            success: function(data) {
                location.reload();
            }
        });

    });


    $('#datetimepicker4').on('blur',function(e){
        var txtdate = e.target.value;
        var DateEnd = newDayAdd(txtdate,{{ @$Default->packageDays-1 }});
        document.getElementById('packageDateEnd').value = DateEnd;
    });

    //Date range picker
    $('#reservation').daterangepicker({
        locale: {
        format: 'YYYY/MM/DD'
        }
    });


</script>
@endsection