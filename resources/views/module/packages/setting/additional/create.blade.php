@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-body">

<form role="form" id="AutoForm"  action="{{action('Package\PromotionController@save_additionalDetail')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">

    <div class="panel-content">
        <div class="panel-header">
            <h3 class="panel-title-site text-center"> กำหนดอัดราค่าบริการเสริม  </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('common.additional')}} *</strong></label>
                        <input type="text" class="form-control" name="additional_service" id="additional_service"  required>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.PriceSale')}} *</strong></label>
                        <input type="number" class="form-control" name="price_service" id="price_service"  required>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a  href="{{url('package/details/price/'.Session::get('packageDescID'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </div>
</form>
                </div></div></div></section>
    @endsection