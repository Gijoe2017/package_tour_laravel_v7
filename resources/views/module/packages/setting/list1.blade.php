@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <link rel=stylesheet href={{asset('member/waterfull/styles/main.css')}}>

        <div class="panel">
            <div class="panel-heading">
                <form id="form-set-country"  method="post" action="{{action('Package\PackageController@sort_by_country')}}">
                    {{csrf_field()}}
                <div class="row">
                    <div class="col-md-2 text-right">
                        <label>Filter By:</label>
                    </div>
                <div class="col-md-4">
                    <div class="form-group" >
                       <select name="country" id="country" class="form-control">
                           <option value="">Choose Country</option>
                           <option value="all" {{isset($country)?$country=='all'?'Selected':'':''}}>All Country</option>
                           @foreach($PackageCountry as $rows)
                               <option value="{{$rows->country_id}}" {{isset($country)?$country==$rows->country_id?'Selected':'':''}}>{{$rows->country}}</option>
                           @endforeach
                       </select>
                   </div>

                </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="txtsearch" name="txtsearch" value="{{isset($txtsearch)?$txtsearch:''}}" placeholder="Search by name">
                    </div>
                    <div class="col-md-1">
                        <div class="input-group-append pull-left">
                            <button class="btn btn-success" type="submit">Go</button>
                        </div>
                    </div>

                {{--<div class="col-md-4">--}}
                    {{--<div class="input-group">--}}

                            {{--<input type="text" name="txtsearch" id="txtsearch" class="form-control" placeholder="Search by name">--}}

                        {{--<div class="input-group-append">--}}
                            {{--<span class="input-group-text"><i class="fa fa-search"></i> </span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                </div>
                </form>
            </div>
        </div>
        <div class=container>
            @if($Packages->count()==0)
                <div class="col-md-12 text-center">
                    <h2>Have no package.</h2>
                </div>
            @else
                <div class="row" style="margin-left: -25px">
                    <div class=waterfall></div>
                </div>
            @endif
    </div>


    <script src={{asset('member/waterfull/scripts/vendor.js')}}></script>
    <script src={{asset('member/waterfull/bootstrap-waterfall.js')}}></script>

    <script id=waterfall-template type=text/template>
        @foreach($Packages as $rows)
        <ul class="list-group">
            <li class="list-group-item text-center">
                <div class="panel-body">{{$rows->packageName}}</div>  <br>
                {{trans('common.package_by')}} :
                <a href="javascript:;">
                    <img class="img-thumbnail" src="{{asset('images/package-tour/mid/'.$rows->Image)}}">
                </a>
            </li>
            <li class="list-group-item text-center">
                <?php
                    $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                    $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                    $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                    $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                ?>
                {{--@if($check>0)--}}
                {{--<a href="{{url('package/users/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Members</a>--}}
                <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Preview</a>
                {{--@endif--}}
                <a href="{{url('package/edit/'.$rows->packageID)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                @if($check_del==0 && $check_del2==0 && $check_owner==1)
                    <a href="{{url('package/delete/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                @endif
            </li>
        </ul>
        @endforeach

    </script>
    <script id=another-template type=text/template>
        <ul class="list-group">
            <li class="list-group-item"><a href="javascript:;"> <img src="images/20.jpg"/> </a></li>
            <li class="list-group-item">
                <button type="button" class="btn btn-default btn-xs" title="thumb up"><span
                            class="glyphicon glyphicon-thumbs-up"></span></button>
                <button type="button" class="btn btn-default btn-xs" title="thumb down"><span
                            class="glyphicon glyphicon-thumbs-down"></span></button>
                <button type="button" class="btn btn-default btn-xs pull-right" title="pin"><span
                            class="glyphicon glyphicon-pushpin"></span></button>
            </li>
            <li class="list-group-item">
                <div class="media">
                    <div class="media-left"><a href="javascript:;">
                            <img class="media-object img-rounded" style="width: 30px; height: 30px;" src="images/avatar_30.png"/> </a></div>
                    <div class="media-body"><h5 class="media-heading">Liber</h5>
                        <small>This is very cool!</small>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="list-group">
            <li class="list-group-item"><a href="javascript:;"> <img src="images/10.jpg"/> </a></li>
            <li class="list-group-item">
                <button type="button" class="btn btn-default btn-xs" title="thumb up"><span
                            class="glyphicon glyphicon-thumbs-up"></span></button>
                <button type="button" class="btn btn-default btn-xs" title="thumb down"><span
                            class="glyphicon glyphicon-thumbs-down"></span></button>
                <button type="button" class="btn btn-default btn-xs pull-right" title="pin"><span
                            class="glyphicon glyphicon-pushpin"></span></button>
            </li>
            <li class="list-group-item">
                <div class="media">
                    <div class="media-left"><a href="javascript:;"> <img class="media-object img-rounded"
                                                                         style="width: 30px; height: 30px;"
                                                                         src="images/avatar_30.png"/> </a></div>
                    <div class="media-body"><h5 class="media-heading">Liber</h5>
                        <small>How about this? Awesome! I love this pin!I love this pin!</small>
                    </div>
                </div>
            </li>
        </ul>
    </script>

    <script>

        $('#txtsearch').on('blur', function (e) {
            if(e.target.value){
                $('#form-set-country').submit();
            }
        });



        $('.waterfall')
            .data('bootstrap-waterfall-template', $('#waterfall-template').html())
            .on('finishing.mystist.waterfall', function () {
                setTimeout(function () { // simulate ajax
                    $('.waterfall').data('mystist.waterfall').addPins($($('#another-template').html()))
                }, 500);
            })
            .waterfall();

        $('#country').on('change',function (e) {
            if(e.target.value){
               $('#form-set-country').submit();
            }
//            $.ajax({
//                url:SP_source()+'/ajax/sort-country',
//                type:'get',
//                data:{'country':e.target.value},
//                success:function (data) {
//                    alert('dd');
//                }
//            });
        });
    </script>


@endsection
