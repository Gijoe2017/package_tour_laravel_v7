@extends('layouts.manage.master')

@section('contents')
    <style type="text/css">

        .dropzone .dz-message{
            margin: 0;
        }

        .col-lg-2 {
            padding-right: 5px;
            padding-left: 5px;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            overflow: visible;
            margin: 3px 3px 25px 0;
            padding: 1px;
            float: left;
            width: 150px;
            height: 100px;
            font-size: 4em;
            text-align: center;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                    <div class="box-body">
                        @if (isset($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            <form role="form"  action="{{action('Package\ScheduleController@updateProgram')}}" enctype="multipart/form-data" method="post" novalidate>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{$Program->programID}}">
                                <input type="hidden" name="Days" id="Days" value="{{$Program->packageDays}}" >
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12 text-center-xs" style="margin-top: 60px">
                                            <h1 class="section-title-inner" style="margin-top: 12px;"><span><a href="{{Session::get('userURL')?url('work-of-art/'.Session::get('userURL')):url('/'.Auth::user()->id)}}"><i class="fa fa-paint-brush"></i> PackageTour </a></span></h1>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-6 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
                                            @if(isset($Default))
                                                @if($Default->packageStatus=='D')
                                                    <h4 class="caps"> <a href="{{url('editWork-Of-Art/'.$Default->packageID.'/'.$Default->packageBy)}}"><lable class="text-danger"><i class="fa fa-edit"></i>{{trans('package.IncompleteInformation')}}</lable></a>
                                                        | <button type="submit" class="btn btn-info pull-right">{{trans('package.PostWorkOfArtNow')}}</button>
                                                    </h4>
                                                @else
                                                    <h4 class="caps">
                                                        <a href="{{url('Work-Of-Art-Info/'.$Default->packageID.'/'.$Default->packageBy.'/'.Session::get('Language'))}}"><lable class="text-success"><i class="fa fa-info-circle"></i> {{trans('package.InformationIsDisplayed')}}</lable></a>
                                                    </h4>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="w100 clearfix">
                                            <ul class="orderStep orderStepLook2">
                                                <li ><a href="{{url('package/setting/'.Session::get('package'))}}"> <i class="fa fa-pencil-square-o"></i> <span> {{trans('package.SettingPackage')}}</span> </a>
                                                </li>
                                                <li><a href="{{url('package/info')}}"> <i class="fa fa-picture-o"></i> <span> {{trans('package.PackageInfo')}} </span></a>
                                                </li>
                                                <li class="active"><a href="{{url('package/schedule')}}"> <i class="fa fa-newspaper-o"></i> <span> {{trans('package.PackageSchedule')}} </span></a>
                                                </li>
                                                <li ><a href="{{url('package/details')}}"> <i class="fa fa-object-group"></i> <span> {{trans('package.PackageDetails')}} </span></a>
                                                </li>

                                            </ul>
                                            <!--/.orderStep end-->
                                        </div>
                                        <div  class="alert alert-success">
                                            <div class="row">

                                                <div class="col-md-12">
                                                  <span style="font-size: 1.5em"> <i class="fa fa-calendar"></i>
                                                  {{trans('package.ProgramTour')}} {{$NameDay->DayName}} </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                                <div class="col-lg-12">
                                                    <div class="row">
                                                    @if(isset($Program))
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><strong>{{trans('package.Time')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                                                            <input type="text" class="form-control bfh-phone"  name="packageTime" id="packageTime" value="{{$Program->packageTime}}"  maxlength="23:59" data-format="dd:dd" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label><strong>{{trans('package.Time')}} *</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                                                        <select class="form-control" name="packageTime2" id="packageTime2">
                                                            <option value="0"> {{trans('package.Choose')}} </option>
                                                            @foreach($Times as $rows)
                                                                <option value="{{$rows->TimeCode}}">{{$rows->Time_text}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label><strong>{{trans('package.PackageDetails')}}*</strong></label><BR>
                                                        <textarea type="text" class="form-control" name="packageDetails" id="packageDetails" required>{{$Program->packageDetails}}</textarea>
                                                    </div>
                                                    @else
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><strong>{{trans('package.Time')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                                                                <input type="time" class="form-control bfh-phone"  name="packageTime" id="packageTime"  data-format="dd:dd" required>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label><strong>{{trans('package.PackageHeadDetails')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Head Details.</span></label><BR>
                                                            <input type="text" class="form-control" name="packageTitle" id="packageTitle"  >
                                                        </div>

                                                        <div class="form-group">
                                                            <label><strong>{{trans('package.PackageDetails')}}*</strong></label><BR>
                                                            <textarea type="text" class="form-control" name="packageDetails" id="packageDetails" required></textarea>
                                                        </div>

                                                    @endif




                                                </div>




                                        <!-- /.modal-content -->


                                    </div>
                                <div class="clearfix"></div>
<hr>
                                    <a href="{{url('package/schedule')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.UpdateandContinue')}}</button>

                            </div>

                        </div>

                            </form>

                    </div>


            </div>
        </div>
    </div>

{{--<script src="{{asset('assets/js/bootstrap-filestyle.min.js')}}"></script>--}}
<script src="{{asset('assets/js/main.js')}}"></script>

<script language="javascript">
    function deleteItem(id) {
        if (confirm('ยืนยันการลบสินค้ารายการนี้!')) {
            $.ajax({
                type: 'get',
                url: '{{URL::to('program/delImage')}}',
                data: {'picID': id},
                success: function (data) {
                    $('#showPics').html(data);
                    document.getElementById('dropzoneFileUpload').innerHTML="<div class=\"dz-default dz-message\"><span><img src=\"{{asset('images/default-add.jpg')}}\" width=\"100%\"></span></div>";
                    $("#Manage-image").load(location.href + " #showPics");
                }
            });
        }
    }

    $('#packageTime').on('blur',function (e) {

        if(e.target.value > '23'){
            document.getElementById('packageTime').value='23:59';
        }
    });

    $('#packageTime').on('keyup',function (e) {
        var time=document.getElementById('packageTime').value;

        if(time.length>0){
            $("#packageTime2").prop('disabled', true);
        }else{
            $("#packageTime2").prop('disabled', false);
        }

    });

    $('#packageTime2').on('change',function (e) {

        if(e.target.value>0){
            $.ajax({
                url: '{{URL::to('package/program/gettime')}}',
                type: "get",
                data: {'id': e.target.value},
                success: function (data) {
                    $("#packageTime").val(data);
                    $("#packageTime").prop('readonly', true);
                }
            });

        }else{
            $("#packageTime").val('');
            $("#packageTime").prop('readonly', false);
        }

    });


    var simplemde = new SimpleMDE(
            { element: document.getElementById("packageDetails"),
                toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
            });
</script>

@stop()
<div id="PopupSortImage" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">สลับตำแหน่งรูปภาพ</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-12">

                        <div id="showPics1" class="showPics1">
                            @if(isset($Photos))
                                <ul id="sortable" >
                                    @foreach($Photos as $rows)
                                        <li id="img-{{$rows->ImageID}}">
                                            <img class="quickview1" style="height: 100%" src="{{$rows->image_path.'/small/'.$rows->Image}}">
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>