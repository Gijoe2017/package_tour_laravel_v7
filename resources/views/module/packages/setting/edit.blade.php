@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
        {{--<h2><span><i class="fa fa-paint-brush"></i> Package Edit. </span></h2>--}}
            <h2><span><i class="fa fa-paint-brush"></i> {{trans('common.package').trans('common.edit')}} </span></h2>
        </div>
    </section>
@endsection

@section('content')
    <section class="content">       
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <!-- <form id="checkout-form"></form> -->
                    <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{Session::get('package')?url('/package/update'):action('Package\PackageController@store')}}" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{Session::get('package')}}">
                        <div class="box-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.defaultLanguage')}} * </label><BR>
                                                <select class="select2 form-control" name="packageLanguage" id="packageLanguage" required="required">
                                                    <option value="">Choose Language</option>
                                                    @foreach( Config::get('app.locales') as $key => $value)
                                                        <option value="{{$key}}" {{$key==$Package->packageLanguage?'selected':''}}>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.defaultCurrency')}} *</label><BR>
                                                <select class="form-control select2" name="packageCurrency" id="packageCurrency" required="required">
                                                    <option value="">Choose Currency</option>
                                                    @foreach($Currency as $row)
                                                        <option value="{{$row->currency_code}}" {{$row->currency_code==$Package->packageCurrency?'selected':''}}>{{$row->currency_name}} [{{$row->currency_code}}]</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.numberDays')}} *</label><BR>
                                                <input type="number" class="form-control" name="packageDays" id="packageDays" value="{{$Package->packageDays}}">
                                                {{--<select class="form-control select2" name="packageDays" id="packageDays" required="required">--}}
                                                    {{--<option value="">Choose Number of Days</option>--}}
                                                    {{--@for($i=1;$i<=15;$i++)--}}
                                                        {{--<option value="{{$i}}" {{$i==$Package->packageDays?'selected':''}}>{{$i}}</option>--}}
                                                    {{--@endfor--}}
                                                {{--</select>--}}
                                            </div>

                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.numberNight')}} *</label><BR>
                                                <input type="number" class="form-control" name="packageNight" id="packageNight" value="{{$Package->packageNight}}">
                                                {{--<select class="form-control select2" name="packageNight" id="packageNight" required="required">--}}
                                                    {{--<option value="">Choose Number of Night</option>--}}
                                                    {{--@for($i=1;$i<=15;$i++)--}}
                                                        {{--<option value="{{$i}}" {{$i==$Package->packageNight?'selected':''}}>{{$i}}</option>--}}
                                                    {{--@endfor--}}
                                                {{--</select>--}}
                                            </div>


                                            <div class="col-lg-4 form-group">
                                                <label>{{trans('package.set_up_a_visa_package')}} <span class="text-red">*</span></label> <BR>
                                                <label class="radio-inline"><input type="radio" class="package-visa" name="have_visa" value="1" {{$Package->have_visa=='1'?'checked':''}}> {{trans('package.this_package_require_a_visa')}}</label>
                                                <label class="radio-inline"><input type="radio" class="package-visa" name="have_visa" value="0" {{$Package->have_visa=='0'?'checked':''}}> {{trans('package.this_package_does_not_require_a_visa')}}</label>
                                            </div>
                                            <div id="show_content" class="col-lg-8 form-group">
                                                <textarea class="form-control" id="condition_visa" name="condition_visa" placeholder="{{trans('package.visa_conditions')}}" required >{{$Package->condition_visa}}</textarea>
                                            </div>
                                            <div class="col-lg-12"><hr></div>

                                            <div class="col-lg-12 form-group">
                                                <label>{{trans('package.chooseCountry')}} *</label><BR>
                                                <select class="form-control select2" data-style="btn-info" name="packagePeopleIn[]" id="packagePeopleIn" multiple required="required" title="Choose Country...">
                                                    @foreach($Country as $row)
                                                        <?php
                                                        $Selected="";
                                                        $check=DB::table('package_tourin')
                                                            ->where('packageID',Session::get('package'))
                                                            ->where('CountryCode',$row->country_id)->first();
                                                        if(isset($check)){
                                                            $Selected="Selected";
                                                        }
                                                        ?>
                                                        <option value="{{$row->country_id}}" {{$Selected}}>{{$row->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label>{{trans('package.package_tour_from')}}<span class="text-red">*</span></label> <BR>
                                                <label class="radio-inline"><input type="radio" class="package-from" name="package_owner" value="Yes" {{$Package->package_owner=='Yes'?'checked':''}} > {{trans('package.package_owner')}}</label>
                                                <label class="radio-inline"><input type="radio" class="package-from" name="package_owner" value="" {{$Package->package_owner==''?'checked':''}}> {{trans('package.package_another')}}</label>
                                            </div>
                                            <div id="select-partner" class="col-lg-12" style="display: {{$Package->package_owner==''?'':'none'}}" >
                                                <label>{{trans('common.partner')}}<span class="text-red">*</span> </label><small class="text-warning"> (กรณีที่ต้องการเอาทัวร์จากบริษัทอื่นมาขายต้องได้รับการยินยอมจากบริษัทเหล่านั้นก่อน โดยการยื่นคำขอตามระบบ)</small><Br>
                                                <select class=" form-control"  name="owner_timeline_id" id="owner_timeline_id">
                                                    <option value="">{{trans('profile.Choose').trans('common.partner')}}</option>
                                                    @foreach($Partners as $row)
                                                        <option value="{{$row->request_partner_id}}" {{$Package->owner_timeline_id==$row->request_partner_id?'selected':''}}>{{$row->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="row">
                                            <div class="col-lg-12">
                                                <hr>
                                                <div class="col-lg-7">
                                                    <div class="form-group">
                                                    <label>{{trans('common.original_package_code')}} </label><BR>
                                                    <input type="text" class="form-control" name="original_package_code" id="original_package_code" value="{{$Package->original_package_code}}">
                                                    </div>
                                                    <div class="form-group">
                                                    <label><strong>{{trans('common.upload_original_file')}}</strong></label><small class="text-warning"> (If you want to change original file you can select new file and update)</small>
                                                    <input type="file" name="file"  class="filestyle" data-buttonName="btn-primary" >
                                                </div>
                                            </div>
                                            @if($Package->original_file)
                                            <div class="col-lg-5"><h3>
                                                <i class="fa fa-file"></i> <a href="{{asset('images/package-tour/docs/'.$Package->original_file)}}" target="_blank">{{$Package->original_file}}</a>
                                                </h3>
                                            </div>
                                            @endif
                                            </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <a href="{{url('package/list')}}" class="btn btn-lg btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                    <button type="submit" class="btn btn-lg btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.SaveandContinue')}}</button>

                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection