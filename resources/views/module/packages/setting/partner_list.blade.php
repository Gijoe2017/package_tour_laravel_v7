@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <link rel=stylesheet href={{asset('member/waterfull/styles/main.css')}}>
    @if($Partners->count()==0)
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>Have no package.</h2>
                    </div>

                </div>
            </div>
        </div>
        @else

        <div class=container>
        <div class="row" style="margin-left: -25px">
            <div class=waterfall></div>
        </div>
    </div>

    @endif

    <script src={{asset('member/waterfull/scripts/vendor.js')}}></script>
    <script src={{asset('member/waterfull/bootstrap-waterfall.js')}}></script>

    <script id=waterfall-template type=text/template>
        @foreach($Partners as $rows)
        <?php
            $timeline=\App\Timeline::where('id',$rows->id)->first();
            $media=\App\Media::where('id',$timeline->avatar_id)->first();

        ?>
        <ul class="list-group">
            <li class="list-group-item text-center">
                <div class="panel-body">{{str_limit($rows->name,30)}}</div>  <br>
                <a href="javascript:;">
                    @if($media)
                        <img class="img-thumbnail" src="{{url('user/avatar/small/'.$media->source)}}">
                        @else
                    <img class="img-thumbnail" src="{{url('user/avatar/default-user.png')}}">
                        @endif
                </a>
            </li>
            <li class="list-group-item text-center">
                 <a href="{{url('package/partner/packages/'.$rows->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> Package List</a>
            </li>

        </ul>
        @endforeach

    </script>


    <script>
        $('.waterfall')
            .data('bootstrap-waterfall-template', $('#waterfall-template').html())
            .on('finishing.mystist.waterfall', function () {
                setTimeout(function () { // simulate ajax
                    $('.waterfall').data('mystist.waterfall').addPins($($('#another-template').html()))
                }, 500);
            })
            .waterfall()

        $('#country').on('change',function (e) {
            if(e.target.value){
               $('#form-set-country').submit();
            }
//            $.ajax({
//                url:SP_source()+'/ajax/sort-country',
//                type:'get',
//                data:{'country':e.target.value},
//                success:function (data) {
//                    alert('dd');
//                }
//            });
        });
    </script>


@endsection
