
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h3 class="modal-title-site text-center"> กำหนดตาราง / ราคา  </h3>
        </div>
        <div class="modal-body">
           <div class="col-md-12">
               <input type="hidden" id="packageDescID" value="{{$packageDescID}}">
               <table id="table-rate" class="table table-bordered table">
                   <thead>
                   <tr><th></th>
                       <th>Condition</th>
                       <th>Price</th>
                       <th>Price Show</th>
                       <th>Price + Ticket</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($Rate as $rows)
                   <tr>
                       <td style="width: 12%">
                           {{--<a data-toggle="modal" data-target="#popupForm" href="{{url('/package/ajax/rate/edit/'.$rows->psub_id)}}"  class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>--}}
                           <a data-id="{{$rows->psub_id}}" class="btn btn-sm btn-danger delete-rate"><i class="fa fa-trash"></i> </a>
                       </td>
                       <td>{{$rows->TourType}}</td>
                       <td><strong>{{$rows->PriceSale>0?number_format($rows->PriceSale):$rows->PriceSale}}</strong></td>
                       <td><strong>{{$rows->price_system_fees>0?number_format($rows->price_system_fees):$rows->price_system_fees}}</strong></td>
                       <td><strong>{{$rows->PriceAndTicket>0?number_format($rows->PriceAndTicket):$rows->PriceAndTicket}}</strong></td>
                   </tr>
                   @endforeach
                   </tbody>
               </table>
           </div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
        </div>
    </div>
    <!-- /.modal-content -->

{{--<script>--}}
    {{--$('#edit-rate').on('click',function () {--}}
        {{--$.ajax({--}}
            {{--url : "{{URL::to('package/ajax/rate/edit')}}",--}}
            {{--type: "get",--}}
            {{--data: {'id':$(this).data('id'),'packageDescID':$('#packageDescID').val()},--}}
            {{--success: function(data) {--}}
                {{--alert(data);--}}
                {{--$('#table-rate tbody').html(data);--}}
            {{--}--}}
        {{--});--}}
    {{--});--}}

    {{--$('#del').on('click',function () {--}}

        {{--$.ajax({--}}
            {{--url : "{{URL::to('package/ajax/rate/del')}}",--}}
            {{--type: "get",--}}
            {{--data: {'id':$(this).data('id')},--}}
            {{--success: function(data) {--}}
                {{--$('tbody').html(data);--}}
            {{--}--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}