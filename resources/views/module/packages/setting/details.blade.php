@extends('layouts.member.layout_master_new')
@section('header')
    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
        .btn-sm{
            margin-bottom: 2px;
        }
    </style>
    <script src="{{asset('assets/bootstrap/js/moment-with-locales.js')}}"></script>
    <script src="{{asset('assets/bootstrap/js/bootstrap-datetimepicker.min.js')}}"></script>
    <section class="content-header">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
        <h2><span><i class="fa fa-paint-brush"></i> {{trans('common.package_details')}} </span></h2>
    </div>
    </section>
@endsection

@section('content')
    <style type="text/css">
        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 35px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 35px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 20px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 20px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
    <section class="content">
    <div class="row">
            <div class="box box-info">
                <div class="box-body">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="row">
                                <div class="col-md-6">
                                    <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-calendar"></i> {{trans('package.TravelSchedule')}} </span>
                                </div>
                                <div class="col-md-6">
                                    <div align="right">
                                    {{--<a href="{{url('package/ajax/detail/create')}}"  class="btn btn-info"><i class="fa fa-plus"></i>  Add Package Details </a>--}}
                                    <a  href="{{url('package/detail/create')}}"  class="btn btn-info"><i class="fa fa-plus"></i>  {{trans('common.add_package_details')}} </a>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    {{--<div  class="alert alert-success">--}}
                                    {{--</div>--}}
                                    <div class="content40"><hr></div>
                                    @if($PackageDesc->count())

                                            <table class="table">
                                                <thead>
                                                <tr><th>{{trans('common.status')}}</th>
                                                    <th>{{trans('common.date_start')}}</th>
                                                    <th>{{trans('common.date_end')}}</th>
                                                    <th>{{trans('common.closing_date')}}</th>
                                                    <th>{{trans('package.travel_by')}}</th>
                                                    <th>{{trans('common.airline')}}</th>
                                                    <th>{{trans('common.flight')}}</th>
                                                    <th>{{trans('common.number_of_people')}}</th>
                                                    <th>{{trans('common.company_commission')}}</th>
                                                    <th>{{trans('common.seller_commission')}}</th>
                                                    <th>{{trans('common.price_sale')}}</th>
                                                    <th><div align="center">{{trans('package.Action')}}</div> </th>
                                                </tr>
                                                </thead>
                                                <tbody> <?php $i=1?>
                                                @foreach($PackageDesc as $rows)
                                                    <?php
                                                        $airline_name='';$travel='';
                                                        if($rows->Airline){
                                                            $airline=DB::table('airline')->where('airline',$rows->Airline)->first();
                                                            if($airline){
                                                                $airline_name=$airline->airline_name.' ('.$airline->icao.')';
                                                            }
                                                        }
                                                        $check=DB::table('package_details_sub')->where('packageDescID',$rows->packageDescID)->count();
                                                        $check_condition=DB::table('condition_in_package_details')->where('packageID',$rows->packageID)->count();
                                                        $check_program=DB::table('package_booking_details')->where('package_detail_id',$rows->packageDescID)->count();
                                                        $travel="-";
                                                        if($rows->use_airplan=='1'){
                                                            $travel_by=DB::table('the_vehicle')->where('id',$rows->travel_by)->where('language_code',Session::get('language'))->first();
                                                            if($travel_by){
                                                                $travel =$travel_by->vehicle;
                                                            }
                                                        }
//                                                        $closing_date = date('Y-m-d', strtotime("-" . $rows->number_of_days_the_sale_is_closed . " day",strtotime($rows->packageDateStart)));
                                                        ?>
                                                    <tr>
                                                        <td>
                                                            <div class="material-switch">
                                                                <input class="switch" data-id="{{$rows->packageDescID}}" id="status-{{$rows->packageDescID}}" name="status-{{$rows->packageDescID}}" type="checkbox" value="Y"  {{$rows->status=='Y'?'checked':''}}/>
                                                                <label for="status-{{$rows->packageDescID}}" class="label-success"></label><span></span>
                                                            </div>
                                                        </td>
                                                        <td>{{$rows->packageDateStart}}</td>
                                                        <td>{{$rows->packageDateEnd}}</td>
                                                        <td>{{$rows->closing_date}}</td>
                                                        <td>{{$travel}}</td>
                                                        <td>{{$airline_name?$airline_name:'-'}}</td>

                                                        <td>{{$rows->Flight?$rows->Flight:'-'}}</td>

                                                        <td>{{$rows->NumberOfPeople}}</td>
                                                        <td>{{$rows->Commission>0?number_format($rows->Commission).' '.$rows->unit_commission:''}}</td>
                                                        <td>{{$rows->Commission_sell>0?number_format($rows->Commission_sell).''.$rows->unit_commission:''}}</td>
                                                        <td>
                                                            @if($check>0)
                                                                <a href="{{url('package/ajax/rate/'.$rows->packageDescID)}}" data-toggle="modal" data-target="#popupForm" class="btn btn-sm btn-info"><i class="fa fa-search"></i> </a>
                                                            @else
                                                                <a href="{{url('package/details/price/'.$rows->packageDescID)}}"  class="btn btn-sm btn-default"><i class="fa fa-plus"></i> </a>
                                                            @endif
                                                        </td>
                                                        <td width="15%" align="right">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-default">{{trans('package.Action')}}</button>
                                                                <button type="button" class="btn btn-sm btn-danger dropdown-toggle" data-toggle="dropdown">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    {{--@if($check_condition>0)--}}
                                                                    <li><a   class="dropdown-item" href="{{url('package/details/copy/'.$rows->packageDescID)}}" ><i class="fa fa-copy"></i> {{trans('common.copy')}}</a></li>
                                                                        {{--<li><a class="dropdown-item" href="{{url('package/condition/view/'.$rows->packageDescID)}}" ><i class="fa fa-search"></i> {{trans('common.condition')}}</a></li>--}}
                                                                    {{--@else--}}
                                                                        {{--<li><a class="dropdown-item" href="{{url('package/condition/list/'.$rows->packageDescID)}}" ><i class="fa fa-edit"></i> {{trans('common.condition')}}</a></li>--}}
                                                                    {{--@endif--}}
                                                                    <li><a  class="dropdown-item" href="{{url('package/details/price/'.$rows->packageDescID)}}" ><i class="fa fa-money"></i> {{trans('common.price')}}</a></li>
                                                                    <li><a  class="dropdown-item" href="{{url('/package/detail/edit/'.$rows->packageDescID)}}" >
                                                                    <i class="fa fa-edit"></i> {{trans('common.edit')}}</a></li>
                                                                    @if($rows->status!='Y' || $check_program==0)
                                                                        <li><a  class="dropdown-item" href="{{url('package/details/del/'.$rows->packageDescID)}}"  onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a></li>
                                                                    @endif
                                                                </ul>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>{{trans('common.no_have_data')}}</h2>
                                        @endif
                                </div>
                                <div class="col-md-12">
                                    <a href="{{url('/package/condition/list')}}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> {{trans('common.add').trans('common.condition')}}</a>
                                    <a href="{{url('/package/list')}}" class="btn btn-default"><i class="fa fa-list"></i> {{trans('package.BackToList')}}</a>
                                    <a href="{{url('/package/schedule')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>



@stop()