@extends('layouts.member.layout_master_check')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
                <div class="box box-info">
                    <div class="box-body">

                        <form role="form" id="AutoForm"  action="{{action('Package\PackageController@updateDetail')}}" enctype="multipart/form-data" method="post" novalidate>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="packageDescID" id="packageDescID" value="{{$Details->packageDescID}}" >

                            <div class="panel-content">
                                <div class="panel-header">

                                    <h3 class="panel-title-site text-center"> กำหนดตารางการเดินทาง / ราคา </h3>
                                    <hr>
                                </div>
                                <?php
                                $dateSt=date('m/d/Y',strtotime($Details->packageDateStart));
                                $dateEnd=date('m/d/Y',strtotime($Details->packageDateEnd));
                                ?>
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-md-8">
                                                    <div id="sandbox-container" class="form-group">
                                                        <label><strong>{{trans('package.Daterange')}} * </strong></label> <small class="text-gray">(กำหนดวันที่เริ่มออกเดินทางและวันที่สิ้นสุดการเดินทาง)</small><BR>
                                                        {{--<div class="input-group">--}}
                                                        {{--<div class="input-group-addon">--}}
                                                        {{--<i class="fa fa-calendar"></i>--}}
                                                        {{--</div>--}}
                                                        {{--<input type="text" class="form-control pull-right" name="package_date" value="{{$dateSt}}-{{$dateEnd}}" id="reservation">--}}
                                                        {{--</div>--}}

                                                        <div class="input-daterange input-group" id="datepicker">
                                                            <input type="text" class="form-control"  name="package_date_start" value="{{$dateSt}}"  autocomplete="true" />
                                                            <span class="input-group-addon">{{trans('common.to')}}</span>
                                                            <input type="text" class="form-control"  name="package_date_end" value="{{$dateEnd}}" autocomplete="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Country_Airport')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                                <select class="form-control" name="Country_id" id="Country_id" >
                                                    <option value="">{{trans('common.choose')}}</option>
                                                    @foreach($country as $rows)
                                                        <option value="{{$rows->country_id}}" {{$rows->country_id==$Details->Country_id?'Selected':''}}>{{$rows->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>

                                                <select class="form-control" name="Airline" id="Airline" >
                                                    <option value="">{{trans('common.choose')}}</option>
                                                    @foreach($Airline as $rows)
                                                        <?php

                                                        if($rows->iata && $rows->icao){
                                                            $code='('.$rows->iata.', '.$rows->icao.')';
                                                        }else if($rows->icao){
                                                            $code='('.$rows->icao.')';
                                                        }else if($rows->iata){
                                                            $code='('.$rows->iata.')';
                                                        }
                                                        ?>
                                                        <option value="{{$rows->airline}}" {{$rows->airline==$Details->Airline?'Selected':''}} >{{$rows->airline_name.' '.$code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                                                <input type="text" class="form-control" name="Flight" id="Flight" value="{{$Details->Flight}}" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.NumberOfPeople')}} *</strong></label><BR>
                                                <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople" value="{{$Details->NumberOfPeople}}"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Commission')}} *</strong></label><BR>
                                                <input type="number" class="form-control" name="Commission" id="Commission" value="{{$Details->Commission}}"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><strong>{{trans('package.Commission_sell')}} *</strong></label><BR>
                                                <input type="number" class="form-control" name="Commission_sell" id="Commission_sell" value="{{$Details->Commission_sell}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                                                <select class="form-control select2" style="width: 100%" name="unit_commission" id="unit_commission" required >
                                                    <option value="">{{trans('profile.Choose')}}</option>
                                                    <option value="%" {{$Details->unit_commission=='%'?'Selected':''}}>%</option>
                                                    <option value="{{$Package->packageCurrency}}" {{$Package->packageCurrency==$Details->unit_commission?'selected':''}} >{{$Package->packageCurrency}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <hr>
                                <div >
                                    @if(Session::has('detail'))
                                        <a class="btn btn-default  pull-left" href="{{url('package/details/'.Session::get('package'))}}" aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                    @else
                                        <a class="btn btn-default  pull-left" href="{{url('package/details_check/'.Session::get('package'))}}" aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                    @endif
                                    <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </form>
                        <!-- jQuery 3 -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('member/bootstrap-datepicker/js/jquery-1.9.1.min.js')}}"></script>
    <script src="{{asset('member/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('member/bootstrap-datepicker/locales/bootstrap-datepicker.'.App::getLocale().'.min.js')}}"></script>




    <script language="javascript">

        $('#Country_id').on('change',function (e) {
            $.ajax({
                type:'get',
                url:SP_source() + 'package/ajax/get-airline',
                data:{'country_id':e.target.value},
                success:function(data){

                    $('#Airline').empty();
                    $('#Airline').append(data);
                }
            });
        });
        var today=new Date();
        $('#sandbox-container .input-daterange').datepicker({
//        maxViewMode: 0,
            startDate: today,
            forceParse: false,
//        calendarWeeks: true,
            autoclose: true,
//        todayHighlight: true,
//        format: "yyyy-mm-dd",
            language: "{{Session::get('language')}}"

        });


    </script>

@endsection