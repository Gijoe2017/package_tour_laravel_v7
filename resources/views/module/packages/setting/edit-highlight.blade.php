@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-body">
<link rel="stylesheet" href="{{asset('member/assets/plugins/iCheck/square/blue.css')}}" type="text/css"  />
<link rel="stylesheet" href="{{asset('assets/css/build.css')}}" type="text/css"  />
<link rel="stylesheet" href="{{asset('assets/icon-line-pro/style.css')}}">
<form role="form"  action="{{action('Package\HighlightOfDayController@update')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="panel-header">
        {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>--}}
        <h3 class="panel-title-site text-center"> {{trans('package.highlight') .' '.$NameDay->DayName}}  </h3>
    </div>
<div class="panel-body">

    <div class="col-lg-12">
        @if(isset($HighlightToday))
            <div class="form-group">
                <label><strong>{{trans('package.Title').' '.$NameDay->DayName}}</strong> <span id="error2" style="display: none; color: red">Need to insert Head Details.</span></label><BR>
                <textarea type="text" rows="10" class="form-control" name="Highlight" id="Highlight" required>{{$HighlightToday->Highlight}}</textarea>
            </div>
            <label><strong><i class="icon-travel-109 u-line-icon-pro align-middle"></i> &nbsp;{{trans('common.food_options_for_today')}}</strong></label>
            <br>        
            <input type="hidden" name="id" value="{{$HighlightToday->HighlightID}}">
            <input type="hidden" name="Days" id="Days" value="{{$HighlightToday->packageDays}}" >
            <div class="checkbox checkbox-success checkbox-inline">
                <input type="checkbox" class="styled" name="Breakfast" id="Breakfast" {{$HighlightToday->Breakfast=='Y'?'checked':''}} value="Y">
                <label for="inlineCheckbox1"> {{trans('common.breakfast')}} </label>
            </div>
            <div class="checkbox checkbox-success checkbox-inline">
                <input type="checkbox" class="styled" name="Lunch" id="Lunch" {{$HighlightToday->Lunch=='Y'?'checked':''}} value="Y" >
                <label for="inlineCheckbox2"> {{trans('common.lunch')}} </label>
            </div>
            <div class="checkbox checkbox-success checkbox-inline">
                <input type="checkbox" class="styled" name="Dinner" id="Dinner" {{$HighlightToday->Dinner=='Y'?'checked':''}} value="Y">
                <label for="inlineCheckbox3"> {{trans('common.dinner')}} </label>
            </div>
        @endif
        <br>

    </div>
    {{--<div class="col-lg-4">--}}
        {{--<div class="form-group">--}}
        {{--<div class="text-center">--}}
            {{--<img src="{{asset('images/default-add.jpg')}}" id="blah" class="img-thumbnail">--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="clearfix"></div>
</div>

<div class="modal-footer">
    @if(Session::has('checking'))
    <a href="{{url('package/details_check/'.Session::get('package'))}}" class="btn btn-default pull-left"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
        @elseif(Session::has('detail'))
            <a href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default pull-left"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>

        @else
    <a href="{{url('package/schedule')}}" class="btn btn-default pull-left"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
    @endif
    <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
</div>


    </form>
                </div></div></div></section>
@endsection
<script src="{{asset('member/assets/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-filestyle.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
<script language="javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        content: function(){return '<img src="'+$(this).data('img') + '" />';}
    });
    $("#imgInp").change(function(){
        readURL(this);
    });

//    var simplemde = new SimpleMDE(
//            { element: document.getElementById("Highlight"),
//                toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
//
//            });
</script>