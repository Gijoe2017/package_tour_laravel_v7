
@extends('layouts.member.layout_master_new')
<?php
App::setLocale(Session::get('language'));
?>
@section('header')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">

        <h2><span><i class="fa fa-paint-brush"></i> {{trans('common.package').' '.trans('common.create')}}. </span></h2>

        </div>
        {{--<div class="col-lg-6 col-md-6 col-sm-6 rightSidebar col-xs-6 col-xxs-12 text-center-xs">--}}
            {{--@if(isset($Package))--}}
                {{--@if($Package->packageStatus=='D')--}}
                    {{--<h4 class="caps"> <a href="{{url('member/package/edit/'.$Package->packageID)}}"><lable class="text-danger"><i class="fa fa-edit"></i>{{trans('package.IncompleteInformation')}}</lable></a>--}}
                        {{--<a class="btn btn-info pull-right">{{trans('package.PostWorkOfArtNow')}}</a>--}}
                    {{--</h4>--}}
                {{--@else--}}
                    {{--<h4 class="caps">--}}
                        {{--<a class="btn btn-info pull-right" href="{{url('member/package/view/'.$Package->packageID.'/'.$Package->packageBy.'/'.Session::get('Language'))}}"><lable class="text-success"><i class="fa fa-info-circle"></i> {{trans('package.InformationIsDisplayed')}}</lable></a>--}}
                    {{--</h4>--}}
                {{--@endif--}}
            {{--@endif--}}
        {{--</div>--}}
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- <form id="checkout-form"></form> -->
                <form role="form" id="checkout-form" name="frm" enctype="multipart/form-data" action="{{action('Package\PackageController@store')}}" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{Session::get('package')}}">
                <div class="box box-info">

                    <div class="box-body">
                        @if (!isset($errors))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <br class="row">

                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.defaultLanguage')}} <span class="text-red">*</span> </label><BR>
                                                <select class="select2 form-control" name="packageLanguage" id="packageLanguage" required="required">
                                                    <option value="">{{trans('profile.Choose')}}</option>
                                                    @foreach( Config::get('app.locales') as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.defaultCurrency')}} <span class="text-red">*</span></label><BR>
                                                <select class="form-control select2" name="packageCurrency" id="packageCurrency" required="required">
                                                    <option value="">{{trans('profile.Choose')}}</option>
                                                    @foreach($Currency as $row)
                                                        <option value="{{$row->currency_code}}">{{$row->currency_name}} [{{$row->currency_code}}]</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.numberDays')}} <span class="text-red">*</span> </label><BR>
                                                <input type="number" class="form-control" name="packageDays" id="packageDays" required >

                                                {{--<select class="form-control select2" name="packageDays" id="packageDays" required="required">--}}
                                                    {{--<option value="">{{trans('profile.Choose')}}</option>--}}
                                                    {{--@for($i=1;$i<=15;$i++)--}}
                                                        {{--<option value="{{$i}}">{{$i}} {{trans('package.Days')}}</option>--}}
                                                    {{--@endfor--}}
                                                {{--</select>--}}
                                            </div>

                                            <div class="col-lg-6 form-group">
                                                <label>{{trans('package.numberNight')}} <span class="text-red">*</span></label><BR>
                                                <input type="number" class="form-control" name="packageNight" id="packageNight" required>

                                                {{--<select class="form-control select2" name="packageNight" id="packageNight" required="required">--}}
                                                    {{--<option value="">{{trans('profile.Choose')}}</option>--}}
                                                    {{--@for($i=1;$i<=15;$i++)--}}
                                                        {{--<option value="{{$i}}">{{$i}} {{trans('package.Night')}}</option>--}}
                                                    {{--@endfor--}}
                                                {{--</select>--}}
                                            </div>

                                            <div class="col-lg-4 form-group">
                                                <label>{{trans('package.set_up_a_visa_package')}} <span class="text-red">*</span></label> <BR>
                                                <label class="radio-inline"><input type="radio" class="package-visa" name="have_visa" value="1" checked> {{trans('package.this_package_require_a_visa')}}</label>
                                                <label class="radio-inline"><input type="radio" class="package-visa" name="have_visa" value="0"> {{trans('package.this_package_does_not_require_a_visa')}}</label>
                                            </div>
                                            <div id="show_content" class="col-lg-8 form-group">
                                                <textarea class="form-control" id="condition_visa" name="condition_visa" placeholder="{{trans('package.visa_conditions')}}" required ></textarea>
                                            </div>
                                            <div class="col-lg-12"><hr></div>
                                            <div class="col-lg-12 form-group">
                                                <label>{{trans('package.chooseCountry')}} <span class="text-red">*</span></label><BR>
                                                <select class="select2 form-control" data-style="btn-info" name="packagePeopleIn[]" id="packagePeopleIn" multiple required="required" title="Choose Country...">
                                                    @foreach($Country as $row)
                                                        <option value="{{$row->country_id}}">{{$row->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        <div class="col-lg-12 form-group">
                                            <label>{{trans('package.package_tour_from')}} <span class="text-red">*</span></label> <BR>
                                            <label class="radio-inline"><input type="radio" class="package-from" name="package_owner" value="Yes" checked> {{trans('package.package_owner')}}</label>
                                            <label class="radio-inline"><input type="radio" class="package-from" name="package_owner" value=""> {{trans('package.package_another')}}</label>
                                        </div>

                                        <div id="select-partner" class="col-lg-12" style="display: none" >
                                            <label>{{trans('common.partner')}} <span class="text-red">*</span> </label><small class="text-warning"> {{trans('common.if_wanting_to_take_tours_from_other_companies')}}</small><Br>
                                            <select class="form-control"  name="owner_timeline_id" id="owner_timeline_id">
                                                <option value="">{{trans('profile.Choose').' '.trans('common.partner')}}</option>
                                                @foreach($Partners as $row)
                                                    <option value="{{$row->request_partner_id}}">{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="row col-lg-12" ><BR>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                            <label>{{trans('common.original_package_code')}}</label>
                                            <input type="text" class="form-control" name="original_package_code" id="original_package_code">
                                        </div>
                                        </div>
                                        <div class="col-lg-8">

                                            <div class="form-group">
                                                <label><strong>{{trans('common.upload_original_file')}}</strong></label>
                                                <input type="file" name="file"  class="filestyle" data-buttonName="btn-primary" >
                                            </div>
                                        </div>
                                        </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-12">
                                <a href="{{url('package/list')}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToList')}}</a>
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.SaveandContinue')}}</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop()