<script src="{{asset('assets/bootstrap/js/moment-with-locales.js')}}"></script>
<script src="{{asset('assets/bootstrap/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/theme-admin/components/bootstrap/dist/js/bootstrap-filestyle.min.js')}}"></script>
<form role="form" id="AutoForm"  action="{{action('Package\PackageController@updatePackageDetail')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="id" id="id" value="{{$packdetail->packageDescID}}" >
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload();"> &times; </button>
        <h3 class="modal-title-site text-center"> กำหนดตารางการเดินทาง / ราคา  </h3>
    </div>
    <div class="modal-body">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.DateStart')}}*</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                        <input type="text" class="form-control"  name="packageDateStart" id="startDate" value="{{$packdetail->packageDateStart}}" required>
                        <script type="text/javascript">
                            $(function () {
                                $('#startDate').datetimepicker({
//                                                viewMode: 'years',
                                    format: 'YYYY-MM-DD',

                                });
                            });
                        </script>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.DateEnd')}}*</strong></label><BR>
                        <input type="text" class="form-control" name="packageDateEnd" id="DateEnd" value="{{$packdetail->packageDateEnd}}" readonly required>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <label><strong>{{trans('package.Filght')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Flight.</span></label><BR>
                    <input type="text" class="form-control" name="Flight" id="Flight" value="{{$packdetail->Flight}}" >
                </div>
                <div class="form-group">
                    <label><strong>{{trans('package.Airline')}}</strong> <span id="error2" style="display: none; color: red">Need to insert Airline.</span></label><BR>
                    <input type="text" class="form-control" name="Airline" id="Airline" value="{{$packdetail->Airline}}" >
                </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.NumberOfPeople')}}*</strong></label><BR>
                        <input type="number" class="form-control" name="NumberOfPeople" id="NumberOfPeople" value="{{$packdetail->NumberOfPeople}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.Commission')}}*</strong></label><BR>
                        <input type="number" class="form-control" name="Commission" id="Commission" value="{{$packdetail->Commission}}" required>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.PriceSale')}}*</strong></label><BR>
                        <input type="number" class="form-control" name="PriceSale" id="PriceSale"  value="{{$packdetail->PriceSale}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><strong>{{trans('package.PriceAndTicket')}}*</strong></label><BR>
                        <input type="number" class="form-control" name="PriceAndTicket" id="PriceAndTicket" value="{{$packdetail->PriceAndTicket}}"  required>
                    </div>
                </div>
                </div>

            </div>
            <div class="col-lg-4">
                @if($packdetail->Image)
                <div class="text-center">
                    <img src="{{asset('images/package-tour/small/'.$packdetail->Image)}}" id="blah" class="img-thumbnail">
                </div>
                @else
                    <div class="text-center">
                        <img src="{{asset('images/default-add.jpg')}}" id="blah" class="img-thumbnail">
                    </div>
                    @endif
                <BR>
                <div class="form-group">
                    <input type="file" id="EditImg" name="EditImg"  class="filestyle" data-buttonName="btn-primary">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <a class="btn  pull-left" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Close</a>
            <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </div>
    <!-- /.modal-content -->
</form>

<script type="text/javascript">
    $('#datepicker').datepicker({
        autoclose: true
    });

    function newDayAdd(inputDate,addDay){
            var d = new Date(inputDate);
            d.setDate(d.getDate()+addDay);
            mkMonth=d.getMonth()+1;
            mkMonth=new String(mkMonth);
            if(mkMonth.length==1){
                mkMonth="0"+mkMonth;
            }
            mkDay=d.getDate();
            mkDay=new String(mkDay);
            if(mkDay.length==1){
                mkDay="0"+mkDay;
            }
            mkYear=d.getFullYear();
            return mkYear+"-"+mkMonth+"-"+mkDay;
            //  return mkMonth+"/"+mkDay+"/"+mkYear; // แสดงผลลัพธ์ในรูปแบบ เดือน/วัน/ปี
        }


        function sortValue(elm){
            document.getElementById('Days').value=elm.value;
            $.ajax({
                url : "{{URL::to('package/program/sortby_days')}}",
                type: "get",
                data: {'id':elm.value},
                success: function(data) {
                    $('tbody').html(data);
                }
            });
        }

        $('#startDate').on('blur',function(e){
            var txtdate = e.target.value;
            var DateEnd = newDayAdd( txtdate, {{ @$Default->packageDays-1 }} );
            document.getElementById('DateEnd').value = DateEnd;
        });
</script>
       