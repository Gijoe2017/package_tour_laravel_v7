@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <?php
        App::setLocale(Session::get('language'));
    ?>
    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
        .col-md-1{
            padding-right: 2px;
            padding-left: 2px;
        }
        .col-md-3{
            padding-right: 2px;
            padding-left: 2px;
        }
    </style>
        <section class="content">
            <div class="row">
                @if($PackageLastest)
                <div class="panel">
                    <div class="panel-body">
                        <div class="panel-header">
                           <p><strong>{{trans('common.recently_opened_tour_packages')}}</strong></p>
                        </div>
                        <div class="row">
                        @foreach($PackageLastest as $rows)
                            <?php
                            $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                            $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                            $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                            $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                            ?>
                               <div class="col-md-12" style="padding-bottom: 10px">
                                    <a href="{{url('package/details/'.$rows->packageID)}}"><span class="text-danger">{{trans('common.code')}} {{$rows->packageID}}:</span> {{$rows->packageName}}</a>
                                    <span class="text-red"> | </span> (  <a href="{{url('package/checking/'.$rows->packageID)}}" ><i class="fa fa-edit"></i> {{trans('common.checking')}}</a>   )

                                    <br>
                                </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
                    <div class="panel">
                        <div class="panel-heading">
                            {{--<form id="form-set-country"  method="post" action="{{action('Package\PackageController@sort_by_country')}}">--}}
                                {{--{{csrf_field()}}--}}
                                <input type="hidden" name="status" id="status">
                                <div class="row">
                                    <div class="col-md-1 text-right">
                                        <label>{{trans('common.filter')}} : </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" >
                                            <?php
                                                $Partner=DB::table('package_tour_partner as a')
                                                    ->join('timeline_info as b','b.timeline_id','=','a.request_partner_id')
                                                    ->where('a.partner_id',Session::get('timeline_id'))
                                                    ->get();
                                            ?>
                                            <select name="partner" id="partner" class="form-control">
                                                <option value="">{{trans('common.choose')}}{{trans('common.partner')}}</option>
                                                <option value="all" {{isset($partner)?$partner=='all'?'Selected':'':''}}>{{trans('common.partner')}}{{trans('common.all')}}</option>
                                                @foreach($Partner as $rows)
                                                    <option value="{{$rows->request_partner_id}}" {{Session::get('partner')==$rows->request_partner_id?'Selected':''}}>{{$rows->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" >
                                            <select name="country" id="country" class="form-control">
                                                <option value="">{{trans('common.choose')}}{{trans('common.country')}}</option>
                                                <option value="all" {{isset($country)?$country=='all'?'Selected':'':''}}>{{trans('common.country')}}{{trans('common.all')}}</option>
                                                @foreach($PackageCountry as $rows)
                                                    <option value="{{$rows->country_id}}" {{Session::get('country')==$rows->country_id?'Selected':''}}>{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                     </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="txtsearch" name="txtsearch" value="{{Session::get('txtsearch')}}" placeholder="{{trans('common.search_by_name')}}">
                                    </div>
                                    <div class="col-md-1">
                                        <div class="input-group-append pull-left">
                                            <button class="btn btn-success" type="submit">{{trans('common.search')}}</button>
                                        </div>
                                    </div>

                                    {{--<div class="col-md-4">--}}
                                    {{--<div class="input-group">--}}
                                    {{--<input type="text" name="txtsearch" id="txtsearch" class="form-control" placeholder="Search by name">--}}
                                    {{--<div class="input-group-append">--}}
                                    {{--<span class="input-group-text"><i class="fa fa-search"></i> </span>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-md-12 text-center" ><hr>
                                        <div class="btn-group">
                                            <button type="button" id="my_checking" data-id="my_checking" class="status btn {{Session::get('status')=='my_checking'?'active  btn-success':'btn-default'}}"> {{trans('common.the_package_i_checked')}}</button>
                                            <button type="button" id="not_check" data-id="not_check" class="status btn {{Session::get('status')=='not_check'?'active btn-success':'btn-default'}}"> {{trans('common.packages_that_i_havenot_checked')}}</button>
                                            <button type="button" id="public"  data-id="public" class="status btn {{Session::get('status')=='public'?'active btn-success':'btn-default'}} "> {{trans('common.open_package')}}</button>
                                            <button type="button" id="close_package"  data-id="close_package" class="status btn {{Session::get('status')=='close_package'?'active btn-success':'btn-default'}}"> {{trans('common.close_package')}}</button>
                                            <a href="{{url('package/clear/search')}}" class="btn btn-info"><i class="fa fa-refresh"></i> {{trans('common.clear_filter')}}</a>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-12" style="margin-top: 15px">--}}
                                        {{--<div class="form-group col-md-1" ></div>--}}
                                        {{--<div class="form-group col-md-3" >--}}
                                            {{--<a  class="status btn btn-block btn-default" data-id="Y"><i class="fa fa-check"></i> แพ็คเกจที่ฉันเป็นคนตรวจสอบ</a>--}}
                                            {{--<input class="status"  value="Y" name="status" id="status" type="radio">  แพ็คเกจที่ฉันเป็นคนตรวจสอบ--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group col-md-3" >--}}
                                            {{--<a  class="status btn btn-block btn-default" data-id="Y"><i class="fa fa-check"></i> แพ็คเกจที่ฉันเป็นคนตรวจสอบ</a>--}}
                                            {{--<input class="status" value="not_check" name="status" id="status" type="radio"> แพ็คเกจที่ฉันเป็นคนตรวจสอบ--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group col-md-3" >--}}
                                            {{--<a  class="status btn btn-block btn-default" data-id="Y"><i class="fa fa-check"></i> แพ็คเกจที่เปิดขาย</a>--}}
                                            {{--<input class="status" value="public" name="status" id="public" type="radio">  แพ็คเกจที่เปิดขาย--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>

                            {{--</form>--}}
                        </div>



                    </div>

                    <div class="package-list">

                    <div class="row">
                    <div class="col-md-12">{{!Session::get('status')?Session::put('status','all'):''}}
                        <h3 class="alert alert-info"><i class="fa fa-list"></i> {{trans('common.list_of_tour_packages')}} {{trans('common.'.Session::get('status')).":".$Packages->total()}}</h3>
                    </div>
                    </div>

                    <div class="infinite-scroll">
                        <div class="card-columns">
                            @foreach($Packages as $rows)
                                <?php

                                    if($rows->package_owner=='Yes'){
                                        $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                                        if(isset($timeline->name)){
                                            $pacakge_by=$timeline->name;
                                        }
                                    }else{
                                        $timeline_sell=\App\Timeline::where('id',$rows->timeline_id)->first();
                                        $timeline=\App\Timeline::where('id',$rows->owner_timeline_id)->first();
                                        if(isset($timeline->name)){
                                            $pacakge_by=$timeline->name;
                                        }
                                        if(isset($timeline_sell->name)){
                                            $sell_by=$timeline_sell->name;
                                        }
                                    }
                                ?>
                                <div class="card card-pin">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <strong><span class="text-danger">{{trans('common.code')}} {{$rows->packageID}}:</span> {{$rows->packageName}}</strong> <BR>
                                            @if(isset($sell_by))
                                            <small class="text-success">{{trans('common.sell_by')}} : {{$sell_by}}</small><BR>
                                            @endif
                                            @if(isset($pacakge_by))
                                                <small class="text-info">{{trans('common.package_by')}} : {{$pacakge_by}}
                                                    @if($rows->original_package_code)
                                                        <BR> {{trans('common.source_code_from_the_tour_company')}} : {{$rows->original_package_code}}
                                                    @endif
                                                </small>
                                            {{--<small class="text-info">{{trans('common.package_by')}} : {{$pacakge_by}}--}}
                                                {{--@if($rows->original_package_code)--}}
                                                {{--({{$rows->original_package_code}})--}}
                                                {{--@endif--}}
                                            {{--</small>--}}
                                            @endif
                                            @if($rows->package_close=='Y')
                                                <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->package_close)}}</p>

                                            @elseif($rows->packageStatus=='CP')
                                                <?php
                                                    $user=DB::table('users_info')->where('UserID',$rows->package_checked_by)->first();
                                                    $date= new Date($rows->package_checked_date);
                                                    $check=DB::table('package_tour_checked_log')->where('package_id',$rows->packageID)->count();
                                                ?>
                                                <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}
                                                    {{$date->format('d F Y')}} : {{trans('common.by')}} {{isset($user->FirstName)?$user->FirstName:''}}
                                                    @if($check>1)
                                                        <a href="{{url('package/ajax/list/user_approve/'.$rows->packageID)}}" data-target="#popupForm" data-toggle="modal"> <i class="fa fa-arrow-circle-o-right"></i></a>
                                                    @endif
                                                </p>
                                            @else
                                                <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}</p>
                                            @endif
                                        </div>
                                        <div class="panel-body">
                                            <a href="{{url('package/details/'.$rows->packageID)}}">
                                                <img class="img-thumbnail" src="{{url('/package/tour/mid/'.$rows->Image)}}">
                                            </a>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="timeline-footer text-center">
                                                <?php
                                                $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                                                $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                                                $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                                                $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                                                ?>
                                                {{--@if($check>0)--}}
                                                {{--<a href="{{url('package/users/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> Members</a>--}}
                                                <a href="{{url('package/checking/'.$rows->packageID)}}" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> {{trans('common.checking')}}</a>
                                                <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary btn-xs"><i class="fa fa-search-minus"></i> {{trans('common.preview')}}</a>
                                                {{--@endif--}}
                                                <a href="{{url('package/edit/'.$rows->packageID)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                @if($check_del==0 && $check_del2==0 && $check_owner==1)
                                                    <a href="{{url('package/delete/'.$rows->packageID)}}" onclick="return confirm_delete()" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                    {{$Packages->render()}}
                </div>
                    </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
        </section>
        {{--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>--}}
        <script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>



        <script type="text/javascript">
            $('ul.pagination').hide();
            $(function () {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {
                        $('ul.pagination').remove();
                      //  jQuery("time.timeago").timeago();
                    }
                });
            });


            $('body').on('blur','#txtsearch', function (e) {
                if(e.target.value){
                    var partner=$('#partner').val();
                    var country=$('#country').val();
                    $('#preload').show();
                    $.ajax({
                        type:'get',
                        url:SP_source() + 'package/search-partner-package',
                        data:{'txtsearch':e.target.value,'partner':partner,'country':country},
                        success:function(data){
                            $('.package-list').html(data);
                            $('#preload').hide();
                        }
                    });
                  //  $('#form-set-country').submit();
                }
            });

            $('body').on('click','.status',function (e) {
                var country=$('#country').val();
                var partner=$('#partner').val();
                var txtsearch=$('#txtsearch').val();
                $('#status').val($(this).data('id'));
                $('#preload').show();

                var status=$(this).data('id');

                    $('.status').removeClass('active btn-success');
                    $(this).addClass('active btn-success');
                     $.ajax({
                        type:'get',
                        url:SP_source() + 'package/search-partner-package',
                        data:{'country':country,'partner':partner,'txtsearch':txtsearch,'status':status},
                        success:function(data){
                            $('#preload').hide();
                            $('.package-list').html(data);
                        }
                     });
            });

            $('body').on('change','#country',function (e) {
                var partner=$('#partner').val();
                var txtsearch=$('#txtsearch').val();
                var status=$('#status').val();
                $('#preload').show();
                if(e.target.value){
//                    $('#form-set-country').submit();
                    $.ajax({
                        type:'get',
                        url:SP_source() + 'package/search-partner-package',
                        data:{'country':e.target.value,'partner':partner,'txtsearch':txtsearch,'status':status},
                        success:function(data){
                            $('.package-list').html(data);
                            $('#preload').hide();
                        }
                    });

                }
            });

            $('body').on('change','#partner',function (e) {
                var text=  $("#country option:first").text();
                $('#txtsearch').val('');
                $('.status').removeClass('active');
                $('#status').val('');

                if(e.target.value){
                    $('#preload').show();
                   // alert('test');
                    $.ajax({
                        type:'get',
                        url:SP_source() + 'ajax/get-country-partner',
                        data:{'partner':e.target.value},
                        success:function(data){
                            $('#country').empty();
                            $('#country').append("<option value=''>"+ text +"</option>");
                            $('#country').append(data);
                        }
                    });
                  //  alert('test');
                    $.ajax({
                        type:'get',
                        url:SP_source() + 'package/search-partner-package',
                        data:{'partner':e.target.value},
                        success:function(data){
                            $('.package-list').html(data);
                            $('#preload').hide();
                        }
                    });
                   // $('#form-set-country').submit();
                }
            });


        </script>
@endsection
