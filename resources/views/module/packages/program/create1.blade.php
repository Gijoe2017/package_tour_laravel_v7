@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-body">

<link rel="stylesheet" href="{{asset('member/assets/dist/css/bootstrap-formhelpers.css')}}">
<script src="{{asset('member/assets/dist/js/bootstrap-formhelpers.js')}}"></script>
<form role="form" id="AutoForm"  action="{{action('Package\ScheduleController@store')}}" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    {{--<input type="hidden" name="Days" id="Days">--}}

    <div class="panel-content">
        <div class="panel-header">
            <h2 class="panel-title-site text-center">
                @if(isset($HighlightToday))
                    {!! str_limit($HighlightToday->Highlight,50) !!}
                @endif
            </h2>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="row">
                <div class="col-md-6">
                    <label><strong>{{trans('package.Time')}} *</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                    <input type="text" class="form-control bfh-phone"  name="packageTime" id="packageTime" data-format="dd:dd" required>
                </div>
                <div class="col-md-6">
                    <label><strong>{{trans('package.Time')}} *</strong> <span id="error1" style="display: none; color: red">Need to insert Time.</span></label><BR>
                    <select class="form-control" name="packageTime2" id="packageTime2">
                        <option value="0"> {{trans('package.Choose')}} </option>
                        @foreach(\App\Package\OptionTime::active()->get() as $rows)
                            <option value="{{$rows->TimeCode}}">{{$rows->Time_text}}</option>
                        @endforeach
                    </select>
                </div>
                </div>

                <div class="clearfix"></div>

                <div class="form-group"><BR>
                    <label><strong>{{trans('package.PackageDetails')}}*</strong></label><BR>
                    <textarea type="text" rows="10" class="form-control summernote" name="packageDetails" id="packageDetails" ></textarea>
                </div>

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            @if(Session::has('checking'))
                <a  href="{{url('package/details_check/'.Session::get('package'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            @elseif(Session::get('event')=='details')
                <a  href="{{url('package/details/'.Session::get('package'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            @else
                <a  href="{{url('package/schedule')}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            @endif
            <button type="submit"  id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>
    </div>
    <!-- /.modal-content -->
</form>

<script>


    $('#packageTime').on('keyup',function (e) {

        var time=document.getElementById('packageTime').value;

        if(time.length>0){
            $("#packageTime2").prop('disabled', true);
        }else{
            $("#packageTime2").prop('disabled', false);
        }

    });

    $('#packageTime2').on('change',function (e) {
        if(e.target.value>0){
            $.ajax({
                url: '{{URL::to('package/program/gettime')}}',
                type: "get",
                data: {'id': e.target.value},
                success: function (data) {
                    $("#packageTime").val(data);
                    $("#packageTime").prop('readonly', true);
                }
            });
        }else{
            $("#packageTime").val(e.target.value);
            $("#packageTime").prop('readonly', false);
        }
    });
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300,
            toolbar:[
                ['font',['bold','italic','underline','clear']],
                ['para',['ul','ol']],
                ['view',['fullscreen','codeview']],
            ],
            cleaner:{
                action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                newline: '<br>', // Summernote's default is to use '<p><br></p>'
                notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                icon: '<span class="note-icon-eraser"> Clear format</span>',
                keepHtml: false, // Remove all Html formats
                keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], // If keepHtml is true, remove all tags except these
                keepClasses: false, // Remove Classes
                badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                badAttributes: ['style', 'start'], // Remove attributes from remaining tags
                limitChars: false, // 0/false|# 0/false disables option
                limitDisplay: 'both', // text|html|both
                limitStop: false // true/false
            }
        });
    });
//    $(function () {
//        $('#packageDetails').wysihtml5({
//            toolbar: {
//                "font-styles": false, // Font styling, e.g. h1, h2, etc.
//                "emphasis": true, // Italics, bold, etc.
//                "lists": false, // (Un)ordered lists, e.g. Bullets, Numbers.
//                "html": false, // Button which allows you to edit the generated HTML.
//                "link": false, // Button to insert a link.
//                "image": false, // Button to insert an image.
//                "color": false, // Button to change color of font
//                "blockquote": false, // Blockquote
//            }
//        });
//    });

</script>
                </div></div></div></section>
@endsection