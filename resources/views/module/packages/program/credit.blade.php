@extends('layouts.member.layout_master_new')

@section('content')
<style>
    .form-group{
        margin-bottom: 15px;
    }
</style>
<div class="row">
    <div class="col-md-12">
<form role="form" id="AutoForm"  action="{{action('Package\PackageController@updateCredit')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="id" value="{{$Image->id}}">
<div class="panel">
    <div class="panel-header">

        <h3 class="panel-title-site text-center"> ให้เครดิตกับเจ้าของภาพ </h3>
    </div>
    <div class="panel-body">
        <div class="row">
        <div class="col-lg-12">
            <div class="col-md-6">
                <img src="{{asset('user/gallery/mid/'.$Image->source)}}" class="img-responsive">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label><strong>{{trans('package.Credit')}}</strong> </label><BR>
                    <input type="text" class="form-control" name="photo_credit_name" id="photo_credit_name" value="{{$Image->photo_credit_name}}">
                </div>

                <div class="form-group">
                    <label><strong>{{trans('package.CreditUrl')}}* </strong></label> <BR>
                    <input type="text" class="form-control" name="photo_credit_url" id="photo_credit_url" value="{{$Image->photo_credit_url}}">
                </div>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-footer">
        <a class="btn btn-default  pull-left" href="{{url('package/program/highlight/'.$Image->id)}}"><i class="fa fa-reply"></i> Back</a>
        <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
    </div>
</div>
</form>
    </div>
</div>
@endsection