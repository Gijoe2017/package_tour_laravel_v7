<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
    <h3 class="modal-title-site text-center"> {{trans('LItem.Swapposition')}}  </h3>
</div>
<div class="modal-body">
<div class="row">
    <div class="col-lg-12">

        @if(count($Highlight))
            <ul id="sortable" class="ui-sortable">
                @foreach($Highlight as $pic)
                    <li id="img-{{$pic->LocationID}}">

                        <img class="quickview1 img-thumbnail" src="{{$pic->image_path.'/small/'.$pic->Image}}">

                    </li>
                @endforeach
            </ul>
        @endif
    </div>

    <div class="clearfix"></div>
</div>
</div>

<div class="modal-footer">
    <a  class="btn btn-default pull-right" data-dismiss="modal" aria-hidden="true">X  {{trans('package.Close')}}</a>
</div>


<script language="JavaScript" >
    $(function () {
        $('#sortable').sortable({
            update: function (event, ui) {
                var data = $(this).sortable('serialize');
                // alert(ui.item.index());
                $.ajax({
                    data: data,
                    type: 'get',
                    url: '{{URL::to('ajax/Highlight/location/sortable')}}',
                    success:function () {
                        $("#HighlightUse").load(location.href + " #HighlightUse");
                    }

                });
            }
        });

        $("#sortable").disableSelection();
    });
</script>