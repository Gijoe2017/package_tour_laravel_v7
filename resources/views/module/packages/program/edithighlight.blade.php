<link rel="stylesheet" href="{{asset('assets/css/build.css')}}" type="text/css"  />
<link rel="stylesheet" href="{{asset('assets/icon-line-pro/style.css')}}">
<form role="form"  action="{{action('Package\HighlightOfDayController@store')}}" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="Days" value="{{Session::get('days')}}">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h3 class="modal-title-site text-center"> {{trans('package.Highlight') .$DayName->DayName}}  </h3>
    </div>
<div class="modal-body">

    <div class="col-lg-12">

        <div class="form-group">
            <label><strong>{{trans('package.Title').$DayName->DayName}}</strong> <span id="error2" style="display: none; color: red">Need to insert Head Details.</span></label><BR>
            <textarea type="text" class="form-control" name="Highlight" id="Highlight" required></textarea>
        </div>
            <label><strong><i class="icon-travel-109 u-line-icon-pro align-middle"></i> &nbsp;ตัวเลือกอาหารสำหรับวันนี้</strong></label>
        <br>
        <div class="checkbox checkbox-success checkbox-inline">
            <input type="checkbox" class="styled" name="Breakfast" id="Breakfast" value="Y">
            <label for="inlineCheckbox1"> อาหารเช้า </label>
        </div>
        <div class="checkbox checkbox-success checkbox-inline">
            <input type="checkbox" class="styled" name="Lunch" id="Lunch" value="Y" >
            <label for="inlineCheckbox2"> อาหารกลางวัน </label>
        </div>
        <div class="checkbox checkbox-success checkbox-inline">
            <input type="checkbox" class="styled" name="Dinner" id="Dinner" value="Y">
            <label for="inlineCheckbox3"> อาหารเย็น </label>
        </div>
        <br>

    </div>
    {{--<div class="col-lg-4">--}}
        {{--<div class="form-group">--}}
        {{--<div class="text-center">--}}
            {{--<img src="{{asset('images/default-add.jpg')}}" id="blah" class="img-thumbnail">--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="clearfix"></div>
</div>

<div class="modal-footer">
    <button type="submit" id="saveSchedule" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
</div>


    </form>
<script src="{{asset('assets/js/bootstrap-filestyle.min.js')}}"></script>

<script language="javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        content: function(){return '<img src="'+$(this).data('img') + '" />';}
    });
    $("#imgInp").change(function(){
        readURL(this);
    });

//    var simplemde = new SimpleMDE(
//            { element: document.getElementById("Highlight"),
//                toolbar: ["bold", "italic", "heading", "|", "unordered-list", "ordered-list", "preview", "fullscreen", "side-by-side", "guide"],
//
//            });
</script>