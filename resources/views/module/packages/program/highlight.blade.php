@extends('layouts.member.layout_master_new')

@section('content')

    {{--<script src="https://unpkg.com/smartphoto@1.1.0/js/smartphoto.min.js"></script>--}}
    {{--<link rel="stylesheet" href="https://unpkg.com/smartphoto@1.1.0/css/smartphoto.min.css">--}}

    {{--<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery.jspanel.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('themes/default/assets/select1/jquery.typeahead.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('themes/default/assets/css/style-module-package.css')}}">--}}


    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }


        .panel-create .panel-heading {
            background: #FAFBFC;
            font-size: 16px;
            color: #5B6B81;
            line-height: 21px;
            border-width: 0px;
            border-bottom: 1px solid #E6EAEE;
        }
        .panel-create .panel-body {
            padding: 0px;
            padding-bottom: 7px;
        }
        .panel-create .panel-body textarea {
            border: 0px;
            box-shadow: none;
            padding-left: 0px;
            resize: none;
            font-size: 14px;
            padding: 6px 15px;
        }
        .panel-create .panel-footer {
            background: #FAFBFC;
            padding: 7px 15px;
            border-top-color: #E6EAEE;
        }
        .panel-create .panel-footer ul {
            margin: 0px;
        }
        .panel-create .panel-footer .left-list {
            float: left;
        }
        .panel-create .panel-footer .left-list li {
            padding-left: 9px;
            padding-right: 9px;
        }
        .panel-create .panel-footer .left-list li:first-child {
            padding-left: 0px;
        }
        @media (max-width: 460px) {
            .panel-create .panel-footer .left-list li {
                padding-left: 5px;
                padding-right: 5px;
            }
        }
        @media (max-width: 400px) {
            .panel-create .panel-footer .left-list li {
                padding-left: 4px;
                padding-right: 4px;
            }
        }
        .panel-create .panel-footer .left-list li a {
            color: #859AB5;
            font-size: 18px;
        }
        @media (max-width: 350px) {
            .panel-create .panel-footer .left-list li a {
                font-size: 15px;
            }
        }
        .panel-create .panel-footer .right-list {
            float: right;
        }
        .panel-create .panel-footer .right-list li {
            padding-left: 8px;
            padding-right: 8px;
        }
        .panel-create .panel-footer .right-list li:last-child {
            padding-right: 0px;
        }
        @media (max-width: 424px) {
            .panel-create .panel-footer .right-list li {
                padding-left: 0px;
                padding-right: 4px;
            }
        }
        @media (max-width: 350px) {
            .panel-create .panel-footer .right-list li {
                padding-right: 1px;
            }
        }
        .panel-create .panel-footer .right-list .create-album {
            color: #2298F1;
            font-size: 14px;
            text-decoration: none;
            vertical-align: middle;
        }
        @media (max-width: 424px) {
            .panel-create .panel-footer .right-list .create-album {
                font-size: 13px;
            }
        }
        @media (max-width: 350px) {
            .panel-create .panel-footer .right-list .create-album {
                font-size: 12px;
            }
        }
        .panel-create .panel-footer .right-list .btn {
            padding: 2px 17px;
        }
        @media (max-width: 350px) {
            .panel-create .panel-footer .right-list .btn {
                padding: 2px 12px;
                font-size: 12px;
            }
        }
        .panel-post {
            border-width: 0px;
        }
        .panel-post .panel-heading {
            padding: 14px 15px;
            height: auto;
            border: 1px solid #E6EAEE !important;
        }
        .panel-post .panel-heading .post-author .post-options {
            float: right;
            color: #859AB5;
            margin-top: 6px;
        }
        .panel-post .panel-heading .post-author .post-options > ul > li > a {
            color: #859AB5;
            border-radius: 83px;
            font-size: 20px;
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu {
            right: -15px;
            left: auto;
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu li:hover a {
            background-color: #FAFBFC;
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu li a {
            color: #354052;
            font-size: 14px;
            font-family: 'Source Sans Pro', sans-serif;
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu li a i {
            width: 20px;
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a {
            font-weight: 600;
            font-size: 14px;
        }
        @media (max-width: 500px) {
            .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a {
                font-size: 13px;
                padding-left: 10px;
                padding-right: 16px;
            }
        }
        @media (max-width: 350px) {
            .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a {
                font-size: 12px;
            }
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a .small-text {
            font-weight: 400;
            font-size: 14px;
            color: #7F8FA4;
            display: block;
            padding-left: 22px;
        }
        @media (max-width: 500px) {
            .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a .small-text {
                font-size: 12px;
            }
        }
        @media (max-width: 350px) {
            .panel-post .panel-heading .post-author .post-options .dropdown-menu .main-link a .small-text {
                font-size: 11px;
            }
        }
        .panel-post .panel-heading .post-author .post-options .dropdown-menu li.active a {
            background-color: #FAFBFC;
        }
        .panel-post .panel-heading .post-author .user-avatar {
            width: 40px;
            height: 40px;
            float: left;
            margin-right: 10px;
        }
        .panel-post .panel-heading .post-author .user-avatar img {
            width: 100%;
            height: 100%;
            border-radius: 4px;
        }
        .panel-post .panel-heading .post-author .user-post-details {
            line-height: 21px;
        }
        .panel-post .panel-heading .post-author .user-post-details ul {
            padding-left: 50px;
        }
        .panel-post .panel-heading .post-author .user-post-details li {
            color: #859AB5;
            font-size: 16px;
            font-weight: 400;
        }
        .panel-post .panel-heading .post-author .user-post-details li .user {
            text-transform: capitalize;
        }
        .panel-post .panel-heading .post-author .user-post-details li .post-time {
            color: #354052;
            font-size: 12px;
            letter-spacing: 0px;
            margin-right: 3px;
        }
        .panel-post .panel-heading .post-author .user-post-details li:last-child {
            font-size: 14px;
        }
        .panel-post .panel-heading .post-author .user-post-details li:last-child i {
            margin-left: 6px;
            margin-right: 2px;
        }
        .panel-post .panel-heading .post-author .user-post-details li:last-child .post-place {
            text-transform: capitalize;
        }
        .panel-post .panel-body {
            border: 1px solid #E6EAEE;
            border-top-width: 0px;
            padding-bottom: 7px;
        }
        .panel-post .panel-body .text-wrapper p {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 14px;
            font-weight: 400;
            color: #5B6B81;
            letter-spacing: 0.3px;
            line-height: 20px;
            margin-bottom: 0px;
            word-break: break-word;
        }
        .panel-post .panel-body .text-wrapper .post-image-holder {
            margin-top: 10px;
            /*max-height: 200px;*/
            overflow: hidden;
        }
        .panel-post .panel-body .text-wrapper .post-image-holder img {
            max-width: 100%;
            max-height: 200px;
            margin-top: 3px;
            margin-right: 3px;
            margin-bottom: 3px;
            border-radius: 4px;
        }




        .panel-post .panel-body iframe {
            width: 100%;
            margin-top: 10px;
            height: 273px;
        }
        .panel-post .panel-body .actions-count {
            margin-top: 10px;
            margin-bottom: 0px;
        }
        .panel-post .panel-body .actions-count li a {
            text-decoration: none;
            font-size: 13px;
            text-transform: capitalize;
            color: #859AB5;
        }
        @media (max-width: 350px) {
            .panel-post .panel-body .actions-count li a {
                font-size: 12px;
            }
        }

        .panel-post .panel-body .actions-count li a .count-circle {
            width: 16px;
            height: 16px;
            border-radius: 50%;
            text-align: center;
            display: inline-block;
            padding: 0px;
            background-color: #9FA9BA;
            line-height: 13px;
            margin-right: 3px;
            vertical-align: 2px;
        }
        .panel-post .panel-body .actions-count li a .count-circle i {
            font-size: 10px;
            color: #fff;
        }
        .panel-post .panel-body.image-post {
            padding: 0px;
        }
        .panel-post .panel-body.image-post .text-wrapper p {
            padding-top: 15px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .panel-post .panel-body.image-post .text-wrapper .post-image-holder {
            margin-top: 10px;
            max-height: 800px;
            overflow: hidden;
        }
        .panel-post .panel-body.image-post .text-wrapper .post-image-holder a {
            width: 100%;
            height: auto;
            margin-bottom: 0px;
        }
        .panel-post .panel-body.image-post .text-wrapper .post-image-holder a img {
            width: 100%;
            height: auto;
            border-radius: 0px;
            margin: 0px;
        }
        .panel-post .panel-body.image-post .actions-count {
            padding-left: 15px;
            padding-bottom: 7px;
        }

        .panel-post .panel-footer.socialite .footer-list {
            margin-bottom: 0px;
        }
        .panel-post .panel-footer.socialite .footer-list li {
            padding-left: 14px;
            padding-right: 14px;
        }
        @media (max-width: 350px) {
            .panel-post .panel-footer.socialite .footer-list li {
                padding-left: 10px;
                padding-right: 10px;
            }
        }
        .panel-post .panel-footer.socialite .footer-list li:first-child {
            padding-left: 5px;
        }
        .panel-post .panel-footer.socialite .footer-list li:nth-child(2) {
            padding-left: 5px;
        }
        .panel-post .panel-footer.socialite .footer-list li a {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 400;
            font-size: 14px;
            color: #859AB5;
            text-transform: capitalize;
        }
        .panel-post .panel-footer.socialite .footer-list li a i {
            font-size: 16px;
            color: #9FA9BA;
            letter-spacing: 0px;
            margin-right: 4px;
        }
        .panel-post .comments-section {
            width: 100%;
            height: auto;
            padding-left: 4px;
            padding-right: 4px;
            background-color: #eff3f6;
        }
        .panel-post .comments-section .comments-wrapper {
            width: 100%;
            padding-left: 15px;
            padding-right: 15px;
            padding-bottom: 26px;
            height: auto;
            background: #FCFCFC;
            border: 1px solid #E6EAEE;
            border-top-width: 0px;
        }
        .panel-post .comments-section .comments-wrapper .comment-form {
            position: relative;
        }
        .panel-post .comments-section .comments-wrapper .comment-form .comment-holder {
            height: 36px;
            position: relative;
        }
        .panel-post .comments-section .comments-wrapper .comment-form .comment-holder .meme-reply {
            position: absolute;
            top: 50%;
            margin-bottom: 0px;
            margin-top: -9px;
            right: 10px;
        }
        .panel-post .comments-section .comments-wrapper .comment-form .comment-holder .meme-reply li a i {
            font-size: 16px;
            color: #859AB5;
        }
        .panel-post .comments-section .comments-wrapper .comment-form #comment-image-holder {
            padding-top: 10px;
        }
        .panel-post .comments-section .comments-wrapper .comment-form #comment-image-holder img {
            width: auto !important;
            height: 60px !important;
            margin-right: 5px;
            padding-left: 13px;
        }
        .panel-post .comments-section .comments-wrapper .to-comment {
            padding-top: 16px;
        }
        .panel-post .comments-section .comments-wrapper .to-comment.comment-reply {
            margin-left: 45px;
            padding-top: 12px;
        }
        .panel-post .comments-section .comments-wrapper .to-comment.comment-reply .commenter-avatar {
            width: 30px;
            height: 30px;
        }
        .panel-post .comments-section .comments-wrapper .to-comment.comment-reply .comment-textfield {
            padding-left: 45px;
        }
        .panel-post .comments-section .comments-wrapper .to-comment.comment-reply .comment-textfield .form-control {
            height: 30px;
            font-size: 13px;
        }
        .panel-post .comments-section .comments-wrapper .commenter {
            margin-top: -3px;
            padding-left: 45px;
        }
        .panel-post .comments-section .comments-wrapper .commenter-avatar {
            width: 36px;
            height: 36px;
            float: left;
            display: inline-block;
        }
        .panel-post .comments-section .comments-wrapper .commenter-avatar img {
            width: 100%;
            height: 100%;
            border-radius: 4px;
        }
        .panel-post .comments-section .comments-wrapper .comment-replies {
            margin-left: 45px;
        }
        .panel-post .comments-section .comments-wrapper .comment-textfield {
            padding-left: 51px;
        }
        .panel-post .comments-section .comments-wrapper .comment-textfield .form-control {
            border-radius: 0px;
            padding-top: 8px;
            padding-right: 60px;
            padding-bottom: 8px;
        }
        .panel-post .comments-section .comments-wrapper .comments {
            padding-top: 15px;
        }
        .panel-post .comments-section .comments-wrapper .delete-comment {
            float: right;
            margin-right: 2px;
            font-size: 14px;
            color: #7F8FA4;
        }
        .panel-post .comments-section .comments-wrapper .commenter-name a {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 600;
            font-size: 14px;
            color: #2298F1;
            letter-spacing: 0px;
            text-transform: capitalize;
        }


        .panel-post .comments-section .comments-wrapper .comment-options li {
            padding-left: 4px;
            padding-right: 4px;
            color: #859AB5;
        }
        .panel-post .comments-section .comments-wrapper .comment-options li:first-child {
            padding-left: 5px;
        }
        .panel-post .comments-section .comments-wrapper .comment-options li a {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 400;
            font-size: 12px;
            color: #859AB5;
            text-transform: capitalize;
        }
        .panel-post .comments-section .comments-wrapper .comment-options li a i {
            margin-right: 3px;
        }
        .panel-post .comments-section .comments-wrapper .replies-count {
            margin-top: 13px;
            width: 100px;
            display: block;
            text-transform: capitalize;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 400;
            font-size: 14px;
            color: #9FA9BA;
            margin-left: 45px;
        }
        .panel-post .comments-section .comments-wrapper .replies-count i {
            font-size: 16px;
            color: #9FA9BA;
            margin-right: 5px;
            vertical-align: -1px;
        }


    </style>

    <div class="row">
            <form method="post" action="{{action('Package\PackageController@search_location_highlight')}}">
                <div class="col-md-2">
                    <a href="{{url('package/program/highlight/'.Session::get('program_id'))}}" class="btn btn-block btn-default"><i class="fa fa-list"></i> {{trans('common.list_all')}}</a>
                </div>
                <div class="col-md-6">
                        {{csrf_field()}}
                        <div class="input-group">
                            <input type="text" name="txtsearch" class="form-control" placeholder="Search..." required>
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                </div>
                <div class="col-md-2">
                    <a href="{{ url('/add/location/highlight') }}" class="btn btn-success btn-block"  target="_parent"><i class="fa fa-map-marker"></i> {{trans('common.create_location')}}</a>
                </div>
                <div class="col-md-2">
                    @if(Session::has('checking'))
                        <a href="{{ url('package/details_check/'.Session::get('package')) }}" class="btn btn-default btn-block"  ><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                        @elseif(Session::has('detail'))
                            <a href="{{ url('package/details/'.Session::get('package')) }}" class="btn btn-default btn-block"  ><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>

                        @else
                        <a href="{{ url('package/schedule') }}" class="btn btn-default btn-block"  ><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                    @endif
                </div>
            </form>

             @if($mylocations->count()>0)
             <div class="col-md-12">
                <h3 class="alert alert-danger"><i class="fa fa-list"></i> {{trans('common.highlight_in_schedule')}}</h3>
                    <div class="row">

                        @foreach($mylocations as $location)
                            <?php
                            $timeline=$location->timeline()->first();
                            $timeline_info=DB::table('timeline_info')
                                ->where('timeline_id',$location->timeline_id)
                                ->where('language_code',Session::get('language'))
                                ->first();
                            $post=\App\Post::where('represent_timeline_id',$location->timeline_id)->latest()->first();
                            if(!$post){
                                $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
                            }


                            $media=\App\Media::where('id',$timeline->avatar_id)->first();
                            $city=null;$state=null;
                            $country=$timeline->country()->where('language_code',Session::get('language'))->first();
                            if(!$country){
                                $country=$timeline->country()->where('language_code','en')->first();
                            }
                            if($timeline->state_id){
                                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
                                if(!$state){
                                    $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code','en')->first();
                                }
                                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
                                if(!$city){
                                    $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code','en')->first();
                                }
                            }
                            if(!$country){
                                $country=$timeline->country()->where('language_code','en')->first();
                            }
                            //    dd($post);

                            ?>
                            <div class="col-md-4">
                                <div class="panel panel-default panel-post animated" id="post{{ $post->id }}">
                                    <div class="panel-heading no-bg">
                                        <div class="post-author">
                                            <?php
                                            $date=\Date::parse($post->created_at);
                                            ?>
                                            <div class="user-avatar">
                                                <a href="{{ url($timeline->username) }}" target="_blank">
                                                    @if($media!=null)
                                                        <img src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }} " title="{{ $timeline->name }}">
                                                    @else
                                                        <img src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                    @endif
                                                </a>
                                                {{--<a href="{{ url($post->user->username) }}"><img src="{{ $post->user->avatar }}" alt="{{ $post->user->name }}" title="{{ $post->user->name }}"></a>--}}
                                                <?php
                                                $star_rating=0;
                                                $RateAll2=DB::table('star_rating')
                                                    ->where('timeline_id',$timeline->id)
                                                    ->whereIn('rate_group',[2,3,4,5])
                                                    ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
                                                    ->first();
                                                if($RateAll2){
                                                    if($RateAll2->star>0){
                                                        $star_rating=round($RateAll2->star/$RateAll2->userCount,1);
                                                    }
                                                }
                                                ?>
                                                @if($star_rating>0)
                                                    @if($star_rating>=4)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-success" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @elseif($star_rating>=3)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-warning" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @elseif($star_rating<3)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-danger" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="user-post-details">
                                                <ul class="list-unstyled no-margin">
                                                    <li>
                                                        @if(isset($sharedOwner))
                                                            <a target="_blank" href="{{ url($sharedOwner->user->username) }}" title="{{ '@'.$sharedOwner->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                {{ $sharedOwner->user->name }}
                                                            </a>
                                                            shared
                                                        @endif


                                                        <a target="_blank" href="{{ url($timeline->username) }}" title="{{ '@'.$post->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                            {{ isset($timeline_info->name)?$timeline_info->name:$timeline->name }}
                                                        </a>
                                                        @if($post->user->verified)
                                                            <span class="verified-badge bg-success">
                                                                <i class="fa fa-check"></i>
                                                            </span>
                                                        @endif

                                                        @if(isset($sharedOwner))
                                                            's post
                                                        @endif

                                                        @if($post->users_tagged->count() > 0)
                                                            {{ trans('common.with') }}
                                                            <?php $post_tags = $post->users_tagged->pluck('name')->toArray(); ?>
                                                            <?php $post_tags_ids = $post->users_tagged->pluck('id')->toArray(); ?>
                                                            @foreach($post->users_tagged as $key => $user)
                                                                @if($key==1)
                                                                    {{ trans('common.and') }}
                                                                    @if(count($post_tags)==1)
                                                                        <a href="{{ url($user->username) }}">{{ $user->name }}</a>
                                                                    @else
                                                                        <a href="#" data-toggle="tooltip" title="" data-placement="top" class="show-users-modal" data-html="true" data-heading="{{ trans('common.with_people') }}"  data-users="{{ implode(',', $post_tags_ids) }}" data-original-title="{{ implode('<br />', $post_tags) }}"> {{ count($post_tags).' '.trans('common.others') }}</a>
                                                                    @endif
                                                                    @break
                                                                @endif
                                                                @if($post_tags != null)
                                                                    <a href="{{ url('home/'.$user->username) }}" class="user"> {{ array_shift($post_tags) }} </a>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </li>

                                                    <li>
                    <span>
                    <?php
                    $category=\App\Category::where('category_id',$location->category_id)->where('language_code',Session::get('language'))->first();
                    if(!$category){
                        $category=\App\Category::where('category_id',$location->category_id)->where('language_code','en')->first();
                    }

                    if($location->category_sub1_id){
                        $sub1category=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                    }
                    ?>

                    @if($location->category_sub2_id>0)
                        <?php
                        $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                            ->where('language_code',Session::get('language'))
                            ->first();
                        if(!$category_sub2){
                            $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                ->where('language_code','en')
                                ->first();
                        }
                        ?>
                        {{$category_sub2->category_sub2_name}},

                    @elseif($location->category_sub1_id>0)

                        <?php
                        $subname=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                        if($subname){
                            echo $subname->sub1_name.',';
                        }
                        ?>

                    @else

                        @if($location->category_id)
                            <?php
                            $category=\App\Category::where('category_id',$location->category_id)
                                ->where('language_code',Session::get('language'))->first();
                            if($category){
                                $category=\App\Category::where('category_id',$location->category_id)
                                    ->where('language_code','en')->first();
                            }
                            ?>
                            {{$category->name}},

                        @endif
                    @endif

                </span>

                  <span>
					@if($city)
                        {{ $city->city }}
                    @endif
                    @if($state)
                        {{$state->state}}
                    @endif
                    @if($country)
                        {{$country->country}}
                    @endif
				  </span>


                  @if($post->location != NULL && !isset($sharedOwner))
                      {{ trans('common.at') }}
                      <span class="post-place">
                      <a target="_blank" href="{{ url('locations'.$post->location) }}">
                          <i class="fa fa-map-marker"></i> {{ $post->location }}
                      </a>
                      </span>
                                                    </li>
                                                    @endif
                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="panel-body">
                                        <div class="text-wrapper">
                                            <?php
                                            $links = preg_match_all("/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $post->description, $matches);
                                            $main_description = $post->description;
                                            ?>
                                            @foreach($matches[0] as $link)
                                                <?php
                                                $linkPreview = new LinkPreview($link);
                                                $parsed = $linkPreview->getParsed();
                                                foreach ($parsed as $parserName => $main_link) {
                                                    $data = '<div class="row link-preview">
                                                              <div class="col-md-3">
                                                                <a target="_blank" href="'.$link.'"><img src="'.$main_link->getImage().'"></a>
                                                              </div>
                                                              <div class="col-md-9">
                                                                <a target="_blank" href="'.$link.'">'.$main_link->getTitle().'</a><br>'.substr($main_link->getDescription(), 0, 500). '...'.'
                                                              </div>
                                                            </div>';
                                                }
                                                $main_description = str_replace($link, $data, $main_description);
                                                ?>
                                            @endforeach

                                            <p class="post-description">
                                                {{--************************ Cannot fig here ****************************--}}
                                                {!! $main_description !!}
                                                {{--{!! clean($main_description) !!}--}}
                                            </p>
                                            @if(count($post->images()->get())>0)
                                            <div class="post-image-holder  @if(count($post->images()->get()) == 1) single-image @endif">
                                                @foreach($post->images()->get() as $postImage)
                                                    @if($postImage->type=='image')
                                                        @if(!file_exists(storage_path('user/gallery/mid/'.$postImage->source)))
                                                            <a href="{{ url('user/gallery/'.$postImage->source) }}" data-lightbox="imageGallery.{{ $post->id }}" >
                                                                <img src="{{ url('user/gallery/mid/'.$postImage->source) }}" class="img-responsive" title="{{ $post->user->name }}" alt="{{ $post->user->name }}">
                                                                {{--{{ $post->user->name }} |--}}
                                                                {{--<time class="post-time timeago" datetime="{{ $post->created_at }}+00:00" title="{{ $post->created_at }}+00:00">--}}
                                                                {{--{{ $post->created_at }}+00:00--}}
                                                                {{--</time>--}}
                                                            </a>

                                                            <div class="form-group pull-right">
                                                                @if($postImage->photo_credit_name)

                                                                    <a href="{{$postImage->photo_credit_url}}">เครดิตภาพ:{{$postImage->photo_credit_name}}</a>
                                                                    @if($postImage->user_id==Auth::user()->id)
                                                                    <a href="{{url('package/highlight/image/credit/'.$postImage->id)}}"> [{{trans('common.edit')}}]</a>
                                                                    @endif
                                                                @else
                                                                    @if($postImage->user_id==Auth::user()->id)
                                                                    <a href="{{url('package/highlight/image/credit/'.$postImage->id)}}">+ เครดิตภาพ {{Auth::user()->id.$postImage->user_id}}</a>
                                                                        @endif
                                                                @endif
                                                            </div>

                                                        @else
                                                            <img src="{{ url('location/avatar/no-photo-available.png') }}">
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </div>
                                                @else

                                                    <div class="post-image-holder  single-image">
                                                        <img src="{{ url('location/avatar/no-photo-available.png') }}">
                                                    </div>
                                                @endif
                                            <div class="post-v-holder">
                                                @foreach($post->images()->get() as $postImage)
                                                    @if($postImage->type=='video')
                                                        <video width="100%" preload="none" height="auto" poster="{{ url('user/gallery/video/'.$postImage->title) }}.jpg" controls class="video-video-playe">
                                                            <source src="{{ url('user/gallery/video/'.$postImage->source) }}" type="video/mp4">
                                                            <!-- Captions are optional -->
                                                        </video>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        @if($post->youtube_video_id)
                                            <iframe  src="https://www.youtube.com/embed/{{ $post->youtube_video_id }}" frameborder="0" allowfullscreen></iframe>
                                        @endif
                                        @if($post->soundcloud_id)
                                            <div class="soundcloud-wrapper">
                                                <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $post->soundcloud_id }}&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                            </div>
                                        @endif
                                        <ul class="actions-count list-inline">
                                            @if($post->users_liked()->count() > 0)
                                                <?php
                                                $liked_ids = $post->users_liked->pluck('id')->toArray();
                                                $liked_names = $post->users_liked->pluck('name')->toArray();
                                                ?>
                                                <li>
                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.likes') }}"  data-users="{{ implode(',', $liked_ids) }}" data-original-title="{{ implode('<br />', $liked_names) }}"><span class="count-circle"><i class="fa fa-thumbs-up"></i></span> {{ $post->users_liked->count() }} {{ trans('common.likes') }}</a>
                                                </li>
                                            @endif

                                            @if($post->comments->count() > 0)
                                                <li>
                                                    <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>{{ $post->comments->count() }} {{ trans('common.comments') }}</a>
                                                </li>
                                            @endif

                                            @if($post->shares->count() > 0)
                                                <?php
                                                $shared_ids = $post->shares->pluck('id')->toArray();
                                                $shared_names = $post->shares->pluck('name')->toArray(); ?>
                                                <li>
                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.shares') }}"  data-users="{{ implode(',', $shared_ids) }}" data-original-title="{{ implode('<br />', $shared_names) }}"><span class="count-circle"><i class="fa fa-share"></i></span> {{ $post->shares->count() }} {{ trans('common.shares') }}</a>
                                                </li>
                                            @endif
                                        </ul>
                                        <div class="text-right">
                                            @if($timeline->type == 'location')
                                                @if($post->rate_group>0)
                                                    @if($post->date_visited_location!='0000-00-00')
                                                        <small style="color: #9cc2cb"><i class="fa fa-calendar"></i> {{trans('common.date_visit_location').' '.\Date::parse($post->date_visited_location.'01:01:10')->format('j F Y')}}</small>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <?php
                                    $check=DB::table('highlight_in_schedule')
                                        ->where('packageID',Session::get('package'))
                                        ->where('programID',Session::get('program_id'))
                                        ->where('LocationID',$location->id)
                                        ->first();
                                    ?>
                                    <div class="panel-footer socialite">
                                        <ul class="list-inline footer-list">
                                            <li>
                                                <input type="checkbox" class="add_highlight" name="add_highlight" id="add_highlight" value="{{$location->id}}" {{$check?'checked':''}}> {{trans('common.add_highlight')}}
                                                {{--<div class="material-switch pull-left">--}}
                                                {{--<input class="add_highlight" data-id="{{$location->timeline_id}}" id="add_highlight-{{$location->timeline_id}}" name="add_highlight-{{$location->timeline_id}}" type="checkbox" value="Y"  />--}}
                                                {{--<label for="status-{{$location->timeline_id}}" class="label-success"></label><span>&nbsp; {{trans('common.add_highlight')}}</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="pull-right" >--}}
                                                    {{--<a target="_blank" href="{{url('/'.$timeline->username)}}"><i class="fa fa-map-marker"></i> {{trans('common.update_location')}}</a>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
            </div>
             @endif

            <div class="col-md-12">
                @if(isset($locations))
                    <h3 class="alert alert-warning"><i class="fa fa-list"></i> All locations.</h3>
                    <div class="infinite-scroll">
                         <div class="card-columns">
                        @foreach($locations as $location)
                            <?php

                            $timeline=$location->timeline()->first();

                            $timeline_info=DB::table('timeline_info')
                            ->where('timeline_id',$location->timeline_id)
                            ->where('language_code',Session::get('language'))
                            ->first();
                            $post=\App\Post::where('represent_timeline_id',$location->timeline_id)->latest()->first();
                            if(!$post){
                               $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
                            }

                           // dd($timeline->avatar_id);
                            $media=\App\Media::where('id',$timeline->avatar_id)->first();
                            $city=null;$state=null;
                            $country=$timeline->country()->where('language_code',Session::get('language'))->first();
                            if(!$country){
                                $country=$timeline->country()->where('language_code','en')->first();
                            }
                            if($timeline->state_id){
                                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
                                if(!$state){
                                    $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code','en')->first();
                                }
                                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
                                if(!$city){
                                    $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code','en')->first();
                                }
                            }
                            if(!$country){
                                $country=$timeline->country()->where('language_code','en')->first();
                            }


                            ?>
                            @if($post)
                            <div class="card card-pin">
                                <div class="panel panel-default panel-post animated" id="post{{ $post->id }}">
                                    <div class="panel-heading no-bg">
                                        <div class="post-author">
                                            <?php

                                            $date=\Date::parse($post->created_at);


                                            ?>

                                            <div class="user-avatar">
                                                <a href="{{ url($timeline->username) }}" target="_blank">
                                                    @if($media!=null)
                                                        <img src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                    @else
                                                        <img src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                    @endif
                                                </a>

                                                {{--<a href="{{ url($post->user->username) }}"><img src="{{ $post->user->avatar }}" alt="{{ $post->user->name }}" title="{{ $post->user->name }}"></a>--}}
                                                <?php
                                                $star_rating=0;
                                                $RateAll2=DB::table('star_rating')
                                                    ->where('timeline_id',$timeline->id)
                                                    ->whereIn('rate_group',[2,3,4,5])
                                                    ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
                                                    ->first();
                                                if($RateAll2){
                                                    if($RateAll2->star>0){
                                                        $star_rating=round($RateAll2->star/$RateAll2->userCount,1);
                                                    }
                                                }
                                                ?>
                                                @if($star_rating>0)
                                                    @if($star_rating>=4)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-success" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @elseif($star_rating>=3)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-warning" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @elseif($star_rating<3)
                                                        <span title="{{trans('common.overall_accessible_rating')}}" class="text-danger" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="user-post-details">
                                                <ul class="list-unstyled no-margin">
                                                    <li>
                                                        @if(isset($sharedOwner))
                                                            <a target="_blank" href="{{ url($sharedOwner->user->username) }}" title="{{ '@'.$sharedOwner->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                {{ $sharedOwner->user->name }}
                                                            </a>
                                                            shared
                                                        @endif
                                                        <a target="_blank" href="{{ url($timeline->username) }}" title="{{ '@'.$post->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                            {{ isset($timeline_info->name)?$timeline_info->name:$timeline->name }}
                                                        </a>

                                                        @if($post->user->verified)
                                                            <span class="verified-badge bg-success">
                                                                <i class="fa fa-check"></i>
                                                            </span>
                                                        @endif

                                                        @if(isset($sharedOwner))
                                                            's post
                                                        @endif

                                                        @if($post->users_tagged->count() > 0)
                                                            {{ trans('common.with') }}
                                                            <?php $post_tags = $post->users_tagged->pluck('name')->toArray(); ?>
                                                            <?php $post_tags_ids = $post->users_tagged->pluck('id')->toArray(); ?>
                                                            @foreach($post->users_tagged as $key => $user)
                                                                @if($key==1)
                                                                    {{ trans('common.and') }}
                                                                    @if(count($post_tags)==1)
                                                                        <a href="{{ url($user->username) }}">{{ $user->name }}</a>
                                                                    @else
                                                                        <a href="#" data-toggle="tooltip" title="" data-placement="top" class="show-users-modal" data-html="true" data-heading="{{ trans('common.with_people') }}"  data-users="{{ implode(',', $post_tags_ids) }}" data-original-title="{{ implode('<br />', $post_tags) }}"> {{ count($post_tags).' '.trans('common.others') }}</a>
                                                                    @endif
                                                                    @break
                                                                @endif
                                                                @if($post_tags != null)
                                                                    <a href="{{ url('home/'.$user->username) }}" class="user"> {{ array_shift($post_tags) }} </a>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </li>

                                                    <li>
                <span>
                    <?php
                    $category=\App\Category::where('category_id',$location->category_id)->where('language_code',Session::get('language'))->first();
                    if(!$category){
                        $category=\App\Category::where('category_id',$location->category_id)->where('language_code','en')->first();
                    }

                    if($location->category_sub1_id){
                        $sub1category=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                    }

                    ?>

                    @if($location->category_sub1_id>0)

                            @if($sub1category)
                        <?php
                                $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                    ->where('language_code',Session::get('language'))
                                    ->first();
                                if(!$category_sub2){
                                    $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                        ->where('language_code','en')
                                        ->first();
                                }
                                ?>
                                {{$category_sub2?$category_sub2->category_sub2_name:''}},
                             @endif

                    @elseif($location->category_sub1_id>0)

                        <?php
                        $subname=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                        if($subname){
                            echo $subname->sub1_name.',';
                        }
                        ?>

                    @else

                        @if($location->category_id)

                            <?php

                            $category=\App\Category::where('category_id',$location->category_id)
                                ->where('language_code',Session::get('language'))->first();
                            if($category){
                                $category=\App\Category::where('category_id',$location->category_id)
                                    ->where('language_code','en')->first();
                            }
                            ?>
                            {{$category->name}},

                        @endif
                    @endif

                </span>

                                                        <span>
					  @if($city)
                                                                {{ $city->city }}
                                                            @endif
                                                            @if($state)
                                                                {{$state->state}}
                                                            @endif
                                                            @if($country)
                                                                {{$country->country}}
                                                            @endif
				  </span>


                                                        @if($post->location != NULL && !isset($sharedOwner))
                                                            {{ trans('common.at') }}
                                                            <span class="post-place">
                      <a target="_blank" href="{{ url('locations'.$post->location) }}">
                          <i class="fa fa-map-marker"></i> {{ $post->location }}
                      </a>
                      </span>
                                                    </li>
                                                    @endif
                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="panel-body">
                                        <div class="text-wrapper">
                                            <?php
                                            $links = preg_match_all("/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $post->description, $matches);
                                            $main_description = $post->description;
                                            ?>
                                            @foreach($matches[0] as $link)
                                                <?php
                                                $linkPreview = new LinkPreview($link);
                                                $parsed = $linkPreview->getParsed();
                                                foreach ($parsed as $parserName => $main_link) {
                                                    $data = '<div class="row link-preview">
                                                              <div class="col-md-3">
                                                                <a target="_blank" href="'.$link.'"><img src="'.$main_link->getImage().'"></a>
                                                              </div>
                                                              <div class="col-md-9">
                                                                <a target="_blank" href="'.$link.'">'.$main_link->getTitle().'</a><br>'.substr($main_link->getDescription(), 0, 500). '...'.'
                                                              </div>
                                                            </div>';
                                                }
                                                $main_description = str_replace($link, $data, $main_description);
                                                ?>
                                            @endforeach

                                            <p class="post-description">
                                                {{--************************ Cannot fig here ****************************--}}
                                                {!! $main_description !!}
                                                {{--{!! clean($main_description) !!}--}}
                                            </p>
                                            <div class="post-image-holder  @if(count($post->images()->get()) == 1) single-image @endif">

                                                @foreach($post->images()->get() as $postImage)
                                                    @if($postImage->type=='image')
                                                        @if(!file_exists(storage_path('user/gallery/mid/'.$postImage->source)))

                                                            <a href="{{ url('user/gallery/'.$postImage->source) }}" data-lightbox="imageGallery.{{ $post->id }}" >
                                                                <img src="{{ url('user/gallery/mid/'.$postImage->source) }}" class="img-responsive" title="{{ $post->user->name }}" alt="{{ $post->user->name }}">
                                                                {{--{{ $post->user->name }} |--}}
                                                                {{--<time class="post-time timeago" datetime="{{ $post->created_at }}+00:00" title="{{ $post->created_at }}+00:00">--}}
                                                                {{--{{ $post->created_at }}+00:00--}}
                                                                {{--</time>--}}
                                                            </a>
                                                            <div class="pull-right" >
                                                                @if($postImage->photo_credit_name)
                                                                    <a href="{{$postImage->photo_credit_url}}">เครดิตภาพ:{{$postImage->photo_credit_name}}</a>
                                                                    @if($postImage->user_id==Auth::user()->id)
                                                                        <a  href="{{url('package/highlight/image/credit/'.$postImage->id)}}"> [{{trans('common.edit')}}]</a>
                                                                    @endif

                                                                @else
                                                                    <a  href="{{url('package/highlight/image/credit/'.$postImage->id)}}">+ เครดิตภาพ</a>
                                                                @endif
                                                            </div>
                                                        @else
                                                            <img src="{{ url('location/avatar/no-image-full.jpg') }}">
                                                        @endif

                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="post-v-holder">
                                                @foreach($post->images()->get() as $postImage)
                                                    @if($postImage->type=='video')
                                                        <video width="100%" preload="none" height="auto" poster="{{ url('user/gallery/video/'.$postImage->title) }}.jpg" controls class="video-video-playe">
                                                            <source src="{{ url('user/gallery/video/'.$postImage->source) }}" type="video/mp4">
                                                            <!-- Captions are optional -->
                                                        </video>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        @if($post->youtube_video_id)
                                            <iframe  src="https://www.youtube.com/embed/{{ $post->youtube_video_id }}" frameborder="0" allowfullscreen></iframe>
                                        @endif
                                        @if($post->soundcloud_id)
                                            <div class="soundcloud-wrapper">
                                                <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $post->soundcloud_id }}&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                            </div>
                                        @endif
                                        <ul class="actions-count list-inline">
                                            @if($post->users_liked()->count() > 0)
                                                <?php
                                                $liked_ids = $post->users_liked->pluck('id')->toArray();
                                                $liked_names = $post->users_liked->pluck('name')->toArray();
                                                ?>
                                                <li>
                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.likes') }}"  data-users="{{ implode(',', $liked_ids) }}" data-original-title="{{ implode('<br />', $liked_names) }}"><span class="count-circle"><i class="fa fa-thumbs-up"></i></span> {{ $post->users_liked->count() }} {{ trans('common.likes') }}</a>
                                                </li>
                                            @endif

                                            @if($post->comments->count() > 0)
                                                <li>
                                                    <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>{{ $post->comments->count() }} {{ trans('common.comments') }}</a>
                                                </li>
                                            @endif

                                            @if($post->shares->count() > 0)
                                                <?php
                                                $shared_ids = $post->shares->pluck('id')->toArray();
                                                $shared_names = $post->shares->pluck('name')->toArray(); ?>
                                                <li>
                                                    <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.shares') }}"  data-users="{{ implode(',', $shared_ids) }}" data-original-title="{{ implode('<br />', $shared_names) }}"><span class="count-circle"><i class="fa fa-share"></i></span> {{ $post->shares->count() }} {{ trans('common.shares') }}</a>
                                                </li>
                                            @endif


                                        </ul>
                                        <div class="text-right">
                                            @if($timeline->type == 'location')
                                                @if($post->rate_group>0)
                                                    @if($post->date_visited_location!='0000-00-00')
                                                        <small style="color: #9cc2cb"><i class="fa fa-calendar"></i> {{trans('common.date_visit_location').' '.\Date::parse($post->date_visited_location.'01:01:10')->format('j F Y')}}</small>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <?php
                                    $check=DB::table('highlight_in_schedule')
                                        ->where('packageID',Session::get('package'))
                                        ->where('programID',Session::get('program_id'))
                                        ->where('LocationID',$location->id)
                                        ->first();
                                    ?>

                                    <div class="panel-footer socialite">
                                        <ul class="list-inline footer-list">
                                            <li>
                                                <input type="checkbox" class="add_highlight" name="add_highlight" id="add_highlight" value="{{$location->id}}" {{$check?'checked':''}}> {{trans('common.add_highlight')}}
                                                {{--<div class="material-switch pull-left">--}}
                                                {{--<input class="add_highlight" data-id="{{$location->timeline_id}}" id="add_highlight-{{$location->timeline_id}}" name="add_highlight-{{$location->timeline_id}}" type="checkbox" value="Y"  />--}}
                                                {{--<label for="status-{{$location->timeline_id}}" class="label-success"></label><span>&nbsp; {{trans('common.add_highlight')}}</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="pull-right" >--}}
                                                    {{--<a target="_blank" href="{{url('/'.$timeline->username)}}"><i class="fa fa-map-marker"></i> {{trans('common.update_location')}}</a>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif
                            {{--{!! Theme::partial('location-home',compact('post','location','timeline','media','city','state','country','category_options','country_options','next_page_url')) !!}--}}
                        @endforeach
                     {{$locations->render()}}
                </div>
                    </div>
                @endif

            </div>


    </div>
    @endsection
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>

<script type="text/javascript">

    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                jQuery("time.timeago").timeago();
            }
        });

    });
//    $('.popup').click(function(){
//        var width=$(window).width();
//        var height=$(window).height();
//        $.jsPanel({
//            iframe: {
//                width:width,
//                height:height,
//                src:    $(this).data('content')+'#post',
//                name:   'myFrame',
//                style:  {'border': '10px solid transparent'}
//            },
//            size:     'auto',
//            position: {left: 0, top: 0},
//            theme:    'black',
//        });
//    });
</script>
