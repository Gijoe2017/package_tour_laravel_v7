
@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-body">
<form role="form" id="AutoForm"  action="{{action('Package\PromotionController@update')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="promotion_id" value="{{$promotion->promotion_id}}">

    <div class="panel-content">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

    <div class="panel-header">
        {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload();"> &times; </button>--}}
        <h3 class="panel-title-site text-center"> {{trans('common.promotion_setting')}} </h3>
    </div>

        <div class="panel-body">
        <div class="row">
        <div class="col-md-12">
              <div class="panel panel-white">
                   <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-heading">
                                        <div class="col-lg-12">
                                            <div id="condition" class="form-group col-md-12">
                                                <label>{{trans('package.Condition_group')}} * </label><BR>
                                                <select class="form-control select2" name="operator_code" id="operator_code1" required>
                                                    <option value="">{{trans('profile.Choose')}}</option>
                                                    @foreach($Condition as $rows)
                                                        <option value="{{$rows->operator_code}}" {{$rows->operator_code==$promotion->promotion_operator?'selected':''}}>{{$rows->operator_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="mod" style="display: {{$promotion->promotion_operator=='Between'?'none':''}}">
                                                <div class="form-group col-md-6">
                                                    <label><strong>{{trans('common.booking')}} *</strong> </label><BR>
                                                    <input type="number" class="form-control" name="every_booking" id="every_booking" value="{{$promotion->every_booking}}" required  >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="between" style="display: {{$promotion->promotion_operator=='Mod'?'none':''}}">
                                            <div class="col-md-12">
                                            <div class="form-group col-md-6" >
                                                <label id='date_start'><strong>{{trans('common.date_start')}} *</strong> </label><BR>
                                                {{--<input type="text" class="form-control" name="promotion_date_start" id="datepicker"  data-date-format="yyyy-mm-dd" autocomplete="off"  required>--}}
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" name="promotion_date_start" class="form-control pull-right" value="{{substr($promotion->promotion_date_start,0,10)}}" data-date-format="yyyy-mm-dd"  autocomplete="off" id="datepicker">
                                                </div>
                                            </div>

                                            <div class="col-md-6" >
                                                <label id='date_start'><strong>{{trans('common.times')}} *</strong> </label><BR>

                                                <div class="input-group">
                                                    <input type="time" name="time_start" value="{{date('H:i:s',strtotime(substr($promotion->promotion_date_start,10)))}}" class="form-control">
                                                        <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label id="date_end" ><strong>{{trans('common.date_end')}} *</strong> </label><BR>
                                                {{--<input type="text" class="form-control" name="promotion_date_end" id="datepicker1"  data-date-format="yyyy-mm-dd" autocomplete="off" required >--}}
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" name="promotion_date_end" class="form-control pull-right" value="{{substr($promotion->promotion_date_end,0,10)}}" data-date-format="yyyy-mm-dd"  autocomplete="off" id="datepicker1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label id="date_end" ><strong>{{trans('common.time_end')}} *</strong> </label><BR>
                                                <div class="input-group">
                                                    <input type="time" name="time_end" value="{{date('H:i:s',strtotime(substr($promotion->promotion_date_end,10)))}}" class="form-control">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>




                                        <div class="clearfix"></div>
                                        <!-- /.modal-content -->
                                    <div class="col-md-12">
                                        <br>

                                        <div class="form-group col-md-6">
                                            <label><strong>{{trans('common.process')}} *</strong> </label><BR>
                                            <select class="form-control select2" style="width: 100%" name="promotion_operator2" id="promotion_operator2" required >
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                <option value="up" {{$promotion->promotion_operator2=='up'?'selected':''}} >{{trans('common.up')}}</option>
                                                <option value="down" {{$promotion->promotion_operator2=='down'?'selected':''}}>{{trans('common.down')}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label><strong>{{trans('common.amount')}} *</strong> </label><BR>
                                            <input type="number" class="form-control" name="value_promotion" id="value_promotion" value="{{$promotion->promotion_value}}" required  >
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label><strong>{{trans('common.unit')}} *</strong> </label><BR>
                                            <select class="form-control select2" style="width: 100%" name="unit_promotion" id="unit_promotion" required >
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                <option value="%" {{$promotion->promotion_unit=='%'?'selected':''}}>%</option>
                                                <option value="{{$Package->packageCurrency}}" {{$Package->packageCurrency==$promotion->promotion_unit?'selected':''}}>{{$Package->packageCurrency}}</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                    <div class="col-lg-12">
                                     <hr>
                                     {{--<a href="{{url('package/promotion/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>--}}
                                     {{--<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>--}}
                                    </div>
                            </div>
              </div>
        </div>
    </div>
            </div>
        <div class="modal-footer">
            <a  href="{{url('package/details/price/'.Session::get('packageDescID'))}}" class="btn btn-default  pull-left"  aria-hidden="true"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
            <button type="submit"  class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
        </div>

    </div>
</form>
</div></div></div></section>

        <script language="javascript">
            $('#operator_code1').on('change',function (e) {

                $('.between').hide();
                if(e.target.value=='Between') {
                    $('#condition').removeClass( "col-md-6" ).addClass('col-md-12');
                    $('.between').hide();
                    $('.between').show();
                    $('.mod').hide();
                    $("#promotion_date_end").prop('required', true);
                    $("#promotion_date_start").prop('required', true);
                    $("#every_booking").prop('required', false);
                }else{
                    $('#condition').removeClass( "col-md-12" ).addClass('col-md-6');
                    $('.between').hide();
                    $('.mod').show();
                    $("#promotion_date_end").prop('required', false);
                    $("#promotion_date_start").prop('required', false);
                    $("#every_booking").prop('required', true);
                }
            });
        </script>
 @endsection


