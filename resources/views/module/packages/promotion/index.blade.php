@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Package Detail. </span></h2>
        </div>
    </section>
@endsection

@section('content')
    {{--formhelpers--}}
    <link href="{{asset('public/assets/css/bootstrap-formhelpers.min.css')}}" rel='stylesheet' type='text/css' />
    <script src="{{asset('public/assets/js/bootstrap-formhelpers.min.js')}}"></script>

    <style type="text/css">
        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <form role="form"  action="{{action('Package\PromotionController@setPromotion')}}" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <span style="font-size: 2em; margin-left: 15px"><i class="fa fa-houzz"></i> โปรโมชั่นสำหรับโปรแกรมนี้</span>
                                        </div>
                                        <div class="col-md-4" align="right">
                                            <a href="{{url('package/detail/promotion/create')}}" class="btn btn-sm btn-info"><i class="fa fa-plus"></i>  Add New Promotion </a>
                                        </div>
                                    </div>
                                    <div class="content40"></div>
                                    @if(isset($promotion))
                                        <table class="table">
                                            <tbody> <?php $i=1;$group=''?>
                                            @foreach($promotion as $rows)
                                                <?php
                                                $CheckUse=DB::table('promotion_in_package_details')
                                                    ->where('promotion_id',$rows->promotion_id)
                                                    ->where('packageDescID',Session::get('packageDescID'))
                                                    ->count();
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" name="promotion_id[]" value="{{$rows->promotion_id }}" {{$CheckUse>0?'checked':''}}></td>
                                                    <td><div class="text-left"> {{$rows->promotion_title}}</div></td>
                                                    <td align="right">
                                                        @if($CheckUse==0)
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/promotion/edit/'.$rows->promotion_id)}}" class="btn btn-sm"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                                                            <a class="btn btn-sm btn-danger" href="{{url('package/promotion/delete/'.$rows->promotion_id)}}" class="btn btn-sm" onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                                                        @else
                                                            <label class="text-red">{{trans('common.condition_is_used')}}</label>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <h2>!ยังไม่มีการเขียนโปรโมชั่น</h2>
                                    @endif

                                    <hr>


                                    <div class="row">
                                        @if(Session::has('message'))
                                            <div class="alert alert-success">
                                                {{Session::get('message')}}
                                            </div>
                                        @endif

                                    </div>
                                    @if(isset($promotion))
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success btn-lg pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                            </div>
                                        </div>
                                     @endif
                                </form>
                            </div>

                            <br>
                            <div class="col-md-12">
                                <hr>
                                <a href="{{url('package/ajax/detail/edit/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.BackToListInfo')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop()


