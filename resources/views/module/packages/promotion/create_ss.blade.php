@extends('layouts.member.layout_master_new')
@section('pageHeader')
    <section class="content-header">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-center-xs">
            <h2><span><i class="fa fa-paint-brush"></i> Package Detail. </span></h2>
        </div>
    </section>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">

                    <form role="form"  action="{{action('Package\PromotionController@save')}}" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-heading">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>{{trans('package.Condition_group')}} * </label><BR>
                                                <select class="form-control select2" name="operator_code" id="operator_code1" required>
                                                    <option value="">{{trans('profile.Choose')}}</option>
                                                    @foreach($Condition as $rows)
                                                        <option value="{{$rows->operator_code}}">{{$rows->operator_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class=" not-deposit form-group col-md-3" >
                                            <label id='date_start'><strong>วันที่ *</strong> </label><BR>
                                            <input type="text" class="form-control" name="promotion_date_start" id="datepicker"  data-date-format="yyyy/mm/dd" autocomplete="off" >
                                        </div>
                                        <div class="between" style="display: none">
                                            <div class="form-group col-md-3">
                                                <label id="date_end" ><strong>วันที่ *</strong> </label><BR>
                                                <input type="text" class="form-control" name="promotion_date_end" id="datepicker1"  data-date-format="yyyy/mm/dd" autocomplete="off" >
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <!-- /.modal-content -->

                                    <div class="keep_value_deposit">
                                        <div class="form-group col-md-6">
                                            <label><strong>ส่วนลด *</strong> </label><BR>
                                            <input type="number" class="form-control" name="value_promotion" id="value_promotion"  >
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label><strong>หน่วย *</strong> </label><BR>
                                            <select class="form-control select2" style="width: 100%" name="unit_promotion" id="unit_promotion" >
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                <option value="%" >%</option>
                                                <option value="{{$Package->packageCurrency}}">{{$Package->packageCurrency}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="col-lg-12">
                                     <hr>
                                     <a href="{{url('package/promotion/list/'.Session::get('packageDescID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('package.Back')}}</a>
                                     <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> {{trans('package.Save')}}</button>
                                    </div>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>


@stop()
