@extends('layouts.member.layout_master_new')

@section('content')

    <div class="box box-danger">
        <div class="box-header with-border">
            <h2>Register Module</h2>
        </div>
    <div class="row">

            <div class="col-md-12">
                <div class="box-body">
                <div class="alert alert-success">{{Session::get('message')}}</div>
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Business</th>
                       <th>Module Type</th>
                       <th>Status</th>
                       <th>Modify</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($Module as $rows)
                   <tr>
                       <td>{{$rows->username}}</td>
                       <td>{{$rows->module_name}}</td>
                       <td>{{$rows->business_verified_status}}</td>
                       <td>
                           <a href="{{url('module/manage_edit/'.$rows->timeline_id)}}" class="btn btn-default"><i class="fa fa-edit"></i> {{trans('common.edit')}}</a>
                           <a href="{{url('module/documents/'.$rows->timeline_id)}}" class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Documents</a>
                           <a class="btn btn-info"><i class="fa fa-edit"></i> Update</a>
                           <a href="{{url('module/show_users/'.$rows->timeline_id)}}" class="btn btn-danger"><i class="fa fa-users"></i> Add User</a>
                           <a href="{{url('module/show_users/'.$rows->timeline_id)}}" class="btn btn-default"><i class="fa fa-trash"></i> {{trans('common.delete')}}</a>
                       </td>
                   </tr>
                       @endforeach
                   </tbody>
               </table>
                </div>
            </div>

    </div>


    </div>
@endsection