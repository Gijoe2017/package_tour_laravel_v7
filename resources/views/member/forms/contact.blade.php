<h3>
    <span class="icon"><i class="ti-email"></i></span>
    <span class="title_text">Contact</span>
</h3>
<input type="hidden" id="h_country_id" name="h_country_id">
<input type="hidden" id="h_state_id" name="h_state_id">
<input type="hidden" id="h_city_id" name="h_city_id">
<input type="hidden" id="h_city_sub1_id" name="h_city_sub1_id">
<input type="hidden" id="h_address_type" name="h_address_type">
<fieldset>
    <legend>
        <span class="step-number">Step 3 / 5</span>
    </legend>
    <div class="row">
        <div class="col-md-4">
            <div class="form-select">
            <div class="select-list">
                <label for="address_type_id" class="form-label required">{{trans('profile.AddressType')}}</label>
                <select name="address_type_id" id="address_type_id">
                    <option value="">-- {{trans('profile.Choose')}} --</option>
                    @foreach(App\models\AddressType::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->address_type_id}}">{{$rows->address_type}}</option>
                    @endforeach
                </select>
            </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-select">
                <label for="address" class="form-label required">{{trans('profile.Address')}}</label>
                <input type="text" name="address" id="address" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="additional" class="form-label">{{trans('profile.Additional')}}</label>
                <input type="text" name="additional" id="additional" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-select">
                <div class="select-list">
                <label for="country_id" class="form-label required">{{trans('profile.Country')}}</label>
                    <select name="country_id" id="country_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        @foreach($country as $rows)
                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4 box-state">
            <div class="form-select">
                <div class="select-list">
                <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                    <select name="state_id" id="state_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                            {{--<promotion value="{{$rows->state_id}}">{{$rows->state}}</promotion>--}}
                        {{--@endforeach--}}
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4 box-state city_id">
            <div class="form-group">
                <div class="select-list">
                <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                    <select name="city_id" id="city_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                            {{--<promotion value="{{$rows->city_id}}">{{$rows->city}}</promotion>--}}
                        {{--@endforeach--}}
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 box-state city_sub1_id">
            <div class="form-group">
                <div class="select-list">
                <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                    <select name="city_sub1_id" id="city_sub1_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                            {{--<promotion value="{{$rows->city_sub1_id}}">{{$rows->city_sub1_name}}</promotion>--}}
                        {{--@endforeach--}}
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4 city_sub1_id">
            <div class="form-group">
                <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                <input type="text" name="zip_code" id="zip_code" />
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="email" class="form-label required">{{trans('profile.Email')}}</label>
                <input type="email" name="email[]" id="email" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="email" class="form-label">{{trans('profile.Email')}} 2</label>
                <input type="email" name="email[]" id="email2" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="email" class="form-label">{{trans('profile.Email')}} 3</label>
                <input type="email" name="email[]" id="email2" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="phone" class="form-label required">{{trans('profile.Phone')}}</label>
                <input type="number" name="phone[]" id="phone" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="phone" class="form-label">{{trans('profile.Phone')}} 2</label>
                <input type="number" name="phone[]" id="phone2" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="phone" class="form-label">{{trans('profile.Phone')}} 3</label>
                <input type="number" name="phone[]" id="phone2" />
            </div>
        </div>
    </div>


</fieldset>