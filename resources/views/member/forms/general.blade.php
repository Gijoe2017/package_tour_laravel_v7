<h3>
    <span class="icon"><i class="ti-user"></i></span>
    <span class="title_text">General</span>
</h3>
<fieldset>
    <legend>
        <span class="step-heading">Personal Informaltion: </span>
        <span class="step-number">Step 1 / 5</span>
    </legend>

    <input type="hidden" id="h_user_title_id" name="h_user_title_id">
    <input type="hidden" id="h_sex_id" name="h_sex_id">
    <input type="hidden" id="h_country_of_birth_id" name="h_country_of_birth_id">
    <input type="hidden" id="h_marital_status_id" name="h_marital_status_id">
    <input type="hidden" id="h_origin_id" name="h_origin_id">
    <input type="hidden" id="h_religion_id" name="h_religion_id">
    <input type="hidden" id="h_nationality_no" name="h_nationality_no">

     <div class="row">
        <div class="col-md-9">
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="identification_id" class="form-label required">Identification ID</label>
                    <input type="text" class="bfh-phone" name="identification_id" id="identification_id" data-format="d-dddd-ddddd-dd-d"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="select-list">
                        <label for="user_title_id" class="form-label required ">Title</label>
                        <select name="user_title_id" id="user_title_id">
                            <option value="">{{trans('profile.Choose')}}</option>
                            @foreach(\App\models\Title::where('language_code',Auth::user()->language)->get() as $rows)
                                <option value="{{$rows->user_title_id}}">{{$rows->user_title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label for="first_name" class="form-label required">First name</label>
                    <input type="text" name="first_name" id="first_name" />
                </div>
                </div>
                <div class="col-md-6">

                <div class="form-group">
                    <label for="last_name" class="form-label required">Last name</label>
                    <input type="text" name="last_name" id="last_name" />
                </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="middle_name" class="form-label">Middle name</label>
                    <input type="text" name="middle_name" id="middle_name" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="select-list">
                    <label for="sex_id" class="form-label required">Gender</label>
                        <select name="sex_id" id="sex_id">
                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                            @foreach(\App\models\Sex::where('language_code',Auth::user()->language)->get() as $rows)
                                <option value="{{$rows->sex_id}}" >{{$rows->sex_status}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-3">
            <label for="passport_cover_image" class="form-label ">{{trans('profile.picture')}}</label>
            <div class="form-group" align="center">
                <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
            </div>
            <div class="form-group"><center>
                    <input type="file"  id="imgInp" name="passport_cover_image" class="filestyle" data-input="false" ></center>
            </div>
        </div>

     </div>

    <hr>
        <div class="row">

                <div class="col-md-5">
                <div class="form-date">
                    <label for="birth_date" class="form-label">Date of birth</label>
                    <div class="form-date-group">
                        <div class="form-date-item">
                            <select id="birth_date" name="birth_date"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="birth_month" name="birth_month"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="birth_year" name="birth_year"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-4">
                <div class="form-group">
                <div class="select-list">
                <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}}</label>
                    <select name="country_of_birth_id" id="country_of_birth_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        @foreach($country as $rows)
                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                        @endforeach
                    </select>
                </div>
                </div>
            </div>
                <div class="col-md-3">
                <div class="form-group">
                <div class="form-select">
                    <label for="marital_status_id" class="form-label required">{{trans('profile.status')}}</label>
                    <div class="select-list">
                        <select name="marital_status_id" id="marital_status_id">
                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                            @foreach(\App\models\MaritalStatus::where('language_code',Auth::user()->language)->get() as $rows)
                                <option value="{{$rows->marital_status_id}}" {{$rows->marital_status=='1'?'selected':''}}>{{$rows->marital_status}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                </div>
            </div>

        </div>
    <hr>
        <div class="row">

        <div class="col-md-4">
            <label for="origin_id" class="form-label required">{{trans('profile.origin')}}</label>
            <div class="select-list">
                <select name="origin_id" id="origin_id">
                    <option value="">-- {{trans('profile.Choose')}} *--</option>
                    @foreach(\App\models\Origin::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->origin_id}}" >{{$rows->origin_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <label for="religion_id" class="form-label required">{{trans('profile.religion')}}</label>
            <div class="select-list">
                <select name="religion_id" id="religion_id">
                    <option value="">-- {{trans('profile.Choose')}} *--</option>
                    @foreach(\App\models\Religion::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->religion_id}}" >{{$rows->religion}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <label for="nationality_no" class="form-label required">{{trans('profile.nationality')}}</label>
            <div class="select-list">
                <select name="nationality_no" id="nationality_no">
                    <option value="">-- {{trans('profile.Choose')}} *--</option>
                    @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->nationality_no}}" >{{$rows->nationality}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>






    {{--<div class="form-row">--}}
        {{--<div class="form-date">--}}
            {{--<label for="birth_date" class="form-label">Date of birth</label>--}}
            {{--<div class="form-date-group">--}}
                {{--<div class="form-date-item">--}}
                    {{--<select id="birth_date" name="birth_date"></select>--}}
                    {{--<span class="select-icon"><i class="ti-angle-down"></i></span>--}}
                {{--</div>--}}
                {{--<div class="form-date-item">--}}
                    {{--<select id="birth_month" name="birth_month"></select>--}}
                    {{--<span class="select-icon"><i class="ti-angle-down"></i></span>--}}
                {{--</div>--}}
                {{--<div class="form-date-item">--}}
                    {{--<select id="birth_year" name="birth_year"></select>--}}
                    {{--<span class="select-icon"><i class="ti-angle-down"></i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-select">--}}
            {{--<label for="gender" class="form-label">Gender</label>--}}
            {{--<div class="select-list">--}}
                {{--<select name="gender" id="gender">--}}
                    {{--<promotion value="">Male</promotion>--}}
                    {{--<promotion value="Male">Male</promotion>--}}
                    {{--<promotion value="Female">Female</promotion>--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<label for="user_name" class="form-label required">User name</label>--}}
        {{--<input type="text" name="user_name" id="user_name" />--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<label for="password" class="form-label required">Password</label>--}}
        {{--<input type="password" name="password" id="password" />--}}
    {{--</div>--}}
</fieldset>