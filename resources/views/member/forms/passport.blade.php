<h3>
    <span class="icon"><i class="ti-eye"></i></span>
    <span class="title_text">Passport</span>
</h3>
<fieldset>
    <legend>
        <span class="step-heading">Passport Informaltion: </span>
        <span class="step-number">Step 2 / 5</span>
    </legend>
    <div class="row">
        <div class="col-md-9">

                <div class="form-group">
                    <label for="passport_number" class="form-label required">{{trans('profile.passport_number')}}</label>
                    <input type="text" class="bfh-phone" name="passport_number" id="passport_number" data-format="d-dddd-ddddd-dd-d" />
                </div>

                <div class="form-group">
                    <label for="passport_country_id" class="form-label required">{{trans('profile.passport_coutry')}}</label>
                    <div class="select-list">
                        <select name="passport_country_id" id="passport_country_id">
                            <option value="">-- {{trans('profile.Choose')}} --</option>
                            @foreach($country as $rows)
                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
           <hr>

            <div class="col-md-6">
                <div class="form-date">
                    <label for="passport_DOI" class="form-label">Passport DOI </label>
                    <div class="form-date-group">
                        <div class="form-date-item">
                            <select id="passport_DOI_date" name="passport_DOI_date"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="passport_DOI_month" name="passport_DOI_month"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="passport_DOI_year" name="passport_DOI_year"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-date">
                    <label for="passport_DOE_date" class="form-label">Passport DOE </label>
                    <div class="form-date-group">
                        <div class="form-date-item">
                            <select id="passport_DOE_date" name="birth_date"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="passport_DOE_month" name="birth_month"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                        <div class="form-date-item">
                            <select id="passport_DOE_year" name="birth_year"></select>
                            <span class="select-icon"><i class="ti-angle-down"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <label for="photo_for_visa" class="form-label ">{{trans('profile.picture')}}</label>
            <div class="form-group" align="center">
                <img id="blah1" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
            </div>
            <div class="form-group"><center>
                    <input type="file"  id="imgInp1" name="photo_for_visa" class="filestyle" data-input="false" ></center>
            </div>
        </div>
    </div>






    {{--<div class="form-group">--}}
        {{--<label for="department" class="form-label required">Department</label>--}}
        {{--<input type="text" name="department" id="department" />--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<label for="work_hours" class="form-label required">Working hours</label>--}}
        {{--<input type="text" name="work_hours" id="work_hours" />--}}
    {{--</div>--}}
</fieldset>