@extends('layouts.member.layout_master_new')

@section('content-chart')
    <div class="box">
        <div class="box-header">
            <div class="row">
            <div class="col-md-6">
                <h2 class="box-title"><i class="fa fa-users"></i> Member List. </h2>
            </div>
            <div class="col-md-6" align="right">
                <a href="{{url('/member/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a>
            </div>
            </div>
        </div>

       <div class="box-body">
           <table id="example1" class="table table-bordered table-hover">
               <thead>
               <tr>
                   <th>Identification</th>
                   <th>Full name</th>
                   <th>Date of birth</th>
                   <th>Religion</th>
                   <th>Nationality</th>
                   <th></th>
               </tr>
               </thead>
               <tbody>
               @foreach($Members as $rows)
               <tr>
                   <td>{{$rows->identification_id}}</td>
                   <td>{{$rows->timeline_id}} {{\App\models\Title::where('user_title_id',$rows->user_title_id)->active()->first()->user_title}}
                       {{$rows->first_name}} {{$rows->last_name }} {{$rows->middle_name?'('.$rows->middle_name.')':''}}</td>
                   <td>{{$rows->date_of_birth}}</td>
                   <td>{{\App\models\Religion::where('religion_id',$rows->religion_id)->active()->first()->religion}}</td>
                   <td>{{\App\models\Nationality::where('nationality_id',$rows->current_nationality_id)->active()->first()->nationality}}</td>
                   <td align="right">
                       <a href="{{url('member/change-language/'.$rows->passport_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}} </a>
                       <a href="{{url('member/edit/'.$rows->passport_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}} </a>
                       <a href="{{url('member/delete/'.$rows->passport_id)}}" class="btn btn-danger btn-sm" onclick="return confirm_delete()" href="{{url('member/delete/'.$rows->passport_id)}}">
                           <i class="fa fa-trash"></i> {{trans('profile.Delete')}}
                       </a>
                   </td>
               </tr>
                @endforeach

               </tbody>
               <tfoot>
               <tr>
                   <th>Identification</th>
                   <th>Full name</th>
                   <th>Date of birth</th>
                   <th>Religion</th>
                   <th>Nationality</th>
               </tr>
               </tfoot>
           </table>
       </div>
   </div>




    @endsection