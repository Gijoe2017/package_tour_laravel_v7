@extends('layouts.member.layout_master_new')
@section('content')

    <link rel="stylesheet" href="{{asset('themes/default/assets/select1/jquery.typeahead.css')}}">
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('themes/default/assets/select1/jquery.typeahead.js')}}"></script>


    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">Education <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-heading">Education Informaltion: </span>
                        <span class="step-number">Step 5 / 6</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Member\EducationController@store')}}">
                        {{csrf_field()}}

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="education_type_id" class="form-label ">{{trans('profile.education_type')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <?php
                                            $eduType=\App\models\EducationType::where('language_code',Auth::user()->language)->get();
                                                if(!$eduType){
                                                    $eduType=\App\models\EducationType::where('language_code','en')->get();
                                                }
                                            ?>
                                            <select class="form-control select2" name="education_type_id" id="education_type_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach($eduType as $rows)
                                                    <option value="{{$rows->education_type_id}}" >{{$rows->education_type_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date Graduate</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="date_graduate" id="datepicker" autocomplete="off" >
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="program" class="form-label required">{{trans('profile.education_program')}} </label>
                                        <input class="form-control" type="text" name="education_program" id="education_program"/>
                                    </div>
                                </div>
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.education_program_en')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="program_en" id="program_en" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="university_id" class="form-label required">{{trans('profile.university')}} <span class="text-red">*</span></label>--}}
                                        {{--<select class="form-control select2" name="university_id" id="university_id" required>--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach(\App\models\University::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->university_id}}" >{{$rows->university_name}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}



                                <div class="search-location">
                                    <div class="col-md-12">
                                        <label for="program" class="form-label">{{trans('profile.university')}} </label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input class="js-typeahead form-control"
                                                           name="q"
                                                           type="search"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div style="display: none" class="new-location">

                                    <div class="col-md-12">
                                        <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                            {{ Form::label('name', trans('profile.university'), ['class' => 'control-label']) }}
                                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    {{ $errors->first('name') }}
                                                    </span>
                                            @endif
                                        </fieldset>
                                    </div>
                                    {{--<div class="col-md-6">--}}
                                        {{--<fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">--}}
                                            {{--{{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}--}}
                                            {{--{{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control','required' => 'required')) }}--}}
                                            {{--@if ($errors->has('category_id'))--}}
                                                {{--<span class="help-block">--}}
										                {{--{{ $errors->first('category_id') }}--}}
									                {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</fieldset>--}}

                                    {{--</div>--}}
                                </div>

                                <div style="display: none" class="row new-location">
                                    <div class="col-md-12">
                                        <br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                            <input class="form-control" type="text" name="address" id="address" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>

                                            <select style="width: 100%" class="form-control select2" name="country_id" id="country_id" >
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state">
                                        <div class="form-group">
                                            <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="state_id" id="state_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_id">
                                        <div class="form-group">
                                            <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_id" id="city_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_sub1_id">
                                        <div class="form-group">
                                            <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 city_sub1_id">
                                        <div class="form-group">
                                            <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                            <input class="form-control" readonly type="text" name="zip_code" id="zip_code"  />
                                        </div>
                                    </div>
                                </div>

                                </div>

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>--}}

                                        {{--<select class="form-control select2" name="country_id" id="country_id" required>--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->country_id}}" >{{$rows->country}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>
                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" align="right">
                                        <a href="{{url('package/member/step4')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </section>

    <script>
        function SP_source() {
            return "{{url('/')}}/";
        }
    </script>
    <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>
@endsection