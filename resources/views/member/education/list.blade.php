@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.education_list')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/member/education/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">{{trans('profile.step')}} 5 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    @if($Education->count()>0)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.education_type')}}</th>
                            <th>{{trans('profile.date_graduate')}}</th>
                            <th>{{trans('profile.education_program')}}</th>
                            <th>{{trans('profile.graduate_from')}}</th>
                            <th>{{trans('profile.Country')}}</th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Education as $education)
                            <?php
                            $state=null;$city=null;
                            $country=App\Country::where('country_id',$education->country_id)->first();

                            $state=$country->state()->where('state_id',$education->state_id)->where('language_code',Auth::user()->langauge)->first();
                            //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$education->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }
                            ?>
                            <tr>
                                <td>{{$education->education_type_name}}</td>
                                <td>{{$education->date_graduate}}</td>
                                <td>{{$education->education_program}}</td>
                                <td>{{$education->name}}</td>
                                <td>{{$country->country}}</td>
                                <td>
                                    <a href="{{url('/member/education/change-language/'.$education->education_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>
                                    <a href="{{url('/member/education/edit/'.$education->education_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/member/education/delete/'.$education->education_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="text-center"><h4>{{trans('common.have_no_data')}}</h4></div>
                    @endif
                        <div class="box-footer">
                           <hr>
                        </div>
                        <div class="form-group" align="right">
                            <a href="{{url('member/step4')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                            <a type="button" href="{{url('/member/documents')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>
                        </div>

                </div>

                </div>

                </div>
    </section>
@endsection
