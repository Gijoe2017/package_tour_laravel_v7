@extends('theme.AdminLTE.layout_master')

@section('content-chart')
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title">Customer Form</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="{{action('CustomerController@store')}}">
                {!! csrf_field() !!}
                    <div class="box-body">
                        @if (isset($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Code * </label>
                                <div class="col-sm-10"><span id="errorCode" class="text-danger"></span>
                                    <input type="text" class="form-control" id="Customer_Code" name="Customer_Code" placeholder="Customer Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label"> Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="Customer_Name" name="Customer_Name" placeholder="Customer Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Customer_Address" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">ZipCode</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Customer_Zipcode" placeholder="ZipCode">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="Customer_Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                </div><!-- /.input group -->
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <img src="{{asset('public/images/default-user.png')}}" id="blah" class="img-thumbnail">
                            </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10"><span id="errorEmail" class="text-danger"></span>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email *">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Permission</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="userType">
                                    <option value="U">Customer</option>
                                    <option value="A">Administrator</option>
                                    <option value="S">Supervizer</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label class="radio-inline">
                                        <input type="radio" class="flat-red" name="Customer_Status" checked value="On"> On
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="flat-red" name="Customer_Status" value="Off"> Off
                                    </label>
                                </div>
                            </div>
                        </div>
                        </div>


                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{url('customer/list')}}" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                        <button type="reset" class="btn btn-success">Clear</button>
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
            <!-- general form elements disabled -->

        </div>
    </div>
    <script src="{{asset('public/assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <script language="javascript">
        $('#Customer_Code').on('blur',function (e) {
           $.ajax({
               url:'{{URL::to('ajax/customer/code')}}',
               type:'get',
               data:{'code':e.target.value},
               success:function (data) {
                   if(data.length>2){
                       $('#errorCode').html(data);
                       $('#Customer_Code').focus();
                   }else{
                       $('#errorCode').html('');
                   }

               }
           });
        });

        $('#emails').on('blur',function (e) {
            $.ajax({
                url:'{{URL::to('emails')}}',
                type:'get',
                data:{'email':e.target.value},
                success:function (data) {
                    if(data.length>2){
                        $('#errorEmail').html(data);
                        $('#email').focusemails                   }else{
                        $('#errorEmail').html('');
                    }

                }
            });
        });

    </script>

@endsection

