@extends('theme.AdminLTE.layout_master')

@section('content-chart')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h2 class="box-title"><i class="fa fa-users"></i> Customer List </h2>
                    <div class="box-tools pull-right">
                        <a class="btn btn-box-tool" href="{{url('customer/create')}}" ><i class="fa fa-plus"></i> Add New Customer </a>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        {{--<div class="btn-group">--}}
                            {{--<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>--}}
                            {{--<ul class="dropdown-menu" role="menu">--}}
                                {{--<li><a href="#">Action</a></li>--}}
                                {{--<li><a href="#">Another action</a></li>--}}
                                {{--<li><a href="#">Something else here</a></li>--}}
                                {{--<li class="divider"></li>--}}
                                {{--<li><a href="#">Separated link</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <table id="Table_customer" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Customer Code</th>
                                    <th>Customer Name</th>
                                    <th>Customer Address</th>
                                    <th>Customer Email</th>
                                    <th>Permission</th>
                                    <th><center>Action</center></th>
                                </tr>
                                </thead>
                               <tbody>
                                    @foreach($Customers as $rows)
                                       <tr>
                                           <td>{{$rows->Customer_Code}}</td>
                                           <td>{{$rows->Customer_Name}}</td>
                                           <td>{{$rows->Customer_Address}}</td>
                                           <td>{{$rows->Customer_Email}}</td>
                                           <td>{{$rows->userType}}</td>
                                           <td><center><a href="{{url('customer/edit/'.$rows->ID)}}"><i class="fa fa-edit"></i> Edit</a> <small> | </small>
                                           <a href="{{url('customer/delete/'.$rows->ID)}}" onclick="return confirmDel()"><i class="fa fa-trash"></i> Delete</a></center></td>
                                       </tr>
                                    @endforeach
                               </tbody>
                                <tfoot>
                                <tr>
                                    <th>Customer Code</th>
                                    <th>Customer Name</th>
                                    <th>Customer Address</th>
                                    <th>Customer Email</th>
                                    <th><center>Action</center></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div><!-- /.row -->
                </div><!-- ./box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>



@endsection