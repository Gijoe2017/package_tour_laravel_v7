@extends('layouts.package.master')

@section('program-highlight')

    <section class="invoice">
        <div class="container">
            <div class="row invoice-warning">
                <!-- Profile Image -->
                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-responsive img-circle"  src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->user_name }}">
                            </div>
                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
                            <p class="text-muted text-center"><i class="fa fa-edit"></i> {{trans('common.edit').trans('common.general')}}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <form method="post" id="form1" class="form-control form-booking" enctype="multipart/form-data" action="{{action('Package\OrderMemberController@booking_more_address_store')}}">
                                {!! csrf_field() !!}
                            <link rel="stylesheet" href="{{asset('member/assets/select2/dist/css/select2.min.css')}}">
                            <script src="{{asset('member/assets/select2/dist/js/select2.full.min.js')}}"></script>

                                <div class="form-group">
                                    <h4><i class="fa fa-info"></i> {{trans('common.information_for_invoice')}}</h4>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('profile.FullName')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="entry_firstname" name="entry_firstname"  required  placeholder="{{trans('profile.FirstName')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="entry_lastname" name="entry_lastname" required  placeholder="{{trans('profile.LastName')}}">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.emails')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="tax_email" id="tax_email"  placeholder="{{trans('common.emails')}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.phone')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="tax_phone" id="tax_phone" placeholder="{{trans('common.phone')}}" required>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.address')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="{{trans('common.address')}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.country')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <?php
                                        $Country=\App\Country::where('language_code',Auth::user()->language)->get();
                                        if($Country->count()==0){
                                            $Country=\App\Country::where('language_code','en')->get();
                                        }
                                        ?>
                                        <select name="country_id" id="country_id" class="form-control select2" required>
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($Country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group state_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.state')}}</strong></label>
                                    <div class="col-sm-8">
                                        <select name="state_id" id="state_id" class="form-control select2">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group city_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city')}}</strong></label>
                                    <div class="col-sm-8">
                                        <select name="city_id" id="city_id" class="form-control select2">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group city_sub1_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city_sub')}}</strong></label>
                                    <div class="col-sm-8">
                                        <select name="city_sub1_id" id="city_sub1_id" class="form-control select2">

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group city_sub1_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.zipcode')}}</strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="{{trans('common.zipcode')}}">
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <hr>
                                    <div class="pull-left">
                                    <a href="{{url('booking/show/address')}}" class="btn btn-warning btn-lg"><i class="fa fa-arrow-left"></i> {{trans('common.back_to_list')}}</a>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-arrow-right"></i> {{trans('common.continuous')}}</button>
                                </div>
                            </form>

                            <script src="{{asset('member/assets/bootstrap/js/app.js')}}"></script>
                            <script>
                                $(function () {
                                    //Initialize Select2 Elements
                                    $('.select2').select2()
                                });

                                $( document ).ready(function() {
                                   $('.state_id').hide();
                                    $('.city_id').hide();
                                    $('.city_sub1_id').hide();
                                });
                            </script>

                        </div>
                    </div>
                </div>

                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection