<div class="box box-info">

    <div class="text-center">
        <img class="profile-user-img img-responsive img-circle"  src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->user_name }}">
    </div>

    <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>

    <p class="text-muted text-center"><i class="fa fa-edit"></i> {{trans('common.edit').trans('common.general')}}</p>

    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fa fa-cogs"></i> {{trans('profile.general')}}</a>
        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fa  fa-list-ul"></i> {{trans('common.history_buy')}}</a>
        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fa fa-history"></i> {{trans('common.history_return')}}</a>
        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fa  fa-bell"></i> {{trans('common.warning')}}</a>
    </div>


    <!-- /.box-body -->
</div>