
@extends('layouts.package.master-order')

@section('program-highlight')
<style>
    .img-circle {
        border-radius: 50%;
    }

    .nav-pills .nav-link.active, .nav-pills .show > .nav-link{
        color: #fff;
        background-color: #e86d38;
    }
</style>
    <section class="section-content bg padding-y border-top">
        <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class="fa fa-user"></i> {{trans('common.my_account')}}</h1>
        </section>
            <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">
                    <!-- Profile Menu -->
                   @include('member.account.profile-menu')
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">

                    <div class="box box-info">
                    <h4><i class="fa fa-edit"></i> {{trans('common.edit').trans('common.general')}}</h4>
                            <hr>
                            <div class="form-group">
                                <div class="col-sm-10">
                                <img  id="blah" src="{{Auth::user()->avatar}}" style="max-width: 300px">
                                    <span class="pull-right bt-pic"> <button type="button" class="picture btn btn-default"><i class="fa fa-edit"></i> {{trans('common.update_picture')}} </button></span>
                                </div>
                                <BR>
                                <div class="col-sm-10 up-pic" style="display: none">
                                    <form method="post" id="form1" enctype="multipart/form-data" action="{{action('Member\MemberController@change_picture')}}">
                                        {!! csrf_field() !!}
                                        <input type="file" name="file" id="imgInp"  required>
                                        <span class="pull-right"> <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> {{trans('common.update_picture')}}</button></span>
                                    </form>
                                </div>
                                <hr>
                            </div>
                        <form method="post" id="form1"  action="{{action('Member\MemberController@change_name')}}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('profile.FirstName')}}<span class="text-danger">*</span></strong></label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{Auth::user()->first_name}}" required  placeholder="{{trans('profile.FirstName')}}">

                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('profile.LastName')}}<span class="text-danger">*</span></strong></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{Auth::user()->last_name}}" required  placeholder="{{trans('profile.LastName')}}">
                                </div>
                            </div>
                            <div class="col-sm-10">
                                @if(Session::has('message_name'))
                                    <span class="text-success"> {{Session::get('message_name')}}</span>
                                    @endif
                                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                            </div>
                        </form>
                           <BR>
                        <hr>

                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.emails')}}<span class="text-danger">*</span></strong></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="tax_email" id="tax_email" value="{{Auth::user()->email}}" placeholder="{{trans('common.emails')}}" readonly>
                                </div>
                            </div>

                            <BR>
                        <hr>
                            <div class="form-group hide-pw">
                                <div class="col-sm-10">
                                    @if(Session::has('message'))
                                    <span class="text-green">{{Session::get('message')}}</span><BR>
                                    @endif
                                <i class="fa fa-key"></i> {{trans('profile.Password')}} <strong>********</strong>  <span class="pull-right"> <button type="button" class="pw btn btn-default"><i class="fa fa-edit"></i> {{trans('common.update_password')}} </button></span>
                                </div>
                            </div>
                            <div class="password" style="display: none; border: solid 1px saddlebrown">
                                <form method="post" id="form1"  action="{{action('Member\MemberController@change_password')}}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('profile.Password')}} <span class="text-danger">*</span></strong></label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control chk-pw" name="password" id="password" placeholder="{{trans('profile.Password')}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.confirm_password')}} <span class="text-danger">*</span></strong></label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control chk-pw" name="confirm_password" id="confirm_password" placeholder="{{trans('profile.ConfirmPassword')}}" required>
                                        </div>
                                        <BR>
                                        <span class="pw-error text-danger col-sm-8 text-center"></span>
                                    </div>

                                    <div class="form-group text-center">
                                       <button type="submit" class="btn btn-info btn-lg submit_pw"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                                    </div>
                                </form>
                            </div>
                    </div>
                    </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
        </div>
    </section>

    <script>

        $('body').on('click','.picture',function () {
            $('.up-pic').show();
            $('.bt-pic').hide();
        });

        $('body').on('click','.pw',function () {
            $('.password').show();
            $('.hide-pw').hide();
        });

        $('body').on('blur','.chk-pw',function () {
            if($('#password').val() != '' && $('#confirm_password').val() != '' ){

                if ($('#password').val() != $('#confirm_password').val()) {
                    $('.pw-error').html('Password does not match!');
                    $('#confirm_password').select();
                    $('.submit_pw').prop('disabled', true);
                } else {
                    $('.pw-error').html('');
                    $('.submit_pw').prop('disabled', false);
                }
            }
        });

        $('body').on('keyup','#confirm_password',function () {
            $('.submit_pw').prop('disabled', false);
        });

        $(".submit_pw").hover(function(){

            if($('#password').val() != '' && $('#confirm_password').val() != '' ){
                if($('#password').val() != $('#confirm_password').val()){
                    $('.pw-error').html('Password does not match!');
                    $(this).prop('disabled', true);
                    $('#confirm_password').select();
                }else{
                    $('.pw-error').html('');
                    $(this).prop('disabled', false);
                }
            }

        });





    </script>
@endsection