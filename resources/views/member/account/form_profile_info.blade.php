@extends('layouts.package.master-order')

@section('program-highlight')
    <style>
        .img-circle {
            border-radius: 50%;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link{
            color: #fff;
            background-color: #e86d38;
        }
    </style>
    <section class="section-content bg padding-y border-top">
        <div class="container">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1><i class="fa fa-user"></i> {{trans('common.my_account')}}</h1>
            </section>
            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Menu -->
                    @include('member.account.profile-menu')
                    <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">

                       ddd

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
    </section>

@endsection