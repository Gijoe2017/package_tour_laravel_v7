@extends('layouts.package.master')

@section('program-highlight')
    <style>



        .example {
            margin:5px 20px 0 0;
        }
        .example input {
            display: none;
        }
        .example label {
            width: 80%;
            margin-right: 20px;
            display: inline-block;
            cursor: pointer;
        }

        .ex1 span {
            display: block;
            padding: 8px 10px 8px 30px;
            border: 1px solid #ddd;
            border-radius: 5px;
            position: relative;
            transition: all 0.25s linear;
        }
        .ex1 span:before {
            content: '';
            position: absolute;
            left: 5px;
            top: 50%;
            -webkit-transform: translatey(-50%);
            transform: translatey(-50%);
            width: 18px;
            height: 18px;
            border-radius: 50%;
            background-color: #ddd;
            transition: all 0.25s linear;
        }
        .ex1 input:checked + span {
            background-color: #fff;
            box-shadow: 0 0 10px 2px rgba(0, 0, 0, 0.1);
        }
        .ex1 .red input:checked + span {
            color: red;
            border-color: red;
        }
        .ex1 .red input:checked + span:before {
            background-color: red;
        }
        .ex1 .blue input:checked + span {
            color: blue;
            border-color: blue;
        }
        .ex1 .blue input:checked + span:before {
            background-color: blue;
        }
        .ex1 .orange input:checked + span {
            color: orange;
            border-color: orange;
        }
        .ex1 .orange input:checked + span:before {
            background-color: orange;
        }
        .col-md-4{
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
    <section class="invoice">
        <div class="container">
            <div class="row invoice-warning">
                <!-- Profile Image -->
                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-responsive img-circle"  src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->user_name }}">
                            </div>

                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
                            <p class="text-muted text-center"><i class="fa fa-edit"></i> {{trans('common.edit').trans('common.general')}}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-9">

                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <h3>
                                        <i class="fa fa-map-marker"></i> {{trans('common.address_for_invoice_or_tax')}}
                                        <span class="pull-right"><small><a href="{{url('booking/add/more/invoice_address')}}"> <i class="fa fa-plus"></i>  {{trans('common.add_new_address')}}</a></small></span>
                                    </h3>
                                </div>
                                <div class="col-sm-12"><hr><div id="show_form_tax" class="text-success"></div>
                                    <div class="example ex1">

                                        @foreach($AddressBook as $rows)
                                            <label class="radio red">
                                                <input type="radio" name="address_book" id="address_book" {{$rows->default_address=='1'?'checked':''}}  value="{{$rows->address_book_id}}"/>
                                            <span> {!! $rows->address_show?$rows->address_show:$rows->entry_street_address!!}  <small class="pull-right"><a href="{{url('booking/edit/invoice_address/'.$rows->address_book_id)}}"> <i class="fa fa-edit"></i> {{trans('common.edit')}}</a></small></span>

                                            </label>

                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-sm-12"><hr>
                                    {{--<a href="{{url('booking/continuous/step2')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> {{trans('common.back_to_list')}}</a>--}}
                                    <a href="{{Session::get('CurrentUrl')}}" class="btn btn-info pull-right"><i class="fa fa-arrow-right"></i> {{trans('common.continuous')}}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>


    <script>
        $('body').on('click','#address_book',function () {
            if($(this).is(":checked")){
                $.ajax({
                    type:'get',
                    url:SP_source() + 'booking/ajax/set/address',
                    data:{'address_id':$(this).val(),'type':'2'},
                    success:function(data){
                        $('#show_form_tax').html(data);
                        $("#show_form_tax").hide(2000,"swing");
                    }
                });
            }
        });
    </script>
@endsection