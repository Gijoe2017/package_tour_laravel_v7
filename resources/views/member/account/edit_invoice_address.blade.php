@extends('layouts.package.master')

@section('program-highlight')

    <section class="invoice">
        <div class="container">
            <div class="row invoice-warning">
                <!-- Profile Image -->

                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-responsive img-circle"  src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->user_name }}">
                            </div>

                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>

                            <p class="text-muted text-center"><i class="fa fa-edit"></i> {{trans('common.edit').trans('common.general')}}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <form method="post" id="form1" class="form-control form-booking" enctype="multipart/form-data" action="{{action('Package\OrderMemberController@booking_invoice_address_update')}}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="address_book_id" value="{{$Address->address_book_id}}">
                            <link rel="stylesheet" href="{{asset('member/assets/select2/dist/css/select2.min.css')}}">
                            <script src="{{asset('member/assets/select2/dist/js/select2.full.min.js')}}"></script>

                                <div class="form-group">
                                    <h4><i class="fa fa-info"></i> {{trans('common.address_for_invoice_or_tax')}}</h4>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('profile.FullName')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="entry_firstname" name="entry_firstname" value="{{$Address->entry_firstname}}" required  placeholder="{{trans('profile.FirstName')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="entry_lastname" name="entry_lastname" value="{{$Address->entry_lastname}}" required  placeholder="{{trans('profile.LastName')}}">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.emails')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="tax_email" id="tax_email"  value="{{$Address->entry_email}}" placeholder="{{trans('common.emails')}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.phone')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="tax_phone" id="tax_phone" value="{{$Address->entry_phone}}" placeholder="{{trans('common.phone')}}" required>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.address')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="address" id="address" value="{{$Address->entry_street_address}}" placeholder="{{trans('common.address')}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.country')}} <span class="text-danger">*</span></strong></label>
                                    <div class="col-sm-8">
                                        <?php
                                        $Country=\App\Country::where('language_code',Auth::user()->language)->get();
                                        if($Country->count()==0){
                                            $Country=\App\Country::where('language_code','en')->get();
                                        }
                                        ?>
                                        <select name="country_id" id="country_id" class="form-control select2" required>
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($Country as $rows)
                                                <option value="{{$rows->country_id}}" {{$Address->entry_country_id==$rows->country_id?'selected':''}}>{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group box-state">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.state')}}</strong></label>
                                    <div class="col-sm-8">
                                        <?php
                                        $states = DB::table('states')
                                            ->where('language_code', Auth::user()->language)
                                            ->where('country_id',$Address->entry_country_id)
                                            ->get();
                                        if (!$states) {
                                            $states = DB::table('states')
                                                ->where('country_id',$Address->entry_country_id)
                                                ->where('language_code', 'en')
                                                ->get();
                                        }
                                        ?>
                                        <select name="state_id" id="state_id" class="form-control select2">
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($states as $rows)
                                                <option value="{{$rows->state_id}}" {{$Address->entry_state==$rows->state_id?'selected':''}}>{{$rows->state}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group box-state city_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city')}}</strong></label>
                                    <div class="col-sm-8">
                                        <?php
                                        $city = DB::table('cities')
                                            ->where('language_code', Auth::user()->language)
                                            ->where('state_id',$Address->entry_state)
                                            ->get();

                                        if (!$city) {
                                            $city = DB::table('cities')
                                                ->where('language_code', 'en')
                                                ->where('state_id',$Address->entry_state)
                                                ->get();
                                        }

                                        ?>
                                        <select name="city_id" id="city_id" class="form-control select2">
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($city as $rows)
                                                <option value="{{$rows->city_id}}" {{$Address->entry_city==$rows->city_id?'selected':''}}>{{$rows->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group box-state city_sub1_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city_sub')}}</strong></label>
                                    <div class="col-sm-8">
                                        <?php
                                        $city_sub = DB::table('cities_sub1')
                                            ->where('language_code', Auth::user()->language)
                                            ->where('country_id',$Address->entry_country_id)
                                            ->where('state_id',$Address->entry_state)
                                            ->where('city_id',$Address->entry_city)
                                            ->get();

                                        if (!$city_sub) {
                                            $city_sub = DB::table('cities_sub1')
                                                ->where('country_id',$Address->entry_country_id)
                                                ->where('state_id',$Address->entry_state)
                                                ->where('city_id',$Address->entry_city)
                                                ->where('language_code', 'en')
                                                ->get();
                                        }
                                        ?>
                                        <select name="city_sub1_id" id="city_sub1_id" class="form-control select2">
                                            <option value="">{{trans('common.choose')}}</option>
                                            @foreach($city_sub as $rows)
                                                <option value="{{$rows->city_sub1_id}}" {{$Address->entry_city_sub==$rows->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group city_sub1_id">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.zipcode')}}</strong></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="zip_code" id="zip_code" value="{{$Address->entry_postcode}}" placeholder="{{trans('common.zipcode')}}">
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <hr>
                                    <div class="pull-left">
                                    <a href="{{url('booking/show/invoice_address')}}" class="btn btn-warning btn-lg"><i class="fa fa-arrow-left"></i> {{trans('common.back_to_list')}}</a>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                                </div>
                            </form>

                            <script src="{{asset('member/assets/bootstrap/js/app.js')}}"></script>
                            <script>
                                $(function () {
                                    //Initialize Select2 Elements
                                    $('.select2').select2()
                                });


                            </script>

                        </div>
                    </div>
                </div>

                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection