@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">{{trans('profile.family_informaition')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">{{trans('profile.step')}} 4 / 6</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Member\FamilyController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="relation_id" value="{{$Family->relation_id}}">
                        <input type="hidden" name="event" value="{{$event}}">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $Relation=\App\models\Relation::where('language_code',Auth::user()->language)->get();
                                        if(count($Relation)==0){
                                            $Relation=\App\models\Relation::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="relation_type_id" class="form-label ">{{trans('profile.relation')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <select class="form-control select2" name="relation_type_id" id="relation_type_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach($Relation as $rows)
                                                    <option value="{{$rows->relation_type_id}}" {{$rows->relation_type_id==$Family->relation_type_id?'Selected':''}}>{{$rows->relation_type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_first_name" class="form-label required">{{trans('profile.f_name')}} <span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="first_name" id="first_name" value="{{$event!='change'?$Family->first_name:''}}" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_last_name" class="form-label required">{{trans('profile.l_name')}} <span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="last_name" id="last_name" value="{{$event!='change'?$Family->last_name:''}}" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_middle_name" class="form-label">{{trans('profile.n_name')}} </label>
                                        <input class="form-control" type="text" name="middle_name" id="middle_name" value="{{$event!='change'?$Family->middle_name:''}}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $country=\App\Country::where('language_code',Auth::user()->language)->get();
                                        if(count($country)==0){
                                            $country=\App\Country::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="country_of_birth_id" id="country_of_birth_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$Family->country_of_birth_id?'Selected':''}}>{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="place_of_birth" class="form-label required">{{trans('profile.place_of_birth')}} <span class="text-red">*</span></label>
                                        <input  class="form-control" type="text" name="place_of_birth" id="place_of_birth" value="{{$Family->place_of_birth}}" required />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $Religion=\App\models\Religion::where('language_code',Auth::user()->language)->get();
                                        if(count($Religion)==0){
                                            $Religion=\App\models\Religion::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="religion_id" class="form-label required">{{trans('profile.religion')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="religion_id" id="religion_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Religion as $rows)
                                                <option value="{{$rows->religion_id}}" {{$rows->religion_id==$Family->religion_id?'Selected':''}} >{{$rows->religion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $Nationality=\App\models\Nationality::where('language_code',Auth::user()->language)->get();
                                        if(count($Nationality)==0){
                                            $Nationality=\App\models\Nationality::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="nationality_no" class="form-label required">{{trans('profile.nationality')}} <span class="text-red">*</span></label>
                                        <select class="form-control" name="nationality_id" id="nationality_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Family->nationality_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="prev_nationality_id" class="form-label required">{{trans('profile.previous_nationality')}}</label>
                                        <select class="form-control select2" name="prev_nationality_id" id="prev_nationality_id">
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Family->prev_nationality_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" align="right">
                                        <a href="{{url('member/step4')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </section>
@endsection