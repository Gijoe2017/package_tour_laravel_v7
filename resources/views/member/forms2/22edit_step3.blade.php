@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">Address <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                    <span class="step-heading">ะำห Contact Informaltion: </span>
                    <span class="step-number">Step 3 / 5</span>
                        </div>
                </div>
                <div class="box-body">
                <form method="post" action="{{action('Member\MemberController@save_step3')}}">
                    {{csrf_field()}}
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                        <label for="address_type_id" class="form-label required">{{trans('profile.AddressType')}}</label>
                                        <select class="form-control select2" name="address_type_id" id="address_type_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach(App\models\AddressType::where('language_code',Auth::user()->language)->get() as $rows)
                                                <option value="{{$rows->address_type_id}}">{{$rows->address_type}}</option>
                                            @endforeach
                                        </select>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-select">
                                    <label for="address" class="form-label required">{{trans('profile.Address')}}</label>
                                    <input class="form-control" type="text" name="address" id="address" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="additional" class="form-label">{{trans('profile.Additional')}}</label>
                                    <input class="form-control" type="text" name="additional" id="additional" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">

                                        <label for="country_id" class="form-label required">{{trans('profile.Country')}}</label>
                                        <select class="form-control select2" name="country_id" id="country_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>

                                </div>
                            </div>
                            <div class="col-md-4 box-state">
                                <div class="form-group">

                                        <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                        <select  class="form-control select2" name="state_id" id="state_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->state_id}}">{{$rows->state}}</promotion>--}}
                                            {{--@endforeach--}}
                                        </select>

                                </div>
                            </div>
                            <div class="col-md-4 box-state city_id">
                                <div class="form-group">

                                        <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                        <select class="form-control select2" name="city_id" id="city_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->city_id}}">{{$rows->city}}</promotion>--}}
                                            {{--@endforeach--}}
                                        </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 box-state city_sub1_id">
                                <div class="form-group">

                                        <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                        <select class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->city_sub1_id}}">{{$rows->city_sub1_name}}</promotion>--}}
                                            {{--@endforeach--}}
                                        </select>

                                </div>
                            </div>
                            <div class="col-md-4 city_sub1_id">
                                <div class="form-group">
                                    <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                    <input class="form-control" type="text" name="zip_code" id="zip_code" />
                                </div>
                            </div>
                        </div>

                        <h3>
                            <span class="icon"><i class="fa fa-phone-square"></i></span>
                            <span class="title_text">Contact</span>
                        </h3>
                        <hr>
                        <div class="col-md-12" style="position: absolute; z-index: 9" align="right">
                            <button type="button" style="display: none" class="btn btn-xs btn-warning" id="removeButtonEmail"><i class="fa fa-trash"></i> Remove input email</button>
                            <button type="button" class="btn btn-xs btn-info" id="addButtonEmail"><i class="fa fa-plus"></i> Add more email</button>
                        </div>
                        <div class="row more-email">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email" class="form-label required">{{trans('profile.Email')}}</label>
                                    <input class="form-control"  type="email" name="email[]" id="email" />
                                </div>

                            </div>
                        </div>
                        <hr>

                        {{--<div class="row">--}}
                            {{--<div class="col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="phone_country_id" class="form-label required">{{trans('profile.Country')}}</label>--}}
                                    {{--<select class="form-control" name="phone_country_id" id="phone_country_id">--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                        {{--@foreach($country as $rows)--}}
                                            {{--<promotion value="{{$rows->country_id}}">{{$rows->country}}</promotion>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="phone_country_code" class="form-label required">{{trans('profile.Country')}}</label>--}}
                                    {{--<select class="form-control" name="phone_country_code" id="phone_country_code">--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                        {{--@foreach(\App\models\PhoneCode::all() as $rows)--}}
                                            {{--<promotion value="{{$rows->phone_code}}">{{$rows->country}} ({{$rows->phone_code}})</promotion>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="phone" class="form-label required">{{trans('profile.Phone')}}</label>--}}
                                    {{--<input class="form-control"  type="text" name="phone[]" id="phone" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="phone" class="form-label required">{{trans('profile.PhoneType')}}</label>--}}
                                    {{--<select class="form-control" name="phone_type" id="phone_type">--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                        {{--<promotion value="mobile">-- {{trans('profile.mobile')}} --</promotion>--}}
                                        {{--<promotion value="phone">-- {{trans('profile.phone')}} --</promotion>--}}

                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-2">

                                <label for="phone_country_id" class="form-label required">{{trans('profile.Country')}}</label>
                                <select class="form-control select2" name="phone_country_id" id="phone_country_id">
                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                    @foreach($country as $rows)
                                        <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="phone_country_code" class="form-label required">{{trans('profile.phone_code')}}</label>
                                <select class="form-control select2" name="phone_country_code" id="phone_country_code">
                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                    @foreach(\App\models\PhoneCode::all() as $rows)
                                        <option value="{{$rows->phone_code}}" >{{$rows->country}} - {{$rows->phone_code}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="phone_type" class="form-label required">{{trans('profile.phone_type')}}</label>
                                <select class="form-control select2" name="phone_type" id="phone_type">
                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                    <option value="moblie" >{{trans('profile.mobile')}}</option>
                                    <option value="phone" >{{trans('profile.phone')}}</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="phone_number" class="form-label">{{trans('profile.phone_name')}}</label>
                                <div class="input-group">
                                    <input type="text" name="phone_number" id="phone_number" class="form-control"  data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                    <div class="input-group-btn">
                                        <button id="add-phone-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                        <button id="add-phone" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">
<hr>
                        <div id="show-phone-detail" style="{{isset($phone)?'':'display:none'}}" class="col-md-12">
                        @if(isset($phone))
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>{{trans('profile.Country')}}</th>
                                    <th>{{trans('profile.phone_code')}}</th>
                                    <th>{{trans('profile.phone_type')}}</th>
                                    <th>{{trans('profile.phone_name')}}</th>
                                    <th>{{trans('profile.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($phone as $rows)
                                    <tr>
                                        <td>{{$rows->country}}</td>
                                        <td>{{$rows->phone_country_code}}</td>
                                        <td>{{$rows->phone_type}}</td>
                                        <td>{{$rows->phone_number}}</td>
                                        <td align="center"><button type="button" class="btn btn-danger btn-xs" onclick="delete_phone('{{$rows->id}}')" ><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                            </div>
                        </div>

                <hr>
                <div class="form-group" align="right">
                    <a href="{{url('member/edit-step2')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                    <button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-save"></i> Save and continue</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
    </section>
 @endsection