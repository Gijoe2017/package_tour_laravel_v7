@extends('layouts.member.layout_master_new')
@section('content')

    <section class="content">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-phone"></i></span>
                        <span class="title_text">Contact <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">Step 3 / 10</span>
                    </div>
                </div>
                <div class="box-body">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tap-address" data-toggle="tab"><strong><i class="fa fa-map-marker"></i> Address</strong></a></li>
                            <li><a href="#tap-email" data-toggle="tab"><strong><i class="fa fa-mail-reply-all"></i> Email</strong></a></li>
                            <li><a href="#tap-phone" data-toggle="tab"><strong><i class="fa fa-mobile-phone"></i> Phone / Mobile</strong></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="tap-address">
                                <div class="box-body">
                                    @if(isset($AddressInfo))
                                        <form method="post" id="myform-address" action="{{action('Member\MemberController@update_address')}}" >
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="address_type_id" class="form-label required">{{trans('profile.AddressType')}}</label>
                                                        <select class="form-control select2" name="address_type_id" id="address_type_id" required>
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach(App\models\AddressType::where('language_code',Auth::user()->language)->get() as $rows)
                                                                <option value="{{$rows->address_type_id}}" {{$rows->address_type_id==$AddressInfo->address_type_id?'selected':''}}>{{$rows->address_type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-select">
                                                        <label for="address" class="form-label required">{{trans('profile.Address')}}</label>
                                                        <input class="form-control" type="text" name="address" id="address" value="{{$AddressInfo->address}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional" class="form-label">{{trans('profile.Additional')}}</label>
                                                        <input class="form-control" type="text" name="additional" id="additional" value="{{$AddressInfo->additional}}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="country_id" class="form-label required">{{trans('profile.Country')}}</label>
                                                        <select class="form-control select2" name="country_id" id="country_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$AddressInfo->country_id?'selected':''}}>{{$rows->country}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state">
                                                    <div class="form-group">
                                                        <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                                        <select  class="form-control select2" name="state_id" id="state_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 box-state city_id">
                                                    <div class="form-group">
                                                        <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                                        <select class="form-control select2" name="city_id" id="city_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state city_sub1_id">
                                                    <div class="form-group">

                                                        <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                                        <select class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 city_sub1_id">
                                                    <div class="form-group">
                                                        <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                                        <input class="form-control" type="text" name="zip_code" id="zip_code" value="{{$AddressInfo->zip_code}}" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"><label></label>
                                                        <button type="button" id="address-submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Save</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    @else
                                        <form method="post" id="myform-address" action="{{action('Member\MemberController@update_address')}}" >
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="address_type_id" class="form-label required">{{trans('profile.AddressType')}}</label>
                                                        <select class="form-control select2" name="address_type_id" id="address_type_id" required>
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach(App\models\AddressType::where('language_code',Auth::user()->language)->get() as $rows)
                                                                <option value="{{$rows->address_type_id}}" >{{$rows->address_type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-select">
                                                        <label for="address" class="form-label required">{{trans('profile.Address')}}</label>
                                                        <input class="form-control" type="text" name="address" id="address" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional" class="form-label">{{trans('profile.Additional')}}</label>
                                                        <input class="form-control" type="text" name="additional" id="additional"  />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="country_id" class="form-label required">{{trans('profile.Country')}}</label>
                                                        <select class="form-control select2" name="country_id" id="country_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                                <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state">
                                                    <div class="form-group">
                                                        <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                                        <select  class="form-control select2" name="state_id" id="state_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 box-state city_id">
                                                    <div class="form-group">
                                                        <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                                        <select class="form-control select2" name="city_id" id="city_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state city_sub1_id">
                                                    <div class="form-group">

                                                        <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                                        <select class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                            {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 city_sub1_id">
                                                    <div class="form-group">
                                                        <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                                        <input class="form-control" type="text" name="zip_code" id="zip_code"  />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"><label></label>
                                                        <button type="button" id="address-submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Save</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>

                                    @endif
                                    <div class="row">
                                        <hr>
                                        <div id="show-address-detail" class="col-lg-12 ">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.AddressType')}}</th>
                                                    <th>{{trans('profile.Address')}}</th>
                                                    <th>{{trans('profile.Zip_code')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($Address as $rows)
                                                    <tr>
                                                        <td>{{$rows->address_type}}</td>
                                                        <td>{{$rows->address.' '.$rows->additional}}</td>
                                                        <td>{{$rows->zip_code}}</td>
                                                        <td>
                                                            <a class="btn btn-sm btn-warning address-edit" data-id="{{$rows->address_id}}" ><i class="fa fa-edit"></i>  {{trans('profile.Edit')}}</a>
                                                            <button class="btn btn-sm btn-danger delete_address" data-id="{{$rows->address_id}}"><i class="fa fa-trash"></i>  {{trans('profile.Delete')}}</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tap-email">
                                <!-- The timeline -->
                                <div class="box-body">

                                    <div class="col-md-6">
                                        <label for="phone_number" class="form-label">{{trans('profile.Email')}}</label>
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control" >
                                            <div class="input-group-btn">
                                                <button id="add-email-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                                <button id="add-email" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                            </div>
                                        </div>


                                        <hr>
                                        <div id="show-email-detail">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.Email')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($Email as $rows)
                                                    <tr>
                                                        <td>{{$rows->email}}</td>
                                                        <td><button data-id="{{$rows->id}}" class="btn btn-sm btn-danger delete_email"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="tap-phone">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="phone_country_id">{{trans('profile.Country')}}</label><BR>
                                            <select class="form-control select2" name="phone_country_id" id="phone_country_id" style="width: 100%">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="phone_country_code" >{{trans('profile.phone_code')}}</label><BR>
                                            <select class="form-control select2" name="phone_country_code" id="phone_country_code" style="width: 100%">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\models\PhoneCode::all() as $rows)
                                                    <option value="{{$rows->phone_code}}">{{$rows->country}} - {{$rows->phone_code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone_type" >{{trans('profile.phone_type')}}</label><BR>
                                                <select class="form-control select2" name="phone_type" id="phone_type" style="width: 100%">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    <option value="moblie">{{trans('profile.mobile')}}</option>
                                                    <option value="phone">{{trans('profile.phone')}}</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <label for="phone_number">{{trans('profile.phone_name')}}</label>
                                            <div class="input-group">
                                                <input type="text" name="phone_number" id="phone_number" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                                <div class="input-group-btn">
                                                    <button id="add-phone-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                                    <button id="add-phone" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div id="show-phone-detail" style="display:{{isset($Phones)?'none':''}} none" class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.Country')}}</th>
                                                    <th>{{trans('profile.phone_code')}}</th>
                                                    <th>{{trans('profile.phone_type')}}</th>
                                                    <th>{{trans('profile.phone_name')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($Phones as $phone)
                                                    <tr>
                                                        <td>{{$phone->country_id}}</td>
                                                        <td>{{$phone->phone_country_code}}</td>
                                                        <td>{{$phone->phone_type}}</td>
                                                        <td>{{$phone->phone_number}}</td>
                                                        <td align='center'><button class="btn btn-danger btn-sm" id="phone-delete" data-id="{{$phone->id}}"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>


                    <div class="form-group" align="right">
                        <a href="{{url('member/edit-step2')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                        <a type="button" href="{{url('/member/step4')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>
                    </div>



                </div>
            </div>
        </div>
    </section>
@endsection