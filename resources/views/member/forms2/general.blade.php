@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">{{trans('profile.general')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                    <span class="step-heading">{{trans('profile.personal_informaltion')}}: </span>
                    <span class="step-number">{{trans('profile.step')}} 1 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" action="{{action('Member\MemberController@save_step1')}}">
                    {{csrf_field()}}
                    <div class="col-lg-12">
                    <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label  for="inputError" class="control-label">Identification ID <span class="text-red">*</span><span class="text-red" id="inputError"></span> </label>
                                    <input type="text" class="form-control" name="identification_id" id="identification_id"  required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="user_title_id" class="form-label required ">Title <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="user_title_id" id="user_title_id" required>
                                            <option value="">{{trans('profile.Choose')}}</option>
                                            @foreach(\App\models\Title::where('language_code',Auth::user()->language)->get() as $rows)
                                                <option value="{{$rows->user_title_id}}">{{$rows->user_title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-label required">First name <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="first_name" id="first_name" required />
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="last_name" class="form-label required">Last name <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="last_name" id="last_name" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="middle_name" class="form-label">Middle name</label>
                                    <input class="form-control" type="text" name="middle_name" id="middle_name" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="sex_id" class="form-label required">Gender <span class="text-red">*</span></label>
                                        <select class="form-control select2"  name="sex_id" id="sex_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach(\App\models\Sex::where('language_code',Auth::user()->language)->get() as $rows)
                                                <option value="{{$rows->sex_id}}" >{{$rows->sex_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
<hr>                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date of birth <span class="text-red">*</span></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="date_of_birth" id="datepicker" required>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="country_of_birth_id" id="country_of_birth_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-select">
                                        <label for="marital_status_id" class="form-label required">{{trans('profile.status')}} <span class="text-red">*</span></label>
                                        <div class="select-list">
                                            <select class="form-control select2" name="marital_status_id" id="marital_status_id" required>
                                                <option value="">-- {{trans('profile.Choose')}} *--</option>
                                                @foreach(\App\models\MaritalStatus::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->marital_status_id}}" {{$rows->marital_status=='1'?'selected':''}}>{{$rows->marital_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="origin_id" class="form-label required">{{trans('profile.citizenship')}} <span class="text-red">*</span></label>

                                    <select class="form-control select2" name="origin_id" id="origin_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Origin::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->origin_id}}" >{{$rows->origin_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="religion_id" class="form-label required">{{trans('profile.religion')}} <span class="text-red">*</span></label>

                                    <select class="form-control select2" name="religion_id" id="religion_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Religion::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->religion_id}}" >{{$rows->religion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<label for="nationality_no" class="form-label required">{{trans('profile.nationality')}} <span class="text-red">*</span></label>--}}

                                    {{--<select class="form-control select2" name="nationality_id" id="nationality_id" required>--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} *--</promotion>--}}
                                        {{--@foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->nationality_id}}" >{{$rows->nationality}}</promotion>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="nationality_no" class="form-label required">{{trans('profile.current_nationality')}} <span class="text-red">*</span></label>

                                    <select class="form-control select2" name="current_nationality_id" id="current_nationality_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->nationality_id}}" >{{$rows->nationality}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="naturalization_id" class="form-label required">{{trans('profile.naturalization')}} </label>

                                    <select class="form-control select2" name="naturalization_id" id="naturalization_id" >
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->nationality_id}}" >{{$rows->nationality}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div align="center">
                        <label class="form-label">{{trans('profile.picture')}}</label></div>
                        <div class="form-group" align="center">
                            <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                        </div>
                        <div class="form-group"><center>
                                <input type="file"  id="imgInp" name="passport_cover_image" class="filestyle" data-input="false" ></center>
                        </div>
                    </div>

                </div>



                <hr>
                <div class="form-group" align="right">
                    <button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-save"></i> Save and continue</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
    </section>


 @endsection