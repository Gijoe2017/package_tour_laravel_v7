@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-users"></i></span>
                        <span class="title_text">{{trans('profile.family_informaition')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                    <span class="step-number">{{trans('profile.step')}} 4 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-red ">

                                </div>
                                <a href="{{url('member/family/create')}}">
                                <div class="widget-user-image">
                                    <img class="img-circle" src="{{asset('images/member/user-plus.png')}}" alt="User Avatar">
                                </div>
                                </a>
                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 border-right">
                                            <div class="description-block">

                                            </div>
                                            <!-- /.description-block -->
                                        </div>

                                    </div>

                                    <div >
                                        <a href="{{url('member/family/create')}}" class="btn btn-success btn-block btn-xs"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a>
                                    </div>

                                    <!-- /.row -->
                                </div>

                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <?php $i=0;?>
                        @foreach($Families as $rows)
                        <?php if($i==4){$i=0;}
                        $i++;
                        ?>
                                <!-- /.col -->
                        <div class="col-md-3">
                            <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header {{$bgColor[$i]}} ">
                                    <h4 class="widget-user-username text-center">
                                        {{$rows->first_name.' '.$rows->last_name}}
                                    </h4>
                                    <h5 class="widget-user-desc"></h5>
                                </div>
                                <div class="widget-user-image">
                                        <img class="img-circle" src="{{asset('images/member/user-plus.png')}}" alt="User Avatar">
                                </div>
                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header">{{trans('profile.relation')}}: {{$rows->relation_type}}</h5>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>

                                    </div>

                                    <div >
                                        <a href="{{url('member/family/edit/'.$rows->relation_id)}}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"></i> {{trans('profile.update')}}</a>
                                        <a href="{{url('member/family/change-language/'.$rows->relation_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-xs">
                                            <i class="fa fa-flag"></i> {{trans('profile.language')}}</a>
                                        <a class="btn btn-warning btn-xs pull-right" href="{{url('member/family/delete/'.$rows->relation_id)}}" onclick="return confirm_delete()">
                                            <i class="fa fa-trash"></i> {{trans('profile.Delete')}}
                                        </a>
                                    </div>

                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <!-- /.col -->
                        @endforeach


                                <!-- /.col -->
                    </div>
                    <div class="form-group" align="right">
                        <a href="{{url('member/edit-step3')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                        <a type="button" href="{{url('/member/education')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>
                    </div>
                    {{--<div class="form-group" align="right">--}}
                        {{--<a href="{{url('member/edit-step3')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> Previous</a>--}}
                        {{--<a type="button" href="{{url('/member/education')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> Continue</a>--}}
                    {{--</div>--}}
                </div>
            </div>
            </div>
    </section>
 @endsection