@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">{{trans('profile.passport_info')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                    <span class="step-number">{{trans('profile.step')}} 2 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                <form method="post" id="form-continue-step3" enctype="multipart/form-data" action="{{action('Member\MemberController@update_step2')}}">
                    {{csrf_field()}}
                    <input type="hidden" id="event" name="event">
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-md-9">

                                <div class="form-group">
                                    <label for="passport_number" class="form-label required">{{trans('profile.passport_number')}}</label>
                                    <input type="text" class="form-control" name="passport_number" id="passport_number" value="{{$Member->passport_number}}" />
                                </div>

                                <div class="form-group">
                                    <?php
                                    $country=\App\Country::where('language_code',Auth::user()->language)->get();
                                    if(count($country)==0){
                                        $country=\App\Country::where('language_code','en')->get();
                                    }
                                    ?>
                                    <label for="passport_country_id" class="form-label required">{{trans('profile.passport_coutry')}}</label>
                                    <div class="select-list">
                                        <select class="form-control select2" name="passport_country_id" id="passport_country_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$Member->passport_country_id?'selected':''}}>{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>

                        <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('profile.passport_DOI')}} <span class="text-red">*</span></label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="passport_DOI" id="datepicker" value="{{$Member->passport_DOI}}" required>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>{{trans('profile.passport_DOE')}} <span class="text-red">*</span></label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="passport_DOE" id="datepicker1" value="{{$Member->passport_DOE}}" required>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
</div>
                            </div>
                            <div class="col-md-3">
                            <div align="center">
                                <label for="photo_for_visa" class="form-label ">{{trans('profile.passport_picture')}}</label>
                                <div class="form-group" align="center">
                                    @if($Member->photo_for_visa)
                                        <img id="blah" src="{{asset('images/member/visa/'.$Member->photo_for_visa)}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                                        @else
                                        <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                                        @endif
                                </div>
                                <div class="form-group"><center>
                                        <input type="file"  id="imgInp" name="photo_for_visa" class="filestyle" data-input="false" ></center>
                                </div>
                                </div>
                            </div>
                        </div>



                <hr>
                        @if(Session::has('message'))
                            <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                        @endif
                <div class="col-md-6" >
                    <a href="{{url('member/edit-step1')}}" class="btn btn-warning btn-lg"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                    <a id="continue-step3" href="#" class="btn btn-danger btn-lg "><i class="fa fa-mail-forward"></i> {{trans('profile.save_and_continue')}}</a>
                </div>
                    <div class="col-md-6" align="right">
                    <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
    </section>
 @endsection