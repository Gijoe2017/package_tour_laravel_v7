@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">Passpost <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                    <span class="step-heading">Personal Informaltion: </span>
                    <span class="step-number">Step 2 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" action="{{action('Member\MemberController@save_step2')}}">
                    {{csrf_field()}}
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-md-9">

                                <div class="form-group">
                                    <label for="passport_number" class="form-label required">{{trans('profile.passport_number')}}</label>
                                    <input type="text" class="form-control" name="passport_number" id="passport_number" />
                                </div>

                                <div class="form-group">
                                    <label for="passport_country_id" class="form-label required">{{trans('profile.passport_coutry')}}</label>
                                    <div class="select-list">
                                        <select class="form-control select2" name="passport_country_id" id="passport_country_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>

                        <div class="row">
                                <div class="col-md-6">


                                        <div class="form-group">
                                            <label>Passport DOI  <span class="text-red">*</span></label>

                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="passport_DOI" id="datepicker" required>
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Passport DOE  <span class="text-red">*</span></label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="passport_DOE" id="datepicker1" required>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
</div>
                            </div>
                            <div class="col-md-3">
                                <label for="photo_for_visa" class="form-label ">{{trans('profile.picture')}}</label>
                                <div class="form-group" align="center">
                                    <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                                </div>
                                <div class="form-group"><center>
                                        <input type="file"  id="imgInp" name="photo_for_visa" class="filestyle" data-input="false" ></center>
                                </div>
                            </div>
                        </div>



                <hr>
                <div class="form-group" align="right">
                    <a href="{{url('member/edit-step1')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                    <button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-save"></i> Save and continue</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
    </section>
 @endsection