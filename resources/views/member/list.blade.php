@extends('layouts.member.layout_master_new')

@section('content-chart')
    <style type="text/css">
        .btn{
            text-align: left;
        }
        .widget-user-username{
            font-size: 18px !important;
            font-style: normal;
        }
        .nav>li>a{
            padding:5px 10px;
        }
    </style>
<div class="row">
    <div class="col-md-3">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-red ">
                <h4 class="widget-user-username">Create New</h4>
                <h5 class="widget-user-desc"></h5>
            </div>
            <div class="widget-user-image">
               <img class="img-circle" src="{{asset('images/member/user-plus.png')}}" alt="User Avatar">
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header">0</h5>
                            <span class="description-text">E-mail</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header">0</h5>
                            <span class="description-text">Award</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header">0</h5>
                            <span class="description-text">Visa</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                </div>

                <div >
                    <a href="{{url('member/create')}}" class="btn btn-success btn-block btn-xs"><i class="fa fa-plus"></i> Create New Member</a>
                </div>

                <!-- /.row -->
            </div>

        </div>
        <!-- /.widget-user -->
    </div>
<?php $i=0;?>
@foreach($Members as $rows)
    <?php if($i==4){$i=0;}
    $i++;
    ?>
    <!-- /.col -->
        <div class="col-md-3">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header {{$bgColor[$i]}} ">
                    <h4 class="widget-user-username">
                        <?php
                        $Titles=\App\models\Title::where('user_title_id',$rows->user_title_id)
                            ->where('language_code',Auth::user()->language)->first();
                        if(!$Titles){
                            $Titles=\App\models\Title::where('user_title_id',$rows->user_title_id)->where('language_code','en')->first();
                        }
                        ?>
                        {{$Titles->user_title .$rows->first_name.' '.$rows->last_name}}
                    </h4>
                    <h5 class="widget-user-desc">Lead Developer</h5>
                </div>
                <div class="widget-user-image">
                    <?php $fileName = public_path('images/member/visa/'.$rows->photo_for_visa);?>
                    @if (file_exists($fileName))
                        <img class="img-circle" src="{{asset('images/member/visa/'.$rows->photo_for_visa)}}" alt="User Avatar">
                    @else
                        <img class="img-circle" src="{{asset('images/member/visa/avatar.png')}}" alt="User Avatar">
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">3</h5>
                                <span class="description-text">E-mail</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">1</h5>
                                <span class="description-text">Award</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">3</h5>
                                <span class="description-text">Visa</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->

                    </div>

                    <div >
                        <a href="{{url('member/edit/'.$rows->passport_id)}}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> {{trans('profile.update')}}</a>
                        <a href="{{url('member/change-language/'.$rows->passport_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-xs">
                           <i class="fa fa-flag"></i> {{trans('profile.language')}}</a>
                        <a class="btn btn-warning btn-xs pull-right" onclick="return confirm_delete()" href="{{url('member/delete/'.$rows->passport_id)}}">
                            <i class="fa  fa-trash"></i> {{trans('profile.Delete')}}
                        </a>
                    </div>

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
        <!-- /.col -->
@endforeach


<!-- /.col -->
</div>
    @endsection