@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.travel_history')}}  <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/member/visited/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right">
                     <span class="step-number">{{trans('profile.step')}} 10 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    @if($Visiting)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.will_visit_country')}}</th>
                            <th>{{trans('profile.Address')}} </th>
                            <th>{{trans('profile.Country')}} </th>
                            <th>{{trans('profile.Zip_code')}} </th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Visiting as $rows)
                            <?php
                            $state=null;$city=null;
                            $country=App\Country::where('country_id',$rows->country_id)->first();
                            $state=$country->state()->where('state_id',$rows->state_id)->where('language_code',Auth::user()->langauge)->first();
                          //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }
                            ?>
                            <tr>
                                <td> {{$rows->name}}</td>
                                <td>{{$rows->address}} {{$state}} {{$city}} </td>
                                <td>{{$country->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td align="right">
                                    {{--<a href="{{url('/member/visited/change-language/'.$rows->visited_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>--}}
                                    <a href="{{url('/member/visited/edit/'.$rows->visited_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/member/visited/delete/'.$rows->visited_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>

 <div class="box-body">
     @if($Visit)
         <hr>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.now_in_country')}}</th>
                            <th>{{trans('profile.Address')}} </th>
                            <th>{{trans('profile.Country')}} </th>
                            <th>{{trans('profile.Zip_code')}} </th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Visit as $rows)
                            <?php
                            $state=null;$city=null;
                            $country=App\Country::where('country_id',$rows->country_id)->first();
                            $state=$country->state()->where('state_id',$rows->state_id)->where('language_code',Auth::user()->langauge)->first();
                          //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }
                            ?>
                            <tr>
                                <td> {{$rows->name}}</td>
                                <td>{{$rows->address}} {{$state}} {{$city}} </td>
                                <td>{{$country->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td align="right">
                                    {{--<a href="{{url('/member/visited/change-language/'.$rows->visited_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>--}}
                                    <a href="{{url('/member/visited/edit/'.$rows->visited_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/member/visited/delete/'.$rows->visited_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>

 <div class="box-body">
     @if($Visited)
         <hr>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.country_visited')}}</th>
                            <th>{{trans('profile.Address')}} </th>
                            <th>{{trans('profile.Country')}} </th>
                            <th>{{trans('profile.Zip_code')}} </th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Visited as $rows)
                            <?php
                            $state=null;$city=null;
                            $country=App\Country::where('country_id',$rows->country_id)->first();
                            $state=$country->state()->where('state_id',$rows->state_id)->where('language_code',Auth::user()->langauge)->first();
                          //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }
                            ?>
                            <tr>
                                <td> {{$rows->name}}</td>
                                <td>{{$rows->address}} {{$state}} {{$city}} </td>
                                <td>{{$country->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td align="right">
                                    {{--<a href="{{url('/member/visited/change-language/'.$rows->visited_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>--}}
                                    <a href="{{url('/member/visited/edit/'.$rows->visited_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/member/visited/delete/'.$rows->visited_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
         @endif
         <div class="box-footer">
             <hr>
         </div>
         <div class="form-group" align="right">
             <a href="{{url('member/place')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
             {{--<a type="button" href="{{url('/member/visited')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>--}}
         </div>

                </div>

                </div>

                </div>
    </section>
@endsection
