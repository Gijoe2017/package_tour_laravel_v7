@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">{{trans('profile.occupation_information')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        {{--<span class="step-heading">Occupation Informaltion: </span>--}}
                        {{--<span class="step-number">Step 4 / 6</span>--}}
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Member\OccupationController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="occupation_group_id" value="{{$Occupation->occupation_id}}">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name" class="form-label required">{{trans('profile.occupation')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <select class="form-control select2" name="occupation_id" id="occupation_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach(\App\models\Occupation::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->occupation_id}}" {{$rows->occupation_id==$Occupation->occupation_id?'selected':''}} >{{$rows->occupation}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name" class="form-label required">{{trans('profile.occupation_level')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <select class="form-control select2" name="occupation_level_id" id="occupation_level_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach(\App\models\OccupationLevel::active()->get() as $rows)
                                                    <option value="{{$rows->occupation_level_id}}" {{$rows->occupation_level_id==$Occupation->occupation_level_id?'selected':''}} >{{$rows->occupation_level}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('profile.work_date_start')}} <span class="text-red">*</span></label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="date_work" value="{{$Occupation->date_work}}" id="datepicker" required>
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('profile.work_date_end')}}<span class="text-red">*</span></label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="date_work_end" value="{{$Occupation->date_work_end}}" id="datepicker1" required>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name" class="form-label required">{{trans('profile.business_name')}}<span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="name" id="name" value="{{isset($Occupation->name)?$Occupation->name:''}}" required />
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="alert alert-info"><i class="fa fa-map-o"></i> {{trans('profile.Address')}}</h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="address" id="address" value="{{isset($Occupation->address)?$Occupation->address:''}}" required/>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>

                                        <select class="form-control select2" name="country_id" id="country_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$Occupation->country_id?'selected':''}} >{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 box-state">
                                    <div class="form-group">
                                        <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                        <select  class="form-control select2" name="state_id" id="state_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($state as $rows)
                                            <option value="{{$rows->state_id}}" {{$rows->state_id==$Occupation->state_id?'selected':''}}>{{$rows->state}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-4 box-state city_id">
                                    <div class="form-group">
                                        <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                        <select class="form-control select2" name="city_id" id="city_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($city as $rows)
                                            <option value="{{$rows->city_id}}" {{$rows->city_id==$Occupation->city_id?'selected':''}}>{{$rows->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 box-state city_sub1_id">
                                    <div class="form-group">
                                        <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                        <select class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($city_sub as $rows)
                                            <option value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$Occupation->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 city_sub1_id">
                                    <div class="form-group">
                                        <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                        <input class="form-control" type="text" name="zip_code" id="zip_code"  value="{{$Occupation->zip_code}}" readonly />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="alert alert-warning"><i class="fa fa-phone"></i> {{trans('profile.Phone')}}</h3>
                                </div>
                                <div class="col-md-2">
                                    <label for="phone_country_id">{{trans('profile.Country')}}</label><BR>
                                    <select class="form-control select2" name="phone_country_id" id="phone_country_id" style="width: 100%">
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="phone_country_code" >{{trans('profile.phone_code')}}</label><BR>
                                    <select class="form-control select2" name="phone_country_code" id="phone_country_code" style="width: 100%">
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\models\PhoneCode::all() as $rows)
                                            <option value="{{$rows->phone_code}}">{{$rows->country}} - {{$rows->phone_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone_type" >{{trans('profile.phone_type')}}</label><BR>
                                        <select class="form-control select2" name="phone_type" id="phone_type" style="width: 100%">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            <option value="moblie">{{trans('profile.mobile')}}</option>
                                            <option value="phone">{{trans('profile.phone')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="phone_number">{{trans('profile.phone_name')}}</label>
                                    <div class="input-group">
                                        <input type="text" name="occupation_phone_number" id="occupation_phone_number" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                        <div class="input-group-btn">
                                            <button id="add-phone-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i>{{trans('profile.add_new')}}</button>
                                            <button id="occupation-add-phone" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                </div>

                                <div id="show-phone-detail" style="display:{{count($Phones)==0?'none':''}}" class="col-md-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>{{trans('profile.Country')}}</th>
                                            <th>{{trans('profile.phone_code')}}</th>
                                            <th>{{trans('profile.phone_type')}}</th>
                                            <th>{{trans('profile.phone_name')}}</th>
                                            <th>{{trans('profile.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($Phones as $phone)
                                            <tr>
                                                <td>{{$phone->country}}</td>
                                                <td>{{$phone->phone_country_code}}</td>
                                                <td>{{$phone->phone_type}}</td>
                                                <td>{{$phone->phone_number}}</td>
                                                <td align='center'><button class="btn btn-danger btn-sm occupation-phone-delete"  data-id="{{$phone->id}}"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" align="right">
                                        <a href="{{url('member/occupation')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
@endsection