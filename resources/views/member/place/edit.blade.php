@extends('layouts.member.layout_master_new')
@section('content')
    <link rel="stylesheet" href="{{asset('themes/defaults/assets/select1/jquery.typeahead.css')}}">
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('themes/defaults/assets/select1/jquery.typeahead.js')}}"></script>

    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">Address stay place <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-heading">Address stay place Informaltion: </span>
                        {{--<span class="step-number">Step 4 / 6</span>--}}
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Member\PlaceController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="place_stay_id" value="{{$Place->place_stay_id}}">
                        <div class="col-md-12">

                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.place_name')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="name" id="name" value="{{isset($Place->name)?$Place->name:''}}" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="address" id="address" value="{{isset($Place->address)?$Place->address:''}}" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>--}}

                                        {{--<select class="form-control select2" name="country_id" id="country_id" required>--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->country_id}}" {{$rows->country_id==$Place->country_id?'selected':''}} >{{$rows->country}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 box-state">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="state_id" class="form-label required">{{trans('profile.State')}}</label>--}}
                                        {{--<select  class="form-control select2" name="state_id" id="state_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($state as $rows)--}}
                                            {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$Place->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 box-state city_id">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="city_id" class="form-label required">{{trans('profile.City')}}</label>--}}
                                        {{--<select class="form-control select2" name="city_id" id="city_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($city as $rows)--}}
                                            {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$Place->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 box-state city_sub1_id">--}}
                                    {{--<div class="form-group">--}}

                                        {{--<label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>--}}
                                        {{--<select class="form-control select2" name="city_sub1_id" id="city_sub1_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($city_sub as $rows)--}}
                                            {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$Place->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 city_sub1_id">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>--}}
                                        {{--<input class="form-control" type="text" name="zip_code" id="zip_code" value="{{$Place->zip_code}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                            {{--</div>--}}


                            <div class="search-location">
                                <div class="col-md-12">
                                    <label for="program" class="form-label required">{{trans('profile.place_stay')}} </label>
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
                                            <div class="typeahead__query">
                                                <input class="js-typeahead form-control"
                                                       name="q"
                                                       type="search"
                                                       value="{{$Place->name}}"
                                                       autofocus
                                                       autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="display: none" class="new-location">

                                <div class="col-md-6">
                                    <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                        {{ Form::label('name', trans('profile.place_stay'), ['class' => 'control-label']) }}
                                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                    {{ $errors->first('name') }}
                                                    </span>
                                        @endif
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                        {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
										                {{ $errors->first('category_id') }}
									            </span>
                                        @endif
                                    </fieldset>
                                </div>
                            </div>
                            <div style="display: none" class="row new-location">
                                <div class="col-md-12">
                                    <br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                            <input class="form-control" type="text" name="address" id="address"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>

                                            <select style="width: 100%" class="form-control select2" name="country_id" id="country_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state">
                                        <div class="form-group">
                                            <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="state_id" id="state_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_id">
                                        <div class="form-group">
                                            <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_id" id="city_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_sub1_id">
                                        <div class="form-group">
                                            <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 city_sub1_id">
                                        <div class="form-group">
                                            <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                            <input class="form-control" readonly type="text" name="zip_code" id="zip_code"  />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="alert alert-info"><i class="fa fa-phone"></i> Phone</h3>

                                </div>
                                <div class="col-md-2">
                                    <label for="phone_country_id">{{trans('profile.Country')}}</label><BR>
                                    <select class="form-control select2" name="phone_country_id" id="phone_country_id" style="width: 100%">
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="phone_country_code" >{{trans('profile.phone_code')}}</label><BR>
                                    <select class="form-control select2" name="phone_country_code" id="phone_country_code" style="width: 100%">
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\models\PhoneCode::all() as $rows)
                                            <option value="{{$rows->phone_code}}">{{$rows->country}} - {{$rows->phone_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone_type" >{{trans('profile.phone_type')}}</label><BR>
                                        <select class="form-control select2" name="phone_type" id="phone_type" style="width: 100%">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            <option value="moblie">{{trans('profile.mobile')}}</option>
                                            <option value="phone">{{trans('profile.phone')}}</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <label for="phone_number">{{trans('profile.phone_name')}}</label>
                                    <div class="input-group">
                                        <input type="text" name="place_phone_number" id="place_phone_number" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                        <div class="input-group-btn">
                                            <button id="add-phone-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                            <button id="place-add-phone" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr>

                                </div>

                                <div id="show-phone-detail" style="display:{{count($Phones)==0?'none':''}}" class="col-md-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>{{trans('profile.Country')}}</th>
                                            <th>{{trans('profile.phone_code')}}</th>
                                            <th>{{trans('profile.phone_type')}}</th>
                                            <th>{{trans('profile.phone_name')}}</th>
                                            <th>{{trans('profile.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($Phones as $phone)
                                            <tr>
                                                <td>{{$phone->country}}</td>
                                                <td>{{$phone->phone_country_code}}</td>
                                                <td>{{$phone->phone_type}}</td>
                                                <td>{{$phone->phone_number}}</td>
                                                <td align='center'><button class="btn btn-danger btn-sm place-phone-delete"  data-id="{{$phone->id}}"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" align="right">
                                        <a href="{{url('member/place')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    <script>
        function SP_source() {
            return "{{url('/')}}/";
        }
    </script>
    <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>
@endsection