@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.accommodation_information')}}  <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/member/place/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right">
                        {{--<span class="step-heading">Address stay place Informaltion: </span>--}}
                        <span class="step-number">{{trans('profile.step')}} 9 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.accommodation_name')}}</th>
                            <th>{{trans('profile.Address')}}</th>
                            <th>{{trans('profile.Country')}}</th>
                            <th>{{trans('profile.Zip_code')}}</th>
                            <th>{{trans('profile.Phone')}}</th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Place as $rows)
                            <?php
                                    $state=null;$city=null;
                                    $country=App\Country::where('country_id',$rows->country_id)->first();

                            $state=$country->state()->where('state_id',$rows->state_id)->where('language_code',Auth::user()->langauge)->first();
                          //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }

                            ?>

                            <tr>
                                <td> {{$rows->name}}</td>
                                <td>{{$rows->address}} {{$state}} {{$city}} </td>
                                <td>{{$country->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td>
                                    <?php
                                    $check=DB::table('users_passport_address_place_stay_phone')->where('passport_id',$rows->passport_id)->get();
                                    ?>
                                    @foreach($check as $phone)
                                        {{$phone->phone_country_code}} {{$phone->phone_number}} {{$phone->phone_type}} <BR>
                                    @endforeach
                                </td>
                                <td align="right">
                                    {{--<a href="{{url('/member/place/change-language/'.$rows->id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>--}}
                                    <a href="{{url('/member/place/edit/'.$rows->place_stay_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/member/place/delete/'.$rows->place_stay_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="box-footer">
                        <hr>
                    </div>
                    <div class="form-group" align="right">
                        <a href="{{url('member/occupation')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                        <a type="button" href="{{url('/member/visited')}}" class="btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>
                    </div>
                </div>

                </div>

                </div>
    </section>
@endsection
