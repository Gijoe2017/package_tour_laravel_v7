@extends('layouts.package.master')
@section('program-highlight')
    <style type="text/css">
        .badge-new2 {
            display: block;
            z-index: 10;
            padding: 2px 7px;
            font-size: 18px;
            background-color: #ef5f5f;
            color: #fff;
            border-radius: 4px;
        }

        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
        }
        .panel-body {
            padding: 15px;
        }
        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
        }
        .panel-heading > .dropdown .dropdown-toggle {
            color: inherit;
        }
        .panel-title {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 16px;
            color: inherit;
        }
        .panel-title > a,
        .panel-title > small,
        .panel-title > .small,
        .panel-title > small > a,
        .panel-title > .small > a {
            color: inherit;
        }
        .panel-footer {
            padding: 10px 15px;
            background-color: #f5f5f5;
            border-top: 1px solid #ddd;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }
        .panel > .list-group,
        .panel > .panel-collapse > .list-group {
            margin-bottom: 0;
        }
        .panel > .list-group .list-group-item,
        .panel > .panel-collapse > .list-group .list-group-item {
            border-width: 1px 0;
            border-radius: 0;
        }
        .panel > .list-group:first-child .list-group-item:first-child,
        .panel > .panel-collapse > .list-group:first-child .list-group-item:first-child {
            border-top: 0;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
        }
        .panel > .list-group:last-child .list-group-item:last-child,
        .panel > .panel-collapse > .list-group:last-child .list-group-item:last-child {
            border-bottom: 0;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }
        .panel-heading + .list-group .list-group-item:first-child {
            border-top-width: 0;
        }
        .list-group + .panel-footer {
            border-top-width: 0;
        }
        .panel > .table,
        .panel > .table-responsive > .table,
        .panel > .panel-collapse > .table {
            margin-bottom: 0;
        }
        .panel > .table caption,
        .panel > .table-responsive > .table caption,
        .panel > .panel-collapse > .table caption {
            padding-left: 15px;
            padding-right: 15px;
        }
        .panel > .table:first-child,
        .panel > .table-responsive:first-child > .table:first-child {
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
        }
        .panel > .table:first-child > thead:first-child > tr:first-child,
        .panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child,
        .panel > .table:first-child > tbody:first-child > tr:first-child,
        .panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child {
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }
        .panel > .table:first-child > thead:first-child > tr:first-child td:first-child,
        .panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:first-child,
        .panel > .table:first-child > tbody:first-child > tr:first-child td:first-child,
        .panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:first-child,
        .panel > .table:first-child > thead:first-child > tr:first-child th:first-child,
        .panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:first-child,
        .panel > .table:first-child > tbody:first-child > tr:first-child th:first-child,
        .panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:first-child {
            border-top-left-radius: 3px;
        }
        .panel > .table:first-child > thead:first-child > tr:first-child td:last-child,
        .panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:last-child,
        .panel > .table:first-child > tbody:first-child > tr:first-child td:last-child,
        .panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:last-child,
        .panel > .table:first-child > thead:first-child > tr:first-child th:last-child,
        .panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:last-child,
        .panel > .table:first-child > tbody:first-child > tr:first-child th:last-child,
        .panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:last-child {
            border-top-right-radius: 3px;
        }
        .panel > .table:last-child,
        .panel > .table-responsive:last-child > .table:last-child {
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }
        .panel > .table:last-child > tbody:last-child > tr:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child,
        .panel > .table:last-child > tfoot:last-child > tr:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child {
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
        }
        .panel > .table:last-child > tbody:last-child > tr:last-child td:first-child,
        .panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:first-child,
        .panel > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
        .panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
        .panel > .table:last-child > tbody:last-child > tr:last-child th:first-child,
        .panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:first-child,
        .panel > .table:last-child > tfoot:last-child > tr:last-child th:first-child,
        .panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:first-child {
            border-bottom-left-radius: 3px;
        }
        .panel > .table:last-child > tbody:last-child > tr:last-child td:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:last-child,
        .panel > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
        .panel > .table:last-child > tbody:last-child > tr:last-child th:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:last-child,
        .panel > .table:last-child > tfoot:last-child > tr:last-child th:last-child,
        .panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:last-child {
            border-bottom-right-radius: 3px;
        }
        .panel > .panel-body + .table,
        .panel > .panel-body + .table-responsive,
        .panel > .table + .panel-body,
        .panel > .table-responsive + .panel-body {
            border-top: 1px solid #ddd;
        }
        .panel > .table > tbody:first-child > tr:first-child th,
        .panel > .table > tbody:first-child > tr:first-child td {
            border-top: 0;
        }
        .panel > .table-bordered,
        .panel > .table-responsive > .table-bordered {
            border: 0;
        }
        .panel > .table-bordered > thead > tr > th:first-child,
        .panel > .table-responsive > .table-bordered > thead > tr > th:first-child,
        .panel > .table-bordered > tbody > tr > th:first-child,
        .panel > .table-responsive > .table-bordered > tbody > tr > th:first-child,
        .panel > .table-bordered > tfoot > tr > th:first-child,
        .panel > .table-responsive > .table-bordered > tfoot > tr > th:first-child,
        .panel > .table-bordered > thead > tr > td:first-child,
        .panel > .table-responsive > .table-bordered > thead > tr > td:first-child,
        .panel > .table-bordered > tbody > tr > td:first-child,
        .panel > .table-responsive > .table-bordered > tbody > tr > td:first-child,
        .panel > .table-bordered > tfoot > tr > td:first-child,
        .panel > .table-responsive > .table-bordered > tfoot > tr > td:first-child {
            border-left: 0;
        }
        .panel > .table-bordered > thead > tr > th:last-child,
        .panel > .table-responsive > .table-bordered > thead > tr > th:last-child,
        .panel > .table-bordered > tbody > tr > th:last-child,
        .panel > .table-responsive > .table-bordered > tbody > tr > th:last-child,
        .panel > .table-bordered > tfoot > tr > th:last-child,
        .panel > .table-responsive > .table-bordered > tfoot > tr > th:last-child,
        .panel > .table-bordered > thead > tr > td:last-child,
        .panel > .table-responsive > .table-bordered > thead > tr > td:last-child,
        .panel > .table-bordered > tbody > tr > td:last-child,
        .panel > .table-responsive > .table-bordered > tbody > tr > td:last-child,
        .panel > .table-bordered > tfoot > tr > td:last-child,
        .panel > .table-responsive > .table-bordered > tfoot > tr > td:last-child {
            border-right: 0;
        }
        .panel > .table-bordered > thead > tr:first-child > td,
        .panel > .table-responsive > .table-bordered > thead > tr:first-child > td,
        .panel > .table-bordered > tbody > tr:first-child > td,
        .panel > .table-responsive > .table-bordered > tbody > tr:first-child > td,
        .panel > .table-bordered > thead > tr:first-child > th,
        .panel > .table-responsive > .table-bordered > thead > tr:first-child > th,
        .panel > .table-bordered > tbody > tr:first-child > th,
        .panel > .table-responsive > .table-bordered > tbody > tr:first-child > th {
            border-bottom: 0;
        }
        .panel > .table-bordered > tbody > tr:last-child > td,
        .panel > .table-responsive > .table-bordered > tbody > tr:last-child > td,
        .panel > .table-bordered > tfoot > tr:last-child > td,
        .panel > .table-responsive > .table-bordered > tfoot > tr:last-child > td,
        .panel > .table-bordered > tbody > tr:last-child > th,
        .panel > .table-responsive > .table-bordered > tbody > tr:last-child > th,
        .panel > .table-bordered > tfoot > tr:last-child > th,
        .panel > .table-responsive > .table-bordered > tfoot > tr:last-child > th {
            border-bottom: 0;
        }
        .panel > .table-responsive {
            border: 0;
            margin-bottom: 0;
        }
        .panel-group {
            margin-bottom: 20px;
        }
        .panel-group .panel {
            margin-bottom: 0;
            border-radius: 4px;
        }
        .panel-group .panel + .panel {
            margin-top: 5px;
        }
        .panel-group .panel-heading {
            border-bottom: 0;
        }
        .panel-group .panel-heading + .panel-collapse > .panel-body,
        .panel-group .panel-heading + .panel-collapse > .list-group {
            border-top: 1px solid #ddd;
        }
        .panel-group .panel-footer {
            border-top: 0;
        }
        .panel-group .panel-footer + .panel-collapse .panel-body {
            border-bottom: 1px solid #ddd;
        }
        .panel-default {
            border-color: #ddd;
        }
        .panel-default > .panel-heading {
            color: #333333;
            background-color: #f5f5f5;
            border-color: #ddd;
        }
        .panel-default > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #ddd;
        }
        .panel-default > .panel-heading .badge {
            color: #f5f5f5;
            background-color: #333333;
        }
        .panel-default > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #ddd;
        }
        .panel-primary {
            border-color: #337ab7;
        }
        .panel-primary > .panel-heading {
            color: #fff;
            background-color: #337ab7;
            border-color: #337ab7;
        }
        .panel-primary > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #337ab7;
        }
        .panel-primary > .panel-heading .badge {
            color: #337ab7;
            background-color: #fff;
        }
        .panel-primary > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #337ab7;
        }
        .panel-success {
            border-color: #d6e9c6;
        }
        .panel-success > .panel-heading {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
        .panel-success > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #d6e9c6;
        }
        .panel-success > .panel-heading .badge {
            color: #dff0d8;
            background-color: #3c763d;
        }
        .panel-success > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #d6e9c6;
        }
        .panel-info {
            border-color: #bce8f1;
        }
        .panel-info > .panel-heading {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        .panel-info > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #bce8f1;
        }
        .panel-info > .panel-heading .badge {
            color: #d9edf7;
            background-color: #31708f;
        }
        .panel-info > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #bce8f1;
        }
        .panel-warning {
            border-color: #faebcc;
        }
        .panel-warning > .panel-heading {
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
        }
        .panel-warning > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #faebcc;
        }
        .panel-warning > .panel-heading .badge {
            color: #fcf8e3;
            background-color: #8a6d3b;
        }
        .panel-warning > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #faebcc;
        }
        .panel-danger {
            border-color: #ebccd1;
        }
        .panel-danger > .panel-heading {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }
        .panel-danger > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #ebccd1;
        }
        .panel-danger > .panel-heading .badge {
            color: #f2dede;
            background-color: #a94442;
        }
        .panel-danger > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #ebccd1;
        }
        .panel-post .panel-heading .post-author .user-avatar {
            width: 40px;
            height: 40px;
            float: left;
            margin-right: 10px;
        }
        .panel-post .panel-heading .post-author .user-avatar img {
            width: 100%;
            height: 100%;
            border-radius: 4px;
        }
        .post-image-holder.single-image {
            max-height: inherit !important;
            height: auto;
        }
        .post-image-holder.single-image a {
            max-height: inherit;
            height: auto;
            width: 100%;
        }
        .post-image-holder.single-image a img {
            max-height: inherit!important;
            height: auto;
            width: 100%;
        }
    </style>
        <!-- ========================= SECTION CONTENT ========================= -->
        <section class="section-content bg padding-y-sm">
            <div class="container">

                <nav class="mb-3">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home/package/all')}}">{{trans('common.package')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{url('home/package/category/'.$Category->TourCategoryID)}}">{{$Category->TourCategoryName}}</a></li>
                        @if($SubCateroty)
                        <li class="breadcrumb-item"><a href="{{url('home/package/sub_cate/'.$SubCateroty->groupID)}}">{{$SubCateroty->TourCategorySub1Name}}</a></li>
                        @endif
                        <li class="breadcrumb-item active" aria-current="page">{{Session::get('pname')}}</li>
                    </ol>
                </nav>

                <div class="row">
                    <div class="col-xl-10 col-md-9 col-sm-12">
                        <main class="card">
                            <div class="row no-gutters">
                                <aside class="col-sm-6 border-right">
                                    <article class="gallery-wrap">
                                        <div class="img-big-wrap">
                                            <div class="text-center">
                                                    {{--<img src="{{asset('images/package-tour/mid/'.$PackageTourOne->Image)}}">--}}
                                                <a target="_blank" href="{{url('package/tour/mid/'.$PackageTourOne->Image)}}">
                                                    <img src="{{url('package/tour/mid/'.$PackageTourOne->Image)}}">
                                                </a>
                                            </div>
                                        </div> <!-- slider-product.// -->
                                        {{--<div class="img-small-wrap">--}}
                                            {{--<div class="item-gallery"> <img src="images/banners/slide1-2.jpg"></div>--}}
                                            {{--<div class="item-gallery"> <img src="images/banners/slide1-3.jpg"></div>--}}
                                            {{--<div class="item-gallery"> <img src="images/banners/slide1-2.jpg"></div>--}}
                                            {{--<div class="item-gallery"> <img src="images/banners/slide1-5.jpg"></div>--}}
                                        {{--</div> <!-- slider-nav.// -->--}}
                                    </article> <!-- gallery-wrap .end// -->
                                </aside>
                                <?php
                                $current=\App\Currency::where('currency_code',$PackageTourOne->packageCurrency)->first();

                                $Detail=DB::table('package_details')
                                    ->where('packageID',$PackageTourOne->packageID)
                                    ->where('packageDateStart','>',date('Y-m-d'))
                                    ->where('status','Y')
                                    ->orderby('packageDateStart','asc')
                                    ->first();

                                $Details=DB::table('package_details')
                                        ->where('packageID',$PackageTourOne->packageID)
                                        ->where('packageDateStart','>',date('Y-m-d'))
                                        ->where('status','Y')
                                        ->orderby('packageDateStart','asc')
                                        ->get();

                                $Price=DB::table('package_details_sub')
                                        ->where('packageID',$PackageTourOne->packageID)
                                        ->where('status','Y')
                                        ->orderby('PriceSale','desc')
                                        ->first();
                                // dd($Price);
                                $Price_now=0;
                                $Price_up=0;
                                $Price_down=0;
                                //****************** Update Status if promotion finish **********************//
                                DB::table('package_promotion')
                                        ->where('promotion_date_end','<',date('Y-m-d'))
                                        ->where('promotion_status','Y')
                                        ->where('promotion_operator','Between')
                                        ->update(['promotion_status'=>'N']);
                                //****************** Update Status if promotion finish **********************//

                                $data_target=null;
                                $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                        //->where('promotion_date_start','>',date('Y-m-d'))
                                        ->orderby('promotion_date_start','asc')
                                        ->first();
                                $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                                // dd($pricePro);
                                $promotion_title=null;$every_booking=0;

                                if($promotion){
                                    $every_booking=$promotion->every_booking;
                                    $promotion_title='Between';
                                    if($promotion->promotion_operator=='Between'){
                                        if($promotion->promotion_date_start<=date('Y-m-d') && $promotion->promotion_date_end>=date('Y-m-d')){

                                            if($promotion->promotion_operator2=='up'){
                                            if($promotion->promotion_unit=='%'){
                                                if($promotion->promotion_value>0){
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_up=$rowPrice->PriceSale*$promotion->promotion_value/100;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale+$Price_up]);
                                                    }
                                                }
                                            }else{
                                                foreach ($pricePro as $rowPrice){
                                                    $Price_up=$promotion->promotion_value;
                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale+$Price_up]);
                                                }

                                            }

                                        }else{ // promotion down
                                            if($promotion->promotion_unit=='%'){
                                                if($promotion->promotion_value>0){
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_down=$Price->PriceSale*$promotion->promotion_value/100;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale-$Price_down]);
                                                    }
                                                }
                                            }else{
                                                foreach ($pricePro as $rowPrice){
                                                    $Price_down=$promotion->promotion_value;
                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->PriceSale-$Price_down]);
                                                }
                                            }

                                        }
                                            }
                                        $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                    }else{
                                        $promotion_title='Mod';
                                       // dd($rows->packageID.' '.$Price->packageDescID);
                                        $booking=DB::table('package_bookings as a')
                                                ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                                                ->where('b.package_id',$PackageTourOne->packageID)
                                                ->where('b.package_detail_id',$Price->packageDescID)
                                                ->sum('b.number_of_person');
                                        // dd($booking);
                                        if($booking>0){ // Check booking
                                            if($booking % $promotion->every_booking==1){ // every booking
                                                if($booking>$promotion->every_booking){
                                                    if($promotion->promotion_operator2=='up'){
                                                        if($promotion->promotion_unit=='%'){
                                                            if($promotion->promotion_value>0){
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                }
                                                            }
                                                        }else{
                                                            foreach ($pricePro as $rowPrice){
                                                                $Price_up=$promotion->promotion_value;
                                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                            }
                                                        }
                                                        /// $Price_by_promotion+=$Price_up;
                                                    }else{
                                                        if($promotion->promotion_unit=='%'){
                                                            if($promotion->promotion_value>0){
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                                }
                                                            }
                                                        }else{
                                                            foreach ($pricePro as $rowPrice){
                                                                $Price_down=$promotion->promotion_value;
                                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                        }
//                                                    dd($Price_now);
                                    }
                                }

                                //   dd($data_target);

                                $Price=DB::table('package_details_sub')
                                        ->where('packageID',$PackageTourOne->packageID)
                                        ->where('status','Y')
                                        ->orderby('PriceSale','desc')
                                        ->first();
                                   // dd($Price);
                                ?>

                                <aside class="col-sm-6">
                                    <article class="card-body">
                                        <!-- short-info-wrap -->
                                        <div class="info-wrap-2">
                                            <h3 class="title mb-3">{{$PackageTourOne->packageName}}</h3>
                                        </div>

                                        <div class="price-wrap h5">
                                            <var class="price h3 text-warning">

                                                <span class="num">{{$current->currency_symbol}}{{number_format($Price->Price_by_promotion)}}</span>
                                            </var>
                                            <span>/ {{trans('common.per_person')}}</span>
                                            @if($Price->Price_by_promotion!=$Price->PriceSale)
                                            <br>
                                            <del class="price-old">{{$current->currency_symbol.number_format($Price->PriceSale)}}</del>
                                            @if($promotion)
                                            <span class="price-save">
                                                {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                @if($promotion->promotion_operator2=='up')
                                                    {{$current->currency_symbol.number_format($Price->Price_by_promotion-$Price->PriceSale)}}
                                                @else
                                                    {{$current->currency_symbol.number_format($Price->PriceSale-$Price->Price_by_promotion)}}
                                                @endif
                                            </span>
                                            @endif
                                            @endif
                                        </div> <!-- price-wrap.// -->
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>{{trans('common.travel')}}</th>
                                                <th>{{trans('common.price')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @if(Auth::check())
                                                    <?php $url_link=Auth::user()->name.'/details/'.$PackageTourOne->packageID;?>
                                                @else
                                                    <?php $url_link='home/details/'.$PackageTourOne->packageID;?>
                                                @endif
                                                <?php

                                                $st=explode('-',$Detail->packageDateStart);
                                                $end=explode('-',$Detail->packageDateEnd);

                                                if($st[1]==$end[1]){
                                                    $date=\Date::parse($Detail->packageDateStart);
                                                    $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                    // dd($end[0]);
                                                }else{
                                                    $date=\Date::parse($Detail->packageDateStart);
                                                    $date1=\Date::parse($Detail->packageDateEnd);
                                                    $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                }

                                                $Category=DB::table('package_details_sub')
                                                        ->where('packageID',$PackageTourOne->packageID)
                                                        ->where('packageDescID',$Detail->packageDescID)
                                                        ->get();

                                                $promotion=\App\PackagePromotion::where('packageDescID',$Detail->packageDescID)->active()
                                                        ->orderby('promotion_date_start','asc')
                                                        ->first();

                                                $data_target=null;
                                                if($promotion && $promotion->promotion_operator!='Mod'){
                                                    $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                                }

                                                $booking=DB::table('package_booking_details')
                                                        ->where('package_id',$PackageTourOne->packageID)
                                                        ->where('package_detail_id',$Detail->packageDescID)
                                                        ->sum('number_of_person');

                                                $person_booking=$Detail->NumberOfPeople;

                                                if($booking){
                                                    $person_booking=$Detail->NumberOfPeople-$booking;
                                                }
                                                ?>

                                                <form method="post" action="{{action('Package\OrderTourController@store_order')}}">
                                                    <input type="hidden" name="id" value="{{$Detail->packageDescID}}">
                                                    {{csrf_field()}}
                                                    <tr>
                                                    <td class="date-text">{{$package_date}}<br>
                                                        <var class="price">
                                                            <select class="form-control form-control-sm" name="tour_type" id="tour_type" required style="width:90%;">
                                                                <span class="form-check-label"></span>
                                                                <option value="" > {{trans('common.category')}}</option>
                                                                @foreach($Category as $cate)
                                                                    <option value="{{$cate->psub_id}}"> {{$cate->TourType}}</option>
                                                                @endforeach
                                                                {{--<option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>--}}
                                                            </select>
                                                        </var>
                                                        <var class="price">
                                                            <select class="form-control form-control-sm" name="number_of_person" id="number_of_person" required style="width:90%;">
                                                                <span class="form-check-label"></span>
                                                                <option value=""> {{trans('common.amount')}}</option>
                                                                @for($i=1;$i<=10;$i++)
                                                                <option value="{{$i}}"> {{$i}} {{trans('common.person')}}</option>
                                                                @endfor
                                                            </select>
                                                        </var>
                                                    </td>
                                                    <td>
                                                        @if($Price->Price_by_promotion!=$Price->PriceSale)
                                                            <del>{{$current->currency_symbol.number_format($Price->PriceSale)}}</del> - {{$current->currency_symbol.number_format($Price->Price_by_promotion)}} <br>
                                                        @endif
                                                        <span class="num h5 text-warning">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}} </span>
                                                        @if($Price->Price_by_promotion!=$Price->PriceSale)
                                                            @if($promotion)
                                                                <span class="text-danger">
                                                                {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                @if($promotion->promotion_operator2=='up')
                                                                {{$current->currency_symbol.number_format($Price->Price_by_promotion-$Price->PriceSale)}}
                                                                    @else
                                                                        {{$current->currency_symbol.number_format($Price->PriceSale-$Price->Price_by_promotion)}}
                                                                    @endif
                                                        </span>
                                                                @endif
                                                            <br>
                                                        @endif

                                                        @if($data_target != null)
                                                            <br>
                                                            <span class="price-save-date">
                                                                  <span id="clockdiv{{$Detail->packageDescID}}">({{trans('package.this_price')}}
                                                                     <span class="days"></span> {{trans('common.day')}}
                                                                     <span class="hours"></span>:
                                                                     <span class="minutes"></span>:
                                                                     <span class="seconds"></span>, {{$Detail->NumberOfPeople}} {{trans('package.the_last_place_only')}})
                                                                  </span>
                                                            </span>
                                                            <script language="JavaScript">
                                                                initializeClock('clockdiv{{$Detail->packageDescID}}', new Date('{{$data_target}}'));
                                                            </script>
                                                        @else
                                                             <BR>
                                                             <span class="price-save-date">
                                                                 ({{trans('package.this_price_is_only')}} {{$person_booking}} {{trans('package.the_last_place_only')}})
                                                             </span>
                                                        @endif
                                                        <br>
                                                        <button type="submit" name="submit_cart" value="Y" class="btn btn-sm btn-outline-primary">
                                                            <i class="fas fa-shopping-cart"></i> {{trans('common.addtocart')}}
                                                        </button>
                                                        <button type="submit" name="submit_order" value="Y" class="btn btn-sm btn-primary">
                                                            {{trans('common.booking')}}
                                                        </button>
                                                            {{--<a href="{{url('/booking/'.$Detail->packageDescID)}}" class="btn btn-sm btn-primary"> {{trans('common.booking')}} </a>--}}
                                                    </td>
                                                </tr>
                                                </form>
                                            </tbody>
                                        </table>


                                        <dl>
                                            <dt>Description</dt>
                                            <dd><p>{!! $PackageTourOne->packageHighlight !!}</p></dd>
                                        </dl>

                                        <div class="rating-wrap">
                                            <a href="detail.html" class="#nav-tab-review">
                                                <ul class="rating-stars">
                                                    <li style="width:80%" class="stars-active">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                </ul>
                                                <div class="label-rating">132 {{trans('common.review')}}</div>
                                            </a>

                                            <div class="label-rating">
                                                <a href="detail.html">154 {{trans('common.booking')}} </a>
                                            </div>

                                            @if(Auth::check())
                                                <a class="add-wishlist" href="#" data-id="{{$Detail->packageDescID}}"><i class="fa fa-heart"></i> Add to wishlist</a>
                                            @else
                                                <a class="add-wishlist" href="{{url('ajax/login')}}" data-toggle="modal" data-target="#popupForm" ><i class="fa fa-heart"></i> Add to wishlist</a>
                                            @endif

                                        </div> <!-- rating-wrap.// -->
                                        <hr>
                                        <a href="#" class="btn  btn-warning"> <i class="fa fa-envelope"></i> Contact Supplier </a>
                                        <a href="#" class="btn  btn-outline-warning"> Start Order </a>
                                        <hr>
                                        <!-- short-info-wrap .// -->
                                    </article> <!-- card-body.// -->
                                </aside> <!-- col.// -->
                                <aside class="col-sm-12">
                                    <article class="card-body">
                                        <dl class="row">
                                            <div class="col-sm-12"><dt>ช่วงเวลาอืน ๆ</dt>
                                                <div class=" table-wrapper-scroll-y scrollbar" id="style-1">
                                                    <table class="table table-hover fixed_header">
                                                        <thead>
                                                        <tr>
                                                            <th>{{trans('common.travel')}}</th>
                                                            <th>{{trans('common.price')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($Details as $detail)
                                                            @if(Auth::check())
                                                                <?php $url_link=Auth::user()->name.'/details/'.$detail->packageID;?>
                                                            @else
                                                                <?php $url_link='home/details/'.$detail->packageID;?>
                                                            @endif
                                                            <?php
                                                            $st=explode('-',$detail->packageDateStart);
                                                            $end=explode('-',$detail->packageDateEnd);

                                                            if($st[1]==$end[1]){
                                                                $date=\Date::parse($detail->packageDateStart);
                                                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                // dd($end[0]);
                                                            }else{
                                                                $date=\Date::parse($detail->packageDateStart);
                                                                $date1=\Date::parse($detail->packageDateEnd);
                                                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                            }

                                                            $Category=DB::table('package_details_sub')
                                                                    ->where('packageID',$PackageTourOne->packageID)
                                                                    ->where('packageDescID',$detail->packageDescID)
                                                                    ->get();

                                                            $promotion=\App\PackagePromotion::where('packageDescID',$detail->packageDescID)->active()
                                                                    ->orderby('promotion_date_start','asc')
                                                                    ->first();


                                                            $data_target=null;
                                                            if($promotion && $promotion->promotion_operator!='Mod'){
                                                                $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                                            }

                                                            $booking=DB::table('package_booking_details')
                                                                    ->where('package_id',$PackageTourOne->packageID)
                                                                    ->where('package_detail_id',$detail->packageDescID)
                                                                    ->sum('number_of_person');

                                                            $person_booking=$detail->NumberOfPeople;

                                                            if($booking){
                                                                $person_booking=$detail->NumberOfPeople-$booking;
                                                            }
                                                            ?>

                                                            <form class="form-booking" method="post" action="{{action('Package\OrderTourController@store_order')}}">
                                                                <input type="hidden" name="id" value="{{$detail->packageDescID}}">
                                                                {{csrf_field()}}
                                                                <tr>
                                                                <td class="date-text">{{$package_date}}<br>
                                                                    <var class="price">
                                                                        <select class="form-control form-control-sm" required id="tour_type" name="tour_type" style="width:90%;">
                                                                            <span class="form-check-label"></span>
                                                                            <option value=""> {{trans('common.category')}}</option>
                                                                            @foreach($Category as $cate)
                                                                                <option value="{{$cate->psub_id}}"> {{$cate->TourType}}</option>
                                                                            @endforeach
                                                                            {{--<option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>--}}
                                                                        </select>
                                                                    </var>
                                                                    <var class="price">
                                                                        <select class="form-control form-control-sm" id="number_of_person" name="number_of_person" required style="width:90%;">
                                                                            <span class="form-check-label"></span>
                                                                            <option value=""> {{trans('common.amount')}}</option>
                                                                            @for($i=1;$i<=10;$i++)
                                                                            <option value="{{$i}}"> {{$i}} คน</option>
                                                                            @endfor
                                                                            {{--<option> 2 คน</option>--}}
                                                                            {{--<option> 3 คน</option>--}}
                                                                            {{--<option> 4 คน</option>--}}
                                                                        </select>
                                                                    </var>
                                                                </td>
                                                                <td>
                                                                    @if($Price->Price_by_promotion!=$Price->PriceSale)
                                                                        <del>{{$current->currency_symbol.number_format($Price->PriceSale)}}</del> - {{$current->currency_symbol.number_format($Price->Price_by_promotion)}} <br>
                                                                    @endif
                                                                    <span class="num h5 text-warning">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}} </span>
                                                                    @if($Price->Price_by_promotion!=$Price->PriceSale)
                                                                        @if($promotion)
                                                                            <span class="price-save">
                                                                                {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                                @if($promotion->promotion_operator2=='up')
                                                                                    {{$current->currency_symbol.number_format($Price->Price_by_promotion-$Price->PriceSale)}}
                                                                                @else
                                                                                    {{$current->currency_symbol.number_format($Price->PriceSale-$Price->Price_by_promotion)}}
                                                                                @endif
                                                                            </span>
                                                                            @endif
                                                                      <br>
                                                                    @endif

                                                                    @if($data_target != null)
                                                                        <br>
                                                                        <span class="price-save-date">
                                                                                 <span id="clockdiv{{$detail->packageDescID}}">(ราคานี้
                                                                                     <span class="days"></span> {{trans('common.day')}}
                                                                                     <span class="hours"></span>:
                                                                                     <span class="minutes"></span>:
                                                                                     <span class="seconds"></span>, {{$detail->NumberOfPeople}} ที่สุดท้ายเท่านั้น)
                                                                                    </span>
                                                                                </span>
                                                                        <script language="JavaScript">
                                                                            initializeClock('clockdiv{{$detail->packageDescID}}', new Date('{{$data_target}}'));
                                                                        </script>
                                                                    @else
                                                                        <BR>
                                                                         <span class="price-save-date">
                                                                                     (ราคานี้เหลือเพียง {{$person_booking}} ที่นั่งสุดท้ายเท่านั้น)
                                                                         </span>
                                                                    @endif
                                                                    <br>
                                                                    <button type="submit" name="submit_cart"  value="Y" class="btn btn-sm btn-outline-primary">
                                                                        <i class="fas fa-shopping-cart"></i> {{trans('common.addtocart')}}
                                                                    </button>
                                                                    <button type="submit" name="submit_order"  value="Y" class="btn btn-sm btn-primary"> {{trans('common.booking')}} </button>
                                                                    {{--<a href="{{url('/booking/'.$detail->packageDescID)}}" class="btn btn-sm btn-primary"> {{trans('common.booking')}} </a>--}}
                                                                </td>
                                                            </tr>
                                                            </form>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </dl>
                                    </article>
                                </aside>
                            </div> <!-- row.// -->
                        </main> <!-- card.// -->
                        <!-- PRODUCT DETAIL -->

                        <article class="card mt-3">
                            <article class="card">
                                <div class="card-body p-3">
                                    <ul class="nav bg radius nav-pills nav-fill mb-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" data-toggle="pill" href="#nav-tab-calendar">
                                                ตารางการเดินทาง</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#nav-tab-condition">
                                                เงื่อนไข & ข้อตกลง </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#nav-tab-prepare">
                                                แนะนำก่อนเดินทาง </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#nav-tab-review">
                                                <ul class="rating-stars">
                                                    <li style="width:80%" class="stars-active">
                                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                                    </li>
                                                </ul>
                                                <div class="label-rating">132 Ratings & Reviews</div>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade active show" id="nav-tab-calendar">
                                            <h4>ตารางการเดินทาง</h4>
                                            <hr>

                                            @for($day=1;$day<=$PackageTourOne->packageDays;$day++)
                                                <?php
                                                $Programs=DB::table('package_programs as a')
                                                        ->join('package_program_info as b','b.programID','=','a.programID')
                                                        ->where('a.packageID',$PackageTourOne->packageID)
                                                        ->where('b.LanguageCode',Session::get('language'))
                                                        ->where('a.packageDays',$day)
                                                        ->orderby('a.packageDays','asc')
                                                        ->orderby('a.packageID','asc')
//                                                        ->orderby('a.packageTime','asc')
                                                        ->get();

                                                $PackageHighlight=DB::table('package_program_highlight as a')
                                                        ->join('package_program_highlight_info as b','b.HighlightID','=','a.HighlightID')
                                                        ->where('b.LanguageCode',Session::get('language'))
                                                        ->where('a.packageID',$PackageTourOne->packageID)
                                                        ->where('a.packageDays',$day)
                                                        ->orderby('packageDays','asc')
                                                        ->first();
                                                   // dd($Programs);
//                                                    $Date=DB::table('package_days')->where('DayCode',$day)->where('LanguageCode',Session::get('language'))->fisrt();
                                                    $Date=\App\PackageDay::where('DayCode',$day)->active()->first();


                                                ?>
                                                <h5 class="text-primary">
                                                    {{$Date->DayName}} {{$day}} : {{$PackageHighlight->Highlight}}
                                                </h5>

                                                @if($Programs)

                                                <div class="row">
                                                    @foreach($Programs as $rows)
                                                        <?php
                                                        $time_text=$rows->packageTime;
                                                        if($rows->packageTime2>0){
                                                            $Time=\App\PackageTime::where('TimeCode',$rows->packageTime2)->active()->first();
    //                                                        $Time=DB::table('package_times')->where('TimeCode',$day)->where('LanguageCode',Session::get('language'))->first();
                                                            if($Time){
                                                                $time_text=$Time->Time_text;
                                                            }
                                                         }
                                                        ?>
                                                        <div class="col-md-1" style="margin-bottom: 15px">
                                                            <strong class="text-warning">{{$time_text}} </strong>
                                                        </div>
                                                        <div class="col-md-11">
                                                            {!! $rows->packageDetails !!}
                                                        </div>
                                                    {{--<li><strong>23.25 น.</strong> ออกเดินทางสู่ <strong>เมืองเดลลี</strong> โดยสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) <strong>เที่ยวบินที่ TG331 </strong>(ใช้เวลาเดินทางโดยประมาณ  4.25 ชม.)(มีบริการอาหารบนเครื่อง)</li>--}}
                                                    {{--<li><strong>20.25  น. </strong>คณะมาพร้อมกันที่ <strong>สนามบินสุวรรณภูมิ</strong> ผู้โดยสารขาออกชั้น <strong>4</strong> ของสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) โดยมีเจ้าหน้าที่บริษัทและหัวหน้าทัวร์ให้การต้อนรับ และอำนวยความสะดวก</li>--}}
                                                    {{--<li><strong>23.25 น.</strong> ออกเดินทางสู่ <strong>เมืองเดลลี</strong> โดยสายการบิน<strong>ไทย</strong> (<strong>Thai Airways</strong>) <strong>เที่ยวบินที่ TG331 </strong>(ใช้เวลาเดินทางโดยประมาณ  4.25 ชม.)(มีบริการอาหารบนเครื่อง)</li>--}}
                                                            <?php
                                                            $HighlightProgram=DB::table('highlight_in_schedule')
                                                            ->where('programID',$rows->programID)
                                                            ->get();
                                                            ?>
                                                            @if($HighlightProgram->count()>0)
                                                            <div class="col-md-12">
                                                                    <div class="col-md-12">
                                                                        <h5>{{trans('package.PackageHighlight')}}</h5></div>
                                                                    @foreach($HighlightProgram as $rowh)
                                                                        <?php
                                                                        $location=\App\Location::where('id',$rowh->LocationID)->first();
                                                                        $timeline=\App\Timeline::where('id',$location->timeline_id)->first();
                                                                        $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
                                                                        $media=\App\Media::where('id',$timeline->avatar_id)->first();
                                                                        $city=null;$state=null;
                                                                        $country=$timeline->country()->where('language_code',Session::get('language'))->first();
                                                                        if(!$country){
                                                                            $country=$timeline->country()->where('language_code','en')->first();
                                                                        }

                                                                        if($timeline->state_id){
                                                                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
                                                                            if(!$state){
                                                                                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code','en')->first();
                                                                            }

                                                                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
                                                                            if(!$city){
                                                                                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code','en')->first();
                                                                            }
                                                                        }
                                                                        if(!$country){
                                                                            $country=$timeline->country()->where('language_code','en')->first();
                                                                        }
                                                                        ?>

                                                                        <div class="col-md-4">
                                                                            @if($post!=null)
                                                                                <div class="card card-pin">
                                                                                    <div class="panel panel-default panel-post animated" id="post{{ $post->id }}">
                                                                                        <div class="panel-heading no-bg">
                                                                                            <div class="post-author">
                                                                                                <?php
                                                                                                $date=\Date::parse($post->created_at);
                                                                                                ?>
                                                                                                <div class="user-avatar">
                                                                                                    <a href="{{ url('home/'.$timeline->username) }}">
                                                                                                        @if($media!=null)
                                                                                                            <img src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                                                                        @else
                                                                                                            <img src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                                                                                                        @endif
                                                                                                    </a>
                                                                                                    {{--<a href="{{ url($post->user->username) }}"><img src="{{ $post->user->avatar }}" alt="{{ $post->user->name }}" title="{{ $post->user->name }}"></a>--}}
                                                                                                    <?php
                                                                                                    $star_rating=0;
                                                                                                    $RateAll2=DB::table('star_rating')
                                                                                                            ->where('timeline_id',$timeline->id)
                                                                                                            ->whereIn('rate_group',[2,3,4,5])
                                                                                                            ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
                                                                                                            ->first();
                                                                                                    if($RateAll2){
                                                                                                        if($RateAll2->star>0){
                                                                                                            $star_rating=round($RateAll2->star/$RateAll2->userCount,1);
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                    @if($star_rating>0)
                                                                                                        @if($star_rating>=4)
                                                                                                            <span title="{{trans('common.overall_accessible_rating')}}" class="text-success" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                                                                        @elseif($star_rating>=3)
                                                                                                            <span title="{{trans('common.overall_accessible_rating')}}" class="text-warning" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>
                                                                                                        @elseif($star_rating<3)
                                                                                                            <span title="{{trans('common.overall_accessible_rating')}}" class="text-danger" data-selenium="hotel-header-review-score"><i class="fa fa-wheelchair-alt"></i> {{number_format($star_rating,1)}}</span>

                                                                                                        @endif
                                                                                                    @endif
                                                                                                </div>
                                                                                                <div class="user-post-details">
                                                                                                    <ul class="list-unstyled no-margin">
                                                                                                        <li>
                                                                                                            @if(isset($sharedOwner))
                                                                                                                <a href="{{ url($sharedOwner->user->username) }}" title="{{ '@'.$sharedOwner->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                                                                    {{ $sharedOwner->user->name }}
                                                                                                                </a>
                                                                                                                shared
                                                                                                            @endif
                                                                                                            <a href="{{ url('home/'.$timeline->username) }}" title="{{ '@'.$post->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                                                                                                                {{ $timeline->name }}
                                                                                                            </a>
                                                                                                            @if($post->user->verified)
                                                                                                                <span class="verified-badge bg-success">
                        <i class="fa fa-check"></i>
                    </span>
                                                                                                            @endif

                                                                                                            @if(isset($sharedOwner))
                                                                                                                's post
                                                                                                            @endif

                                                                                                            @if($post->users_tagged->count() > 0)
                                                                                                                {{ trans('common.with') }}
                                                                                                                <?php $post_tags = $post->users_tagged->pluck('name')->toArray(); ?>
                                                                                                                <?php $post_tags_ids = $post->users_tagged->pluck('id')->toArray(); ?>
                                                                                                                @foreach($post->users_tagged as $key => $user)
                                                                                                                    @if($key==1)
                                                                                                                        {{ trans('common.and') }}
                                                                                                                        @if(count($post_tags)==1)
                                                                                                                            <a href="{{ url($user->username) }}">{{ $user->name }}</a>
                                                                                                                        @else
                                                                                                                            <a href="#" data-toggle="tooltip" title="" data-placement="top" class="show-users-modal" data-html="true" data-heading="{{ trans('common.with_people') }}"  data-users="{{ implode(',', $post_tags_ids) }}" data-original-title="{{ implode('<br />', $post_tags) }}"> {{ count($post_tags).' '.trans('common.others') }}</a>
                                                                                                                        @endif
                                                                                                                        @break
                                                                                                                    @endif
                                                                                                                    @if($post_tags != null)
                                                                                                                        <a href="{{ url('home/'.$user->username) }}" class="user"> {{ array_shift($post_tags) }} </a>
                                                                                                                    @endif
                                                                                                                @endforeach

                                                                                                            @endif

                                                                                                        </li>

                                                                                                        <li>
                <span>
                    <?php
                    $category=\App\Category::where('category_id',$location->category_id)->where('language_code',Session::get('language'))->first();
                    if(!$category){
                        $category=\App\Category::where('category_id',$location->category_id)->where('language_code','en')->first();
                    }

                    if($location->category_sub1_id){
                        $sub1category=$category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first();
                    }

                    ?>
                    @if($location->category_sub2_id>0)
                        <?php
                        $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                ->where('language_code',Session::get('language'))
                                ->first();
                        if(!$category_sub2){
                            $category_sub2=$sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)
                                    ->where('language_code','en')
                                    ->first();
                        }
                        ?>
                        {{$category_sub2->category_sub2_name}},

                    @elseif($location->category_sub1_id>0)
                        {{ $category->subcategory_home()->where('category_sub1_id',$location->category_sub1_id)->first()->sub1_name }},

                    @else
                        @if($location->category_id)
                            <?php
                            $category=\App\Category::where('category_id',$location->category_id)
                                    ->where('language_code',Session::get('language'))->first();
                            if($category){
                                $category=\App\Category::where('category_id',$location->category_id)
                                        ->where('language_code','en')->first();
                            }
                            ?>
                            {{$category->name}},

                        @endif
                    @endif

                </span>

                  <span>
					  @if($city)
                          {{ $city->city }}
                      @endif
                      @if($state)
                          {{$state->state}}
                      @endif
                      @if($country)
                          {{$country->country}}
                      @endif
				</span>


                                                                                                            @if($post->location != NULL && !isset($sharedOwner))
                                                                                                                {{ trans('common.at') }}
                                                                                                                <span class="post-place">
                      <a target="_blank" href="{{ url('locations'.$post->location) }}">
                          <i class="fa fa-map-marker"></i> {{ $post->location }}
                      </a>
                      </span>
                                                                                                        </li>
                                                                                                        @endif
                                                                                                    </ul>

                                                                                                </div>
                                                                                                <div  style="font-size: 12px;padding-left: 50px; color:#859AB5 ">

                    <span>
                        <div class="rating-wrap">
                            <span>
                                (<a href="{{ url('home/'.$post->user->username) }}" class="user"> {{ $post->user->name }}</a>
                                @if($post->rate_group!='')
                                    {{trans('common.reviewed_on')}}
                                    <?php
                                    $rate_group=$post->rate_group;
                                    if($rate_group=='1') {
                                        $rate_group_title = trans('common.over_all_rating');
                                    }else if($rate_group=='2'){
                                        $rate_group_title=trans('common.inside_rating');
                                    }else if($rate_group=='3'){
                                        $rate_group_title=trans('common.outside_rating');
                                    }else if($rate_group=='4'){
                                        $rate_group_title=trans('common.toilet_rating');
                                    }else if($rate_group=='5'){
                                        $rate_group_title=trans('common.parking_rating');
                                    }else if($rate_group=='6'){
                                        $rate_group_title=trans('common.cleanliness');
                                    }else if($rate_group=='7'){
                                        $rate_group_title=trans('common.normal_parking');
                                    }else if($rate_group=='8'){
                                        $rate_group_title=trans('common.normal_toilet');
                                    }
                                    ?>
                                    {{$rate_group_title}}
                                @else
                                    {{trans('common.posted_on')}}
                                @endif
                            </span>

                            @if($post->rate_group!='')
                                <?php
                                $width=0;$star_rating=0;
                                if($post->star_rating){
                                    $width=$post->star_rating*20;
                                    $star_rating=$post->star_rating;
                                }
                                ?>
                                <ul class="rating-stars">
                                    <li style="width:{{$width}}%" class="stars-active">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </li>
                                </ul>
                            @endif
                            <span>{{$date->ago()}})</span>
                        </div>
                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="panel-body">
                                                                                            <div class="text-wrapper">
                                                                                                <?php
                                                                                                $links = preg_match_all("/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $post->description, $matches);

                                                                                                $main_description = $post->description;
                                                                                                ?>
                                                                                                @foreach($matches[0] as $link)
                                                                                                    <?php
                                                                                                    $linkPreview = new LinkPreview($link);
                                                                                                    $parsed = $linkPreview->getParsed();
                                                                                                    foreach ($parsed as $parserName => $main_link) {
                                                                                                        $data = '<div class="row link-preview">
                                                                                                                  <div class="col-md-3">
                                                                                                                    <a target="_blank" href="'.$link.'"><img src="'.$main_link->getImage().'"></a>
                                                                                                                  </div>
                                                                                                                  <div class="col-md-9">
                                                                                                                    <a target="_blank" href="'.$link.'">'.$main_link->getTitle().'</a><br>'.substr($main_link->getDescription(), 0, 500). '...'.'
                                                                                                                  </div>
                                                                                                                </div>';
                                                                                                    }
                                                                                                    $main_description = str_replace($link, $data, $main_description);
                                                                                                    ?>
                                                                                                @endforeach

                                                                                                <p class="post-description">
                                                                                                    {{--************************ Cannot fig here ****************************--}}
                                                                                                    {!! $main_description !!}
                                                                                                    {{--{!! clean($main_description) !!}--}}
                                                                                                </p>
                                                                                                <div class="post-image-holder  @if(count($post->images()->get()) == 1) single-image @endif">
                                                                                                    @foreach($post->images()->get() as $postImage)
                                                                                                        @if($postImage->type=='image')
                                                                                                            @if(!file_exists(storage_path('user/gallery/mid/'.$postImage->source)))
                                                                                                                <a href="{{ url('user/gallery/'.$postImage->source) }}" data-lightbox="imageGallery.{{ $post->id }}" >
                                                                                                                    <img src="{{ url('user/gallery/mid/'.$postImage->source) }}"  title="{{ $post->user->name }}" alt="{{ $post->user->name }}">
                                                                                                                    {{--{{ $post->user->name }} |--}}
                                                                                                                    {{--<time class="post-time timeago" datetime="{{ $post->created_at }}+00:00" title="{{ $post->created_at }}+00:00">--}}
                                                                                                                    {{--{{ $post->created_at }}+00:00--}}
                                                                                                                    {{--</time>--}}
                                                                                                                </a>
                                                                                                            @else
                                                                                                                <img src="{{ url('location/avatar/no-image-full.jpg') }}">
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </div>
                                                                                                <div class="post-v-holder">
                                                                                                    @foreach($post->images()->get() as $postImage)
                                                                                                        @if($postImage->type=='video')
                                                                                                            <video width="100%" preload="none" height="auto" poster="{{ url('user/gallery/video/'.$postImage->title) }}.jpg" controls class="video-video-playe">
                                                                                                                <source src="{{ url('user/gallery/video/'.$postImage->source) }}" type="video/mp4">
                                                                                                                <!-- Captions are optional -->
                                                                                                            </video>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                            @if($post->youtube_video_id)
                                                                                                <iframe  src="https://www.youtube.com/embed/{{ $post->youtube_video_id }}" frameborder="0" allowfullscreen></iframe>
                                                                                            @endif
                                                                                            @if($post->soundcloud_id)
                                                                                                <div class="soundcloud-wrapper">
                                                                                                    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $post->soundcloud_id }}&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                                                                                </div>
                                                                                            @endif
                                                                                            <ul class="actions-count list-inline">
                                                                                                @if($post->users_liked()->count() > 0)
                                                                                                    <?php
                                                                                                    $liked_ids = $post->users_liked->pluck('id')->toArray();
                                                                                                    $liked_names = $post->users_liked->pluck('name')->toArray();
                                                                                                    ?>
                                                                                                    <li>
                                                                                                        <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.likes') }}"  data-users="{{ implode(',', $liked_ids) }}" data-original-title="{{ implode('<br />', $liked_names) }}"><span class="count-circle"><i class="fa fa-thumbs-up"></i></span> {{ $post->users_liked->count() }} {{ trans('common.likes') }}</a>
                                                                                                    </li>
                                                                                                @endif

                                                                                                @if($post->comments->count() > 0)
                                                                                                    <li>
                                                                                                        <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>{{ $post->comments->count() }} {{ trans('common.comments') }}</a>
                                                                                                    </li>
                                                                                                @endif

                                                                                                @if($post->shares->count() > 0)
                                                                                                    <?php
                                                                                                    $shared_ids = $post->shares->pluck('id')->toArray();
                                                                                                    $shared_names = $post->shares->pluck('name')->toArray(); ?>
                                                                                                    <li>
                                                                                                        <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.shares') }}"  data-users="{{ implode(',', $shared_ids) }}" data-original-title="{{ implode('<br />', $shared_names) }}"><span class="count-circle"><i class="fa fa-share"></i></span> {{ $post->shares->count() }} {{ trans('common.shares') }}</a>
                                                                                                    </li>
                                                                                                @endif
                                                                                            </ul>
                                                                                            <div class="text-right">
                                                                                                @if($timeline->type == 'location')
                                                                                                    @if($post->rate_group>0)
                                                                                                        @if($post->date_visited_location!='0000-00-00')
                                                                                                            <small style="color: #9cc2cb"><i class="fa fa-calendar"></i> {{trans('common.date_visit_location').' '.\Date::parse($post->date_visited_location.'01:01:10')->format('j F Y')}}</small>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>




                                                                                    </div>

                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                            <!-- col // -->
                                                                    @endforeach
                                                            </div>

                                                            @endif

                                                    @endforeach
                                                </div>
                                                @endif
                                                {{--@if($PackageHighlight->Breakfast=='Y' || $PackageHighlight->Lunch=='Y' || $PackageHighlight->Dinner=='Y')--}}
                                                    {{--<h5>{{trans('package.food')}}</h5>--}}
                                                    {{--<ul class="list">--}}
                                                        {{--@if($PackageHighlight->Breakfast=='Y')--}}
                                                            {{--<li><strong>{{trans('package.breakfast')}} </strong> (บริการอาหารกลางวัน ณ ภัตตาคาร) </li>--}}
                                                        {{--@endif--}}
                                                        {{--@if($PackageHighlight->Lunch=='Y')--}}
                                                            {{--<li><strong>{{trans('package.lunch')}} </strong> - </li>--}}
                                                        {{--@endif--}}
                                                        {{--@if($PackageHighlight->Dinner=='Y')--}}
                                                            {{--<li><strong>{{trans('package.dinner')}} </strong> (บริการอาหารกลางวัน ณ ภัตตาคาร) </li>--}}
                                                        {{--@endif--}}
                                                    {{--</ul>--}}
                                                {{--@endif--}}
                                                <hr>

                                                    {{--<h5>ที่พัก</h5>--}}
                                                    {{--<div class="row">--}}
                                                        {{--<div class="col-md-4">--}}
                                                            {{--<figure class="card card-product">--}}
                                                                {{--<div class="img-wrap"><a href="detail.html"><img src="images/banners/slide1-1.jpg"></a></div>--}}
                                                                {{--<figcaption class="info-wrap-2">--}}
                                                                    {{--<h5 class="title"><a href="detail.html">ชื่อสถานที่ตามไทมไลน</a></h5>--}}
                                                                    {{--<div class="rating-wrap">--}}
                                                                        {{--<a href="detail.html">--}}
                                                                            {{--<ul class="rating-stars">--}}
                                                                                {{--<li style="width:80%" class="stars-active">--}}
                                                                                    {{--<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>--}}
                                                                                {{--</li>--}}
                                                                                {{--<li>--}}
                                                                                    {{--<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>--}}
                                                                                {{--</li>--}}
                                                                            {{--</ul>--}}
                                                                            {{--<div class="label-rating">132 รีวิว</div>--}}
                                                                        {{--</a>--}}
                                                                    {{--</div> <!-- rating-wrap.// -->--}}
                                                                {{--</figcaption>--}}
                                                            {{--</figure>--}}
                                                        {{--</div> <!-- col // -->--}}
                                                    {{--</div>--}}
                                                    {{--<hr>--}}


                                            @endfor





                                        </div> <!-- tab-pane.// -->
                                        <div class="tab-pane fade" id="nav-tab-condition">

                                            <h4>เงื่อนไข & ข้อตกลง</h4>
                                            <hr>
                                            <?php
                                                $ConditionGroup=DB::table('condition_group')
                                                    ->orderby('condition_group_id','asc')
                                                    ->where('language_code',Session::get('language'))
                                                    ->get();
                                            ?>

                                            @foreach ($ConditionGroup as $group)
                                                <?php
                                                    $Conditions=DB::table('condition_in_package_details as a')
                                                        ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                                        ->where('a.packageDescID',$Detail->packageDescID)
                                                        ->where('b.condition_group_id',$group->condition_group_id)
                                                        ->where('language_code',Session::get('language'))
                                                        ->get();

                                                ?>
                                                @if($Conditions->count()>0)
                                                <h5 style="color: green">{{$group->condition_group_title}}</h5>

                                                <ul class="list">
                                                    @foreach($Conditions as $condition)
                                                    <li>{!! $condition->condition_title !!} </li>
                                                    @endforeach
                                                    {{--<li>เสื้อกันลม&nbsp;  ผ้าพันคอ ถุงเท้า รองเท้าใส่ที่เดินสบาย แจ็คเก็ตกันลม เสื้อผ้าควรสวมสบาย  ๆ ซักง่าย แห้งง่าย ไม่ต้องรีด </li>--}}
                                                    {{--<li>คุณผู้หญิงขอแนะนำให้ใส่กระโปรงยาวหรือกางเกงที่เป็นผ้ายืด  เพื่อความสะดวกในการเข้าห้องน้ำ (แบบธรรมชาติ) </li>--}}
                                                    {{--<li>หมวกกันแดดหรือร่ม,&nbsp; ผ้าปิดจมูกกันฝุ่น, ยาทากันยุง ( กลางคืนยุงค่อนข้างเยอะ  ) กล้องและฟิล์มถ่ายภาพ ปลั๊กชาร์จแบตเตอรี่ของกล้องดิจิตอล </li>--}}
                                                    {{--<li>ปลั๊กแปลงขาเสียบ,ไฟฉาย ธูป เทียน ทองคำเปลว  ร่มหรือหมวก แว่นตาคอนเทคเลนส์ แว่นกันแดด ฯลฯ </li>--}}
                                                    {{--<li>ของใช้ส่วนตัว  พวกแชมพู สบู่ ยาสีฟัน แปรงสีฟัน ผ้าเช็ดตัว ผ้าผลัดเปลี่ยนเวลาอาบน้ำ </li>--}}
                                                    {{--<li>กระเป๋าเดินทางน้ำหนักรวมต้องไม่เกินท่านละ 15  กิโลกรัม หากน้ำหนักของท่านเกิน ท่านต้องเป็นผู้รับผิดชอบค่าระวางน้ำหนัก</li>--}}
                                                    {{--<li>ส่วนเกินที่ทางสายการบินเรียกเก็บจากท่าน </li>--}}
                                                    {{--<li>เงินที่นำไปใช้ส่วนตัวควรแลกเป็นดอลล่าห์สหรัฐ  หรือเงินบาท&nbsp; หากไม่สะดวกแลกเงินดอลลาร์ไป  สามารถนำเงินบาทไปแลกที่อินเดีย </li>--}}
                                                    {{--<li>กับร้านแลกเงินได้&nbsp; และควรเก็บสิ่งของมีค่าไว้ให้เรียบร้อย  เพื่อป้องกันการสูญหายอันอาจจะเกิดขึ้นได้</li>--}}
                                                </ul>
                                                 @endif

                                            @endforeach


                                        </div> <!-- tab-pane.// -->
                                        <div class="tab-pane fade" id="nav-tab-prepare">

                                            <h4>แนะนำก่อนเดินทาง</h4>
                                            <hr>
                                            <h5>เตรียมตัวเดินทาง</h5>
                                            <ul class="list">
                                                <li>สำหรับท่านที่มีโรคประจำตัว  โปรดแจ้งหัวหน้าทัวร์ให้ทราบ และนำยาของท่านติดตัวไปให้เพียงพอตลอดรายการ </li>
                                                <li>เสื้อกันลม&nbsp;  ผ้าพันคอ ถุงเท้า รองเท้าใส่ที่เดินสบาย แจ็คเก็ตกันลม เสื้อผ้าควรสวมสบาย  ๆ ซักง่าย แห้งง่าย ไม่ต้องรีด </li>
                                                <li>คุณผู้หญิงขอแนะนำให้ใส่กระโปรงยาวหรือกางเกงที่เป็นผ้ายืด  เพื่อความสะดวกในการเข้าห้องน้ำ (แบบธรรมชาติ) </li>
                                                <li>หมวกกันแดดหรือร่ม,&nbsp; ผ้าปิดจมูกกันฝุ่น, ยาทากันยุง ( กลางคืนยุงค่อนข้างเยอะ  ) กล้องและฟิล์มถ่ายภาพ ปลั๊กชาร์จแบตเตอรี่ของกล้องดิจิตอล </li>
                                                <li>ปลั๊กแปลงขาเสียบ,ไฟฉาย ธูป เทียน ทองคำเปลว  ร่มหรือหมวก แว่นตาคอนเทคเลนส์ แว่นกันแดด ฯลฯ </li>
                                                <li>ของใช้ส่วนตัว  พวกแชมพู สบู่ ยาสีฟัน แปรงสีฟัน ผ้าเช็ดตัว ผ้าผลัดเปลี่ยนเวลาอาบน้ำ </li>
                                                <li>กระเป๋าเดินทางน้ำหนักรวมต้องไม่เกินท่านละ 15  กิโลกรัม หากน้ำหนักของท่านเกิน ท่านต้องเป็นผู้รับผิดชอบค่าระวางน้ำหนัก</li>
                                                <li>ส่วนเกินที่ทางสายการบินเรียกเก็บจากท่าน </li>
                                                <li>เงินที่นำไปใช้ส่วนตัวควรแลกเป็นดอลล่าห์สหรัฐ  หรือเงินบาท&nbsp; หากไม่สะดวกแลกเงินดอลลาร์ไป  สามารถนำเงินบาทไปแลกที่อินเดีย </li>
                                                <li>กับร้านแลกเงินได้&nbsp; และควรเก็บสิ่งของมีค่าไว้ให้เรียบร้อย  เพื่อป้องกันการสูญหายอันอาจจะเกิดขึ้นได้</li>
                                            </ul>

                                            <hr>
                                            <h5>เตรียมตัวเดินทาง 2</h5>
                                            <ul class="list">
                                                <li>สำหรับท่านที่มีโรคประจำตัว  โปรดแจ้งหัวหน้าทัวร์ให้ทราบ และนำยาของท่านติดตัวไปให้เพียงพอตลอดรายการ </li>
                                                <li>เสื้อกันลม&nbsp;  ผ้าพันคอ ถุงเท้า รองเท้าใส่ที่เดินสบาย แจ็คเก็ตกันลม เสื้อผ้าควรสวมสบาย  ๆ ซักง่าย แห้งง่าย ไม่ต้องรีด </li>
                                                <li>คุณผู้หญิงขอแนะนำให้ใส่กระโปรงยาวหรือกางเกงที่เป็นผ้ายืด  เพื่อความสะดวกในการเข้าห้องน้ำ (แบบธรรมชาติ) </li>
                                                <li>หมวกกันแดดหรือร่ม,&nbsp; ผ้าปิดจมูกกันฝุ่น, ยาทากันยุง ( กลางคืนยุงค่อนข้างเยอะ  ) กล้องและฟิล์มถ่ายภาพ ปลั๊กชาร์จแบตเตอรี่ของกล้องดิจิตอล </li>
                                                <li>ปลั๊กแปลงขาเสียบ,ไฟฉาย ธูป เทียน ทองคำเปลว  ร่มหรือหมวก แว่นตาคอนเทคเลนส์ แว่นกันแดด ฯลฯ </li>
                                                <li>ของใช้ส่วนตัว  พวกแชมพู สบู่ ยาสีฟัน แปรงสีฟัน ผ้าเช็ดตัว ผ้าผลัดเปลี่ยนเวลาอาบน้ำ </li>
                                                <li>กระเป๋าเดินทางน้ำหนักรวมต้องไม่เกินท่านละ 15  กิโลกรัม หากน้ำหนักของท่านเกิน ท่านต้องเป็นผู้รับผิดชอบค่าระวางน้ำหนัก</li>
                                                <li>ส่วนเกินที่ทางสายการบินเรียกเก็บจากท่าน </li>
                                                <li>เงินที่นำไปใช้ส่วนตัวควรแลกเป็นดอลล่าห์สหรัฐ  หรือเงินบาท&nbsp; หากไม่สะดวกแลกเงินดอลลาร์ไป  สามารถนำเงินบาทไปแลกที่อินเดีย </li>
                                                <li>กับร้านแลกเงินได้&nbsp; และควรเก็บสิ่งของมีค่าไว้ให้เรียบร้อย  เพื่อป้องกันการสูญหายอันอาจจะเกิดขึ้นได้</li>
                                            </ul>

                                        </div> <!-- tab-pane.// -->
                                        <div class="tab-pane fade" id="nav-tab-review">

                                            <h4>Ratings & Reviews</h4>
                                            <hr>
                                            <h5>เตรียมตัวเดินทาง</h5>
                                            <ul class="list">
                                                <li>สำหรับท่านที่มีโรคประจำตัว  โปรดแจ้งหัวหน้าทัวร์ให้ทราบ และนำยาของท่านติดตัวไปให้เพียงพอตลอดรายการ </li>
                                                <li>เสื้อกันลม&nbsp;  ผ้าพันคอ ถุงเท้า รองเท้าใส่ที่เดินสบาย แจ็คเก็ตกันลม เสื้อผ้าควรสวมสบาย  ๆ ซักง่าย แห้งง่าย ไม่ต้องรีด </li>
                                                <li>คุณผู้หญิงขอแนะนำให้ใส่กระโปรงยาวหรือกางเกงที่เป็นผ้ายืด  เพื่อความสะดวกในการเข้าห้องน้ำ (แบบธรรมชาติ) </li>
                                                <li>หมวกกันแดดหรือร่ม,&nbsp; ผ้าปิดจมูกกันฝุ่น, ยาทากันยุง ( กลางคืนยุงค่อนข้างเยอะ  ) กล้องและฟิล์มถ่ายภาพ ปลั๊กชาร์จแบตเตอรี่ของกล้องดิจิตอล </li>
                                                <li>ปลั๊กแปลงขาเสียบ,ไฟฉาย ธูป เทียน ทองคำเปลว  ร่มหรือหมวก แว่นตาคอนเทคเลนส์ แว่นกันแดด ฯลฯ </li>
                                                <li>ของใช้ส่วนตัว  พวกแชมพู สบู่ ยาสีฟัน แปรงสีฟัน ผ้าเช็ดตัว ผ้าผลัดเปลี่ยนเวลาอาบน้ำ </li>
                                                <li>กระเป๋าเดินทางน้ำหนักรวมต้องไม่เกินท่านละ 15  กิโลกรัม หากน้ำหนักของท่านเกิน ท่านต้องเป็นผู้รับผิดชอบค่าระวางน้ำหนัก</li>
                                                <li>ส่วนเกินที่ทางสายการบินเรียกเก็บจากท่าน </li>
                                                <li>เงินที่นำไปใช้ส่วนตัวควรแลกเป็นดอลล่าห์สหรัฐ  หรือเงินบาท&nbsp; หากไม่สะดวกแลกเงินดอลลาร์ไป  สามารถนำเงินบาทไปแลกที่อินเดีย </li>
                                                <li>กับร้านแลกเงินได้&nbsp; และควรเก็บสิ่งของมีค่าไว้ให้เรียบร้อย  เพื่อป้องกันการสูญหายอันอาจจะเกิดขึ้นได้</li>
                                            </ul>

                                            <hr>
                                            <h5>เตรียมตัวเดินทาง 2</h5>
                                            <ul class="list">
                                                <li>สำหรับท่านที่มีโรคประจำตัว  โปรดแจ้งหัวหน้าทัวร์ให้ทราบ และนำยาของท่านติดตัวไปให้เพียงพอตลอดรายการ </li>
                                                <li>เสื้อกันลม&nbsp;  ผ้าพันคอ ถุงเท้า รองเท้าใส่ที่เดินสบาย แจ็คเก็ตกันลม เสื้อผ้าควรสวมสบาย  ๆ ซักง่าย แห้งง่าย ไม่ต้องรีด </li>
                                                <li>คุณผู้หญิงขอแนะนำให้ใส่กระโปรงยาวหรือกางเกงที่เป็นผ้ายืด  เพื่อความสะดวกในการเข้าห้องน้ำ (แบบธรรมชาติ) </li>
                                                <li>หมวกกันแดดหรือร่ม,&nbsp; ผ้าปิดจมูกกันฝุ่น, ยาทากันยุง ( กลางคืนยุงค่อนข้างเยอะ  ) กล้องและฟิล์มถ่ายภาพ ปลั๊กชาร์จแบตเตอรี่ของกล้องดิจิตอล </li>
                                                <li>ปลั๊กแปลงขาเสียบ,ไฟฉาย ธูป เทียน ทองคำเปลว  ร่มหรือหมวก แว่นตาคอนเทคเลนส์ แว่นกันแดด ฯลฯ </li>
                                                <li>ของใช้ส่วนตัว  พวกแชมพู สบู่ ยาสีฟัน แปรงสีฟัน ผ้าเช็ดตัว ผ้าผลัดเปลี่ยนเวลาอาบน้ำ </li>
                                                <li>กระเป๋าเดินทางน้ำหนักรวมต้องไม่เกินท่านละ 15  กิโลกรัม หากน้ำหนักของท่านเกิน ท่านต้องเป็นผู้รับผิดชอบค่าระวางน้ำหนัก</li>
                                                <li>ส่วนเกินที่ทางสายการบินเรียกเก็บจากท่าน </li>
                                                <li>เงินที่นำไปใช้ส่วนตัวควรแลกเป็นดอลล่าห์สหรัฐ  หรือเงินบาท&nbsp; หากไม่สะดวกแลกเงินดอลลาร์ไป  สามารถนำเงินบาทไปแลกที่อินเดีย </li>
                                                <li>กับร้านแลกเงินได้&nbsp; และควรเก็บสิ่งของมีค่าไว้ให้เรียบร้อย  เพื่อป้องกันการสูญหายอันอาจจะเกิดขึ้นได้</li>
                                            </ul>

                                        </div> <!-- tab-pane.// -->
                                    </div> <!-- tab-content .// -->

                                </div> <!-- card-body.// -->
                            </article> <!-- card.// -->

                        </article>

                        <!-- card.// -->

                        <!-- PRODUCT DETAIL .// -->

                    </div> <!-- col // -->
                    <?php
                    //dd($PackageTourOne);
                    if(Session::get('timeline_id')){
                        $timeline=\App\Timeline::where('id','=',$PackageTourOne->timeline_id)->first();
                        $location = \App\Location::where('timeline_id', '=', $PackageTourOne->timeline_id)->first();
                        // dd($timeline);
                        $getCountry=\App\Country::where('country_id',$timeline->country_id)
                            ->where('language_code',Session::get('language'))
                            ->first();
                    }
                    ?>

                    <aside class="col-xl-2 col-md-3 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                ข้อมูลผู้ขาย
                            </div>
                            <div class="card-body small">
                                <span>
                                    <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}} | {{$timeline->country->country}}</a>
                                </span>
                                <hr>
                                ทะเบียนท่องเที่ยว: <a href="">11/2548</a><br>
                                ยืนยันตัวตนระดับ: <a href="">สูงสุด</a>
                                <hr>
                                การจอง: 200+<br>
                                ลูกค้าประทับใจ: <a href="">96%</a>
                                <hr>
                                การตอบกลับ 24 ชม <br>
                                เรทการตอบกลับ: 94% <br>
                                เปิดใช้ระบบ: 1 เดือน
                                <hr>
                                <a href="">Visit profile</a>
                            </div> <!-- card-body.// -->
                        </div> <!-- card.// -->
                        <div class="card mt-3">
                            <div class="card-header">
                                คุณอาจชอบ
                            </div>
                            <div class="card-body row">
                                <?php

                                $PackageTourLike = DB::table('package_tour as a')
                                    ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                                    ->join('users', 'a.packageBy', '=', 'users.id')
                                    ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
                                    ->join('package_details as d','d.packageID','=','a.packageID')
                                    ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
                                    ->select('users.id', 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale','users_info.FirstName', 'users_info.LastName', 'b.packageName','b.packageHighlight')
                                    ->where('b.LanguageCode', Session::get('language'))
                                    ->whereIn('a.packageID',function ($query) use($Category_arr){
                                        $query->select('packageID')->from('package_tour_category')
                                            ->where('TourCategoryID',$Category_arr);
                                    })
                                    ->where('b.Status_Info','P')
                                    ->where('d.status','Y')
                                    ->where('e.status','Y')
                                    ->groupby('a.packageID')
                                    ->orderby('e.PriceSale', 'asc')
                                    ->orderby('e.psub_id', 'asc')
                                    ->limit(5)
                                    ->get();
                               // dd($PackageTourLike);
                                ?>
                                @foreach($PackageTourLike as $rows)
                                    <div class="col-md-12 col-sm-3">
                                        <figure class="item border-bottom mb-3">
                                            <a href="{{url('home/details/'.$rows->packageID)}}" class="img-wrap">
                                                <img src="{{asset('package/images/banners/slide1-2.jpg')}}" class="img-md">
                                            </a>
                                            <figcaption class="info-wrap">
                                                <a href="{{url('home/details/'.$rows->packageID)}}" class="title">{{$rows->packageName}}</a>
                                                <div class="price-wrap mb-3">
                                                    <span class="currency">{{$current->currency_symbol}}</span>
                                                    <span class="num">{{number_format($rows->PriceSale)}}</span>
                                                    {{--<span class="price-new">$280</span> <del class="price-old">$280</del>--}}
                                                </div> <!-- price-wrap.// -->
                                            </figcaption>
                                        </figure> <!-- card-product // -->
                                    </div> <!-- col.// -->

                                @endforeach

                            </div> <!-- card-body.// -->
                        </div> <!-- card.// -->
                    </aside> <!-- col // -->
                </div>

                <!-- row.// -->

            </div><!-- container // -->
        </section>
        <!-- ========================= SECTION CONTENT .END// ========================= -->
@endsection