@extends('layouts.package.master')

@section('program-highlight')
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .pull-right-sm {
            float: right;
        }
        .col-md-12{
            margin-top: 20px;
        }
        .col-sm-8{
            padding-right: 1px;
            padding-left: 1px;
        }

        .col-sm-4{
            padding-right: 1px;
            padding-left: 1px;
        }
        .col-sm-12{
            margin-top: 2px;
        }

        .pull-right-sm {
            float: right;
        }
        .col-md-12{
            margin-top: 20px;
        }
        .col-sm-8{
            padding-right: 1px;
            padding-left: 1px;
        }

        .col-sm-4{
            padding-right: 1px;
            padding-left: 1px;
        }

        .col-sm-6{
            padding-right: 1px;
            padding-left: 15px;
        }
        .col-sm-12{
            margin-top: 2px;
        }


        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }


        /* The container */
        .container-cart {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            /*font-size: 22px;*/
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container-cart input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            border-radius:50%;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container-cart:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-cart input:checked ~ .checkmark {
            background-color: #fe5806;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-cart input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-cart .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;

            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .tooltip-inner {
            padding: 10px 15px 10px 10px;
            background: #c3e6cb;
            color: red;
            border: 1px solid #737373;
            text-align: left;
        }

    </style>

    <input type="hidden" id="step" name="step" value="2">
    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')

                @if($Carts)
                    <div id="cart-detail">
                    <div class="col-md-9">
                        <?php $currentPackage=''; $pricetotal=0; $pricetotal_tax=0;$pricetotal_no_tax=0; $discount=0;$pricedeposittotal=0; $price_include_vat='';?>
                        @foreach($Carts as $cart)
                            <?php
                            $PackageTourOne = DB::table('package_tour as a')
                                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                                ->where('b.LanguageCode', Session::get('language'))
                                ->where('a.packageID',$cart->package_id)
                                ->first();
                            //  dd($cart);
                            $CartDetails=DB::table('package_booking_cart_details')
                                ->where('booking_cart_id',$cart->booking_cart_id)
                                ->where('package_id',$cart->package_id)
                                ->where('item_status','Y')
                                ->get();
                              //  dd($CartDetails);
                            $timeline=\App\Timeline::where('id',$PackageTourOne->timeline_id)->first();
                            ?>
                            <div class="card">
                                <div class="col-md-12">
                                    @if($cart->package_id!=$currentPackage)
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="img-wrap">
                                                    <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">
                                                        <img src="{{asset('images/package-tour/mid/'.$PackageTourOne->Image)}}" class="img-thumbnail">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <h4 class="title">
                                                    <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">{!! $PackageTourOne->packageName !!}</a>
                                                </h4>
                                                <hr>
                                                <?php
                                                $current=\App\Currency::where('currency_code',$PackageTourOne->packageCurrency)->first();
                                                Session::put('current',$current->currency_symbol);
                                                $Detail=DB::table('package_details')
                                                    ->where('packageID',$PackageTourOne->packageID)
                                                    ->where('packageDescID',$cart->package_detail_id)
                                                    ->first();

                                                $Details=DB::table('package_details')
                                                    ->where('packageID',$PackageTourOne->packageID)
                                                    ->where('packageDateStart','>',date('Y-m-d'))
                                                    ->where('status','Y')
                                                    ->orderby('packageDateStart','asc')
                                                    ->get();

                                                $Price=DB::table('package_details_sub')
                                                    ->where('psub_id',$cart->tour_type)
                                                    ->where('packageDescID',$cart->package_detail_id)
                                                    ->first();

                                               //dd($Price);

                                                $Price_now=0;
                                                $Price_up=0;
                                                $Price_down=0;

                                                $data_target=null;

                                                $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                                                // dd($pricePro);
                                                $promotion_title=null;$every_booking=0;

                                                $promotion=\App\PackagePromotion::where('packageDescID',$Detail->packageDescID)->active()
                                                    ->orderby('promotion_date_start','asc')
                                                    ->first();

                                                $data_target=null;
                                                if($promotion && $promotion->promotion_operator!='Mod'){
                                                    $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));
                                                }

                                                $booking=DB::table('package_booking_details')
                                                    ->where('package_id',$PackageTourOne->packageID)
                                                    ->where('package_detail_id',$Detail->packageDescID)
                                                    ->sum('number_of_person');

                                                $person_booking=$Detail->NumberOfPeople;

                                                if($booking){
                                                    $person_booking=$Detail->NumberOfPeople-$booking;
                                                }

                                                $price_promotion=round($Price->Price_by_promotion);
                                                $price_system=round($Price->price_system_fees);
                                                ?>

                                                @if($promotion)
                                                    <div class="price-wrap h5">
                                                        <var class="price h3 text-warning">
                                                            <small class="text-success">{{trans('common.start_price')}}</small>
                                                            <span class="currency">{{$current->currency_symbol}}</span>
                                                            <span class="num">{{number_format($price_promotion)}}</span>
                                                            <small>/{{trans('common.per_person')}}</small>
                                                        </var>
                                                        @if($price_promotion!=$price_system)
                                                            <br>
                                                            <del class="price-old">{{trans('common.normal_price')}} {{$current->currency_symbol.number_format($price_system)}}</del>
                                                            <span class="price-save">
                                                                {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                {{$current->currency_symbol.number_format($price_system-$price_promotion)}}
                                                            </span>
                                                        @endif

                                                        @if($data_target != null)
                                                            <BR>
                                                            <span class="price-save-date">
                                                                <span id="clockdiv{{$cart->booking_cart_detail_id}}">({{trans('common.this_price')}}
                                                                    <span class="days"></span> {{trans('common.day')}}
                                                                    <span class="hours"></span>:
                                                                    <span class="minutes"></span>:
                                                                    <span class="seconds"></span>, {{$Detail->NumberOfPeople}} {{trans('common.only_last_place')}})</span>
                                                                </span>
                                                            <script language="JavaScript">
                                                                initializeClock('clockdiv{{$cart->booking_cart_detail_id}}', new Date('{{$data_target}}'));
                                                            </script>
                                                        @else
                                                            <BR>
                                                            <span class="price-save-date">({{trans('common.this_price_is_only')}} {{$person_booking}} {{trans('common.only_last_place')}})</span>
                                                        @endif
                                                        <hr>
                                                    </div>
                                                @else
                                                    <div class="price-wrap h5">
                                                        <var class="price h3 text-warning">
                                                            {{--<small class="text-success">{{trans('common.price')}}</small>--}}
                                                            <span class="num">{{$current->currency_symbol}}{{number_format($price_system)}}</span>
                                                            <small>/{{trans('common.per_person')}}</small>
                                                        </var>
                                                    </div>
                                                @endif

                                                <div class="col-lg-12">
                                                    {{trans('common.package_tour_by')}} <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a>

                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="pull-right-sm">
                                                    @if(Auth::check())
                                                        <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="" data-id="{{$Detail->packageDescID}}" class="add-wishlist btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a>
                                                    @else
                                                        <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="{{url('ajax/login_wishlist')}}" data-toggle="modal" data-target="#popupForm" class="btn btn-outline-success" > <i class="fa fa-heart"></i></a>
                                                    @endif
                                                    <button id="remove-cart" data-id="{{$cart->booking_cart_id}}" data-name="{{$cart->package_id}}" class="btn remove-cart btn-outline-danger"> × {{trans('common.remove')}}</button>
                                                    <br>  <br>
                                                    <a href="{{url('home/details/'.$PackageTourOne->packageID)}}" class="btn btn-block btn-outline-success">  <i class="fa fa-plus"></i> {{trans('common.buy_more')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($CartDetails)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- price-wrap .// -->
                                                <table class="table table-borderless">
                                                    <thead>
                                                    <tr>
                                                        <td><strong>{{trans('common.travel_time')}}</strong></td>
                                                        <td><strong>{{trans('common.tour_type')}}</strong></td>
                                                        <td><strong>{{trans('common.amount')}}</strong></td>
                                                        <td></td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $price_service=0;$count=0;$deposit=0;$Price_visa=0;?>

                                                    @foreach($CartDetails as $cart_detail)
                                                        <?php
                                                        $count++;
                                                        $PackageDetails=DB::table('package_details')
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->where('packageDateStart','>=',date('Y-m-d'))
                                                            ->where('status','Y')
                                                            ->get();
                                                        $max_person=$PackageDetails[0]->NumberOfPeople;

                                                        $booked=DB::table('package_booking_details')->where('package_detail_id',$cart_detail->package_detail_id)->get()->sum('number_of_person');

                                                        $empty_seat=$max_person-$booked;

                                                        $CheckOff=DB::table('package_details')
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->where('packageDateStart','<=',date('Y-m-d'))
                                                            ->first();

                                                        //              dd($PackageDetails);

                                                        $Category=DB::table('package_details_sub')
                                                            ->where('packageID',$cart_detail->package_id)
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->get();

                                                        $Additional=DB::table('package_additional_services')
                                                            ->where('packageID',$cart_detail->package_id)
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->where('status','Y')
                                                            ->get();

                                                        $PromotionCart=\App\PackagePromotion::where('packageDescID',$cart_detail->package_detail_id)->active()
                                                            ->where('psub_id',$cart_detail->tour_type)
                                                            ->orderby('promotion_date_start','asc')
                                                            ->first();
                                                        // dd($PromotionCart);

                                                        $Price=DB::table('package_details_sub')
                                                            ->where('psub_id',$cart_detail->tour_type)
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->first();

                                                        $PackageDetailsOne=DB::table('package_details')
                                                            ->where('packageDescID',$cart_detail->package_detail_id)
                                                            ->first();

                                                        if($PackageDetailsOne->season=='Y'){
                                                            $orderby="desc";
                                                        }else{
                                                            $orderby="asc";
                                                        }

                                                        $Condition=DB::table('condition_in_package_details as a')
                                                            ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                                            ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                                            ->where('b.condition_group_id','1')
                                                            ->where('b.formula_id','>',0)
                                                            ->where('a.packageID',$cart_detail->package_id)
                                                            ->orderby('c.value_deposit',$orderby)
                                                            ->first();

                                                        if($Condition){
                                                            $deposit=$Condition->value_deposit;
                                                        }

                                                        $CheckAddition=DB::table('package_additional_services')->where('id',$Price->additional_id)->where('condition_check_first','Y')->first();
                                                        ?>

                                                        @if($PackageDetails->count())
                                                            <tr>
                                                                <td style="width: 30%">
                                                                    <select class="form-control" name="update_tour_time" required>
                                                                        <option value="">{{trans('common.traveling_date')}}</option>
                                                                        @foreach($PackageDetails as $detail)
                                                                            <?php
                                                                            $st=explode('-',$detail->packageDateStart);
                                                                            $end=explode('-',$detail->packageDateEnd);
                                                                            if($st[1]==$end[1]){
                                                                                $date=\Date::parse($detail->packageDateStart);
                                                                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                                // dd($end[0]);
                                                                            }else{
                                                                                $date=\Date::parse($detail->packageDateStart);
                                                                                $date1=\Date::parse($detail->packageDateEnd);
                                                                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                                            }

                                                                            ?>
                                                                            <option value="{{$detail->packageDescID}}" {{$detail->packageDescID==$cart_detail->package_detail_id?'selected':''}} > {{$package_date}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control cart-type-update" name="update_tour_type" id="update_tour_type" data-id="{{$cart_detail->booking_cart_detail_id}}" required >
                                                                        <option value=""> {{trans('common.choose').trans('common.tour_type')}}</option>
                                                                        @foreach($Category as $rowsCate)
                                                                            <option value="{{$rowsCate->psub_id}}" {{$rowsCate->psub_id==$cart_detail->tour_type?'selected':''}}> {{$rowsCate->TourType}}
                                                                                {{--{{$current->currency_symbol.number_format($rowsCate->Price_by_promotion)}}--}}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control cart-amount-update" name="update_number_of_person" id="update_number_of_person" data-id="{{$cart_detail->booking_cart_detail_id}}" required>
                                                                        <option value=""> {{trans('common.amount')}}</option>
                                                                        @for($i=1;$i<=$empty_seat;$i++)
                                                                            <option value="{{$i}}" {{$i==$cart_detail->number_of_person?'selected':''}}> {{$i}} {{trans('common.person')}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <button data-id="{{$cart_detail->booking_cart_detail_id}}" class="btn btn-outline-danger btn-sm delete-detail"> × {{trans('common.delete')}}</button>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $Addition_price=0;
                                                            $AdditionalCart=DB::table('package_booking_cart_additional')
                                                                ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                                                ->get();
                                                            ?>

                                                            @if($Additional->count()>0)
                                                                <tr>
                                                                    <td><span class="btn btn-block btn-default text-success">{{trans('common.additional')}}</span> </td>
                                                                    <td colspan="2">
                                                                        @if($AdditionalCart)
                                                                            @foreach($AdditionalCart as $rowAdd)
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 bg-default">
                                                                                        <label class="form-control"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</label>
                                                                                    </div>
                                                                                    @if($rowAdd->bind_package!='Y' || $cart_detail->number_of_person>1)
                                                                                        <div class="col-sm-2">
                                                                                            <button data-id="{{$rowAdd->id}}" class="addition-delete btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>
                                                                                            {{--<button class="btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>--}}
                                                                                        </div>
                                                                                    @else
                                                                                        @if($CheckAddition)
                                                                                            <div class="col-sm-6 bg-default">
                                                                                                <label class="container-cart" style="margin-top: 5px">
                                                                                                    <input type="checkbox" class="someone_share" name="someone_to_share" value="Y" data-id="{{$rowAdd->id}}" {{$rowAdd->need_someone_share=='Y'?'checked':''}}>
                                                                                                    <label>
                                                                                                        <strong>{{trans('common.can_share')}}</strong>
                                                                                                        <strong class="text-danger" data-toggle="tooltip" data-placement="top" title="หากท่านเลือกออฟชั่นนี้ ท่านไม่จำเป็นต้องชำระค่าบริการเสริมนี้ จนกว่าทางผู้ขายแจ้งท่านว่าไม่สามารถหาผู้พักร่วมให้ท่านได้"><i class="fa fa-info-circle"></i> </strong>
                                                                                                    </label>
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        @endif
                                                                                    @endif
                                                                                </div>
                                                                                <?php
                                                                                    if($rowAdd->need_someone_share!='Y'){
                                                                                        $Addition_price+=$rowAdd->price_service;
                                                                                    }
                                                                                ?>
                                                                            @endforeach
                                                                        @endif
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                <select class="form-control addition_cart" data-id="{{$cart_detail->booking_cart_detail_id}}" id="addition_cart" name="addition_cart">
                                                                                    <option value=""> {{trans('common.choose')}} </option>
                                                                                    @foreach($Additional as $rowAdd)
                                                                                        <option value="{{$rowAdd->id}}"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            {{--<div class="col-sm-2">--}}
                                                                                {{--<button  class="btn btn-outline-success btn-sm"> + {{trans('common.add')}}</button>--}}
                                                                            {{--</div>--}}
                                                                        </div>
                                                                    </td>
                                                                    {{--<td>--}}
                                                                    {{--<a href="{{url('ajax/additional/'.$cart_detail->booking_cart_detail_id)}}" data-toggle="" da class="btn btn-outline-success btn-sm"> + เพิ่ม</a>--}}
                                                                    {{--<a href="#" class="btn btn-outline-warning btn-sm"> × ลบ</a>--}}
                                                                    {{--</td>--}}
                                                                    <td></td>
                                                                </tr>
                                                            @endif
                                                            <tr><td colspan="5"><hr></td></tr>


                                                            <?php
                                                            $price_promotion=round($Price->Price_by_promotion);
                                                            $price_system=round($Price->price_system_fees);
                                                            ?>
                                                            @if($PackageTourOne->have_visa=='1')
                                                                <tr>
                                                                    <td colspan="2" class="text-right" style="color: #5B6B81">
                                                                        {{trans('common.number_of_people_who_need_a_visa')}}<BR>
                                                                        <small class="text-danger"><i>{{$PackageTourOne->condition_visa}}</i></small>
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <select class="form-control cart-visa-update" name="update_visa" id="update_visa" data-id="{{$cart_detail->booking_cart_detail_id}}" required >
                                                                            <option value=""> {{trans('common.choose')}}</option>
                                                                            @for($i=1;$i<=$cart_detail->number_of_person;$i++)
                                                                                <option value="{{$i}}" {{$i==$cart_detail->number_of_need_visa?'selected':''}}> {{$i}} {{trans('common.person')}} </option>
                                                                            @endfor
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr><td colspan="5"><hr></td></tr>
                                                                <?php
                                                                $Price_visa=$Price->price_for_visa*$cart_detail->number_of_need_visa;
                                                                ?>
                                                            @endif

                                                            <tr>
                                                                <td colspan="2"  style="color: #5B6B81">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            @if(isset($PromotionCart))
                                                                              <tr>
                                                                                  <td><small><i>*{{$PromotionCart->promotion_title}}</i></small> <strong>{{trans('common.normal_price')}} <del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del></strong></td>
                                                                              </tr>
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td colspan="3">
                                                                    <table class="table">
                                                                        <tbody>

                                                                        @if($PromotionCart)
                                                                            @if($price_promotion==$price_system)
                                                                                @if($Addition_price>0)
                                                                                    <tr>
                                                                                        <td>{{trans('common.value_added_service')}} </td><td><strong> {{$current->currency_symbol.number_format($Addition_price)}} x 1</strong></td>
                                                                                    </tr>
                                                                                @endif

                                                                                @if($PackageTourOne->have_visa=='1')

                                                                                    <tr>
                                                                                        <th style="width:50%">{{$Price->price_visa_details}} </th>
                                                                                        <td><strong> {{$current->currency_symbol.number_format($Price->price_for_visa)}} x {{$cart_detail->number_of_need_visa}}</strong></td>
                                                                                    </tr>
                                                                                @endif


                                                                                <tr>
                                                                                        <td><h4 style="color:orangered">{{trans('common.totals')}} </h4></td><td><h4 style="color:orangered"> {{$current->currency_symbol.number_format($price_promotion*$cart_detail->number_of_person+$Addition_price+$Price_visa)}}</h4></td>
                                                                                    </tr>
                                                                            @else
                                                                                <?php
                                                                                if($cart_detail->item_status=='Y'){
                                                                                    $discount+=round(($price_system-$price_promotion)*$cart_detail->number_of_person);
                                                                                }
                                                                                ?>
                                                                                {{--<tr>--}}
                                                                                    {{--<td> <strong>{{trans('common.normal_price')}}</strong></td>--}}
                                                                                    {{--<td><strong><del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del></strong></td>--}}
                                                                                {{--</tr>--}}
                                                                               <tr>
                                                                                   <td><strong>{{trans('common.sale')}}</strong></td>
                                                                                   <td><strong>{{$current->currency_symbol.number_format($price_promotion)}} x {{$cart_detail->number_of_person}}</strong></td>
                                                                               </tr>


                                                                                @if($Addition_price>0)
                                                                                    <tr>
                                                                                        <td><strong>{{trans('common.value_added_service')}}</strong></td>
                                                                                        <td><strong>{{$current->currency_symbol.number_format($Addition_price)}} x 1</strong></td>
                                                                                    </tr>
                                                                                @endif
                                                                                @if($PackageTourOne->have_visa=='1')

                                                                                    <tr>
                                                                                        <th style="width:50%">{{$Price->price_visa_details}} </th>
                                                                                        <td><strong> {{$current->currency_symbol.number_format($Price->price_for_visa)}} x {{$cart_detail->number_of_need_visa}}</strong></td>
                                                                                    </tr>
                                                                                @endif

                                                                                <tr>
                                                                                    <td><h4 style="color:orangered">{{trans('common.totals')}} </h4></td>
                                                                                    <td><h4 style="color:orangered">{{$current->currency_symbol.number_format($price_promotion*$cart_detail->number_of_person+$Addition_price+$Price_visa)}}</h4></td>
                                                                                </tr>

                                                                            @endif
                                                                            <?php $price=$price_promotion?>
                                                                        @else
                                                                            <tr>
                                                                                <td><strong>{{trans('common.sale')}}</strong></td>
                                                                                <td><strong>{{$current->currency_symbol.number_format($price_system)}} x {{$cart_detail->number_of_person}}</strong></td>
                                                                            </tr>

                                                                            @if($Addition_price>0)
                                                                                <tr>
                                                                                    <td><strong>{{trans('common.value_added_service')}} </strong></td>
                                                                                <td><strong>{{$current->currency_symbol.number_format($Addition_price)}} x 1</strong></td>
                                                                                </tr>

                                                                            @endif
                                                                            @if($PackageTourOne->have_visa=='1')

                                                                                <tr>
                                                                                    <th style="width:50%">{{$Price->price_visa_details}} </th>
                                                                                    <td><strong> {{$current->currency_symbol.number_format($Price->price_for_visa)}} x {{$cart_detail->number_of_need_visa}}</strong></td>
                                                                                </tr>
                                                                            @endif

                                                                            <tr>
                                                                                <td><h4 style="color:orangered">{{trans('common.totals')}} </h4><td><h4 style="color:orangered"> {{$current->currency_symbol.number_format($price_system*$cart_detail->number_of_person+$Addition_price+$Price_visa)}}</h4></td>
                                                                            </tr>
                                                                            <?php $price=$price_system?>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    @if($Price->price_include_vat!='Y')
                                                                                        <small> <i>({{trans('common.this_price_not_include_vat')}})</i></small>
                                                                                    @else
                                                                                        <small> <i>({{trans('common.this_price_include_vat')}})</i></small>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>

                                                                        @endif

                                                                        </tbody></table>

                                                                </td>
                                                            </tr>

                                                            @if($CartDetails->count()!=$count)
                                                                <tr><td  colspan="4"><hr></td></tr>
                                                            @endif
                                                            <BR>
                                                            <?php
                                                            if($cart_detail->item_status=='Y'){
                                                                $pricetotal+=round($price*$cart_detail->number_of_person+$Addition_price+$Price_visa);
                                                                if($Price->price_include_vat!='Y'){
                                                                    $pricetotal_no_tax+=round($price*$cart_detail->number_of_person+$Addition_price+$Price_visa);
                                                                }else{
                                                                    $pricetotal_tax+=round($price*$cart_detail->number_of_person+$Addition_price+$Price_visa);
                                                                }
                                                            }
                                                            $pricedeposittotal+=round($cart_detail->number_of_person*$deposit);
                                                            ?>

                                                        @endif

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <BR>
                            <?php
                                $currentPackage=$cart->package_id;
//                                $include_vat=$cart_detail->price_include_vat;
                                if($Price->price_include_vat=='Y'){
                                    $price_include_vat='Y';
                                }
                                $tax=0;
                            ?>
                        @endforeach


                    </div>

                    @if($Carts->count()>0)
                        <div class="col-md-3">
                            {{--<p class="alert alert-success">Add USD 5.00 of eligible items to your order to qualify for FREE Shipping. </p>--}}
                            <h3 class="alert alert-success"><i class="fa fa-check"></i> {{trans('common.order_summary')}}  </h3>
                            <div class="itemside mb-3">
                                @if($price_include_vat=='Y')
                                <div class="col-sm-8"><strong>{{trans('common.totals').' ('.trans('common.include_tax')}} 7%) :</strong> </div>
                                @else
                                    <div class="col-sm-8"><strong>{{trans('common.totals')}} :</strong> </div>
                                @endif
                                <div class="col-sm-4 text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal)}}</strong></div>
                            </div>
                            {{--<div class="itemside mb-3">--}}
                                {{--<div class="col-sm-8">{{trans('common.system_fee')}}:</div>--}}
                                {{--<div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($System_fee)}}</div>--}}
                            {{--</div>--}}

                            <div class="itemside mb-3">
                                <div class="col-sm-8">{{trans('common.discount')}}:</div>
                                <div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($discount)}}</div>
                            </div>
                            @if($price_include_vat!='Y')
                                <?php
                                $tax=$pricetotal_no_tax*.07;
                               // dd($tax);
                                ?>
                                <div class="itemside mb-3">
                                    <div class="col-sm-8">{{trans('common.tax')}} 7%:</div>
                                    <div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($tax)}}</div>
                                </div>
                                <dl class="dlist-align h4">
                                    <dt>{{trans('common.totals')}}:</dt>
                                    <dd class="text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal_no_tax+$pricetotal_tax+$tax)}}</strong></dd>
                                </dl>
                            @else
                                <dl class="dlist-align h4">
                                    <dt>{{trans('common.totals')}}:</dt>
                                    <dd class="text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal+$tax)}}</strong></dd>
                                </dl>
                            @endif

                            <hr>
                            <div class="row">
                           <div class="col-sm-12">
                               <div class="col-sm-8 h5">{{trans('common.tour_deposit')}} </div>
                               <div class="col-sm-4 text-right h4"><strong class="text-warning">{{$current->currency_symbol}}{{number_format($pricedeposittotal)}}</strong></div>
                           </div>
                           </div>
                            <hr>

                            {{--<figure class="itemside mb-3">--}}
                                {{--<aside class="aside"><img src="{{asset('package/images/icons/pay-visa.png')}}"></aside>--}}
                                {{--<div class="text-wrap small text-muted">--}}
                                    {{--Pay 84.78 AED ( Save 14.97 AED )--}}
                                    {{--By using ADCB Cards--}}
                                {{--</div>--}}
                            {{--</figure>--}}

                            <figure class="itemside mb-3 text-danger">
                                * ค่ามัดจำต้องชำระหลังจากยืนยันรายการภายใน  <br>
                                วันที่กำหนดในใบเสร็จ ที่ทางบริษัทออกให้ไป
                            </figure>
                            <hr>
                            @if(Auth::check())
                                @if($pricetotal>0)
                                    <a href="{{url('booking/continuous/invoice')}}" class="btn btn-success btn-block btn-lg">
                                        <i class="fa fa-credit-card"></i> {{trans('common.confirm_order')}}
                                    </a>
                                    {{--<a href="{{url('booking/continuous/payment')}}" class="btn btn-success btn-block btn-lg">--}}
                                        {{--<i class="fa fa-credit-card"></i> {{trans('common.choose_payment')}}--}}
                                    {{--</a>--}}
                                @endif
                            @else
                                @if($pricetotal>0)
                                    <a href="{{url('register/2')}}" class="btn btn-success btn-block btn-lg">
                                        <i class="fa fa-credit-card"></i> {{trans('common.choose_payment')}}
                                    </a>
                                @endif
                            @endif

                            <a href="{{url('home/package_order/more')}}" class="btn btn-info btn-block btn-lg">
                                <i class="fa fa-reply"></i> {{trans('common.buy_more_packages')}}
                            </a>
                            @if($AddressBook)
                                <hr>
                                <div class="alert alert-info">
                                    <span class="pull-right-sm">
                                        <a href="{{url('booking/show/address')}}">
                                        <i class="fa fa-edit"></i> {{trans('common.edit')}}
                                        </a>
                                    </span>
                                    <h5><i class="fa fa-map-marker"></i> <strong>{{trans('common.address_for_invoice')}}</strong></h5>
                                   {!! $AddressBook->address_show !!}
                                </div>
                            @endif

                        </div>
                    @endif
                    </div>
                @else
                    <div class="col-sm-12 text-center"><h1></h1>
                        <h1><i class="fa fa-shopping-cart"></i> Your cart is empty.</h1>
                        <div><a href="{{url('home/package/'.Session::get('group'))}}" class="btn btn-default btn-lg">Make your booking.</a>

                        </div>

                    </div>
                @endif
            </div>
        </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->


    <script type="text/javascript">
        $('body').on('click','.btn-success',function () {
            $('#divLoading').show();
            $(this).attr('disabled', true).append(' <i class="fa fa-spinner fa-pulse "></i>');
        });
        $('body').on('click','.delete-detail',function () {
            if(confirm('Confirm delete this cart detail?')){
                alert('test'+step);
                $('#divLoading').show();
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        $('#divLoading').hide();
                        $("#cart-detail").html(data);
                    }
                });
            }
        });


        $('body').on('click','.someone_share',function(){
            $('#divLoading').show();
            if($(this).is(":checked")){
                var status='Y';
            }else{
                var status='N';
            }
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/set/someone_share',
                data:{'id':$(this).data('id'),'status':status,'step':'2'},
                success:function (data) {
                    count_item_cart();
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
                }
            });
        });


        $(document).ready(function(){
            var counter = 1;
            $("#add").click(function () {
                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);

                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');


                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });



        });
    </script>


@endsection