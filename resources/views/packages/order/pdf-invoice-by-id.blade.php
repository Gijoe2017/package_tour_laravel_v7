
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    table {
        font-family: "THSarabunNew";
        border-collapse: collapse;
    }

    th, td {
        border-bottom: 1px solid #ddd;
    }
    .page-break {
        page-break-after: always;
    }

    .page-header{
        text-align: center;
    }
</style>




<?php
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Invoice->invoice_package_id)
            ->first();

        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();
       // dd($media);
        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();
        // dd($BusinessInfo1);
        $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
        if(!$country){
            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
        }

        $states=DB::table('states')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$states){
            $states=DB::table('states')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('language_code','en')
                ->first();
        }
        $city=DB::table('cities')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('city_id',$BusinessInfo->city_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$city){
            $city=DB::table('cities')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('city_id',$BusinessInfo->city_id)
                ->where('language_code','en')
                ->first();
        }

        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();

        $Details=DB::table('package_booking_details as a')
            ->where('a.package_id',$Invoice->invoice_package_id)
            ->where('a.booking_id',$Invoice->invoice_booking_id)
            ->where('a.timeline_id',$Invoice->invoice_timeline_id)
            ->get();

        ?>

<table class="table" width="100%">
    <tr>
        <td colspan="3">
            <h2 class="page-header" >
                @if($Invoice->invoice_type=='1')
                    {{trans('common.invoice_deposit')}}
                @else
                    {{trans('common.invoice_balance')}}
                @endif
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="3">
               <table class="table" width="100%">
                    <tr>
                        <td width="60%">
                            @if($media!=null)
                                <img class="logo-invoice" style="height: 110px" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @else
                                <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @endif
                        </td>
                        <td>
                            <b>{{trans('common.invoice_no')}}:</b> #{{$Invoice->invoice_id}}<br>
                            <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($Invoice->invoice_date))}}<br>
                            <b>{{trans('common.order_id')}}:</b> #{{$Invoice->invoice_booking_id}}<br>
                            {{--<b>{{trans('common.reference')}}:</b> ACB11<br>--}}
                            <?php
                                $invoice_status=2;
                                if($Invoice->invoice_status==2){
                                    $invoice_status=4;
                                }
                                $Status=DB::table('booking_status')->where('booking_status',$invoice_status)->first();
                            ?>
                           <strong>
                                @if($Invoice->invoice_type==2)
                                   @if(($Invoice->invoice_status==1))
                                       {{trans('common.status')}}: {{trans('common.wait_pay_balance')}}
                                   @else
                                       {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}}
                                   @endif
                                @else
                                   @if(($Invoice->invoice_status==1))
                                       {{trans('common.status')}}: {{trans('common.wait_pay_deposit')}}
                                   @else
                                       {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}}
                                   @endif
                                @endif

                           </strong>
                        </td>
                    </tr>
                </table>

        </td>
    </tr>
    <tr>
        <td colspan="3" >
            <table class="table" width="100%">
                <tr>
                    <td width="50%">
                        <strong>Billing From/จาก:  </strong><br>
                        <address>
                            {{$BusinessInfo1->legal_name}}<br>
                            {{$BusinessInfo1->address}}<br>
                            {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                            {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                        </address>
                    </td>
                    <td >
                        <strong>Billing To/ถึง:</strong>
                        @if($AddressBook)
                            <address>
                                {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                {!! $AddressBook->address_show !!}<br>
                                {{--{{$AddressBook->entry_city}}, {{$AddressBook->country}} {{$AddressBook->entry_postcode}}<br>--}}
                                {{trans('common.phone')}}: {{$AddressBook->entry_phone?$AddressBook->entry_phone:'-'}}<br>
                                {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                            </address>
                        @endif
                    </td>
                </tr></table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table style="width: 100%">
                <thead>
                <tr>
                    <th>{{trans('common.items')}}</th>
                    <th>{{trans('common.description')}}</th>
                    <th>{{trans('common.unit_price')}}</th>
                    <th>{{trans('common.unit')}}</th>
                    <th align="right">{{trans('common.unit_total')}}</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;$TotalsAll=0;$SubTotals=0;$Tax=0;$discount=0;$pay_more=0;$price_include_vat=''; $Deposit=0;?>

                @foreach($Details as $rows)
                    <?php
                    $Deposit_title=0;$AdditionalPrice=0;$PriceVisa=0;
                    $Timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                    // dd($Timeline);

                    $Deposit_title=$rows->deposit_price;
                    $Deposit+=$rows->deposit_price*$rows->number_of_person;

                    $Additional=DB::table('package_booking_additional')
                        ->where('booking_detail_id',$rows->booking_detail_id)
                        ->get();

                    $Promotion=DB::table('package_booking_promotion')
                        ->where('booking_detail_id',$rows->booking_detail_id)
                        ->first();
                    $promotion_title='';
                    if($Promotion){
                        $promotion_title=$Promotion->promotion_title;
                        if($Promotion->promotion_operator=='Between'){
                            if($Promotion->promotion_unit=='%'){
                                $discount=$rows->booking_normal_price*$Promotion->promotion_value/100;
                            }else{
                                $discount=$rows->booking_normal_price-$Promotion->promotion_value;
                            }
                        }else{
                            if($Promotion->promotion_operator2=='Up'){
                                if($Promotion->promotion_unit=='%'){
                                    $pay_more=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                }else{
                                    $pay_more=$Promotion->promotion_value;
                                }
                            }else{
                                if($Promotion->promotion_unit=='%'){
                                    $discount=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                }else{
                                    $discount=$rows->booking_realtime_price-$Promotion->promotion_value;
                                }
                            }
                        }
                    }

                    ?>
                    @if($Invoice->invoice_type==1)
                        <tr>
                            <td align="center">{{$i++}}</td>
                            <td>{!! $rows->package_detail_title !!}</td>
                            <td align="right">{{$Invoice->currency_symbol.number_format($Deposit_title)}}</td>
                            <td align="center">{{$rows->number_of_person}}</td>
                            <td style="text-align: right">
                                {{$Invoice->currency_symbol.number_format($Deposit_title*$rows->number_of_person)}}
                            </td>
                        </tr>
                        <?php

                        $TotalsAll+=round($Deposit_title*$rows->number_of_person);
                        ?>
                    @else
                        <tr>
                            <td align="center">{{$i++}}</td>
                            <td>
                                {!! $rows->package_detail_title !!}<BR>
                                @if($Deposit_title>0)
                                <span class="text-danger"> {{trans('common.deposit')}} {{$rows->tour_type}}: {{$Invoice->currency_symbol.number_format($Deposit_title)}} x {{$rows->number_of_person}}</span>
                                @endif
                            </td>
                            <td align="right">

                                @if($pay_more>0)
                                    <?php
                                    $Price_sub=$rows->booking_realtime_price+$pay_more;
                                    ?>
                                    {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}+{{$pay_more}}
                                @elseif($discount>0)
                                    <?php
                                    $Price_sub=$rows->booking_normal_price-$discount;
                                    ?>
                                    <del>{{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}</del>
                                    {{$Invoice->currency_symbol.number_format($rows->booking_normal_price-$discount)}}
                                @else
                                    <?php
                                    $Price_sub=$rows->booking_normal_price;
                                    ?>
                                    {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}
                                @endif

                            </td>
                            <td align="center">{{$rows->number_of_person}}</td>
                            <td style="text-align: right">
                                {{$Invoice->currency_symbol.number_format($Price_sub*$rows->number_of_person)}}
                            </td>
                        </tr>
                        @if($Additional)
                            @foreach($Additional as $rowA)
                                <tr>
                                    <td align="center">{{$i++}}</td>
                                    <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                    <td align="right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                    <td align="center">1</td>
                                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                </tr>
                                <?php
                                $AdditionalPrice+=$rowA->price_service;
                                ?>
                            @endforeach
                        @endif

                        @if($rows->price_for_visa)
                            <tr>
                                <td align="center">{{$i++}}</td>
                                <td><strong>{{$rows->price_visa_details}}</strong></td>
                                <td align="right">{{$Invoice->currency_symbol.number_format($rows->price_for_visa)}}</td>
                                <td align="center">{{$rows->number_of_need_visa}}</td>
                                <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rows->price_for_visa*$rows->number_of_need_visa)}}</td>
                            </tr>
                           <?php $PriceVisa+=$rows->price_for_visa*$rows->number_of_need_visa?>
                        @endif

                        <?php
                        $SubTotals=round($Price_sub*$rows->number_of_person)+$AdditionalPrice+$PriceVisa;
                        $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                        if($rows->price_include_vat=='Y'){
                            $price_include_vat=$rows->price_include_vat;
                        }else{
                            $Tax+=round($SubTotals*.07);
                        }
                        $TotalsAll+=$SubTotals;
                        ?>
                    @endif

                @endforeach

                <?php
                $Totals=$TotalsAll+$Tax;
                ?>
                @if($Invoice->invoice_type==1)
                    <tr>
                        <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                        <td  style="text-align: right">{{$Invoice->currency_symbol.number_format($TotalsAll)}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: right"><strong>{{trans('common.total_amount')}}:</strong></td>
                        <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Totals)}} </strong></td>
                    </tr>
                @else
                            <tr>
                                <td colspan="4" style="text-align: right"><strong>
                                    @if($price_include_vat=='Y')
                                     {{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)
                                    @else
                                     {{trans('common.subtotal')}}
                                    @endif
                                    </strong>
                                </td>
                                <td  style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Totals)}}</strong></td>
                            </tr>
                            @if($price_include_vat!='Y')

                                <tr>
                                    <td colspan="4" style="text-align: right"><strong>({{trans('common.include_tax')}} 7%):</strong></td>
                                    <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Tax)}} </strong></td>
                                </tr>
                            @endif
                            @if($discount>0)
                                <tr>
                                    <td></td>
                                    <td colspan="2" >{{$promotion_title}}</td>
                                    <td  style="text-align: right">{{trans('common.discount')}}</td>
                                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($discount)}}</td>
                                </tr>
                            @endif

                            <tr>
                                <td colspan="4" style="text-align: right">{{trans('common.deposit')}}</td>
                                <td style="text-align: right">-{{$Invoice->currency_symbol.number_format($Deposit)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.total_amount')}}:</strong></td>
                                <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Totals-$Deposit)}} </strong></td>
                            </tr>
                 @endif
                </tbody>
            </table>

        </td>
    </tr>
  
</table>




