@extends('layouts.package.master')

@section('program-highlight')
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap2.min.css?v1')}}">
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- Bootstrap time Picker -->
    <!-- bootstrap datepicker -->
    <!-- Font Awesome -->
    {{--<link rel="stylesheet" href="{{asset('member/assets/')}}/font-awesome/css/font-awesome.min.css">--}}
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">

    <style type="text/css">

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;

        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }


    </style>

    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">

                <div class="col-md-12 show-invoice">
                    @if(Session::has('message'))
                        <div class="alert alert-success">{{Session::get('message')}}</div>
                    @endif
                <form class="form-group" method="post" action="{{action('Package\PaymentController@store')}}" enctype="multipart/form-data">
                {!! csrf_field() !!}
               <input type="hidden" name="type" id="type" value="{{$Invoice->invoice_type}}">
                <div class="col-md-12">
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">ฟอร์มแจ้งการชำระเงิน</h3>
                            <hr>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('common.invoice_no')}} <span class="text-danger">*</span> </label>
                                <select name="invoice_id" id="invoice_id" class="form-control" required>
                                    @foreach($InvoiceAll as $rows)
                                    <option value="{{$rows->invoice_id}}" {{$rows->invoice_id==$Invoice->invoice_id?'selected':''}}>#{{sprintf('%07d',$rows->invoice_id)}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>{{trans('common.amount')}} <span class="text-danger">*</span></label>
                                <input type="text" name="amount" value="{{number_format($Amount)}}" class="form-control" required>
                            </div>
                            <!-- /.form group -->
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date <span class="text-danger">*</span> :</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="date_transfer" class="form-control pull-right" value="{{date('Y-m-d')}}" id="datepicker" required>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Time <span class="text-danger">*</span> :</label>
                                        <div class="input-group">
                                            <input type="text" name="time_transfer" class="form-control timepicker" required>
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('common.bank')}} <span class="text-danger">*</span></label>
                                    <select  name="bank" class="form-control" required>
                                    @foreach($BankInfo as $rows)
                                        <option value="{{$rows->id}}">
                                            {{$rows->bank_name}}({{$rows->country}}) {{trans('common.account_name')}}: {{$rows->account_name}}
                                            {{trans('common.account_number')}}: {{$rows->bank_account_number}} {{trans('common.branch')}}: {{$rows->sub_bank}}
                                        </option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{trans('common.slip')}} <span class="text-danger">*</span></label>
                                    <input type="file" name="file" class="form-control" required>
                                </div>

                                <div class="form-group text-right">
                                    @if(!Session::has('message'))
                                        <button type="submit" name="submit" class="btn btn-lg btn-success"><i class="fa fa-save"></i> แจ้งการชำระเงิน </button>
                                    @else
                                        <a href="{{url('my/bookings')}}" class="btn btn-lg btn-warning"><i class="fa fa-reply"></i> Back to My Booking </a>
                                    @endif
                                </div>


                            </div>
                            <div class="col-md-6">
                                <div class="detail-invoice">
                                    <?php $Deposit=0;?>
                                    @foreach($Details as $rows)

                                    <?php
                                            $Package=DB::table('package_tour as a')
                                                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                                                ->where('a.packageID',$rows->package_id)
                                                ->first();
                                            $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

                                            $PackageDetailsOne=DB::table('package_details')
                                            ->where('packageDescID',$rows->package_detail_id)
                                            ->first();

                                        if($PackageDetailsOne->season=='Y'){
                                            $order_by="desc";
                                        }else{
                                            $order_by="asc";
                                        }
                                        $Condition=DB::table('condition_in_package_details as a')
                                            ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                            ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                            ->where('b.condition_group_id','1')
                                            ->where('b.formula_id','>',0)
                                            ->where('a.packageID',$rows->package_id)
                                            ->orderby('c.value_deposit',$order_by)
                                            ->first();

                                        if($Condition){
                                            $Deposit+=$Condition->value_deposit*$rows->number_of_person;
                                        }



                                    $st=explode('-',$rows->packageDateStart);
                                    $end=explode('-',$rows->packageDateEnd);

                                    if($st[1]==$end[1]){
                                        $date=\Date::parse($rows->packageDateStart);
                                        $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                        // dd($end[0]);
                                    }else{
                                        $date=\Date::parse($rows->packageDateStart);
                                        $date1=\Date::parse($rows->packageDateEnd);
                                        $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                    }
                                    ?>
                                    <p>
                                        <strong>
                                            @if($Invoice->invoice_type==1)
                                                {{trans('common.tour_deposit')}}
                                            @else
                                                {{trans('common.tour_balance')}}
                                            @endif
                                            {{$rows->TourType}}: {{trans('common.traveling_date')}} {{$package_date}}</strong><br>
                                        {{$Package->packageName}} <br>
                                        {{$rows->TourType }} / {{$rows->number_of_person}}

                                    </p>
                                        @endforeach
<hr>
                                        <h2 class="text-danger">{{$current->currency_symbol.number_format($Amount)}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                </div>
            </div>
        </div>
    </section>

    <!-- bootstrap datepicker -->
    <script src="{{asset('/member/assets/')}}/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="{{asset('/member/assets/')}}/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <script type="text/javascript">
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        });

        $('body').on('change','#invoice_id',function (e) {
                var invoice=e.target.value;
                var type=$('#type').val();
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/select/invoice',
                    data:{'invoice_id':invoice,'type':type},
                    success:function (data) {
                        $('.show-invoice').html(data);
                    }
                });
        });




    </script>


@endsection