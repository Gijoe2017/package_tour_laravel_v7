@extends('layouts.package.master')

@section('program-highlight')
    <!-- ========================= SECTION CONTENT ========================= -->
    <style>
        .example {
            margin:5px 20px 0 0;
        }
        .example input {
            display: none;
        }

        .example label {
            width: 100%;
            margin-right: 10px;
            display: inline-block;
            cursor: pointer;
        }

        .example2 {
            margin:5px 20px 0 0;
        }
        .example2 input {
            display: none;
        }
        .example2 label {
            width: 40%;
            margin-right: 10px;
            display: inline-block;
            cursor: pointer;
        }

        .ex1 span {
            display: block;
            padding: 8px 10px 8px 30px;
            border: 1px solid #ddd;
            border-radius: 5px;
            position: relative;
            transition: all 0.25s linear;
        }
        .ex1 span:before {
            content: '';
            position: absolute;
            left: 5px;
            top: 50%;
            -webkit-transform: translatey(-50%);
            transform: translatey(-50%);
            width: 18px;
            height: 18px;
            border-radius: 50%;
            background-color: #ddd;
            transition: all 0.25s linear;
        }
        .ex1 input:checked + span {
            background-color: #fff;
            box-shadow: 0 0 10px 2px rgba(0, 0, 0, 0.1);
        }
        .ex1 .red input:checked + span {
            color: red;
            border-color: red;
        }
        .ex1 .red input:checked + span:before {
            background-color: red;
        }
        .ex1 .blue input:checked + span {
            color: blue;
            border-color: blue;
        }
        .ex1 .blue input:checked + span:before {
            background-color: blue;
        }
        .ex1 .orange input:checked + span {
            color: orange;
            border-color: orange;
        }
        .ex1 .orange input:checked + span:before {
            background-color: orange;
        }




        .bank_error{
            border: solid 1px red;
            padding: 5px;
        }
    </style>

    <!-- daterange picker -->
    <!-- Bootstrap time Picker -->

    {{--<link rel="stylesheet" href="{{asset('member/assets/dateinput/lib/themes/default.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('member/assets/dateinput/lib/themes/default.date.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('member/assets/dateinput/lib/themes/default.time.css')}}">--}}


    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{asset('member/plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('member/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">


    <section class="section-content bg padding-y border-top">
        <div class="container">
            <div class="row">
                @if(!$check)
                <h2>{{trans('common.payment_notification')}} @if(Session::has('message')) <span class="text-success">{{Session::get('message')}}</span>  @endif</h2>
                <form method="post" id="form1" class="form-control form-booking" enctype="multipart/form-data" action="{{action('Package\OrderTourController@payment_notification')}}">
                    {!! csrf_field() !!}
                    <input type='hidden' name="type" id="type"  value="{{$Invoice->invoice_type}}">
                    <input type='hidden' name="bank_hidden" id="bank_hidden"  >
                     <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.order_id')}}<span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="invoice_booking_id" name="invoice_booking_id" value="{{$Invoice->invoice_booking_id}}" placeholder="{{trans('common.order_id')}}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        @if($Invoice->invoice_type==1)
                            <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.invoice_no')}}<span class="text-danger">*</span></strong> <BR>({{trans('common.value_deposit')}})</label>
                        @else
                            <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.invoice_no')}}<span class="text-danger">*</span></strong> <BR>({{trans('common.value_tour')}})</label>
                        @endif
                        <div class="col-sm-10">
                            <div class="example ex1">
                                {{--<label class="radio red">--}}
                                    {{--<input type="checkbox" name="invoice_all" id="invoice_all" value="all" data-id="{{$Invoice->invoice_booking_id}}"/>--}}
                                    {{--<span> {!! trans('common.invoice').'<BR> <strong>'.trans('common.all').'</strong>'!!} </span>--}}
                                {{--</label>--}}
                                <?php

                                $Detail=DB::table('package_booking_details as a')
//                                    ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//                                    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                                    ->where('a.package_id',$Invoice->invoice_package_id)
                                    ->where('a.booking_id',$Invoice->invoice_booking_id)
                                    ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                                    ->where('a.package_detail_id',$Invoice->invoice_package_detail_id)
                                    ->first();

                                if($Invoice->invoice_type==1){
                                    $status=trans('common.invoice_deposit');
                                }else{
                                    $status=trans('common.invoice_balance');
                                }
//                                        $tax=0;
//                                        if($Invoice->invoice_type==2){
//                                            if(!$Invoice->price_include_vat=='Y'){
//                                                $tax=$Invoice->price_system_fees*7/100;
//                                            }
//                                        }
                                $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice->invoice_id)->first();

                                    ?>
                                @if(!$check)
                                    <label class="radio red">
                                        <input type="checkbox" class="invoice" name="invoice_no[]" data-id="{{$Invoice->invoice_booking_id}}"  checked id="invoice_no" value="{{$Invoice->invoice_id}}"/>
                                        <span> {!! $status.':#'.$Invoice->invoice_id.'<BR>'.$Detail->package_detail_title.'<BR> <strong>'.trans('common.balance').' '.number_format($Invoice->invoice_amount).'</strong>'!!} </span>
                                    </label>
                                    {{--<label class="radio red">--}}
                                        {{--<input type="checkbox" class="invoice" name="invoice_no[]" data-id="{{$Invoice->invoice_booking_id}}" checked  id="invoice_no" value="{{$Invoice->invoice_id}}"/>--}}
                                        {{--<span> {!! trans('common.invoice_no').':#'.$Invoice->invoice_id.'<BR> <strong>'.trans('common.balance').' '.number_format($Invoice->invoice_amount+$tax).'</strong>'!!} </span>--}}
                                    {{--</label>--}}
                                @endif

                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.transfer_amount')}}<span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="transfer_amount" name="transfer_amount" placeholder="{{trans('common.transfer_amount')}}" value="{{$Invoice->invoice_amount}}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.phone')}}</strong></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="transfer_phone" name="transfer_phone" placeholder="{{trans('common.phone')}}">
                        </div>
                    </div>

                    {{--<div class="form-group row">--}}
                        {{--<label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.date_time')}}<span class="text-danger">*</span></strong></label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<div class="input-group date">--}}
                                {{--<input type="text" class="form-control datepicker" name="date_trans_fer" id="timepicker"  data-value="{{date('Y-m-d')}}" autocomplete="off" >--}}
                                {{--<input type="text" class="form-control timepicker" name="time_trans_fer" id="timepicker"  value="{{date('H:i:s')}}" autocomplete="off">--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.date_time')}}<span class="text-danger">*</span></strong></label>
                        <div class="col-sm-5">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right" name="transfer_date" id="datepicker" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="input-group date">
                                <input type="text" name="transfer_time" class="form-control timepicker">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.bank_transter')}}<span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10" >
                            <div id="box_bank" class="example ex1">
                            @foreach($Banks as $rows)
                                <label class="radio red">
                                    <input type="radio" class="banks" name="transfer_bank" id="transfer_bank" value="{{$rows->id}}"/>
                                    <span> {!!  trans('common.bank_account_number').' <strong>'.$rows->bank_account_number.'</strong>'!!} {!! trans('common.bank_name').' <strong>'.$rows->account_name.'</strong>'!!} {!!  trans('common.bank').' <strong>'.$rows->bank_name.'</strong>'!!} {!!  trans('common.sub_bank').' '.$rows->sub_bank!!}</span>
                                </label>
                            @endforeach
                            </div>
                        <span id="error_bank" class="text-red" style="display: none">*** {{trans('common.please_select_the_transfer_bank')}} ***</span>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.transfer_bank')}} <span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_transfer" id="bank_transfer"  required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.sub_bank')}} <span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="sub_bank" id="sub_bank"  required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.slip')}} <span class="text-danger">*</span></strong></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="file" id="file" placeholder="{{trans('common.slip')}}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.remark')}} </strong></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="transfer_remark" name="transfer_remark" placeholder="{{trans('common.remark')}}"></textarea>
                        </div>
                    </div>

                   <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.about_tax')}} </strong></label>
                        <div class="col-sm-10">
                            <div class="example2 ex1">
                                <label class="radio orange">
                                    <input type="radio" name="need_tax_invoice" id="need_tax_invoice" data-id="{{$Invoice->invoice_booking_id}}" value="1" />
                                    <span> <strong>{!! trans('common.need_tax_invoice')!!}</strong></span>
                                </label>
                                <label class="radio orange">
                                    <input type="radio" name="need_tax_invoice" id="need_tax_invoice" data-id="{{$Invoice->invoice_booking_id}}" value="0" checked />
                                    <span> <strong>{!! trans('common.no_need_tax_invoice')!!}</strong></span>
                                </label>
                            </div>
                        </div>
                    </div>
                   <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><strong>{{trans('common.address')}} </strong></label>
                        <div class="col-sm-10">
                            <div class="example2 ex1">
                                @if($AddressBook)
                                    <label class="radio orange">
                                        <input type="radio" name="add_address" id="add_address" data-id="{{$Invoice->invoice_booking_id}}" value="2" checked/>
                                        <span> <strong>{!! trans('common.use_the_same_address')!!}</strong></span>
                                    </label>
                                @endif
                                    <label class="radio orange">
                                        <input type="radio" name="add_address" id="add_address" data-id="{{$Invoice->invoice_booking_id}}" value="1"/>
                                        <span><strong>{!! trans('common.add_new_address')!!}</strong></span>
                                    </label>
                            </div>
                            <div id="address_old" class="col-sm-10">
                                <div class="direct-chat-msg ">
                                    <!-- /.direct-chat-info -->
                                    <div class="direct-chat-img"><i class="fa  fa-map-marker fa-2x text-warning"></i> </div>
                                    <!-- /.direct-chat-img -->
                                    @if($AddressBook)
                                    <div class="direct-chat-text">
                                        {{$AddressBook->entry_company?$AddressBook->entry_company:$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<BR>
                                        {!! $AddressBook->address_show !!}
                                        <span class="pull-right"><a href="{{url('booking/show/invoice_address')}}"> <i class="fa fa-edit"></i> {{trans('common.change_address')}}</a></span>
                                    </div>
                                    @endif
                                    <!-- /.direct-chat-text -->
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="show_form_tax"></div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <hr>
                            <h3>การขอใบกำกับภาษี และ การหักภาษี ณ ที่จ่าย</h3>
                            <p> 1. วันที่ในใบกำกับภาษี จะลงเป็นวันที่แจ้งชำระเงิน</p>
                            <p> 2. อัตราค่าบริการ รวมภาษีมูลค่าเพิ่มแล้ว</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12 text-center">
                            <hr>
                            @if(!Session::has('message'))
                            <button type="submit" id="submit" class="btn btn-success btn-lg" ><i class="fa fa-check-circle"></i> {{trans('common.confirm').trans('common.payment_notification')}}</button>
                            @endif
                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-warning btn-lg pull-left" ><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                        </div>
                    </div>
                </form>

                @else

                    <div class="col-md-12">
                        <h2>{{trans('common.payment_notification')}}</h2><hr>
                    </div>
                    <div class="col-md-10">
                        <div class="alert alert-default">
                            <h5><strong>
                                    <u>{{trans('common.invoice_no')}}#{{$check->payment_invoice_id}} </u><BR><Br>

                            <span class="text-success"> <i class="fa fa-check"></i> {{trans('common.invoice_tour_notification_already')}}</span> </strong><BR><BR>
                            <strong class="text-danger"> *{{trans('common.status').trans('common.awaiting_confirmation')}} </strong>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <span class="text-right"><a href="{{url('my/bookings')}}" class="btn btn-info"><i class="fa fa-reply"></i> {{trans('common.back_to_mybooking')}}</a> </span>
                    </div>
                @endif
            </div>


        </div> <!-- container .//  -->


    </section>



    <!-- ========================= SECTION CONTENT END// ========================= -->
    <!-- bootstrap time picker -->


    {{--<script src="{{asset('member/assets/dateinput/lib/picker.js')}}"></script>--}}
    {{--<script src="{{asset('member/assets/dateinput/lib/picker.date.js')}}"></script>--}}
    {{--<script src="{{asset('member/assets/dateinput/lib/picker.time.js')}}"></script>--}}
    {{--<script src="{{asset('member/assets/dateinput/lib/legacy.js')}}"></script>--}}



    <!-- bootstrap datepicker -->
    <script src="{{asset('member/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{asset('member/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script type="text/javascript">

        $('body').on('click','.banks',function () {
            if($(this).is(':checked')){
                $('#bank_hidden').val($(this).val());
                $('#submit').prop('disabled', false);
                $('#box_bank').removeClass( "bank_error" );
            }
        });

        $('body').on('keyup','#bank_transfer',function () {

           if($('#bank_hidden').val()==''){
                $('#error_bank').show();
                $('#box_bank').addClass( "bank_error" );
               $('#submit').prop('disabled', true);
           }else{
                $('#error_bank').hide();
               $('#box_bank').removeClass( "bank_error" );
               $('#submit').prop('disabled', false);
           }
        });



        //Timepicker
        $('.timepicker').timepicker({
           showInputs: false
        });

        //Date picker
        $('#datepicker').datepicker({
            format:'yyyy-mm-dd',
            autoclose: true
        });

        $('body').on('click','#add_address',function () {
            if($(this).is(":checked")){
                if($(this).val()=='1'){
                    $.ajax({
                        type:'get',
                        url:SP_source() + 'package/ajax/invoice/tax_address',
                        data:{'booking_id':$(this).data('id')},
                        success:function(data){
                            $('#address_old').hide();
                            $('#show_form_tax').html(data);
                        }
                    });
                }else{
                    $('#address_old').show();
                    $('#show_form_tax').html('');
                }
            }
        });

        $('body').on('click','#invoice_all',function () {
            if($(this).is(":checked")){
                $('.invoice').prop('checked', true);
                $.ajax({
                    type:'get',
                    url:SP_source() + 'package/ajax/invoice/amount',
                    data:{'booking_id':$(this).data('id'),'invoice_no':$(this).val(),'type':$('#type').val()},
                    success:function(data){
                        $('#transfer_amount').val(data)
                    }
                });
            }else{
                $('.invoice').prop('checked', false);
                $('#transfer_amount').val('');
            }
        });

        $('body').on('click','.invoice',function () {
            var invoice_no = [];
            $.each($("input[id='invoice_no']:checked"), function(){
                invoice_no.push($(this).val());
            });

            if($(this).is(":checked")){
              // alert(favorite);
            }else{
                $('#invoice_all').prop('checked', false);
                $('#transfer_amount').val('');
            }

            $.ajax({
                type:'get',
                url:SP_source() + 'package/ajax/invoice/amount',
                data:{'booking_id':$(this).data('id'),'invoice_no':invoice_no,'type':$('#type').val()},
                success:function(data){
                    $('#transfer_amount').val(data)
                }
            });
        });

    </script>




@endsection