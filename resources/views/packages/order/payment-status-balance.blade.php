@extends('layouts.package.master')

@section('program-highlight')
    {{--<link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;
        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }
    </style>

    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')
            </div>
        </div>
    </section>

    <div id="dataexample">

    @foreach($Invoice as $rowN)
        <?php


        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$rowN->invoice_package_id)
            ->first();

        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();

        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();

        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();

        $Details=DB::table('package_booking_details as a')
            ->where('a.package_detail_id',$rowN->invoice_package_detail_id)
            ->where('a.booking_id',$rowN->invoice_booking_id)
            ->where('a.timeline_id',$rowN->invoice_timeline_id)
            ->get();

        $Deposit=0;$Totals=0;

        $Status=DB::table('booking_status')->where('booking_status',$rowN->invoice_status)->first();

        ?>

        @if($rowN->invoice_type=='1')
            <!-- Main content -->
            <section class="invoice">
                <div class="container">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                {{trans('common.deposit_invoice')}}
                                {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                            </h2>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-8 invoice-col">
                            @if($media!=null)
                                <img class="logo-invoice" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @else
                                <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @endif
                            {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                        </div>
                        <div class="col-sm-4 invoice-col">
                            <b>{{trans('common.invoice_no')}}: #{{$rowN->invoice_id}}</b><br>
                            <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($rowN->invoice_date))}}<br>
                            <b>{{trans('common.order_id')}}:</b> {{$rowN->invoice_booking_id}}<br>
                            <b>{{trans('common.status')}}:</b> {{trans('common.'.$Status->status_name)}}<br>
                        </div>
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <br>
                        <div class="col-sm-4 invoice-col">
                            <strong>Billing From/จาก:  </strong><br>
                            <address>
                                {{$BusinessInfo1->legal_name}}<br>
                                {{$BusinessInfo1->address}}
                                {{--, {{$city->city}}<br>--}}
                                {{--{{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>--}}
                                {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                {{trans('common.email')}}: {{$BusinessInfo1->email}}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <strong>Billing To/ถึง:</strong>
                            @if($AddressBook)
                                <address>
                                    {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                    {{$AddressBook->address_show}}<br>

                                    {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                    {{trans('common.email')}}: {{$AddressBook->entry_email}}
                                </address>
                            @else
                            @endif
                        </div>

                        <div class="col-sm-4">
                            <!-- Conversations are loaded here -->
                            <div class="small-box bg-yellow">
                                <div class="inner text-center">
                                    <h3>{{trans('common.'.$Status->status_name)}}</h3>
                                    <p>{{trans('common.date')}} : {{date('d/m/Y H:i',strtotime($rowN->payment_date.' '.$rowN->payment_time))}}<br></p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>{{trans('common.items')}}</th>
                                    <th>{{trans('common.description')}}</th>
                                    <th>{{trans('common.deposit')}}</th>
                                    <th>{{trans('common.unit')}}</th>
                                    <th>{{trans('common.unit_total')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;$TotalsAll=0;$Tax=0;$discount=0; $date_payment_deposit=$rowN->payment_date; ?>
                                    @foreach($Details as $Detail)
                                    <?php
//                                    $PackageDetailsOne=DB::table('package_details')
//                                        ->where('packageDescID',$Detail->package_detail_id)
//                                        ->first();
//                                    if($PackageDetailsOne->season=='Y'){
//                                        $order_by="desc";
//                                    }else{
//                                        $order_by="asc";
//                                    }
//                                    $Condition=DB::table('condition_in_package_details as a')
//                                        ->join('package_condition as b','b.condition_code','=','a.condition_id')
//                                        ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
//                                        ->where('b.condition_group_id','1')
//                                        ->where('b.formula_id','>',0)
//                                        ->where('a.packageID',$Detail->package_id)
//                                        ->orderby('c.value_deposit',$order_by)
//                                        ->first();
//
//                                    if($Condition){
//                                        $Deposit_title=$Condition->value_deposit;
//                                        $Deposit+=$Condition->value_deposit*$Detail->number_of_person;
//                                    }

                                    $Deposit_title=$Detail->deposit_price;
                                    $Deposit+=$Detail->deposit_price*$Detail->number_of_person;

                                    $AdditionalPrice=0;
                                    $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();

                                    $Promotion=DB::table('package_booking_promotion')
                                        ->where('booking_detail_id',$Detail->booking_detail_id)
                                        ->first();
                                    $promotion_title='';
                                    if($Promotion){
                                        $promotion_title=$Promotion->promotion_title;
                                        if($Promotion->promotion_operator=='Between'){
                                            if($Promotion->promotion_unit=='%'){
                                                $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                            }else{
                                                $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                            }
                                        }else{
                                            if($Promotion->promotion_operator2=='Up'){
                                                if($Promotion->promotion_unit=='%'){
                                                    $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                }else{
                                                    $pay_more=$Promotion->promotion_value;
                                                }
                                            }else{
                                                if($Promotion->promotion_unit=='%'){
                                                    $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                }else{
                                                    $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                                }
                                            }
                                        }
                                    }
                                    $Price_sub=0;

                                    ?>
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>
                                            <strong>{!! $Detail->package_detail_title !!}</strong><BR>
                                            {{trans('common.deposit')}} {{$Detail->TourType}} {{$current->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}
                                        </td>
                                        <td>{{$current->currency_symbol.number_format($Deposit_title)}}</td>
                                        <td>{{$Detail->number_of_person}}</td>
                                        <td style="text-align: right">
                                            {{$current->currency_symbol.number_format($Deposit_title*$Detail->number_of_person)}}
                                        </td>
                                    </tr>
                                @endforeach
                                {{--<tr>--}}
                                    {{--<td colspan="4" style="text-align: right">{{trans('common.deposit')}}:</td>--}}
                                    {{--<td style="text-align: right"><h4>{{$current->currency_symbol.number_format($Deposit)}}</h4></td>--}}
                                {{--</tr>--}}
                                <tr>
                                    <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                    <td style="text-align: right"><h4>{{$current->currency_symbol.number_format($Deposit)}} </h4></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <?php
                            $Condition=DB::table('condition_in_package_details as a')
                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                ->where('a.packageID',$Detail->package_id)
                                ->where('a.condition_group_id','8')
                                ->where('b.language_code',Session::get('language'))
                                ->get();
                            ?>
                            @if($Condition->count())
                                <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Remark/หมายเหตุ</h3>
                                        </div>
                                        <!-- Conversations are loaded here -->
                                        <div class="direct-chat-messages-payment">
                                            <!-- Message. Default to the left -->
                                            <!-- Message to the right -->
                                            <div class="direct-chat-msg right">
                                                <!-- /.direct-chat-info -->
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        @foreach($Condition as $Detail)
                                                            {!! $Detail->condition_title !!}
                                                        @endforeach
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.direct-chat-text -->
                                            </div>
                                            <!-- /.direct-chat-msg -->
                                            <hr>
                                        </div>
                                        <!--/.direct-chat-messages-->
                                    </div>
                                <!-- /.box-body -->
                            @endif
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-xs-12">
                            {{--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>--}}
                            <a href="{{url('booking/download/invoice/pdf/'.$rowN->invoice_id.'/1')}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Generate PDF
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        @else
            @if($rowN->invoice_status=='4')
                <!-- Main content -->
                <section class="invoice">
                    <div class="container">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="page-header">
                                    {{trans('common.invoice_balance')}}
                                    {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                                </h2>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-8 invoice-col">
                                @if($media!=null)
                                    <img class="logo-invoice" src="{{url('images/logo-toechok.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @else
                                    <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @endif

                            </div>
                            <div class="col-sm-4 invoice-col">
                                <b>{{trans('common.invoice_no')}}: #{{$rowN->invoice_id}}</b><br>
                                <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($rowN->invoice_date))}}<br>
                                <b>{{trans('common.order_id')}}:</b> {{$rowN->invoice_booking_id}}<br>
                                <b>{{trans('common.status')}}:</b> {{trans('common.'.$Status->status_name)}}<br>
                            </div>

                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <br>
                            <div class="col-sm-4 invoice-col">
                                <strong>Billing From/จาก:  </strong><br>
                                <address>
                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}

                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.email')}}: {{$BusinessInfo1->email}}
                                </address>
                            </div>

                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <strong>Billing To/ถึง:</strong>
                                @if($AddressBook)
                                    <address>
                                        {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                        {{$AddressBook->address_show}}<br>

                                        {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                        {{trans('emails')}}: {{$AddressBook->entry_email}}
                                    </address>
                                @endif
                            </div>


                            <div class="col-sm-4">
                                     <!-- Conversations are loaded here -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner text-center">
                                            <h3>{{trans('common.'.$Status->status_name)}}</h3>
                                            <p>{{trans('common.date')}} : {{date('d/m/Y H:i',strtotime($rowN->payment_date))}}<br></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                    </div>
                            </div>

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>{{trans('common.items')}}</th>
                                        <th>{{trans('common.description')}}</th>
                                        <th>{{trans('common.unit_price')}}</th>
                                        <th>{{trans('common.unit')}}</th>
                                        <th>{{trans('common.unit_total')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;$TotalsAll=0;$Tax=0;$discount_total=0;?>
                                        @foreach($Details as $Detail)
                                        <?php

                                        $pay_more=0;$discount=0;$AdditionalPrice=0;$SubTotals=0;$PriceVisa=0;
                                        $Deposit_title=$Detail->deposit_price;
                                        $Deposit+=$Detail->deposit_price*$Detail->number_of_person;



                                        $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();

                                        $Promotion=DB::table('package_booking_promotion')
                                            ->where('booking_detail_id',$Detail->booking_detail_id)
                                            ->first();
                                        $promotion_title='';
                                        if($Promotion){
                                            $promotion_title=$Promotion->promotion_title;
                                            if($Promotion->promotion_operator=='Between'){
                                                if($Promotion->promotion_unit=='%'){
                                                    $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                                }else{
                                                    $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                                }
                                            }else{
                                                if($Promotion->promotion_operator2=='Up'){
                                                    if($Promotion->promotion_unit=='%'){
                                                        $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $pay_more=$Promotion->promotion_value;
                                                    }
                                                }else{
                                                    if($Promotion->promotion_unit=='%'){
                                                        $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                                    }
                                                }
                                            }
                                        }
                                        $Price_sub=0;
                                        ?>
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>
                                                <strong>{!! $Detail->package_detail_title !!}</strong><br>

                                                {{trans('common.deposit')}} {{$Detail->TourType}} {{$current->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}

                                            </td>
                                            <td>
                                                @if($pay_more>0)
                                                    <?php
                                                    $Price_sub=$Detail->booking_realtime_price+$pay_more;
                                                    ?>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}+{{$pay_more}}
                                                @elseif($discount>0)
                                                    <?php
                                                    $Price_sub=$Detail->booking_normal_price-$discount;
                                                    ?>
                                                    <del>{{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}</del>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price-$discount)}}
                                                @else
                                                    <?php
                                                    $Price_sub=$Detail->booking_normal_price;
                                                    ?>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}
                                                @endif
                                            </td>
                                            <td>{{$Detail->number_of_person}}</td>
                                            <td  style="text-align: right">
                                                {{$current->currency_symbol.number_format($Price_sub*$Detail->number_of_person)}}
                                            </td>
                                        </tr>

                                        @if($Additional)
                                            @foreach($Additional as $rowA)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                                    <td>{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                                    <td>1</td>
                                                    <td  style="text-align: right">{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                                </tr>
                                                <?php
                                                $AdditionalPrice+=$rowA->price_service;
                                                ?>
                                            @endforeach
                                        @endif
                                        @if($Detail->price_for_visa>0)
                                            <?php $PriceVisa+=$Detail->price_for_visa*$Detail->number_of_need_visa;?>
                                            @if($rowN->invoice_type=='2')
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td><strong>{{$Detail->price_visa_details}} </strong></td>
                                                    <td>{{$rowN->currency_symbol.number_format($Detail->price_for_visa)}}</td>
                                                    <td>{{$Detail->number_of_need_visa}}</td>
                                                    <td style="text-align: right">{{$rowN->currency_symbol.number_format($Detail->price_for_visa*$Detail->number_of_need_visa)}}</td>
                                                </tr>
                                            @endif
                                        @endif
                                        <?php

                                        $price_include_vat=$Detail->price_include_vat;
                                        if($discount){
                                            $discount_total=$discount*$rows->number_of_person;
                                        }
                                        $SubTotals=round($Price_sub*$Detail->number_of_person)+$AdditionalPrice+$PriceVisa;

                                        $Status=DB::table('booking_status')->where('booking_status',$rowN->invoice_status)->first();
                                        if($Detail->price_include_vat!='Y'){
                                           $Tax+=$SubTotals*.07;
                                        }
                                        $TotalsAll+=$SubTotals;

                                        ?>

                                    @endforeach

                                    @if($price_include_vat=='Y')
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($TotalsAll)}}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($TotalsAll)}}</td>
                                        </tr>
                                    @endif

                                    @if($discount_total)
                                    <tr>
                                        <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                        <td style="text-align: right">{{$current->currency_symbol.number_format($discount_total)}}</td>
                                    </tr>
                                    @endif
                                    {{--<tr>--}}
                                        {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                                        {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                                    {{--</tr>--}}
                                    @if($price_include_vat!='Y')
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.include_tax')}} 7%</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($Tax)}}</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td colspan="4" style="text-align: right"><h5 class="text-red">{{trans('common.deposit')}}:</h5></td>
                                        <td style="text-align: right"><h5 class="text-red">-{{$current->currency_symbol.number_format($Deposit)}}</h5></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                        <td style="text-align: right"><h4>{{$current->currency_symbol.number_format($TotalsAll+$Tax-$Deposit)}} </h4></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                            <?php
                            $Condition=DB::table('condition_in_package_details as a')
                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                ->where('a.packageID',$Detail->package_id)
                                ->where('a.condition_group_id','8')
                                ->where('b.language_code',Session::get('language'))
                                ->get();

                            ?>
                                @if($Condition->count())
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Remark/หมายเหตุ</h3>
                                        </div>
                                        <!-- Conversations are loaded here -->
                                        <div class="direct-chat-messages-payment">
                                            <!-- Message. Default to the left -->
                                            <!-- Message to the right -->
                                            <div class="direct-chat-msg right">
                                                <!-- /.direct-chat-info -->
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        @foreach($Condition as $Detail)
                                                            {!! $Detail->condition_title !!}
                                                        @endforeach
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.direct-chat-text -->
                                            </div>
                                            <!-- /.direct-chat-msg -->
                                            <hr>
                                        </div>
                                        <!--/.direct-chat-messages-->
                                    </div>
                                    <!-- /.box-body -->
                                @endif
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-xs-12">
                                {{--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>--}}
                                <a href="{{url('booking/download/invoice/pdf/'.$rowN->invoice_id.'/2')}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                    <i class="fa fa-download"></i> Generate PDF
                                </a>
                            </div>
                        </div>

                        <div style="text-align: center;">
                            <a href="{{url('package/add/tour/info/'.$rowN->invoice_booking_id)}}" target="_blank" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{trans('common.add')}}{{trans('common.tour_information')}}</a>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            @else
                <!-- Main content -->
                <section class="invoice">
                    <div class="container">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="page-header">
                                    {{trans('common.invoice_balance')}}
                                    {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                                </h2>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-8 invoice-col">
                                @if($media!=null)
                                    <img class="logo-invoice" src="{{url('images/logo-toechok.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @else
                                    <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @endif
                                {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                            </div>
                            <div class="col-sm-4 invoice-col">
                                <b>{{trans('common.invoice_no')}}: #{{$rowN->invoice_id}}</b><br>
                                <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($rowN->invoice_date))}}<br>
                                <b>{{trans('common.order_id')}}:</b> #{{$rowN->invoice_booking_id}}<br>
                                <b>{{trans('common.status')}}:</b> {{trans('common.'.$Status->status_name)}}<br>
                            </div>

                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <br>
                            <div class="col-sm-4 invoice-col">
                                <strong>Billing From/จาก:  </strong><br>
                                <address>
                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}

                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.email')}}: {{$BusinessInfo1->email}}
                                </address>
                            </div>

                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <strong>Billing To/ถึง:</strong>
                                @if($AddressBook)
                                    <address>
                                        {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                        {{$AddressBook->address_show}}<br>

                                        {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                        {{trans('emails')}}: {{$AddressBook->entry_email}}
                                    </address>

                                @endif
                            </div>
                            <div class="col-sm-4">
                                <!-- Conversations are loaded here -->
                                {{--<div class="small-box bg-yellow">--}}
                                    {{--<div class="inner text-center">--}}

                                        {{--<h3>ชำระแล้ว</h3>--}}
                                        {{--<p>--}}
                                            {{--{{trans('common.date')}} : {{date('d/m/Y H:i',strtotime($rowN->payment_date))}}<br>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="fa fa-file-text-o"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->

                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>{{trans('common.items')}}</th>
                                        <th>{{trans('common.description')}}</th>
                                        <th>{{trans('common.unit_price')}}</th>
                                        <th>{{trans('common.unit')}}</th>
                                        <th>{{trans('common.unit_total')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;$TotalsAll=0;$Tax=0;$discount_total=0;?>
                                    @foreach($Details as $Detail)
                                        <?php

                                        $pay_more=0;$discount=0;$AdditionalPrice=0;$SubTotals=0;$PriceVisa=0;
                                        $Deposit_title=$Detail->deposit_price;
                                        $Deposit+=$Detail->deposit_price*$Detail->number_of_person;



                                        $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();

                                        $Promotion=DB::table('package_booking_promotion')
                                            ->where('booking_detail_id',$Detail->booking_detail_id)
                                            ->first();
                                        $promotion_title='';
                                        if($Promotion){
                                            $promotion_title=$Promotion->promotion_title;
                                            if($Promotion->promotion_operator=='Between'){
                                                if($Promotion->promotion_unit=='%'){
                                                    $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                                }else{
                                                    $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                                }
                                            }else{
                                                if($Promotion->promotion_operator2=='Up'){
                                                    if($Promotion->promotion_unit=='%'){
                                                        $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $pay_more=$Promotion->promotion_value;
                                                    }
                                                }else{
                                                    if($Promotion->promotion_unit=='%'){
                                                        $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                                    }
                                                }
                                            }
                                        }
                                        $Price_sub=0;
                                        ?>
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>
                                                <strong>{!! $Detail->package_detail_title !!}</strong><br>

                                                {{trans('common.deposit')}} {{$Detail->TourType}} {{$current->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}

                                            </td>
                                            <td>
                                                @if($pay_more>0)
                                                    <?php
                                                    $Price_sub=$Detail->booking_realtime_price+$pay_more;
                                                    ?>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}+{{$pay_more}}
                                                @elseif($discount>0)
                                                    <?php
                                                    $Price_sub=$Detail->booking_normal_price-$discount;
                                                    ?>
                                                    <del>{{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}</del>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price-$discount)}}
                                                @else
                                                    <?php
                                                    $Price_sub=$Detail->booking_normal_price;
                                                    ?>
                                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}
                                                @endif
                                            </td>
                                            <td>{{$Detail->number_of_person}}</td>
                                            <td  style="text-align: right">
                                                {{$current->currency_symbol.number_format($Price_sub*$Detail->number_of_person)}}
                                            </td>
                                        </tr>

                                        @if($Additional)
                                            @foreach($Additional as $rowA)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                                    <td>{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                                    <td>1</td>
                                                    <td  style="text-align: right">{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                                </tr>
                                                <?php
                                                $AdditionalPrice+=$rowA->price_service;
                                                ?>
                                            @endforeach
                                        @endif
                                        @if($Detail->price_for_visa>0)
                                            <?php $PriceVisa+=$Detail->price_for_visa*$Detail->number_of_need_visa;?>
                                            @if($rowN->invoice_type=='2')
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td><strong>{{$Detail->price_visa_details}} </strong></td>
                                                    <td>{{$rowN->currency_symbol.number_format($Detail->price_for_visa)}}</td>
                                                    <td>{{$Detail->number_of_need_visa}}</td>
                                                    <td style="text-align: right">{{$rowN->currency_symbol.number_format($Detail->price_for_visa*$Detail->number_of_need_visa)}}</td>
                                                </tr>
                                            @endif
                                        @endif
                                        <?php

                                        $price_include_vat=$Detail->price_include_vat;
                                        if($discount){
                                            $discount_total=$discount*$rows->number_of_person;
                                        }
                                        $SubTotals=round($Price_sub*$Detail->number_of_person)+$AdditionalPrice+$PriceVisa;

                                        $Status=DB::table('booking_status')->where('booking_status',$rowN->invoice_status)->first();
                                        if($Detail->price_include_vat!='Y'){
                                            $Tax+=$SubTotals*.07;
                                        }
                                        $TotalsAll+=$SubTotals;

                                        ?>

                                    @endforeach

                                    @if($price_include_vat=='Y')
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($TotalsAll)}}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($TotalsAll)}}</td>
                                        </tr>
                                    @endif

                                    @if($discount_total)
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                            <td style="text-align: right">{{$current->currency_symbol.number_format($discount_total)}}</td>
                                        </tr>
                                    @endif
                                    {{--<tr>--}}
                                    {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                                    {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                                    {{--</tr>--}}
                                    @if($price_include_vat!='Y')
                                        <tr>
                                            <td colspan="4" style="text-align: right">{{trans('common.include_tax')}} 7%</td>
                                            <td  style="text-align: right">{{$current->currency_symbol.number_format($Tax)}}</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td colspan="4" style="text-align: right"><h5 class="text-red">{{trans('common.deposit')}}:</h5></td>
                                        <td style="text-align: right"><h5 class="text-red">-{{$current->currency_symbol.number_format($Deposit)}}</h5></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                        <td style="text-align: right"><h4>{{$current->currency_symbol.number_format($TotalsAll+$Tax-$Deposit)}} </h4></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                            <?php

                            $Condition=DB::table('condition_in_package_details as a')
                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                ->where('a.packageID',$Detail->package_id)
                                ->where('a.condition_group_id','8')
                                ->where('b.language_code',Session::get('language'))
                                ->get();

                            ?>
                            @if($Condition->count())
                                <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Remark/หมายเหตุ</h3>
                                        </div>
                                        <!-- Conversations are loaded here -->
                                        <div class="direct-chat-messages-payment">
                                            <!-- Message. Default to the left -->
                                            <!-- Message to the right -->
                                            <div class="direct-chat-msg right">
                                                <!-- /.direct-chat-info -->
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        @foreach($Condition as $Detail)
                                                            {!! $Detail->condition_title !!}
                                                        @endforeach
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.direct-chat-text -->
                                            </div>
                                            <!-- /.direct-chat-msg -->
                                            <hr>
                                        </div>
                                        <!--/.direct-chat-messages-->
                                    </div>
                                    <!-- /.box-body -->
                                @endif
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-xs-12">
                                {{--<a href="#" id="print" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>--}}
                                <a href="{{url('booking/download/invoice/pdf/'.$rowN->invoice_id.'/2')}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                    <i class="fa fa-download"></i> Generate PDF
                                </a>
                            </div>
                        </div>

                        <div style="text-align: center;">
                            <a href="{{url('package/add/tour/info/'.$rowN->invoice_booking_id)}}" target="_blank" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{trans('common.add')}}{{trans('common.tour_information')}}</a>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            @endif
        @endif
    @endforeach
</div>


@endsection



