<div class="col-md-9">

    <?php $currentPackage='';$tax=0; $pricetotal=0;$priceOne=0; $discount=0;$price_include_vat=''; ?>

    @foreach($Carts as $cart)
        <?php
        $PackageTourOne = DB::table('package_tour as a')
        ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
        ->where('b.LanguageCode', Session::get('language'))
        ->where('a.packageID',$cart->package_id)
        ->first();
        // dd($PackageTourOne);

        $Off=DB::table('package_details')
        ->where('packageID',$cart->package_id)
        ->where('status','Y')
        ->count();

        $CartDetails=DB::table('package_booking_cart_details')
        ->where('booking_cart_id',$cart->booking_cart_id)
        ->where('package_id',$cart->package_id)
        ->get();


        $checking=DB::table('package_booking_cart_details')
        ->where('booking_cart_id',$cart->booking_cart_id)
        ->where('package_id',$cart->package_id)
        ->where('item_status','N')
        ->get();
        //dd($checking);
        $timeline=\App\Timeline::where('id',$PackageTourOne->timeline_id)->first();
        ?>
        <div class="card">
            <div class="col-sm-12">
                @if($cart->package_id!=$currentPackage)
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="container-cart">
                                <input type="checkbox" class="setCare-all"  name="package_select" {{$checking->count()==0?'checked':''}} data-id="{{$PackageTourOne->packageID}}">
                                <span class="checkmark"></span>
                            </label>
                            <div class="img-wrap">
                                <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">
                                    <img src="{{asset('images/package-tour/mid/'.$PackageTourOne->Image)}}" class="img-thumbnail">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <h4 class="title">
                                <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">{!! $PackageTourOne->packageName !!}</a>
                            </h4>
                            <hr>
                            <?php
                            $current=\App\Currency::where('currency_code',$PackageTourOne->packageCurrency)->first();
                            $Detail=DB::table('package_details')
                            ->where('packageID',$PackageTourOne->packageID)
                            ->where('packageDescID',$cart->package_detail_id)
                            ->first();

                            $Details=DB::table('package_details')
                            ->where('packageID',$PackageTourOne->packageID)
                            ->where('packageDateStart','>',date('Y-m-d'))
                            ->where('status','Y')
                            ->orderby('packageDateStart','asc')
                            ->get();

                            $Price_now=0;
                            $Price_up=0;
                            $Price_down=0;

                            $Price=DB::table('package_details_sub')
                            ->where('psub_id',$cart->tour_type)
                            ->where('packageDescID',$cart->package_detail_id)
                            ->first();

                            //****************** Update Status if promotion finish **********************//
                            $check=DB::table('package_promotion')
                            ->where('promotion_date_end','<',date('Y-m-d H:i:s'))
                            ->where('promotion_status','Y')
                            ->where('promotion_operator','Between')
                            //                                            ->first();
                            //                                    dd($check);
                            ->update(['promotion_status'=>'N']);
                            //****************** Update Status if promotion finish **********************//

                            $data_target=null;
                            $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                            ->where('psub_id',$Price->psub_id)
                            ->orderby('promotion_date_start','asc')
                            ->first();

                            $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                            // dd($pricePro);
                            $promotion_title=null;$every_booking=0;
                            //
                            $data_target=null;
                            if($promotion && $promotion->promotion_operator!='Mod'){
                            $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));
                            }else{
                            if($promotion){
                            // dd($promotion);
                            }

                            }

                            $booking=DB::table('package_booking_details')
                            ->where('package_id',$PackageTourOne->packageID)
                            ->where('package_detail_id',$Detail->packageDescID)
                            ->sum('number_of_person');

                            $person_booking=$Detail->NumberOfPeople;

                            if($booking){
                            $person_booking=$Detail->NumberOfPeople-$booking;
                            }
                            // dd($person_booking);

                            $price_promotion=floatval($Price->Price_by_promotion);
                            $price_system=floatval($Price->price_system_fees);
                            ?>
                            @if($promotion)
                                <div class="price-wrap h5">
                                    <var class="price h3 text-warning">
                                        <small class="text-success">{{trans('common.start_price')}}</small>
                                        <span class="currency">{{$current->currency_symbol}}</span>
                                        <span class="num">{{number_format($price_promotion)}}</span>
                                        <small>/{{trans('common.per_person')}}</small>
                                    </var>
                                    @if($price_promotion!=$price_system)
                                        <br>
                                        <del class="price-old">{{trans('common.normal_price')}} {{$current->currency_symbol.number_format($price_system)}}</del>
                                        <span class="price-save">
                                                             {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                            {{$current->currency_symbol.number_format($price_system-$price_promotion)}}
                                                        </span>
                                    @endif

                                    @if($data_target != null)
                                        <BR>
                                        <span class="price-save-date">
                                                                <span id="clockdiv{{$cart->booking_cart_detail_id}}">({{trans('common.this_price')}}
                                                                    <span class="days"></span> {{trans('common.day')}}
                                                                    <span class="hours"></span>:
                                                                 <span class="minutes"></span>:
                                                                 <span class="seconds"></span>, {{$Detail->NumberOfPeople}} {{trans('common.only_last_place')}})
                                                                </span>
                                                            </span>
                                        <script language="JavaScript">
                                            initializeClock('clockdiv{{$cart->booking_cart_detail_id}}', new Date('{{$data_target}}'));
                                        </script>
                                    @else
                                        <BR><span class="text-danger">{{$promotion->promotion_title}}</span>
                                        <span class="price-save-date">({{trans('common.this_price_is_only')}} {{$person_booking}} {{trans('common.only_last_place')}})</span>
                                    @endif
                                    <hr>
                                </div>
                            @else
                                <div class="price-wrap h5">
                                    <var class="price h3 text-warning">
                                        {{--<small class="text-success">{{trans('common.price')}}</small>--}}
                                        <span class="num">{{$current->currency_symbol}}{{number_format($price_system)}}</span>
                                        <small>/{{trans('common.per_person')}}</small>
                                    </var>
                                </div>
                            @endif
                            <div class="col-lg-12">
                                {{trans('common.package_tour_by')}} <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="pull-right-sm">
                                @if(Auth::check())
                                    <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="" data-id="{{$Detail->packageDescID}}" class="add-wishlist btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a>
                                @else
                                    <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="{{url('ajax/login_wishlist')}}" data-toggle="modal" data-target="#popupForm" class="btn btn-outline-success" > <i class="fa fa-heart"></i></a>
                                @endif

                                <button id="remove-cart" data-id="{{$cart->booking_cart_id}}" data-name="{{$cart->package_id}}" class="btn remove-cart btn-outline-danger"> × {{trans('common.remove')}}</button>
                                <br>  <br>
                                @if($Off>0)
                                    <a href="{{url('home/details/'.$PackageTourOne->packageID)}}" class="btn btn-block btn-outline-success">  <i class="fa fa-plus"></i> {{trans('common.buy_more')}}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif

                @if($CartDetails)
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- price-wrap .// -->
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td><strong>{{trans('common.travel_time')}}</strong></td>
                                    <td><strong>{{trans('common.tour_type')}}</strong></td>
                                    <td><strong>{{trans('common.amount')}}</strong></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $price_service=0;$count=0;$totalPrice=0;$Price_visa=0; $discount=0;$pay_more=0;
                                ?>

                                @foreach($CartDetails as $cart_detail)
                                    <?php
                                    // dd($cart_detail);
                                    $count++;
                                    $PackageDetails=DB::table('package_details')
                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                    ->get();

                                    $max_person=$PackageDetails[0]->NumberOfPeople;
                                    $booked=DB::table('package_booking_details')->where('package_detail_id',$cart_detail->package_detail_id)->get()->sum('number_of_person');
                                    $empty_seat=$max_person-$booked;

                                    $CheckOff=DB::table('package_details')
                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                    ->where('packageDateStart','<=',date('Y-m-d'))
                                    ->first();

                                    if($CheckOff){
                                    DB::table('package_booking_cart_details')
                                    ->where('booking_cart_id',$cart_detail->booking_cart_id)
                                    ->where('package_detail_id',$cart_detail->package_detail_id)
                                    ->update(['item_status'=>'N']);
                                    }

                                    $Category=DB::table('package_details_sub')
                                    ->where('packageID',$cart_detail->package_id)
                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                    ->get();
                                    // dd($Category);
                                    $Additional=DB::table('package_additional_services')
                                    ->where('packageID',$cart_detail->package_id)
                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                    ->where('status','Y')
                                    ->get();


                                    $PromotionCart=\App\PackagePromotion::where('packageDescID',$cart_detail->package_detail_id)->active()
                                    ->where('psub_id',$cart_detail->tour_type)
                                    ->orderby('promotion_date_start','asc')
                                    ->first();
                                    // dd($PromotionCart);

                                    $Price=DB::table('package_details_sub')
                                    ->where('psub_id',$cart_detail->tour_type)
                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                    ->first();

                                    $CheckAddition=DB::table('package_additional_services')->where('id',$Price->additional_id)->where('condition_check_first','Y')->first();

                                    ?>
                                    @if(!$CheckOff)
                                        <tr>
                                            <td style="width: 15%">
                                                <label class="container-cart">
                                                    <input type="checkbox" name="package_select" {{$cart_detail->item_status=='Y'?'checked':''}} class="setCart" data-id="{{$cart_detail->booking_cart_detail_id}}">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </td>
                                            <td style="width: 30%">
                                                <select class="form-control" name="update_tour_time" required>
                                                    <option value="">{{trans('common.traveling_date')}}</option>
                                                    @foreach($PackageDetails as $detail)
                                                        <?php
                                                        $st=explode('-',$detail->packageDateStart);
                                                        $end=explode('-',$detail->packageDateEnd);
                                                        if($st[1]==$end[1]){
                                                        $date=\Date::parse($detail->packageDateStart);
                                                        $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                        // dd($end[0]);
                                                        }else{
                                                        $date=\Date::parse($detail->packageDateStart);
                                                        $date1=\Date::parse($detail->packageDateEnd);
                                                        $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                        }
                                                        ?>
                                                        <option value="{{$detail->packageDescID}}" {{$detail->packageDescID==$cart_detail->package_detail_id?'selected':''}} > {{$package_date}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td style="width: 30%">
                                                <select class="form-control cart-type-update" name="update_tour_type" id="update_tour_type" data-id="{{$cart_detail->booking_cart_detail_id}}" required >
                                                    <option value=""> {{trans('common.choose').trans('common.tour_type')}}</option>
                                                    @foreach($Category as $rowsCate)
                                                        <option value="{{$rowsCate->psub_id}}" {{$rowsCate->psub_id==$cart_detail->tour_type?'selected':''}}> {{$rowsCate->TourType .' '.number_format($rowsCate->price_system_fees).'/'.trans('common.person')}} </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td style="width: 12%">
                                                <select class="form-control cart-amount-update" name="update_number_of_person" id="update_number_of_person" data-id="{{$cart_detail->booking_cart_detail_id}}" required>
                                                    <option value=""> {{trans('common.amount')}}</option>
                                                    @for($i=1;$i<=$empty_seat;$i++)
                                                        <option value="{{$i}}" {{$i==$cart_detail->number_of_person?'selected':''}}> {{$i}} {{trans('common.person')}}</option>
                                                    @endfor
                                                </select>
                                            </td>
                                            <td style="width: 6%">
                                                <button data-id="{{$cart_detail->booking_cart_detail_id}}" class="btn btn-outline-danger btn-sm delete-detail"> × {{trans('common.delete')}}</button>
                                            </td>
                                        </tr>
                                        <?php

                                        $Addition_price=0;
                                        $AdditionalCart=DB::table('package_booking_cart_additional')
                                        ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                        ->get();


                                        ?>
                                        @if($Additional->count()>0)
                                            <tr>
                                                <td><span class="btn btn-block btn-default text-success">{{trans('common.additional')}}</span> </td>
                                                <td colspan="2">
                                                    @if($AdditionalCart)
                                                        @foreach($AdditionalCart as $rowAdd)
                                                            <div class="row">
                                                                <div class="col-sm-6 bg-default">
                                                                    <label class="form-control"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</label>
                                                                </div>
                                                                @if($rowAdd->bind_package!='Y')
                                                                    <div class="col-sm-2">
                                                                        <button data-id="{{$rowAdd->id}}" class="addition-delete btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>
                                                                        {{--<button class="btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>--}}
                                                                    </div>
                                                                @else
                                                                    @if($CheckAddition)
                                                                        <div class="col-sm-6 bg-default">
                                                                            <label class="container-cart" style="margin-top: 5px">
                                                                                <input type="checkbox" class="someone_share" name="someone_to_share" value="Y" data-id="{{$rowAdd->id}}" {{$rowAdd->need_someone_share=='Y'?'checked':''}}>
                                                                                <label>
                                                                                    <strong>{{trans('common.can_share')}}</strong>
                                                                                    <strong class="text-danger" data-toggle="tooltip" data-placement="top" title="หากท่านเลือกออฟชั่นนี้ ท่านไม่จำเป็นต้องชำระค่าบริการเสริมนี้ จนกว่าทางผู้ขายแจ้งท่านว่าไม่สามารถหาผู้พักร่วมให้ท่านได้"><i class="fa fa-info-circle"></i> </strong>
                                                                                </label>
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <?php
                                                            if($rowAdd->need_someone_share!='Y'){
                                                            $Addition_price+=$rowAdd->price_service;
                                                            }
                                                            ?>
                                                        @endforeach
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <select class="form-control addition_cart" data-id="{{$cart_detail->booking_cart_detail_id}}" id="addition_cart" name="addition_cart">
                                                                <option value=""> {{trans('common.choose')}} </option>
                                                                @foreach($Additional as $rowAdd)
                                                                    <option value="{{$rowAdd->id}}"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        {{--<div class="col-sm-2">--}}
                                                        {{--<button  class="btn btn-outline-success btn-sm"> + {{trans('common.add')}}</button>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </td>



                                            </tr>
                                        @endif


                                        @if($PackageTourOne->have_visa=='1')
                                            <tr>
                                                <td colspan="3" class="text-right" style="color: #5B6B81">
                                                    {{trans('common.number_of_people_who_need_a_visa')}}<BR>
                                                    <small class="text-danger"><i>{{$PackageTourOne->condition_visa}}</i></small>
                                                </td>
                                                <td colspan="2">
                                                    <select class="form-control cart-visa-update" name="update_visa" id="update_visa" data-id="{{$cart_detail->booking_cart_detail_id}}" required >
                                                        <option value=""> {{trans('common.choose')}}</option>
                                                        <option value="0"> {{trans('common.donot_want')}}</option>
                                                        @for($i=1;$i<=$cart_detail->number_of_person;$i++)
                                                            <option value="{{$i}}" {{$i==$cart_detail->number_of_need_visa?'selected':''}}> {{$i}} {{trans('common.person')}} </option>
                                                        @endfor
                                                    </select>
                                                </td>
                                            </tr>

                                            <?php
                                            $Price_visa=$Price->price_for_visa*$cart_detail->number_of_need_visa;
                                            //  dd($Price_visa);
                                            ?>
                                        @endif

                                        <?php
                                        //   $price_promotion=floatval($Price->Price_by_promotion);
                                        //   $price_system=floatval($Price->price_system_fees);

                                        $price_promotion=round($Price->Price_by_promotion);
                                        $price_system=round($Price->price_system_fees);
                                        ?>
                                        <tr><td colspan="5"><hr></td></tr>

                                        <tr>
                                            <td colspan="3"  style="color: #5B6B81">
                                                @if(isset($PromotionCart))
                                                    <small><i>*{{$PromotionCart->promotion_title}}</i></small> {{trans('common.normal_price')}} <del>{{ $current->currency_symbol.number_format($price_system)}}</del></strong>
                                                    <?php

                                                    if($PromotionCart->promotion_operator=='Between'){
                                                        if($PromotionCart->promotion_unit=='%'){
                                                            $discount+=($price_system*$PromotionCart->promotion_value/100)*$cart_detail->number_of_person;
                                                        }else{
                                                            $discount+=$PromotionCart->promotion_value*$cart_detail->number_of_person;
                                                        }

                                                    }else{
                                                        if($PromotionCart->promotion_operator2=='down'){
                                                            if($PromotionCart->promotion_unit=='%'){
                                                                $discount+=($price_promotion*$PromotionCart->promotion_value/100)*$cart_detail->number_of_person;
                                                            }else{
                                                                $discount+=$PromotionCart->promotion_value*$cart_detail->number_of_person;
                                                            }
                                                        }else{
                                                            if($PromotionCart->promotion_unit=='%'){
                                                                $pay_more+=($price_promotion*$PromotionCart->promotion_value/100)*$cart_detail->number_of_person;
                                                            }else{
                                                                $pay_more+=$PromotionCart->promotion_value*$cart_detail->number_of_person;
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                @endif
                                            </td>
                                            <td colspan="2">
                                                <div class="text-right">
                                                    <table class="table table-striped table-condensed">
                                                        <tr>
                                                            <td>
                                                                <strong>{{trans('common.sale')}} </strong></td><td><strong>{{$current->currency_symbol.number_format($price_promotion)}} x {{$cart_detail->number_of_person}}</strong>
                                                            </td>
                                                        </tr>

                                                        @if($Addition_price>0)
                                                            <tr>
                                                                <td><strong>{{trans('common.value_added_service')}}</strong></td>
                                                                <td><strong> {{$current->currency_symbol.number_format($Addition_price)}} x 1</strong></td>
                                                            </tr>
                                                        @endif
                                                        @if($PackageTourOne->have_visa)
                                                            @if($cart_detail->number_of_need_visa>0)
                                                                <tr><td><strong>{{$Price->price_visa_details}}</strong></td><td><strong>{{$current->currency_symbol.number_format($Price->price_for_visa)}} x {{$cart_detail->number_of_need_visa}}</strong></td></tr>
                                                            @endif
                                                        @endif
                                                        <tr>
                                                            <td><h4 style="color:orangered">{{trans('common.totals')}}</h4> </td>
                                                            <td><h4 style="color:orangered">{{$current->currency_symbol.number_format(round($price_promotion*$cart_detail->number_of_person+$Addition_price+$Price_visa))}}</h4></td>
                                                        </tr>
                                                        <?php $price=round($price_promotion)?>
                                                        @if($Price->price_include_vat!='Y')
                                                            <tr><td colspan="2"><small> <i>({{trans('common.this_price_not_include_vat')}})</i></small></td></tr>
                                                        @else
                                                            <tr><td colspan="2"><small> <i>({{trans('common.this_price_include_vat')}})</i></small></td></tr>
                                                        @endif

                                                    </table>
                                                </div>

                                            </td>
                                        </tr>


                                        @if($CartDetails->count()!=$count)
                                            <tr><td  colspan="4"><hr></td></tr>
                                        @endif
                                        <BR>
                                        <?php
                                        if($cart_detail->item_status=='Y'){
                                        $pricetotal+=round($price*$cart_detail->number_of_person+$Addition_price+$Price_visa);
                                        $priceOne=round($price*$cart_detail->number_of_person+$Addition_price+$Price_visa);
                                        }
                                        ?>
                                    @else
                                        <tr>
                                            <td style="width: 30%">
                                                <select class="form-control" name="update_tour_time" disabled required>
                                                    <option value="">{{trans('common.traveling_date')}}</option>
                                                    @foreach($PackageDetails as $detail)
                                                        <?php
                                                        $st=explode('-',$detail->packageDateStart);
                                                        $end=explode('-',$detail->packageDateEnd);
                                                        if($st[1]==$end[1]){
                                                        $date=\Date::parse($detail->packageDateStart);
                                                        $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                        // dd($end[0]);
                                                        }else{
                                                        $date=\Date::parse($detail->packageDateStart);
                                                        $date1=\Date::parse($detail->packageDateEnd);
                                                        $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                        }
                                                        ?>
                                                        <option value="{{$detail->packageDescID}}" {{$detail->packageDescID==$cart_detail->package_detail_id?'selected':''}} > {{$package_date}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control cart-type-update" name="update_tour_type" id="update_tour_type" data-id="{{$cart_detail->booking_cart_detail_id}}" disabled required >
                                                    <option value=""> {{trans('common.choose').trans('common.tour_type')}}</option>
                                                    @foreach($Category as $rowsCate)
                                                        <option value="{{$rowsCate->psub_id}}" {{$rowsCate->psub_id==$cart_detail->tour_type?'selected':''}}> {{$rowsCate->TourType}} {{$current->currency_symbol.number_format($rowsCate->Price_by_promotion)}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td style="width: 12%">
                                                <select class="form-control cart-amount-update" name="update_number_of_person" id="update_number_of_person" data-id="{{$cart_detail->booking_cart_detail_id}}" disabled required>
                                                    <option value=""> {{trans('common.amount')}}</option>
                                                    @for($i=1;$i<=$empty_seat;$i++)
                                                        <option value="{{$i}}" {{$i==$cart_detail->number_of_person?'selected':''}}> {{$i}} {{trans('common.person')}}</option>
                                                    @endfor
                                                </select>
                                            </td>
                                            <td style="width: 6%">
                                                <button data-id="{{$cart_detail->booking_cart_detail_id}}" class="btn btn-outline-danger btn-sm delete-detail"> × {{trans('common.delete')}}</button>
                                            </td>
                                        </tr>



                                        <?php
                                        // dd('test');
                                        $Addition_price=0;
                                        $AdditionalCart=DB::table('package_booking_cart_additional')
                                        ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                        ->get();
                                        ?>

                                        @if($Additional->count()>0)
                                            <tr>
                                                <td><span class="btn btn-block btn-default text-success">{{trans('common.additional')}}</span></td>
                                                <td colspan="2">
                                                    @if($AdditionalCart)
                                                        @foreach($AdditionalCart as $rowAdd)
                                                            <div class="row">
                                                                <div class="col-sm-10">
                                                                    <label style="background-color: #e9ecef" class="form-control"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</label>
                                                                </div>

                                                            </div>
                                                            <?php $Addition_price+=$rowAdd->price_service?>
                                                        @endforeach
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <select class="form-control addition_cart" data-id="{{$cart_detail->booking_cart_detail_id}}" id="addition_cart" name="addition_cart" disabled>
                                                                <option value=""> {{trans('common.choose')}} </option>
                                                                @foreach($Additional as $rowAdd)
                                                                    <option value="{{$rowAdd->id}}"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </td>
                                                {{--<td>--}}
                                                {{--<a href="{{url('ajax/additional/'.$cart_detail->booking_cart_detail_id)}}" data-toggle="" da class="btn btn-outline-success btn-sm"> + เพิ่ม</a>--}}
                                                {{--<a href="#" class="btn btn-outline-warning btn-sm"> × ลบ</a>--}}
                                                {{--</td>--}}
                                                <td></td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>
                                                @if(isset($PromotionCart))
                                                    <i>{{$PromotionCart->promotion_title}}</i>
                                                @endif
                                            </td>
                                            <td colspan="3">
                                                <div class="pull-left">
                                                    <strong class="text-danger">{{trans('common.this_tour_package_closed_for_sale')}}</strong>
                                                </div>
                                                <div class="text-right">
                                                    @if($price_promotion==$price_system)
                                                        @if($Addition_price>0)
                                                            <br><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}},</strong>
                                                        @endif
                                                        <?php
                                                        if($cart_detail->item_status=='Y'){
                                                        $totalPrice+=round($price_promotion*$cart_detail->number_of_person+$Addition_price);
                                                        }
                                                        ?>
                                                        <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($totalPrice)}}</h4>
                                                    @else
                                                        <?php
                                                        //                                                                                $discount+=($price_promotion-$price_system)*$cart_detail->number_of_person;
                                                        ?>
                                                        <strong>{{trans('common.normal_price')}} <del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del></strong>
                                                        <strong>{{trans('common.sale')}} {{$current->currency_symbol.number_format($price_promotion)}}</strong>
                                                        @if($Addition_price>0)
                                                            <BR><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}}</strong>
                                                        @endif
                                                        <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($price_promotion*$cart_detail->number_of_person+$Addition_price)}}</h4>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>

                                        @if($CartDetails->count()!=$count)
                                            <tr><td  colspan="4"><hr></td></tr>
                                        @endif

                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <BR>
        <?php
        $currentPackage=$cart->package_id;
        $tax+=$priceOne*.07;
        if($Price->price_include_vat=='Y'){
        $price_include_vat='Y';
        }
        ?>
    @endforeach

</div>

@if($Carts->count()>0)
    <div class="col-md-3">
        {{--<p class="alert alert-success">Add USD 5.00 of eligible items to your order to qualify for FREE Shipping. </p>--}}
        <h3 class="alert alert-success"><i class="fa fa-check"></i> {{trans('common.order_summary')}}</h3>

        <figure class="itemside mb-3">

            @if($price_include_vat=='Y')
                <div class="col-sm-8"><strong>{{trans('common.totals').' ('.trans('common.include_tax')}} 7%) :</strong> </div>
            @else
                <div class="col-sm-8"><strong>{{trans('common.totals')}} :</strong> </div>
            @endif
            <div class="col-sm-4 text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal)}}</strong></div>
        </figure>
        {{--<figure class="itemside mb-3">--}}
            {{--<div class="col-sm-8">{{trans('common.discount')}}: </div>--}}
            {{--<div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($discount)}}</div>--}}
        {{--</figure>--}}

        @if($discount>0)
            <figure class="itemside mb-3">
                <div class="col-sm-8">{{trans('common.discount')}}: </div>
                <div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($discount)}}</div>
            </figure>
        @endif

        @if($pay_more>0)
            <figure class="itemside mb-3">
                <div class="col-sm-8">{{trans('common.price_title_up')}}: </div>
                <div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($pay_more)}}</div>
            </figure>
        @endif



    @if($price_include_vat!='Y')
            <div class="itemside mb-3">
                <div class="col-sm-8">{{trans('common.tax')}} 7%:</div>
                <div class="col-sm-4 text-right">{{$current->currency_symbol}}{{number_format($tax)}}</div>
            </div>
        @endif

        @if($price_include_vat!='Y')
            <dl class="dlist-align h5">
                <dt>{{trans('common.totals')}}:</dt>
                <dd class="text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal+$tax)}}</strong></dd>
            </dl>
        @else
            <dl class="dlist-align h5">
                <dt>{{trans('common.totals')}}:</dt>
                <dd class="text-right"><strong>{{$current->currency_symbol}}{{number_format($pricetotal)}}</strong></dd>
            </dl>
        @endif
        <hr>


        @if(Auth::check())
            @if($pricetotal>0)
                <a href="{{url('booking/continuous/step2')}}" class="btn btn-success btn-block btn-lg">
                    <i class="fa fa-arrow-right"></i> {{trans('common.order_summary')}}
                </a>
            @endif
        @else
            @if($pricetotal>0)
                <a href="{{url('register/2')}}" class="btn btn-success btn-block btn-lg">
                    <i class="fa fa-arrow-right"></i> {{trans('common.continuous')}}
                </a>
            @endif
        @endif


        <a href="{{url('home/package_order/more')}}" class="btn btn-info btn-block btn-lg">
            <i class="fa fa-reply"></i> {{trans('common.buy_more_packages')}}
        </a>


    </div>
@else
    <div class="col-sm-12 text-center"><h1></h1>
        <h1><i class="fa fa-shopping-cart"></i> Your cart is empty.</h1>
        <div><a href="{{url('home/package/'.Session::get('group'))}}" class="btn btn-default btn-lg">Make your booking.</a> </div>
    </div>
@endif