
        @if($Bookings->count())
            <table id="example2" class="table table-bordered">
            <thead>
                <tr>
                    <th>{{trans('common.booking_id')}}</th>
                    <th>{{trans('common.booking_date')}}</th>
                    <th>{{trans('common.package')}}</th>
                    {{--<th>{{trans('common.status')}}</th>--}}
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $TotalsAll=0;$AdditionalPrice=0;$PackageID='';
                ?>
                @foreach($Bookings as $rows)
                <?php
                    $Details=DB::table('package_booking_details as a')
//                                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//                                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                        ->where('booking_detail_status',$status)
                        ->where('a.booking_id',$rows->booking_id)
                        ->get();
                        $Deposit=0;$Totals=0;

                if($Details->count()){
                    foreach($Details as $rowD){
                        if($rowD->deposit_price){
                            $Deposit+=$rowD->deposit_price*$rowD->number_of_person;
                        }
                        $TotalsAll+=$rowD->number_of_person*$rowD->booking_normal_price;
                    }

                    $Totals=$TotalsAll+$AdditionalPrice;
                    $Timeline=\App\Timeline::where('id',$rowD->timeline_id)->first();
                    $check_cancel='yes';
                    ?>

                    <tr>
                        <td><a href="{{url('booking/show/invoice/'.$rows->booking_id.'/all')}}" target="_blank">#{{$rows->booking_id}}</a></td>
                        <td>{{$rows->booking_date}}</td>
                        <td>
                            <table class="table borderless">

                                @foreach($Details as $rowD)
                               <?php

                                    if($rowD->booking_detail_status!='6'){
                                       $check_cancel='no';
                                    }

                                    $Packagetour=DB::table('package_tour as a')
                                        ->join('package_tour_info as b','b.packageID','=','a.packageID')
                                        ->where('a.packageID',$rowD->package_id)
                                        ->first();
                                   // dd($Packagetour);
                                    $Invoice_deposit=DB::table('package_invoice')
                                        ->where('invoice_booking_id',$rowD->booking_id)
                                        ->where('invoice_package_id',$rowD->package_id)
                                        ->where('invoice_package_detail_id',$rowD->package_detail_id)
                                        ->where('invoice_type','1')
                                        ->first();

                                    $Invoice_balance=DB::table('package_invoice')
                                        ->where('invoice_booking_id',$rowD->booking_id)
                                        ->where('invoice_package_id',$rowD->package_id)
                                        ->where('invoice_package_detail_id',$rowD->package_detail_id)
                                        ->where('invoice_type','2')
                                        ->first();
                                    if($rowD->booking_id=='28'){
                                      //  dd($rowD->booking_detail_status);
                                    }

                                    ?>
                                        @if($rowD->package_id!=$PackageID)
                                            <tr class="bg-package">
                                                <td colspan="3"> {{trans('common.package_tour')}} <a href="{{url('home/details/'.$rowD->package_id)}}" target="_blank">{{$Packagetour->packageName}}</a> | {{trans('common.sell_by').':'. $Timeline->username}} <BR></td>
                                            </tr>
                                        @endif

                                            <tr>
                                                <td width="65%">
                                                    {!! $rowD->package_detail_title!!}
                                                </td>

                                                <td width="6%">
                                                    {{$rowD->booking_person_number.' '.trans('common.person')}}
                                                </td>
                                                <td width="29%">
                                                    @if($rowD->booking_detail_status!='6' && $rowD->booking_detail_status!='7')
                                                        <small>
                                                    @if($Packagetour->package_partner=='Yes' || $Packagetour->package_owner=='Yes')

                                                        @if(!$Invoice_deposit)
                                                            @if($Invoice_balance->invoice_status==4)
                                                                <i class="fa fa-check"></i> {{trans('common.paid')}}<BR>
                                                            @else
                                                                <i class="fa fa-hourglass-start text-danger"></i> {{trans('common.pending_payment')}}<BR>
                                                            @endif
                                                        @else

                                                            @if($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==4)
                                                                <i class="fa fa-check text-success"></i> {{trans('common.paid')}}<BR>
                                                            @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==1)
                                                                <i class="fa fa-check"></i> {{trans('common.deposit_paid')}}<BR>
                                                            @else

                                                            <?php
                                                                $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice_deposit->invoice_id)->first();
                                                                ?>
                                                                @if($check)
                                                                   <span class="text-warning"><i class="fa fa-hourglass-start"></i> {{trans('common.awaiting_confirmation').trans('common.tour_deposit')}}</span><BR>
                                                                @else
                                                                    <i class="fa fa-hourglass-start text-danger"></i> {{trans('common.pending_payment')}}<BR>
                                                                @endif

                                                            @endif
                                                        @endif

                                                    @else

                                                        @if(!$Invoice_deposit)

                                                            @if($Invoice_balance)
                                                                <?php
                                                                $Status=DB::table('booking_status')->where('booking_status',$Invoice_balance->invoice_status)->first();
                                                                $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice_balance->invoice_id)->first();
                                                                ?>

                                                                @if($check)
                                                                    @if($check->status=='s')
                                                                        {{trans('common.awaiting_confirmation')}}
                                                                    @else
                                                                        {{trans('common.paid')}}
                                                                    @endif
                                                                @else
                                                                    {{trans('common.'.$Status->status_name)}}<BR>
                                                                    <span class="text-danger">{{trans('common.payment_before')}} : {{date("Y-m-d",strtotime($Invoice_balance->invoice_payment_date))}}</span>
                                                                @endif
                                                            @endif
                                                        @else

                                                            @if($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==4)
                                                                <i class="fa fa-check text-success"></i> {{trans('common.paid')}}<BR>
                                                            @else

                                                                @if($Invoice_deposit->invoice_status=='1')
                                                                    <?php
                                                                    $check=DB::table('payment_notification_sub')
                                                                        ->where('payment_invoice_id',$Invoice_deposit->invoice_id)
                                                                        ->first();
                                                                    ?>

                                                                    @if($check)
                                                                            @if($check->status=='s')
                                                                                <span class="text-warning"> {{trans('common.awaiting_confirmation').trans('common.tour_deposit')}}</span><BR>
                                                                                <?php
                                                                                $check2=DB::table('payment_notification_sub')
                                                                                    ->where('payment_invoice_id',$Invoice_balance->invoice_id)
                                                                                    ->first();
                                                                                ?>
                                                                                @if($check2)
                                                                                    <span class="text-warning">{{trans('common.awaiting_confirmation').trans('common.tour_balance')}}</span><BR>
                                                                                @else
                                                                                    {{trans('common.pending_payment').trans('common.tour_balance')}}<BR>
                                                                                    <span class="text-danger">{{trans('common.payment_due_date')}} : {{date("Y-m-d",strtotime($Invoice_balance->invoice_payment_date))}}</span>
                                                                                @endif
                                                                            @else
                                                                                {{trans('common.pending_payment').trans('common.tour_deposit')}}<BR>
                                                                                <span class="text-danger">{{trans('common.payment_before')}} : {{date("Y-m-d",strtotime($Invoice_deposit->invoice_payment_date))}}</span><BR>
                                                                            @endif
                                                                    @else
                                                                            {{trans('common.pending_payment').trans('common.tour_deposit')}}<BR>
                                                                            <span class="text-danger">{{trans('common.payment_before')}} : {{date("Y-m-d",strtotime($Invoice_deposit->invoice_payment_date))}}</span><BR>
                                                                            {{trans('common.pending_payment').trans('common.tour_balance')}}<BR>
                                                                            <span class="text-danger">{{trans('common.payment_due_date')}} : {{date("Y-m-d",strtotime($Invoice_balance->invoice_payment_date))}}</span>
                                                                    @endif
                                                                @else
                                                                    <span class="text-success"> {{trans('common.deposit_paid')}}</span><BR>
                                                                    <?php
                                                                    $check=DB::table('payment_notification_sub')
                                                                        ->where('payment_invoice_id',$Invoice_balance->invoice_id)
                                                                        ->first();
                                                                    ?>
                                                                    @if($check)
                                                                        @if($check->status=='s')
                                                                            {{trans('common.awaiting_confirmation').trans('common.tour_balance')}}<BR>
                                                                        @endif
                                                                    @else
                                                                        {{trans('common.pending_payment').trans('common.tour_balance')}}<BR>
                                                                        <span class="text-danger">{{trans('common.payment_due_date')}} : {{date("Y-m-d",strtotime($Invoice_balance->invoice_payment_date))}}</span>
                                                                    @endif
                                                                @endif
                                                            @endif

                                                        @endif
                                                    @endif
</small>
                                                    @else
                                                        @if($rowD->booking_detail_status=='7')
                                                            <h5 class="text-danger">  {{trans('common.awaiting_confirm_seats')}}</h5>
                                                        @else
                                                            <h5 class="text-danger"> <i class="fa fa-times"></i> {{trans('common.canceled')}}</h5>
                                                        @endif
                                                        <small class="text-danger"></small>
                                                    @endif
                                                </td>
                                            </tr>

                                        <?php $PackageID=$rowD->package_id;?>
                                @endforeach
                            </table>
                        </td>

                        <td class="text-right">
                            <a class="btn btn-success " href="{{url('booking/show/invoice/'.$rows->booking_id.'/all')}}"><i class="fa fa-list"></i>
                                {{trans('common.booking_detail')}}
                            </a> <BR>
                            @if($check_cancel=='no')
                                <div class="btn-group ">
                                    <button type="button" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> {{trans('common.manage')}} <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                        @if($Packagetour->package_partner=='Yes' || $Packagetour->package_owner=='Yes')
                                            @if(!$Invoice_deposit)
                                                @if($Invoice_balance)
                                                    @if($Invoice_balance->invoice_status==1)
                                                        <li><a href="{{url('booking/show/invoice/'.$rows->booking_id.'/2')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.pay_balance')}}</a></li>
                                                        <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                    @elseif($Invoice_balance->invoice_status==4)
                                                        <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                        <li><a href="{{url('booking/download/invoice/pdf/'.$Invoice_balance->invoice_id)}}/all" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                        <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                    @endif
                                                @endif
                                            @else
                                                @if($Invoice_deposit->invoice_status==1 && $Invoice_balance->invoice_status==1)
                                                    <li><a href="{{url('booking/show/invoice/'.$rows->booking_id.'/1')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.pay_deposit')}}</a></li>
                                                    {{--<li><a href="{{url('booking/show/invoice/'.$rows->booking_id.'/all')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.pay_all')}}</a></li>--}}
                                                @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==1)

                                                    <li><a href="{{url('booking/show/invoice/'.$rows->booking_id.'/2')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.pay_balance')}}</a></li>
                                                    <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                    <li><a href="{{url('booking/download/invoice/pdf/'.$Invoice_deposit->invoice_id)}}/1" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                    <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==4)
                                                    <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                    {{--<li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>--}}
                                                    <li><a href="{{url('booking/download/invoice/pdf/'.$Invoice_balance->invoice_id)}}/all" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                    <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                @elseif($Invoice_balance->invoice_status==4)
                                                    <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                    <li><a href="{{url('booking/download/invoice/pdf/'.$Invoice_balance->invoice_id)}}/all" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                    <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                @endif
                                            @endif
                                        @else

                                                @if($rowD->booking_detail_status!='7')
                                                @if(!$Invoice_deposit)
                                                    @if($Invoice_balance)
                                                        <?php
                                                        $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice_balance->invoice_id)->first();
                                                        ?>
                                                        @if(!$check)
                                                            <li><a href="{{url('booking/show/form_notification/'.$rows->booking_id.'/all')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.notify_tour_payment')}}</a></li>
                                                        @else
                                                             <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                        @endif
                                                            <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                    @endif
                                                @else
                                                    @if($Invoice_deposit->invoice_status==1 && $Invoice_balance->invoice_status==1)
                                                        <?php
                                                        $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice_deposit->invoice_id)->first();
                                                        ?>
                                                        @if(!$check)
                                                             <li><a href="{{url('booking/show/form_notification/'.$rows->booking_id.'/1')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.notify_deposit_payment')}}</a></li>
                                                        @endif
                                                             <li><a href="{{url('booking/show/form_notification/'.$rows->booking_id.'/all')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.notify_tour_payment')}}</a></li>

                                                        @if($check)
                                                            <li><a href="" ><i class="fa fa-info"></i> {{trans('common.awaiting_confirmation')}}</a></li>
                                                        @endif
                                                    @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==1)
                                                        <?php
                                                            $check=DB::table('payment_notification_sub')->where('payment_invoice_id',$Invoice_deposit->invoice_id)->first();
                                                        ?>
                                                        @if($check->status=='y')
                                                            <li><a href="{{url('booking/show/form_notification/'.$rows->booking_id.'/2')}}" target="_blank"><i class="fa fa-credit-card"></i> {{trans('common.pay_balance')}}</a></li>
                                                        @else
                                                            <li><a href="" ><i class="fa fa-info"></i> {{trans('common.awaiting_confirmation')}}</a></li>
                                                        @endif
                                                        <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                        <li><a href="{{url('booking/download/invoice/pdf/'.$rows->booking_id)}}/1" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                        <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                    @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==4)
                                                        <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                        {{--<li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>--}}
                                                        <li><a href="{{url('booking/download/invoice/pdf/'.$rows->booking_id)}}/all" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                        <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                    @elseif($Invoice_balance->invoice_status==4)
                                                        <li><a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-users"></i> {{trans('common.manage_tourist')}}</a></li>
                                                        <li><a href="{{url('booking/download/invoice/pdf/'.$rows->booking_id)}}/all" target="_blank"><i class="fa fa-print"></i> {{trans('common.print_invoice')}}</a></li>
                                                        <li><a href="{{url('booking/cancel/invoice/'.$rows->booking_id)}}" target="_blank"><i class="fa fa-times"></i> {{trans('common.cancel')}}</a></li>
                                                    @endif
                                                @endif
                                                    @else

                                                    {{trans('common.awaiting_confirm_seats')}}
                                                @endif
                                        @endif

                                    </ul>
                                </div>
                            @else
                                <h5 class="text-danger"> <i class="fa fa-times"></i> {{trans('common.canceled')}}</h5>
                            @endif
                        </td>
                    </tr>

                    <?php }?>
                @endforeach
            </tbody>
        </table>
        @else
            <h3 class="text-center text-danger">Your booking hasn't been found!</h3>
            <p class="text-center"><a href="{{url('home/package/all')}}" class="btn btn-warning"> Make a booking click go!!</a> </p>
        @endif
