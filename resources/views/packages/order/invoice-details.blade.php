@extends('layouts.member.layout_master_new')

@section('content')
    <!-- Main content -->
    <section class="invoice">
        <div class="container">

<?php

            $Package=DB::table('package_tour as a')
                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                ->where('a.packageID',$Invoice->invoice_package_id)
                ->first();

            $Timeline=\App\Timeline::where('id','37850')->first();

            $media=\App\Media::where('id',$Timeline->avatar_id)->first();


            $BankInfo=DB::table('business_verified_bank as a')
                ->join('banks as b','b.bank_code','=','a.bank_id')
                ->where('a.timeline_id',$Timeline->id)
                ->where('b.language_code',Auth::user()->language)
                ->get();
           // dd($BankInfo);
            $BusinessInfo=DB::table('business_verified_info1')
                ->where('timeline_id',$Timeline->id)
                ->first();

            $BusinessInfo1=DB::table('business_verified_info2')
                ->where('language_code',Auth::user()->language)
                ->where('timeline_id',$Timeline->id)
                ->first();

//            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
//            if(!$country){
//                $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
//            }
//
//            $states=DB::table('states')
//                ->where('country_id',$BusinessInfo->country_id)
//                ->where('state_id',$BusinessInfo->state_id)
//                ->where('language_code',Auth::user()->langauge)
//                ->first();
//            if(!$states){
//                $states=DB::table('states')
//                    ->where('country_id',$BusinessInfo->country_id)
//                    ->where('state_id',$BusinessInfo->state_id)
//                    ->where('language_code','en')
//                    ->first();
//            }
//            $city=DB::table('cities')
//                ->where('country_id',$BusinessInfo->country_id)
//                ->where('state_id',$BusinessInfo->state_id)
//                ->where('city_id',$BusinessInfo->city_id)
//                ->where('language_code',Auth::user()->langauge)
//                ->first();
//            if(!$city){
//                $city=DB::table('cities')
//                    ->where('country_id',$BusinessInfo->country_id)
//                    ->where('state_id',$BusinessInfo->state_id)
//                    ->where('city_id',$BusinessInfo->city_id)
//                    ->where('language_code','en')
//                    ->first();
//            }

            $AddressBook=DB::table('address_book as a')
                ->join('countries as b','b.country_id','=','a.entry_country_id')
                ->where('a.timeline_id',Auth::user()->timeline_id)
                ->where('a.default_address','1')
                ->where('a.address_type','1')
                ->first();

            $Details=DB::table('package_booking_details as a')
                ->where('a.booking_id',$Invoice->invoice_booking_id)
                ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                ->where('a.package_detail_id',$Invoice->invoice_package_detail_id)
                ->get();

            $Deposit=0;$Totals=0;
            $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
            $currency_code=$Invoice->currency_code;
            $currency_symbol=$Invoice->currency_symbol;
        ?>


            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {{trans('common.invoice_deposit')}}
                        {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-8 invoice-col">
                    @if($media!=null)
                        <img class="logo-invoice" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @else
                        <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @endif
                    {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                </div>
                <div class="col-sm-4 invoice-col">
                    <b>{{trans('common.invoice_no')}}: #{{$Invoice->invoice_id}}</b><br>
                    <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($Invoice->invoice_date))}}<br>
                    <b>{{trans('common.order_id')}}:</b> #{{$Invoice->invoice_booking_id}}<br>
                    <b>{{trans('common.status')}}:</b> {{trans('common.'.$Status->status_name)}}<br>
                    {{--<b>{{trans('common.reference')}}:</b> ACB11<br>--}}
                </div>
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <br>
                <div class="col-sm-6 invoice-col">
                    <strong>Billing From/จาก:  </strong><br>
                    <address>
                        {{$BusinessInfo1->legal_name}}<br>
                        {{$BusinessInfo1->address}}<br>
                        {{--{{$BusinessInfo1->address}}, {{$city->city}}<br>--}}
                        {{--{{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>--}}
                        {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                        {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                    </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                    <strong>Billing To/ถึง:</strong>
                    @if($AddressBook)
                        <address>
                            {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                            {!! $AddressBook->address_show !!}<br>


                            {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                            {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                        </address>
                        @else
                    @endif
                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('common.items')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th>{{trans('common.unit_price')}}</th>
                            <th>{{trans('common.unit')}}</th>
                            <th>{{trans('common.unit_total')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;$TotalsAll=0;$Tax=0; $discount_total=0; $pay_more_total=0;?>

                        @foreach($Details as $rows)
                            <?php

                            $pay_more=0;$discount=0;$AdditionalPrice=0;$Price_sub=0;$PriceVisa=0;
                            $include_vat=$rows->price_include_vat;
                            $Detail_title=$rows->deposit_price;
                            $Deposit+=round($rows->deposit_price*$rows->number_of_person);


                             $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();

                            $Promotion=DB::table('package_booking_promotion')
                                ->where('booking_detail_id',$rows->booking_detail_id)
                                ->first();
                            $promotion_title='';
                            if($Promotion){
                                $promotion_title=$Promotion->promotion_title;
                                if($Promotion->promotion_operator=='Between'){
                                    if($Promotion->promotion_unit=='%'){
                                        $discount=$rows->booking_normal_price*$Promotion->promotion_value/100;
                                    }else{
                                        $discount=$rows->booking_normal_price-$Promotion->promotion_value;
                                    }
                                }else{
                                    if($Promotion->promotion_operator2=='Up'){
                                        if($Promotion->promotion_unit=='%'){
                                            $pay_more=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                        }else{
                                            $pay_more=$Promotion->promotion_value;
                                        }
                                    }else{
                                        if($Promotion->promotion_unit=='%'){
                                            $discount=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                        }else{
                                            $discount=$rows->booking_realtime_price-$Promotion->promotion_value;
                                        }
                                    }
                                }
                            }

                            ?>

                            <tr>
                                <td valign="top" align="center">{{$i++}}</td>
                                <td>
                                    <strong> {!! $rows->package_detail_title !!} </strong><BR>
                                    @if($Invoice->invoice_type==2)
                                    <span class="text-danger"> {{trans('common.deposit')}} {{$rows->TourType}}: {{$Invoice->currency_symbol.number_format($Detail_title)}} x {{$rows->number_of_person}}</span>
                                    @endif
                                </td>
                                @if($Invoice->invoice_type==2)
                                    <td valign="top">{{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}</td>
                                    <td valign="top">{{$rows->number_of_person}}</td>
                                    <td style="text-align: right">
                                        @if($pay_more>0)
                                            <?php
                                            $Price_sub=$rows->booking_realtime_price+$pay_more;
                                            ?>
                                            {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}+{{$pay_more}}
                                        @elseif($discount>0)
                                            <?php
                                            $Price_sub=$rows->booking_normal_price-$discount;
                                            ?>
                                            <del>{{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}</del>
                                            {{$Invoice->currency_symbol.number_format($rows->booking_normal_price-$discount)}}
                                        @else
                                            <?php
                                            $Price_sub=$rows->booking_normal_price;
                                            ?>
                                            {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}
                                        @endif

                                    </td>
                                 @else
                                    <td valign="top">{{$Invoice->currency_symbol.number_format($Detail_title)}}</td>
                                    <td valign="top">{{$rows->number_of_person}}</td>
                                    <td style="text-align: right">
                                        {{$Invoice->currency_symbol.number_format($Detail_title*$rows->number_of_person)}}
                                    </td>
                                @endif
                            </tr>
                            @if($Invoice->invoice_type==2)
                            @if($Additional)
                                @foreach($Additional as $rowA)
                                    <tr>
                                        <td valign="top" align="center">{{$i++}}</td>
                                        <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                        <td valign="top">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                        <td valign="top">1</td>
                                        <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                    </tr>
                                    <?php
                                    $AdditionalPrice+=$rowA->price_service;
                                    ?>
                                @endforeach
                            @endif
                            @if($rows->price_for_visa>0)
                                <?php $PriceVisa=$rows->price_for_visa*$rows->number_of_need_visa;?>
                                <tr>
                                    <td valign="top" align="center">{{$i++}}</td>
                                    <td><strong>{{$rows->price_visa_details}} </strong></td>
                                    <td>{{$Invoice->currency_symbol.number_format($rows->price_for_visa)}}</td>
                                    <td>{{$rows->number_of_need_visa}}</td>
                                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($PriceVisa)}}</td>
                                </tr>
                            @endif
                            @endif

                            <?php
                            $Total=round($Price_sub*$rows->number_of_person)+$AdditionalPrice+$PriceVisa;

                            if($include_vat!='Y'){
                                $Tax+=round($Total*7/100);
                            }
                            $Totals+=round($Price_sub*$rows->number_of_person)+$AdditionalPrice+$PriceVisa;
                            $TotalsAll+=$Total+$Tax;
                            $discount_total+=round($discount*$rows->number_of_need_visa);
                            $pay_more_total+=round($pay_more*$rows->number_of_need_visa);
                            $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                            ?>

                        @endforeach


                        @if($Invoice->invoice_type=='2')
                                @if($rows->price_include_vat=='Y')
                                    <tr>
                                        <td colspan="4" style="text-align: right">{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</td>
                                        <td  style="text-align: right">{{$Invoice->currency_symbol.number_format($TotalsAll)}}</td>
                                    </tr>

                                @else
                                    <tr>
                                        <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                                        <td  style="text-align: right">{{$Invoice->currency_symbol.number_format($TotalsAll)}}</td>
                                    </tr>
                                @endif
                                @if($rows->price_include_vat!='Y')
                                    <tr>
                                        <td colspan="4" style="text-align: right">{{trans('common.include_tax')}} 7%</td>
                                        <td style="text-align: right"><h6>{{$Invoice->currency_symbol.number_format($Tax)}}</h6></td>
                                    </tr>
                                @endif
                                @if($discount>0)
                                    <tr>
                                        <td></td>
                                        <td colspan="2" >{{$promotion_title}}</td>
                                        <td  style="text-align: right">{{trans('common.discount')}}</td>
                                        <td style="text-align: right">{{$Invoice->currency_symbol.number_format($discount_total)}}</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td colspan="4" style="text-align: right">{{trans('common.deposit')}}</td>
                                    <td style="text-align: right"><h6>-{{$Invoice->currency_symbol.number_format($Deposit)}}</h6></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                    <td style="text-align: right"><h4>{{$Invoice->currency_symbol.number_format($TotalsAll-$Deposit)}} </h4></td>
                                </tr>



                        @else
                            <tr>
                                <td colspan="4" style="text-align: right"><h4>{{trans('common.subtotal')}}:</h4></td>
                                <td style="text-align: right"><h4>{{$Invoice->currency_symbol.number_format($Deposit)}}</h4></td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                <td style="text-align: right"><h4>{{$Invoice->currency_symbol.number_format($Deposit)}} </h4></td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>


    <div class="row">
        <!-- accepted payments column -->

        @if($Package->package_partner=='Yes' || $Package->package_owner=='Yes')
            <div class="col-md-12">
                <!-- DIRECT CHAT PRIMARY -->
                <div class="box box-warning direct-chat direct-chat-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-credit-card"></i> {{trans('common.payment_methods')}}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-6">
                            <img src="{{asset('images/credit/omise-payments.png')}}" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            @foreach($BankInfo as $bank)
                                <div class="direct-chat-msg">
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="{{asset('images/credit/'.$bank->logo)}}" alt="{{$bank->bank_name}}"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        {{trans('common.bank').$bank->bank_name}} {{trans('common.bank_name').$bank->account_name}} <BR>
                                        {{trans('common.bank_account_number').$bank->bank_account_number}} {{trans('common.sub_bank').$bank->sub_bank}}
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>

                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        @else
            <div class="col-md-12">
                <!-- DIRECT CHAT PRIMARY -->
                <div class="box box-warning direct-chat direct-chat-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-credit-card"></i> {{trans('common.payment_methods')}}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                        @foreach($BankInfo as $bank)

                            <div class="col-md-6">
                                <div class="direct-chat-msg">
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="{{asset('images/credit/'.$bank->logo)}}" alt="{{$bank->bank_name}}"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        {{trans('common.bank').$bank->bank_name}} {{trans('common.bank_name').$bank->account_name}} <BR>
                                        {{trans('common.bank_account_number').$bank->bank_account_number}} {{trans('common.sub_bank').$bank->sub_bank}}
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
    @endif
    <!-- /.col -->


        <!-- /.col -->
    </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
    <!-- this row will not appear when printing -->
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-warning text-right">

                <a href="{{url('booking/download/invoice/pdf/'.$Invoice->invoice_id.'/'.$Invoice->invoice_type)}}" class="btn btn-primary" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> {{trans('common.print_invoice')}}
                </a>

            </div>


        </div>
    </div>

        <!-- /.content -->


<div class="row no-print">
    <div class="col-xs-12">
        <a href="{{Session::get('currentUrl')}}"  class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_payment_list')}}</a>

    </div>
</div>
            </div>
        </section>

    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script type="text/javascript">
        // Set default parameters
        OmiseCard.configure({
            publicKey: 'pkey_test_5dtunq6l6nl03ywhud7',
            image: 'https://toechok.com/setting/logo.jpg',
            frameLabel: 'TOECHOK CO.,LTD.',
        });

        OmiseCard.configureButton('#checkout-button-0', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_deposit')}} {{strtolower($currency_symbol)}}{{number_format($Deposit)}} ',
            submitLabel: '{{trans('common.pay_deposit')}}',
            amount: '{{$Deposit*100}}',
            currency: 'thb'
        });
        @if($Invoice->invoice_status=='2')
            OmiseCard.configureButton('#checkout-button-1', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{strtolower($currency_symbol)}}{{number_format($Totals-$Deposit)}}',
            submitLabel: '{{trans('common.pay_all')}}',
            amount: '{{($Totals-$Deposit)*100}}',
            currency: 'thb'
            });
        @else
          OmiseCard.configureButton('#checkout-button-1', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{strtolower($currency_symbol)}}{{number_format($Totals)}}',
            submitLabel: '{{trans('common.pay_all')}}',
            amount: '{{($Totals)*100}}',
            currency: 'thb'
        });
        @endif
        // Then, attach all of the config and initiate it by 'OmiseCard.attach();' method
        OmiseCard.attach();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.stepper').mdbStepper();
        });
        $('.delete-detail').on('click',function () {
            if(confirm('Confirm delete this cart detail?')){
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        // alert('test');
                        window.location =  window.location.href;
                    }
                });
            }
        });

        $(document).ready(function(){
            var counter = 1;
            $("#add").click(function () {
                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');
                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });

        });
    </script>


@endsection