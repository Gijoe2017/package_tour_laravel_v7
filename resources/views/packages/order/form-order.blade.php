@extends('layouts.package.master')

@section('program-highlight')
    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content bg padding-y border-top">
        <div class="container">

            <div class="row">
                <main class="col-sm-9">
                    <div class="card">
                        <table class="table table-hover shopping-cart-wrap">
                            <thead class="text-muted">
                            <tr>
                                <th scope="col" width="80%">Product</th>
                                <th scope="col" class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <figure class="media">
                                        <div class="img-wrap"><img src="images/banners/slide1-2.jpg" class="img-thumbnail img-sm"></div>
                                        <figcaption class="media-body">
                                            <h6 class="title text-truncate">ชื่อโปรแกรม xxxxx01</h6>
                                            <div class="price-wrap">
                                                <del>฿25,999</del> <span class="price-save-date">(ราคานี้ 2 วัน 23:59:36, 5 ที่สุดท้ายเท่านั้น)</span>
                                                <var class="price">฿ 24,500 <small class="text-muted">(ต่อท่าน)</small> <span class="price-save">ประหยัด ฿2,999</span></var>

                                            </div> <!-- price-wrap .// -->
                                            <dl class="dlist-inline small">
                                                <var class="price">
                                                    <select class="form-control form-control-sm same-line" style="width:100%;">
                                                        <option> วันเดินทาง</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 5-10 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> ประเภท</option>
                                                        <option> เด็ก</option>
                                                        <option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> จำนวน</option>
                                                        <option> 1 คน</option>
                                                        <option> 2 คน</option>
                                                        <option> 3 คน</option>
                                                        <option> 4 คน</option>
                                                    </select>
                                                </var>
                                                <dt>บริการเสริม</dt><br>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> เตียงเสริม</option>
                                                    <option> เตียงเสริม: 1</option>
                                                    <option> เตียงเสริม: 2</option>
                                                    <option> เตียงเสริม: 3</option>
                                                    <option> เตียงเสริม: 4</option>
                                                    <option> เตียงเสริม: 5</option>
                                                </select>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> อาหารเสริม</option>
                                                    <option> อาหารเสริม: 1</option>
                                                    <option> อาหารเสริม: 2</option>
                                                    <option> อาหารเสริม: 3</option>
                                                    <option> อาหารเสริม: 4</option>
                                                    <option> อาหารเสริม: 5</option>
                                                </select>
                                                <dl class="dlist-inline same-line">
                                                    <dt><a href="">+ ลบ</a> </dt></dt>
                                                </dl>
                                            </dl>
                                            <hr>
                                            <dl class="dlist-inline small">
                                                <var class="price">
                                                    <select class="form-control form-control-sm same-line" style="width:100%;">
                                                        <option> วันเดินทาง</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 5-10 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> ประเภท</option>
                                                        <option> เด็ก</option>
                                                        <option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> จำนวน</option>
                                                        <option> 1 คน</option>
                                                        <option> 2 คน</option>
                                                        <option> 3 คน</option>
                                                        <option> 4 คน</option>
                                                    </select>
                                                </var>
                                                <dt>บริการเสริม</dt><br>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> เตียงเสริม</option>
                                                    <option> เตียงเสริม: 1</option>
                                                    <option> เตียงเสริม: 2</option>
                                                    <option> เตียงเสริม: 3</option>
                                                    <option> เตียงเสริม: 4</option>
                                                    <option> เตียงเสริม: 5</option>
                                                </select>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> อาหารเสริม</option>
                                                    <option> อาหารเสริม: 1</option>
                                                    <option> อาหารเสริม: 2</option>
                                                    <option> อาหารเสริม: 3</option>
                                                    <option> อาหารเสริม: 4</option>
                                                    <option> อาหารเสริม: 5</option>
                                                </select>
                                                <dl class="dlist-inline same-line">
                                                    <dt><a href="">+ ลบ</a> </dt> | <dt><a href="">+ เพิ่ม</a> </dt>
                                                </dl>
                                            </dl>
                                            <hr>

                                        </figcaption>
                                    </figure>
                                </td>
                                <td class="text-right">
                                    <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a>
                                    <a href="" class="btn btn-outline-danger"> × Remove</a>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <figure class="media">
                                        <div class="img-wrap"><img src="images/banners/slide1-3.jpg" class="img-thumbnail img-sm"></div>
                                        <figcaption class="media-body">
                                            <h6 class="title text-truncate">ชื่อโปรแกรม xxxxx03</h6>
                                            <div class="price-wrap">
                                                <del>฿25,999</del> <span class="price-save-date">(ราคานี้ 2 วัน 23:59:36, 5 ที่สุดท้ายเท่านั้น)</span>
                                                <var class="price">฿ 24,500 <small class="text-muted">(ต่อท่าน)</small> <span class="price-save">ประหยัด ฿2,999</span></var>

                                            </div> <!-- price-wrap .// -->
                                            <dl class="dlist-inline small">
                                                <var class="price">
                                                    <select class="form-control form-control-sm same-line" style="width:100%;">
                                                        <option> วันเดินทาง</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 5-10 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                        <option> 1-5 ม.ค. 2562</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> ประเภท</option>
                                                        <option> เด็ก</option>
                                                        <option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>
                                                    </select>
                                                    <select class="form-control form-control-sm same-line" style="width:90%;">
                                                        <option> จำนวน</option>
                                                        <option> 1 คน</option>
                                                        <option> 2 คน</option>
                                                        <option> 3 คน</option>
                                                        <option> 4 คน</option>
                                                    </select>
                                                </var>
                                                <dt>บริการเสริม</dt><br>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> เตียงเสริม</option>
                                                    <option> เตียงเสริม: 1</option>
                                                    <option> เตียงเสริม: 2</option>
                                                    <option> เตียงเสริม: 3</option>
                                                    <option> เตียงเสริม: 4</option>
                                                    <option> เตียงเสริม: 5</option>
                                                </select>
                                                <select class="form-control form-control-sm same-line" style="width:100%;">
                                                    <option> อาหารเสริม</option>
                                                    <option> อาหารเสริม: 1</option>
                                                    <option> อาหารเสริม: 2</option>
                                                    <option> อาหารเสริม: 3</option>
                                                    <option> อาหารเสริม: 4</option>
                                                    <option> อาหารเสริม: 5</option>
                                                </select>
                                                <dl class="dlist-inline same-line">
                                                    <dt><a href="">+ เพิ่ม</a> </dt>
                                                </dl>
                                            </dl>
                                            <hr>

                                        </figcaption>
                                    </figure>
                                </td>
                                <td class="text-right">
                                    <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a>
                                    <a href="" class="btn btn-outline-danger"> × Remove</a>

                                </td>
                            </tr>
                            <hr>

                            </tbody>
                        </table>
                    </div> <!-- card.// -->

                </main> <!-- col.// -->
                <aside class="col-sm-3">
                    <p class="alert alert-success">Add USD 5.00 of eligible items to your order to qualify for FREE Shipping. </p>
                    <dl class="dlist-align">
                        <dt>Total price: </dt>
                        <dd class="text-right">USD 568</dd>
                    </dl>
                    <dl class="dlist-align">
                        <dt>Discount:</dt>
                        <dd class="text-right">USD 658</dd>
                    </dl>
                    <dl class="dlist-align h4">
                        <dt>Total:</dt>
                        <dd class="text-right"><strong>USD 1,650</strong></dd>
                    </dl>
                    <hr>
                    <figure class="itemside mb-3">
                        <aside class="aside"><img src="images/icons/pay-visa.png"></aside>
                        <div class="text-wrap small text-muted">
                            Pay 84.78 AED ( Save 14.97 AED )
                            By using ADCB Cards
                        </div>
                    </figure>
                    <figure class="itemside mb-3">
                        <aside class="aside"> <img src="images/icons/pay-mastercard.png"> </aside>
                        <div class="text-wrap small text-muted">
                            Pay by MasterCard and Save 40%. <br>
                            Lorem ipsum dolor
                        </div>
                    </figure>

                </aside> <!-- col.// -->
            </div>

        </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->

    <!-- ========================= SECTION  ========================= -->
    <section class="section-name bg-white padding-y">
        <div class="container">
            <header class="section-heading">
                <h2 class="title-section">Section name</h2>
            </header><!-- sect-heading -->

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div><!-- container // -->
    </section>
    <!-- ========================= SECTION  END// ========================= -->

    <!-- ========================= SECTION  ========================= -->
    <section class="section-name bg padding-y">
        <div class="container">
            <h4>Another section if needed</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div><!-- container // -->
    </section>
    <!-- ========================= SECTION  END// ========================= -->
@endsection