@extends('layouts.package.master')

@section('program-highlight')
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;

        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }

        .omise-checkout-button{
            padding: 10px 20px;
            background-color: #00a65a;
            border-color: #008d4c;
            color: #ffffff;
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
        }
        h5{
            font-size: 18px;
            font-weight: bold;
            color: orange;
        }

    </style>

    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')
           </div>
           </div>
    </section>

<?php

            $Package=DB::table('package_tour as a')
                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                ->where('a.packageID',$Invoice->invoice_package_id)
                ->first();

            $Timeline=\App\Timeline::where('id','37850')->first();

            $media=\App\Media::where('id',$Timeline->avatar_id)->first();

            $BankInfo=DB::table('business_verified_bank as a')
                ->join('banks as b','b.bank_code','=','a.bank_id')
                ->where('a.timeline_id',$Timeline->id)
                ->where('b.language_code',Auth::user()->language)
                ->get();
           // dd($BankInfo);
            $BusinessInfo=DB::table('business_verified_info1')
                ->where('timeline_id',$Timeline->id)
                ->first();

            $BusinessInfo1=DB::table('business_verified_info2')
                ->where('language_code',Auth::user()->language)
                ->where('timeline_id',$Timeline->id)
                ->first();

            $AddressBook=DB::table('address_book as a')
                ->join('countries as b','b.country_id','=','a.entry_country_id')
                ->where('a.timeline_id',Auth::user()->timeline_id)
                ->where('a.default_address','1')
                ->where('a.address_type','1')
                ->first();

            $Details=DB::table('package_booking_details as a')
//                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                ->where('a.package_id',$Invoice->invoice_package_id)
                ->where('a.booking_id',$Invoice->invoice_booking_id)
                ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                ->get();
           // dd($Details);
            $Deposit=0;$Totals=0;

            $currency_code=$Invoice->currency_code;
            $currency_symbol=$Invoice->currency_symbol;

            $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();

        ?>

        <!-- Main content -->
        <section class="invoice">
            <div class="container">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {{trans('common.invoice_deposit')}}
                        {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-8 invoice-col">
                    @if($media!=null)
                        <img class="logo-invoice" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @else
                        <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @endif
                    {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                </div>
                <div class="col-sm-4 invoice-col">
                    <b>{{trans('common.order_id')}}:</b> {{$Invoice->invoice_booking_id}}<br>
                    <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($Invoice->invoice_date))}}<br>
                    <b>{{trans('common.status')}}:</b> {{trans('common.'.$Status->status_name)}}<br>
                    {{--<b>{{trans('common.reference')}}:</b> ACB11<br>--}}
                </div>

            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <br>
                <div class="col-sm-6 invoice-col">
                    <strong>Billing From/จาก:  </strong><br>
                    <address>
                        {{$BusinessInfo1->legal_name}}<br>
                        {{$BusinessInfo1->address}}<br>
                        {{--{{$BusinessInfo1->address}}, {{$city->city}}<br>--}}
                        {{--{{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>--}}
                        {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                        {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                    </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                    <strong>Billing To/ถึง:</strong>
                    @if($AddressBook)
                    <address>
                        {{$AddressBook->entry_company?$AddressBook->entry_company:$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}} <BR>
                        {!! $AddressBook->address_show !!}<br>
                        {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                        {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                    </address>
                        @else
                    @endif
                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('common.items')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th>{{trans('common.unit_price')}}</th>
                            <th>{{trans('common.unit')}}</th>
                            <th>{{trans('common.unit_total')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;$TotalsAll=0;$Totals=0;$Total=0;$Tax=0;$discount=0;$pay_more=0;$PriceTax=0;$Price_sub=0; $AdditionalPrice=0;$PriceVisa=0;?>

                        @foreach($Details as $rows)
                            <?php
                            //dd($rows);
                            $include_vat=$rows->price_include_vat;
                            $Timeline=\App\Timeline::where('id',$rows->timeline_id)->first();


                            $Detail_title=$rows->deposit_price;
                            $Deposit+=round($rows->deposit_price*$rows->number_of_person);
                            $PriceAdditional=0;

                            $Additional=DB::table('package_booking_additional')
                                ->where('booking_detail_id',$rows->booking_detail_id)
                                ->get();

                            $Promotion=DB::table('package_booking_promotion')
                                ->where('booking_detail_id',$rows->booking_detail_id)
                                ->first();
                            $promotion_title='';
                            if($Promotion){
                                $promotion_title=$Promotion->promotion_title;
                                if($Promotion->promotion_operator=='Between'){
                                    if($Promotion->promotion_unit=='%'){
                                          $discount=$rows->booking_normal_price*$Promotion->promotion_value/100;
                                    }else{
                                          $discount=$rows->booking_normal_price-$Promotion->promotion_value;
                                    }
                                }else{
                                    if($Promotion->promotion_operator2=='Up'){
                                        if($Promotion->promotion_unit=='%'){
                                            $pay_more=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                        }else{
                                            $pay_more=$Promotion->promotion_value;
                                        }
                                    }else{
                                        if($Promotion->promotion_unit=='%'){
                                            $discount=$rows->booking_realtime_price*$Promotion->promotion_value/100;
                                        }else{
                                            $discount=$rows->booking_realtime_price-$Promotion->promotion_value;
                                        }
                                    }
                                }
                            }

                            ?>
                            <tr>
                                <td valign="top" align="center">{{$i++}}</td>
                                <td>
                                    {!! $rows->package_detail_title !!}<BR>
                                    <strong class="text-info">{{trans('common.tour_deposit')}}:{{$Detail_title}} x {{$rows->number_of_person}}</strong>
                                </td>
                                <td valign="top">
                                    @if($pay_more>0)
                                        <?php
                                        $Price_sub=$rows->booking_realtime_price+$pay_more;
                                        ?>
                                        {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}+{{$pay_more}}
                                    @elseif($discount>0)
                                        <?php
                                        $Price_sub=$rows->booking_normal_price-$discount;
                                        ?>
                                        <del>{{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}</del>
                                        {{$Invoice->currency_symbol.number_format($rows->booking_normal_price-$discount)}}
                                    @else
                                        <?php
                                        $Price_sub=$rows->booking_normal_price;
                                        ?>
                                        {{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}
                                    @endif
                                </td>
                                <td valign="top">{{$rows->number_of_person}}</td>
                                <td style="text-align: right">
                                    {{$Invoice->currency_symbol.number_format($Price_sub*$rows->number_of_person)}}
                                </td>
                            </tr>
                            @foreach ($Additional as $rowA)
                                @if($rowA->need_someone_share!='Y')
                                    <tr>
                                        <td valign="top" align="center">{{$i++}}</td>
                                        <td><strong>{{trans('common.additional')}} {{$rowA->additional_service}}</strong></td>
                                        <td>{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                        <td>1</td>
                                        <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                    </tr>
                                    <?php $PriceAdditional=$rowA->price_service;?>
                                @else
                                    <tr>
                                        <td valign="top" align="center">{{$i++}}</td>
                                        <td colspan="2"><strong>{{trans('common.additional')}} {{$rowA->additional_service}}</strong> <span class="text-danger">{{trans('common.wait_for_the_tour_operator_to_find_a_guest_to_share')}}</span> </td>
                                        <td>1</td>
                                        <td style="text-align: right"><del>{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</del></td>
                                    </tr>
                                @endif
                            @endforeach

                            @if($rows->price_for_visa>0)
                                <?php $PriceVisa=$rows->price_for_visa*$rows->number_of_need_visa;?>
                                <tr>
                                    <td valign="top" align="center">{{$i++}}</td>
                                    <td><strong>{{$rows->price_visa_details}} </strong></td>
                                    <td>{{$Invoice->currency_symbol.number_format($rows->price_for_visa)}}</td>
                                    <td>{{$rows->number_of_need_visa}}</td>
                                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($PriceVisa)}}</td>
                                </tr>
                            @endif

                            <?php
                                $Total=round($Price_sub*$rows->number_of_person)+$PriceAdditional+$PriceVisa;

                                if($include_vat!='Y'){
                                    $PriceTax+=round($Total*7/100);
                                }
                                $Totals+=round($Price_sub*$rows->number_of_person)+$PriceAdditional+$PriceVisa;
                                $TotalsAll+=$Total+$PriceTax;
                            ?>
                        @endforeach
                        <tr class="bg-gray"><td colspan="5"><br></td></tr>
                        <tr>
                            @if($include_vat=='Y')
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</strong></td>
                            @else
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.subtotal')}}</strong></td>
                            @endif
                            <td  style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Totals)}}</strong></td>
                        </tr>
                        @if($discount>0)
                            <tr>
                                <td></td>
                                <td colspan="2" >{{$promotion_title}}</td>

                            <td  style="text-align: right">{{trans('common.discount')}}</td>
                            <td style="text-align: right">{{$Invoice->currency_symbol.number_format($discount*$rows->number_of_person)}}</td>
                        </tr>
                        @endif
                        @if($include_vat!='Y')
                        <tr>
                            <td colspan="4" style="text-align: right">{{trans('common.include_tax')}} 7%</td>
                            <td style="text-align: right">{{$Invoice->currency_symbol.number_format($PriceTax)}}</td>
                        </tr>
                        @endif
                        {{--<tr>--}}
                            {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                            {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                        {{--</tr>--}}

                            {{--<tr>--}}
                                {{--<td colspan="4" style="text-align: right"><strong>{{trans('common.total_deposit')}}:</strong></td>--}}
                                {{--<td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Deposit)}} </strong></td>--}}
                            {{--</tr>--}}

                        <tr>
                            <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                            <td style="text-align: right"><h4>{{$Invoice->currency_symbol.number_format($TotalsAll)}} </h4></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>


            <div class="row">
                <!-- accepted payments column -->

                @if($Package->package_partner=='Yes' || $Package->package_owner=='Yes')
                    <div class="col-md-6">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Payment Methods/ช่องท่างชำระเงิน:</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment_All_invoice')}}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="booking_id" value="{{$Invoice->invoice_booking_id}}" >
                                    <input type="hidden" name="type" value="1" >
                                    <input type="hidden" name="invoice_id" value="{{$Invoice->invoice_id}}" >
                                    <input type="hidden" name="amount" value="{{$TotalsAll}}" >
                                    <input type="hidden" name="currency" value="{{strtolower($Invoice->currency_code)}}" >
                                    <!-- small box -->
                                    <div class="small-box bg-grey">
                                        <div class="inner text-center">
                                            <h4>{{trans('common.list_of_payment_tour_deposit_payment')}}</h4>
                                            <h3>{{$Invoice->currency_symbol.number_format($Deposit)}}</h3>
                                            <p>
                                                {{trans('common.payment_date')}} : {{date('d/m/Y H:i',strtotime($Invoice->payment_date))}}<br>
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        @if($Invoice->invoice_status!='2' && $Invoice->invoice_status!='4')
                                            <a href="#" class="small-box-footer">
                                                {{trans('common.status')}}: {{trans('common.deposit_paid')}} <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                            <button type="submit" class="btn btn-default btn-block btn-lg" disabled id="checkout-button-0"> {{trans('common.pay_deposit')}}</button>
                                            @else
                                            <?php
                                                $status=DB::table('booking_status')->where('booking_status',$Booking->booking_status)->first();
                                            ?>
                                            <a href="#" class="small-box-footer">
                                                {{trans('common.status')}} : {{trans('common.'.$status->status_name)}} <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        @endif

                                    </div>
                                </form>
                                <!--/.direct-chat-messages-->
                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <div class="col-md-6">
                            <!-- DIRECT CHAT PRIMARY -->
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment_All_invoice')}}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="booking_id" value="{{$Invoice->invoice_booking_id}}" >
                                    <input type="hidden" name="type" value="2" >
                                    <input type="hidden" name="invoice_id" value="{{$Invoice->invoice_id}}" >
                                    <input type="hidden" name="amount" value="{{($TotalsAll)}}" >
                                    <input type="hidden" name="deposit" value="{{$Deposit}}" >
                                    <input type="hidden" name="currency" value="{{strtolower($Invoice->currency_code)}}" >
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner text-center">
                                            <h4>{{trans('common.total_amount')}}</h4>
                                            <h3>{{$Invoice->currency_symbol.number_format($TotalsAll)}}</h3>
                                            <p>
                                                {{trans('common.payment_due_date')}} : {{date('d/m/Y H:i',strtotime($Invoice->invoice_payment_date))}}<br>
                                            </p>
                                        </div>
                                        <a href="#" class="small-box-footer">
                                            {{trans('common.status')}}: {{trans('common.waiting_for_payment')}} <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                        @if($Invoice->invoice_status!='4')
                                            <div class="icon">
                                                <i class="fa fa-file-text-o"></i>
                                            </div>
                                            <button type="submit" class="btn btn-info btn-block btn-lg" id="checkout-button-1"> {{trans('common.pay_all')}}</button>
                                        @else
                                            <?php
                                            $status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                                            ?>
                                            <a href="#" class="small-box-footer">
                                                {{trans('common.status')}} : {{trans('common.'.$status->status_name)}} <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        @endif
                                    </div>
                                </form>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    <div class="col-md-12">
                            <!-- DIRECT CHAT PRIMARY -->
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-credit-card"></i> {{trans('common.payment_methods')}}</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-md-6">
                                        @foreach($BankInfo as $bank)
                                            <div class="direct-chat-msg">
                                                <!-- /.direct-chat-info -->
                                                <img class="direct-chat-img" src="{{asset('images/credit/'.$bank->logo)}}" alt="{{$bank->bank_name}}"><!-- /.direct-chat-img -->
                                                <div class="direct-chat-text">
                                                    {{trans('common.bank').$bank->bank_name}} {{trans('common.bank_name').$bank->account_name}} <BR>
                                                    {{trans('common.bank_account_number').$bank->bank_account_number}} {{trans('common.sub_bank').$bank->sub_bank}}
                                                </div>
                                                <!-- /.direct-chat-text -->
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            @if(($Invoice->invoice_status=='1' && $Invoice->invoice_type=='1') || ($Invoice->invoice_status=='1' && $Invoice->invoice_type=='2'))
                                                <a href="{{url('booking/show/form_notification/'.$Invoice->invoice_booking_id.'/all')}}" class="btn btn-warning btn-lg"><i class="fa fa-paper-plane"></i> {{trans('common.payment_notification')}}</a>

                                                @if($Invoice->invoice_type=='2')
                                                    <p class="text-danger text-center">
                                                        <strong>{{trans('common.please_pay_the_remaining_amount_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}</strong>
                                                    </p>
                                                @else
                                                    <p class="text-danger text-center">
                                                        <strong>{{trans('common.please_pay_the_deposit_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}</strong>
                                                    </p>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                @else
                    <div class="col-md-12">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-credit-card"></i> {{trans('common.payment_methods')}}</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-6">
                                    @foreach($BankInfo as $bank)
                                        <div class="direct-chat-msg">
                                            <!-- /.direct-chat-info -->
                                            <img class="direct-chat-img" src="{{asset('images/credit/'.$bank->logo)}}" alt="{{$bank->bank_name}}"><!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                {{trans('common.bank').$bank->bank_name}} {{trans('common.bank_name').$bank->account_name}} <BR>
                                                {{trans('common.bank_account_number').$bank->bank_account_number}} {{trans('common.sub_bank').$bank->sub_bank}}
                                            </div>
                                            <!-- /.direct-chat-text -->
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        @if(($Invoice->invoice_status=='1' && $Invoice->invoice_type=='1') || ($Invoice->invoice_status=='1' && $Invoice->invoice_type=='2'))
                                            <a href="{{url('booking/show/form_notification/'.$Invoice->invoice_booking_id.'/all')}}" class="btn btn-warning btn-lg"><i class="fa fa-paper-plane"></i> {{trans('common.payment_notification')}}</a>

                                            @if($Invoice->invoice_type=='2')
                                                <p class="text-danger text-center">
                                                    <strong>{{trans('common.please_pay_the_remaining_amount_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}</strong>
                                                </p>
                                            @else
                                                <p class="text-danger text-center">
                                                    <strong>{{trans('common.please_pay_the_deposit_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}</strong>
                                                </p>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                @endif
                <!-- /.col -->

                <div class="col-md-12">
                    <div class="box box-warning direct-chat direct-chat-warning">
                    <?php
                    $Condition=DB::table('condition_in_package_details as a')
                        ->join('package_condition as b','b.condition_code','=','a.condition_id')
                        ->where('a.packageID',$Invoice->invoice_booking_id)
                        ->where('a.condition_group_id','8')
                        ->where('b.language_code',Session::get('language'))
                        ->get();
                    ?>
                    @if($Condition)
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Remark/หมายเหตุ</h3>
                        </div>
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages-payment">
                            <!-- Message. Default to the left -->
                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <!-- /.direct-chat-info -->
                                <div class="attachment-pushed">
                                    <div class="attachment-text">
                                        @foreach($Condition as $rows)
                                        {!! $rows->condition_title !!}
                                        @endforeach
                                    </div>
                                    <!-- /.attachment-text -->
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                            <hr>
                        </div>
                        <!--/.direct-chat-messages-->
                    </div>
                    <!-- /.box-body -->
                    @endif
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="{{url('booking/show/invoice/'.$Invoice->invoice_booking_id.'/all')}}" target="_blank" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                    <a href="{{url('booking/download/invoice/pdf/'.$Invoice->invoice_booking_id)}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </a>
                </div>
            </div>
            </div>
        </section>
        <!-- /.content -->


    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script type="text/javascript">
        // Set default parameters
        OmiseCard.configure({
            publicKey: 'pkey_test_5dtunq6l6nl03ywhud7',
            image: 'https://toechok.com/setting/logo.jpg',
            frameLabel: 'TOECHOK CO.,LTD.',
        });

        OmiseCard.configureButton('#checkout-button-0', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_deposit')}} {{strtolower($currency_symbol)}}{{number_format($Deposit)}} ',
            submitLabel: '{{trans('common.pay_deposit')}}',
            amount: '{{$Deposit*100}}',
            currency: 'thb'
        });
        @if($Invoice->invoice_status=='2')
            OmiseCard.configureButton('#checkout-button-1', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{strtolower($currency_symbol)}}{{number_format($TotalsAll-$Deposit)}}',
            submitLabel: '{{trans('common.pay_all')}}',
            amount: '{{($TotalsAll-$Deposit)*100}}',
            currency: 'thb'
            });
        @else
          OmiseCard.configureButton('#checkout-button-1', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{strtolower($currency_symbol)}}{{number_format($TotalsAll)}}',
            submitLabel: '{{trans('common.pay_all')}}',
            amount: '{{($TotalsAll)*100}}',
            currency: 'thb'
        });
        @endif
        // Then, attach all of the config and initiate it by 'OmiseCard.attach();' method
        OmiseCard.attach();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.stepper').mdbStepper();
        });
        $('.delete-detail').on('click',function () {
            if(confirm('Confirm delete this cart detail?')){
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        // alert('test');
                        window.location =  window.location.href;
                    }
                });
            }
        });

        $(document).ready(function(){
            var counter = 1;
            $("#add").click(function () {
                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');
                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });

        });
    </script>


@endsection