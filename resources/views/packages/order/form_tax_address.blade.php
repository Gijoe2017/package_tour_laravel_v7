        <link rel="stylesheet" href="{{asset('member/assets/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('member/assets/select2/dist/css/select2.min.css')}}">
        <script src="{{asset('member/assets/select2/dist/js/select2.full.min.js')}}"></script>

<div class="col-sm-12">
    <hr>
         <div class="form-group">
            <h4><i class="fa fa-info"></i> {{trans('common.information_for_receipt_or_tax_invoice')}}</h4>
         </div>

         <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.name_company_for_receipt_tax_invoice')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="tax_business_name" name="tax_business_name" required  placeholder="{{trans('common.name_company_for_receipt_tax_invoice')}}">
            </div>
         </div>

         <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.id_card_number')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="id_card_number" id="id_card_number" required placeholder="{{trans('common.id_card_number')}}">
            </div>
         </div>
    <hr>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.emails')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="tax_email" id="tax_email" placeholder="{{trans('common.emails')}}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.phone')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="tax_phone" id="tax_phone" placeholder="{{trans('common.phone')}}" required>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.address')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" id="address" placeholder="{{trans('common.address')}}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.country')}} <span class="text-danger">*</span></strong></label>
            <div class="col-sm-8">
                <?php
                $Country=\App\Country::where('language_code',Auth::user()->language)->get();
                if($Country->count()==0){
                    $Country=\App\Country::where('language_code','en')->get();
                }
                ?>
                <select name="country_id" id="country_id" class="form-control select2" required>
                    <option value="">{{trans('common.choose')}}</option>
                        @foreach($Country as $rows)
                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                        @endforeach
                </select>
            </div>
        </div>

        <div class="form-group box-state">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.state')}}</strong></label>
            <div class="col-sm-8">
                   <select name="state_id" id="state_id" class="form-control select2">

                   </select>
            </div>
        </div>

        <div class="form-group box-state city_id">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city')}}</strong></label>
            <div class="col-sm-8">
                <select name="city_id" id="city_id" class="form-control select2">

                </select>
            </div>
        </div>

        <div class="form-group box-state city_sub1_id">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.city_sub')}}</strong></label>
            <div class="col-sm-8">
                <select name="city_sub1_id" id="city_sub1_id" class="form-control select2">

                </select>
            </div>
        </div>
        <div class="form-group city_sub1_id">
            <label for="inputPassword" class="col-sm-4 col-form-label"><strong>{{trans('common.zipcode')}}</strong></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="{{trans('common.zipcode')}}">
            </div>
        </div>


</div>
        <script src="{{asset('member/assets/bootstrap/js/app.js')}}"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });
</script>

