
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    table {
        font-family: "THSarabunNew";
        border-collapse: collapse;

    }

    th, td {
        border-bottom: 1px solid #ddd;
    }
    .page-break {
        page-break-after: always;
    }
</style>

        <?php


        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Invoice->invoice_package_id)
            ->first();

        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();
       // dd($media);
        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();
        // dd($BusinessInfo1);
        $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
        if(!$country){
            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
        }

        $states=DB::table('states')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$states){
            $states=DB::table('states')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('language_code','en')
                ->first();
        }
        $city=DB::table('cities')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('city_id',$BusinessInfo->city_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$city){
            $city=DB::table('cities')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('city_id',$BusinessInfo->city_id)
                ->where('language_code','en')
                ->first();
        }


        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();



        $Details=DB::table('package_booking_details as a')
            ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.booking_id',$Invoice->invoice_booking_id)
            ->where('a.timeline_id',$Invoice->invoice_timeline_id)
            ->get();
       // dd($rows);
        $Deposit=0;
        foreach ($Details as $rowD){

            $PackageDetailsOne=DB::table('package_details')
                ->where('packageDescID',$rowD->package_detail_id)
                ->first();

            if($PackageDetailsOne->season=='Y'){
                $order_by="desc";
            }else{
                $order_by="asc";
            }
            $Condition=DB::table('condition_in_package_details as a')
                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                ->where('b.condition_group_id','1')
                ->where('b.formula_id','>',0)
                ->where('a.packageID',$rowD->package_id)
                ->orderby('c.value_deposit',$order_by)
                ->first();

            if($Condition){
                $Deposit_title=$Condition->value_deposit;
                $Deposit+=$Condition->value_deposit*$rowD->number_of_person;
            }
        }

        $Totals=DB::table('package_booking_details as a')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.package_id',$Invoice->invoice_package_id)
            ->sum('a.booking_normal_price');

        $InvoiceNo=sprintf('%07d',$Invoice->invoice_id);




        ?>
<table class="table" width="100%">
    <tr>
        <td colspan="3" >
            <h2 class="page-header">
                @if($Invoice->invoice_type==1)
                {{trans('common.deposit_invoice')}}
                @else
                    {{trans('common.invoice_balance')}}
                    @endif
                <small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y',strtotime($Invoice->invoice_date))}}</small>
            </h2>
        </td>
    </tr>
    <tr>

        <td colspan="3">
               <table class="table" width="100%">
                    <tr>
                        <td width="60%">
                            @if($media!=null)
                                <img class="logo-invoice" style="height: 110px" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @else
                                <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                            @endif
                        </td>
                        <td>
                            <b>{{trans('common.invoice_no')}}: #{{$Invoice->invoice_id}}</b><br>
                            <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i')}}<br>
                            <b>{{trans('common.order_id')}}:</b> #{{$Invoice->invoice_booking_id}}<br>

                            <?php
                            $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                            ?>
                           <strong>
                               {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}}
                                <small>
                                   @if($Invoice->invoice_type=='1' && $Invoice->invoice_status=='2')
                                        {{trans('common.payment_date').':'.$Invoice->payment_date.' '.$Invoice->payment_time}}
                                   @elseif($Invoice->invoice_type=='2' && $Invoice->invoice_status=='4')
                                        {{trans('common.payment_date').':'.$Invoice->payment_date.' '.$Invoice->payment_time}}
                                   @else
                                       {{trans('common.payment_due_date').':'.$Invoice->invoice_payment_date}}
                                   @endif
                                </small>
                           </strong>
                        </td>
                    </tr>

                </table>
        </td>
    </tr>

    <tr>
        <td colspan="3" >
            <table class="table" width="100%">
                <tr>
                    <td width="50%">
                        <strong>Billing From/จาก:  </strong><br>
                        <address>
                            {{$BusinessInfo1->legal_name}}<br>
                            {{$BusinessInfo1->address}}<br>

                            {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                            {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                        </address>
                    </td>
                    <td >
                        <strong>Billing To/ถึง:</strong>
                        @if($AddressBook)
                            <address>
                                {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                {{$AddressBook->address_show}}<br>

                                {{trans('common.phone')}}: {{$AddressBook->entry_phone?$AddressBook->entry_phone:'-'}}<br>
                                {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                            </address>
                        @endif
                    </td>
                </tr></table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table style="width: 100%">
                <thead>
                <tr>
                    <th>{{trans('common.items')}}</th>
                    <th>{{trans('common.description')}}</th>
                    <th>{{trans('common.unit_price')}}</th>
                    <th>{{trans('common.unit')}}</th>
                    <th align="right">{{trans('common.unit_total')}}</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;$TotalsAll=0;$Tax=0;$discount=0;$price_include_vat=''; $Deposit=0;$AdditionalPrice=0;?>

                @foreach($Details as $rows)
                    <?php

                    $Timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                    // dd($Timeline);
                    $PackageDetailsOne=DB::table('package_details')
                        ->where('packageDescID',$rows->package_detail_id)
                        ->first();

                    if($PackageDetailsOne->season=='Y'){
                        $order_by="desc";
                    }else{
                        $order_by="asc";
                    }

                    $Condition=DB::table('condition_in_package_details as a')
                        ->join('package_condition as b','b.condition_code','=','a.condition_id')
                        ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                        ->where('b.condition_group_id','1')
                        ->where('b.formula_id','>',0)
                        ->where('a.packageID',$rows->package_id)
                        ->orderby('c.value_deposit',$order_by)
                        ->first();

                    if($Condition){
                        $Deposit_title=$Condition->value_deposit;
                        $Deposit+=$Condition->value_deposit*$rows->number_of_person;
                    }

                    $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();

                    $st=explode('-',$rows->packageDateStart);
                    $end=explode('-',$rows->packageDateEnd);

                    if($st[1]==$end[1]){
                        $date=\Date::parse($rows->packageDateStart);
                        $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                        // dd($end[0]);
                    }else{
                        $date=\Date::parse($rows->packageDateStart);
                        $date1=\Date::parse($rows->packageDateEnd);
                        $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                    }


                    $promotion_title=null;$every_booking=0;

                    $promotion=\App\PackagePromotion::where('packageDescID',$rows->package_detail_id)->active()
                        ->orderby('promotion_date_start','asc')
                        ->first();

                    $data_target=null;
                    if($promotion && $promotion->promotion_operator!='Mod'){
                        $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                    }
                    //   dd($promotion);

                    ?>
                    <tr>
                        <td align="center">{{$i++}}</td>
                        <td>
                            <strong> {{$rows->TourType}}: {{trans('common.traveling_date')}} {{$package_date}}</strong><br>
                            {{$Package->packageName}}<BR>
                            {{trans('common.sell_by').':'. $Timeline->username}} <BR>
                            <span class="text-danger"> {{trans('common.deposit')}} {{$rows->TourType}}: {{$Invoice->currency_symbol.number_format($Deposit_title)}} x {{$rows->number_of_person}}</span>
                        </td>
                        <td>{{$Invoice->currency_symbol.number_format($rows->booking_normal_price)}}</td>
                        <td align="center">{{$rows->number_of_person}}</td>
                        <td style="text-align: right">
                            {{$Invoice->currency_symbol.number_format($rows->booking_normal_price*$rows->number_of_person)}}
                        </td>
                    </tr>
                    @if($Additional)
                        @foreach($Additional as $rowA)
                            <tr>
                                <td>{{$i++}}</td>
                                <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                <td>{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                                <td align="center">1</td>
                                <td style="text-align: right">{{$Invoice->currency_symbol.number_format($rowA->price_service)}}</td>
                            </tr>
                            <?php
                            $AdditionalPrice+=$rowA->price_service;
                            ?>
                        @endforeach
                    @endif

                    <?php
                    if($promotion){
                        if($rows->booking_normal_price!=$rows->booking_realtime_price){
                            $discount+=($rows->booking_normal_price-$rows->booking_realtime_price);
                        }
                    }
                    $TotalsAll+=round($rows->booking_normal_price*$rows->number_of_person);
                    $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                    if($rows->price_include_vat=='Y'){
                        $price_include_vat=$rows->price_include_vat;
                    }


                    ?>

                @endforeach


                <?php
                $Totals=$TotalsAll+$AdditionalPrice;
                ?>
                <tr>
                    <td colspan="4" style="text-align: right">
                        @if($price_include_vat=='Y')
                         {{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)
                        @else
                         {{trans('common.subtotal')}}
                        @endif
                    </td>

                    <td  style="text-align: right">{{$Invoice->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}}</td>
                </tr>

                <tr>
                    <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                    <td style="text-align: right">{{$Invoice->currency_symbol.number_format($discount)}}</td>
                </tr>
                {{--<tr>--}}
                {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                {{--</tr>--}}

                @if($Invoice->invoice_type=='2')

                    <tr>
                        <td colspan="4" style="text-align: right"><strong>{{trans('common.deposit')}}:</strong></td>
                        <td style="text-align: right"><strong>-{{$Invoice->currency_symbol.number_format($Deposit)}}</strong></td>
                    </tr>

                    @if($price_include_vat!='Y')
                        <?php $Tax=$Totals*.07;?>
                        <tr>
                            <td colspan="4" style="text-align: right"><strong>({{trans('common.include_tax')}} 7%):</strong></td>
                            <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Tax)}} </strong></td>
                        </tr>
                    @endif

                    <tr>
                        <td colspan="4" style="text-align: right"><strong>{{trans('common.total_amount')}}:</strong></td>
                        <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($TotalsAll+$Tax+$AdditionalPrice-$discount-$Deposit)}} </strong></td>
                    </tr>
                @else
                    <tr>
                        <td colspan="4" style="text-align: right"><strong>{{trans('common.total_deposit')}}:</strong></td>
                        <td style="text-align: right"><strong>{{$Invoice->currency_symbol.number_format($Deposit)}}</strong></td>
                    </tr>
                @endif

                </tbody>
            </table>

        </td>
    </tr>
  
</table>
      

