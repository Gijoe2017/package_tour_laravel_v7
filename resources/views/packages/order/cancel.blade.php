@extends('layouts.package.master')
@section('program-highlight')
    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">

                        <!-- title row -->

                            <div class="col-xs-12">
                                <h3 class="page-header">
                                    {{trans('common.order_id')}}:#{{$Booking->booking_id}}
                                    <small class="pull-right">{{trans('common.booking_date')}}: {{date('d/m/Y',strtotime($Booking->booking_date))}}</small>
                                </h3>
                                <hr>

                                @foreach($Booking_details as $rows)
                                    <p>{!! $rows->package_detail_title !!}</p>
                                @endforeach
                                <hr>
                            </div>
                            <!-- /.col -->

                            <div class="col-sm-12 invoice-col">
                                <h5><i class="fa fa-check-circle"></i> {{trans('common.option_cancel')}} </h5>
                            </div>

                        <!-- this row will not appear when printing -->

                            <div class="col-xs-12">
                                <hr>
                                <div class="btn-group ">
                                    <button type="button" data-id="{{$Booking->booking_id}}" id="all" data-option="all" class="btn btn-lg btn-warning option-cancel"><i class="fa fa-times"></i> {{trans('common.cancel_booking')}}</button>
                                    @if($Booking_details->count()>1)
                                        @if($check_package->count()>1)
                                            <button type="button" data-id="{{$Booking->booking_id}}" id="package" data-option="package" class="btn btn-lg btn-info option-cancel"><i class="fa fa-times"></i> {{trans('common.cancel_some_package')}}</button>
                                        @endif
                                            <button type="button" data-id="{{$Booking->booking_id}}" id="someone" data-option="someone" class="btn btn-lg btn-danger option-cancel"><i class="fa fa-times"></i> {{trans('common.cancel_individually')}}</button>
                                    @endif
                                </div>

                            </div>

                            <div id="show-option" class="col-xs-12">

                            </div>

                    <!-- /.content -->
                    <div class="clearfix"></div>

                    <div class="col-xs-12">
                        <hr>
                        <div class="text-right">
                            <a href="{{url('my/bookings')}}" class="btn btn-warning" ><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                        </div>
                    </div>


            </div>
        </div>
    </section>

    <script>
        $('body').on('click','.option-cancel',function () {
            var option=$(this).data('option');
            $.ajax({
                type:'get',
                url:SP_source() +'booking/cancel/option',
                data:{'booking_id':$(this).data('id'),'option':option},
                success:function (data) {
                   $('#show-option').html(data);
                    $('.option-cancel').attr('disabled', false).addClass('btn-warning');
                    $('#'+option).removeClass('btn-warning');
                    $('#'+option).attr('disabled', true);
                }
            });
        });


        $('body').on('click','.chk_package',function () {
            var package_detail_id=$(this).data('package');

            $.ajax({
                type:'get',
                url:SP_source() +'booking/cancel/package',
                data:{'booking_id':$(this).data('id'),'package_detail_id':package_detail_id},
                success:function (data) {
                    $('#btn_confirm').show();
                    $('#show-message-'+package_detail_id).show();
                    $('#show-message-'+package_detail_id).html(data);
                    $('#show-message').hide();

                }

            });
        });

        $('body').on('click','.chk_person',function () {
            var package_detail_id=$(this).data('package');
            if($(this).val()!='0'){
                $.ajax({
                    type:'get',
                    url:SP_source() +'booking/cancel/person',
                    data:{'booking_id':$(this).data('id'),'package_detail_id':package_detail_id},
                    success:function (data) {
                        $('#btn_confirm').show();
                        $('#show-message-'+package_detail_id).show();
                        $('#show-message-'+package_detail_id).html(data);
                        $('#show-message').hide();
                    }
                });
                $('#checked-option').val(1);
            }else{
                $('#checked-option').val(parseInt($('#checked-option').val()+0));
                $('#show-message-'+package_detail_id).hide();

                if(parseInt($('#checked-option').val())==100){
                    $('#btn_confirm').hide();
                }

            }

        });

        $('body').on('click','#submit_cancel_person',function () {

            var package = new Array();
            var person = new Array();

            $('input[name^="group"]').each(function() {
                var chk_person='chk_person'+$(this).val()+'[]';

                $('input[name="'+chk_person+'"]:checked').each(function(){
                    package.push($(this).data('package'));
                    person.push($(this).val());
                });
            });

            $.ajax({
                type:'get',
                url:SP_source() +'booking/confirm_cancel/person',
                data:{'booking_id':$(this).data('id'),'package':package,'person':person},
                success:function (data) {
                    $('#btn_confirm').hide();
                    $('input[name^="group"]').each(function() {
                        $('#show-message-'+$(this).val()).hide();
                    });
                    $('#show-message').show();
                    $('#show-message').html('<h3>'+data+'</h3>');
                }
            });
        });


        $('body').on('click','#submit_cancel',function () {

            var package = new Array();
            $('input[name="chk_package[]"]:checked').each(function(){
                package.push($(this).val());
            });

            $.ajax({
                type:'get',
                url:SP_source() +'booking/confirm_cancel/package',
                data:{'booking_id':$(this).data('id'),'package':package},
                success:function (data) {
                    $('#btn_confirm').hide();
                    $('#show-message').html(data);

                }

            });
        });





    </script>


@endsection