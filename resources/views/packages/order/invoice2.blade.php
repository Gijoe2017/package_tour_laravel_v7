
@extends('layouts.package.master')

@section('program-highlight')
    {{--<link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;

        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }


    </style>



    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')
            </div>
        </div>
    </section>


        <?php

//               *****  pay all not pay diposit before ****
        $check_pay_all=DB::table('package_invoice')
            ->where('invoice_package_id',$Booking->invoice_package_id)
            ->where('invoice_type','1')
            ->where('invoice_status','2')
            ->first();
       // dd($check_pay_all);

        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Booking->invoice_package_id)
            ->first();
        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();

        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();

        $Status=DB::table('booking_status')->where('booking_status',$Booking->invoice_status)->first();

        $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
        if(!$country){
            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
        }

        $states=DB::table('states')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$states){
            $states=DB::table('states')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('language_code','en')
                ->first();
        }
        $city=DB::table('cities')
            ->where('country_id',$BusinessInfo->country_id)
            ->where('state_id',$BusinessInfo->state_id)
            ->where('city_id',$BusinessInfo->city_id)
            ->where('language_code',Auth::user()->langauge)
            ->first();
        if(!$city){
            $city=DB::table('cities')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('city_id',$BusinessInfo->city_id)
                ->where('language_code','en')
                ->first();
        }

        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();

        $Details=DB::table('package_booking_details as a')
            ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
            ->where('a.booking_id',$Booking->invoice_booking_id)
            ->where('a.timeline_id',$Booking->invoice_timeline_id)
            ->get();

        $Deposit=0;$Totals=0;
        foreach ($Details as $rowD){
            $PackageDetailsOne=DB::table('package_details')
                ->where('packageDescID',$rowD->package_detail_id)
                ->first();

            if($PackageDetailsOne->season=='Y'){
                $order_by="desc";
            }else{
                $order_by="asc";
            }
            $Condition=DB::table('condition_in_package_details as a')
                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                ->where('b.condition_group_id','1')
                ->where('b.formula_id','>',0)
                ->where('a.packageID',$rowD->package_id)
                ->orderby('c.value_deposit',$order_by)
                ->first();

            if($Condition){
                $Deposit+=$Condition->value_deposit*$rowD->number_of_person;
            }

            $Totals+=$rowD->booking_normal_price*$rowD->number_of_person;
        }

        $HomeCon=new \App\Http\Controllers\Package\OrderTourController();
        $rate=$HomeCon->get_rate($Deposit);


        $system_fee=$Totals*$rate/100;

        $InvoiceNo=sprintf('%09d',$Booking->invoice_id);
        ?>

        <!-- Main content -->
        <section class="invoice">
            <div class="container">
                <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            {{trans('common.deposit_invoice')}}
                            <small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-8 invoice-col">
                        @if($media!=null)
                            <img class="logo-invoice" src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                        @else
                            <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                        @endif
                        {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <b>{{trans('common.invoice_no')}}: #{{$InvoiceNo}}</b><br>
                        <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i')}}<br>
                        <b>{{trans('common.order_id')}}:</b> 4F3S8J<br>
                        <b>{{trans('common.reference')}}:</b> ACB11<br>
                    </div>

                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <br>
                    <div class="col-sm-4 invoice-col">
                        <strong>Billing From/จาก:  </strong><br>
                        <address>
                            {{$BusinessInfo1->legal_name}}<br>
                            {{$BusinessInfo1->address}}, {{$city->city}}<br>
                            {{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>
                            {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                            {{trans('emails')}}: {{$BusinessInfo1->email}}
                        </address>
                    </div>

                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <strong>Billing To/ถึง:</strong>
                        @if($AddressBook)
                            <address>
                                {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                {{$AddressBook->entry_street_address}}<br>
                                {{$AddressBook->entry_city}}, {{$AddressBook->country}} {{$AddressBook->entry_postcode}}<br>
                                {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                {{trans('common.email')}}: {{$BusinessInfo1->email}}
                            </address>
                        @else
                        @endif
                    </div>
                    <div class="col-sm-4"><BR>
                        <!-- Conversations are loaded here -->
                        <div class="small-box bg-green">
                            <div class="inner text-center">
                                <h3>ชำระแล้ว</h3>
                                <p>{{trans('common.date')}} : {{date('d/m/Y H:i',strtotime($Booking->payment_date))}}<br></p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-lg-4 col-xs-12">--}}
                        {{--<!-- small box -->--}}
                        {{--<div class="small-box bg-green">--}}
                            {{--<div class="inner text-center">--}}
                                {{--<h4>{{trans('common.list_of_payment_tour_balance_payment')}}</h4>--}}
                                {{--<h3>{{$current->currency_symbol.number_format($Totals-$Deposit+$system_fee)}}</h3>--}}
                                {{--<p>--}}
                                    {{--{{trans('common.payment_due_date')}} : {{date('d/m/Y H:i',strtotime($Booking->invoice_payment_date))}}<br>--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="icon">--}}
                                {{--<i class="fa fa-file-text-o"></i>--}}
                            {{--</div>--}}
                            {{--<a href="#" class="small-box-footer">--}}

                                {{--{{trans('common.status')}}: {{trans('common.'.$Status->status_name)}} <i class="fa fa-arrow-circle-right"></i>--}}

                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>{{trans('common.items')}}</th>
                                <th>{{trans('common.description')}}</th>
                                <th>{{trans('common.unit_price')}}</th>
                                <th>{{trans('common.unit')}}</th>
                                <th>{{trans('common.unit_total')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;$TotalsAll=0;$system_fee=0?>
                            @foreach($Details as $rows)
                                <?php
                                $AdditionalPrice=0;
                                $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();


                                $st=explode('-',$rows->packageDateStart);
                                $end=explode('-',$rows->packageDateEnd);

                                if($st[1]==$end[1]){
                                    $date=\Date::parse($rows->packageDateStart);
                                    $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                    // dd($end[0]);
                                }else{
                                    $date=\Date::parse($rows->packageDateStart);
                                    $date1=\Date::parse($rows->packageDateEnd);
                                    $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                }
                                ?>
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>
                                        <strong>{{trans('common.tour_balance')}} {{$rows->TourType}}: {{trans('common.traveling_date')}} {{$package_date}}</strong><br>
                                        {{$Package->packageName}}
                                    </td>
                                    <td>{{$current->currency_symbol.number_format($rows->booking_normal_price)}}</td>
                                    <td>{{$rows->number_of_person}}</td>
                                    <td class="text-right">
                                        {{$current->currency_symbol.number_format($rows->booking_normal_price*$rows->number_of_person)}}
                                    </td>
                                </tr>

                                @if($Additional)
                                    @foreach($Additional as $rowA)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                            <td>{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                            <td>1</td>
                                            <td class="text-right">{{$current->currency_symbol.number_format($rowA->price_service)}}</td>
                                        </tr>
                                        <?php
                                        $AdditionalPrice+=$rowA->price_service;
                                        ?>
                                    @endforeach
                                @endif

                                <?php
                                $TotalsAll+=$rows->booking_normal_price*$rows->number_of_person;
                                ?>

                            @endforeach

                            <tr>
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.totals')}}{{trans('common.tax')}} 7%</strong></td>
                                <td class="text-right"><strong>{{$current->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}} </strong></td>
                            </tr>
                            @if(!$check_pay_all)
                            <tr>
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.deposit')}}</strong></td>
                                <td class="text-right"><strong>-{{$current->currency_symbol.number_format($Deposit)}} </strong></td>
                            </tr>
                            @endif
                            {{--<tr>--}}
                                {{--<td colspan="4" style="text-align: right"><strong>{{trans('common.system_fee')}}</strong></td>--}}
                                {{--<td><strong>{{$current->currency_symbol.number_format($system_fee)}}</strong></td>--}}
                            {{--</tr>--}}

                            <tr>
                                <td colspan="4" style="text-align: right"><strong>{{trans('common.total_amount')}}:</strong></td>
                                <td class="text-right"><strong>{{$current->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}}</strong> </td>
                            </tr>
                            <tr>
                                @if($check_pay_all)
                                 <td colspan="4" style="text-align: right">
                                        <h4 class="text-green">{{trans('common.pay_all')}}:</h4>
                                 </td>
                                <td class="text-right"><h4 class="text-green">{{$current->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}}</h4></td>
                                @else
                                    <td colspan="4" style="text-align: right">
                                         <h4 class="text-green">{{trans('common.tour_balance')}}:</h4>
                                    </td>
                                    <td class="text-right"><h4 class="text-green">{{$current->currency_symbol.number_format($TotalsAll+$AdditionalPrice-$Deposit)}}</h4></td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <?php
                    $Totals=($TotalsAll+$AdditionalPrice)-$Deposit;
                    ?>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-md-6">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Payment Methods/ช่องท่างชำระเงิน:</h3>


                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages-payment">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">โอนเข้าบัญชี</span>
                                    </div>
                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="{{asset('images/credit/Thailand-KasikornBank.png')}}" alt="Message User Image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            ธนาคารกสิกรไทย (KASIKORN BANK) ชื่อบัญชี: ชุติกาญจน์ ธนจิตรวิไลย
                                            เลขที่บัญชี: 408-623481-7 สาขา : เทสโก้ โลตัส วังหิน
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="{{asset('images/credit/Thailand-Siam-Commercial-Bank.jpg')}}" alt="Message User Image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            ธนาคารไทยพาณิชย์ (SCB) ชื่อบัญชี: ชุติกาญจน์ ธนจิตรวิไลย
                                            เลขที่บัญชี: 408-623481-7 สาขา : เทสโก้ โลตัส วังหิน
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">

                                        <!-- /.direct-chat-info -->
                                        <div class="attachment-pushed">
                                            <span class="direct-chat-name pull-left">Payment Information/ข้อมูลการชำระเงิน</span><br>
                                            <div class="attachment-text">
                                                - ค่าธรรมเนียมในการโอนเงินทั้งหมดเป็นความรับผิดชอบของผู้โอน <br>
                                                - หากยืนยัน กรุณาชำระเงินภายในวันเวลาที่กำหนดข้างต้น มิฉะนั้นระบบจะตัดที่นั่งโดยอัตโนมัติ <br>
                                                <br> <span class="direct-chat-name pull-left">Inform Money Transfer/แจ้งการโอนเงิน:</span><br>
                                                <div class="attachment-text">
                                                    - หลังจากทำการโอนเงินแล้วกรุณาคลิกปุ่ม "คลิกที่นี้เพื่อแจ้งโอนเงิน" ด้านล่างและกรอกรายละเอียดการโอนในแบบฟอร์มการแจ้งโอนเงิน<br>

                                                    @if($Booking->invoice_status==1)
                                                        <br>
                                                        {{--<a href="{{url('/payment/notification/'.$Booking->invoice_id.'/'.$invoice_type)}}" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> คลิกที่นี้เพื่อแจ้งโอนเงิน--}}
                                                        {{--</a>--}}


                                                        <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment')}}">
                                                            {!! csrf_field() !!}
                                                            <script type="text/javascript" src="https://cdn.omise.co/omise.js"
                                                                    data-key="pkey_test_5dtunq6l6nl03ywhud7"
                                                                    data-image="https://toechok.com/setting/logo.jpg"
                                                                    data-frame-label="TOECHOK CO.,LTD."
                                                                    data-button-label="<i class='fa fa-credit-card'></i> Pay Balance"
                                                                    data-submit-label="Submit"
                                                                    data-location="on"
                                                                    data-amount="{{round($Totals)}}00"
                                                                    data-currency="{{strtolower($current->currency_code)}}"
                                                            >
                                                            </script>
                                                            <!--the script will render <input type="hidden" name="omiseToken"> for you automatically-->
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- /.attachment-text -->
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                </div>
                                <!--/.direct-chat-messages-->


                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Remark/หมายเหตุ</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages-payment">
                                    <!-- Message. Default to the left -->
                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">
                                        <!-- /.direct-chat-info -->
                                        <div class="attachment-pushed">
                                            <div class="attachment-text">
                                                - Personal expense such as passport fee, extra foods
                                                and beverage, internet fees and etc are not included. <br>
                                                - Tips for Local guide and driver are not included. <br>
                                                - Visa service fees for aliens are not included.
                                            </div>
                                            <!-- /.attachment-text -->
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                </div>
                                <!--/.direct-chat-messages-->


                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                    <div class="col-xs-12">
                        <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                        <a href="{{url('booking/download/invoice/pdf/'.$Booking->invoice_booking_id.'/'.$Booking->invoice_package_id)}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                            <i class="fa fa-download"></i> Generate PDF
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->

    <script type="text/javascript">
        $(document).ready(function () {
            $('.stepper').mdbStepper();
        });
        $('.delete-detail').on('click',function () {
            if(confirm('Confirm delete this cart detail?')){
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        // alert('test');
                        window.location =  window.location.href;
                    }
                });
            }
        });



        $(document).ready(function(){
            var counter = 1;
            $("#add").click(function () {
                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);

                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');


                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });



        });
    </script>


@endsection