@extends('layouts.package.master')

@section('program-highlight')
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;
        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

    </style>

    <style>
        /* The container */
        .container-cart {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .container-cart input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 18px;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }

        /* On mouse-over, add a grey background color */
        .container-cart:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .container-cart input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the indicator (the dot/circle - hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the indicator (dot/circle) when checked */
        .container-cart input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the indicator (dot/circle) */
        .container-cart .checkmark:after {
            top: 9px;
            left: 9px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
        }

        a :hover{
            color: #9b392f;
        }
.card{
    padding: 15px;
}
.nav-item{
    width: 25%;
}
        .nav > li > a:hover, .nav > li > a:active, .nav > li > a:focus {
            color: #f8f9fa;
            background: #f6b11b;
        }
    </style>


    <?php
    $disabled='disabled="disabled"';$onclick2="";$onclick3="";$onclick4="";
    $onclick2="onclick=\"window.location.href='".url('booking/continuous/step2')."'\"";
    $onclick3="onclick=\"window.location.href='".url('booking/continuous/payment')."'\"";
    $onclick4="onclick=\"window.location.href='".url('booking/continuous/invoice')."'\"";


    ?>


    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="process">
                        <div class="process-row">
                            <div class="process-step">
                                <button type="button" class="btn btn-default btn-circle" onclick="window.location.href='{{url('booking/u/'.Auth::user()->id)}}'"><i class="fa fa-shopping-cart fa-2x"></i></button>
                                <p>{{trans('common.cart')}}</p>
                            </div>
                            <div class="process-step">
                                <button type="button" class="btn btn-default btn-circle" {!! Session::get('step')>=2?$onclick2:$disabled !!}><i class="fa fa-list fa-2x"></i></button>
                                <p>{{trans('common.continuous')}}</p>
                            </div>
                            <div class="process-step">
                                <button type="button" class="btn btn-success btn-circle" {!! Session::get('step')>=3?$onclick3:$disabled !!}><i class="fa fa-credit-card fa-2x"></i></button>
                                <p>{{trans('common.choose_payment')}}</p>
                            </div>
                            <div class="process-step">
                                <button type="button" class="btn btn-default btn-circle" {!! Session::get('step')>=4?$onclick4:$disabled !!}><i class="fa fa-check-square fa-2x"></i></button>
                                <p>{{trans('common.show_invoice')}}</p>
                            </div>
                            <div class="process-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-book fa-2x"></i></button>
                                <p>{{trans('common.complete')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
           </div>
    </section>

    <section class="invoice">
    <div class="container">
        <div class="row">
            <!-- accepted payments column -->

            <?php
            $price_total=0;$Additional_price=0;$discount=0;$total=0;$pricedeposittotal=0;$deposit=0;
            // dd($Carts);
           ?>

            @foreach($Carts as $rowCart)
                <?php
                    $total+=$rowCart->booking_cart_normal_price*$rowCart->number_of_person;
                    $Additional=DB::table('package_booking_cart_additional')
                        ->where('booking_cart_detail_id',$rowCart->booking_cart_detail_id)
                        ->get();
                    foreach ($Additional as $rows){
                        $Additional_price+=$rows->price_service;
                    }

                $Package=DB::table('package_tour as a')
                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
                    ->where('a.packageID',$rowCart->package_id)
                    ->first();
                $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

                $BankInfo=DB::table('business_verified_bank as a')
                    ->join('banks as b','b.bank_code','=','a.bank_id')
                    ->join('countries as c','c.country_id','=','a.country_id')
                    ->where('a.timeline_id',$Package->timeline_id)
                    ->where('b.language_code',Session::get('language'))
                    ->where('c.language_code',Session::get('language'))
                    ->groupby('a.timeline_id')
                    ->groupby('b.bank_code')
                    ->get();

                $PackageDetailsOne=DB::table('package_details')
                    ->where('packageDescID',$rowCart->package_detail_id)
                    ->first();

                if($PackageDetailsOne->season=='Y'){
                    $orderby="desc";
                }else{
                    $orderby="asc";
                }
                $Condition=DB::table('condition_in_package_details as a')
                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
                    ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                    ->where('b.condition_group_id','1')
                    ->where('b.formula_id','>',0)
                    ->where('a.packageID',$rowCart->package_id)
                    ->orderby('c.value_deposit',$orderby)
                    ->first();


                if($Condition){
                    $deposit=$Condition->value_deposit;
                }

                $pricedeposittotal+=$rowCart->number_of_person*$deposit;
                ?>




            @endforeach

            <?php


            $price_total=$total+$Additional_price;

            ?>

            {{--<div class="col-md-9">--}}
                {{--<!-- DIRECT CHAT PRIMARY -->--}}
                {{--<div class="box box-warning direct-chat direct-chat-warning">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">{{trans('common.payment_methods')}}:</h3>--}}
                        {{--<hr>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-header -->--}}
                    {{--<div class="box-body">--}}
                        {{--<!-- Conversations are loaded here -->--}}
                        {{--<div class="direct-chat-messages-payment">--}}
                            {{--<div class="direct-chat-info clearfix">--}}
                                {{--<span class="direct-chat-name pull-left">{{trans('common.transfer')}}</span>--}}
                            {{--</div>--}}
                            {{--<!-- Message. Default to the left -->--}}
                            {{--@foreach($BankInfo as $rows)--}}
                            {{--<div class="direct-chat-msg">--}}
                                {{--<label class="container-cart">--}}
                                    {{--<input type="radio" checked="checked" name="bank">--}}
                                    {{--<span class="checkmark"></span>--}}
                                {{--</label>--}}
                                {{--<!-- /.direct-chat-info -->--}}
                                {{--<img class="direct-chat-img" src="{{asset('images/credit/'.$rows->logo)}}" alt="{{$rows->bank_name}}"><!-- /.direct-chat-img -->--}}
                                {{--<div class="direct-chat-text">--}}
                                    {{--{{$rows->bank_name}}({{$rows->country}}) {{trans('common.account_name')}}: {{$rows->account_name}}--}}
                                    {{--{{trans('common.account_number')}}: {{$rows->bank_account_number}} {{trans('common.branch')}}: {{$rows->sub_bank}}--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-text -->--}}
                            {{--</div>--}}
                            {{--@endforeach--}}
                            {{--<!-- /.direct-chat-msg -->--}}
                            {{--<!-- Message to the right -->--}}
                            {{--<div class="direct-chat-msg right">--}}
                                {{--<!-- /.direct-chat-info -->--}}
                                {{--<div class="attachment-pushed">--}}
                                    {{--<span class="direct-chat-name pull-left">Payment Information/ข้อมูลการชำระเงิน</span><br>--}}
                                    {{--<div class="attachment-text">--}}
                                        {{--- ค่าธรรมเนียมในการโอนเงินทั้งหมดเป็นความรับผิดชอบของผู้โอน <br>--}}
                                        {{--- หากยืนยัน กรุณาชำระเงินภายในวันเวลาที่กำหนดข้างต้น มิฉะนั้นระบบจะตัดที่นั่งโดยอัตโนมัติ <br>--}}
                                        {{--<br> <span class="direct-chat-name pull-left">Inform Money Transfer/แจ้งการโอนเงิน:</span><br>--}}
                                        {{--<div class="attachment-text">--}}
                                        {{--- หลังจากทำการโอนเงินแล้วกรุณาคลิกปุ่ม "คลิกที่นี้เพื่อแจ้งโอนเงิน" ด้านล่างและกรอกรายละเอียดการโอนในแบบฟอร์มการแจ้งโอนเงิน<br>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- /.attachment-text -->--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-text -->--}}
                            {{--</div>--}}
                            {{--<!-- /.direct-chat-msg -->--}}
                        {{--</div>--}}
                        {{--<!--/.direct-chat-messages-->--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                    {{--<!-- /.box-footer-->--}}
                {{--</div>--}}
                {{--<!--/.direct-chat -->--}}
            {{--</div>--}}
            {{----}}


            <div class="col-lg-9">

                <!-- Classic tabs -->
                    <div class="classic-tabs mx-2">

                    <ul class="nav tabs-orange" id="myClassicTabOrange" role="tablist">
                        <li class="nav-item active">
                            <a data-id="1" class="nav-link text-center active show" id="profile-tab-classic-orange" data-toggle="tab" href="#profile-classic-orange"
                               role="tab" aria-controls="profile-classic-orange" aria-selected="true" aria-expanded="true">
                                <i class="fa fa-credit-card fa-2x pb-2" aria-hidden="true"></i><br>Credit/Debit Card</a>
                        </li>
                        <li class="nav-item">
                            <a data-id="2" class="nav-link waves-light text-center" id="follow-tab-classic-orange" data-toggle="tab" href="#follow-classic-orange"
                               role="tab" aria-controls="follow-classic-orange" aria-selected="false">

                                <i class="fa fa-tablet fa-2x pb-2" aria-hidden="true"></i><br>Mobile Banking</a>
                        </li>
                        <li class="nav-item">
                            <a data-id="3" class="nav-link waves-light text-center" id="contact-tab-classic-orange" data-toggle="tab" href="#contact-classic-orange"
                               role="tab" aria-controls="contact-classic-orange" aria-selected="false">
                                <i class="fa fa-university fa-2x pb-2" aria-hidden="true"></i><br>Bank Transfer</a>
                        </li>
                        <li class="nav-item">

                            <a data-id="4" class="nav-link waves-light text-center" id="awesome-tab-classic-orange" data-toggle="tab" href="#awesome-classic-orange"
                               role="tab" aria-controls="awesome-classic-orange" aria-selected="false">
                                <i class="fa fa-desktop fa-2x pb-2" aria-hidden="true"></i><br>Other</a>
                        </li>
                    </ul>

                    <div class="tab-content card" id="myClassicTabContentOrange">
                        <div class="tab-pane fade active show" id="profile-classic-orange" role="tabpanel" aria-labelledby="profile-tab-classic-orange">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="creditCard" required="" class="next-col-16 next-form-item-label">หมายเลขบัตร</label>
                                    <input type="text" class="form-control" placeholder="หมายเลขบัตร">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="creditCard" required="" class="next-col-16 next-form-item-label">ชื่อผู้ถือบัตร</label>
                                    <input type="text" class="form-control" placeholder="ชื่อผู้ถือบัตร">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="creditCard" required="" class="next-col-16 next-form-item-label">วันหมดอายุ</label>
                                    <input type="text" class="form-control" placeholder="วันหมดอายุ">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="creditCard" required="" class="next-col-16 next-form-item-label">CVV</label>
                                    <input type="text" class="form-control" placeholder="CVV">
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <a href="{{url('booking/continuous/invoice')}}" class="btn btn-success btn-block btn-lg">
                                        <i class="fa fa-check"></i> {{trans('common.confirm_order')}}
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="follow-classic-orange" role="tabpanel" aria-labelledby="follow-tab-classic-orange">

                            <?php
                            $Banks=DB::table('banks')->where('language_code',Session::get('language'))->limit(6)->get();
                            ?>

                            @foreach($Banks as $rows)
                                <div class="col-md-4">
                                    <div  class="alert alert-default">
                                        <img src="{{asset('images/credit/'.$rows->logo)}}">
                                        {{$rows->bank_name}}

                                    </div>
                                </div>
                             @endforeach

                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <a href="{{url('booking/continuous/invoice')}}" class="btn btn-success btn-block btn-lg">
                                            <i class="fa fa-check"></i> {{trans('common.confirm_order')}}
                                        </a>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane fade" id="contact-classic-orange" role="tabpanel" aria-labelledby="contact-tab-classic-orange">
                                    @foreach($BankInfo as $rows)
                                        <div class="col-md-8">
                                        <div class="direct-chat-msg">
                                            <!-- /.direct-chat-info -->
                                            <img class="direct-chat-img" src="{{asset('images/credit/'.$rows->logo)}}" alt="{{$rows->bank_name}}"><!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                {{$rows->bank_name}}({{$rows->country}}) {{trans('common.account_name')}}: {{$rows->account_name}} <BR>
                                                {{trans('common.account_number')}}: {{$rows->bank_account_number}} {{trans('common.branch')}}: {{$rows->sub_bank}}
                                            </div>
                                            <!-- /.direct-chat-text -->
                                        </div>
                                        </div>
                                    @endforeach
                                        <div class="col-md-12">
                                            <hr>
                                        <div class="direct-chat-msg right">
                                            <!-- /.direct-chat-info -->
                                            <div class="attachment-pushed">
                                                <span class="direct-chat-name pull-left">Payment Information/ข้อมูลการชำระเงิน</span><br>
                                                <div class="attachment-text">
                                                    - ค่าธรรมเนียมในการโอนเงินทั้งหมดเป็นความรับผิดชอบของผู้โอน <br>
                                                    - หากยืนยัน กรุณาชำระเงินภายในวันเวลาที่กำหนดข้างต้น มิฉะนั้นระบบจะตัดที่นั่งโดยอัตโนมัติ <br>
                                                    <br> <span class="direct-chat-name pull-left">Inform Money Transfer/แจ้งการโอนเงิน:</span><br>
                                                    <div class="attachment-text">
                                                        - หลังจากทำการโอนเงินแล้วกรุณาคลิกปุ่ม "คลิกที่นี้เพื่อแจ้งโอนเงิน" ด้านล่างและกรอกรายละเอียดการโอนในแบบฟอร์มการแจ้งโอนเงิน<br>
                                                    </div>
                                                </div>
                                                <!-- /.attachment-text -->
                                            </div>
                                            <!-- /.direct-chat-text -->
                                        </div>
                                        </div>


                                        <div class="col-md-12 text-right">
                                            <div class="form-group">

                                                <a href="{{url('booking/continuous/invoice')}}" class="btn btn-success btn-block btn-lg">
                                                    <i class="fa fa-check"></i> {{trans('common.confirm_order')}}
                                                </a>

                                            </div>
                                        </div>

                                </div>
                                <div class="tab-pane fade" id="awesome-classic-orange" role="tabpanel" aria-labelledby="awesome-tab-classic-orange">
                                    <h2> Other </h2>
                                </div>
                        </div>

                    </div>
                    <!-- Classic tabs -->

            </div> <!-- End lg-9 -->

            <div class="col-lg-3">
                <div class="box box-success direct-chat direct-chat-warning">

                <dl class="dlist-align">
                    <dt>{{trans('common.total_price')}}: </dt>
                    <dd class="text-right">{{$current->currency_symbol}} {{number_format($price_total)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>{{trans('common.discount')}}:</dt>
                    <dd class="text-right">{{$current->currency_symbol}} {{number_format($discount)}}</dd>
                </dl>
                <dl class="dlist-align h4">
                    <dt>{{trans('common.totals')}}:</dt>
                    <dd class="text-right"><strong>{{$current->currency_symbol}} {{number_format($price_total)}}</strong></dd>
                </dl>
<hr>
                    <div class="row">
                        <div class="col-md-7">{{trans('common.deposit_payment')}}</div>
                        <div class="col-md-5 text-right h5"><strong class="text-warning">{{$current->currency_symbol}} {{number_format($pricedeposittotal)}}</strong>
                           </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-7">{{trans('common.remaining_balance')}}</div>
                        <div class="col-md-5 text-right h5"><strong class="text-danger">{{$current->currency_symbol}} {{number_format($price_total-$pricedeposittotal)}}</strong>
                        </div>
                    </div>

                <hr>
                {{--<figure class="itemside mb-3">--}}
                    {{--<aside class="aside"><img src="{{asset('package/images/icons/pay-visa.png')}}"></aside>--}}
                    {{--<div class="text-wrap small text-muted">--}}
                        {{--Pay 84.78 AED ( Save 14.97 AED )--}}
                        {{--By using ADCB Cards--}}
                    {{--</div>--}}
                {{--</figure>--}}
                <figure class="itemside mb-3">
                    {{--<aside class="aside"> <img src="{{asset('package/images/icons/pay-mastercard.png')}}"> </aside>--}}
                    {{--<div class="text-wrap small text-muted">--}}
                        {{--Pay by MasterCard and Save 40%. <br>--}}
                        {{--Lorem ipsum dolor--}}
                    {{--</div>--}}
                </figure>

                </div>

            </div>  <!-- End lg-3/.col -->
        </div>  <!-- end row -->

<div class="row">
    <div class="col-md-12">
        <!-- DIRECT CHAT PRIMARY -->
        <div class="box box-warning direct-chat direct-chat-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Remark/หมายเหตุ</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages-payment">
                    <!-- Message. Default to the left -->
                    <!-- Message to the right -->
                    <div class="direct-chat-msg right">
                        <!-- /.direct-chat-info -->
                        <div class="attachment-pushed">
                            <div class="attachment-text">
                                - Personal expense such as passport fee, extra foods
                                and beverage, internet fees and etc are not included. <br>
                                - Tips for Local guide and driver are not included. <br>
                                - Visa service fees for aliens are not included.
                            </div>
                            <!-- /.attachment-text -->
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->
                </div>
                <!--/.direct-chat-messages-->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer-->
        </div>
    </div>

</div>

 </div>
    </section>

    <script>
        $('body').on('click','.nav-link',function () {

            $.ajax({
                type:'get',
                url:SP_source() +'ajax/set/payment',
                data:{'option':$(this).data('id')},
                success:function (data) {

                }
            });
        });
    </script>

@endsection