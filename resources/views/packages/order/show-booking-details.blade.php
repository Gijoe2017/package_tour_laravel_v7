

@extends('layouts.member.layout_master_new')

@section('header')
    <section class="content-header">
        <h1>Order List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
        {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <style>
        .status{
            width: 40%;
            float: right;
            text-align: center;
            font-size: 18px;
            padding: 8px;
            color:floralwhite;
            background-color: #f5cd5e;
            border: 1px solid #e3e3e3;
        }
        .status_paid{
            width: 40%;
            float: right;
            text-align: center;
            font-size: 18px;
            padding: 8px;
            color:floralwhite;
            background-color: #0dbc1d;
            border: 1px solid #e3e3e3;
        }
    </style>
        <?php

        $Deposit=0;
        ?>
        <!-- Main content -->
        <section class="invoice">
            <div class="container">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {{trans('common.booking_detail').' | '.trans('common.order_id').':#'.$Invoices[0]->invoice_booking_id}}
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->

            @foreach($Invoices as $Invoice)

            <div class="row invoice-info">
                <div class="col-md-6">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                            <?php
                            $tax=0; $number_of_person=0;
                            $Package=DB::table('package_tour as a')
                                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                                ->where('a.packageID',$Invoice->invoice_package_id)
                                ->first();
                            //dd($Invoice);

                            $Details=DB::table('package_booking_details')
                                ->where('booking_id',$Invoice->invoice_booking_id)
                                ->where('package_id',$Invoice->invoice_package_id)
                                ->where('package_detail_id',$Invoice->invoice_package_detail_id)
                                ->orderby('tour_type','asc')
                                ->get();

                            $currency_code=$Invoice->currency_code;
                            $currency_symbol=$Invoice->currency_symbol;

                            $Payment=DB::table('payment_notification as a')
                                ->join('payment_notification_sub as b','b.payment_id','=','a.payment_id')
                                ->select('b.status as invoice_status','b.payment_invoice_id','b.payment_amount','a.*')
                                ->where('a.invoice_booking_id',$Invoice->invoice_booking_id)
                                ->where('b.payment_invoice_id',$Invoice->invoice_id)
                                ->first();
                          //  dd($Payment);

                        $PackageDetailsOne=DB::table('package_details')
                            ->where('packageDescID',$Details[0]->package_detail_id)
                            ->first();

                        if($PackageDetailsOne->season=='Y'){
                            $order_by="desc";
                        }else{
                            $order_by="asc";
                        }
                        $Condition=DB::table('condition_in_package_details as a')
                            ->join('package_condition as b','b.condition_code','=','a.condition_id')
                            ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                            ->where('b.condition_group_id','1')
                            ->where('b.formula_id','>',0)
                            ->where('a.packageID',$Details[0]->package_id)
                            ->orderby('c.value_deposit',$order_by)
                            ->first();

                            if($Condition){
                                $Deposit+=$Condition->value_deposit*$Details[0]->number_of_person;
                            }
                        ?>

                        <div class="box-body box-profile">
                            @if(($Invoice->invoice_type=='1' && $Invoice->invoice_status=='2') || ($Invoice->invoice_type=='2' && $Invoice->invoice_status=='4'))
                                <div class="status_paid text-muted well no-shadow">
                                    <strong><i class="glyphicon glyphicon-saved"></i> {{trans('common.status').trans('common.paid')}}</strong>
                                </div>
                            @else
                                <div class="status text-muted well  no-shadow">
                                    <strong>{{trans('common.status'). trans('common.pending_payment')}}</strong>
                                </div>
                            @endif

                            <b>{{trans('common.invoice_no')}}: #{{$Invoice->invoice_id}}</b><br>
                            <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($Invoice->invoice_date))}}<br>
                            {{--<b>{{trans('common.order_id')}}:</b> {{$OrderID}}<br>--}}
                            <b>{{trans('common.order_id')}}:</b> {{':#'.$Invoices[0]->invoice_booking_id}}<br>
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    @foreach($Details as $Detail)
                                            <?php $number_of_person+=$Detail->number_of_person?>
                                            <span class="pull-right text-top"><strong>{{$Detail->number_of_person.' '.trans('common.person')}}</strong></span>
                                            {!! $Detail->package_detail_title !!} </BR>
                                    @endforeach
                                </p>
                            @if($Invoice->invoice_type=='2')
                                <?php
                                    if($Condition){
                                        $value_deposit=$Condition->value_deposit;
                                    }
                                    ?>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>{{trans('common.number_of_tourist')}}</b> <strong class="pull-right">{{$number_of_person}} {{trans('common.person')}}</strong>
                                    </li>

                                    <li class="list-group-item">
                                        <b>{{trans('common.totals')}}
                                            @if($Detail->price_include_vat!='Y')
                                                + {{trans('common.include_tax')}} 7%
                                            @endif
                                        </b>
                                        <strong class="pull-right">{{$currency_symbol.number_format($Invoice->invoice_amount)}}</strong>
                                    </li>
                                </ul>
                            @else
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>{{trans('common.number_of_tourist')}}</b> <strong class="pull-right">{{$number_of_person}} {{trans('common.person')}}</strong>
                                    </li>

                                    {{--<li class="list-group-item">--}}
                                        {{--<b>{{trans('common.subtotal')}}</b> <strong class="pull-right">{{$currency_symbol.number_format($Condition->value_deposit*$number_of_person)}}</strong>--}}
                                    {{--</li>--}}

                                    <li class="list-group-item">
                                        <b>{{trans('common.totals')}}</b> <strong class="pull-right">{{$currency_symbol.number_format($Invoice->invoice_amount)}}</strong>
                                    </li>
                                </ul>
                            @endif

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-body box-profile">
                                @if($Payment)
                                    <b>{{trans('common.payment_detail')}}</b>
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        {{trans('common.name_company_for_receipt_tax_invoice')}}{!!  $Payment->tax_business_name !!}<BR>
                                        {{trans('common.id_card_number').' '.$Payment->id_card_number}}<BR>
                                        {{trans('common.phone').' '.$Payment->transfer_phone}}<BR>
                                        {{trans('common.emails').' '.$Payment->tax_email}}
                                    </p>

                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        {!!  trans('common.transfer_amount').' <strong>'.$currency_symbol.number_format($Payment->payment_amount).'</strong>'!!}<BR>
                                        {!!  trans('common.date_time').' <strong>'.$Payment->transfer_date.' '.$Payment->transfer_time.'</strong>'!!}<BR>
                                        {!!  trans('common.transfer_bank').' <strong>'.$Payment->bank_transfer.'</strong> '.trans('common.sub_bank').' <strong>'.$Payment->sub_bank.'</strong>'!!}<BR>
                                    </p>

                                    @if($Payment->transfer_remark)
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        {{trans('common.remark').': '.$Payment->transfer_remark}}
                                    </p>
                                    @endif
                                    <a href="{{url('images/member/slip/'.$Payment->transfer_slip)}}" target="_blank"><i class="glyphicon glyphicon-picture"></i> {{trans('common.slip')}}</a>
                                    @if($Payment->invoice_status=='s')
                                    <BR><hr>

                                    <div class="form-group text-center">
                                        <a href="{{url('booking/backend/update/invoice/'.$Invoice->invoice_id.'/'.$Invoice->invoice_booking_id)}}" class="btn btn-success btn-lg"><i class="glyphicon glyphicon-ok"></i> {{trans('common.confirm_payment')}}</a>
                                    </div>
                                    @endif
                                @else
                                    @if($Invoice->invoice_status=='2' || $Invoice->invoice_status=='4' )
                                    <h2 class="text-green"><i class="fa fa-check"></i> {{trans('common.pay_via_payment_gateway')}}</h2>
                                    @else
                                    <h2>{{trans('common.pending_payment')}}</h2>
                                        @endif
                                @endif

                        </div>
                    </div>
                </div>

            </div>
                @endforeach
            </div>

        </section>
        <!-- /.content -->




@endsection