@extends('layouts.member.layout_booking')

@section('content')


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Invoice
                <small>#007612</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Invoice</li>
            </ol>
        </section>

        <div class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-info"></i> Note:</h4>
                This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
            </div>
        </div>

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        Full Invoice/ใบแจ้งชำระเงินค่าทัวร์ทั้งหมด
                        <small class="pull-right">Date/วันที่: 2/10/2018</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <div class="row invoice-info">
                <div class="col-sm-8 invoice-col">
                    <img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">
                </div>
                <div class="col-sm-4 invoice-col">
                    <b>Invoice/ใบแจ้งชำระเงิน: #007612</b><br>
                    <b>Billing Date/Time/วันออกใบแจ้งชำระเงิน:</b> 2/22/2014-12:00<br>
                    <b>Order ID/รหัสสั่งซื้อ:</b> 4F3S8J<br>
                    <b>Reference:</b> ACB11<br>
                </div>



            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <br>
                <div class="col-sm-4 invoice-col">
                    <strong>Billing From/จาก:  </strong><br>
                    <address>
                        บริษัท ABC จำกัด<br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone/เบอร์โทรศัพท์: (804) 123-5432<br>
                        Email/อีเมล: info@almasaeedstudio.com
                    </address>
                </div>



                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>Billing To/ถึง:</strong>
                    <address>
                        Narong Mungtom<br>
                        795 Folsom Ave, Suite 600<br>
                        Chonburi, Thailand 20260<br>
                        Phone/เบอร์โทรศัพท์: (555) 539-1037<br>
                        Email/อีเมล: john.doe@example.com
                    </address>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h4>รายการแจ้งชำระเงินค่าทัวร์ทั้งหมด</h4>
                            <h3>฿41,600</h3>

                            <p>

                                Payment Due Date/กำหนดชำระภายในวันที่: 2/22/2018-18:00<br>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            สถานะ: รอชำระเงิน <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Item/รายการ</th>
                            <th>Description/รายละเอียด</th>
                            <th>Unit Price/ราคาต่อหน่วย</th>
                            <th>Unit/จำนวน</th>
                            <th>Unit Total/รวมต่อหน่วย</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td><strong>ผู้ใหญ่: เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿10,000</td>
                            <td>2</td>
                            <td>฿20,000</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><strong>เด็ก: เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿8,000</td>
                            <td>1</td>
                            <td>฿8,000</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td><strong>ผู้พิการ: เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿8,000</td>
                            <td>1</td>
                            <td>฿8,000</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td><strong>เด็ก(เสริมเตียง): เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿2,000</td>
                            <td>2</td>
                            <td>฿4,000</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td><strong>เด็ก(ไม่เสริมเตียง): เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿2,000</td>
                            <td>2</td>
                            <td>฿4,000</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td><strong>ทารก: เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿2,000</td>
                            <td>2</td>
                            <td>฿4,000</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td><strong>พักเดี่ยว: เดินทาง 10 - 15 ธันวาคม 2561</strong><br>
                                โปรแกรมทัวร์ สักการะ “พิฆเนศวรเทวา อัฎฐวินายักยาตรา”
                                ประสิทธิ์ประสาทมนตราแห่งความสำเร็จอันยิ่งใหญ่
                                ระยะเวลาเดินทาง 5 วัน 3 คืน</td>
                            <td>฿2,000</td>
                            <td>2</td>
                            <td>฿4,000</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right">Subtotal/รวมย่อย (included tax 7%/รวมภาษีแล้ว 7%)</td>
                            <td>	฿40,000 </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right">Tax/ภาษี</td>
                            <td>	฿1,600</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right">Shipping/ข่นส่ง:</td>
                            <td>฿0</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right">Total Amount/รวมทั้งหมด:</td>
                            <td>฿41,600</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-md-6">
                    <!-- DIRECT CHAT PRIMARY -->
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Payment Methods/ช่องท่างชำระเงิน:</h3>


                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages-payment">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">โอนเข้าบัญชี</span>
                                </div>
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../../dist/img/credit/Thailand-KasikornBank.png" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        ธนาคารกสิกรไทย (KASIKORN BANK) ชื่อบัญชี: ชุติกาญจน์ ธนจิตรวิไลย
                                        เลขที่บัญชี: 408-623481-7 สาขา : เทสโก้ โลตัส วังหิน
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->

                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../../dist/img/credit/Thailand-Siam-Commercial-Bank.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        ธนาคารไทยพาณิชย์ (SCB) ชื่อบัญชี: ชุติกาญจน์ ธนจิตรวิไลย
                                        เลขที่บัญชี: 408-623481-7 สาขา : เทสโก้ โลตัส วังหิน
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">

                                    <!-- /.direct-chat-info -->
                                    <div class="attachment-pushed">
                                        <span class="direct-chat-name pull-left">Payment Information/ข้อมูลการชำระเงิน</span><br>
                                        <div class="attachment-text">
                                            - ค่าธรรมเนียมในการโอนเงินทั้งหมดเป็นความรับผิดชอบของผู้โอน <br>
                                            - หากยืนยัน กรุณาชำระเงินภายในวันเวลาที่กำหนดข้างต้น มิฉะนั้นระบบจะตัดที่นั่งโดยอัตโนมัติ <br>
                                            <br> <span class="direct-chat-name pull-left">Inform Money Transfer/แจ้งการโอนเงิน:</span><br>
                                            <div class="attachment-text">
                                                - หลังจากทำการโอนเงินแล้วกรุณาคลิกปุ่ม "คลิกที่นี้เพื่อแจ้งโอนเงิน" ด้านล่างและกรอกรายละเอียดการโอนในแบบฟอร์มการแจ้งโอนเงิน<br>

                                                <br><button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> คลิกที่นี้เพื่อแจ้งโอนเงิน
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.attachment-text -->
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            </div>
                            <!--/.direct-chat-messages-->


                        </div>
                        <!-- /.box-body -->
                        <!-- /.box-footer-->
                    </div>
                    <!--/.direct-chat -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <!-- DIRECT CHAT PRIMARY -->
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Remark/หมายเหตุ</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages-payment">
                                <!-- Message. Default to the left -->
                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <!-- /.direct-chat-info -->
                                    <div class="attachment-pushed">
                                        <div class="attachment-text">
                                            - Personal expense such as passport fee, extra foods
                                            and beverage, internet fees and etc are not included. <br>
                                            - Tips for Local guide and driver are not included. <br>
                                            - Visa service fees for aliens are not included.
                                        </div>
                                        <!-- /.attachment-text -->
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            </div>
                            <!--/.direct-chat-messages-->


                        </div>
                        <!-- /.box-body -->
                        <!-- /.box-footer-->
                    </div>
                    <!--/.direct-chat -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                    </button>
                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </button>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
    </div>
@endsection