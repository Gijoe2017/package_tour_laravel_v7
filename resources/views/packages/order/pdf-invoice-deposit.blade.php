<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    table {
        font-family: "THSarabunNew";
        border-collapse: collapse;

    }

    th, td {
        border-bottom: 1px solid #ddd;
        padding: 5px;

    }
    .page-break {
        page-break-after: always;
    }

</style>
<?php $page=0;?>
  
        <?php

        $page++;
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$Invoice->invoice_package_id)
            ->first();

        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();
       // dd($media);
        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();
        // dd($BusinessInfo1);
//        $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
//        if(!$country){
//            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
//        }
//
//        $states=DB::table('states')
//            ->where('country_id',$BusinessInfo->country_id)
//            ->where('state_id',$BusinessInfo->state_id)
//            ->where('language_code',Auth::user()->langauge)
//            ->first();
//        if(!$states){
//            $states=DB::table('states')
//                ->where('country_id',$BusinessInfo->country_id)
//                ->where('state_id',$BusinessInfo->state_id)
//                ->where('language_code','en')
//                ->first();
//        }
//        $city=DB::table('cities')
//            ->where('country_id',$BusinessInfo->country_id)
//            ->where('state_id',$BusinessInfo->state_id)
//            ->where('city_id',$BusinessInfo->city_id)
//            ->where('language_code',Auth::user()->langauge)
//            ->first();
//        if(!$city){
//            $city=DB::table('cities')
//                ->where('country_id',$BusinessInfo->country_id)
//                ->where('state_id',$BusinessInfo->state_id)
//                ->where('city_id',$BusinessInfo->city_id)
//                ->where('language_code','en')
//                ->first();
//        }

        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();



        ?>
        <table class="table" width="100%">
            <tr>
                <td colspan="3" >
                    <h2 class="page-header">
                        @if($Invoice->invoice_type=='1')
                            {{trans('common.invoice_deposit')}}
                        @else
                            {{trans('common.invoice_balance')}}
                        @endif
                        {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y',strtotime($Invoice->invoice_date))}}</small>--}}
                    </h2>
                </td>
            </tr>
            <tr>
                <td colspan="3"  >
                       <table class="table" width="100%">
                            <tr>
                                <td width="60%">
                                    @if($media!=null)
                                        <img class="logo-invoice" style="height: 100%" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                    @else
                                        <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                    @endif
                                </td>
                                <td>
                                    <b>{{trans('common.invoice_no')}}: #{{$Invoice->invoice_id}}</b><br>
                                    <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i')}}<br>
                                    <b>{{trans('common.order_id')}}:</b> #{{$Invoice->invoice_booking_id}}<br>

                                    <?php
                                    $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                                    ?>
                                   <strong> {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}} <i class="fa fa-arrow-circle-right"></i> </strong>
                                </td>
                            </tr>

                        </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    <table class="table" width="100%">
                        <tr>
                            <td width="50%">
                                <strong>Billing From/จาก:  </strong><br>
                                <address>
                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}<br>

                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                                </address>
                            </td>
                            <td >
                                <strong>Billing To/ถึง:</strong>
                                @if($AddressBook)
                                    <address>
                                        {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                        {{$AddressBook->address_show}}<br>

                                        {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                        {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                                    </address>
                                @endif
                            </td>
                        </tr></table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>{{trans('common.items')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th align="right">{{trans('common.unit_price')}}</th>
                            <th align="right">{{trans('common.unit')}}</th>
                            <th align="right">{{trans('common.unit_total')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i=1;$TotalsAll=0;$Tax=0; $Deposit=0;

                        $Details=DB::table('package_booking_details as a')
//                            ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//                            ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                            ->where('a.booking_id',$Invoice->invoice_booking_id)
                            ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                            ->where('a.package_detail_id',$Invoice->invoice_package_detail_id)
                            ->get();
                        //dd($Details);
                        foreach ($Details as $Detail){

                       // dd($Detail);
                        /*////////////////////  for Deposit /////////////////////////*/
//                                $PackageDetailsOne=DB::table('package_details')
//                                    ->where('packageDescID',$Detail->package_detail_id)
//                                    ->first();
//
//                                if($PackageDetailsOne->season=='Y'){
//                                    $order_by="desc";
//                                }else{
//                                    $order_by="asc";
//                                }
//                                $Condition=DB::table('condition_in_package_details as a')
//                                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
//                                    ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
//                                    ->where('b.condition_group_id','1')
//                                    ->where('b.formula_id','>',0)
//                                    ->where('a.packageID',$Detail->package_id)
//                                    ->orderby('c.value_deposit',$order_by)
//                                    ->first();
//
//                                if($Condition){
//                                    $Deposit_title=$Condition->value_deposit;
//                                    $Deposit+=$Condition->value_deposit*$Detail->number_of_person;
//                                }

                                $Deposit+=$Detail->deposit_price*$Detail->number_of_person;

                              //  dd($Deposit_title);
                        /////////////////////  for Deposit //////////////////////////

//                            $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();
//
//                        $Promotion=DB::table('package_booking_promotion')
//                            ->where('booking_detail_id',$Detail->booking_detail_id)
//                            ->first();
//                        $promotion_title='';
//                        if($Promotion){
//                            $promotion_title=$Promotion->promotion_title;
//                            if($Promotion->promotion_operator=='Between'){
//                                if($Promotion->promotion_unit=='%'){
//                                    $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
//                                }else{
//                                    $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
//                                }
//                            }else{
//                                if($Promotion->promotion_operator2=='Up'){
//                                    if($Promotion->promotion_unit=='%'){
//                                        $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
//                                    }else{
//                                        $pay_more=$Promotion->promotion_value;
//                                    }
//                                }else{
//                                    if($Promotion->promotion_unit=='%'){
//                                        $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
//                                    }else{
//                                        $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
//                                    }
//                                }
//                            }
//                        }
//                        $Price_sub=0;
                            ?>

                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <strong>{!! $Detail->package_detail_title !!}</strong>
                                </td>

                                <td align="right">{{$Invoice->currency_symbol.number_format($Detail->deposit_price)}}</td>
                                <td align="right">{{$Detail->number_of_person}}</td>
                                <td align="right">
                                    {{$Invoice->currency_symbol.number_format($Detail->deposit_price*$Detail->number_of_person)}}
                                </td>
                            </tr>


                            <?php

                            $TotalsAll+=$Detail->deposit_price*$Detail->number_of_person;


                            } //// end foreach Invoice
                            ?>



                        <tr>
                            <td colspan="4" style="text-align: right">
                                <strong>{{trans('common.subtotal')}} </strong></td>
                            <td align="right"> <strong>{{$Invoice->currency_symbol.number_format($TotalsAll)}}</strong> </td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td colspan="4" style="text-align: right"> <strong>{{trans('common.tax')}}</strong></td>--}}
                            {{--<td align="right"> <strong>{{$Invoice->currency_symbol.number_format($Tax)}}</strong></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                        {{--<td colspan="4" style="text-align: right">{{trans('common.shipping')}}:</td>--}}
                        {{--<td>฿0</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td colspan="4" style="text-align: right"> <strong>{{trans('common.total_amount')}}:</strong></td>
                            <td align="right"> <strong>{{$Invoice->currency_symbol.number_format($TotalsAll)}}</strong> </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>


  

