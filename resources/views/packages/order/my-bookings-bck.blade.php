@extends('layouts.package.master')
@section('program-highlight')
    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script type="text/javascript">
        // Set default parameters
        OmiseCard.configure({
            publicKey: 'pkey_test_5dtunq6l6nl03ywhud7',
            image: 'https://toechok.com/setting/logo.jpg',
            frameLabel: 'TOECHOK CO.,LTD.',
        });
   </script>

    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    @if($Bookings->count())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>{{trans('common.booking_id')}}</th>
                            <th>{{trans('common.booking_date')}}</th>
                            <th>{{trans('common.status')}}</th>
                            <th style="width: 45%">{{trans('common.payment')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $TotalsAll=0;$AdditionalPrice=0;
                        ?>
                        @foreach($Bookings as $rows)
                            <?php
                               //     dd($rows);
                                $Invoice_deposit=DB::table('package_invoice')
                                    ->where('invoice_booking_id',$rows->booking_id)
                                    ->where('invoice_type','1')
                                    ->first();

                                $Invoice_balance=DB::table('package_invoice')
                                ->where('invoice_booking_id',$rows->booking_id)
                                ->where('invoice_type','2')
                                ->first();
                               // echo $rows->booking_id;
                              // dd($Invoice);
                                $Details=DB::table('package_booking_details as a')
                                    ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                                    ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                                    ->where('a.booking_id',$rows->booking_id)
                                    ->get();

                                $Deposit=0;$Totals=0;
                            foreach($Details as $rowD){
//                                $Package=DB::table('package_tour as a')
//                                    ->join('package_tour_info as b','b.packageID','=','a.packageID')
//                                    ->where('a.packageID',$rowD->package_id)
//                                    ->first();
//                                $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

                                $PackageDetailsOne=DB::table('package_details')
                                    ->where('packageDescID',$rowD->package_detail_id)
                                    ->first();

                                if($PackageDetailsOne->season=='Y'){
                                    $order_by="desc";
                                }else{
                                    $order_by="asc";
                                }
                                $Condition=DB::table('condition_in_package_details as a')
                                    ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                    ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                    ->where('b.condition_group_id','1')
                                    ->where('b.formula_id','>',0)
                                    ->where('a.packageID',$rowD->package_id)
                                    ->orderby('c.value_deposit',$order_by)
                                    ->first();

                                if($Condition){
                                    $Deposit+=$Condition->value_deposit*$rowD->number_of_person;
                                }
                                $TotalsAll+=$rowD->number_of_person*$rowD->booking_normal_price;
                            }

//                            $HomeCon=new \App\Http\Controllers\Package\OrderTourController();
//                            $rate=$HomeCon->get_rate($Deposit);
//                            $system_fee=$TotalsAll*$rate/100;

                            $Totals=$TotalsAll+$AdditionalPrice;
                            ?>


                            <tr>
                                <td>#{{sprintf('%07d',$rows->booking_id)}}</td>
                                <td>{{$rows->booking_date}}</td>
                                <td>{{trans('common.'.$rows->status_name)}}</td>
                                <td class="text-right">

                                <div class="row">

                                        @if($Invoice_deposit->invoice_status=='2' || $Invoice_balance->invoice_status=='4')
                                            <div class="col-md-4">
                                                <p><i class="fa fa-check"></i> {{trans('common.deposit_paid')}} <br>
                                                    <a href="{{url('booking/view/invoice/'.$Invoice_deposit->invoice_id.'/1')}}" target="_blank"><i class="fa fa-file"></i> View invoice</a>
                                                </p>
                                            </div>
                                        @else
                                            <div class="col-md-6">
                                                <p><button type="submit" class="btn btn-info btn-block" id="checkout-button-1{{$rows->booking_id}}"> {{trans('common.pay_deposit')}}</button></p>
                                            </div>
                                        @endif

                                        @if($Invoice_balance->invoice_status=='4')
                                            <div class="col-md-4">
                                                <p><i class="fa fa-check"></i> {{trans('common.pay_all')}}<BR>
                                                    <a href="{{url('booking/view/invoice/'.$Invoice_balance->invoice_id.'/2')}}" target="_blank"><i class="fa fa-file"></i> View invoice</a>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="{{url('package/add/tour/info/'.$rows->booking_id)}}" target="_blank" class="btn btn-danger">{{trans('common.manage_booking')}}</a>
                                            </div>
                                        @else
                                            <div class="col-md-6">

                                                <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment')}}">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="booking_id" value="{{$rows->booking_id}}" >
                                                    <input type="hidden" name="type" value="2" >
                                                    <input type="hidden" name="invoice_id" value="{{$rowN->invoice_id}}" >
                                                    <input type="hidden" name="amount" value="{{$Totals}}" >
                                                    <input type="hidden" name="deposit" value="{{$Deposit}}" >
                                                    <input type="hidden" name="currency" value="{{strtolower($rowN->currency_code)}}" >
                                                <p><button type="submit" class="btn btn-success btn-block" id="checkout-button-2{{$rows->booking_id}}"><i class="fa fa-credit-card"></i> {{trans('common.pay_all')}}</button></p>
                                                </form>

                                            </div>
                                        @endif
                                </div>
                                    <script type="text/javascript">
                                        OmiseCard.configureButton('#checkout-button-1{{$rows->booking_id}}', {
                                            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_deposit')}} {{number_format($Deposit)}} Baht',
                                            submitLabel: '{{trans('common.pay_deposit')}}',
                                            amount: '{{$Deposit*100}}',
                                            currency: 'thb'
                                        });

                                        OmiseCard.configureButton('#checkout-button-2{{$rows->booking_id}}', {
                                            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{number_format($Totals)}} Baht',
                                            submitLabel: '{{trans('common.pay_all')}}',
                                            amount: '{{$Totals*100}}',
                                            currency: 'thb'
                                        });

                                    </script>
                                        {{--<table class="table table-borderless">--}}
                                            {{--@foreach($Invoice as $rowIn)--}}

                                                {{--@if($rowIn->invoice_type==1)--}}
                                                    {{--<tr>--}}
                                                        {{--<td>#{{sprintf('%07d',$rowIn->invoice_id)}}</td>--}}
                                                        {{--<td>--}}
                                                            {{--@if($rowIn->invoice_status>1)--}}
                                                                {{--<a href="{{url('/images/docs/'.$rowIn->payment_slip)}}"  class="btn btn-info" target="_blank"><i class="fa fa-search"></i> หลักฐานการโอนเงิน</a>--}}
                                                            {{--@else--}}
                                                                {{--<a href="{{url('/booking/view/invoice/'.$rowIn->invoice_id.'/1')}}" class="btn btn-warning"><i class="fa fa-file"></i> {{trans('common.invoice_deposit')}}</a>--}}
                                                            {{--@endif--}}
                                                        {{--</td>--}}

                                                    {{--</tr>--}}
                                                {{--@else--}}
                                                    {{--<tr>--}}
                                                        {{--<td>#{{sprintf('%07d',$rowIn->invoice_id)}}</td>--}}
                                                        {{--<td>--}}
                                                            {{--@if($rowIn->invoice_status>1)--}}
                                                                {{--<a href="{{url('/images/docs/'.$rowIn->payment_slip)}}"  class="btn btn-info" target="_blank"><i class="fa fa-search"></i> หลักฐานการโอนเงิน</a>--}}
                                                            {{--@else--}}
                                                                {{--@if($rowIn->invoice_status==1)--}}
                                                                    {{--<a href="{{url('/booking/view/invoice/'.$rowIn->invoice_id.'/2')}}" class="btn btn-success"><i class="fa fa-file"></i> {{trans('common.invoice_balance')}}</a>--}}
                                                                {{--@else--}}
                                                                   {{--<button type="button" class="btn btn-danger"><i class="fa fa-file"></i> {{trans('common.invoice_balance')}}</button>--}}
                                                                {{--@endif--}}
                                                            {{--@endif--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                {{--@endif--}}
                                            {{--@endforeach--}}
                                        {{--</table>--}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h3 class="text-center text-danger">Your booking hasn't been found!</h3>
                    <p class="text-center"><a href="{{url('home/package/all')}}" class="btn btn-warning"> Make a booking click go!!</a> </p>
                @endif
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript">
        // Then, attach all of the config and initiate it by 'OmiseCard.attach();' method
        OmiseCard.attach();
    </script>
@endsection