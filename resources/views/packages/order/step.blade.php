<?php
$disabled='disabled="disabled"';$onclick2="";$onclick3="";$onclick4="";
$onclick2="onclick=\"window.location.href='".url('booking/continuous/step2')."'\"";
$onclick4="onclick=\"window.location.href='".url('booking/continuous/invoice')."'\"";
if(isset($Booking)){
    $onclick4="onclick=\"window.location.href='".url('add/tour/info/'.$Booking->booking_id)."'\"";
}


?>

<div class="col-md-12">
    <div class="process">
        <div class="process-row">
            <div class="process-step">
                <button type="button" class="btn btn-{{Session::get('step_click')==1?'success':'default'}} btn-circle" @if(Auth::check()) onclick="window.location.href='{{url('booking/u/'.Auth::user()->id)}}'" @endif ><i class="fa fa-shopping-cart fa-2x"></i></button>
                <p>{{trans('common.cart')}}</p>
            </div>
            <div class="process-step">
                <button type="button" class="btn btn-{{Session::get('step_click')==2?'success':'default'}} btn-circle" {!! Session::get('step')>=2?$onclick2:$disabled !!}   ><i class="fa fa-list fa-2x"></i></button>
                <p>{{trans('common.order_summary')}}</p>
            </div>

            <div class="process-step">
                <button type="button" class="btn btn-{{Session::get('step_click')==3?'success':'default'}} btn-circle" {!! Session::get('step')>=3?$onclick4:$disabled !!} ><i class="fa fa-check-square fa-2x"></i></button>
                <p>{{trans('common.show_invoice')}}</p>
            </div>

            <div class="process-step">

                    <button type="button" class="btn btn-{{Session::get('step_click')==4?'success':'default'}} btn-circle" {!! Session::get('step')>=4?$onclick4:$disabled !!} ><i class="fa fa-users fa-2x"></i></button>
                    <p>{{trans('common.tour_information')}}</p>


            </div>

            <div class="process-step">
                <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-book fa-2x"></i></button>
                <p>{{trans('common.complete')}}</p>
            </div>
        </div>
    </div>
</div>