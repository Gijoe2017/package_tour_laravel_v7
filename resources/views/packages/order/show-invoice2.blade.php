@extends('layouts.package.master')

@section('program-highlight')
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;

        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }

        .omise-checkout-button{
            padding: 10px 20px;
            background-color: #00a65a;
            border-color: #008d4c;
            color: #ffffff;
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
        }
        h5{
            font-size: 18px;
            font-weight: bold;
            color: orange;
        }

    </style>

    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')
           </div>
           </div>
    </section>
    @foreach($Bookings as $Booking)
<?php

            $Package=DB::table('package_tour as a')
                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                ->where('a.packageID',$Booking->invoice_package_id)
                ->first();
           // dd($Package);
          //  $InvoiceNo=sprintf('%07d',$Booking->invoice_id);
            $OrderID='TC'.sprintf('%09d',$Booking->invoice_booking_id);
            $Timeline=\App\Timeline::where('id','37850')->first();

            $media=\App\Media::where('id',$Timeline->avatar_id)->first();

            $BankInfo=DB::table('business_verified_bank')
                ->where('timeline_id',$Timeline->id)
                ->get();
            $BusinessInfo=DB::table('business_verified_info1')
                ->where('timeline_id',$Timeline->id)
                ->first();

            $BusinessInfo1=DB::table('business_verified_info2')
                ->where('language_code',Auth::user()->language)
                ->where('timeline_id',$Timeline->id)
                ->first();


            $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code',Auth::user()->language)->first();
            if(!$country){
                $country=DB::table('country')->where('country_id',$BusinessInfo->country_id)->where('language_code','en')->first();
            }

            $states=DB::table('states')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('language_code',Auth::user()->langauge)
                ->first();
            if(!$states){
                $states=DB::table('states')
                    ->where('country_id',$BusinessInfo->country_id)
                    ->where('state_id',$BusinessInfo->state_id)
                    ->where('language_code','en')
                    ->first();
            }
            $city=DB::table('cities')
                ->where('country_id',$BusinessInfo->country_id)
                ->where('state_id',$BusinessInfo->state_id)
                ->where('city_id',$BusinessInfo->city_id)
                ->where('language_code',Auth::user()->langauge)
                ->first();
            if(!$city){
                $city=DB::table('cities')
                    ->where('country_id',$BusinessInfo->country_id)
                    ->where('state_id',$BusinessInfo->state_id)
                    ->where('city_id',$BusinessInfo->city_id)
                    ->where('language_code','en')
                    ->first();
            }

            $AddressBook=DB::table('address_book as a')
                ->join('countries as b','b.country_id','=','a.entry_country_id')
                ->where('a.timeline_id',Auth::user()->timeline_id)
                ->where('a.default_address','1')
                ->where('a.address_type','1')
                ->first();
            //dd($Booking);
            $Details=DB::table('package_booking_details as a')
                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                ->where('a.package_id',$Booking->invoice_package_id)
                ->where('a.booking_id',$Booking->invoice_booking_id)
                ->where('a.timeline_id',$Booking->invoice_timeline_id)
                ->where('a.package_detail_id',$Booking->invoice_package_detail_id)
                ->get();
            //dd($Details);
            $Deposit=0;$Totals=0;

            $currency_code=$Booking->currency_code;
            $currency_symbol=$Booking->currency_symbol;

            $Status=DB::table('booking_status')->where('booking_status',$Booking->invoice_status)->first();
            $bg='';
            if($Booking->invoice_status==2 || $Booking->invoice_status==4){
                $bg='bg-green';
            }
        ?>

        <!-- Main content -->
        <section class="invoice">
            <div class="container">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {{trans('common.deposit_invoice')}}
                        {{--<small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y')}}</small>--}}
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-8 invoice-col">
                    @if($media!=null)
                        <img class="logo-invoice" src="{{url('images/logo-toechok.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @else
                        <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                    @endif
                    {{--<img src="../../dist/img/credit/BunraksarTravel-New-Use.png" alt="Message User Image" style="height: 120px">--}}
                </div>
                <div class="col-sm-4 invoice-col">
                    <b>{{trans('common.invoice_no')}}: #{{$Booking->invoice_id}}</b><br>
                    <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i',strtotime($Booking->invoice_date))}}<br>
                    <b>{{trans('common.order_id')}}:</b> {{$Booking->invoice_booking_id}}<br>
                    {{--<b>{{trans('common.reference')}}:</b> ACB11<br>--}}
                </div>

            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <br>
                <div class="col-sm-4 invoice-col">
                    <strong>Billing From/จาก:  </strong><br>
                    <address>
                        {{$BusinessInfo1->legal_name}}<br>
                        {{$BusinessInfo1->address}}<br>
                        {{--{{$BusinessInfo1->address}}, {{$city->city}}<br>--}}
                        {{--{{$states->state}}, {{$country->country}} {{$BusinessInfo->zip_code}}<br>--}}
                        {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                        {{trans('common.email')}}: {{$BusinessInfo1->email}}
                    </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <strong>Billing To/ถึง:</strong>
                    @if($AddressBook)
                    <address>
                        {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                        {{$AddressBook->entry_street_address}}<br>
                        {{$AddressBook->entry_city}}, {{$AddressBook->country}} {{$AddressBook->entry_postcode}}<br>
                        {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                        {{trans('common.email')}}: {{$BusinessInfo1->email}}
                    </address>
                        @else
                    @endif
                </div>
                <div class="col-sm-4 invoice-col">
                    <div class="direct-chat-text text-center {{$bg}}">
                        <h4>{{trans('common.status').trans('common.'.$Status->status_name)}}</h4>

                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('common.items')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th>{{trans('common.unit_price')}}</th>
                            <th>{{trans('common.unit')}}</th>
                            <th>{{trans('common.unit_total')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;$TotalsAll=0;$Tax=0;$discount=0; $AdditionalPrice=0;?>

                        @foreach($Details as $rows)
                            <?php

                            $include_vat='';
                            $Timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                           // dd($Timeline);
                            $PackageDetailsOne=DB::table('package_details')
                                ->where('packageDescID',$rows->package_detail_id)
                                ->first();

                            if($PackageDetailsOne->season=='Y'){
                                $order_by="desc";
                            }else{
                                $order_by="asc";
                            }

                            $Condition=DB::table('condition_in_package_details as a')
                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                ->where('b.condition_group_id','1')
                                ->where('b.formula_id','>',0)
                                ->where('a.packageID',$rows->package_id)
                                ->orderby('c.value_deposit',$order_by)
                                ->first();

                            if($Condition){
                                $Deposit+=$Condition->value_deposit*$rows->number_of_person;
                            }

                             $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$rows->booking_detail_id)->get();

                             $st=explode('-',$rows->packageDateStart);
                             $end=explode('-',$rows->packageDateEnd);

                            if($st[1]==$end[1]){
                                $date=\Date::parse($rows->packageDateStart);
                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                // dd($end[0]);
                            }else{
                                $date=\Date::parse($rows->packageDateStart);
                                $date1=\Date::parse($rows->packageDateEnd);
                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                            }

                            if($rows->price_include_vat=='Y'){
                                $include_vat="Y";
                            }
                            $promotion_title=null;$every_booking=0;

                            $promotion=\App\PackagePromotion::where('packageDescID',$rows->package_detail_id)->active()
                                ->orderby('promotion_date_start','asc')
                                ->first();

                            $data_target=null;
                            if($promotion && $promotion->promotion_operator!='Mod'){
                                $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                            }

                         //   dd($promotion);

                            ?>
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <strong> {{$rows->TourType}}: {{trans('common.traveling_date')}} {{$package_date}}</strong><br>
                                    {{$Package->packageName}}<BR>
                                    {{trans('common.sell_by').':'. $Timeline->username}} <BR>
                                    @if($Deposit)
                                    <span class="text-danger"> {{trans('common.deposit')}} {{$rows->TourType}}: {{$Booking->currency_symbol.number_format($Condition->value_deposit)}} x {{$rows->number_of_person}}</span>
                                    @endif
                                </td>
                                @if($Booking->invoice_type=='1')
                                    <td>{{$Booking->currency_symbol.number_format($Condition->value_deposit)}}</td>
                                    <td>{{$rows->number_of_person}}</td>
                                    <td  style="text-align: right">
                                        {{$Booking->currency_symbol.number_format($Condition->value_deposit*$rows->number_of_person)}}
                                    </td>
                                @else
                                    <td>{{$Booking->currency_symbol.number_format($rows->booking_normal_price)}}</td>
                                    <td>{{$rows->number_of_person}}</td>
                                    <td  style="text-align: right">
                                        {{$Booking->currency_symbol.number_format($rows->booking_normal_price*$rows->number_of_person)}}
                                    </td>
                                @endif
                            </tr>
                            @if($Additional)
                                @foreach($Additional as $rowA)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                        <td>{{$Booking->currency_symbol.number_format($rowA->price_service)}}</td>
                                        <td>1</td>
                                        <td style="text-align: right">{{$Booking->currency_symbol.number_format($rowA->price_service)}}</td>
                                    </tr>
                                   <?php
                                    $AdditionalPrice+=$rowA->price_service;
                                    ?>
                                @endforeach
                            @endif

                            <?php
                                if($promotion){
                                    if($rows->booking_normal_price!=$rows->booking_realtime_price){
                                        $discount+=($rows->booking_normal_price-$rows->booking_realtime_price);
                                    }
                                }
                                $TotalsAll+=$rows->booking_normal_price*$rows->number_of_person;
                                $Status=DB::table('booking_status')->where('booking_status',$Booking->invoice_status)->first();
                            ?>

                        @endforeach


                        <?php
                        $Totals=$TotalsAll+$AdditionalPrice;
                        ?>
                        @if($Booking->invoice_type=='1')
                            <tr>
                            <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                                <td  style="text-align: right">{{$Booking->currency_symbol.number_format($Deposit)}}</td>

                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                <td style="text-align: right">{{$Booking->currency_symbol.number_format($discount)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                <td style="text-align: right"><h4>{{$Booking->currency_symbol.number_format($Deposit)}} </h4></td>
                            </tr>
                        @else
                            <tr>
                                @if($include_vat=='Y')
                                    <td colspan="4" style="text-align: right">{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</td>
                                @else
                                    <td colspan="4" style="text-align: right">{{trans('common.subtotal')}}</td>
                                @endif
                                <td  style="text-align: right">{{$Booking->currency_symbol.number_format($TotalsAll+$AdditionalPrice-$Deposit)}}</td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td colspan="4" style="text-align: right">{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</td>--}}
                                {{--<td  style="text-align: right">{{$Booking->currency_symbol.number_format($TotalsAll+$AdditionalPrice)}}</td>--}}
                            {{--</tr>--}}

                            <tr>
                                <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                <td style="text-align: right">{{$Booking->currency_symbol.number_format($discount)}}</td>
                            </tr>
                            @if($include_vat!='Y')
                                <?php $Tax=$Totals*.07;?>
                                <tr>
                                    <td colspan="4" style="text-align: right">({{trans('common.include_tax')}} 7%)</td>
                                    <td style="text-align: right">{{$Booking->currency_symbol.number_format($Tax)}}</td>
                                </tr>
                            @endif
                            {{--<tr>--}}
                                {{--<td colspan="4" style="text-align: right">{{trans('common.system_fee')}}</td>--}}
                                {{--<td style="text-align: right">{{$current->currency_symbol.number_format($system_fee)}}</td>--}}
                            {{--</tr>--}}
                        @if($Deposit>0)
                            <tr>
                                <td colspan="4" style="text-align: right"><h5>{{trans('common.deposit')}}:</h5></td>
                                <td style="text-align: right"><h5>-{{$Booking->currency_symbol.number_format($Deposit)}}</h5></td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="4" style="text-align: right"><h4>{{trans('common.total_amount')}}:</h4></td>
                                <td style="text-align: right"><h4>{{$Booking->currency_symbol.number_format($TotalsAll+$AdditionalPrice-$discount-$Deposit)}} </h4></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>


            <div class="row">
                <!-- accepted payments column -->
                @if($Package->package_partner=='Yes' || $Package->package_owner=='Yes')

                    <div class="col-md-6">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Payment Methods/ช่องท่างชำระเงิน:</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment')}}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="booking_id" value="{{$Booking->booking_id}}" >
                                    <input type="hidden" name="type" value="1" >
                                    <input type="hidden" name="invoice_id" value="{{$Booking->invoice_id}}" >
                                    <input type="hidden" name="amount" value="{{$Deposit}}" >
                                    <input type="hidden" name="currency" value="{{strtolower($Booking->currency_code)}}" >
                                    <!-- small box -->
                                    <div class="small-box bg-warning">
                                        <div class="inner text-center">
                                            <h4>{{trans('common.list_of_payment_tour_deposit_payment')}}</h4>
                                            <h3>{{$Booking->currency_symbol.number_format($Deposit)}}</h3>
                                            <p>
                                                {{trans('common.payment_due_date')}} : {{date('d/m/Y H:i',strtotime($Booking->invoice_payment_date))}}<br>
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        @if($Booking->invoice_status!='2' && $Booking->invoice_status!='4')

                                            <a href="#" class="small-box-footer">
                                                {{trans('common.status')}}: {{trans('common.waiting_for_payment')}} <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                            <button type="submit" class="btn btn-info btn-block btn-lg" id="checkout-button-0"> {{trans('common.pay_deposit')}}</button>
                                        @else
                                            <?php
                                            $status=DB::table('booking_status')->where('booking_status',$Booking->invoice_status)->first();
                                            ?>
                                            <a href="#" class="small-box-footer">
                                                {{trans('common.status')}} : {{trans('common.'.$status->status_name)}} <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        @endif

                                    </div>
                                </form>
                                <!--/.direct-chat-messages-->
                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>

                    <!-- /.col -->
                    <div class="col-md-6">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                            </div>

                            <form name="checkoutForm" method="POST" action="{{action('Package\PaymentController@OmisePayment')}}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="booking_id" value="{{$Booking->booking_id}}" >
                                <input type="hidden" name="type" value="2" >
                                <input type="hidden" name="invoice_id" value="{{$Booking->invoice_id}}" >
                                <input type="hidden" name="amount" value="{{$Totals}}" >
                                <input type="hidden" name="deposit" value="{{$Deposit}}" >
                                <input type="hidden" name="currency" value="{{strtolower($Booking->currency_code)}}" >
                                <!-- small box -->
                                <div class="small-box bg-green">
                                    <div class="inner text-center">
                                        <h4>{{trans('common.total_amount')}}</h4>
                                        <h3>{{$Booking->currency_symbol.number_format($Totals)}}</h3>
                                        <p>
                                            {{trans('common.payment_due_date')}} : {{date('d/m/Y H:i',strtotime($Booking->invoice_payment_date))}}<br>
                                        </p>
                                    </div>

                                    @if($Booking->invoice_status!='4')
                                        <a href="#" class="small-box-footer">
                                            {{trans('common.status')}}: {{trans('common.waiting_for_payment')}} <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                        <div class="icon">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <button type="submit" class="btn btn-info btn-block btn-lg" id="checkout-button-1"> {{trans('common.pay_deposit')}}</button>
                                    @else
                                        <?php
                                        $status=DB::table('booking_status')->where('booking_status',$Booking->invoice_status)->first();
                                        ?>
                                        <a href="#" class="small-box-footer">
                                            {{trans('common.status')}} : {{trans('common.'.$status->status_name)}} <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                    @endif
                                </div>
                            </form>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                @else
                     <div class="col-md-12">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-credit-card"></i> {{trans('common.payment_methods')}}</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-6">
                                @foreach($BankInfo as $bank)

                                        <div class="direct-chat-msg">
                                            <!-- /.direct-chat-info -->
                                            <img class="direct-chat-img" src="{{asset('images/credit/Thailand-Siam-Commercial-Bank.jpg')}}" alt="Message User Image"><!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                {{trans('common.bank').$bank->bank_name}} {{trans('common.bank_name').$bank->account_name}} <BR>
                                                {{trans('common.bank_account_number').$bank->bank_account_number}} {{trans('common.sub_bank').$bank->sub_bank}}

                                            </div>
                                            <!-- /.direct-chat-text -->
                                        </div>

                                @endforeach
                                </div>
                                <div class="col-md-6 text-right">
                                    @if(($Booking->invoice_status=='1' && $Booking->invoice_type=='1') || ($Booking->invoice_status=='1' && $Booking->invoice_type=='2'))
                                        <a href="{{url('booking/show/form_notification/'.$Booking->invoice_booking_id.'/'.$Booking->invoice_type)}}" class="btn btn-warning btn-lg"><i class="fa fa-paper-plane"></i> {{trans('common.payment_notification')}}</a>
                                    @endif
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                @endif

                <div class="col-md-12">
                    <div class="box box-warning direct-chat direct-chat-warning">
                    <?php
                    $Condition=DB::table('condition_in_package_details as a')
                        ->join('package_condition as b','b.condition_code','=','a.condition_id')
                        ->where('a.packageID',$rows->packageID)
                        ->where('a.condition_group_id','8')
                        ->where('b.language_code',Session::get('language'))
                        ->get();

                    ?>
                    @if($Condition)
                        <!-- /.box-header -->
                            <div class="box-body">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Remark/หมายเหตุ</h3>
                                </div>
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages-payment">
                                    <!-- Message. Default to the left -->
                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">
                                        <!-- /.direct-chat-info -->
                                        <div class="attachment-pushed">
                                            <div class="attachment-text">
                                                @foreach($Condition as $rows)
                                                    {!! $rows->condition_title !!}
                                                @endforeach
                                            </div>
                                            <!-- /.attachment-text -->
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->
                                    <hr>
                                </div>
                                <!--/.direct-chat-messages-->
                            </div>
                            <!-- /.box-body -->
                        @endif
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    <a href="{{url('booking/download/invoice/pdf/'.$Booking->invoice_booking_id)}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </a>
                </div>
            </div>
            </div>
        </section>
        <!-- /.content -->
@endforeach

    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script type="text/javascript">
        // Set default parameters
        OmiseCard.configure({
            publicKey: 'pkey_test_5dtunq6l6nl03ywhud7',
            image: 'https://toechok.com/setting/logo.jpg',
            frameLabel: 'TOECHOK CO.,LTD.',
        });

        OmiseCard.configureButton('#checkout-button-0', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_deposit')}} {{strtolower($currency_symbol)}}{{number_format($Deposit)}} ',
            submitLabel: '{{trans('common.pay_deposit')}}',
            amount: '{{$Deposit*100}}',
            currency: 'thb'
        });

        OmiseCard.configureButton('#checkout-button-1', {
            buttonLabel: '<i class="fa fa-credit-card"></i> {{trans('common.pay_all')}} {{strtolower($currency_symbol)}}{{number_format($Totals)}}',
            submitLabel: '{{trans('common.pay_all')}}',
            amount: '{{$Totals*100}}',
            currency: 'thb'
        });
        // Then, attach all of the config and initiate it by 'OmiseCard.attach();' method
        OmiseCard.attach();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.stepper').mdbStepper();
        });
        $('.delete-detail').on('click',function () {
            if(confirm('Confirm delete this cart detail?')){
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        // alert('test');
                        window.location =  window.location.href;
                    }
                });
            }
        });

        $(document).ready(function(){
            var counter = 1;
            $("#add").click(function () {
                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');
                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });

        });
    </script>


@endsection