@extends('layouts.package.master')

@section('program-highlight')
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .invoice-info{
            font-family: "Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
            font-size: 14px;
        }
        .invoice {
            position: relative;
            background: #fff;
            border: 1px solid #f4f4f4;
            padding: 10px;
            margin: 10px 25px;

        }
        address {
            margin-bottom: 20px;
            font-style: normal;
            line-height: 1.42857143;
        }

        .logo-invoice{
            width:120px
        }

        .omise-checkout-button{
            padding: 10px 20px;
            background-color: #00a65a;
            border-color: #008d4c;
            color: #ffffff;
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
        }
        h5{
            font-size: 18px;
            font-weight: bold;
            color: orange;
        }

        .table th{
            height: 40px;
            padding: 10px;
        }
        .bg-package {
            font-weight: bold;
            color: #000000;
            background-color: #D2E0E6 !important;
        }



    </style>

    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                @include('packages.order.step')
           </div>
           </div>
    </section>

    <section class="invoice">
        <div class="container">
            <div class="row invoice-warning">
        <!-- Main content -->

            <div class="col-md-12">

                <div class="col-md-6">
                    @if($Invoices->count())
                        <h4>{{trans('common.order_id').' #'.$Invoices[0]->invoice_booking_id}}</h4>
                    @else
                        <h4>{{trans('common.order_id').' #'.$booking_id}}</h4>
                    @endif
                </div>

                <div class="col-md-6">
                    <div class="pull-right">
                        <a href="{{url('my/bookings')}}" class="btn btn-default"><i class="fa fa-shopping-cart"></i> {{trans('common.back_to_mybooking')}}</a>
                    </div>
                </div>

                <div class="col-xs-12 table-responsive">

                    @if($Invoices->count())
                        <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('common.invoice_tour')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th>{{trans('common.totals')}}</th>
                            <th>{{trans('common.status')}}</th>
                            <th>{{trans('common.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $packageDetail='';$invoice_status='';?>

                        @foreach($Invoices as $Invoice)
                            <?php
                                $check_invoice_deposit=DB::table('package_invoice')
                                    ->where('invoice_booking_id',$Invoice->invoice_booking_id)
                                    ->where('invoice_package_id',$Invoice->invoice_package_id)
                                    ->where('invoice_package_detail_id',$Invoice->invoice_package_detail_id)
                                    ->where('invoice_type','1')
                                    ->first();

                                $Package=DB::table('package_tour as a')
                                ->join('package_tour_info as b','b.packageID','=','a.packageID')
                                ->where('a.packageID',$Invoice->invoice_package_id)
                                ->first();
                           // dd($Package);
                            $Timeline=\App\Timeline::where('id','37850')->first();

                            $media=\App\Media::where('id',$Timeline->avatar_id)->first();

                            $BankInfo=DB::table('business_verified_bank')
                                ->where('timeline_id',$Timeline->id)
                                ->get();
                            $BusinessInfo=DB::table('business_verified_info1')
                                ->where('timeline_id',$Timeline->id)
                                ->first();

                            $BusinessInfo1=DB::table('business_verified_info2')
                                ->where('language_code',Auth::user()->language)
                                ->where('timeline_id',$Timeline->id)
                                ->first();

                            $Details=DB::table('package_booking_details as a')
//                                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
//                                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                                ->where('a.package_id',$Invoice->invoice_package_id)
                                ->where('a.booking_id',$Invoice->invoice_booking_id)
                                ->where('a.timeline_id',$Invoice->invoice_timeline_id)
                                ->where('a.package_detail_id',$Invoice->invoice_package_detail_id)
                                ->get();
                            //dd($Details);
                            $Deposit=0;$Totals=0;

                            $currency_code=$Invoice->currency_code;
                            $currency_symbol=$Invoice->currency_symbol;

                            $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();
                            $bg='';
                            if($Invoice->invoice_status==2 || $Invoice->invoice_status==4){
                                $bg='bg-green';
                            }

                            ?>

                            @if($Invoice->invoice_package_detail_id!=$packageDetail)
                                <?php
                                if($Invoice->invoice_type==1){
                                    $invoice_status=$Invoice->invoice_status;
                                }
                                ?>
                            <tr>
                                <td colspan="5" class="bg-package">{{$Package->packageName}}</td>
                            </tr>
                            @endif

                        @if(!$check_invoice_deposit)

                            <tr>
                                <td>
                                  {{trans('common.invoice_tour')}} #{{$Invoice->invoice_id}} <Br>
                                </td>
                                <td>
                                    <?php  $AdditionalPrice=0;$discount=0;$pay_more=0;$Totals=0?>

                                    @foreach($Details as $Detail)
                                        <?php

                                        $Totals=$Detail->deposit_price*$Detail->number_of_person;
                                        $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();

                                        $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();
                                        if($Invoice->invoice_type==2){
                                            $Promotion=DB::table('package_booking_promotion')
                                                ->where('booking_detail_id',$Detail->booking_detail_id)
                                                ->first();
                                            $promotion_title='';
                                            if($Promotion){
                                                $promotion_title=$Promotion->promotion_title;
                                                if($Promotion->promotion_operator=='Between'){
                                                    if($Promotion->promotion_unit=='%'){
                                                        $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                                    }
                                                }else{
                                                    if($Promotion->promotion_operator2=='Up'){
                                                        if($Promotion->promotion_unit=='%'){
                                                            $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                        }else{
                                                            $pay_more=$Promotion->promotion_value;
                                                        }
                                                    }else{
                                                        if($Promotion->promotion_unit=='%'){
                                                            $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                        }else{
                                                            $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>

                                            {!!  $Detail->package_detail_title !!}<BR>

                                            @if($pay_more>0)
                                                {{trans('common.unit_price').' '.$Invoice->currency_symbol.number_format($Detail->booking_realtime_price).'+'.$pay_more}}
                                            @elseif($discount>0)
                                                {{trans('common.unit_price').' '}}
                                                <del>{{$Invoice->currency_symbol.number_format($Detail->booking_normal_price)}}</del>
                                                {{$Invoice->currency_symbol.number_format($Detail->booking_normal_price-$discount)}}
                                            @else
                                                {{trans('common.unit_price').' '.$Invoice->currency_symbol.number_format($Detail->booking_normal_price)}}
                                            @endif

                                            x {{$Detail->number_of_person}} {{trans('common.person')}}

                                            @if($Additional)
                                                <br>
                                                @foreach($Additional as $rowA)
                                                    <span class="text-warning">{{trans('common.additional')}}</span> {{$rowA->additional_service.' '.$Invoice->currency_symbol.number_format($rowA->price_service)}} x 1 <br>
                                                    <?php
                                                    $AdditionalPrice+=$rowA->price_service;
                                                    ?>
                                                @endforeach
                                            @endif

                                            @if($Detail->price_for_visa>0)
                                                <?php $PriceVisa=$Detail->price_for_visa*$Detail->number_of_need_visa;?>
                                                <span class="text-warning">{{$Detail->price_visa_details}} </span>
                                                {{$Invoice->currency_symbol.number_format($Detail->price_for_visa)}} x {{$Detail->number_of_need_visa}}
                                            @endif
                                            @if($discount>0)
                                                <br><small>{{$promotion_title}}</small>
                                                {{trans('common.discount')}}
                                                {{$Invoice->currency_symbol.number_format($discount)}}
                                            @endif

                                        @if($Detail->price_include_vat!='Y')
                                            <BR><small class="text-danger"> <i>({{trans('common.this_price_not_include_vat')}})</i></small>
                                        @else
                                            <BR><small class="text-danger"> <i>({{trans('common.this_price_include_vat')}})</i></small>
                                        @endif


                                        <?php
                                        if($Detail->price_include_vat=='Y'){
                                            $include_vat="Y";
                                        }
                                        $Totals=$Invoice->invoice_amount+$AdditionalPrice-$discount;

                                        ?>
                                        <hr>

                                    @endforeach

                                </td>
                                <td>
                                    <strong>{{$Invoice->currency_symbol.number_format($Invoice->invoice_amount)}}</strong>
                                </td>
                                <td>
                                        <?php
                                        $check=DB::table('payment_notification_sub')
                                            ->where('payment_invoice_id',$Invoice->invoice_id)
                                            ->where('status','s')
                                            ->first();

                                        ?>

                                        @if(!$check)
                                            <span class="text-danger"> {{trans('common.'.$Status->status_name)}} </span><BR>
                                        @else
                                            {{trans('common.awaiting_confirmation')}} <BR>
                                        @endif
                                        <small class="text-red">
                                            @if($Invoice->invoice_status=='1' && $Invoice->invoice_type=='1')
                                                {{trans('common.please_pay_the_deposit_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}
                                            @elseif($Invoice->invoice_status=='1' && $Invoice->invoice_type=='2')
                                                {{trans('common.please_pay_the_remaining_amount_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}
                                            @endif
                                        </small>

                                </td>
                                <td>
                                    @if($Package->package_partner=='Yes' || $Package->package_owner=='Yes')

                                        @if(($Invoice->invoice_type=='1' && $Invoice->invoice_status=='1') || ($Invoice->invoice_type=='2' && $Invoice->invoice_status=='1'))
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-success btn-block" target="_blank"><i class="fa fa-credit-card"></i>  {{trans('common.pay_via_credit_card')}}</a>
                                        @endif

                                    @endif
                                        @if($Invoice->invoice_status!='7')
                                            @if(!$check)
                                                <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-warning btn-block"  target="_blank" {{$Invoice->invoice_status=='7'?'disabled':''}}><i class="fa fa-search"></i> {{trans('common.invoice_tour')}}</a>
                                            @else
                                                @if($Invoice->invoice_status==1)
                                                    <strong class="text-danger"> <i class="fa fa-hourglass-end"></i> {{trans('common.awaiting_confirmation')}}</strong>
                                                @else
                                                    <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-success btn-block" target="_blank" {{$Invoice->invoice_status=='7'?'disabled':''}}><i class="fa fa-search"></i> {{trans('common.invoice_balance')}}</a>
                                                @endif
                                            @endif
                                        @endif

                                    @if($Invoice->invoice_status==4)
                                        <a href="{{url('package/add/tour/info/'.$Invoice->invoice_booking_id)}}" class="btn btn-info btn-block" target="_blank" {{$Invoice->invoice_status=='7'?'disabled':''}}><i class="fa fa-users"></i> {{trans('common.tour_information')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @else
                            @if(($Invoice->invoice_type=='1') || $invoice_status=='2' && $Invoice->invoice_type=='2' )
                            <tr>
                            <td>
                                @if($Invoice->invoice_type==1)
                                    {{trans('common.invoice_deposit')}}
                                @else
                                    {{trans('common.invoice_balance')}}
                                @endif
                                    #{{$Invoice->invoice_id}} <Br>
                            </td>
                            <td>

                                <?php  $AdditionalPrice=0;$discount=0;$pay_more=0;$Totals=0?>

                                @foreach($Details as $Detail)
                                    <?php
                                        $Totals=$Detail->deposit_price*$Detail->number_of_person;
                                        $Status=DB::table('booking_status')->where('booking_status',$Invoice->invoice_status)->first();

                                        $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();
                                        if($Invoice->invoice_type==2){
                                        $Promotion=DB::table('package_booking_promotion')
                                            ->where('booking_detail_id',$Detail->booking_detail_id)
                                            ->first();
                                        $promotion_title='';
                                        if($Promotion){
                                            $promotion_title=$Promotion->promotion_title;
                                            if($Promotion->promotion_operator=='Between'){
                                                if($Promotion->promotion_unit=='%'){
                                                    $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                                }else{
                                                    $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                                }
                                            }else{
                                                if($Promotion->promotion_operator2=='Up'){
                                                    if($Promotion->promotion_unit=='%'){
                                                        $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $pay_more=$Promotion->promotion_value;
                                                    }
                                                }else{
                                                    if($Promotion->promotion_unit=='%'){
                                                        $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                                    }else{
                                                        $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                                    }
                                                }
                                            }
                                        }
                                        }


                                    ?>

                                {!!  $Detail->package_detail_title !!}<BR>

                                @if($Invoice->invoice_type==1)
                                    {{trans('common.unit_price').' '.$Invoice->currency_symbol.number_format($Detail->deposit_price)}}
                                @else
                                    @if($pay_more>0)
                                        {{trans('common.unit_price').' '.$Invoice->currency_symbol.number_format($Detail->booking_realtime_price).'+'.$pay_more}}
                                    @elseif($discount>0)
                                        {{trans('common.unit_price').' '}}
                                        <del>{{$Invoice->currency_symbol.number_format($Detail->booking_normal_price)}}</del>
                                       {{$Invoice->currency_symbol.number_format($Detail->booking_normal_price-$discount)}}
                                    @else
                                        {{trans('common.unit_price').' '.$Invoice->currency_symbol.number_format($Detail->booking_normal_price)}}
                                    @endif
                                @endif
                                    x {{$Detail->number_of_person}} {{trans('common.person')}}

                                    @if($Invoice->invoice_type!=1)
                                        @if($Additional)
                                            <br>
                                            @foreach($Additional as $rowA)
                                                    <span class="text-warning">{{trans('common.additional')}}</span> {{$rowA->additional_service.' '.$Invoice->currency_symbol.number_format($rowA->price_service)}} x 1 <br>
                                                <?php
                                                $AdditionalPrice+=$rowA->price_service;
                                                ?>
                                            @endforeach
                                        @endif

                                        @if($Detail->price_for_visa>0)

                                            <?php $PriceVisa=$Detail->price_for_visa*$Detail->number_of_need_visa;?>
                                            <span class="text-warning">{{$Detail->price_visa_details}} </span>
                                            {{$Invoice->currency_symbol.number_format($Detail->price_for_visa)}} x {{$Detail->number_of_need_visa}}
                                        @endif
                                        @if($discount>0)
                                            <br><small>{{$promotion_title}}</small>
                                            {{trans('common.discount')}}
                                            {{$Invoice->currency_symbol.number_format($discount)}}
                                        @endif
                                    @endif

                                    @if($Detail->price_include_vat!='Y')
                                        <BR><small class="text-danger"> <i>({{trans('common.this_price_not_include_vat')}})</i></small>
                                    @else
                                        <BR><small class="text-danger"> <i>({{trans('common.this_price_include_vat')}})</i></small>
                                    @endif


                                    <?php



                                    if($Detail->price_include_vat=='Y'){
                                        $include_vat="Y";
                                    }
                                    $Totals=$Invoice->invoice_amount+$AdditionalPrice-$discount;

                                    ?>


                                <hr>

                                @endforeach

                            </td>
                            <td>
                               <strong>{{$Invoice->currency_symbol.number_format($Invoice->invoice_amount)}}</strong>
                            </td>
                            <td>

                                <?php
                                $check=DB::table('payment_notification_sub')
                                    ->where('payment_invoice_id',$Invoice->invoice_id)
                                    ->where('status','s')
                                    ->first();

                                ?>

                                    @if(!$check)
                                       <span class="text-danger"> {{trans('common.'.$Status->status_name)}} </span><BR>
                                    @else
                                        {{trans('common.awaiting_confirmation')}} <BR>
                                    @endif
                                    <small class="text-red">
                                        @if($Invoice->invoice_status=='1' && $Invoice->invoice_type=='1')
                                            {{trans('common.please_pay_the_deposit_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}
                                        @elseif($Invoice->invoice_status=='1' && $Invoice->invoice_type=='2')
                                            {{trans('common.please_pay_the_remaining_amount_before').' '.date('d/m/Y',strtotime($Invoice->invoice_payment_date))}}
                                        @endif
                                    </small>

                            </td>
                            <td>
                                    @if($Package->package_partner=='Yes' || $Package->package_owner=='Yes')
                                        @if(($Invoice->invoice_type=='1' && $Invoice->invoice_status=='1') || ($Invoice->invoice_type=='2' && $Invoice->invoice_status=='1'))
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-success btn-block" target="_blank"><i class="fa fa-credit-card"></i>  {{trans('common.pay_via_credit_card')}}</a>
                                        @endif
                                    @endif
                                    @if($Invoice->invoice_status!='7')
                                    @if($Invoice->invoice_type==1)
                                        @if(!$check)
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-warning btn-block" target="_blank" ><i class="fa fa-search"></i> {{trans('common.invoice_deposit')}}</a>
                                            @if($Invoice->invoice_status==1)
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_booking_id.'/'.$Invoice->invoice_package_detail_id.'/all')}}" class="btn btn-warning btn-block" target="_blank" {{$Invoice->invoice_status=='7'?'disabled':''}}><i class="fa fa-search"></i> {{trans('common.invoice_tour_all')}}</a>
                                            @endif
                                        @else
                                            <a href="#" class="btn btn-default btn-block" ><i class="fa fa-search"></i> {{trans('common.invoice_deposit')}}</a>
                                        @endif

                                    @else
                                        @if(!$check)
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-search"></i> {{trans('common.invoice_balance')}}</a>
                                        @else
                                            @if($Invoice->invoice_status==1)
                                                <strong class="text-danger"> <i class="fa fa-hourglass-end"></i> {{trans('common.awaiting_confirmation')}}</strong>
                                            @else
                                            <a href="{{url('booking/view/invoice/'.$Invoice->invoice_id)}}" class="btn btn-success btn-block" target="_blank"><i class="fa fa-search"></i> {{trans('common.invoice_balance')}}</a>
                                            @endif
                                        @endif
                                    @endif
                                        @endif

                                    @if($Invoice->invoice_status==2 || $Invoice->invoice_status==4)
                                        <a href="{{url('package/add/tour/info/'.$Invoice->invoice_booking_id)}}" class="btn btn-info btn-block" target="_blank"><i class="fa fa-users"></i> {{trans('common.tour_information')}}</a>
                                    @endif
                            </td>
                        </tr>
                            @endif
                        @endif
                            <?php
                               $packageDetail= $Invoice->invoice_package_detail_id;
                            ?>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4 class="alert alert-warning text-center">{{trans('common.no_invoice')}} <BR> <BR>
                            @if(Session::has('message'))
                                <small class="text-success"><i class="fa fa-check"></i> {{Session::get('message')}}</small>
                            @else
                        <small><a href="{{url('booking/problem/invoice/'.$booking_id)}}" class="btn btn-danger"><i class="fa fa-share-square"></i> {{trans('common.click_report_a_problem_to_check')}}</a></small>
                            @endif
                        </h4>
                    @endif
                </div>
            </div>
         </div>
    </div>
</section>



@endsection