
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
    }
    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    table {
        font-family: "THSarabunNew";
        border-collapse: collapse;

    }

    th, td {
        border-bottom: 1px solid #ddd;
        padding: 5px;

    }
    .page-break {
        page-break-after: always;
    }
</style>
<?php $page=0; $Deposit=0; ?>


@foreach($Invoice as $rowN)
        <?php
        $page++;
        $Package=DB::table('package_tour as a')
            ->join('package_tour_info as b','b.packageID','=','a.packageID')
            ->where('a.packageID',$rowN->invoice_package_id)
            ->first();

        $current=\App\Currency::where('currency_code',$Package->packageCurrency)->first();

        $Timeline=\App\Timeline::where('id','37850')->first();

        $media=\App\Media::where('id',$Timeline->avatar_id)->first();
       // dd($media);
        $BankInfo=DB::table('business_verified_bank')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo=DB::table('business_verified_info1')
            ->where('timeline_id',$Timeline->id)
            ->first();
        $BusinessInfo1=DB::table('business_verified_info2')
            ->where('language_code',Auth::user()->language)
            ->where('timeline_id',$Timeline->id)
            ->first();

        $AddressBook=DB::table('address_book as a')
            ->join('countries as b','b.country_id','=','a.entry_country_id')
            ->where('a.timeline_id',Auth::user()->timeline_id)
            ->where('a.default_address','1')
            ->where('a.address_type','1')
            ->first();

        $Details=DB::table('package_booking_details as a')
            ->where('a.booking_id',$rowN->invoice_booking_id)
            ->where('a.timeline_id',$rowN->invoice_timeline_id)
            ->where('a.package_detail_id',$rowN->invoice_package_detail_id)
            ->get();
        ?>
        <table class="table" width="100%">
            <tr>
                <td colspan="3" >
                    <h2 class="page-header">
                        @if($rowN->invoice_type=='1')
                            {{trans('common.deposit_invoice')}}
                        @else
                            {{trans('common.invoice_balance')}}
                        @endif
                        <small class="pull-right">{{trans('common.date')}}: {{date('d/m/Y',strtotime($rowN->invoice_date))}}</small>
                    </h2>
                </td>
            </tr>
            <tr>
                <td colspan="3"  >
                    <table class="table" width="100%">
                        <tr>
                            <td width="60%">
                                @if($media!=null)
                                    <img class="logo-invoice" style="height: 100%" src="{{url('images/logo-toechok-invoice.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @else
                                    <img class="logo-invoice" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$Timeline->name }}" title="{{ $Timeline->name }}">
                                @endif
                            </td>
                            <td>
                                <b>{{trans('common.invoice_no')}}: #{{$rowN->invoice_id}}</b><br>
                                <b>{{trans('common.billing_date')}}:</b> {{date('d/m/Y H:i')}}<br>
                                <b>{{trans('common.order_id')}}: #{{$rowN->invoice_booking_id}}</b> <br>

                                <?php
                                $Status=DB::table('booking_status')->where('booking_status',$rowN->invoice_status)->first();
                                ?>
                                <strong> {{trans('common.status')}}: {{trans('common.'.$Status->status_name)}} <i class="fa fa-arrow-circle-right"></i> </strong>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    <table class="table" width="100%">
                        <tr>
                            <td width="50%">
                                <strong>Billing From/จาก:  </strong><br>
                                <address>
                                    {{$BusinessInfo1->legal_name}}<br>
                                    {{$BusinessInfo1->address}}<br>
                                    {{trans('common.phone')}}: {{$BusinessInfo1->phone}}<br>
                                    {{trans('common.emails')}}: {{$BusinessInfo1->email}}
                                </address>
                            </td>
                            <td >
                                <strong>Billing To/ถึง:</strong>
                                @if($AddressBook)
                                    <address>
                                        {{$AddressBook->entry_firstname.' '.$AddressBook->entry_lastname}}<br>
                                        {!! $AddressBook->address_show !!}<br>
                                        {{trans('common.phone')}}: {{$AddressBook->entry_phone}}<br>
                                        {{trans('common.emails')}}: {{$AddressBook->entry_email}}
                                    </address>
                                @endif
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>{{trans('common.items')}}</th>
                            <th>{{trans('common.description')}}</th>
                            <th align="right">{{trans('common.unit_price')}}</th>
                            <th align="right">{{trans('common.unit')}}</th>
                            <th align="right">{{trans('common.unit_total')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $i=1;$Totals=0;$TotalsAll=0;$Tax=0;$pay_more=0;$SubTotals=0;$discount_total=0;$pay_more_total=0;$Deposit=0;

                        foreach ($Details as $Detail){
                            $include_vat=$Detail->price_include_vat;
                            $AdditionalPrice=0;$PriceVisa=0;$discount=0;
                            $Additional=DB::table('package_booking_additional')->where('booking_detail_id',$Detail->booking_detail_id)->get();

                        $Deposit_title=$Detail->deposit_price;
                        $Deposit+=$Detail->deposit_price*$Detail->number_of_person;

                        $Promotion=DB::table('package_booking_promotion')
                            ->where('booking_detail_id',$Detail->booking_detail_id)
                            ->first();
                        $promotion_title='';
                        if($Promotion){
                            $promotion_title=$Promotion->promotion_title;
                            if($Promotion->promotion_operator=='Between'){
                                if($Promotion->promotion_unit=='%'){
                                    $discount=$Detail->booking_normal_price*$Promotion->promotion_value/100;
                                }else{
                                    $discount=$Detail->booking_normal_price-$Promotion->promotion_value;
                                }

                            }else{
                                if($Promotion->promotion_operator2=='Up'){
                                    if($Promotion->promotion_unit=='%'){
                                        $pay_more=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                    }else{
                                        $pay_more=$Promotion->promotion_value;
                                    }
                                }else{
                                    if($Promotion->promotion_unit=='%'){
                                        $discount=$Detail->booking_realtime_price*$Promotion->promotion_value/100;
                                    }else{
                                        $discount=$Detail->booking_realtime_price-$Promotion->promotion_value;
                                    }
                                }
                            }
                        }
                        ?>


                        @if($rowN->invoice_type==1)
                            <tr>
                                <td>{{$i++}}</td>
                                <td><strong>{!! $Detail->package_detail_title !!}</strong></td>
                                <td align="right">{{$rowN->currency_symbol.number_format($Deposit_title)}}</td>
                                <td align="right">{{$Detail->number_of_person}}</td>
                                <td align="right">{{$rowN->currency_symbol.number_format($Deposit)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right"> <strong>{{trans('common.deposit')}}:</strong></td>
                                <td align="right"> <strong>{{$rowN->currency_symbol.number_format($Deposit)}}</strong> </td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <strong>{!! $Detail->package_detail_title !!}</strong>
                                    <span class="text-danger"> {{trans('common.deposit')}} {{$Detail->TourType}}: {{$rowN->currency_symbol.number_format($Deposit_title)}} x {{$Detail->number_of_person}}</span>
                                </td>
                                <td align="right">
                                    {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}
                                    @if($pay_more>0)
                                        <?php
                                        $Price_sub=$Detail->booking_realtime_price+$pay_more;
                                        ?>
                                        {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}+{{$pay_more}}
                                    @elseif($discount>0)
                                        <?php
                                        $Price_sub=$Detail->booking_normal_price-$discount;
                                        ?>
                                        <del>{{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}</del>
                                        {{$rowN->currency_symbol.number_format($Detail->booking_normal_price-$discount)}}
                                    @else
                                        <?php
                                        $Price_sub=$Detail->booking_normal_price;
                                        ?>
                                        {{$rowN->currency_symbol.number_format($Detail->booking_normal_price)}}
                                    @endif
                                </td>
                                <td align="right">{{$Detail->number_of_person}}</td>
                                <td align="right">{{$rowN->currency_symbol.number_format($Price_sub*$Detail->number_of_person)}}</td>
                            </tr>

                            @if($Additional)
                                @foreach($Additional as $rowA)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td><strong>{{trans('common.additional')}}</strong> {{$rowA->additional_service}}</td>
                                        <td align="right">{{$rowN->currency_symbol.number_format($rowA->price_service)}}</td>
                                        <td align="right">1</td>
                                        <td align="right">{{$rowN->currency_symbol.number_format($rowA->price_service)}}</td>
                                    </tr>
                                    <?php
                                    $AdditionalPrice+=$rowA->price_service;
                                    ?>
                                @endforeach
                            @endif
                            @if($Detail->price_for_visa>0)
                                <?php $PriceVisa=$Detail->price_for_visa*$Detail->number_of_need_visa;?>
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><strong>{{$Detail->price_visa_details}} </strong></td>
                                    <td>{{$rowN->currency_symbol.number_format($Detail->price_for_visa)}}</td>
                                    <td>{{$Detail->number_of_need_visa}}</td>
                                    <td style="text-align: right">{{$rowN->currency_symbol.number_format($PriceVisa)}}</td>
                                </tr>
                            @endif


                            <?php
                            $Status=DB::table('booking_status')->where('booking_status',$rowN->invoice_status)->first();
                            $discount_total+=$discount*$Detail->number_of_person;
                            $pay_more_total+=$pay_more*$Detail->number_of_person;

                            $SubTotals=round($Price_sub*$Detail->number_of_person)+$AdditionalPrice+$PriceVisa;
                            if($Detail->price_include_vat=='Y'){
                                $include_vat="Y";
                            }else{
                                $Tax=$SubTotals*.07;
                            }
                            $TotalsAll+=$SubTotals;
                            $Totals+=$SubTotals+$Tax;

                            ?>
                        @endif
                      <?php }?>
                        @if($rowN->invoice_type==1)
                            <tr>
                                <td colspan="4" style="text-align: right"> <strong>{{trans('common.total_amount')}}:</strong></td>
                                <td align="right"> <strong>{{$rowN->currency_symbol.number_format($Deposit)}}</strong> </td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="4" style="text-align: right">
                                    <strong>{{trans('common.subtotal')}} ({{trans('common.include_tax')}} 7%)</strong></td>
                                <td align="right"> <strong>{{$rowN->currency_symbol.number_format($TotalsAll)}}</strong> </td>
                            </tr>
                            @if($discount_total)
                                <tr>
                                    <td colspan="4" style="text-align: right">{{trans('common.discount')}}</td>
                                    <td style="text-align: right">{{$rowN->currency_symbol.number_format($discount_total)}}</td>
                                </tr>
                            @endif

                            @if($include_vat!='Y')
                                <tr>
                                    <td colspan="4" style="text-align: right"> <strong>{{trans('common.tax')}}</strong></td>
                                    <td align="right"> <strong>{{$rowN->currency_symbol.number_format($Tax)}}</strong></td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="4" style="text-align: right"> <strong>{{trans('common.deposit')}}:</strong></td>
                                <td align="right"> <strong>-{{$rowN->currency_symbol.number_format($Deposit)}}</strong> </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right"> <strong>{{trans('common.total_amount')}}:</strong></td>
                                <td align="right"> <strong>{{$rowN->currency_symbol.number_format($Totals-$Deposit)}}</strong> </td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                </td>
            </tr>
        </table>

        @if($page<2 && $Invoice->count()>1)
            <div class="page-break"></div>
        @endif
@endforeach