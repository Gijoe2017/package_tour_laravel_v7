@extends('layouts.package.master')



@section('program-latest')
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhai&display=swap" rel="stylesheet">
    <style>
        .price-wrap{
            font-family: 'Baloo Bhai', cursive;
            color: #00A000;
        }
    </style>

    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-md-12">
        <div class="row">

    @if($PackageTour->count()>0)
        @foreach($PackageTour as $rows)
            <?php $url_link='home/details/'.$rows->packageID;?>

            <?php

            $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();

            $current=\App\Currency::where('currency_code',$rows->packageCurrency)->first();
             $detail=DB::table('package_details')
                    ->where('packageID',$rows->packageID)
                    ->where('packageDateStart','>',date('Y-m-d'))
                    ->where('status','Y')
                    ->orderby('packageDateStart','asc');
//                    ->limit(3)
            $countDetail=$detail->count();
            $Details=$detail->limit(4)->get();
            //  dd($Details);
            $Price=DB::table('package_details_sub as a')
                    ->join('package_details as b','b.packageDescID','=','a.packageDescID')
                    ->where('a.packageID',$rows->packageID)
                    ->where('b.status','Y')
                    ->where('a.status','Y')
                    ->orderby('a.PriceSale','desc')
                    ->first();

            $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
           // dd($pricePro);
            //                                        if($rows->packageID==43){
            //                                            dd($Price);
            //                                        }
            $data_target=null;
            $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                ->where('promotion_date_end','>',date('Y-m-d'))
                    ->orderby('promotion_date_start','asc')
                    ->first();



            $promotion_title=null;$every_booking='';
            if($promotion){
                $every_booking=$promotion->every_booking;
                if($promotion->promotion_operator=='Between'){
                    $promotion_title='Between';
                    if($promotion->promotion_operator2=='up'){
                        if($promotion->promotion_unit=='%'){
                            if($promotion->promotion_value>0){
                                foreach ($pricePro as $rowPrice){
                                    $Price_up=$rowPrice->price_system_fees*$promotion->promotion_value/100;
                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees+$Price_up]);
                                }
                            }
                        }else{
                            foreach ($pricePro as $rowPrice){
                                $Price_up=$promotion->promotion_value;
                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees+$Price_up]);
                            }
                        }

                    }else{ // promotion down
                        if($promotion->promotion_unit=='%'){
                            if($promotion->promotion_value>0){
                                foreach ($pricePro as $rowPrice){
                                    $Price_down=$Price->price_system_fees*$promotion->promotion_value/100;
                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees-$Price_down]);
                                }
                            }
                        }else{
                            foreach ($pricePro as $rowPrice){
                                $Price_down=$promotion->promotion_value;
                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees-$Price_down]);
                            }
                        }
                    }

                    $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));

                }else{
                    $promotion_title='Mod';
                    //  dd($rows->packageID.' '.$Price->packageDescID);
                    $booking=DB::table('package_bookings as a')
                            ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                            ->where('b.package_id',$rows->packageID)
                            ->where('b.package_detail_id',$Price->packageDescID)
//                                                ->first();
                            ->sum('b.number_of_person');
//
                    if($booking>0){ // Check booking

                        if($booking % $promotion->every_booking==1){ // every booking
                            if($booking>$promotion->every_booking){
                                if($promotion->promotion_operator2=='up'){
                                    if($promotion->promotion_unit=='%'){
                                        if($promotion->promotion_value>0){
                                            foreach ($pricePro as $rowPrice){
                                                $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                            }
                                        }
                                    }else{
                                        foreach ($pricePro as $rowPrice){
                                            $Price_up=$promotion->promotion_value;
                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                        }
                                    }
                                    /// $Price_by_promotion+=$Price_up;
                                }else{
                                    if($promotion->promotion_unit=='%'){
                                        if($promotion->promotion_value>0){
                                            foreach ($pricePro as $rowPrice){
                                                $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                            }
                                        }
                                    }else{
                                        foreach ($pricePro as $rowPrice){
                                            $Price_down=$promotion->promotion_value;
                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                        }
                                    }
                                }
                            }

                        }

                    }
//                                                    dd($Price_now);
                }
            }


            ?>
            <div class="col-md-4">
                <figure class="card card-product">
                    @if($promotion_title=='Between')
                        @if($data_target!=null)
                            <span class="price-save-date">
                              <span id="clockdiv{{$Price->packageDescID}}">({{trans('package.this_price')}}
                                  <span class="days"></span> {{trans('common.day')}}
                                  <span class="hours"></span>:
                                  <span class="minutes"></span>:
                                  <span class="seconds"></span>, {{$Price->NumberOfPeople}} {{trans('package.the_last_place_only')}})
                              </span>
                            </span>
                            <script language="JavaScript">
                                initializeClock('clockdiv{{$Price->packageDescID}}', new Date('{{$data_target}}'));
                            </script>
                            <span class="badge-offer"><b> {{$Price->packageDescID}} </b></span>
                        @endif
                    @elseif($promotion_title=='Mod')
                        <span class="badge-new">
                            <?php
                                $bookings=\App\Bookings::where('package_id',$rows->packageID)
                                        ->where('package_detail_id',$Price->packageDescID)
                                        ->sum('number_of_person');
                                if($bookings>0){
                                    $every_booking=$every_booking-$bookings % $every_booking;
                                }
                            ?>
                            {{trans('package.this_price_is_only')}} {{$every_booking}} {{trans('common.seat')}}
                        </span>
                    @endif

                    {{--<span class="badge-offer"><b> -50% </b></span>--}}

                    <div class="img-wrap">
                        <a href="{{url($url_link)}}">
                            <img src="{{url('package/tour/small/'.$rows->Image)}}">
                        </a>

                    </div>
                    <div class="col-lg-12 text-right">
                        <small>{{trans('package.package_tour_by')}} <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a></small>
                    </div>

                    <figcaption class="info-wrap-2">
                        <h5 class="title">
                            <a href="{{url($url_link)}}">{{$rows->packageName}}</a>
                        </h5>
                        {{--<div class="rating-wrap">--}}
                            {{--<a href="detail.html">--}}
                                {{--<ul class="rating-stars">--}}
                                    {{--<li style="width:80%" class="stars-active">--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                        {{--<i class="fa fa-star"></i>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                                {{--<div class="label-rating">132 รีวิว</div>--}}
                            {{--</a>--}}
                            {{--@if(\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')>0)--}}
                                {{--<div class="label-rating">--}}
                                    {{--<strong style="color:red">--}}
                                        {{--{{\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')}}</strong>--}}
                                        {{--{{trans('common.booking')}}--}}
                                {{--</div>--}}
                            {{--@endif--}}

                        {{--</div> <!-- rating-wrap.// -->--}}
                    </figcaption>
                    <div class="bottom-wrap">
                        <div class="price-wrap">
                              <span class="price-new">
                                 <a href="{{url('home/details/'.$rows->packageID)}}">
                                     {{$countDetail}} {{trans('common.time_zone')}}

                                     @foreach($Details as $detail)
                                         <?php
                                             $st=explode('-',$detail->packageDateStart);
                                             $end=explode('-',$detail->packageDateEnd);
                                             if($st[1]==$end[1]){
                                                 $date=\Date::parse($detail->packageDateStart);
                                                 $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                 // dd($end[0]);
                                             }else{
                                                 $date=\Date::parse($detail->packageDateStart);
                                                 $date1=\Date::parse($detail->packageDateEnd);
                                                 $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                             }
                                         ?>
                                         {{$package_date}},

                                     @endforeach
                                     @if($countDetail>4)
                                        <span class="pull-right text-secondary">{{trans('common.view_more')}}</span>
                                     @endif
                                 </a>
                              </span>
                        </div> <!-- price-wrap.// -->

                        <div class="col-md-8 price-wrap text-center">
                            {{--<span class="price-discount">฿24,999</span> <del class="price-old">฿26,999</del><br>--}}
                            {{--<span class="price-save">ประหยัด ฿2,999</span>--}}
                            @if($promotion)

                                @if($Price->Price_by_promotion!=$Price->price_system_fees)
                                    {{trans('common.normal_price')}} <del class="price-old">{{$current->currency_symbol.number_format($Price->price_system_fees)}}</del><br>
                                    <span class="price-save">
                                        {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                        {{$current->currency_symbol.number_format($Price->price_system_fees-$Price->Price_by_promotion)}}
                                    </span>
                                @endif
                                   <Br> {{trans('common.sale')}} <span class="price-discount"> {{$current->currency_symbol.number_format($Price->Price_by_promotion)}}</span>
                            @else
                                <BR>
                                <p class="text-center">{{trans('common.start_price')}}</p>
                                <h4 class="price-discount"> {{$current->currency_symbol.number_format($Price->price_system_fees)}} <small class="text-secondary">/{{trans('common.per_person')}}</small></h4>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <BR>
                            <a href="{{url('/booking/c/'.$Price->packageDescID)}}" class="btn btn-lg btn-primary">{{trans('common.booking')}}</a>
                        </div>

                        <!-- price-wrap.// -->
                    </div> <!-- bottom-wrap.// -->
                </figure>
            </div>
        @endforeach

        @else
            <div class="col-md-8 text-center">
                <h5>Cannot find the item you are searching.</h5>
            </div>
                <div class="col-md-4">
                    <div class="card card-body">
                        <header class="clearfix">
                            <div class="title-text-2">
                                <span class="h5">{{trans('common.search_program')}}</span>
                            </div>
                        </header>
                        <form method="post" action="{{url('/home/package/search')}}">
                            {{csrf_field()}}

                            <div class="form-row">
                                <div class="col-md-6 col-sm-12">{{trans('common.destination')}}<br>
                                    <select class="form-control" name="search_country">
                                        <option value="">{{trans('common.destination')}}</option>
                                        @foreach( \App\PackageCountry::where('packageDateStart','>=',date('Y-m-d'))->where('language_code',Session::get('language'))->get() as $rows)
                                            <?php
                                            $countCountry=DB::table('package_details as a')
                                                ->join('package_tourin as d','d.packageID','=','a.packageID')
                                                ->where('a.closing_date','>',date('Y-m-d'))
                                                ->where('a.status','Y')
                                                ->where('d.CountryCode',$rows->country_id)
                                                ->groupby('a.packageID')
                                                ->first();
                                            ?>
                                            @if($countCountry)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    {{--<input class="form-control mb-2" name="search" placeholder="{{trans('common.search_city_country')}}" type="text">--}}
                                </div>
                                <div class="col-md-6 col-sm-12">{{trans('common.time_zone')}}<br>
                                    <input class="form-control mb-2" name="travel_date" value="02/05/2018" type="date">
                                </div> <!-- col.// -->
                                <div class="col-md-12 col-sm-12 filter-content collapse show" id="collapse33">{{trans('common.price')}}
                                    <br>
                                    <input type="range" class="custom-range" min="1000" max="100000" step="500" id="custom_range" oninput="max_price.value = custom_range.value">
                                    <div class="form-row">
                                        <div class="form-group col-md-6 col-sm-6">
                                            <label>{{trans('common.low')}}</label>
                                            <input class="form-control text-center" name="min_price" placeholder="1000" value="1000" type="number">
                                        </div>
                                        <div class="form-group text-right col-md-6 col-sm-6">
                                            <label>{{trans('common.high')}}</label>
                                            <input class="form-control text-center" id="max_price" name="max_price" value="100000" placeholder="100,000" type="number">
                                        </div>
                                    </div> <!-- form-row.// -->
                                    <button type="submit"  class="btn btn-block btn-outline-primary">{{trans('common.data')}}</button>
                                </div> <!-- collapse .// -->
                            </div>
                        </form>
                    </div>

                </div>

            @endif
        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

