@extends('layouts.package.master')

@section('program-highlight')
    <style>
        .owl-theme .owl-dots .owl-dot span {
            width: 10px;
            height: 10px;
            margin: 5px 7px;
            background: #D6D6D6;
            display: block;
            -webkit-backface-visibility: visible;
            transition: opacity .2s ease;
            border-radius: 30px;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhai&display=swap" rel="stylesheet">
    <style>
        .price-wrap{
            font-family: 'Baloo Bhai', cursive;
            color: #00A000;
        }
        .person{
            font-size: 14px;
            font-family: 'Baloo Bhai', cursive;
            color: #0a0a0a;
        }
    </style>
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <header class="clearfix">
                        <div class="title-text-2">
                            <span class="h5">{{trans('package.program_highlight')}}</span>
                        </div>
                    </header>
                    <div class="row row-sm">
                        <div class="col-md-12">
                        @if($PackageTour->count())
                            <!-- ============== owl slide items 2  ============= -->
                            <div class="owl-carousel owl-init slide-items" id="slide_custom_nav-home" data-custom-nav="custom-nav-first" data-items="1" data-margin="20" data-dots="true" data-nav="true">


                                    @foreach($PackageTour as $rows)

                                        <?php $url_link='home/details/'.$rows->packageID;?>
                                        <?php

                                        $current=\App\Currency::where('currency_code',$rows->packageCurrency)->first();
                                        $str=DB::table('package_details')
                                            ->where('packageID',$rows->packageID)
                                            ->where('packageDateStart','>',date('Y-m-d'))
                                            ->where('status','Y')
                                            ->orderby('packageDateStart','asc');
                                        $Detail=$str->first();
                                        $Details=$str->get();

                                        $Price=DB::table('package_details_sub')
                                            ->where('packageID',$rows->packageID)
                                            ->where('packageDescID',$Detail->packageDescID)
                                            ->where('status','Y')
                                            ->orderby('price_system_fees','asc')
                                            ->first();
                                        //dd($Price);
                                        $Price_now=0;
                                        $Price_up=0;
                                        $Price_down=0;
                                        //****************** Update Status if promotion finish **********************//
                                        DB::table('package_promotion')
                                            ->where('promotion_date_end','<',date('Y-m-d H:i:s'))
                                            ->where('promotion_status','Y')
                                            ->where('promotion_operator','Between')
                                            ->update(['promotion_status'=>'N']);
                                        //****************** Update Status if promotion finish **********************//

                                        $data_target=null;
                                        $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                            //->where('promotion_date_start','>',date('Y-m-d'))
                                            ->orderby('promotion_date_start','asc')
                                            ->first();
                                        //  dd($promotion);

                                        $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                                        $promotion_title=null;$every_booking=0;
                                        // dd($pricePro);
                                        $unit="";


                                        if($promotion){
                                            $every_booking=$promotion->every_booking;
                                            $promotion_title='Between';
                                            if($promotion->promotion_operator=='Between'){
                                                if($promotion->promotion_date_start<=date('Y-m-d') && $promotion->promotion_date_end>=date('Y-m-d')){
                                                    if($promotion->promotion_operator2=='up'){
                                                        if($promotion->promotion_unit=='%'){
                                                            if($promotion->promotion_value>0){
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_up=$rowPrice->price_system_fees*$promotion->promotion_value/100;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees+$Price_up]);
                                                                }
                                                            }
                                                            $unit=$promotion->promotion_unit;
                                                        }else{
                                                            foreach ($pricePro as $rowPrice){
                                                                $Price_up=$promotion->promotion_value;
                                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees+$Price_up]);
                                                            }
                                                        }
                                                    }else{ // promotion down
                                                        if($promotion->promotion_unit=='%'){
                                                            if($promotion->promotion_value>0){
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_down=$Price->price_system_fees*$promotion->promotion_value/100;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees-$Price_down]);
                                                                }
                                                            }
                                                            $unit=$promotion->promotion_unit;
                                                        }else{
                                                            foreach ($pricePro as $rowPrice){
                                                                $Price_down=$promotion->promotion_value;
                                                                DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->price_system_fees-$Price_down]);
                                                            }
                                                        }

                                                    }
                                                }

                                                $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));
                                            }else{
                                                $promotion_title='Mod';
                                                // dd($rows->packageID.' '.$Price->packageDescID);
                                                $booking=DB::table('package_bookings as a')
                                                    ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                                                    ->where('b.package_id',$rows->packageID)
                                                    ->where('b.package_detail_id',$Price->packageDescID)
                                                    ->sum('b.number_of_person');
                                                // dd($booking);
                                                if($booking>0){ // Check booking
                                                    if($booking % $promotion->every_booking==1){ // every booking
                                                        //   dd('test'.$promotion->every_booking);
                                                        if($booking>$promotion->every_booking){

                                                            if($promotion->promotion_operator2=='up'){
                                                                if($promotion->promotion_unit=='%'){
                                                                    if($promotion->promotion_value>0){
                                                                        foreach ($pricePro as $rowPrice){
                                                                            $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                        }
                                                                    }
                                                                    $unit=$promotion->promotion_unit;
                                                                }else{
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_up=$promotion->promotion_value;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                    }

                                                                }
                                                                /// $Price_by_promotion+=$Price_up;
                                                            }else{
                                                                if($promotion->promotion_unit=='%'){
                                                                    if($promotion->promotion_value>0){
                                                                        foreach ($pricePro as $rowPrice){
                                                                            $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                                        }
                                                                    }
                                                                    $unit=$promotion->promotion_unit;
                                                                }else{
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_down=$promotion->promotion_value;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
//                                                    dd($Price_now);
                                            }
                                        }

                                        //   dd($data_target);

                                        //                                        $Price=DB::table('package_details_sub')
                                        //                                            ->where('packageID',$rows->packageID)
                                        //                                            ->where('packageID',$Price->packageID)
                                        //                                            ->where('status','Y')
                                        //                                            ->orderby('price_system_fees','asc')
                                        //                                            ->first();

                                        ?>
                                        <div class="item-slide">
                                            <div class="row row-sm">
                                                <div class="col-md-8">
                                                    <figure class="card card-product">
                                                        @if($promotion_title=='Between')

                                                            @if($data_target!=null)
                                                                <span class="badge-new">
                                                                <div id="clockdiv-{{$rows->ID}}">
                                                                    <span class="days"></span> {{trans('common.day')}}
                                                                    <span class="hours"></span> {{trans('common.hour')}}
                                                                    <span class="minutes"></span> {{trans('common.minute')}}
                                                                    <span class="seconds"></span> {{trans('common.seconds')}} {{trans('common.last_one')}}
                                                                </div>
                                                                </span>
                                                                <script language="JavaScript">
                                                                    var deadline = new Date('{{$data_target}}');
                                                                    initializeClock('clockdiv-{{$rows->ID}}', deadline);
                                                                </script>
                                                                <span class="badge-offer"><b> -{{$promotion->promotion_value}}{{$unit==''?$current->currency_symbol:$unit}} </b></span>
                                                            @endif
                                                        @elseif($promotion_title=='Mod')
                                                            <span class="badge-new">
                                                             <?php
                                                                $bookings=\App\Bookings::where('package_id',$rows->packageID)
                                                                    ->where('package_detail_id',$Price->packageDescID)
                                                                    ->sum('number_of_person');
                                                                if($bookings>0){
                                                                    $every_booking=$every_booking-$bookings % $every_booking;
                                                                }
                                                                ?>
                                                                {{trans('package.this_price_is_only')}} {{$every_booking}} {{trans('common.seat')}}
                                                             </span>
                                                        @endif
                                                        <div class="img-wrap3">
                                                            <a href="{{url('home/details/'.$rows->packageID)}}">
                                                                <img src="{{url('package/tour/mid/'.$rows->Image)}}">
                                                            </a>
                                                        </div>
                                                    </figure>
                                                </div>
                                                <div class="col-md-4">
                                                    <p class="bottom-wrap">
                                                        <figcaption class="info-wrap-2">
                                                            <h5 class="title">
                                                                <a href="{{url('home/details/'.$rows->packageID)}}">{{$rows->packageName}}</a>
                                                                {{--@if(Auth::check())--}}
                                                                {{--<a href="{{url(Auth::user()->name.'/details/'.$rows->packageID)}}">{{$rows->packageName}}</a>--}}
                                                                {{--@else--}}
                                                                {{----}}
                                                                {{--@endif--}}
                                                            </h5>
                                                            <?php

                                                            $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                                                            //                                                            dd($timeline);
                                                            ?>
                                                            <div> {{trans('package.package_tour_by')}} <a href="{{url('/home/package/agent/'.$rows->timeline_id)}}">{{$timeline->name}}</a></div>
                                                            <div class="rating-wrap">
                                                                <a href="{{url('home/details/'.$rows->packageID)}}">
                                                                    <ul class="rating-stars">
                                                                        <li style="width:80%" class="stars-active">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                        </li>
                                                                        <li>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="label-rating">132 {{trans('common.review')}}</div>
                                                                </a>

                                                                <div class="label-rating">
                                                                    <a href="{{url('package/booking/'.$rows->packageID)}}"><strong>{{\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')}}</strong> {{trans('common.booking')}} </a>
                                                                </div>
                                                            </div> <!-- rating-wrap.// -->
                                                        </figcaption>
                                                    <div class="price-wrap">
                                                    <span class="price-new">
                                                        <a href="{{url('home/details/'.$rows->packageID)}}">
                                                            {{$Details->count()}} {{trans('common.time_zone')}}
                                                            <?php $i=0;?>
                                                            @foreach($Details as $detail)
                                                                <?php
                                                                $i++;
                                                                $st=explode('-',$detail->packageDateStart);
                                                                $end=explode('-',$detail->packageDateEnd);
                                                                if($st[1]==$end[1]){
                                                                    $date=\Date::parse($detail->packageDateStart);
                                                                    $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                    // dd($end[0]);
                                                                }else{
                                                                    $date=\Date::parse($detail->packageDateStart);
                                                                    $date1=\Date::parse($detail->packageDateEnd);
                                                                    $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                                }
                                                                ?>
                                                                {{$package_date}},
                                                                <?php if($i==2){break;}?>
                                                            @endforeach
                                                        </a>
                                                    </span>
                                                    </div> <!-- price-wrap.// -->
                                                    <?php
                                                    $price_promotion=floatval($Price->Price_by_promotion);
                                                    $price_system=floatval($Price->price_system_fees);

                                                    ?>
                                                    @if($promotion)

                                                        <div class="price-wrap h4">

                                                            @if($price_promotion!=$price_system)

                                                                @if($price_system>$price_promotion)
                                                                    <del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del>  <span class="price-discount">{{$current->currency_symbol.number_format($price_promotion)}}</span> <BR>
                                                                    <span class="price-save">
                                                                        {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                        {{$current->currency_symbol.number_format($price_system-$price_promotion)}}
                                                                    </span>
                                                                @else
                                                                    <span class="price-discount">{{$current->currency_symbol.number_format($price_promotion)}}</span>
                                                                    <span class="price-save">
                                                                        {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                        {{$current->currency_symbol.number_format($price_promotion-$price_system)}}
                                                                    </span>
                                                                @endif
                                                            @endif
                                                            <span class="person">/ {{trans('common.per_person')}}</span>
                                                            <BR>
                                                            <small class="person">{{$promotion->promotion_title}}</small>
                                                        </div> <!-- price-wrap.// -->
                                                    @else
                                                        <div class="price-wrap h4">
                                                            <span class="price-discount">{{$current->currency_symbol.number_format($price_system)}}</span> <span class="person">/ {{trans('common.per_person')}}</span>
                                                        </div>
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class=" table-wrapper-scroll-y scrollbar" id="style-1">
                                                                <table class="table table-hover fixed_header">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>{{trans('common.travel')}}</th>
                                                                        <th>{{trans('common.price')}}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    @foreach($Details as $detail)
                                                                        @if(Auth::check())
                                                                            <?php $url_link=Auth::user()->name.'/details/'.$detail->packageID;?>
                                                                        @else
                                                                            <?php $url_link='home/details/'.$detail->packageID;?>
                                                                        @endif
                                                                        <?php
                                                                        $st=explode('-',$detail->packageDateStart);
                                                                        $end=explode('-',$detail->packageDateEnd);

                                                                        if($st[1]==$end[1]){
                                                                            $date=\Date::parse($detail->packageDateStart);
                                                                            $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                            // dd($end[0]);
                                                                        }else{
                                                                            $date=\Date::parse($detail->packageDateStart);
                                                                            $date1=\Date::parse($detail->packageDateEnd);
                                                                            $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                                        }

                                                                        $Category=DB::table('package_details_sub')
                                                                            ->where('packageID',$rows->packageID)
                                                                            ->where('packageDescID',$detail->packageDescID)
                                                                            ->get();

                                                                        $promotion=\App\PackagePromotion::where('packageDescID',$detail->packageDescID)->active()
                                                                            ->orderby('promotion_date_start','asc')
                                                                            ->first();

                                                                        // dd($promotion);
                                                                        $data_target=null;
                                                                        if($promotion && $promotion->promotion_operator!='Mod'){
                                                                            $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));
                                                                        }

                                                                        $booking=DB::table('package_booking_details')
                                                                            ->where('package_id',$rows->packageID)
                                                                            ->where('package_detail_id',$detail->packageDescID)
                                                                            ->sum('number_of_person');

                                                                        $person_booking=$detail->NumberOfPeople;

                                                                        if($booking){
                                                                            $person_booking=$detail->NumberOfPeople-$booking;
                                                                        }

                                                                        $price_promotion=floatval($Price->Price_by_promotion);
                                                                        $price_system=floatval($Price->price_system_fees);

                                                                        ?>


                                                                        <form class="form-booking" method="post" action="{{action('Package\OrderTourController@store_order')}}">
                                                                            <input type="hidden" name="id" value="{{$detail->packageDescID}}">
                                                                            {{csrf_field()}}
                                                                            <tr>
                                                                                <td class="date-text">{{$package_date}}<br>
                                                                                    <var class="price">
                                                                                        <select class="form-control form-control-sm tour-type" data-id="{{$detail->packageDescID}}" name="tour_type" id="tour_type" style="width:90%;" required>
                                                                                            <span class="form-check-label"></span>
                                                                                            <option value=""> {{trans('common.category')}}</option>
                                                                                            @foreach($Category as $cate)
                                                                                                <option value="{{$cate->psub_id}}"> {{$cate->TourType}}</option>
                                                                                            @endforeach
                                                                                            {{--<option> ผู้ใหญ่รวมตั๋ว นอนเดี่ยว xxxxxxxxxxxxxxxx</option>--}}
                                                                                        </select>
                                                                                    </var>
                                                                                    <var class="price">
                                                                                        <select class="form-control form-control-sm" name="number_of_person" id="number_of_person" style="width:90%;" required>
                                                                                            <span class="form-check-label"></span>
                                                                                            <option value=""> {{trans('common.amount')}}</option>
                                                                                            @for($i=1;$i<=10;$i++)
                                                                                                <option value="{{$i}}"> {{$i}} {{trans('common.person')}}</option>
                                                                                            @endfor
                                                                                        </select>
                                                                                    </var>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="show_price-{{$detail->packageDescID}}">
                                                                                        @if($promotion)
                                                                                            @if($price_promotion==$price_system)
                                                                                                <span class="num h5 text-warning">{{$current->currency_symbol.number_format($price_promotion)}} </span> <span class="person">/ {{trans('common.per_person')}}</span>
                                                                                            @else
                                                                                                @if($price_system>$price_promotion)
                                                                                                    <del>{{$current->currency_symbol.number_format($price_system)}}</del> - &nbsp;{{$current->currency_symbol.number_format($price_promotion)}} <span class="person">/ {{trans('common.per_person')}}</span><br>
                                                                                                    <span class="num h5 text-warning">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}} </span>
                                                                                                    <span class="price-save">{{trans('common.saving')}} {{$current->currency_symbol.number_format($price_system-$price_promotion)}}</span><br>
                                                                                                @else
                                                                                                    {{$current->currency_symbol.number_format($price_promotion)}} <span class="person">/ {{trans('common.per_person')}}</span><br>
                                                                                                    <span class="num h5 text-warning">{{$current->currency_symbol.number_format($price_promotion)}} </span>
                                                                                                    <span class="price-save">
                                                                                                        @if($promotion)
                                                                                                            {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                                                        @endif
                                                                                                            {{$current->currency_symbol.number_format($price_promotion-$price_system)}}
                                                                                                     </span>
                                                                                                    <br>
                                                                                                @endif

                                                                                            @endif

                                                                                            @if($data_target != null)
                                                                                                <span class="price-save-date">
                                                                                                      <span id="clockdiv{{$detail->packageDescID}}">({{trans('package.this_price')}}
                                                                                                          <span class="days"></span> {{trans('common.day')}}
                                                                                                          <span class="hours"></span>:
                                                                                                         <span class="minutes"></span>:
                                                                                                         <span class="seconds"></span>, {{$detail->NumberOfPeople}} {{trans('package.the_last_place_only')}})
                                                                                                      </span>
                                                                                                    </span>
                                                                                                <script language="JavaScript">
                                                                                                    initializeClock('clockdiv{{$detail->packageDescID}}', new Date('{{$data_target}}'));
                                                                                                </script>
                                                                                            @else
                                                                                                <span class="price-save-date">
                                                                                                    ({{trans('package.this_price_is_only')}} {{$person_booking}} {{trans('package.the_last_place_only')}})
                                                                                                    </span>
                                                                                            @endif
                                                                                        @else
                                                                                            <span class="num h4 text-warning">{{$current->currency_symbol.number_format($price_system)}} </span> <span class="person">/ {{trans('common.per_person')}}</span>
                                                                                        @endif
                                                                                    </div>

                                                                                    <button type="submit" name="submit_cart" value="Y" class="btn btn-sm btn-outline-primary">
                                                                                        <i class="fas fa-shopping-cart"></i> {{trans('common.addtocart')}}
                                                                                    </button>
                                                                                    <button type="submit" name="submit_order" value="Y" class="btn btn-sm btn-primary"> {{trans('common.booking')}} </button>
                                                                                </td>
                                                                            </tr>
                                                                        </form>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div> <!-- col.// -->
                                                    </div> <!-- row.// -->
                                                </div> <!-- bottom-wrap.// -->
                                                <hr>
                                            </div>
                                        </div>


                                    @endforeach


                            </div>
                            <!-- ============== owl slide items 2 .end // ============= -->
                            @else
                                <h5 class="text-danger">{{trans('common.package_not_found')}}</h5>
                            @endif
                        </div> <!-- col.// -->
                        <!--</aside>-->
                    </div> <!-- row.// -->
                </div> <!-- card-body .// -->
            </div> <!-- card.// -->

        </div> <!-- container .//  -->
    </section>
@endsection


@section('program-latest')
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-md-12">
                            <header class="clearfix">
                                <div class="title-text-2">
                                    <span class="h5">{{trans('common.program_latest')}}</span>
                                    <div class="btn-group btn-group-sm float-right">
                                        <button type="button" class="custom-nav-first owl-custom-prev btn btn-secondary"> < </button>
                                        <button type="button" class="custom-nav-first owl-custom-next btn btn-secondary"> > </button>
                                    </div>
                                </div>
                            </header>
                            @if($PackageTour->count())
                            <!-- ============== owl slide items 2  ============= -->
                            <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-custom-nav="custom-nav-first" data-items="3" data-margin="20" data-dots="true" data-nav="true">

                                @foreach($PackageTour as $rows)

                                    <?php $url_link='home/details/'.$rows->packageID;?>

                                    <?php
                                    $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                                    $current=\App\Currency::where('currency_code',$rows->packageCurrency)->first();
                                    $Details=DB::table('package_details')
                                        ->where('packageID',$rows->packageID)
                                        ->where('packageDateStart','>',date('Y-m-d'))
                                        ->where('status','Y')
                                        ->orderby('packageDateStart','asc')
                                        ->get();
                                    //  dd($Details);
                                    $Price=DB::table('package_details_sub as a')
                                            ->join('package_details as b','b.packageDescID','=','a.packageDescID')
                                            ->where('a.packageID',$rows->packageID)
                                            ->where('b.status','Y')
                                            ->where('a.status','Y')
                                            ->orderby('a.PriceSale','desc')
                                            ->first();

                                    $data_target=null;
                                    $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                        ->orderby('promotion_date_start','asc')
                                        ->first();

                                    $promotion_title=null;$every_booking=0;
                                    if($promotion){
                                        $every_booking=$promotion->every_booking;
                                        if($promotion->promotion_operator=='Between'){
                                            $promotion_title='Between';
                                            if($promotion->promotion_operator2=='up'){
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_up=$rowPrice->Price_by_promotion*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_up=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                    }

                                                }

                                            }else{ // promotion down
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_down=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                    }
                                                }

                                            }
                                            $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));

                                        }else{
                                            $promotion_title='Mod';
                                            //  dd($rows->packageID.' '.$Price->packageDescID);
                                            $booking=DB::table('package_bookings as a')
                                                ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                                                ->where('a.packageID',$rows->packageID)
                                                ->where('a.packageDescID',$Price->packageDescID)
//                                                ->first();
                                                ->sum('b.number_of_person');
//

                                            if($booking>0){ // Check booking

                                                if($booking % $promotion->every_booking==1){ // every booking
                                                    if($booking>$promotion->every_booking){
                                                        if($promotion->promotion_operator2=='up'){
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_up=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                }
                                                            }
                                                            /// $Price_by_promotion+=$Price_up;
                                                        }else{
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_down=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                                                }
                                                            }
                                                        }
                                                    }

                                                }

                                            }
//                                                    dd($Price_now);
                                        }
                                    }
                                    ?>
                                    <div class="item-slide">
                                        <figure class="card card-product">
                                            @if($promotion_title=='Between')
                                                @if($data_target!=null)
                                                    <span class="badge-new">
                                                        <div id="clockdiv">
                                                            <span class="days"></span> {{trans('common.day')}}
                                                            <span class="hours"></span> {{trans('common.hour')}}
                                                            <span class="minutes"></span> {{trans('common.minute')}}
                                                            <span class="seconds"></span> {{trans('common.seconds')}} {{trans('common.last_one')}}
                                                        </div>
                                                    </span>
                                                    <script language="JavaScript">
                                                        var deadline = new Date('{{$data_target}}');
                                                        initializeClock('clockdiv', deadline);
                                                    </script>

                                                    <span class="badge-offer"><b> {{$Price->packageDescID}} </b></span>
                                                @endif
                                            @elseif($promotion_title=='Mod')
                                                <span class="badge-new">
                                                    <?php
                                                    $bookings=\App\Bookings::where('package_id',$rows->packageID)
                                                        ->where('package_detail_id',$Price->packageDescID)
                                                        ->sum('number_of_person');
                                                    if($bookings>0){
                                                        $every_booking=$every_booking-$bookings % $every_booking;
                                                    }
                                                    ?>
                                                        {{trans('package.this_price_is_only')}} {{$every_booking}} {{trans('common.seat')}}
                                                </span>
                                            @endif

                                            {{--<span class="badge-offer"><b> -50% </b></span>--}}

                                                <div class="img-wrap">
                                                    <a href="{{url($url_link)}}">
                                                        <img src="{{url('package/tour/small/'.$rows->Image)}}">
                                                    </a>
                                                </div>
                                                <div class="col-lg-12 text-right">
                                                    <small>
                                                        {{trans('package.package_tour_by')}}
                                                        <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a>
                                                    </small>
                                                </div>

                                                <figcaption class="info-wrap-2">
                                                <h5 class="title">
                                                    <a href="{{url($url_link)}}">{{$rows->packageName}}</a>
                                                </h5>
                                                <div class="rating-wrap">
                                                    <a href="#">
                                                        <ul class="rating-stars">
                                                            <li style="width:80%" class="stars-active">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </li>
                                                            <li>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </li>
                                                        </ul>
                                                        <div class="label-rating">132 {{trans('common.review')}}</div>
                                                    </a>
                                                    @if(\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')>0)
                                                        <div class="label-rating">
                                                            <strong style="color:red">
                                                                {{\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')}}</strong>
                                                            {{trans('common.booking')}}
                                                        </div>
                                                    @endif

                                                </div> <!-- rating-wrap.// -->
                                            </figcaption>
                                            <div class="bottom-wrap">
                                                <div class="price-wrap">
                                                <span class="price-new">
                                                     <a href="{{url('package/details/'.$rows->packageID)}}">
                                                            {{$Details->count()}} {{trans('common.time_zone')}}
                                                         <?php $i=0;?>
                                                         @foreach($Details as $detail)
                                                             <?php
                                                             $i++;
                                                             $st=explode('-',$detail->packageDateStart);
                                                             $end=explode('-',$detail->packageDateEnd);
                                                             if($st[1]==$end[1]){
                                                                 $date=\Date::parse($detail->packageDateStart);
                                                                 $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                 // dd($end[0]);
                                                             }else{
                                                                 $date=\Date::parse($detail->packageDateStart);
                                                                 $date1=\Date::parse($detail->packageDateEnd);
                                                                 $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                             }
                                                             ?>
                                                             {{$package_date}},
                                                                 <?php if($i==2){break;}?>
                                                         @endforeach
                                                        </a>
                                                </span>
                                                </div> <!-- price-wrap.// -->
                                                <a href="{{url('/booking/c/'.$Price->packageDescID)}}" class="btn btn-sm btn-primary float-right">{{trans('common.booking')}}</a>
                                                <div class="price-wrap h4 text-center">
                                                    {{--<span class="price-discount">฿24,999</span> <del class="price-old">฿26,999</del><br>--}}
                                                    {{--<span class="price-save">ประหยัด ฿2,999</span>--}}
                                                    @if($promotion)
                                                        <span class="price-discount">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}}</span>
                                                        @if($Price->Price_by_promotion!=$Price->price_system_fees)
                                                            <del class="price-old">{{$current->currency_symbol.number_format($Price->price_system_fees)}}</del><br>
                                                            <span class="price-save">
                                                            {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                {{$current->currency_symbol.number_format($Price->price_system_fees)}}
                                                        </span>
                                                        @endif
                                                    @else
                                                        <span class="price-discount">{{$current->currency_symbol.number_format($Price->price_system_fees)}} / {{trans('common.per_person')}}</span>
                                                    @endif
                                                </div>
                                                <!-- price-wrap.// -->
                                            </div> <!-- bottom-wrap.// -->
                                        </figure>
                                    </div>
                                @endforeach
                            </div>
                            <!-- ============== owl slide items 2 .end // ============= -->
    @else
        <h5 class="text-danger">{{trans('common.package_not_found')}}</h5>
    @endif
                        </div> <!-- col.// -->
                    </div> <!-- row.// -->
                </div> <!-- card-body .// -->
            </div> <!-- card.// -->

        </div> <!-- container .//  -->
    </section>


@endsection

@section('program-country')
    <section class="section-request bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <header class="clearfix">
                        <div class="title-text-2">
                            <span class="h5">{{trans('package.tour_programs_in_various_countries')}}</span>
                        </div>
                    </header>
                    <div class="row-sm">
                        @foreach( \App\PackageCountry::where('packageDateStart','>',date('Y-m-d'))->where('language_code',Session::get('language'))->get() as $rows)
                            <?php
                            $countTime=DB::table('package_details as a')
                                ->join('package_tourin as d','d.packageID','=','a.packageID')
                                ->where('a.status','Y')
                                ->where('d.CountryCode',$rows->country_id)
                                ->get();

                            $countCountry=DB::table('package_details as a')
                                ->join('package_tourin as d','d.packageID','=','a.packageID')
                                ->where('a.status','Y')
                                ->where('d.CountryCode',$rows->country_id)
                                ->groupby('a.packageID')
                                ->get();

                            $country=$rows->country_id;
                            $PackageTourOne = DB::table('package_tour as a')
                                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
//                                ->join('users', 'a.packageBy', '=', 'users.id')
//                                ->join('users_info', 'users_info.UserID', '=', 'a.packageBy')
                                ->join('package_details as d','d.packageID','=','a.packageID')
                                ->join('package_details_sub as e','e.packageDescID','=','d.packageDescID')
                                ->select( 'a.*','d.packageDateStart','d.packageDateEnd','e.TourType','e.PriceSale', 'b.packageName','b.packageHighlight')
                                ->where('b.LanguageCode', Session::get('language'))
                                ->whereIn('a.packageID',function ($query) use($country) {
                                    $query->select('packageID')->from('package_tourin')
                                        ->where('CountryCode',$country);
                                })
//                                ->where('d.country_id',$rows->country_id)
                                ->where('b.Status_Info','P')
                                ->where('d.closing_date','>',date('Y-m-d'))
                                ->where('d.status','Y')
                                ->where('e.status','Y')
                                ->groupby('a.packageID')
                                ->orderby('e.PriceSale', 'asc')
                                ->orderby('e.psub_id', 'asc')
                                ->first();
                            $Image='default-add.jpg';
                            if($PackageTourOne){
                                $Image=$PackageTourOne->Image;
                            }

                            ?>


                            @if($PackageTourOne)
                                <div class="col-md-2 col-sm-6">
                                    <figure class="card card-product">
                                        <div class="img-wrap2">
                                            <a href="{{url('home/package/country/'.$rows->country_id)}}" target="_blank">
                                                <img src="{{url('package/tour/small/'.$Image)}}" style="max-height: 105px; width: 100%">
                                            </a>
                                        </div>
                                        <figcaption class="info-wrap">
                                            <h6 class="title-center">
                                                <a href="{{url('home/package/country/'.$rows->country_id)}}"><span>{{$rows->country}}</span></a>
                                            </h6>
                                            <div class="text-center">
                                                <small class="text-center">({{$countCountry->count()}} {{trans('common.program')}}, {{$countTime->count()}} {{trans('common.duration')}})</small>
                                            </div> <!-- price-wrap.// -->
                                        </figcaption>
                                    </figure> <!-- card // -->
                                </div> <!-- col // -->
                            @endif
                        @endforeach


                    </div> <!-- row.// -->
                </div>
            </div>
        </div><!-- container // -->
    </section>
@endsection

@section('program-list-time')
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <header class="clearfix">
                                <div class="title-text-2">
                                    <span class="h5">{{trans('package.recommended_program')}}</span>
                                </div>
                            </header>

                            <ul class="list-icon row">
                                @foreach( \App\PackageCountry::where('packageDateStart','>',date('Y-m-d'))->where('language_code',Session::get('language'))->get() as $rows)
                                    <?php
                                    $countTime=DB::table('package_details as a')
                                        ->join('package_tourin as d','d.packageID','=','a.packageID')
                                        ->where('a.status','Y')
                                        ->where('d.CountryCode',$rows->country_id)
                                        ->get();

                                    $countCountry=DB::table('package_details as a')
                                        ->join('package_tourin as d','d.packageID','=','a.packageID')
                                        ->where('a.closing_date','>',date('Y-m-d'))
                                        ->where('a.status','Y')
                                        ->where('d.CountryCode',$rows->country_id)
                                        ->groupby('a.packageID')
                                        ->get();

                                    //                                    $countCountry=DB::table('package_details')
                                    //                                        ->where('status','Y')
                                    //                                        ->where('country_id',$rows->country_id)
                                    //                                        ->groupby('packageID')
                                    //                                        ->get();

                                    ?>
                                @if($countCountry->count())
                                    <li class="col-md-4">
                                        <a href="{{url('home/package/country/'.$rows->country_id)}}">
                                            {{--<img src="{{asset('theme/default/assets/flags/'.strtolower($rows->country_iso_code).'.svg')}}">--}}
                                            <img src="{{asset('package/images/icons/flag-'.strtolower($rows->country_iso_code).'.png')}}">
                                            <span>{{$rows->country}} ({{$countCountry->count()}} {{trans('common.program')}}, {{$countTime->count()}} {{trans('common.duration')}})
                                        </span>
                                        </a>
                                    </li>
                                        @endif
                                @endforeach
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-in.png')}}"><span>อินเดีย (10, 2 ช่วงเวลา)</span></a></li>--}}

                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-kr.png')}}"><span>เกาหลีใต้ (10, 6 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-tr.png')}}"><span>อินโดนีเซีย (1, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-kr.png')}}"><span>แคนนาดา (5, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-usa.png')}}"><span>อินโดนีเซีย (1, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-in.png')}}"><span>แคนนาดา (5, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-usa.png')}}"><span>สหรัฐอเมริกา (5, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-in.png')}}"><span>อินเดีย (10, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-tr.png')}}"><span>ตุรกี (2, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-kr.png')}}"><span>เกาหลีใต้ (10, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-tr.png')}}"><span>อินโดนีเซีย (1, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-kr.png')}}"><span>แคนนาดา (5, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-usa.png')}}"><span>อินโดนีเซีย (1, 10 ช่วงเวลา)</span></a></li>--}}
                                {{--<li class="col-md-3"><a href="listing-grid.html"><img src="{{asset('package/images/icons/flag-in.png')}}"><span>แคนนาดา (5, 10 ช่วงเวลา)</span></a></li>--}}
                            </ul>
                        </div> <!-- col // -->
                    </div><!-- row // -->
                </div>
            </div>

        </div><!-- container // -->
    </section>
@endsection

