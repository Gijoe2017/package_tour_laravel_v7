@extends('layouts.package.master-order')
@section('program-highlight')
    <!-- CSS -->
    {{--<link href="{{asset('/member/dropzone/style.css')}}" rel="stylesheet" type="text/css">--}}
    <link href="{{asset('/member/dropzone/dropzone.css')}}" rel="stylesheet" type="text/css">

    <!-- Script -->
    <script src='{{asset('/member/dropzone/jquery-3.2.1.min.js')}}'></script>
    <script src="{{asset('/member/dropzone/dropzone.js')}}" type="text/javascript"></script>


    <section class="invoice">
        <div class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">Documents <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/member/document/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.Upload')}}</a> </span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                        <span class="step-number">Step 6 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                        <div class='content'>
                            <form action="{{url('/member/upload/docs')}}" class="dropzone" id="myAwesomeDropzone">
                            {{csrf_field()}}
                            </form>
                        </div>

                     <div class="col-md-12">
                        @foreach($Files as $rows)

                        @endforeach
                     </div>

                    <!-- Script -->
                    <script type='text/javascript'>
                        Dropzone.autoDiscover = false;
                        $(".dropzone").dropzone({
                            addRemoveLinks: true,
                            acceptedFiles: "image/*,application/pdf",
                            removedfile: function(file) {
                               var name = file.name;
                                $.ajax({
                                    type: 'POST',
                                    url: '{{url('/member/upload/docs/remove')}}',
                                    data: {name: name,request: 2,'_token':'{{csrf_token()}}'},
                                    sucess: function(data){
                                        console.log('success: ' + data);
                                    }
                                });
                                var _ref;
                                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                            }
                        });


                    </script>
                </div>
                <div class="box-footer">
                Document list
                </div>
                </div>

                </div>
        </div>
    </section>
@endsection
