@extends('layouts.package.master')



@section('program-highlight')

    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">

                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-md-12">
                            <header class="clearfix">
                                <div class="title-text-2">
                                    <span class="h5">{{trans('common.package')}}</span>
                                    {{--<div class="btn-group btn-group-sm float-right">--}}
                                    {{--<button type="button" class="custom-nav-first owl-custom-prev btn btn-secondary"> < </button>--}}
                                    {{--<button type="button" class="custom-nav-first owl-custom-next btn btn-secondary"> > </button>--}}
                                    {{--</div>--}}
                                </div>
                            </header>
                            <!-- ============== owl slide items 2  ============= -->
                            <div class="row" >

                                {{--<div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-custom-nav="custom-nav-first" data-items="4" data-margin="20" data-dots="true" data-nav="true">--}}
                                @foreach($PackageTour as $rows)

                                    <?php $url_link='home/details/'.$rows->packageID;?>

                                    <?php

                                    $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                                    $current=\App\Currency::where('currency_code',$rows->packageCurrency)->first();
                                    $Details=DB::table('package_details')
                                        ->where('packageID',$rows->packageID)
                                        ->where('packageDateStart','>',date('Y-m-d'))
                                        ->where('status','Y')
                                        ->orderby('packageDateStart','asc')
                                        ->get();
                                    // dd($Details);
                                    $Price=DB::table('package_details_sub as a')
                                        ->join('package_details as b','b.packageDescID','=','a.packageDescID')
                                        ->where('a.packageID',$rows->packageID)
                                        ->where('a.packageDescID',$Details[0]->packageDescID)
                                        ->where('b.status','Y')
                                        ->where('a.status','Y')
                                        ->orderby('a.price_system_fees','asc')
                                        ->first();
                                   // dd($Price);
                                    $data_target=null;
                                    $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                        ->orderby('promotion_date_start','asc')
                                        ->first();

                                    $promotion_title=null;$every_booking=0;
                                    if($promotion){
                                        $every_booking=$promotion->every_booking;
                                        if($promotion->promotion_operator=='Between'){
                                            $promotion_title='Between';
                                            if($promotion->promotion_operator2=='up'){
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_up=$rowPrice->Price_by_promotion*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_up=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                    }

                                                }

                                            }else{ // promotion down
                                                if($promotion->promotion_unit=='%'){
                                                    if($promotion->promotion_value>0){
                                                        foreach ($pricePro as $rowPrice){
                                                            $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                            DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                        }
                                                    }
                                                }else{
                                                    foreach ($pricePro as $rowPrice){
                                                        $Price_down=$promotion->promotion_value;
                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                    }
                                                }

                                            }
                                            $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));

                                        }else{
                                            $promotion_title='Mod';
                                            //  dd($rows->packageID.' '.$Price->packageDescID);
                                            $booking=DB::table('package_bookings as a')
                                                ->join('package_booking_details as b','b.booking_id','=','a.booking_id')
                                                ->where('a.packageID',$rows->packageID)
                                                ->where('a.packageDescID',$Price->packageDescID)
//                                                ->first();
                                                ->sum('b.number_of_person');
//

                                            if($booking>0){ // Check booking

                                                if($booking % $promotion->every_booking==1){ // every booking
                                                    if($booking>$promotion->every_booking){
                                                        if($promotion->promotion_operator2=='up'){
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_up=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_up=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_up]);
                                                                }
                                                            }
                                                            /// $Price_by_promotion+=$Price_up;
                                                        }else{
                                                            if($promotion->promotion_unit=='%'){
                                                                if($promotion->promotion_value>0){
                                                                    foreach ($pricePro as $rowPrice){
                                                                        $Price_down=$Price->Price_by_promotion*$promotion->promotion_value/100;
                                                                        DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion-$Price_down]);
                                                                    }
                                                                }
                                                            }else{
                                                                foreach ($pricePro as $rowPrice){
                                                                    $Price_down=$promotion->promotion_value;
                                                                    DB::table('package_details_sub')->where('psub_id',$rowPrice->psub_id)->update(['Price_by_promotion'=>$rowPrice->Price_by_promotion+$Price_down]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
//                                                    dd($Price_now);
                                        }
                                    }

                                    ?>
                                    <div class="col-md-3">
                                        <figure class="card card-product">
                                            @if($promotion_title=='Between')
                                                @if($data_target!=null)
                                                    <span class="badge-new">
                                                        <div id="clockdiv">
                                                            <span class="days"></span> {{trans('common.day')}}
                                                            <span class="hours"></span> {{trans('common.hour')}}
                                                            <span class="minutes"></span> {{trans('common.minute')}}
                                                            <span class="seconds"></span> {{trans('common.seconds')}} {{trans('common.last_one')}}
                                                        </div>
                                                    </span>
                                                    <script language="JavaScript">
                                                        var deadline = new Date('{{$data_target}}');
                                                        initializeClock('clockdiv', deadline);
                                                    </script>
                                                    <span class="badge-offer"><b> {{$Price->packageDescID}} </b></span>
                                                @endif
                                            @elseif($promotion_title=='Mod')
                                                <span class="badge-new">
                                                    <?php
                                                    $bookings=\App\Bookings::where('package_id',$rows->packageID)
                                                        ->where('package_detail_id',$Price->packageDescID)
                                                        ->sum('number_of_person');
                                                    if($bookings>0){
                                                        $every_booking=$every_booking-$bookings % $every_booking;
                                                    }
                                                    ?>
                                                    {{trans('package.this_price_is_only')}} {{$every_booking}} {{trans('common.seat')}}
                                                </span>
                                            @endif

                                            {{--<span class="badge-offer"><b> -50% </b></span>--}}

                                            <div class="img-wrap">
                                                <a href="{{url($url_link)}}">
                                                    <img src="{{url('package/tour/small/'.$rows->Image)}}">
                                                </a>
                                            </div>
                                            <div class="col-lg-12 text-right"><small>{{trans('package.package_tour_by')}} <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a></small></div>
                                            <figcaption class="info-wrap-2">
                                                <h5 class="title">
                                                    <a href="{{url($url_link)}}">{{$rows->packageName}}</a>
                                                </h5>
                                                <div class="rating-wrap">
                                                    <a href="detail.html">
                                                        <ul class="rating-stars">
                                                            <li style="width:80%" class="stars-active">
                                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                                            </li>
                                                            <li>
                                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                                            </li>
                                                        </ul>
                                                        <div class="label-rating">132 {{trans('common.seat')}}</div>
                                                    </a>
                                                    @if(\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')>0)
                                                        <div class="label-rating">
                                                            <strong style="color:red">
                                                                {{\App\Bookings::where('package_id',$rows->packageID)->where('package_detail_id',$Price->packageDescID)->sum('number_of_person')}}</strong>
                                                            {{trans('common.booking')}}
                                                        </div>
                                                    @endif

                                                </div> <!-- rating-wrap.// -->
                                            </figcaption>
                                            <div class="bottom-wrap">
                                                <div class="price-wrap">
                                                <span class="price-new">
                                                     <a href="{{url('package/details/'.$rows->packageID)}}">
                                                            {{$Details->count()}} {{trans('common.time_zone')}}
                                                        <?php $i=0;?>
                                                         @foreach($Details as $detail)
                                                             <?php
                                                                 $i++;
                                                             $st=explode('-',$detail->packageDateStart);
                                                             $end=explode('-',$detail->packageDateEnd);
                                                             if($st[1]==$end[1]){
                                                                 $date=\Date::parse($detail->packageDateStart);
                                                                 $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                 // dd($end[0]);
                                                             }else{
                                                                 $date=\Date::parse($detail->packageDateStart);
                                                                 $date1=\Date::parse($detail->packageDateEnd);
                                                                 $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                             }
                                                             ?>
                                                             {{$package_date}},

                                                                 <?php if($i==2){break;}?>
                                                         @endforeach
                                                        </a>
                                                </span>
                                                </div> <!-- price-wrap.// -->
                                                <a href="{{url('/booking/c/'.$Price->packageDescID)}}" class="btn btn-sm btn-primary float-right">{{trans('common.booking_now')}}</a>


                                                <div class="price-wrap h5 text-center">
                                                    {{--<span class="price-discount">฿24,999</span> <del class="price-old">฿26,999</del><br>--}}
                                                    {{--<span class="price-save">ประหยัด ฿2,999</span>--}}
                                                    @if($promotion)
                                                        <span class="price-discount">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}}</span>
                                                        @if($Price->Price_by_promotion!=$Price->price_system_fees)
                                                            <del class="price-old">{{$current->currency_symbol.number_format($Price->price_system_fees)}}</del><br>
                                                            <span class="price-save">
                                                            {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                                {{$current->currency_symbol.number_format($Price->price_system_fees)}}
                                                        </span>
                                                        @endif
                                                    @else
                                                        <span class="price-discount">{{$current->currency_symbol.number_format($Price->price_system_fees)}} / {{trans('common.per_person')}}</span>
                                                    @endif
                                                </div>
                                                <!-- price-wrap.// -->
                                            </div> <!-- bottom-wrap.// -->
                                        </figure>
                                    </div>
                                @endforeach
                            </div>
                            <!-- ============== owl slide items 2 .end // ============= -->
                        </div> <!-- col.// -->
                    </div> <!-- row.// -->
                </div> <!-- card-body .// -->
            </div> <!-- card.// -->

        </div> <!-- container .//  -->
    </section>



@endsection

