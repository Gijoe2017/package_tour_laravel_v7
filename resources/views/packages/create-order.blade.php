@extends('layouts.package.master')

@section('program-highlight')
    <!-- ========================= SECTION CONTENT ========================= -->
    <style type="text/css">
        .pull-right-sm {
            float: right;
        }
        .col-md-12{
            margin-top: 10px;
        }
        .col-sm-8{
            padding-right: 1px;
            padding-left: 1px;
        }

        .col-sm-4{
            padding-right: 1px;
            padding-left: 1px;
        }

        .col-sm-12{
            margin-top: 20px;
        }



        .stepwizard-step p {
            margin-top: 10px;
        }

        .process-row {
            display: table-row;
        }

        .process {
            display: table;
            width: 100%;
            position: relative;
        }

        .process-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .process-row:before {
            top: 25px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .process-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .process-step p {
            margin-top:10px;

        }

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }

    </style>

    <style>
        /* The container */
        .container-cart {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            /*font-size: 22px;*/
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container-cart input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container-cart:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-cart input:checked ~ .checkmark {
            background-color: #fe5806;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-cart input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-cart .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>

    <input type="hidden" id="step" name="step" value="1">
    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row">
                    @include('packages.order.step')

                    @if($Carts)
                        <div id="cart-detail">
                        <div class="col-md-9">
                    <?php $currentPackage=''; $pricetotal=0; $discount=0;?>
                    @foreach($Carts as $cart)
                        <?php
                            $PackageTourOne = DB::table('package_tour as a')
                                ->join('package_tour_info as b', 'b.packageID', '=', 'a.packageID')
                                ->where('b.LanguageCode', Session::get('language'))
                                ->where('a.packageID',$cart->package_id)
                                ->first();

                            $Off=DB::table('package_details')
                                ->where('packageID',$cart->package_id)
                                ->where('status','Y')
                                ->count();

                            $CartDetails=DB::table('package_booking_cart_details')
                                ->where('booking_cart_id',$cart->booking_cart_id)
                                ->where('package_id',$cart->package_id)
                                ->get();

                            $checking=DB::table('package_booking_cart_details')
                                ->where('booking_cart_id',$cart->booking_cart_id)
                                ->where('package_id',$cart->package_id)
                                ->where('item_status','N')
                                ->get();
                            $timeline=\App\Timeline::where('id',$PackageTourOne->timeline_id)->first();

                        ?>
                        <div class="card">
                        <div class="col-sm-12">
                            @if($cart->package_id!=$currentPackage)
                                <div class="row">
                                <div class="col-sm-2">

                                    <label class="container-cart">
                                        <input type="checkbox" class="setCare-all"  name="package_select" {{$checking->count()==0?'checked':''}} data-id="{{$PackageTourOne->packageID}}">
                                        <span class="checkmark"></span>
                                    </label>
                                    <div class="img-wrap">
                                        <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">
                                            <img src="{{url('package/tour/small/'.$PackageTourOne->Image)}}" class="img-thumbnail">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <h4 class="title">
                                        <a href="{{url('home/details/'.$PackageTourOne->packageID)}}">{!! $PackageTourOne->packageName !!}</a>
                                    </h4>
                                    <hr>
                                    <?php
                                    $current=\App\Currency::where('currency_code',$PackageTourOne->packageCurrency)->first();
                                    $Detail=DB::table('package_details')
                                            ->where('packageID',$PackageTourOne->packageID)
                                            ->where('packageDescID',$cart->package_detail_id)
                                            ->first();

                                    //  dd($Detail);
                                    $Details=DB::table('package_details')
                                            ->where('packageID',$PackageTourOne->packageID)
                                            ->where('packageDateStart','>',date('Y-m-d'))
                                            ->where('status','Y')
                                            ->orderby('packageDateStart','asc')
                                            ->get();

                                    $Price=DB::table('package_details_sub')
                                            ->where('packageID',$PackageTourOne->packageID)
                                            ->where('status','Y')
                                            ->orderby('PriceSale','asc')
                                            ->first();

                                  //   dd($Price);
                                    $Price_now=0;
                                    $Price_up=0;
                                    $Price_down=0;

                                    //****************** Update Status if promotion finish **********************//
                                    DB::table('package_promotion')
                                            ->where('promotion_date_end','<',date('Y-m-d'))
                                            ->where('promotion_status','Y')
                                            ->where('promotion_operator','Between')
                                            ->update(['promotion_status'=>'N']);
                                    //****************** Update Status if promotion finish **********************//

                                    $data_target=null;
                                    $promotion=\App\PackagePromotion::where('packageDescID',$Price->packageDescID)->active()
                                            ->orderby('promotion_date_start','asc')
                                            ->first();
                                   // dd($promotion);

                                    $pricePro=DB::table('package_details_sub')->where('packageDescID',$Price->packageDescID)->get();
                                    // dd($pricePro);
                                    $promotion_title=null;$every_booking=0;




                                    $promotion=\App\PackagePromotion::where('packageDescID',$Detail->packageDescID)->active()
                                            ->orderby('promotion_date_start','asc')
                                            ->first();
//                                        if($PackageTourOne->packageID==31){
//                                            dd($promotion);
//                                        }
                                    $data_target=null;
                                    if($promotion && $promotion->promotion_operator!='Mod'){
                                        $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
                                    }

                                    $booking=DB::table('package_booking_details')
                                            ->where('package_id',$PackageTourOne->packageID)
                                            ->where('package_detail_id',$Detail->packageDescID)
                                            ->sum('number_of_person');

                                    $person_booking=$Detail->NumberOfPeople;

                                    if($booking){
                                        $person_booking=$Detail->NumberOfPeople-$booking;
                                    }
                                    // dd($person_booking);
                                    $price_promotion=floatval($Price->Price_by_promotion);
                                    $price_system=floatval($Price->price_system_fees);
                                    ?>

                                    {{--<div class="price-wrap">--}}
                                    {{--<del>฿25,999</del> <span class="price-save-date">(ราคานี้ 2 วัน 23:59:36, 5 ที่สุดท้ายเท่านั้น)</span>--}}
                                    {{--<var class="price">฿ 24,500 <small class="text-muted">(ต่อท่าน)</small> <span class="price-save">ประหยัด ฿2,999</span></var>--}}
                                    {{--</div>--}}

                                    @if($promotion)
                                    <div class="price-wrap h5">
                                        <var class="price h3 text-warning">
                                            <span class="currency">{{$current->currency_symbol}}</span>
                                            <span class="num">{{number_format($price_promotion)}}</span>
                                            <span>/{{trans('common.per_person')}}</span></var>
                                            @if($price_promotion!=$price_system)
                                                <br>
                                                <del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del>
                                                <span class="price-save">
                                                        {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                                                        {{$current->currency_symbol.number_format($price_system)}}
                                                </span>
                                            @endif

                                            @if($data_target != null)
                                                    <span class="price-save-date">
                                                        <span id="clockdiv{{$Detail->packageDescID}}">({{trans('common.this_price')}}
                                                         <span class="days"></span> {{trans('common.day')}}
                                                         <span class="hours"></span>:
                                                         <span class="minutes"></span>:
                                                         <span class="seconds"></span>, {{$Detail->NumberOfPeople}} {{trans('common.only_last_place')}})
                                                        </span>
                                                    </span>
                                                    <script language="JavaScript">
                                                        initializeClock('clockdiv{{$Detail->packageDescID}}', new Date('{{$data_target}}'));
                                                    </script>
                                            @else
                                                <span class="price-save-date">({{trans('common.this_price_is_only')}} {{$person_booking}} {{trans('common.only_last_place')}})</span>
                                            @endif
                                        <hr>
                                     </div>
                                        @else
                                        <div class="price-wrap h5">
                                            <var class="price h3 text-warning">
                                                <span class="num">{{$current->currency_symbol}}{{number_format($Price->Price_by_promotion)}}</span>
                                                <span>/{{trans('common.per_person')}}</span>
                                            </var>
                                        </div>
                                    @endif
                                    <div class="col-lg-12">{{trans('common.package_tour_by')}} <a href="{{url('/home/package/agent/'.$timeline->id)}}">{{$timeline->name}}</a></div>
                                </div>
                                <div class="col-sm-3">
                                <div class="pull-right-sm">
                                    @if(Auth::check())
                                        <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="" data-id="{{$Detail->packageDescID}}" class="add-wishlist btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a>
                                    @else
                                        <a data-original-title="Save to Wishlist" title="Save to Wishlist" href="{{url('ajax/login_wishlist')}}" data-toggle="modal" data-target="#popupForm" class="btn btn-outline-success" > <i class="fa fa-heart"></i></a>
                                    @endif
                                    <button id="remove-cart" data-id="{{$cart->booking_cart_id}}" class="btn remove-cart btn-outline-danger"> × {{trans('common.remove')}}</button>
                                    <br>  <br>
                                        @if($Off>0)
                                            <a href="{{url('home/details/'.$PackageTourOne->packageID)}}" class="btn btn-block btn-outline-success">  <i class="fa fa-plus"></i> {{trans('common.buy_more')}}</a>
                                        @endif
                                </div>
                            </div>
                            </div>
                            @endif

                            @if($CartDetails)

                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- price-wrap .// -->
                                         <table class="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <td></td>
                                                        <td><strong>{{trans('common.travel_time')}}</strong></td>
                                                        <td><strong>{{trans('common.tour_type')}}</strong></td>
                                                        <td><strong>{{trans('common.amount')}}</strong></td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                              <?php $price_service=0;$count=0;$totalPrice=0?>

                                              @foreach($CartDetails as $cart_detail)
                                                <?php

                                                $count++;
                                                $PackageDetails=DB::table('package_details')
                                                    ->where('packageDescID',$cart_detail->package_detail_id)
//                                                    ->where('packageDateStart','>=',date('Y-m-d'))
//                                                    ->where('status','Y')
                                                    ->get();

                                                $CheckOff=DB::table('package_details')
                                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                                    ->where('packageDateStart','<=',date('Y-m-d'))
                                                    ->first();

                                                if($CheckOff){

                                                    DB::table('package_booking_cart_details')
                                                         ->where('booking_cart_id',$cart_detail->booking_cart_id)
                                                         ->where('package_detail_id',$cart_detail->package_detail_id)
                                                         ->update(['item_status'=>'N']);
                                                }
//                                                dd($PackageDetails);

                                                $Category=DB::table('package_details_sub')
                                                    ->where('packageID',$cart_detail->package_id)
                                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                                    ->get();



                                                $Additional=DB::table('package_additional_services')
                                                    ->where('packageID',$cart_detail->package_id)
                                                    ->where('packageDescID',$cart_detail->package_detail_id)
                                                    ->where('status','Y')
                                                    ->get();

                                                    // dd($Additional);
                                                $PromotionCart=DB::table('package_booking_cart_promotion')
                                                        ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                                        ->first();

                                                ?>
                                                    @if(!$CheckOff)
                                                        <tr>
                                                        <td style="width: 15%">
                                                            <label class="container-cart">
                                                                <input type="checkbox" name="package_select" {{$cart_detail->item_status=='Y'?'checked':''}} class="setCart" data-id="{{$cart_detail->booking_cart_detail_id}}">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </td>
                                                            <td style="width: 30%">
                                                               <select class="form-control" name="update_tour_time" required>
                                                                    <option value="">{{trans('common.traveling_date')}}</option>
                                                                    @foreach($PackageDetails as $detail)
                                                                        <?php
                                                                            $st=explode('-',$detail->packageDateStart);
                                                                            $end=explode('-',$detail->packageDateEnd);
                                                                            if($st[1]==$end[1]){
                                                                                $date=\Date::parse($detail->packageDateStart);
                                                                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                                // dd($end[0]);
                                                                            }else{
                                                                                $date=\Date::parse($detail->packageDateStart);
                                                                                $date1=\Date::parse($detail->packageDateEnd);
                                                                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                                            }
                                                                        ?>
                                                                        <option value="{{$detail->packageDescID}}" {{$detail->packageDescID==$cart_detail->package_detail_id?'selected':''}} > {{$package_date}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <select class="form-control cart-type-update" name="update_tour_type" id="update_tour_type" data-id="{{$cart_detail->booking_cart_detail_id}}" required >
                                                                    <option value=""> {{trans('common.choose').trans('common.tour_type')}}</option>
                                                                    @foreach($Category as $rowsCate)
                                                                        <option value="{{$rowsCate->psub_id}}" {{$rowsCate->psub_id==$cart_detail->tour_type?'selected':''}}> {{$rowsCate->TourType}} {{$current->currency_symbol.number_format($rowsCate->Price_by_promotion)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <select class="form-control cart-amount-update" name="update_number_of_person" id="update_number_of_person" data-id="{{$cart_detail->booking_cart_detail_id}}" required>
                                                                    <option value=""> {{trans('common.amount')}}</option>
                                                                    @for($i=1;$i<=10;$i++)
                                                                    <option value="{{$i}}" {{$i==$cart_detail->number_of_person?'selected':''}}> {{$i}} {{trans('common.person')}}</option>
                                                                    @endfor
                                                                </select>
                                                            </td>
                                                            <td style="width: 6%">
                                                                <button data-id="{{$cart_detail->booking_cart_detail_id}}" class="btn btn-outline-danger btn-sm delete-detail"> × {{trans('common.delete')}}</button>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $Addition_price=0;
                                                        $AdditionalCart=DB::table('package_booking_cart_additional')
                                                                ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                                                ->get();
                                                        ?>

                                                        @if($Additional->count()>0)
                                                             <tr>
                                                                    <td><span class="btn btn-block btn-default text-success">{{trans('common.additional')}}</span> </td>
                                                                    <td colspan="2">
                                                                        @if($AdditionalCart)
                                                                            @foreach($AdditionalCart as $rowAdd)
                                                                            <div class="row">
                                                                                <div class="col-sm-10 bg-default">
                                                                                   <label class="form-control"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</label>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <button data-id="{{$rowAdd->id}}" class="addition-delete btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>
                                                                                   {{--<button class="btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>--}}
                                                                                </div>
                                                                            </div>
                                                                                <?php $Addition_price+=$rowAdd->price_service?>
                                                                            @endforeach
                                                                        @endif
                                                                        <div class="row">
                                                                            <div class="col-sm-10">
                                                                                <select class="form-control addition_cart" data-id="{{$cart_detail->booking_cart_detail_id}}" id="addition_cart" name="addition_cart">
                                                                                    <option value=""> {{trans('common.choose')}} </option>
                                                                                    @foreach($Additional as $rowAdd)
                                                                                       <option value="{{$rowAdd->id}}"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            <button  class="btn btn-outline-success btn-sm"> + {{trans('common.add')}}</button>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    {{--<td>--}}
                                                                        {{--<a href="{{url('ajax/additional/'.$cart_detail->booking_cart_detail_id)}}" data-toggle="" da class="btn btn-outline-success btn-sm"> + เพิ่ม</a>--}}
                                                                        {{--<a href="#" class="btn btn-outline-warning btn-sm"> × ลบ</a>--}}
                                                                    {{--</td>--}}
                                                                    <td></td>
                                                                </tr>
                                                        @endif
                                                             <tr>
                                                                <td colspan="2" class="text-right text-danger">
                                                                    @if(isset($PromotionCart))
                                                                    <i>{{$PromotionCart->promotion_title}}</i>
                                                                    @endif
                                                                </td>
                                                                <td colspan="2">
                                                                    <div class="text-right">
                                                                        @if($cart_detail->booking_cart_realtime_price==$cart_detail->booking_cart_normal_price)
                                                                            @if($Addition_price>0)
                                                                                <br><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}}</strong>
                                                                            @endif
                                                                            <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($cart_detail->booking_cart_realtime_price*$cart_detail->number_of_person+$Addition_price)}}</h4>
                                                                        @else
                                                                            <?php
                                                                                if($cart_detail->item_status=='Y'){

                                                                                    $discount+=($cart_detail->booking_cart_normal_price-$cart_detail->booking_cart_realtime_price);
                                                                                }
                                                                            ?>
                                                                            <strong>{{trans('common.normal_price')}} <del class="price-old">{{$current->currency_symbol.number_format($cart_detail->booking_cart_normal_price)}}</del></strong>
                                                                            <strong>{{trans('common.sale')}} {{$current->currency_symbol.number_format($cart_detail->booking_cart_realtime_price)}}</strong>
                                                                            @if($Addition_price>0)
                                                                                <BR><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}}</strong>
                                                                            @endif
                                                                            <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($cart_detail->booking_cart_realtime_price*$cart_detail->number_of_person+$Addition_price)}}</h4>
                                                                        @endif
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        @if($CartDetails->count()!=$count)
                                                        <tr><td  colspan="4"><hr></td></tr>
                                                        @endif
                                                        <BR>
                                                        <?php
                                                            if($cart_detail->item_status=='Y'){
                                                                $pricetotal+=$cart_detail->booking_cart_realtime_price*$cart_detail->number_of_person+$Addition_price;
                                                            }
                                                        ?>


                                                    @else
                                                        <tr>
                                                            <td style="width: 30%">
                                                                <select class="form-control" name="update_tour_time" disabled required>
                                                                    <option value="">{{trans('common.traveling_date')}}</option>
                                                                    @foreach($PackageDetails as $detail)
                                                                        <?php
                                                                        $st=explode('-',$detail->packageDateStart);
                                                                        $end=explode('-',$detail->packageDateEnd);
                                                                        if($st[1]==$end[1]){
                                                                            $date=\Date::parse($detail->packageDateStart);
                                                                            $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                                            // dd($end[0]);
                                                                        }else{
                                                                            $date=\Date::parse($detail->packageDateStart);
                                                                            $date1=\Date::parse($detail->packageDateEnd);
                                                                            $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                                                        }
                                                                        ?>
                                                                        <option value="{{$detail->packageDescID}}" {{$detail->packageDescID==$cart_detail->package_detail_id?'selected':''}} > {{$package_date}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="form-control cart-type-update" name="update_tour_type" id="update_tour_type" data-id="{{$cart_detail->booking_cart_detail_id}}" disabled required >
                                                                    <option value=""> {{trans('common.choose').trans('common.tour_type')}}</option>
                                                                    @foreach($Category as $rowsCate)
                                                                        <option value="{{$rowsCate->psub_id}}" {{$rowsCate->psub_id==$cart_detail->tour_type?'selected':''}}> {{$rowsCate->TourType}} {{$current->currency_symbol.number_format($rowsCate->Price_by_promotion)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <select class="form-control cart-amount-update" name="update_number_of_person" id="update_number_of_person" data-id="{{$cart_detail->booking_cart_detail_id}}" disabled required>
                                                                    <option value=""> {{trans('common.amount')}}</option>
                                                                    @for($i=1;$i<=10;$i++)
                                                                        <option value="{{$i}}" {{$i==$cart_detail->number_of_person?'selected':''}}> {{$i}} {{trans('common.person')}}</option>
                                                                    @endfor
                                                                </select>
                                                            </td>
                                                            <td style="width: 6%">
                                                                <button data-id="{{$cart_detail->booking_cart_detail_id}}" class="btn btn-outline-danger btn-sm delete-detail"> × {{trans('common.delete')}}</button>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $Addition_price=0;
                                                        $AdditionalCart=DB::table('package_booking_cart_additional')
                                                            ->where('booking_cart_detail_id',$cart_detail->booking_cart_detail_id)
                                                            ->get();
                                                        ?>

                                                        @if($Additional->count()>0)
                                                            <tr>
                                                                <td><span class="btn btn-block btn-default text-success">{{trans('common.additional')}}</span></td>
                                                                <td colspan="2">
                                                                    @if($AdditionalCart)
                                                                        @foreach($AdditionalCart as $rowAdd)
                                                                            <div class="row">
                                                                                <div class="col-sm-10">
                                                                                    <label style="background-color: #e9ecef" class="form-control"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</label>
                                                                                </div>
                                                                                {{--<div class="col-sm-2">--}}
                                                                                    {{--<button data-id="{{$rowAdd->id}}" class="addition-delete btn btn-outline-danger btn-sm"> × {{trans('common.delete')}}</button>--}}
                                                                                {{--</div>--}}
                                                                            </div>
                                                                            <?php $Addition_price+=$rowAdd->price_service?>
                                                                        @endforeach
                                                                    @endif
                                                                    <div class="row">
                                                                        <div class="col-sm-10">
                                                                            <select class="form-control addition_cart" data-id="{{$cart_detail->booking_cart_detail_id}}" id="addition_cart" name="addition_cart" disabled>
                                                                                <option value=""> {{trans('common.choose')}} </option>
                                                                                @foreach($Additional as $rowAdd)
                                                                                    <option value="{{$rowAdd->id}}"> {{$rowAdd->additional_service.' '.$current->currency_symbol.number_format($rowAdd->price_service)}}</option>
                                                                                @endforeach
                                                                            </select>


                                                                        </div>
                                                                        {{--<div class="col-sm-2">--}}
                                                                            {{--<button  class="btn btn-outline-success btn-sm addition_cart_button"> + {{trans('common.add')}}</button>--}}
                                                                        {{--</div>--}}
                                                                    </div>
                                                                </td>
                                                                {{--<td>--}}
                                                                {{--<a href="{{url('ajax/additional/'.$cart_detail->booking_cart_detail_id)}}" data-toggle="" da class="btn btn-outline-success btn-sm"> + เพิ่ม</a>--}}
                                                                {{--<a href="#" class="btn btn-outline-warning btn-sm"> × ลบ</a>--}}
                                                                {{--</td>--}}
                                                                <td></td>
                                                            </tr>
                                                        @endif
                                                        <tr>
                                                            <td>
                                                                @if(isset($PromotionCart))
                                                                   <i>{{$PromotionCart->promotion_title}}</i>
                                                                @endif

                                                            </td>
                                                            <td colspan="3">
                                                                <div class="pull-left">
                                                                   <strong class="text-danger">{{trans('common.this_tour_package_closed_for_sale')}}</strong>
                                                                </div>
                                                                <div class="text-right">
                                                                    @if($cart_detail->booking_cart_realtime_price==$cart_detail->booking_cart_normal_price)
                                                                        @if($Addition_price>0)
                                                                            <br><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}}</strong>
                                                                        @endif
                                                                        <?php
                                                                            if($cart_detail->item_status=='Y'){
                                                                                $totalPrice+=$cart_detail->booking_cart_realtime_price*$cart_detail->number_of_person+$Addition_price;
                                                                            }

                                                                            ?>
                                                                        <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($totalPrice)}}</h4>
                                                                    @else
                                                                        <?php

                                                                        $discount+=($cart_detail->booking_cart_normal_price-$cart_detail->booking_cart_realtime_price);
                                                                        ?>
                                                                        <strong>{{trans('common.normal_price')}} <del class="price-old">{{$current->currency_symbol.number_format($cart_detail->booking_cart_normal_price)}}</del></strong>
                                                                        <strong>{{trans('common.sale')}} {{$current->currency_symbol.number_format($cart_detail->booking_cart_realtime_price)}}</strong>
                                                                        @if($Addition_price>0)
                                                                            <BR><strong>{{trans('common.value_added_service')}} {{$current->currency_symbol.number_format($Addition_price)}}</strong>
                                                                        @endif
                                                                        <h4 style="color:grey">{{trans('common.totals')}} {{$current->currency_symbol.number_format($cart_detail->booking_cart_realtime_price*$cart_detail->number_of_person+$Addition_price)}}</h4>
                                                                    @endif
                                                                </div>

                                                            </td>
                                                        </tr>
                                                        @if($CartDetails->count()!=$count)
                                                            <tr><td  colspan="4"><hr></td></tr>
                                                        @endif

                                                    @endif
                                              @endforeach
                                            </tbody>
                                       </table>

                                </div>
                                </div>
                            @endif
                            </div>
                        </div>
                        <BR>
                        <?php $currentPackage=$cart->package_id?>
                    @endforeach

                </div>
                        @if($Carts->count()>0)
                        <div class="col-md-3">
                        {{--<p class="alert alert-success">Add USD 5.00 of eligible items to your order to qualify for FREE Shipping. </p>--}}
                        <h3 class="alert alert-success"><i class="fa fa-check"></i> {{trans('common.order_summary')}} </h3>
                            <figure class="itemside mb-3">
                                <div class="col-sm-8"><strong>{{trans('common.total_price')}}: </strong></div>
                                <div class="col-sm-4 text-right"><strong>{{$current->currency_symbol}} {{number_format($pricetotal)}}</strong></div>
                            </figure>

                            <figure class="itemside mb-3">
                                <div class="col-sm-8">{{trans('common.discount')}}: </div>
                                <div class="col-sm-4 text-right">{{$current->currency_symbol}} {{$discount>0?number_format($discount):0}}</div>
                            </figure>

                            <dl class="dlist-align h5">
                                <dt>{{trans('common.totals')}}:</dt>
                                <dd class="text-right"><strong>{{$current->currency_symbol}} {{number_format($pricetotal)}}</strong></dd>
                            </dl>
                            <hr>

                        <hr>
                            @if(Auth::check())
                                @if($pricetotal>0)
                                <a href="{{url('booking/continuous/step2')}}" class="btn btn-success btn-block btn-lg">
                                    <i class="fa fa-arrow-right"></i> {{trans('common.order_summary')}}
                                </a>
                                @endif
                            @else
                                @if($pricetotal>0)
                                <a href="{{url('register/2')}}" class="btn btn-success btn-block btn-lg">
                                    <i class="fa fa-arrow-right"></i> {{trans('common.continuous')}}
                                </a>
                                @endif
                            @endif

                            <a href="{{url('home/package/all')}}" class="btn btn-info btn-block btn-lg">
                                <i class="fa fa-reply"></i> {{trans('common.package_list')}}
                            </a>
                    </div>
                        @endif
                        </div>
                    @else
                    <div class="col-sm-12 text-center"><h1></h1>
                        <h1><i class="fa fa-shopping-cart"></i> Your cart is empty.</h1>
                        <div><a href="{{url('home/package/'.Session::get('group'))}}" class="btn btn-default btn-lg">Make your booking.</a> </div>

                    </div>
                    @endif
            </div>
        </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->

    {{--<!-- ========================= SECTION  ========================= -->--}}
    {{--<section class="section-name bg-white padding-y">--}}
        {{--<div class="container">--}}
            {{--<header class="section-heading">--}}
                {{--<h2 class="title-section">Section name</h2>--}}
            {{--</header><!-- sect-heading -->--}}
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
                {{--tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
                {{--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
                {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
                {{--cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non--}}
                {{--proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>--}}
        {{--</div><!-- container // -->--}}
    {{--</section>--}}
    {{--<!-- ========================= SECTION  END// ========================= -->--}}

    {{--<!-- ========================= SECTION  ========================= -->--}}
    {{--<section class="section-name bg padding-y">--}}
        {{--<div class="container">--}}
            {{--<h4>Another section if needed</h4>--}}
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
                {{--tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
                {{--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
                {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
                {{--cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non--}}
                {{--proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>--}}
        {{--</div><!-- container // -->--}}
    {{--</section>--}}
    {{--<!-- ========================= SECTION  END// ========================= -->--}}


    <script type="text/javascript">

        $('body').on('click','.setCare-all',function(){
            if($(this).prop("checked")==true){
               var status='Y';
            }else{
                var status='N';
            }

            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/set/cart_detail/all',
                data:{'package_id':$(this).data('id'),'status':status},
                success:function (data) {
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
//                    window.location =  window.location.href;
                }
            });

        });

        $('body').on('click','.setCart',function(){
            if($(this).prop("checked")==true){
               var status='Y';
            }else{
                var status='N';
            }
            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/set/cart_detail',
                data:{'package_detail_id':$(this).data('id'),'status':status},
                success:function (data) {
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
//                    window.location =  window.location.href;
                }
            });
        });

        $('body').on('click','.delete-detail',function(){

            if(confirm('Confirm delete this cart detail?')){
                $('#divLoading').show();
                $.ajax({
                    type:'get',
                    url:SP_source() +'ajax/remove/cart_detail',
                    data:{'cart_id':$(this).data('id')},
                    success:function (data) {
                        $('#divLoading').hide();
                        $("#cart-detail").html(data);
                    }
                });
            }
        });

        $(document).ready(function(){
            var counter = 1;
            $('body').on('click','#add',function(){

                if(counter>2){
                    alert("Only 2 textboxes allow");
                    return false;
                }
                var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);

                newTextBoxDiv.after().html(
                    '<div class="col-md-6">' +
                    '<div class="form-group">' +
                    '<strong>ประเภทผู้ซื้อทัวร์ #' + counter + ' *</strong>' +
                    '<input type="text" class="form-control" name="TourType[]" id="TourType' + counter + '"  required>' +
                    '</div></div>' +
                    '<div class="col-md-6">' +
                    '<div class="form-group"><strong>{{trans("package.PriceSale")}}*</strong><input type="number" class="form-control" name="PriceSale[]" id="PriceSale' + counter + '"  required></div></div>');


                newTextBoxDiv.appendTo("#add-txt");
                counter++;
            });

            $("#btn-remove").click(function () {
                if(counter==2){
                    alert("No more textbox to remove");
                    return false;
                }
                counter--;
                $("#TextBoxDiv" + counter).remove();
            });

        });
    </script>


@endsection