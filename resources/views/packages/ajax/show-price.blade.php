
@if($promotion)
    <?php
    $Price=DB::table('package_details_sub')
        ->where('status','Y')
        ->where('psub_id',$promotion->psub_id)
        ->first();
    $price_promotion=floatval($Price->Price_by_promotion);
    $price_system=floatval($Price->price_system_fees);

    $data_target=null;
    if($promotion && $promotion->promotion_operator!='Mod'){
//                                                    $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_start));
        $data_target=date('Y-m-d H:i:s',strtotime($promotion->promotion_date_end));
    }
    ?>
    <div class="price-wrap h4">

        @if($price_promotion!=$price_system)

            @if($price_system>$price_promotion)
                <del class="price-old">{{$current->currency_symbol.number_format($price_system)}}</del>  <span class="price-discount">{{$current->currency_symbol.number_format($price_promotion)}}</span> <BR>
                <span class="price-save">
                    {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                    {{$current->currency_symbol.number_format($price_system-$price_promotion)}}
                </span> <small class="person">/ {{trans('common.per_person')}}</small>
            @else
                <span class="price-discount">{{$current->currency_symbol.number_format($price_promotion)}}</span>
                <span class="price-save">
                    {{trans('common.price_title_'.$promotion->promotion_operator2)}}
                    {{$current->currency_symbol.number_format($price_promotion-$price_system)}}
                </span> <small class="person">/ {{trans('common.per_person')}}</small>
            @endif
        @else
            <div class="price-wrap h4">
                <span class="price-discount text-warning">{{$current->currency_symbol.number_format($price_system)}}</span> <small class="person">/ {{trans('common.per_person')}}</small>
            </div>
        @endif
        <BR>
        @if($promotion)
            @if($data_target != null)
                <span class="price-save-date">
                  <span id="clockdiv{{$promotion->packageDescID}}">({{trans('package.this_price')}}
                      <span class="days"></span> {{trans('common.day')}}
                      <span class="hours"></span>:
                      <span class="minutes"></span>:
                      <span class="seconds"></span>, {{$promotion->NumberOfPeople}} {{trans('package.the_last_place_only')}})
                  </span>
                </span>
                <script language="JavaScript">
                    initializeClock('clockdiv{{$promotion->packageDescID}}', new Date('{{$data_target}}'));
                </script>
            @else
                <span class="price-save-date">
                    ({{trans('package.this_price_is_only')}} {{$person_booking}} {{trans('package.the_last_place_only')}})
                </span>
            @endif
        @endif
            <br>
    </div> <!-- price-wrap.// -->
@else
    <div class="price-wrap h4">
        <span class="price-discount text-warning">{{$current->currency_symbol.number_format($Price->price_system_fees)}}</span> <small class="person">/ {{trans('common.per_person')}}</small>
    </div>
@endif

{{--@if(number_format($Price->Price_by_promotion,2)==number_format($Price->price_system_fees,2))--}}
    {{--<span class="num h5 text-warning">{{$current->currency_symbol.number_format($Price->price_system_fees)}} </span>--}}

{{--@else--}}
    {{--<del>{{$current->currency_symbol.number_format($Price->price_system_fees)}}</del> - {{$current->currency_symbol.number_format($Price->Price_by_promotion)}} <br>--}}
    {{--<span class="num h5 text-warning">{{$current->currency_symbol.number_format($Price->Price_by_promotion)}} </span>--}}
    {{--<span class="price-save">{{trans('common.saving')}} {{$current->currency_symbol.number_format($Price->price_system_fees-$Price->Price_by_promotion)}}</span>--}}
{{--@endif--}}

