@extends('layouts.package.master')



@section('program-latest')
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">

                <div class="card-body">
                    <h2><img class="img-circle" src="{{asset('/images/icon-connect.png')}}" style="max-width: 60px"> Request partner.</h2>

                </div>
            </div>
        </div>
<br>

        <div class="container">
            <div class="card">

                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-md-12">
                         <div class="row">

    @if($Partners->count()>0)
        @foreach($Partners as $rows)

            <?php

              $timeline=\App\Timeline::where('id',$rows->request_partner_id)->first();

              $media=\App\Media::where('id',$timeline->avatar_id)->first();
              $timeline_id=$rows->request_partner_id;
              $countPackage=DB::table('package_tour')->where('timeline_id',$rows->request_partner_id)->count();
              $countPeriod=DB::table('package_details')
                        ->where('status','Y')
                        ->whereIn('packageID',function($query)use($timeline_id){
                            $query->select('packageID')->from('package_tour')
                                    ->where('timeline_id',$timeline_id);
                        })
                        ->count();

            ?>
            <div class="col-md-3 text-center">
                <h5><a href="{{url('/'.$timeline->name)}}" target="_blank" > {{$timeline->name}}</a></h5>
                <figure class="card card-product">
                   <div class="img-wrap">
                       <a href="{{url('/'.$timeline->name)}}" target="_blank" >
                       @if($media!=null)
                           <img class="logo" src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @else
                           <img class="logo" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @endif
                       </a>
                    </div>
               </figure>

                <div class="body">
                    @if($rows->request_status=='accept')
                    <strong class="text-success">Status Accept</strong>
                        @else
                        <strong class="text-danger">Status request waiting to accept.</strong>
                        <a class="btn btn-block btn-success" href="{{url('home/partner/accept/'.$rows->request_partner_id)}}" >Accept</a>
                    @endif
                </div>
            </div>
        @endforeach

        @else
            <div class="col-md-12 text-center">
                <h5>Have No Partner!!</h5>
            </div>

        @endif
        </div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                            <h5>Request partner</h5>
                            <hr>
                         <div class="row">

    @if($Requests->count()>0)
        @foreach($Requests as $rows)

            <?php

              $timeline=\App\Timeline::where('id',$rows->partner_id)->first();

              $media=\App\Media::where('id',$timeline->avatar_id)->first();
              $timeline_id=$rows->partner_id;
              $countPackage=DB::table('package_tour')->where('timeline_id',$rows->partner_id)->count();
              $countPeriod=DB::table('package_details')
                        ->where('status','Y')
                        ->whereIn('packageID',function($query)use($timeline_id){
                            $query->select('packageID')->from('package_tour')
                                    ->where('timeline_id',$timeline_id);
                        })
                        ->count();

            ?>
            <div class="col-md-3 text-center">
                <h5><a href="{{url('/'.$timeline->name)}}" target="_blank" > {{$timeline->name}}</a></h5>
                <figure class="card card-product">
                   <div class="img-wrap">
                       <a href="{{url('/'.$timeline->name)}}" target="_blank" >
                       @if($media!=null)
                           <img class="logo" src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @else
                           <img class="logo" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @endif
                       </a>
                    </div>
               </figure>

                <div class="body">
                    @if($rows->request_status=='accept')
                    <strong class="text-success">Status Accept</strong>
                        @else
                        <strong class="text-danger">Status request waiting to accept.</strong>
                        <a class="btn btn-block btn-success" href="{{url('home/partner/accept/'.$rows->partner_id)}}" >Accept</a>
                    @endif
                </div>
            </div>
        @endforeach

        @else
            <div class="col-md-12 text-center">
                <h5>Have No Partner!!</h5>
            </div>

        @endif
        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

