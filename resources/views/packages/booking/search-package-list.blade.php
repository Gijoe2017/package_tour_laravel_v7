<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>

<div class="row">

    <div class="col-md-12">
        <h3 class="alert alert-info"><i class="fa fa-list"></i> {{trans('common.list_of_tour_packages')}} </h3>
    </div>
</div>

<div class="infinite-scroll2">


    <div class="card-columns">
        @foreach($Packages as $rows)
            <?php
            if($rows->package_owner=='Yes'){
                $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                if(isset($timeline->name)){
                    $pacakge_by=$timeline->name;
                }
            }else{
                $timeline_sell=\App\Timeline::where('id',$rows->timeline_id)->first();
                $timeline=\App\Timeline::where('id',$rows->owner_timeline_id)->first();
                if(isset($timeline->name)){
                    $pacakge_by=$timeline->name;
                }
                if(isset($timeline_sell->name)){
                    $sell_by=$timeline_sell->name;
                }
            }

            ?>
            <div class="card card-pin">
                <div class="panel">
                    <div class="panel-heading">
                        <strong><span class="text-danger">{{trans('common.code')}} {{$rows->packageID}}:</span> {{$rows->packageName}}</strong> <BR>
                        @if(isset($sell_by))
                            <small class="text-success">{{trans('common.sell_by')}} : {{$sell_by}}</small><BR>
                        @endif
                        @if(isset($pacakge_by))
                            <small class="text-info">{{trans('common.package_by')}} : {{$pacakge_by}}
                                @if($rows->original_package_code)
                                    <BR> {{trans('common.source_code_from_the_tour_company')}} : {{$rows->original_package_code}}
                                @endif
                            </small>
                            {{--<small class="text-info">{{trans('common.package_by')}} : {{$pacakge_by}}--}}
                            {{--@if($rows->original_package_code)--}}
                            {{--({{$rows->original_package_code}})--}}
                            {{--@endif--}}
                            {{--</small>--}}
                        @endif
                        @if($rows->package_close=='Y')
                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->package_close)}}</p>

                        @elseif($rows->packageStatus=='CP')
                            <?php
                            $user=DB::table('users_info')->where('UserID',$rows->package_checked_by)->first();
                            $date= new Date($rows->package_checked_date);
                            $check=DB::table('package_tour_checked_log')->where('package_id',$rows->packageID)->count();
                            ?>
                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}
                                {{$date->format('d F Y')}} : {{trans('common.by')}} {{isset($user->FirstName)?$user->FirstName:''}}
                                @if($check>1)
                                    <a href="{{url('package/ajax/list/user_approve/'.$rows->packageID)}}" data-target="#popupForm" data-toggle="modal"> <i class="fa fa-arrow-circle-o-right"></i></a>
                                @endif
                            </p>
                        @else
                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}</p>
                        @endif
                    </div>

                    <div class="panel-body">
                        <a href="{{url('package/details/'.$rows->packageID)}}">
                            <img class="img-thumbnail" src="{{url('/package/tour/mid/'.$rows->Image)}}">
                        </a>
                    </div>
                    <div class="panel-footer">
                        <div class="timeline-footer text-center">
                            <?php
                            $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                            $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                            $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                            $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                            ?>
                                <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary "><i class="fa fa-search-minus"></i> {{trans('common.preview')}}</a>
                                <a href="{{url('booking/backend/add/'.$rows->packageID)}}" class="btn btn-info"><i class="fa fa-edit"></i> {{trans('common.addtocart')}}</a>
                                <a href="{{url('booking/backend/check/'.$rows->packageID)}}" class="btn btn-danger"><i class="fa fa-search"></i> {{trans('common.check_seats_available')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{$Packages->render()}}
    </div>
    @if($Packages->count()>11)
    <div class="col-md-12 text-center">
        <a class="btn btn-default btn-block" href="{{url('/package/list')}}"> {{trans('common.show_all_list')}}</a>
    </div>
        @endif
</div>
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>
<script type="text/javascript">
    $('ul.pagination').hide();
    $(function () {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                //  jQuery("time.timeago").timeago();
            }
        });
    });
    </script>