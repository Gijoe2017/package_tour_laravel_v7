@extends('layouts.member.layout_master')

@section('header')
    <section class="content-header">
        <h1>Package List.
            <small>Preview</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
        {{--<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--</ol>--}}
    </section>
@endsection

@section('content')
    <?php
    App::setLocale(Session::get('language'));
    ?>
    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .btn-app > .badge {
            position: absolute;
            top: -3px;
            right: -10px;
            font-size: 10px;
            font-weight: 400;
        }
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
        .col-md-1{
            padding-right: 2px;
            padding-left: 2px;
        }
        .col-md-3{
            padding-right: 2px;
            padding-left: 2px;
        }
    </style>

    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="panel">
                    <div class="panel-heading">

                        <input type="hidden" name="status" id="status">
                        <div class="row">
                            <div class="col-md-1 text-right">
                                <label class="form-control" style="background-color: #f5ad66">{{trans('common.filter')}} : </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" >
                                    <?php
                                    $Partner=DB::table('package_tour_partner as a')
                                        ->join('timeline_info as b','b.timeline_id','=','a.request_partner_id')
                                        ->where('a.partner_id',Session::get('timeline_id'))
                                        ->get();
                                    ?>
                                    <select name="partner" id="partner" class="form-control">
                                        <option value="">{{trans('common.choose')}}{{trans('common.partner')}}</option>
                                        <option value="all" {{isset($partner)?$partner=='all'?'Selected':'':''}}>{{trans('common.partner')}}{{trans('common.all')}}</option>
                                        @foreach($Partner as $rows)
                                            <option value="{{$rows->request_partner_id}}" {{Session::get('partner')==$rows->request_partner_id?'Selected':''}}>{{$rows->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" >
                                    <select name="country" id="country" class="form-control">
                                        <option value="">{{trans('common.choose')}}{{trans('common.country')}}</option>
                                        <option value="all" {{isset($country)?$country=='all'?'Selected':'':''}}>{{trans('common.country')}}{{trans('common.all')}}</option>
                                        @foreach($PackageCountry as $rows)
                                            <option value="{{$rows->country_id}}" {{Session::get('country')==$rows->country_id?'Selected':''}}>{{$rows->country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="txtsearch" name="txtsearch" value="{{Session::get('txtsearch')}}" placeholder="{{trans('common.search_by_name')}}">
                            </div>
                            <div class="col-md-1">
                                <div class="input-group-append pull-left">
                                    <button class="btn btn-success" type="submit">{{trans('common.search')}}</button>
                                </div>
                            </div>

                        </div>
                        {{--</form>--}}
                    </div>
                </div>
            </div>
            <?php

            $countCart=0;
            if(Auth::check()){
                $Mycart=\App\Mycart::where('auth_id',Auth::user()->id)->get();
                if($Mycart){
                    foreach ($Mycart as $cart){
                        $countCart+=$cart->count_cart()->groupby('package_id')->get()->count();
                    }
                }
                $countWithlist=DB::table('package_wishlist')->where('user_id',Auth::user()->id)->count();
            }else{
                $Mycart=\App\Mycart::where('session_id',Session::getId())->first();
                if($Mycart){
                    $countCart=$Mycart->count_cart()->groupby('package_id')->get()->count();
                }
            }
            ?>
            @if($countCart>0)
                <div class="col-md-3">
                    <div class="col-md-6 text-right">
                        <a class="cart_anchor1 btn btn-app btn-lg btn-warning">
                            <span id="cart_count" class="badge bg-teal">{{$countCart}}</span>
                            <i class="fa fa-opencart"></i>
                            Orders
                        </a>
                    </div>

                    <div class="col-md-6 text-right">
                        <a href="{{url('booking/backend/check_out')}}" class="cart_anchor1 btn btn-app btn-block btn-success">
                            <i class="fa fa-check"></i>
                            Check out
                        </a>
                    </div>
                </div>
            @endif
        </div>



        <div class="row">
            <div class="package-list">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="alert alert-info"><i class="fa fa-list"></i> {{trans('common.list_of_tour_packages')}}</h3>
                    </div>
                </div>

                <div class="infinite-scroll">
                    <div class="card-columns">
                        @foreach($Packages as $rows)
                            <?php
                            if($rows->package_owner=='Yes'){
                                $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
                                if(isset($timeline->name)){
                                    $pacakge_by=$timeline->name;
                                }
                            }else{
                                $timeline_sell=\App\Timeline::where('id',$rows->timeline_id)->first();
                                $timeline=\App\Timeline::where('id',$rows->owner_timeline_id)->first();
                                if(isset($timeline->name)){
                                    $pacakge_by=$timeline->name;
                                }
                                if(isset($timeline_sell->name)){
                                    $sell_by=$timeline_sell->name;
                                }
                            }
                            ?>
                            <div class="card card-pin">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <strong><span class="text-danger">{{trans('common.code')}} {{$rows->packageID}}:</span> {{$rows->packageName}}</strong> <BR>
                                        @if(isset($sell_by))
                                            <small class="text-success">{{trans('common.sell_by')}} : {{$sell_by}}</small><BR>
                                        @endif
                                        @if(isset($pacakge_by))
                                            <small class="text-info">{{trans('common.package_by')}} : {{$pacakge_by}}
                                                @if($rows->original_package_code)
                                                    <BR> {{trans('common.source_code_from_the_tour_company')}} : {{$rows->original_package_code}}
                                                @endif
                                            </small>
                                        @endif
                                        @if($rows->package_close=='Y')
                                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->package_close)}}</p>

                                        @elseif($rows->packageStatus=='CP')

                                            <?php
                                            $user=DB::table('users_info')->where('UserID',$rows->package_checked_by)->first();
                                            $date= new Date($rows->package_checked_date);
                                            $check=DB::table('package_tour_checked_log')->where('package_id',$rows->packageID)->count();
                                            ?>
                                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}
                                                {{$date->format('d F Y')}} : {{trans('common.by')}} {{isset($user->FirstName)?$user->FirstName:''}}
                                                @if($check>1)
                                                    <a href="{{url('package/ajax/list/user_approve/'.$rows->packageID)}}" data-target="#popupForm" data-toggle="modal"> <i class="fa fa-arrow-circle-o-right"></i></a>
                                                @endif
                                            </p>
                                        @else
                                            <p class="text-red">{{trans('common.status')}}: {{trans('common.status_'.$rows->packageStatus)}}</p>
                                        @endif
                                    </div>
                                    <div class="panel-body">
                                        <a href="{{url('package/details/'.$rows->packageID)}}">
                                            <img class="img-thumbnail" src="{{url('package/tour/mid/'.$rows->Image)}}">
                                        </a>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="timeline-footer text-center">
                                            <?php
                                            $check=DB::table('package_details')->where('packageID',$rows->packageID)->count();
                                            $check_owner=DB::table('package_tour')->where('packageID',$rows->packageID)->where('packageBy',Auth::user()->id)->count();
                                            $check_del=DB::table('package_booking_cart_details')->where('package_id',$rows->packageID)->count();
                                            $check_del2=DB::table('package_booking_details')->where('package_id',$rows->packageID)->count();
                                            ?>
                                            <a href="{{url('package/details/'.$rows->packageID)}}" class="btn btn-primary "><i class="fa fa-search-minus"></i> {{trans('common.preview')}}</a>
                                            <a href="{{url('booking/backend/add/'.$rows->packageID)}}" class="btn btn-info"><i class="fa fa-edit"></i> {{trans('common.addtocart')}}</a>
                                            <a href="{{url('booking/backend/check/'.$rows->packageID)}}" class="btn btn-danger"><i class="fa fa-search"></i> {{trans('common.check_seats_available')}}</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{$Packages->render()}}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->

    </section>

    {{--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>--}}
    <script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>

    <script type="text/javascript">
        $('ul.pagination').hide();
        $(function () {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function() {
                    $('ul.pagination').remove();
                    //  jQuery("time.timeago").timeago();
                }
            });
        });


        $('body').on('blur','#txtsearch', function (e) {
            if(e.target.value){
                var partner=$('#partner').val();
                var country=$('#country').val();
                $('#preload').show();
                $.ajax({
                    type:'get',
                    url:SP_source() + 'package/booking/search-partner-package',
                    data:{'txtsearch':e.target.value,'partner':partner,'country':country},
                    success:function(data){
                        $('.package-list').html(data);
                        $('#preload').hide();
                    }
                });
                //  $('#form-set-country').submit();
            }
        });

        $('body').on('change','#country',function (e) {
            var partner=$('#partner').val();
            var txtsearch=$('#txtsearch').val();
            $('#preload').show();
            if(e.target.value){
//                    $('#form-set-country').submit();
                $.ajax({
                    type:'get',
                    url:SP_source() + 'package/booking/search-partner-package',
                    data:{'country':e.target.value,'partner':partner,'txtsearch':txtsearch},
                    success:function(data){
                        $('.package-list').html(data);
                        $('#preload').hide();
                    }
                });

            }
        });

        $('body').on('change','#partner',function (e) {
            var text=  $("#country option:first").text();
            $('#txtsearch').val('');
            if(e.target.value){
                $('#preload').show();

                $.ajax({
                    type:'get',
                    url:SP_source() + 'ajax/get-country-partner',
                    data:{'partner':e.target.value},
                    success:function(data){
                        $('#country').empty();
                        $('#country').append("<option value=''>"+ text +"</option>");
                        $('#country').append(data);
                    }
                });
                //    alert('test');
                $.ajax({
                    type:'get',
                    url:SP_source() + 'package/booking/search-partner-package',
                    data:{'partner':e.target.value},
                    success:function(data){
                        $('.package-list').html(data);
                        $('#preload').hide();
                    }
                });
                // $('#form-set-country').submit();
            }
        });


    </script>


@endsection
