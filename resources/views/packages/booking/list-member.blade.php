@extends('layouts.member.layout_master_new')

@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-calendar-check-o"></i> {{trans('common.list_of_booking')}}</h1>
        <ol class="breadcrumb">
        <li><a href="{{url('package/list_search')}}"><i class="fa fa-dashboard"></i> Home Package</a></li>
        <li><a href="{{url('booking/backend/list')}}"> {{trans('common.list_of_booking')}}</a></li>
        <li> {{trans('common.list_of_tourist')}} </li>
        </ol>
    </section>
@endsection

@section('content')
    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .btn-app > .badge {
            position: absolute;
            top: -3px;
            right: -10px;
            font-size: 10px;
            font-weight: 400;
        }
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
        .col-md-1{
            padding-right: 2px;
            padding-left: 2px;
        }
        .col-md-3{
            padding-right: 2px;
            padding-left: 2px;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading ">
                    <div class="row">

                    </div>
                </div>
                   <div class="show-list">
                <div class="panel-body">

                @if($Members->count())
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{trans('profile.identification_id')}}</th>
                                <th>{{trans('profile.FullName')}}</th>
                                <th>{{trans('profile.date_of_birth')}}</th>
                                <th>{{trans('profile.religion')}}</th>
                                <th>{{trans('profile.nationality')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($Members as $rows)
                                <tr>
                                    <td>{{$rows->identification_id}}</td>
                                    <td>{{\App\models\Title::where('user_title_id',$rows->user_title_id)->active()->first()->user_title}}
                                        {{$rows->first_name}} {{$rows->last_name }} {{$rows->middle_name?'('.$rows->middle_name.')':''}}</td>
                                    <td>{{$rows->date_of_birth}}</td>
                                    <td>{{\App\models\Religion::where('religion_id',$rows->religion_id)->active()->first()->religion}}</td>
                                    <td>{{\App\models\Nationality::where('nationality_id',$rows->current_nationality_id)->active()->first()->nationality}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-gear"></i> {{trans('common.manage')}} </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li class="bg-info"><a href="{{url('package/member/change-language/'.$rows->passport_id)}}" data-toggle="modal" data-target="#modal-add-language" ><i class="fa fa-flag"></i> {{trans('profile.language')}} </a></li>
                                                <li><a href="{{url('member/edit/'.$rows->passport_id)}}" ><i class="fa fa-edit"></i> {{trans('profile.Edit')}} </a></li>
                                                <li><a href="{{url('member/edit-step2/'.$rows->passport_id)}}"><i class="fa fa-address-card"></i> {{trans('profile.passport')}}</a></li>
                                                <li><a href="{{url('member/edit-step3/'.$rows->passport_id)}}"><i class="fa fa-phone"></i> {{trans('profile.contact')}}</a></li>
                                                <li><a href="{{url('member/step4/'.$rows->passport_id)}}"><i class="fa fa-user"></i> {{trans('profile.family')}}</a></li>
                                                <li><a href="{{url('education/list/'.$rows->passport_id)}}"><i class="fa fa-book"></i> {{trans('profile.education')}}</a></li>
                                                <li><a href="{{url('document/list/'.$rows->passport_id)}}"><i class="fa fa-file"></i> {{trans('profile.document_files')}}</a></li>
                                                <li><a href="{{url('reference/list/'.$rows->passport_id)}}"><i class="fa fa-adjust"></i> {{trans('profile.reference')}}</a></li>
                                                <li><a href="{{url('occupation/list/'.$rows->passport_id)}}"><i class="fa fa-book"></i> {{trans('profile.occupation')}}</a></li>
                                                <li><a href="{{url('place/list/'.$rows->passport_id)}}"><i class="fa fa-map"></i> {{trans('profile.place_stay')}}</a></li>
                                                <li><a href="{{url('visited/list/'.$rows->passport_id)}}"><i class="fa fa-plane"></i> {{trans('profile.country_will_travel')}}</a></li>
                                                <li class="bg-orange"><a href="{{url('member/delete/'.$rows->passport_id)}}" onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr><td colspan="6"><BR></td>
                                {{--<th>{{trans('profile.identification_id')}}</th>--}}
                                {{--<th>{{trans('profile.FullName')}}</th>--}}
                                {{--<th>{{trans('profile.date_of_birth')}}</th>--}}
                                {{--<th>{{trans('profile.Religion')}}</th>--}}
                                {{--<th>{{trans('profile.Nationality')}}</th>--}}
                            </tr>
                            </tfoot>
                        </table>


                @else
                    <h3 class="text-center text-danger">Your tourist hasn't been found!</h3>
                    <p class="text-center"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}" class="btn btn-warning"> Make a tourist click go!!</a> </p>
                @endif
            </div>
                   </div>

            </div>
        </div>
        <!-- /.col -->
        </div>
    </section>

    <script>
        $('body').on('change','#status',function () {
            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/booking/sortbystatus',
                data:{'status':$(this).val()},
                success:function (data) {
                    // alert('test');
                    $('#divLoading').hide();
                    $(".show-list").html(data);

                }
            });

        });

    </script>


@endsection
