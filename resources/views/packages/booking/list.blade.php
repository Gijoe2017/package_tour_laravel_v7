@extends('layouts.member.layout_master_new')

@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-calendar-check-o"></i> {{trans('common.list_of_booking')}}</h1>
        <ol class="breadcrumb">
        <li><a href="{{url('package/list_search')}}"><i class="fa fa-dashboard"></i> Home Package</a></li>
        <li><a href="{{url('booking/backend/list')}}"> {{trans('common.list_of_booking')}}</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .btn-app > .badge {
            position: absolute;
            top: -3px;
            right: -10px;
            font-size: 10px;
            font-weight: 400;
        }
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
        .col-md-1{
            padding-right: 2px;
            padding-left: 2px;
        }
        .col-md-3{
            padding-right: 2px;
            padding-left: 2px;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading ">
                    <div class="row">
                    <div class="form-group">
                    <label class="col-md-2 text-right"> <strong>Filter : </strong></label>
                    <div class="col-md-4">
                    <select id="status" name="status" class="form-control">
                        <option value="all"> -- All --</option>
                        @foreach($Status as $st)
                        <option value="{{$st->booking_status}}">{{trans('common.'.$st->status_name)}}</option>
                        @endforeach
                    </select>
                    </div>
                    </div>
                    </div>
                </div>
                   <div class="show-list">
                <div class="panel-body">
                @if($Bookings->count())
                    <table id="example1" class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('common.booking_id')}}</th>
                            <th>{{trans('common.booking_date')}}</th>
                            <th>{{trans('common.package')}}</th>
                            {{--<th>{{trans('common.person')}}</th>--}}
                            {{--<th>{{trans('common.status')}}</th>--}}
                            {{--<th style="width: 15%"></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $TotalsAll=0;$AdditionalPrice=0;
                        ?>
                        @foreach($Bookings as $rows)
                            <?php
                       // dd($rows);

                            $Details=DB::table('package_booking_details as a')
                                ->join('package_details as d','d.packageDescID','=','a.package_detail_id')
                                ->join('package_details_sub as b','b.psub_id','=','a.tour_type')
                                ->where('a.booking_id',$rows->booking_id)
                                ->orderby('packageDateStart','asc')
                                ->get();

                            $Deposit=0;$Totals=0;$person_title=""; $person_count=0;

                            $Totals=$TotalsAll+$AdditionalPrice;
                            $st=explode('-',$Details[0]->packageDateStart);
                            $end=explode('-',$Details[0]->packageDateEnd);

                            if($st[1]==$end[1]){
                                $date=\Date::parse($Details[0]->packageDateStart);
                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                // dd($end[0]);
                            }else{
                                $date=\Date::parse($Details[0]->packageDateStart);
                                $date1=\Date::parse($Details[0]->packageDateEnd);
                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                            }

                            $PackageDetailsOne=DB::table('package_tour_info')
                                ->where('packageID',$Details[0]->package_id)
                                ->first();
                            //  dd($PackageDetailsOne);
                            //    dd($rows);
                                    $i=0;
                            ?>
                            <tr>
                                <td><a href="{{url('booking/backend/show/invoice/'.$rows->booking_id.'/all')}}">#{{$rows->booking_id}}</a></td>
                                <td>{{$rows->booking_date}}</td>
                                <td>
                                    <a href="{{url('home/details/'.$Details[0]->package_id)}}" target="_blank">
                                       <strong> {{$PackageDetailsOne?$PackageDetailsOne->packageName:''}}</strong>
                                    </a>
                                    <table class="table">

                                        @foreach($Details as $rowD)
                                            <?php
                                            $i++;
                                            $Invoice_deposit=DB::table('package_invoice')
                                                ->where('invoice_booking_id',$rowD->booking_id)
                                                ->where('invoice_package_detail_id',$rowD->package_detail_id)
                                                ->where('invoice_package_id',$rowD->package_id)
//                                                ->where('invoice_status','2')
                                                ->where('invoice_type','1')
                                                ->first();

                                            $Invoice_balance=DB::table('package_invoice')
                                                ->where('invoice_booking_id',$rowD->booking_id)
                                                ->where('invoice_package_detail_id',$rowD->package_detail_id)
                                                ->where('invoice_package_id',$rowD->package_id)
//                                                ->where('invoice_status','4')
                                                ->where('invoice_type','2')
                                                ->first();

                                            if($rowD->package_detail_id=='2350'){
//                                                dd($Invoice_deposit);
                                            }
//                                            $Invoice_balance=DB::table('package_invoice')
//                                                ->where('invoice_booking_id',$rows->booking_id)
//                                                ->where('invoice_package_detail_id',$rows->package_detail_id)
//                                                ->where('invoice_type','2')
//                                                ->first();

                                            $PackageDetailsOne=DB::table('package_tour_info')
                                                ->where('packageID',$rowD->package_id)
                                                ->first();
                                            if($rowD->season=='Y'){
                                                $order_by="desc";
                                            }else{
                                                $order_by="asc";
                                            }

                                            $Condition=DB::table('condition_in_package_details as a')
                                                ->join('package_condition as b','b.condition_code','=','a.condition_id')
                                                ->join('mathematical_formula as c','c.formula_id','=','b.formula_id')
                                                ->where('b.condition_group_id','1')
                                                ->where('b.formula_id','>',0)
                                                ->where('a.packageID',$rowD->package_id)
                                                ->orderby('c.value_deposit',$order_by)
                                                ->first();
                                            if($Condition){
                                                $Deposit+=$Condition->value_deposit*$rowD->number_of_person;
                                            }
                                            $TotalsAll+=$rowD->number_of_person*$rowD->booking_normal_price;
                                            $person_count+=$rowD->number_of_person;

                                            $st=explode('-',$rowD->packageDateStart);
                                            $end=explode('-',$rowD->packageDateEnd);

                                            if($st[1]==$end[1]){
                                                $date=\Date::parse($rowD->packageDateStart);
                                                $package_date=$st[2].'-'.$end[2].$date->format(' F Y');
                                                // dd($end[0]);
                                            }else{
                                                $date=\Date::parse($rowD->packageDateStart);
                                                $date1=\Date::parse($rowD->packageDateEnd);
                                                $package_date=$st[2].$date->format(' F').'-'.$end[2].$date1->format(' F').$date1->format(' Y');
                                            }
                                            $person_title=$package_date.' '.$rowD->TourType.' '.trans('common.amount').' '.$rowD->number_of_person.' '.trans('common.person').'<Br>';
                                            ?>
                                         <tr>
                                             <td>{{$package_date}}</td>
                                             <td width="10%">{{$rowD->TourType}}</td>
                                             <td width="10%">{{trans('common.amount').' '.$rowD->number_of_person.' '.trans('common.person')}}</td>
                                             <td width="20%">
                                                 <span class="text-danger">
                                                     @if(!$Invoice_deposit)
                                                         @if($Invoice_balance)
                                                             <?php
                                                             $Status=DB::table('booking_status')->where('booking_status',$Invoice_balance->invoice_status)->first();
                                                             ?>
                                                             @if($Invoice_balance->invoice_status==4)
                                                                 <span class="text-success"> {{trans('common.'.$Status->status_name)}}</span>
                                                             @else
                                                                 {{trans('common.wait_pay_balance')}}
                                                             @endif
                                                         @endif
                                                     @else
                                                         <?php
                                                         $Status=DB::table('booking_status')->where('booking_status','1')->first();
                                                         ?>
                                                         @if($Invoice_deposit->invoice_status==1 && $Invoice_balance->invoice_status==1)
                                                             {{trans('common.'.$Status->status_name)}}

                                                         @elseif($Invoice_deposit->invoice_status==2 && $Invoice_balance->invoice_status==1)
                                                             {{trans('common.wait_pay_balance')}}
                                                         @else

                                                            <span class="text-success">{{trans('common.paid')}}</span>
                                                         @endif
                                                     @endif
                                                 </span>
                                             </td>
                                             <td width="15%" class="text-left">
                                                 <div class="btn-group ">
                                                     <button type="button" class=" btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown"> {{trans('common.manage')}} <span class="caret"></span></button>
                                                     <ul class="dropdown-menu" role="menu">
                                                         @if(!$Invoice_deposit)
                                                             @if($Invoice_balance)
                                                             <li>
                                                                 <a href="{{url('booking/backend/show/invoice/'.$Invoice_balance->invoice_id)}}" >
                                                                     @if($Invoice_balance->invoice_status==4)
                                                                         <i class="fa fa-check text-green"></i>
                                                                     @else
                                                                         <i class="fa fa-close text-red"></i>
                                                                     @endif
                                                                     {{trans('common.invoice_deposit')}}
                                                                 </a>
                                                             </li>
                                                                 @endif
                                                         @else
                                                             <li>
                                                                 <a href="{{url('booking/backend/show/invoice/'.$Invoice_deposit->invoice_id)}}" >
                                                                     @if($Invoice_deposit->invoice_status==2)
                                                                         <i class="fa fa-check text-green"></i>
                                                                     @else
                                                                         <i class="fa fa-close text-red"></i>
                                                                     @endif
                                                                     {{trans('common.invoice_deposit')}}
                                                                 </a>
                                                             </li>

                                                             <li>
                                                                 <a href="{{url('booking/backend/show/invoice/'.$Invoice_balance->invoice_id)}}" >
                                                                     @if($Invoice_balance->invoice_status==4)
                                                                         <i class="fa fa-check text-green"></i>
                                                                     @else
                                                                         <i class="fa fa-close text-red"></i>
                                                                     @endif
                                                                     {{trans('common.invoice_balance')}}
                                                                 </a>
                                                             </li>
                                                         @endif

                                                         <li><a href="{{url('booking/backend/show/invoice/'.$rows->booking_id)}}"><i class="fa fa-exchange"></i> {{trans('common.update_status')}}</a></li>
                                                         <li><a href="{{url('booking/backend/tour/info/'.$rows->booking_id.'/'.$rows->tour_type)}}"><i class="fa fa-users"></i> {{trans('common.tour_information')}}</a></li>
                                                     </ul>
                                                 </div>
                                             </td>
                                         </tr>

                                        @endforeach



                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center text-danger">Your booking hasn't been found!</h3>
                    <p class="text-center"><a href="{{url('home/package/all')}}" class="btn btn-warning"> Make a booking click go!!</a> </p>
                @endif
            </div>
                   </div>

            </div>
        </div>
        <!-- /.col -->
        </div>
    </section>

    <script>
        $('body').on('change','#status',function () {
            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/booking/sortbystatus',
                data:{'status':$(this).val()},
                success:function (data) {
                    // alert('test');
                    $('#divLoading').hide();
                    $(".show-list").html(data);

                }
            });

        });

    </script>


@endsection
