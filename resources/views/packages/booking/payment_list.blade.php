@extends('layouts.member.layout_master_new')

@section('pageHeader')
    <section class="content-header">
        <h1><i class="fa fa-calendar-check-o"></i> {{trans('common.list_payment_notification')}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('package/list_search')}}"><i class="fa fa-dashboard"></i> Home Package</a></li>
            <li><a href="{{url('booking/backend/payment/list')}}"> {{trans('common.list_payment_notification')}}</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
    <style type="text/css">
        .btn-app > .badge {
            position: absolute;
            top: -3px;
            right: -10px;
            font-size: 10px;
            font-weight: 400;
        }
        .card-columns .card{margin-bottom:.75rem}
        @media (min-width:576px){
            .card-columns{-webkit-column-count:3;column-count:3;-webkit-column-gap:1.25rem;column-gap:1.25rem}
            .card-columns .card{display:inline-block;width:100%}}
        .form-group{
            margin-bottom: 0;
        }
        .col-md-1{
            padding-right: 2px;
            padding-left: 2px;
        }
        .col-md-3{
            padding-right: 2px;
            padding-left: 2px;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="panel">
                <div class="panel-heading">
                    <h3><i class="fa fa-list"></i> {{trans('common.list_payment_notification')}}.</h3>
                </div>
            <div class="panel-body">
                @if($Payments->count())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('common.booking_id')}}</th>
                            <th>{{trans('common.invoice_no')}}</th>
                            <th>{{trans('common.payment_details')}}</th>
                            <th>{{trans('common.payer_details')}}</th>
                            <th>{{trans('common.status')}}</th>
                            <th style="width: 15%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Payments as $rows)
                            <?php
                                $Invoice=DB::table('package_invoice')->where('invoice_id',$rows->payment_invoice_id)->first();
                            ?>
                           <tr>
                                <td>#{{$rows->invoice_booking_id}}</td>
                                <td><a href="{{url('booking/backend/show/invoice/'.$rows->payment_invoice_id)}}">#{{$rows->payment_invoice_id}}</a></td>
                                <td>
                                    {!!  trans('common.transfer_amount').' <strong>'.$Invoice->currency_symbol.number_format($rows->transfer_amount,2).'</strong>'!!}<BR>
                                    {!!  trans('common.date_time').' <strong>'.$rows->transfer_date.' '.$rows->transfer_time.'</strong>'!!}<BR>
                                    {!!  trans('common.transfer_bank').' <strong>'.$rows->bank_transfer.'</strong> '.trans('common.sub_bank').' <strong>'.$rows->sub_bank.'</strong>'!!}
                                </td>
                                <td>
                                    {{trans('common.name_company_for_receipt_tax_invoice')}}{!!  $rows->tax_business_name !!}<BR>
                                    {{trans('common.id_card_number').' '.$rows->id_card_number}}<BR>
                                    {{trans('common.phone').' '.$rows->transfer_phone}}<BR>
                                    {{trans('common.emails').' '.$rows->tax_email}}
                                </td>

                                <td>
                                   <span class="text-danger">
                                        @if($rows->status=='s')
                                           {{trans('common.wait_pay_balance')}}
                                        @else
                                           {{trans('common.paid')}}
                                        @endif
                                    </span>
                                </td>
                                <td class="text-left">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> {{trans('common.manage')}} <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{url('booking/backend/show_update/invoice/'.$rows->invoice_booking_id)}}"><i class="fa fa-exchange"></i> {{trans('common.update_status')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center text-danger">Payment notification not found!</h3>
                    {{--<p class="text-center"><a href="{{url('home/package/all')}}" class="btn btn-warning"> Make a booking click go!!</a> </p>--}}
                @endif
            </div>
            </div>
        </div>
        <!-- /.col -->
        </div>
    </section>

@endsection
