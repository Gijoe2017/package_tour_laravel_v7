@extends('layouts.member.layout_master_new')
@section('pageHeader')

    <div class="box-header with-border">
    {{--<section class="content-header">--}}
        <div class="col-md-8">
            <h2>
                Check seats available <small>Package tour</small>
            </h2>
        </div>




    </div>
    {{--</section>--}}
@endsection

@section('content')
    <style type="text/css">
        .btn-app > .badge {
            position: absolute;
            top: -3px;
            right: -10px;
            font-size: 10px;
            font-weight: 400;
        }

        .content{
           padding-top: 0;
        }

        .material-switch > input[type="checkbox"] {
            display: none;
        }
        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 35px;
        }
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 35px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 20px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 20px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .col-md-6{
            padding-left: 1px;
            padding-right: 5px;
        }
        {{--.col-md-4{--}}
            {{--padding-left: 1px;--}}
            {{--padding-right: 5px;--}}
        {{--}--}}
        {{--.col-md-2{--}}
            {{--padding-left: 1px;--}}
            {{--padding-right: 5px;--}}
        {{--}--}}

        {{--.cart_anchor {--}}
            {{--float: right;--}}
            {{--vertical-align: top;--}}
            {{--background: url({{asset('images/cart-icon.png')}}) no-repeat center center / 100% auto;--}}
            {{--width: 50px;--}}
            {{--height: 50px;--}}

        {{--}--}}

    </style>
    <section class="content">

    <div class="row">
            <div class="box box-info">
                <div class="box-body">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <a class="btn btn-default" href="{{url('booking/backend/main')}}"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                        </div>
                                        <hr>
                                    </div>

                                    @if(isset($PackageDetails))


                                            <table class="table">
                                                <thead>
                                                    <th>{{trans('package.package_id')}}</th>
                                                    <th>{{trans('package.PackageName')}}</th>
                                                    <th>{{trans('common.date_start')}}</th>
                                                    <th>{{trans('common.date_end')}}</th>
                                                    <th>{{trans('common.number_of_people')}}</th>
                                                    <th>{{trans('common.number_of_people_booking')}} </th>
                                                    <th>{{trans('common.number_of_people_available')}} </th>
                                                    <th>{{trans('common.action')}} </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $index=0?>
                                                @foreach($PackageDetails as $rows)
                                                    <?php

                                                    $NumberOfPeople=DB::table('package_booking_details as a')
                                                        ->join('package_bookings as b','b.booking_id','=','a.booking_id')
                                                        ->where('a.package_detail_id',$rows->packageDescID)
                                                        ->where('b.booking_status','2')
                                                        ->get()->sum('number_of_person');

                                                    ?>
                                                    <tr>
                                                        <td>#{{$rows->packageID}}</td>
                                                        <td>{{$rows->packageName}}</td>
                                                        <td>{{$rows->packageDateStart}}</td>
                                                        <td>{{$rows->packageDateEnd}}</td>
                                                        <td>{{$rows->NumberOfPeople}}</td>
                                                        <td>{{$NumberOfPeople}}</td>
                                                        <td>{{$rows->NumberOfPeople-$NumberOfPeople}}</td>
                                                        <td><a href="{{url('booking/c/'.$rows->packageDescID)}}" target="_blank" class="btn btn-danger"><i class="fa fa-shopping-cart"></i> {{trans('common.addtocart')}}</a> </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>


                                        @else
                                            <h2>{{trans('common.no_have_data')}}</h2>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <script src="{{asset('effect/')}}/js/jquery.min.js"></script>
    <script>
//        $('#bt-add-cart').on('click',function () {
//            var input1 = 'tour_type'+$(this).data('id');
//            var input2 = 'number_of_person'+$(this).data('id');
//            var tour_type=$('#'+input1).val();
//            var number_of_person=$('#'+input2).val();
//
//            $.ajax({
//                type:'get',
//                url:SP_source() + 'booking/backend/add_cart',
//                data:{'id':$(this).data('id'),'tour_type':tour_type,'number_of_person':number_of_person},
//                success:function(data){
//                   $('#show-cart').show();
//                }
//            });
//
//        });
    </script>

    <script type="text/javascript">

        function flyToElement(flyer, flyingTo) {

            var $func = $(this);
            var divider = 3;
            var flyerClone = $(flyer).clone();
            $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 1000});
            $('body').append($(flyerClone));
            var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
            var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

            $(flyerClone).animate({
                    opacity: 0.4,
                    left: gotoX,
                    top: gotoY,
                    width: $(flyer).width()/divider,
                    height: $(flyer).height()/divider
                }, 700,
                function () {
                    $(flyingTo).fadeOut('fast', function () {
                        $(flyingTo).fadeIn('fast', function () {
                            $(flyerClone).fadeOut('fast', function () {
                                $(flyerClone).remove();
                            });
                        });
                    });
                });
        }


            $('.add-to-cart').on('click',function(){
                //Scroll to top if cart icon is hidden on top

                var input1 = 'tour_type'+$(this).data('id');
                var input2 = 'number_of_person'+$(this).data('id');
                var tour_type=$('#'+input1).val();
                var number_of_person=$('#'+input2).val();

                $.ajax({
                    type:'get',
                    url:SP_source() + 'booking/backend/add_cart',
                    data:{'id':$(this).data('id'),'tour_type':tour_type,'number_of_person':number_of_person},
                    success:function(data){
                        $("#cart_count").html(data);
                    }
                });

                $('html, body').animate({
                    'scrollTop' : $(".cart_anchor1").position().top
                });
                //Select item image and pass to the function
                var itemImg = $(this).parent().find('img').eq(0);

                flyToElement($(itemImg), $('.cart_anchor1'));
            });


    </script>

@stop()