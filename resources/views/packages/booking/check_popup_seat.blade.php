<div class="row">
    <div class="col-md-12 text-center">
        @if($PackageDetail->NumberOfPeople >= $person)
            <h3 class="text-success">{{trans('common.amount_seat_available')}} {{$PackageDetail->NumberOfPeople}}</h3>
        @else
            <h3 class="text-danger">{{trans('common.amount_seat_available')}} {{$PackageDetail->NumberOfPeople}}</h3>
        @endif
    </div>
    <div class="col-md-12 text-center">
        <br>
        @if($PackageDetail->NumberOfPeople>$person)
            <a href="{{url('booking/backend/confirm/seat/'.$booking.'/'.$PackageDetail->packageDescID)}}" class="btn btn-success">{{trans('common.confirm_seat_avalable')}}</a>
        @else
            <a href="{{url('booking/backend/cancel/seat/'.$booking.'/'.$PackageDetail->packageDescID)}}" class="btn btn-danger">{{trans('common.no_seat_avalable')}}</a>
        @endif
    </div>

</div>


