<?php

function dateDiv($t1,$t2){
    $secondsDifference=strtotime($t1)-strtotime($t2);
    return $secondsDifference/86400;
}

$package="";

$Invoices=DB::table('package_invoice')
    ->where('invoice_booking_id',$Booking->booking_id)
    ->where('invoice_type','2')
    ->where('invoice_status','4')
    ->get();
if($Invoices->count()==0){
    $Invoices=DB::table('package_invoice')
        ->where('invoice_booking_id',$Booking->booking_id)
        ->where('invoice_type','1')
        ->where('invoice_status','2')
        ->get();

}



$message_cancel='';
if($Invoices->count()>0){

    foreach ($Invoices as $rows){
        $message_cancel='';
        $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
       // dd($Detail);

        $dateStart=$Detail->packageDateStart;
        $dateCancel=date('Y-m-d h:i:s');

        $message_cancel=trans('common.this_package_departs_on').' '.date('F d, Y',strtotime($dateStart)).'<BR>';
        $message_cancel.=trans('common.cancel_notification_in').' '.date('F d, Y',strtotime($dateCancel)).'<hr>';

        $days=dateDiv($dateStart,$dateCancel);
       // dd($days);
        $package=$rows->invoice_package_id;

        $condition=DB::table('package_condition as a')
            ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
            ->whereIn('a.condition_code',function ($query) use($package){
                $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
            })
            ->where('a.condition_group_id','2')
            ->where('a.timeline_id',$rows->invoice_timeline_id)
            ->where('a.formula_id','!=','0')
            ->where('language_code',Session::get('language'))
            ->get();

            // dd($condition);
            $value_tour=0;$remark='';$message_cancel2='';
            foreach ($condition as $rowC){
                if($rowC->operator_code=='Between'){
                    if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
//                        if($rowC->keep_all_costs=='Y'){
//                            $value_tour=$rows->invoice_amount;
//                        }else if($rowC->keep_value_tour=='Y'){
//                            if($rowC->unit_deposit=='%'){
//                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
//                            }else{
//                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
//                            }
//                        }elseif($rowC->return_all_costs=='Y'){
//                            $value_tour=0;
//                        }
                        $remark=$rowC->condition_title;
                    }

                }else if($rowC->operator_code=='<'){
                    if($days<$rowC->condition_left){
//                        if($rowC->keep_all_costs=='Y'){
//                            $value_tour=$rows->invoice_amount;
//                        }else if($rowC->keep_value_tour=='Y'){
//                            if($rowC->unit_deposit=='%'){
//                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
//                            }else{
//                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
//                            }
//                        }elseif($rowC->return_all_costs=='Y'){
//                            $value_tour=0;
//                        }
                        $remark=$rowC->condition_title;
                    }

                }else{

                    if($days>$rowC->condition_left){
//                        if($days<$rowC->condition_left){
//                            if($rowC->keep_all_costs=='Y'){
//                                $value_tour=$rows->invoice_amount;
//                            }else if($rowC->keep_value_tour=='Y'){
//                                if($rowC->unit_deposit=='%'){
//                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
//                                }else{
//                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
//                                }
//                            }elseif($rowC->return_all_costs=='Y'){
//                                $value_tour=0;
//                            }
//                        }
                        $remark=$rowC->condition_title;
                    }

                }

                $remark?$message_cancel2.=$remark.'<br>':'';
                // dd($value_tour.'='.$remark);
            }


        }

    if($message_cancel2==''){
        $message_cancel.= trans('common.no_condition_cancel').'<hr>';
    }else{
        $message_cancel.=$message_cancel2;
    }

}


//dd($Invoice_deposit->count().'='.$Invoice_balance->count());

?>
<hr>
<div class="card-body">
    <div class="card-text">

        @if($Invoices->count()==0)
            <label>{{trans('common.message_cancel_all')}}</label>
        @else
            <label class="text-danger">{!! $message_cancel !!}</label><BR>
            <label>{{trans('common.message_cancel_all')}}</label>
        @endif

        @if(Session::has('message'))
            <h2 class="text-success">{{Session::get('message')}}</h2>
        @endif

        <BR><BR>
        <a href="{{url('booking/confirm/cancel/'.$Booking->booking_id.'/all')}}"  data-option="all" class="btn btn-danger btn-lg"><i class="fa fa-times"></i> {{trans('common.confirm_cancel_booking')}}</a>
    </div>
</div>