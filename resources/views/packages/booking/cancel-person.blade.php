<?php

function dateDiv($t1,$t2){
    $secondsDifference=strtotime($t1)-strtotime($t2);
    return $secondsDifference/86400;
}

$package="";
$Invoice_deposit=DB::table('package_invoice')
    ->where('invoice_booking_id',$Booking->booking_id)
    ->where('invoice_type','1')
    ->where('invoice_status','2')
    ->get();

$Invoice_balance=DB::table('package_invoice')
    ->where('invoice_booking_id',$Booking->booking_id)
    ->where('invoice_type','2')
    ->where('invoice_status','4')
    ->get();



if($Invoice_deposit->count()>0){

    foreach ($Invoice_deposit as $rows){

        $Detail=DB::table('package_details')->where('packageDescID',$rows->invoice_package_detail_id)->first();
        $dateStart=$Detail->packageDateStart;
        $dateCancel=date('Y-m-d h:i:s');

        $days=dateDiv($dateStart,$dateCancel);
        // dd($days);
        $package=$rows->invoice_package_id;

        $condition=DB::table('package_condition as a')
            ->join('mathematical_formula as b','b.formula_id','=','a.formula_id')
            ->whereIn('a.condition_code',function ($query) use($package){
                $query->select('condition_id')->from('condition_in_package_details')->where('packageID',$package);
            })
            ->where('a.condition_group_id','2')
            ->where('a.timeline_id',$rows->invoice_timeline_id)
            ->where('a.formula_id','!=','0')
            ->where('language_code',Session::get('language'))
            ->get();

            // dd($condition);
            $value_tour=0;$remark='';$message_cancel='';
            foreach ($condition as $rowC){
                if($rowC->operator_code=='Between'){
                    if($days>=$rowC->condition_left && $days<=$rowC->condition_right){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                        $remark=$rowC->condition_title;
                    }

                }else if($rowC->operator_code=='<'){
                    if($days<$rowC->condition_left){
                        if($rowC->keep_all_costs=='Y'){
                            $value_tour=$rows->invoice_amount;
                        }else if($rowC->keep_value_tour=='Y'){
                            if($rowC->unit_deposit=='%'){
                                $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                            }else{
                                $value_tour=$rows->invoice_amount-$rowC->value_tour;
                            }
                        }elseif($rowC->return_all_costs=='Y'){
                            $value_tour=0;
                        }
                        $remark=$rowC->condition_title;
                    }

                }else{

                    if($days>$rowC->condition_left){
                        if($days<$rowC->condition_left){
                            if($rowC->keep_all_costs=='Y'){
                                $value_tour=$rows->invoice_amount;
                            }else if($rowC->keep_value_tour=='Y'){
                                if($rowC->unit_deposit=='%'){
                                    $value_tour=$rows->invoice_amount*$rowC->value_tour/100;
                                }else{
                                    $value_tour=$rows->invoice_amount-$rowC->value_tour;
                                }
                            }elseif($rowC->return_all_costs=='Y'){
                                $value_tour=0;
                            }
                        }
                        $remark=$rowC->condition_title;
                    }

                }

                $remark?$message_cancel.=$remark.'<br>':'';
                // dd($value_tour.'='.$remark);
            }

        }

//    dd($message_cancel);
}

if($Invoice_balance->count()>0){
    $package=$Invoice_balance[0]->invoice_package_id;
}

//dd($Invoice_deposit->count().'='.$Invoice_balance->count());

?>
<hr>
<style>

    .example {
        margin:5px 20px 0 0;
    }
    .example input {
        display: none;
    }
    .example label {
        width: 100%;
        margin-right: 10px;
        display: inline-block;
        cursor: pointer;
    }

    .example2 {
        margin:5px 20px 0 0;
    }
    .example2 input {
        display: none;
    }
    .example2 label {
        width: 15%;
        margin-right: 10px;
        display: inline-block;
        cursor: pointer;
    }

    .ex1 span {
        display: block;

        padding: 8px 10px 8px 30px;
        border: 1px solid #ddd;
        border-radius: 5px;
        position: relative;
        transition: all 0.25s linear;
    }
    .ex1 span:before {
        content: '';
        position: absolute;
        left: 5px;
        top: 50%;
        -webkit-transform: translatey(-50%);
        transform: translatey(-50%);
        width: 18px;
        height: 18px;
        border-radius: 50%;
        background-color: #ddd;
        transition: all 0.25s linear;
    }
    .ex1 input:checked + span {
        background-color: #fff;
        box-shadow: 0 0 10px 2px rgba(0, 0, 0, 0.1);
    }
    .ex1 .red input:checked + span {
        color: red;
        border-color: red;
    }
    .ex1 .red input:checked + span:before {
        background-color: red;
    }
    .ex1 .blue input:checked + span {
        color: blue;
        border-color: blue;
    }
    .ex1 .blue input:checked + span:before {
        background-color: blue;
    }
    .ex1 .orange input:checked + span {
        color: orange;
        border-color: orange;
    }
    .ex1 .orange input:checked + span:before {
        background-color: orange;
    }

    .bank_error{
        border: solid 1px red;
        padding: 5px;
    }
</style>
<div class="card-body">

    <div class="card-text ">
        <div class="row">
        <div class="example ex1">
        <input type="hidden" name="checked-option" id="checked-option" value="0">
        @foreach($Booking_details as $rows)
            <input type="hidden" name="group[]" value="{{$rows->package_detail_id}}">
                <div class="col-sm-6">
                    <label>{!! $rows->package_detail_title !!}</label>
                    <label>{{trans('common.number_of_tourist')}} {{$rows->number_of_person}} {{trans('common.person')}}</label>
                </div>
                <div class="col-sm-6">
                <h5><i class="fa fa-check-square"></i> {{trans('common.choose_the_number_of_people_you_want_to_cancel')}}</h5>
                    <label class="radio orange">
                        <input type="radio" class="chk_person" name="chk_person{{$rows->package_detail_id}}[]" data-id="{{$rows->booking_id}}" data-package="{{$rows->package_detail_id}}"  id="chk_person" value="0"/>
                        <span> {{trans('common.clear_option')}}</span>
                    </label>
                    @for($i=1;$i<=$rows->number_of_person;$i++)
                        <label class="radio orange">
                            <input type="radio" class="chk_person" name="chk_person{{$rows->package_detail_id}}[]" data-id="{{$rows->booking_id}}" data-package="{{$rows->package_detail_id}}" data-amount="{{$i}}" id="chk_person" value="{{$i}}"/>
                            <span> {{$i.trans('common.person')}}</span>
                        </label>
                    @endfor
                </div>
                <div class="col-sm-12">
                    <hr>
                    <div id="show-message-{{$rows->package_detail_id}}" class="text-danger"></div>
                </div>

        @endforeach
        </div>
        </div>

        <div id="show-message" class="text-danger">
            @if(!$Invoice_deposit->count() && !$Invoice_balance->count())
                <label>{{trans('common.message_cancel_all')}}</label>
            @elseif($Invoice_deposit->count() && !$Invoice_balance->count())
                <label class="text-danger">{!! $message_cancel !!}</label>
                <label>{{trans('common.message_cancel_all')}}</label>
            @elseif($Invoice_balance->count())
                <label class="text-danger">{!! $message_cancel !!}</label>
                <label>{{trans('common.message_cancel_all')}}</label>
            @endif
        </div>

        <div id="btn_confirm" style="display: none">
            <BR><BR>
            <button id="submit_cancel_person" data-id="{{$rows->booking_id}}" class="btn btn-danger btn-lg"><i class="fa fa-times"></i> {{trans('common.confirm_cancel_booking')}}</button>
        </div>

    </div>
</div>