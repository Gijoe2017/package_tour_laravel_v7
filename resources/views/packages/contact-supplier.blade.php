<form class="form-horizontal" action="{{action('Package\PackageTourController@send_contact_supplier')}}" method="post">
<div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-envelope"></i> Contact Supplier</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="col-md-12">
            <h4 class="title mb-3">{{$PackageTourOne->packageName}}</h4>
            <p>
                <strong>
                    {{trans('package.package_tour_by')}}
                    {{$timeline->name}}</strong><br>
                    {{$timeline->address}}
            </p>
            <hr>
        </div>
            <div class="col-md-12">
                <h5>{{trans('common.send_message_to_supplier')}}</h5>
                <p><hr></p>
                <!-- Name input-->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">{{trans('common.your_name')}} *</label>
                    <div class="col-md-9">
                        <input id="name" name="name" type="text" placeholder="{{trans('common.your_name')}}" class="form-control" required>
                    </div>
                </div>

                <!-- Email input-->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">{{trans('common.your_email')}}*</label>
                    <div class="col-md-9">
                        <input id="email" name="email" type="text" placeholder="{{trans('common.your_email')}}" class="form-control" required>
                    </div>
                </div>

                <!-- Message body -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="message">{{trans('common.your_message')}}</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="message" name="message" placeholder="{{trans('common.your_message_here')}}" rows="5"></textarea>
                    </div>
                </div>
            </div>


    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
    </div>

</div>

</form>
