@extends('layouts.package.master-order')
@section('program-highlight')
    <section class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.occupation_information')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/package/occupation/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                        <span class="step-number">{{trans('profile.step')}} 8 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    @if(count($Occupation))
                        <table class="table table-bordered">
                        <thead>
                        <tr><th>{{trans('profile.occupation')}}</th>
                            <th>{{trans('profile.work_date_start')}}</th>
                            <th>{{trans('profile.work_date_end')}}</th>
                            <th>{{trans('profile.business_name')}}</th>
                            <th>{{trans('profile.Address')}}</th>
                            <th>{{trans('profile.Country')}}</th>
                            <th>{{trans('profile.ZipCode')}}</th>
                            <th>{{trans('profile.Phone')}}</th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($Occupation as $rows)
                            <?php
                                $state=null;$city=null;
                                $country=App\Country::where('country_id',$rows->country_id)->first();
                                $state=$country->state()->where('state_id',$rows->state_id)->first();
                                // dd($country->state()->first());
                                $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();

                                if(!is_null($state)){
                                    $state=$state->state;
                                }
                                if(!is_null($city)){
                                    $city=$city->city;
                                }
                            ?>

                            <tr>
                                <td>{{$rows->occupation}}</td>
                                <td>{{$rows->date_work}}</td>
                                <td>{{$rows->date_work_end}}</td>
                                <td>{{$rows->name}}</td>
                                <td>{{$rows->address}} {{$state}} {{$city}} </td>
                                <td>{{$country->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td>
                                    <?php
                                        $check=DB::table('users_passport_occupation_phone')->where('passport_id',$rows->passport_id)->get();
                                    ?>
                                    @foreach($check as $phone)
                                        {{$phone->phone_country_code}} {{$phone->phone_number}} {{$phone->phone_type}} <BR>
                                    @endforeach
                                </td>
                                <td align="right">
                                    {{--<a href="{{url('/member/occupation/change-language/'.$rows->occupation_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>--}}
                                    <a href="{{url('/package/occupation/edit/'.$rows->occupation_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/package/occupation/delete/'.$rows->occupation_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                      <div class="text-center"><h2>{{trans('profile.no_data')}}</h2></div>
                    @endif
                </div>
                <div class="col-md-12">
                    <hr>
                    <a href="{{url('package/reference/list')}}" class="btn btn-default btn-lg  pull-left"><i class="fa fa-reply"></i> {{trans('profile.previous')}}</a>
                    <a href="{{url('package/place/list')}}" class="btn btn-info btn-lg  pull-right"><i class="fa fa-share"></i> {{trans('profile.continue')}}</a>

                </div>
                </div>

                </div>
    </section>
@endsection
