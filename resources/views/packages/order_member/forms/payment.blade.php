<h3>
    <span class="icon"><i class="ti-credit-card"></i></span>
    <span class="title_text">Payment</span>
</h3>
<fieldset>
    <legend>
        <span class="step-heading">Payment Informaltion: </span>
        <span class="step-number">Step 4 / 4</span>
    </legend>
    <div class="form-group">
        <label for="bank_name" class="form-label required">Bank Name</label>
        <input type="text" name="bank_name" id="bank_name" />
    </div>

    <div class="form-group">
        <label for="holder_name" class="form-label required">Holder Name</label>
        <input type="text" name="holder_name" id="holder_name" />
    </div>

    <div class="form-row">
        <div class="form-date">
            <label for="expiry_date" class="form-label">Expiry Date</label>
            <div class="form-date-group">
                <div class="form-date-item">
                    <select id="expiry_date" name="expiry_date"></select>
                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                </div>
                <div class="form-date-item">
                    <select id="expiry_month" name="expiry_month"></select>
                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                </div>
                <div class="form-date-item">
                    <select id="expiry_year" name="expiry_year"></select>
                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                </div>
            </div>
        </div>

        <div class="form-select">
            <label for="payment_type" class="form-label">Payment type</label>
            <div class="select-list">
                <select name="payment_type" id="payment_type">
                    <option value="">Master Card</option>
                    <option value="Master Card">Master Card</option>
                    <option value="Visa Card">Visa Card</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group">
            <label for="card_number" class="form-label required">Card Number</label>
            <input type="number" name="card_number" id="card_number" />
        </div>

        <div class="form-group">
            <label for="cvc" class="form-label required">CVC</label>
            <input type="text" name="cvc" id="cvc" />
        </div>
    </div>
</fieldset>