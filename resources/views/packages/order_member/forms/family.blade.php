<h3>
    <span class="icon"><i class="ti-view-grid"></i></span>
    <span class="title_text">Family</span>
</h3>
<fieldset>
    <legend>
        <span class="step-heading">Family Informaltion: </span>
        <span class="step-number">Step 4 / 5</span>
    </legend>
    <input type="hidden" id="h_relation_type_id" name="h_relation_type_id">
    <input type="hidden" id="h_family_country_of_birth_id" name="h_family_country_of_birth_id">
    <input type="hidden" id="h_family_nationality_no" name="h_family_nationality_no">
    <input type="hidden" id="h_family_prv_nationality_no" name="h_family_prv_nationality_no">
    <input type="hidden" id="h_family_religion_id" name="h_family_religion_id">
    <div class="row">


            <div class="col-md-12">
                <div class="form-group">
                    <label for="relation_type_id" class="form-label ">Relation</label>
                    <div class="select-list required">
                        <select name="relation_type_id" id="relation_type_id">
                            <option value="">{{trans('profile.Choose')}}</option>
                            @foreach(\App\models\Relation::where('language_code',Auth::user()->language)->get() as $rows)
                                <option value="{{$rows->relation_type_id}}">{{$rows->relation_type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="family_first_name" class="form-label required">First name</label>
                    <input type="text" name="family_first_name" id="family_first_name" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="family_last_name" class="form-label required">Last name</label>
                    <input type="text" name="family_last_name" id="family_last_name" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="family_middle_name" class="form-label">Middle name</label>
                    <input type="text" name="family_middle_name" id="family_middle_name" />
                </div>
            </div>
       </div>

    <hr>
        <div class="row">


            <div class="col-md-4">

                <label for="family_country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}}</label>
                <div class="select-list">
                    <select name="family_country_of_birth_id" id="family_country_of_birth_id">
                        <option value="">-- {{trans('profile.Choose')}} --</option>
                        @foreach($country as $rows)
                            <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label for="place_of_birth" class="form-label required">{{trans('profile.place_of_birth')}}</label>
                    <input type="text" name="place_of_birth" id="place_of_birth" />
                </div>
            </div>

        </div>
    <hr>
        <div class="row">

        <div class="col-md-4">
            <label for="family_religion_id" class="form-label required">{{trans('profile.religion')}}</label>
            <div class="select-list">
                <select name="family_religion_id" id="family_religion_id">
                    <option value="">-- {{trans('profile.Choose')}} *--</option>
                    @foreach(\App\models\Religion::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->religion_id}}" >{{$rows->religion}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <label for="family_nationality_no" class="form-label required">{{trans('profile.nationality')}}</label>
            <div class="select-list">
                <select name="family_nationality_no" id="family_nationality_no">
                    <option value="">-- {{trans('profile.Choose')}} *--</option>
                    @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                        <option value="{{$rows->nationality_id}}" {{$rows->nationality_no=='1'?'selected':''}}>{{$rows->nationality}}</option>
                    @endforeach
                </select>
            </div>
        </div>

            <div class="col-md-4">
                <label for="family_prv_nationality_no" class="form-label required">{{trans('profile.prev_nationality')}}</label>
                <div class="select-list">
                    <select name="family_prv_nationality_no" id="family_prv_nationality_no">
                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                        @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                            <option value="{{$rows->nationality_id}}" {{$rows->nationality_no=='1'?'selected':''}}>{{$rows->nationality}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
    </div>




</fieldset>