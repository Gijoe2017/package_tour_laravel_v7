@extends('layouts.package.master-order')
@section('program-highlight')
    <section class="invoice">
        <div class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.reference_information')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/package/reference/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right"> <a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                        <span class="step-number">{{trans('profile.step')}} 7 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.reference_name')}}</th>
                            <th>{{trans('profile.Address')}}</th>
                            <th>{{trans('profile.Country')}}</th>
                            <th>{{trans('profile.ZipCode')}}</th>
                            <th>{{trans('profile.Phone')}}</th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Reference as $rows)
                            <?php
                                    $state=null;$city=null;
                                    $country=App\Country::where('country_id',$rows->country_id)->first();

                            $state=$country->state()->where('state_id',$rows->state_id)->where('language_code',Auth::user()->langauge)->first();
                          //  dd($country->state()->first());
                            $city=$country->city()->where('city_id',$rows->city_id)->where('language_code',Auth::user()->langauge)->first();
                            if(!is_null($state)){
                                $state=$state->state;
                            }
                            if(!is_null($city)){
                                $city=$city->city;
                            }

                            ?>

                            <tr>
                                <td>{{$rows->user_title}} {{$rows->reference_first_name}} {{$rows->reference_last_name}} {{$rows->reference_middle_name?"(".$rows->reference_middle_name.")":''}}</td>
                                <td>{{$rows->reference_address}} {{$state}} {{$city}} </td>
                                <td>{{$rows->country}}</td>
                                <td>{{$rows->zip_code}}</td>
                                <td></td>
                                <td>
                                    <a href="{{url('/package/reference/change-language/'.$rows->reference_id)}}" data-toggle="modal" data-target="#modal-add-language" class="btn btn-info btn-sm"><i class="fa fa-flag"></i> {{trans('profile.language')}}</a>
                                    <a href="{{url('/package/reference/edit/'.$rows->reference_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> {{trans('profile.Edit')}}</a>
                                    <a href="{{url('/package/reference/delete/'.$rows->reference_id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <hr>
                    <a href="{{url('package/document/list')}}" class="btn btn-default btn-lg  pull-left"><i class="fa fa-reply"></i> {{trans('profile.previous')}}</a>
                    <a href="{{url('package/occupation/list')}}" class="btn btn-info btn-lg  pull-right"><i class="fa fa-share"></i> {{trans('profile.continue')}}</a>

                </div>
                </div>

                </div>
        </div>
    </section>
@endsection
