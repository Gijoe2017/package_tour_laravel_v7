@extends('layouts.member.layout_master')
@section('content')

    <div id="history">
        <Div class="box box-body">
            <form  method="POST" action="{{action('Member\MemberController@store')}}" id="member-form" enctype="multipart/form-data" class="signup-form">
                {{csrf_field()}}
                {{--@include('member.forms.test')--}}
                @include('member.forms.general')
                {{--@include('member.forms.passport')--}}
                {{--@include('member.forms.contact')--}}
                {{--@include('member.forms.family')--}}
                @include('member.forms.document')
                {{--@include('member.forms.payment')--}}
            </form>
        </div>
    </div>



    @endsection

