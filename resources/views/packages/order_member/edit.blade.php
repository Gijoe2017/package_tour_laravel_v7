@extends('layouts.member.layout_master')
@section('content')


    <style>

        div.savestatus{ /* Style for the "Saving Form Contents" DIV that is shown at the top of the form */
            width:200px;
            padding:6px 10px;
            /*border:1px solid gray;*/
            background:#27CCE4;
            /*-webkit-box-shadow: 0 0 8px #818181 ;*/
            /*box-shadow: 0 0 8px #818181;*/
            color:#FFF;
            font-size: small;
            position:absolute;
            top:-10px;
            margin-left: 20%;
        }
        .form-control, .form-control_2.input-sm{
            height: 28px;
            padding: 5px 10px;
        }

        form#AutoForm div{ /*CSS used by demo form*/
            margin-bottom:6px;
        }
        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
            padding-right: 6px;
            padding-left: 6px;
        }
        .bootstrap-select.btn-group .dropdown-toggle .filter-option{
            height: 28px;
        }
        .container{
            margin-right: auto;
            margin-left: auto;
            padding-left: 0;
            padding-right: 20px;
        }
        @media (min-width: 1200px){
            .container {
                width: 1200px;
            }
        }
        .row{
            margin-left: 0px;
            margin-right: 0px;
        }
        .alert{
            padding: 6px;
        }
    </style>

    <div id="history">
        @if(Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif
        <Div class="box box-body">
            <form id="AutoForm" method="post" data-toggle="validator" enctype="multipart/form-data" action="{{action('Member\MemberController@update')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="col-lg-9" >
                        <h4>{{trans('profile.add_members')}}</h4>
                        <div class="col-sm-12" style="padding: 15px 0px 15px 0px;border: 1px solid #dddddd;">

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.id_card')}}</span>
                                    <input type="text" id="IDCard" name="IDCard" class="form-control bfh-phone" placeholder="{{trans('profile.id_card')}}" data-format="d-dddd-ddddd-dd-d">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.title')}}</span>

                                    <select class="form-control" id="UserTitleID" name="UserTitleID">
                                        <option value="">-- {{trans('profile.title')}} *--</option>
                                        @foreach(\App\models\Title::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->UserTitleID}}" {{$rows->UserTitleID=='1'?'selected':''}}>{{$rows->UserTitle}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.n_name')}}</span>
                                    <input type="text" class="form-control input-sm" id="Nname" name="Nname" placeholder="{{trans('profile.n_name')}}" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.f_name')}}</span>

                                    <input type="text" class="form-control input-sm" id="Fname" name="Fname" placeholder="{{trans('profile.f_name')}}*" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.l_name')}}</span>

                                    <input type="text" class="form-control input-sm" id="Lname" name="Lname" placeholder="{{trans('profile.l_name')}}*" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.sex')}}</span>

                                    <select class="form-control" id="Sex" name="Sex" >

                                        <option value="">-- {{trans('profile.sex')}} *--</option>
                                        @foreach(\App\models\Sex::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->SexID}}" {{$rows->SexID=='1'?'selected':''}}>{{$rows->SexStatus}}</option>
                                        @endforeach


                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.birthdate')}}</span>

                                    <input type='text' class="form-control" id='BirthDate' name="BirthDate"  placeholder="{{trans('profile.birthdate')}}"  />
                                    <script>
                                        $(function() {
                                            $("#BirthDate").datepicker({
                                                format:'yyyy-mm-dd',
                                                autoclose:true
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.age')}}</span>
                                    <input type="text" class="form-control" id="Age" name="Age" placeholder="{{trans('profile.age')}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.status')}}</span>
                                    <select class="form-control" id="Status" name="Status">
                                        <option value="">-- {{trans('profile.status')}} *--</option>
                                        @foreach(\App\models\MaritalStatus::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->MaritalStatusID}}" {{$rows->MaritalStatusID=='1'?'selected':''}}>{{$rows->MaritalStatus}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.religion')}}</span>
                                    <select class="form-control" id="ReligionNo" name="ReligionNo">
                                        <option value="">-- {{trans('profile.religion')}} *--</option>
                                        @foreach(\App\models\Religion::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->ReligionNo}}" {{$rows->ReligionNo=='1'?'selected':''}}>{{$rows->ReligionName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.nationality')}}</span>
                                    <select class="form-control" id="NationalityNo" name="NationalityNo">
                                        <option value="">-- {{trans('profile.nationality')}} --</option>
                                        @foreach(\App\models\Nationality::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->NationalityNo}}" {{$rows->NationalityNo=='1'?'selected':''}}>{{$rows->NationalityName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.origin')}}</span>
                                    <select class="form-control" id="OriginNo" name="OriginNo">
                                        <option value="">-- {{trans('profile.nationality')}} --</option>
                                        @foreach(\App\models\Origin::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->OriginNo}}" {{$rows->OriginNo=='1'?'selected':''}}>{{$rows->OriginName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.career')}}</span>
                                    <select class="form-control" id="OccupationID" name="OccupationID">
                                        <option value="">-- {{trans('profile.career')}} --</option>
                                        @foreach(\App\models\Occupation::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->OccupationID}}">{{$rows->Occupation}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div id="Carreer" style="display: none" class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{trans('profile.career')}}</span>
                                        <input type="text" class="form-control" id="CareerName" name="CareerName" placeholder="{{trans('profile.Career')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.income')}}</span>
                                    <input type="text" class="form-control" id="Income" name="Income" placeholder="{{trans('profile.income')}}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.remark')}}</span>
                                    <textarea  rows="3" class="form-control" id="Comment" name="Comment" placeholder="{{trans('profile.remark')}}"></textarea>
                                </div>
                            </div>

                        </div>


                    </div>

                    <div class="col-lg-3 ">
                        <div class="form-group" align="center"><h4>{{trans('profile.picture')}}</h4></div>
                        <div class="form-group" align="center">
                            <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                        </div>
                        <div class="form-group"><center>
                                <input type="file"  id="imgInp" name="picture" class="filestyle" data-input="false" ></center>
                        </div>

                    </div>


                </div>




                <div class="row" id="horizontal-form">

                    <div class="col-lg-6" >
                        <h4>{{trans('profile.Domicile')}}</h4>
                        <div  class="col-sm-12" style="padding: 15px 0px 15px 0px;border: 1px solid #dddddd;  ">


                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Address')}}</span>
                                    <input type="text" class="form-control input-sm" id="Old_AddressNo" name="Old_AddressNo" placeholder="{{trans('profile.Address')}}"  >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Moo')}}</span>
                                    <input type="text" class="form-control input-sm" id="Old_Moo" name="Old_Moo" placeholder="{{trans('profile.Moo')}}">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Soi')}}</span>
                                    <input type="text" class="form-control input-sm" id="Old_Soi" name="Old_Soi" placeholder="{{trans('profile.Soi')}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Road')}}</span>
                                    <input type="text" class="form-control input-sm" id="Old_Road" name="Old_Road" placeholder="{{trans('profile.Road')}}">
                                </div>
                            </div>




                            {{--<div class="col-sm-4">--}}
                            {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon">{{trans('profile.Zone')}}</span>--}}
                            {{--<select class="form-control" id="Old_ZoneNo" name="Old_ZoneNo" >--}}
                            {{--<promotion value="">-- {{trans('profile.Zone')}} --</promotion>--}}
                            {{--@foreach($Geography as $rows)--}}
                            {{--<promotion value="{{$rows->GEO_ID}}">{{$rows->GEO_NAME}}</promotion>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Province')}}</span>
                                    <select class="form-control" id="Old_ProvinceNo" name="Old_ProvinceNo" >
                                        <option value="">-- {{trans('profile.Province')}} --</option>
                                        @foreach(\App\models\Province::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->StateID}}">{{$rows->State}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Amphur')}}</span>
                                    <select class="form-control" id="Old_AmphurNo" name="Old_AmphurNo" >
                                        <option value="">-- {{trans('profile.Amphur')}} --</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Thumbon')}}</span>
                                    <select class="form-control" id="Old_ThumbonNo" name="Old_ThumbonNo">
                                        <option value="">-- {{trans('profile.Thumbon')}} --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Village')}}</span>
                                    <input type="text" class="form-control" id="Old_Village" name="Old_Village" placeholder="{{trans('profile.Village')}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.ZipCode')}}</span>
                                    <input type="text" class="form-control input-sm" id="Old_PostCode" name="Old_PostCode" placeholder="{{trans('profile.ZipCode')}}">
                                </div>
                            </div>




                        </div>

                    </div>

                    <div class="col-lg-6">
                        <h4>{{trans('profile.CurrentAddress')}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="address_same1" id="address_same1" value="y"> <span style="font-size: small">{{trans('profile.SameAddress')}}</span></h4>
                        <div id="box_address1" class="col-sm-12" style="padding: 15px 0px 15px 0px;border: 1px solid #dddddd;  ">


                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Address')}}</span>
                                    <input type="text" class="form-control input-sm" id="AddressNo" name="AddressNo" placeholder="{{trans('profile.Address')}}" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Moo')}}</span>
                                    <input type="text" class="form-control input-sm" id="Moo" name="Moo" placeholder="{{trans('profile.Moo')}}" >
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Soi')}}</span>
                                    <input type="text" class="form-control input-sm" id="Soi" name="Soi" placeholder="{{trans('profile.Soi')}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Road')}}</span>
                                    <input type="text" class="form-control input-sm" id="Road" name="Road" placeholder="{{trans('profile.Road')}}">
                                </div>
                            </div>




                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Province')}}</span>
                                    <select class="form-control" id="ProvinceNo" name="ProvinceNo" >
                                        <option value="">-- {{trans('profile.Province')}} --</option>
                                        @foreach(\App\models\Province::where('LanguageCode',Session::get('Language'))->get() as $rows)
                                            <option value="{{$rows->StateID}}">{{$rows->State}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Amphur')}}</span>
                                    <select class="form-control" id="AmphurNo" name="AmphurNo" >
                                        <option value="">-- {{trans('profile.Amphur')}} --</option>
                                    </select>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Thumbon')}}</span>
                                    <select class="form-control" id="ThumbonNo" name="ThumbonNo">
                                        <option value="">-- {{trans('profile.Thumbon')}} --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.Village')}}</span>
                                    <input type="text" class="form-control" id="Village" name="Village" placeholder="{{trans('profile.Village')}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{trans('profile.ZipCode')}}</span>
                                    <input type="text" class="form-control input-sm" id="PostCode" name="PostCode" placeholder="{{trans('profile.ZipCode')}}">
                                </div>
                            </div>



                        </div>
                        <div id="box_same" class="col-sm-12" style="padding: 15px 0px 15px 0px;border: 1px solid #dddddd; display: none  " align="center">
                            <h2 style="padding: 15px"><i class="fa fa-arrow-left"></i> {{trans('profile.SameAddress')}}</h2>
                        </div>
                    </div>

                </div>
                <Div align="right" >
                    <a href="{{url('member/list')}}" class="btn btn-default "><i class="fa fa-reply"></i> {{trans('profile.BacktoList')}}</a>
                    <button type="reset" class="btn btn-default "><i class="fa fa-repeat"></i> {{trans('profile.Clear')}}</button>
                    <button type="submit" class="btn btn-success "><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                </Div>
            </form>
        </div>

    </div>



    <script language="javascript">
        $('#EduTypeNo').on('change',function(e){
            console.log(e);

            $.get('/ajex_edutype?EduTypeNo='+ e.target.value,function(data){
                // console.log(data);
                $('#EduLevelNo').empty();
                $('#EduLevelNo').append('<promotion>-- ระดับการศึกษา -- </promotion>');
                $.each(data ,function(index,edutype){
                    $('#EduLevelNo').append('<option value="'+edutype.EduLevelNo+'">'+edutype.EduLevelName+'</option>');
                });
            });
        });





        $('#Other').on('click',function () {
            if($('#Other').is(':checked')){
                $('#TextOther').show();
                $('#TextOther').focus();
            }else{
                $('#TextOther').hide();
            }
        });

        $('#EduLevelNo').on('change',function(e){

            if(e.target.value=='6'){ // 6 หมายถึง อื่น ๆ
                $('#EduLevel').show();
                $('#EduLevelName').focus();
            }else{
                $('#EduLevel').hide();
            }
        });

        $('#CareerNo').on('change',function(e){

            if(e.target.value=='6'){ // 6 หมายถึง อาชีพอื่น ๆ
                $('#Carreer').show();
                $('#CareerName').focus();
            }else{
                $('#Carreer').hide();
            }
        });


        $(function(){
            $("#BirthDate").on("changeDate",function(){

                var dayBirth=$(this).val();
                var getdayBirth=dayBirth.split("-");
                var YB=getdayBirth[0];
                var MB=getdayBirth[1];
                var DB=getdayBirth[2];

                var setdayBirth=moment(YB+"-"+MB+"-"+DB);
                var setNowDate=moment();
                var yearData=setNowDate.diff(setdayBirth, 'years', true); // ข้อมูลปีแบบทศนิยม
                var yearFinal=Math.round(setNowDate.diff(setdayBirth, 'years', true),0); // ปีเต็ม
                var yearReal=setNowDate.diff(setdayBirth, 'years'); // ปีจริง
                var monthDiff=Math.floor((yearData-yearReal)*12); // เดือน
                var str_year_month=yearReal+" ปี "+monthDiff+" เดือน"; // ต่อวันเดือนปี
                $("#Age").val(str_year_month);

            });

        });


        $('#address_same1').on('click',function(){
            elm=document.getElementById('address_same1');
            if(elm.checked==true){
                document.getElementById('box_address1').style.display='none';
                document.getElementById('box_same').style.display='';
            }else{
                document.getElementById('box_address1').style.display='';
                document.getElementById('box_same').style.display='none';
            }
        });






        $('#ZoneNo').on('change',function(e){
            $zone=e.target.value;
            console.log(e);
            $.get('/ajex_zone?ZoneNo='+ $zone,function(data){
                // console.log(data);
                $('#ProvinceNo').empty();
                $('#ProvinceNo').append('<promotion>เลือกจังหวัด</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#ProvinceNo').append('<option value="'+provinceObj.PROVINCE_ID+'">'+provinceObj.PROVINCE_NAME+'</option>');
                });
            });
        });

        $('#ProvinceNo').on('change',function(e){
            console.log(e);
            $.get('/ajex_province?ProvinceID='+ e.target.value,function(data){
                // console.log(data);
                $('#AmphurNo').empty();
                $('#AmphurNo').append('<promotion>เลือกอำเภอ</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#AmphurNo').append('<option value="'+provinceObj.AMPHUR_ID+'">'+provinceObj.AMPHUR_NAME+'</option>');
                });
            });        });
        $('#AmphurNo').on('change',function(e){
            console.log(e);
            $.get('/ajex_amphur?AmpherID='+ e.target.value,function(data){
                // console.log(data);
                $('#ThumbonNo').empty();
                $('#ThumbonNo').append('<promotion>เลือกตำบล</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#ThumbonNo').append('<option value="'+provinceObj.DISTRICT_ID+'">'+provinceObj.DISTRICT_NAME+'</option>');
                });
            });
            $provinceID=document.getElementById('ProvinceNo').value;
            $.get('/ajex_zipcode?AmpherID='+ e.target.value+'&ProvinceID='+$provinceID,function(data){
                // console.log(data);
                document.getElementById('PostCode').value=data.ZIPCODE;

            });
        });

        $('#Old_ZoneNo').on('change',function(e){
            $zone=e.target.value;
            console.log(e);
            $.get('/ajex_zone?ZoneNo='+ $zone,function(data){
                // console.log(data);
                $('#Old_ProvinceNo').empty();
                $('#Old_ProvinceNo').append('<promotion>เลือกจังหวัด</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#Old_ProvinceNo').append('<option value="'+provinceObj.PROVINCE_ID+'">'+provinceObj.PROVINCE_NAME+'</option>');
                });
            });
        });

        $('#Old_ProvinceNo').on('change',function(e){
            console.log(e);
            $.get('/ajex_province?ProvinceID='+ e.target.value,function(data){
                // console.log(data);
                $('#Old_AmphurNo').empty();
                $('#Old_AmphurNo').append('<promotion>เลือกอำเภอ</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#Old_AmphurNo').append('<option value="'+provinceObj.AMPHUR_ID+'">'+provinceObj.AMPHUR_NAME+'</option>');
                });
            });
        });
        $('#Old_AmphurNo').on('change',function(e){
            console.log(e);
            $.get('/ajex_amphur?AmpherID='+ e.target.value,function(data){
                // console.log(data);
                $('#Old_ThumbonNo').empty();
                $('#Old_ThumbonNo').append('<promotion>เลือกตำบล</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#Old_ThumbonNo').append('<option value="'+provinceObj.DISTRICT_ID+'">'+provinceObj.DISTRICT_NAME+'</option>');
                });
            });
            $provinceID=document.getElementById('Old_ProvinceNo').value;
            $.get('/ajex_zipcode?AmpherID='+ e.target.value+'&ProvinceID='+$provinceID,function(data){
                // console.log(data);
                document.getElementById('Old_PostCode').value=data.ZIPCODE;

            });
        });


        $('#EduProvinceNo').on('change',function(e){
            console.log(e);
            $.get('/ajex_province?ProvinceID='+ e.target.value,function(data){
                // console.log(data);
                $('#EduAmphurNo').empty();
                $('#EduAmphurNo').append('<promotion>เลือกอำเภอ</promotion>');
                $.each(data ,function(index,provinceObj){
                    $('#EduAmphurNo').append('<option value="'+provinceObj.AMPHUR_ID+'">'+provinceObj.AMPHUR_NAME+'</option>');
                });
            });
        });

    </script>

@stop()