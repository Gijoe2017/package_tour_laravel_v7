@extends('layouts.member.layout_master_new')
@section('content')
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.document_files')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <span class="pull-right"><a href="{{url('/member/document/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a> </span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">{{trans('profile.step')}} 6 / 6</span>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{trans('profile.date_upload')}}</th>
                            <th>{{trans('profile.file_name')}}</th>
                            <th>{{trans('profile.file_type')}}</th>
                            <th>{{trans('profile.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Docs as $doc)
                            <tr>
                                <td>{{date('d F, Y',strtotime($doc->created_at))}}</td>
                                <td>{{$doc->file_name}}</td>
                                <td>{{$doc->file_type}}</td>
                                <td>
                                    <a target="_blank" href="{{url('/images/member/docs/'.$doc->file_name)}}" class="btn btn-sm btn-warning"><i class="fa fa-search-plus"></i> {{trans('profile.View')}}</a>
                                    <a href="{{url('/member/document/delete/'.$doc->id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                Document list
                </div>
                </div>

                </div>
    </section>
@endsection
