@extends('layouts.member.layout_master_new')
@section('content')
    <!-- CSS -->
    {{--<link href="{{asset('/member/dropzone/style.css')}}" rel="stylesheet" type="text/css">--}}
    <link href="{{asset('/member/dropzone/dropzone.css')}}" rel="stylesheet" type="text/css">

    <!-- Script -->
    <script src='{{asset('/member/dropzone/jquery-3.2.1.min.js')}}'></script>
    <script src="{{asset('/member/dropzone/dropzone.js')}}" type="text/javascript"></script>

    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-book"></i></span>
                        <span class="title_text">{{trans('profile.document_files')}}<span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">{{trans('profile.step')}} 6 / 6</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Member\UploadController@upload')}}">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <label for="file_name" class="form-label required">{{trans('profile.add_document_file')}} </label>
                                    <hr>
                                    <input type="file"  id="imgInp" name="file" class="filestyle"  data-input="false" >
                                    <hr>
                                </div>

                                <div class="form-group" align="right">
                                    <label class="form-label required">&nbsp;</label>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> {{trans('profile.upload')}}</button>
                                </div>
                            </div>

                        </div>

                    </form>
                     <div class="col-md-12">

                         @if(count($Docs)>0)
                         <table class="table table-bordered">
                             <thead>
                             <tr>
                             <th>{{trans('profile.date_upload')}}</th>
                             <th>{{trans('profile.file_name')}}</th>
                             <th>{{trans('profile.file_type')}}</th>
                             <th>{{trans('profile.action')}}</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach($Docs as $doc)
                                 <tr>
                                     <td>{{date('d F, Y',strtotime($doc->created_at))}}</td>
                                     <td>{{$doc->file_name}}</td>
                                     <td>{{$doc->file_type}}</td>
                                     <td>
                                         <a target="_blank" href="{{url('/images/member/docs/'.$doc->file_name)}}" class="btn btn-sm btn-warning"><i class="fa fa-search-plus"></i> {{trans('profile.View')}}</a>
                                         <a href="{{url('/member/document/delete/'.$doc->id)}}" onclick="return confirm_delete()" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a>
                                     </td>
                                 </tr>
                             @endforeach
                             </tbody>
                         </table>
                             @endif
                     </div>

                    <!-- Script -->

                </div>
                <div class="box-footer">
                Document list
                </div>
                </div>

                </div>
    </section>
@endsection
