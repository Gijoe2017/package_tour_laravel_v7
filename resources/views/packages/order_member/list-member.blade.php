@extends('layouts.package.master-order')

@section('program-highlight')
    <style>
        .dropdown-menu>li>a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }
    </style>

    <section class="invoice">
        <div class="container">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="box-title"><i class="fa fa-users"></i> {{trans('profile.member_list')}} Booking No. #{{Session::get('BookingID')}}</h2>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('my/bookings')}}" class="btn btn-default pull-right"><i class="fa fa-reply"></i> {{trans('common.back_to_mybooking')}}</a>
                    </div>
                </div>
            </div>
            @if($Members->count())

                <div class="box-body">
                    @foreach($BookingDetails as $rows)
                        <div class="row">
                            <div class="col-md-12"><hr></div>
                        <div class="col-md-6">
                        <strong> {{trans('common.number_of_tourist').': '.$rows->TourType.' '.$rows->number_of_person.' '.trans('common.person')}} <br> <small>{!! $rows->package_detail_title !!}</small></strong>
                        </div>
                        <div class="col-md-6" align="right">
                            <a href="{{url('package/member/create/'.$rows->tour_type)}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a>
                            <a href="{{url('package/member/list/'.Session::get('BookingID')).'/'.$rows->tour_type}}" class="btn btn-sm btn-warning"><i class="fa fa-folder-open"></i> {{trans('profile.show_list_tourist')}}</a>
                        </div>
                        </div>
                        <?php
                        $Members=DB::table('package_tourist_member as a')
                            ->join('users_passport as b','b.passport_id','=','a.passport_id')
                            ->join('users_passport_info as c','c.passport_id','=','b.passport_id')
                            ->where('a.booking_id',Session::get('BookingID'))
                            ->where('a.tour_type',$rows->tour_type)
                            ->where('c.language_code',Auth::user()->language)
                            ->orderby('b.updated_at','desc')
                            ->groupby('b.passport_id')
                            ->get();
                        ?>
                    @if($Members->count())
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{trans('profile.identification_id')}}</th>
                            <th>{{trans('profile.FullName')}}</th>
                            <th>{{trans('profile.date_of_birth')}}</th>
                            <th>{{trans('profile.religion')}}</th>
                            <th>{{trans('profile.nationality')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($Members as $rows)
                            <tr>
                                <td>{{$rows->identification_id}}</td>
                                <td>{{\App\models\Title::where('user_title_id',$rows->user_title_id)->active()->first()->user_title}}
                                    {{$rows->first_name}} {{$rows->last_name }} {{$rows->middle_name?'('.$rows->middle_name.')':''}}</td>
                                <td>{{$rows->date_of_birth}}</td>
                                <td>{{\App\models\Religion::where('religion_id',$rows->religion_id)->active()->first()->religion}}</td>
                                <td>{{\App\models\Nationality::where('nationality_id',$rows->current_nationality_id)->active()->first()->nationality}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> {{trans('common.manage')}} </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="bg-info"><a href="{{url('package/member/change-language/'.$rows->passport_id)}}" data-toggle="modal" data-target="#modal-add-language" ><i class="fa fa-flag"></i> {{trans('profile.language')}} </a></li>
                                            <li><a href="{{url('package/member/edit/'.$rows->passport_id)}}" ><i class="fa fa-edit"></i> {{trans('profile.Edit')}} </a></li>
                                            <li><a href="{{url('package/member/edit-step2/'.$rows->passport_id)}}"><i class="fa fa-address-card"></i> {{trans('profile.passport')}}</a></li>
                                            <li><a href="{{url('package/member/edit-step3/'.$rows->passport_id)}}"><i class="fa fa-phone"></i> {{trans('profile.contact')}}</a></li>
                                            <li><a href="{{url('package/member/step4/'.$rows->passport_id)}}"><i class="fa fa-user"></i> {{trans('profile.family')}}</a></li>
                                            <li><a href="{{url('package/education/list/'.$rows->passport_id)}}"><i class="fa fa-book"></i> {{trans('profile.education')}}</a></li>
                                            <li><a href="{{url('package/document/list/'.$rows->passport_id)}}"><i class="fa fa-file"></i> {{trans('profile.document_files')}}</a></li>
                                            <li><a href="{{url('package/reference/list/'.$rows->passport_id)}}"><i class="fa fa-adjust"></i> {{trans('profile.reference')}}</a></li>
                                            <li><a href="{{url('package/occupation/list/'.$rows->passport_id)}}"><i class="fa fa-book"></i> {{trans('profile.occupation')}}</a></li>
                                            <li><a href="{{url('package/place/list/'.$rows->passport_id)}}"><i class="fa fa-map"></i> {{trans('profile.place_stay')}}</a></li>
                                            <li><a href="{{url('package/visited/list/'.$rows->passport_id)}}"><i class="fa fa-plane"></i> {{trans('profile.country_will_travel')}}</a></li>
                                            <li class="bg-orange"><a href="{{url('package/member/delete/'.$rows->passport_id)}}" onclick="return confirm_delete()"><i class="fa fa-trash"></i> {{trans('profile.Delete')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr><td colspan="6"><BR></td>
                            {{--<th>{{trans('profile.identification_id')}}</th>--}}
                            {{--<th>{{trans('profile.FullName')}}</th>--}}
                            {{--<th>{{trans('profile.date_of_birth')}}</th>--}}
                            {{--<th>{{trans('profile.Religion')}}</th>--}}
                            {{--<th>{{trans('profile.Nationality')}}</th>--}}
                        </tr>
                        </tfoot>
                    </table>
                        @endif
                    @endforeach

                </div>
            @else
                <div class="box-body">
                    <div class="row">
                    <div class="col-md-6">

                        <a href="{{url('package/member/create_start/'.Session::get('BookingID'))}}" class="btn btn-success btn-block btn-lg"> {{trans('profile.add_tourist')}}</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('package/member/list/'.Session::get('BookingID').'/0')}}" class="btn btn-warning btn-block btn-lg"><i class="fa fa-folder-open"></i> {{trans('profile.show_list_tourist')}}</a>
                    </div>
                    </div>
                </div>

            @endif
        </div>
    </section>



@endsection

