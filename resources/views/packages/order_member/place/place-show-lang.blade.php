<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Add more language.</h4>
</div>
<div class="modal-body">
    <ul class="nav nav-stacked">
        @foreach( Config::get('app.locales') as $key => $value)
            <li class="">
                <a href="#" class="switch-language-place" data-id="{{$address_stay_place_group_id}}" data-language="{{ $key }}">
                    @if($key == 'en')
                        <span class="flag-icon flag-icon-us"></span>
                    @elseif($key == 'iw')
                        <span class="flag-icon flag-icon-il"></span>
                    @elseif($key == 'ja')
                        <span class="flag-icon flag-icon-jp"></span>
                    @elseif($key == 'zh')
                        <span class="flag-icon flag-icon-cn"></span>
                    @elseif($key == 'hi')
                        <span class="flag-icon flag-icon-in"></span>
                    @elseif($key == 'fa')
                        <span class="flag-icon flag-icon-ir"></span>
                    @else
                        <span class="flag-icon flag-icon-{{ $key }}"></span>
                    @endif
                        {{ $value }}
                        <?php
                            $checklist="<span class=\"pull-right badge bg-aqua\"><i class=\"fa fa-check-circle\"></i></span>";
                        ?>
                        @foreach($Place as $rows)
                            {!! $rows->language_code==$key?$checklist:'' !!}
                        @endforeach

                </a>
            </li>
        @endforeach
    </ul>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-outline">Save changes</button>
</div>