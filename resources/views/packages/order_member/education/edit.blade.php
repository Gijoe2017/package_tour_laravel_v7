@extends('layouts.package.master-order')
@section('program-highlight')
    <section class="container">
    <link rel="stylesheet" href="{{asset('themes/defaults/assets/select1/jquery.typeahead.css')}}">
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('themes/defaults/assets/select1/jquery.typeahead.js')}}"></script>


        <style>
            .sinput-group {
                position: relative;
                display: table;
                border-collapse: separate;
            }
            .sinput-group-addon:first-child {
                border-right: 0;
            }

            .sinput-group-addon {
                padding: 6px 12px;
                font-size: 14px;
                font-weight: 400;
                line-height: 1;
                color: #555;
                text-align: center;
                background-color: #eee;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .sinput-group-addon, .sinput-group-btn {
                width: 1%;
                white-space: nowrap;
                vertical-align: middle;
            }
            .sinput-group .form-control, .sinput-group-addon, .sinput-group-btn {
                display: table-cell;
            }
            .sinput-group > .form-control, .sinput-group > .custom-select, .sinput-group > .custom-file {
                position: relative;
                -webkit-box-flex: 1;
                -ms-flex: 1 1 auto;
                flex: 1 1 auto;
                width: 99%;
                margin-bottom: 0;
            }
        </style>
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">Education <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-heading">Education Informaltion: </span>
                        <span class="step-number">{{trans('profile.step')}} 5 / 6</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Package\EducationController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="education_id" value="{{$Education->education_id}}">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                        $eduType=\App\models\EducationType::where('language_code',Auth::user()->language)->get();
                                        if(!$eduType){
                                            $eduType=\App\models\EducationType::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="education_type_id" class="form-label ">{{trans('profile.education_type')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <select class="form-control select2" name="education_type_id" id="education_type_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach($eduType as $rows)
                                                    <option value="{{$rows->education_type_id}}" {{$rows->education_type_id==$Education->education_type_id?'selected':''}} >{{$rows->education_type_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date Graduate </label>
                                        <div class="sinput-group date">
                                            <div class="sinput-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="date_graduate" id="datepicker" value="{{$Education->date_graduate}}">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="program" class="form-label required">{{trans('profile.education_program')}} </label>
                                        <input class="form-control" type="text" name="education_program" id="education_program" value="{{isset($Education->education_program)?$Education->education_program:''}}" />
                                    </div>
                                </div>

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.education_program')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="program_en" id="program_en" value="{{$Education->program_en}}" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="university_id" class="form-label required">{{trans('profile.university')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="name" id="name" value="{{$Education->program}}" required/>--}}

                                        {{--<select class="form-control select2" name="university_id" id="university_id" required>--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach(\App\models\University::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->university_id}}" {{$rows->university_id==$Education->university_id?'selected':''}} >{{$rows->university_name}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                                <div class="search-location">
                                    <div class="col-md-12">
                                        <label for="program" class="form-label required">{{trans('profile.university')}} </label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input class="js-typeahead form-control"
                                                           name="q"
                                                           type="search"
                                                           value="{{$Education->name}}"
                                                           autofocus
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none" class="new-location">

                                    <div class="col-md-6">
                                        <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                            {{ Form::label('name', trans('profile.university'), ['class' => 'control-label']) }}
                                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    {{ $errors->first('name') }}
                                                    </span>
                                            @endif
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                            {{ Form::label('category_id', trans('common.category').'<span class="text-red">*</span>', ['class' => 'control-label']) }}
                                            {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                            @if ($errors->has('category_id'))
                                                <span class="help-block">
										                {{ $errors->first('category_id') }}
									            </span>
                                            @endif
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="display: none" class="row new-location">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                                <input class="form-control" type="text" name="address" id="address"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>

                                                <select style="width: 100%" class="form-control select2" name="country_id" id="country_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                        <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 box-state">
                                            <div class="form-group">
                                                <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="state_id" id="state_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4 box-state city_id">
                                            <div class="form-group">
                                                <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="city_id" id="city_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4 box-state city_sub1_id">
                                            <div class="form-group">
                                                <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4 city_sub1_id">
                                            <div class="form-group">
                                                <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                                <input class="form-control" readonly type="text" name="zip_code" id="zip_code"  />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>--}}
                                            {{--<input class="form-control" type="text" name="address" id="address" value="{{isset($Education->address)?$Education->address:''}}" required/>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>--}}
                                            {{--<select class="form-control select2" name="country_id" id="country_id" required>--}}
                                                {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                                {{--@foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->country_id}}" {{$rows->country_id==$Education->country_id?'selected':''}} >{{$rows->country}}</promotion>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-4 box-state">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="state_id" class="form-label required">{{trans('profile.State')}}</label>--}}
                                            {{--<select  class="form-control select2" name="state_id" id="state_id">--}}
                                                {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                                {{--@foreach($state as $rows)--}}
                                                    {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$Education->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 box-state city_id">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="city_id" class="form-label required">{{trans('profile.City')}}</label>--}}
                                            {{--<select class="form-control select2" name="city_id" id="city_id">--}}
                                                {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                                {{--@foreach($city as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$Education->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-4 box-state city_sub1_id">--}}
                                        {{--<div class="form-group">--}}

                                            {{--<label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>--}}
                                            {{--<select class="form-control select2" name="city_sub1_id" id="city_sub1_id">--}}
                                                {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                                {{--@foreach($city_sub as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$Education->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 city_sub1_id">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>--}}
                                            {{--<input class="form-control" readonly type="text" name="zip_code" id="zip_code" value="{{$Education->zip_code}}" />--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                </div>







                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" align="right">
                                        <a href="{{url('package/education')}}" class="btn btn-default btn-lg pull-left"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </section>

    <script>
        function SP_source() {
            return "{{url('/')}}/";
        }
    </script>
    <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>

@endsection