@extends('theme.AdminLTE.layout_master')

@section('content-chart')
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title">Profile Form Edit</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{action('ProfileController@UserUpdate')}}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{$Customer->ID}}">
                    <input type="hidden" name="userID" value="{{$Customer->UserID}}">
                    <input type="hidden" id="keep" value="{{$Customer->email}}">
                    <div class="box-body">
                        @if (isset($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('Message'))
                            <h3 class="text-green text-center">{{Session::get('Message')}}</h3>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Code *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="Customer_Code" name="Customer_Code" value="{{$Customer->Customer_Code}}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label"> Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="Customer_Name" name="Customer_Name" value="{{$Customer->Customer_Name}}" placeholder="Customer Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Customer_Address" value="{{$Customer->Customer_Address}}" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">ZipCode</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Customer_Zipcode" value="{{$Customer->Customer_Zipcode}}" placeholder="ZipCode">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="Customer_Phone" value="{{$Customer->Customer_Phone}}" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                </div><!-- /.input group -->
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-lg-4" align="center">
                                    <img src="{{asset('public/customer/'.$Customer->avatar)}}" style="max-height: 100px" id="blah" class="img-thumbnail">
                                </div>
                                <div class="form-group col-lg-8">
                                    <label><strong>&nbsp; </strong></label>
                                    <label><strong>Picture </strong></label>
                                    <input type="file" id="imgInp" name="picture"  class="filestyle" data-buttonName="btn-primary">
                                </div>
                            </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8"><span id="errorEmail" class="text-danger"></span>
                                <input type="email" class="form-control" id="email" name="email" value="{{$Customer->email}}" placeholder="Email *" readonly>
                            </div>
                        </div>
                        <hr>
                            <h4><i class="fa fa-key"></i> Change Password</h4>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="password" name="password"  placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Confirm Password</label>
                            <div class="col-sm-8"><span id="errorPassword" class="text-danger"></span>
                                <input type="password" class="form-control" id="Confirmpassword" name="Confirmpassword"  placeholder="Confirm Password">
                            </div>
                        </div>

                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{url('customer/list')}}" class="btn btn-default"><i class="fa fa-reply"></i> Cancel</a>
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Update</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
            <!-- general form elements disabled -->

        </div>
    </div>

    <script src="{{asset('public/assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <script src="{{asset('public/assets/js/bootstrap-filestyle.min.js')}}"></script>
    <script language="javascript">

        $('#Confirmpassword').on('blur',function (e) {
            var pwd=$('#password').val();
            if(pwd!=e.target.value){
                $('#errorPassword').html('Password not mid match Please try again!');
                $('#Confirmpassword').select();
            }else{
                $('#errorPassword').html('');
            }
        });

        $('#email').on('blur',function (e) {
            var keep=document.getElementById('keep').value;

            if(e.target.value!=keep){

                $.ajax({
                    url:'{{URL::to('ajax/customer/email')}}',
                    type:'get',
                    data:{'email':e.target.value},
                    success:function (data) {
                        if(data.length>2){
                            $('#errorEmail').html(data);
                            $('#email').focus();
                        }else{
                            $('#errorEmail').html('');
                        }

                    }
                });
            }

        });

    </script>

@endsection