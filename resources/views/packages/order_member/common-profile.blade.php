@extends('layouts.package.master')

@section('program-highlight')
    <section class="section-content bg padding-y-sm">
        <div class="container">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{strtoupper($Content)}}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
            </div><!-- /.box -->
       </div>
    </div>
    </div>
    </section>

@endsection

