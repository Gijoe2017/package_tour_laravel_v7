@extends('layouts.package.master-order')
@section('program-highlight')
    <style>
        .sinput-group {
            position: relative;
            display: table;
            border-collapse: separate;
        }
        .sinput-group-addon:first-child {
            border-right: 0;
        }

        .sinput-group-addon {
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1;
            color: #555;
            text-align: center;
            background-color: #eee;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .sinput-group-addon, .sinput-group-btn {
            width: 1%;
            white-space: nowrap;
            vertical-align: middle;
        }
        .sinput-group .form-control, .sinput-group-addon, .sinput-group-btn {
            display: table-cell;
        }
        .sinput-group > .form-control, .sinput-group > .custom-select, .sinput-group > .custom-file {
            position: relative;
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            width: 99%;
            margin-bottom: 0;
        }
    </style>
    <section class="invoice">
        <div class="container">
            <div class="row">
                <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">{{trans('profile.general')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <div class="pull-right">
                            <h5>
                            <span class="step-heading">{{trans('profile.personal_informaltion')}}: </span>
                            <span class="step-number">{{trans('profile.step')}} 1 / 10</span>
                            </h5>
                        </div>
                    </h3>

                </div>
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" action="{{action('Package\OrderMemberController@save_step1')}}">
                    {{csrf_field()}}
                    <div class="col-lg-12">
                    <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label  for="inputError" class="control-label">{{trans('profile.identification_id')}} <span class="text-red">*</span><span class="text-red" id="inputError"></span> </label>
                                    <input type="text" class="form-control" name="identification_id" id="identification_id"  required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="user_title_id" class="form-label required ">{{trans('profile.title')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="user_title_id" id="user_title_id" required>
                                            <option value="">{{trans('profile.Choose')}}</option>
                                            @foreach(\App\models\Title::where('language_code',Auth::user()->language)->get() as $rows)
                                                <option value="{{$rows->user_title_id}}">{{$rows->user_title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-label required">{{trans('profile.f_name')}} <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="first_name" id="first_name" required />
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="last_name" class="form-label required">{{trans('profile.l_name')}} <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="last_name" id="last_name" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="middle_name" class="form-label">{{trans('profile.m_name')}}</label>
                                    <input class="form-control" type="text" name="middle_name" id="middle_name" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="sex_id" class="form-label required">{{trans('profile.gender')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2"  name="sex_id" id="sex_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach(\App\models\Sex::where('language_code',Auth::user()->language)->get() as $rows)
                                                <option value="{{$rows->sex_id}}" >{{$rows->sex_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
<hr>                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('profile.date_of_birth')}} <span class="text-red">*</span></label>

                                    <div class="sinput-group w-100 date">
                                        <div class="sinput-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" name="date_of_birth" id="datepicker" autocomplete="off" required>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="select-list">
                                        <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="country_of_birth_id" id="country_of_birth_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-select">
                                        <label for="marital_status_id" class="form-label required">{{trans('profile.status')}} <span class="text-red">*</span></label>
                                        <div class="select-list">
                                            <select class="form-control select2" name="marital_status_id" id="marital_status_id" required>
                                                <option value="">-- {{trans('profile.Choose')}} *--</option>
                                                @foreach(\App\models\MaritalStatus::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->marital_status_id}}" {{$rows->marital_status=='1'?'selected':''}}>{{$rows->marital_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="origin_id" class="form-label required">{{trans('profile.citizenship')}} <span class="text-red">*</span></label>
                                    <select class="form-control select2" name="origin_id" id="origin_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Origin::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->origin_id}}" >{{$rows->origin_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="religion_id" class="form-label required">{{trans('profile.religion')}} <span class="text-red">*</span></label>
                                    <select class="form-control select2" name="religion_id" id="religion_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Religion::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->religion_id}}" >{{$rows->religion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<label for="nationality_no" class="form-label required">{{trans('profile.nationality')}} <span class="text-red">*</span></label>--}}

                                    {{--<select class="form-control select2" name="nationality_id" id="nationality_id" required>--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} *--</promotion>--}}
                                        {{--@foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->nationality_id}}" >{{$rows->nationality}}</promotion>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="nationality_no" class="form-label required">{{trans('profile.current_nationality')}} <span class="text-red">*</span></label>

                                    <select class="form-control select2" name="current_nationality_id" id="current_nationality_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->nationality_id}}" >{{$rows->nationality}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="naturalization_id" class="form-label required">{{trans('profile.naturalization')}} </label>

                                    <select class="form-control select2" name="naturalization_id" id="naturalization_id" >
                                        <option value="">-- {{trans('profile.Choose')}} --</option>
                                        @foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)
                                            <option value="{{$rows->nationality_id}}" >{{$rows->nationality}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div align="center">
                        <label class="form-label">{{trans('profile.SelectPicture')}}</label></div>
                        <div class="form-group" align="center">
                            <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                        </div>
                        <div class="form-group"><center>
                                <input type="file"  id="imgInp" name="passport_cover_image" class="filestyle" accept=".jpg, .png, .jpeg, .gif" data-input="false" ></center>
                        </div>
                    </div>

                </div>



                <hr>
                <div class="form-group">
                    <a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}" class="btn btn-default btn-lg"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>

                    <button type="submit" class="btn btn-danger btn-lg pull-right"><i class="fa fa-save"></i> {{trans('profile.save_and_continue')}}</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
        </div>
    </section>


 @endsection