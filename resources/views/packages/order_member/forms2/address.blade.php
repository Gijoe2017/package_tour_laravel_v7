@extends('layouts.package.master-order')
@section('program-highlight')



    <section class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-phone"></i></span>
                        <span class="title_text">{{trans('profile.contact_informaition')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                        <span class="step-number">{{trans('profile.step')}} 3 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tap-address" data-toggle="tab"><strong><i class="fa fa-map-marker"></i> {{trans('profile.Address')}}</strong></a></li>
                            <li><a href="#tap-email" data-toggle="tab"><strong><i class="fa fa-mail-reply-all"></i> {{trans('profile.Email')}}</strong></a></li>
                            <li><a href="#tap-phone" data-toggle="tab"><strong><i class="fa fa-mobile-phone"></i> {{trans('profile.phone')}}</strong></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="tap-address">
                                <div class="box-body form-address">
                                    <div class="row address-form" style="display:none;">
                                        <form method="post" id="myform-address" action="{{action('Package\OrderMemberController@add_address')}}" >
                                            {{csrf_field()}}

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="address_type_id" class="form-label required">{{trans('profile.AddressType')}}</label>
                                                        <select class="form-control select2" style="width: 100%" name="address_type_id" id="address_type_id" required>
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach(App\models\AddressType::where('language_code',Auth::user()->language)->get() as $rows)
                                                                <option value="{{$rows->address_type_id}}" >{{$rows->address_type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-select">
                                                        <label for="address" class="form-label required">{{trans('profile.Address')}}</label>
                                                        <input class="form-control" type="text" name="address" id="address" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional" class="form-label">{{trans('profile.Additional')}}</label>
                                                        <input class="form-control" type="text" name="additional" id="additional"  />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="country_id" class="form-label required">{{trans('profile.Country')}} </label>
                                                        <select class="form-control select2" style="width: 100%" name="country_id" id="country_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            @foreach($country as $rows)
                                                                <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state">
                                                    <div class="form-group">
                                                        <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                                        <select  class="form-control select2" style="width: 100%" name="state_id" id="state_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach($state as $rows)--}}
                                                                {{--<promotion value="{{$rows->state_id}}" >{{$rows->state}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 box-state city_id">
                                                    <div class="form-group">
                                                        <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                                        <select class="form-control select2" style="width: 100%" name="city_id" id="city_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach($city as $rows)--}}
                                                                {{--<promotion value="{{$rows->city_id}}" >{{$rows->city}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="col-md-4 box-state city_sub1_id">
                                                    <div class="form-group">

                                                        <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                                        <select class="form-control select2" style="width: 100%" name="city_sub1_id" id="city_sub1_id">
                                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                                            {{--@foreach($city_sub as $rows)--}}
                                                                {{--<promotion value="{{$rows->city_sub1_id}}" >{{$rows->city_sub1_name}}</promotion>--}}
                                                            {{--@endforeach--}}
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4 city_sub1_id">
                                                    <div class="form-group">
                                                        <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                                        <input class="form-control" type="text" name="zip_code" id="zip_code" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"><label></label>
                                                        <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>


                                </div>
                                <hr>

                                <div class="row">
                                    <div id="show-address-detail" class="col-lg-12 ">
                                        <div class="col-md-6"> <label><strong><i class="fa fa-list"></i> {{trans('common.member_list')}}</strong></label></div>
                                        <div class="col-md-6"> <button class="btn btn-info pull-right add-new"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</button></div>
                                        @if(count($AddressAll))
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.AddressType')}}</th>
                                                    <th>{{trans('profile.Address')}}</th>
                                                    <th>{{trans('profile.Zip_code')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($AddressAll as $rows)
                                                    <?php
                                                        $State=DB::table('states')->where(['state_id'=>$rows->state_id,'language_code'=>Auth::user()->language])->first();
                                                        $City=DB::table('cities')->where(['city_id'=>$rows->city_id,'country_id'=>$rows->country_id,'language_code'=>Auth::user()->language])->first();
                                                        if($State){
                                                            $state=$State->state.', ';
                                                        }
                                                        if($City){
                                                            $city=$City->city.' ';
                                                        }
                                                    ?>
                                                    <tr>
                                                        <td>{{$rows->address_type}}</td>
                                                        <td>{{$rows->address.' '.$rows->additional .$city.$state.$rows->country}}</td>
                                                        <td>{{$rows->zip_code}}</td>
                                                        <td class="text-right" style="width: 15%">
                                                            <a class="btn btn-sm btn-warning" href="{{url('package/member/edit-address/'.$rows->address_id)}}" data-id="{{$rows->address_id}}" ><i class="fa fa-edit"></i>  {{trans('profile.Edit')}}</a>
                                                            <button class="btn btn-sm btn-danger order_delete_address" data-id="{{$rows->address_id}}"><i class="fa fa-trash"></i>  {{trans('profile.Delete')}}</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h5>Have no data!</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tap-email">
                                <!-- The timeline -->
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <label for="phone_number" class="form-label">{{trans('profile.Email')}}</label>
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control" >
                                            <div class="input-group-btn">
                                                <button id="order-add-email-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                                <button id="order-add-email" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                            </div>
                                        </div>
                                        <hr>
                                        <div id="show-email-detail">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.Email')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($Email as $rows)
                                                    <tr>
                                                        <td>{{$rows->email}}</td>
                                                        <td><button data-id="{{$rows->id}}" class="btn btn-sm btn-danger order_delete_email"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="tap-phone">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="phone_country_id">{{trans('profile.Country')}}</label><BR>
                                            <select class="form-control select2" name="phone_country_id" id="phone_country_id" style="width: 100%">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="phone_country_code" >{{trans('profile.phone_code')}}</label><BR>
                                            <select class="form-control select2" name="phone_country_code" id="phone_country_code" style="width: 100%">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\models\PhoneCode::all() as $rows)
                                                    <option value="{{$rows->phone_code}}">{{$rows->country}} - {{$rows->phone_code}}</option>
                                                @endforeach
                                            </select>



                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone_type" >{{trans('profile.phone_type')}}</label><BR>
                                                <select class="form-control select2" name="phone_type" id="phone_type" style="width: 100%">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    <option value="moblie">{{trans('profile.mobile')}}</option>
                                                    <option value="phone">{{trans('profile.phone')}}</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <label for="phone_number">{{trans('profile.phone_name')}}</label>
                                            <div class="input-group">
                                                <input type="text" name="phone_number" id="phone_number" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                                <div class="input-group-btn">
                                                    <button id="add-phone-none" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add more</button>
                                                    <button id="order-add-phone" style="display: none" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add more</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div id="show-phone-detail" style="display:{{isset($Phones)?'none':''}} none" class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('profile.Country')}}</th>
                                                    <th>{{trans('profile.phone_code')}}</th>
                                                    <th>{{trans('profile.phone_type')}}</th>
                                                    <th>{{trans('profile.phone_name')}}</th>
                                                    <th>{{trans('profile.action')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($Phones as $phone)
                                                    <tr>
                                                        <td>{{$phone->country_id}}</td>
                                                        <td>{{$phone->phone_country_code}}</td>
                                                        <td>{{$phone->phone_type}}</td>
                                                        <td>{{$phone->phone_number}}</td>
                                                        <td align='center'><button class="btn btn-danger btn-sm order-phone-delete" id="phone-delete" data-id="{{$phone->id}}"><i class="fa fa-trash"></i> {{trans('profile.Delete')}} </button></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>

                    <div class="form-group" align="right">
                        <a href="{{url('package/member/edit-step2')}}" class="btn btn-default btn-lg pull-left"><i class="fa fa-reply"></i> {{trans('profile.previous')}}</a>
                        <a type="button" href="{{url('package/member/step4')}}" class="continue btn btn-danger btn-lg"><i class="fa fa-arrow-circle-right"></i> {{trans('profile.continue')}}</a>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection