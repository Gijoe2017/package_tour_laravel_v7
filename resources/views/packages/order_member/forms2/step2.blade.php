@extends('layouts.package.master-order')
@section('program-highlight')
    <style>
        .input-group {
            position: relative;
            display: table;
            border-collapse: separate;
        }
        .input-group-addon:first-child {
            border-right: 0;
        }
        .input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child > .btn, .input-group-btn:first-child > .btn-group > .btn, .input-group-btn:first-child > .dropdown-toggle, .input-group-btn:last-child > .btn-group:not(:last-child) > .btn, .input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .input-group-addon {
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1;
            color: #555;
            text-align: center;
            background-color: #eee;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .input-group-addon, .input-group-btn {

            white-space: nowrap;
            vertical-align: middle;
        }
        .input-group .form-control, .input-group-addon, .input-group-btn {
            display: table-cell;
        }
        .input-group > .form-control, .input-group > .custom-select, .input-group > .custom-file {
            position: relative;
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            width: 100%;
            margin-bottom: 0;
        }
    </style>
    <section class="invoice">
        <div class="container">
        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">{{trans('profile.passport')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                        <div class="pull-right">
                            <h5>

                                <span class="step-heading">{{trans('profile.personal_informaltion')}}: </span>
                                <span class="step-number">{{trans('profile.step')}} 2 / 6</span>
                            </h5>
                        </div>
                    </h3>

                </div>
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" action="{{action('Package\OrderMemberController@save_step2')}}">
                    {{csrf_field()}}
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-md-9">

                                <div class="form-group">
                                    <label for="passport_number" class="form-label required">{{trans('profile.passport_number')}}</label>
                                    <input type="text" class="form-control" name="passport_number" id="passport_number" />
                                </div>

                                <div class="form-group">
                                    <label for="passport_country_id" class="form-label required">{{trans('profile.passport_coutry')}}</label>
                                    <div class="select-list">
                                        <select class="form-control select2" name="passport_country_id" id="passport_country_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}">{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>

                        <div class="row">
                                <div class="col-md-6">


                                        <div class="form-group">
                                            <label>{{trans('profile.passport_DOI')}}  <span class="text-red">*</span></label>

                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="passport_DOI" id="datepicker" required autocomplete="off">
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>{{trans('profile.passport_DOE')}} <span class="text-red">*</span></label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="passport_DOE" id="datepicker2" required autocomplete="off">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
</div>
                            </div>
                            <div class="col-md-3 text-center">
                                <label for="photo_for_visa" class="form-label  ">{{trans('profile.SelectPicture')}}</label>
                                <div class="form-group" align="center">
                                    <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                                </div>
                                <div class="form-group"><center>
                                        <input type="file"  id="imgInp" name="photo_for_visa" class="filestyle" accept=".jpg, .png, .jpeg, .gif" data-input="false" ></center>
                                </div>
                            </div>
                        </div>



                <hr>
                <div class="form-group" align="right">
                    <a href="{{url('package/member/edit-step1')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                    <button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-save"></i> {{trans('profile.save_and_continue')}}</button>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
        </div>
    </section>
 @endsection