@extends('layouts.package.master-order')
@section('program-highlight')
    {{--<link id="bsdp-css" href="{{asset('/bootstrap-datepicker-1.9.0/')}}/css/bootstrap-datepicker3.min.css" rel="stylesheet">--}}
    {{--<script src="{{asset('/bootstrap-datepicker-1.9.0/')}}/js/bootstrap-datepicker.min.js"></script>--}}
    {{--<script src="{{asset('/bootstrap-datepicker-1.9.0/')}}/locales/bootstrap-datepicker.es.min.js" charset="UTF-8"></script>--}}
    <section class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="title_text">{{trans('profile.general')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                    <span class="step-number">{{trans('profile.step')}} 1 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                <form id='step1' method="post" enctype="multipart/form-data" action="{{action('Package\OrderMemberController@update_step1')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="continue" id="continue">
                    <input type="hidden" name="event" value="{{$event}}">

                    <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="identification_id" class="form-label required">{{trans('profile.identification_id')}} <span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="identification_id" id="identification_id"   value="{{$Member->identification_id}}" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="select-list">
                                        <?php
                                        $Titles=\App\models\Title::where('language_code',Auth::user()->language)->get();
                                        if(count($Titles)==0){
                                            $Titles=\App\models\Title::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="user_title_id" class="form-label required ">{{trans('profile.title')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="user_title_id" id="user_title_id" required>
                                            <option value="">{{trans('profile.Choose')}}</option>
                                            @foreach($Titles as $rows)
                                                <option value="{{$rows->user_title_id}}" {{$rows->user_title_id==$Member->user_title_id?'selected':''}}>{{$rows->user_title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first_name" class="form-label required">{{trans('profile.f_name')}} <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="first_name" id="first_name" value="{{$event!='change'?$Member->first_name:''}}" required />
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="last_name" class="form-label required">{{trans('profile.l_name')}} <span class="text-red">*</span></label>
                                    <input class="form-control" type="text" name="last_name" id="last_name" value="{{$event!='change'?$Member->last_name:''}}" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="middle_name" class="form-label">{{trans('profile.m_name')}} </label>
                                    <input class="form-control" type="text" name="middle_name" id="middle_name" value="{{$event!='change'?$Member->middle_name:''}}" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    $Sex=\App\models\Sex::where('language_code',Auth::user()->language)->get();
                                    if(count($Sex)==0){
                                        $Sex=\App\models\Sex::where('language_code','en')->get();
                                    }
                                    ?>
                                    <div class="select-list">
                                        <label for="sex_id" class="form-label required">{{trans('profile.gender')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2"  name="sex_id" id="sex_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Sex as $rows)
                                                <option value="{{$rows->sex_id}}" {{$rows->sex_id==$Member->sex_id?'selected':''}} >{{$rows->sex_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-4">

                               <div class="form-group">
                                    <label>{{trans('profile.date_of_birth')}}  <span class="text-red">*</span></label>

                                    <div class="input-group date">

                                        <input type="text" class="form-control pull-right" name="date_of_birth" id="datepicker"  value="{{$Member->date_of_birth}}" required>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                    $country=\App\Country::where('language_code',Auth::user()->language)->get();
                                    if(count($country)==0){
                                        $country=\App\Country::where('language_code','en')->get();
                                    }
                                    ?>
                                    <div class="select-list">
                                        <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="country_of_birth_id" id="country_of_birth_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$Member->country_of_birth_id?'selected':''}}>{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                    $MaritalStatus=\App\models\MaritalStatus::where('language_code',Auth::user()->language)->get();
                                    if(count($MaritalStatus)==0){
                                        $MaritalStatus=\App\models\MaritalStatus::where('language_code','en')->get();
                                    }
                                    ?>
                                    <div class="form-select">
                                        <label for="marital_status_id" class="form-label required">{{trans('profile.status')}} <span class="text-red">*</span></label>
                                        <div class="select-list">
                                            <select class="form-control select2" name="marital_status_id" id="marital_status_id" required>
                                                <option value="">-- {{trans('profile.Choose')}} *--</option>
                                                @foreach($MaritalStatus as $rows)
                                                    <option value="{{$rows->marital_status_id}}" {{$rows->marital_status_id==$Member->marital_status_id?'selected':''}}>{{$rows->marital_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    $Origin=\App\models\Origin::where('language_code',Auth::user()->language)->get();
                                    if(count($Origin)==0){
                                        $Origin=\App\models\Origin::where('language_code','en')->get();
                                    }
                                    ?>
                                <label for="origin_id" class="form-label required">{{trans('profile.ethnicity')}} <span class="text-red">*</span></label>
                                    <select class="form-control select2" name="origin_id" id="origin_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach($Origin as $rows)
                                            <option value="{{$rows->origin_id}}" {{$rows->origin_id==$Member->origin_id?'selected':''}}>{{$rows->origin_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    $Religion=\App\models\Religion::where('language_code',Auth::user()->language)->get();
                                    if(count($Religion)==0){
                                        $Religion=\App\models\Religion::where('language_code','en')->get();
                                    }
                                    ?>
                                <label for="religion_id" class="form-label required">{{trans('profile.religion')}} <span class="text-red">*</span></label>
                                    <div class="form-group">
                                    <select class="form-control select2" name="religion_id" id="religion_id" required>
                                        <option value="">-- {{trans('profile.Choose')}} *--</option>
                                        @foreach($Religion as $rows)
                                            <option value="{{$rows->religion_id}}" {{$rows->religion_id==$Member->religion_id?'selected':''}}>{{$rows->religion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<label for="nationality_no" class="form-label required">{{trans('profile.nationality')}} <span class="text-red">*</span></label>--}}

                                    {{--<select class="form-control select2" name="nationality_id" id="nationality_id" required>--}}
                                        {{--<promotion value="">-- {{trans('profile.Choose')}} *--</promotion>--}}
                                        {{--@foreach(\App\models\Nationality::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                            {{--<promotion value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Member->nationality_id?'selected':''}}>{{$rows->nationality}}</promotion>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                        $Nationality=\App\models\Nationality::where('language_code',Auth::user()->language)->get();
                                        if(count($Nationality)==0){
                                            $Nationality=\App\models\Nationality::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="current_nationality_id" class="form-label required">{{trans('profile.current_nationality')}} <span class="text-red">*</span></label>

                                        <select class="form-control select2" name="current_nationality_id" id="current_nationality_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Member->current_nationality_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="naturalization_id" class="form-label required">{{trans('profile.nationality')}} </label>
                                        <select class="form-control select2" name="naturalization_id" id="naturalization_id">
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Member->naturalization_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                    </div>
                    </div>
                    <div class="col-md-3">
                        <div align="center">
                        <label class="form-label">{{trans('profile.photo_request_for_visa')}}</label></div>
                        <div class="form-group" align="center">
                            @if($Member->passport_cover_image)
                                <img id="blah" src="{{asset('images/member/cover/'.$Member->passport_cover_image)}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                            @else
                                <img id="blah" src="{{asset('images/member/user-plus.png')}}" style="max-height: 180px" class="img-responsive img-thumbnail">
                            @endif

                        </div>
                        <div class="form-group"><center>
                                <input type="file"  id="imgInp" name="passport_cover_image" class="filestyle" accept=".jpg, .png, .jpeg, .gif" data-input="false" > </center>
                        </div>
                    </div>
                </div>
                <hr>
                @if(Session::has('message'))
                   <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                @endif
                <div class="form-group" align="left">
                    <a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}" class="btn btn-default btn-lg"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                    <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                    <a href="#" id="continue-2" class="btn btn-danger btn-lg pull-right"><i class="fa fa-arrow-right"></i> {{trans('profile.update_and_continue')}}</a>
                </div>

        </div>
                </form>
        </div>
            </div>
            </div>
    </section>

    <script>
        $('#continue-2').on('click',function () {
            $('#continue').val('Yes');
            $('#step1').submit();
        });
    </script>

 @endsection