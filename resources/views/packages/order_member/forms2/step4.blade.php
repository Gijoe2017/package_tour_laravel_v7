@extends('layouts.package.master-order')
@section('program-highlight')
    <section class="container">

        <div class="row">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-users"></i></span>
                        <span class="title_text">{{trans('profile.family_informaition')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                    <span class="step-number">{{trans('profile.step')}} 4 / 10</span>
                        </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-red ">

                                </div>
                                <a href="{{url('member/family/create')}}">
                                <div class="widget-user-image">
                                    <img class="img-circle" src="{{asset('images/member/user-plus.png')}}" alt="User Avatar">
                                </div>
                                </a>
                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 border-right">
                                            <div class="description-block">

                                            </div>
                                            <!-- /.description-block -->
                                        </div>

                                    </div>

                                    <div >
                                        <a href="{{url('package/member/family/create')}}" class="btn btn-success btn-block btn-xs"><i class="fa fa-plus"></i> {{trans('profile.add_new')}}</a>
                                    </div>

                                    <!-- /.row -->
                                </div>

                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <?php $i=0;?>
                        @foreach($Families as $rows)
                        <?php if($i==4){$i=0;}
                        $i++;
                        ?>
                                <!-- /.col -->
                        <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header {{$bgColor[$i]}} ">
                                    <h4 class="widget-user-username text-center">
                                        {{$rows->first_name.' '.$rows->last_name}}
                                    </h4>
                                    <h5 class="widget-user-desc"></h5>
                                </div>
                                <div class="widget-user-image">
                                        <img class="img-circle" src="{{asset('images/member/user-plus.png')}}" alt="User Avatar">
                                </div>
                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header">{{trans('profile.relation')}}: {{$rows->relation_type}}</h5>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>

                                    </div>

                                    <div >
                                        <a href="{{url('package/member/family/edit/'.$rows->relation_id)}}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"></i> {{trans('profile.update')}}</a>
                                        <a href="{{url('package/member/family/change-language/'.$rows->relation_id)}}"  class="btn btn-info btn-xs">
                                            <i class="fa fa-flag"></i> {{trans('profile.language')}}</a>
                                        <a class="btn btn-default btn-xs pull-right" href="{{url('package/member/family/delete/'.$rows->relation_id)}}" onclick="return confirm_delete()">
                                            <i class="fa fa-trash"></i> {{trans('profile.Delete')}}
                                        </a>
                                    </div>

                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <!-- /.col -->
                        @endforeach


                                <!-- /.col -->
                    </div>
                </div>
                <div class="col-md-12">
                    <hr>
                    <a href="{{url('/package/member/edit-step3')}}" class="btn btn-default" ><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                    <a href="{{url('/package/education/list')}}" class="btn btn-danger pull-right" ><i class="fa fa-forward"></i> {{trans('common.continuous')}}</a>
                    </hr>
                </div>

            </div>


            </div>




    </section>
 @endsection