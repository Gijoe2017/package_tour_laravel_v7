@extends('layouts.package.master-order')
@section('program-highlight')

    <style>
        /* The container */
        .container-check {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container-check input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container-check:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-check input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-check input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-check .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>

    <section class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                <h4 class="modal-title">+Add more language.</h4>
            </div>
            </div>
                <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
    @foreach( Config::get('app.locales') as $key => $value)
        @if($key == 'en')
            <span class="flag-icon flag-icon-us"></span>
        @elseif($key == 'iw')
            <span class="flag-icon flag-icon-il"></span>
        @elseif($key == 'ja')
            <span class="flag-icon flag-icon-jp"></span>
        @elseif($key == 'zh')
            <span class="flag-icon flag-icon-cn"></span>
        @elseif($key == 'hi')
            <span class="flag-icon flag-icon-in"></span>
        @elseif($key == 'fa')
            <span class="flag-icon flag-icon-ir"></span>
        @else
            <span class="flag-icon flag-icon-{{ $key }}"></span>
        @endif
            @foreach($FamilyLang as $rows)
               <?php $rows->language_code==$key?$checked='checked':$checked='' ?>
            @endforeach
        <div class="col-md-3">
        <label class="container-check">{{$value}}
            <input type="checkbox" {{$checked}}  value="{{$value}}">
            <span class="checkmark"></span>
        </label>
        </div>
    @endforeach

</div>
                    <div class="col-md-12"><hr>
                        <a href="{{url('package/member/step4')}}" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                        <button type="button" class="btn btn-info  pull-right">Save changes</button>
                    </div>
                    </div>
                </div>
        </div>
    </section>
    @endsection