@extends('layouts.package.master-order')
@section('program-highlight')
    <link rel="stylesheet" href="{{asset('themes/defaults/assets/select1/jquery.typeahead.css')}}">
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('themes/defaults/assets/select1/jquery.typeahead.js')}}"></script>

    <section class="container">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">{{trans('profile.family_informaition')}} <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right">
                        <span class="step-number">{{trans('profile.step')}} 4 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Package\FamilyController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="relation_id" value="{{$Family->relation_id}}">
                        <input type="hidden" name="event" value="{{$event}}">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $Relation=\App\models\Relation::where('language_code',Auth::user()->language)->get();
                                        if(count($Relation)==0){
                                            $Relation=\App\models\Relation::where('language_code','en')->get();
                                        }

                                        $timeline=\App\Timeline::where('id',$Family->place_of_birth)->first();
                                       // dd($timeline);
                                        ?>
                                        <label for="relation_type_id" class="form-label ">{{trans('profile.relation')}} <span class="text-red">*</span></label>
                                        <div class="select-list required">
                                            <select class="form-control select2" name="relation_type_id" id="relation_type_id" required>
                                                <option value="">{{trans('profile.Choose')}}</option>
                                                @foreach($Relation as $rows)
                                                    <option value="{{$rows->relation_type_id}}" {{$rows->relation_type_id==$Family->relation_type_id?'Selected':''}}>{{$rows->relation_type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_first_name" class="form-label required">{{trans('profile.f_name')}} <span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="first_name" id="first_name" value="{{$event!='change'?$Family->first_name:''}}" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_last_name" class="form-label required">{{trans('profile.l_name')}} <span class="text-red">*</span></label>
                                        <input class="form-control" type="text" name="last_name" id="last_name" value="{{$event!='change'?$Family->last_name:''}}" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="family_middle_name" class="form-label">{{trans('profile.n_name')}} </label>
                                        <input class="form-control" type="text" name="middle_name" id="middle_name" value="{{$event!='change'?$Family->middle_name:''}}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $country=\App\Country::where('language_code',Auth::user()->language)->get();
                                        if(count($country)==0){
                                            $country=\App\Country::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="country_of_birth_id" class="form-label required">{{trans('profile.passport_birthdate_coutry')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="country_of_birth_id" id="country_of_birth_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($country as $rows)
                                                <option value="{{$rows->country_id}}" {{$rows->country_id==$Family->country_of_birth_id?'Selected':''}}>{{$rows->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                        <div class="search-location">
                                        <label for="place_of_birth" class="form-label required">{{trans('profile.place_of_birth')}} <span class="text-red">*</span></label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input class="js-typeahead form-control" name="q" value="{{$timeline->name}}" type="search" autofocus autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        {{--<input  class="form-control" type="text" name="place_of_birth" id="place_of_birth" value="{{$Family->place_of_birth}}" required />--}}
                                </div>

                                <div style="display: none" class=" new-location">
                                    <div class="col-md-6">
                                        <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                            {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        {{ $errors->first('name') }}
                                        </span>
                                            @endif
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                            {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                            {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                            @if ($errors->has('category_id'))
                                                <span class="help-block">
                                            {{ $errors->first('category_id') }}
                                        </span>
                                            @endif
                                        </fieldset>
                                    </div>
                                </div>

                                <div style="display: none" class="row hide-address">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                                <input class="form-control" type="text" name="address" id="address" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>
                                                <select style="width: 100%" class="form-control select2" name="country_id" id="country_id" >
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                        <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 box-state">
                                            <div class="form-group">
                                                <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="state_id" id="state_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4 box-state city_id">
                                            <div class="form-group">
                                                <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="city_id" id="city_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 box-state city_sub1_id">
                                            <div class="form-group">
                                                <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                                <select style="width: 100%" class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                    <option value="">-- {{trans('profile.Choose')}} --</option>
                                                    {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                    {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                    {{--@endforeach--}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 city_sub1_id">
                                            <div class="form-group">
                                                <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                                <input class="form-control" readonly type="text" name="zip_code" id="zip_code"  />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $Religion=\App\models\Religion::where('language_code',Auth::user()->language)->get();
                                        if(count($Religion)==0){
                                            $Religion=\App\models\Religion::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="religion_id" class="form-label required">{{trans('profile.religion')}} <span class="text-red">*</span></label>
                                        <select class="form-control select2" name="religion_id" id="religion_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Religion as $rows)
                                                <option value="{{$rows->religion_id}}" {{$rows->religion_id==$Family->religion_id?'Selected':''}} >{{$rows->religion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                        $Nationality=\App\models\Nationality::where('language_code',Auth::user()->language)->get();
                                        if(count($Nationality)==0){
                                            $Nationality=\App\models\Nationality::where('language_code','en')->get();
                                        }
                                        ?>
                                        <label for="nationality_no" class="form-label required">{{trans('profile.nationality')}} <span class="text-red">*</span></label>
                                        <select class="form-control" name="nationality_id" id="nationality_id" required>
                                            <option value="">-- {{trans('profile.Choose')}} --</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Family->nationality_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="prev_nationality_id" class="form-label required">{{trans('profile.previous_nationality')}}</label>
                                        <select class="form-control select2" name="prev_nationality_id" id="prev_nationality_id">
                                            <option value="">-- {{trans('profile.Choose')}} *--</option>
                                            @foreach($Nationality as $rows)
                                                <option value="{{$rows->nationality_id}}" {{$rows->nationality_id==$Family->prev_nationality_id?'selected':''}}>{{$rows->nationality}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" align="right">
                                        <a href="{{url('package/member/step4')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.Save')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </section>

    <script>
        function SP_source() {
            return "{{url('/')}}/";
        }
    </script>
    <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>
@endsection