<section class="invoice">
    <div class="container">

            <div class="box-body">
                <div class="col-md-6">

                    <a href="{{url('package/member/create')}}" class="btn btn-outline-success btn-block btn-lg"> {{trans('profile.add_tourist')}}</a>
                </div>
                <div class="col-md-6">
                    <a href="{{url('package/member/list/'.Session::get('BookingID'))}}" class="btn btn-outline-warning btn-block btn-lg"><i class="fa fa-folder-open"></i> {{trans('profile.show_list_tourist')}}</a>
                </div>
            </div>


    </div>
</section>