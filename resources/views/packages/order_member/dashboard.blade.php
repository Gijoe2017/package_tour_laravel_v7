@extends('layouts.member.layout_master_new')

@section('content-chart')
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

        @foreach(\App\ModuleType::active()->get() as $module)
            <?php
                $checkRole=\App\ModuleVerified::where('timeline_id',Session::get('timeline_id'))
                    ->where('module_type_id',$module->module_type_id)
                    ->active()
                    ->count();
            ?>

        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box {{$checkRole>0?$bgColor[1]:$bgColor[2]}}">
                <div class="inner">
                    @if($module->module_type_id==1)
                    <h3>{{Session::get('Packages')}}</h3>
                    @else
                        <h3>150</h3>
                        @endif
                    <p>{{$module->module_name}}</p>
                </div>
                <div class="icon">
                    <i class="fa {{$module->module_icon}}"></i>
                </div>
                @if($checkRole>0)
                    <a href="{{url('module/manage/'.$module->module_type_id)}}" class="small-box-footer">Manage Module <i class="fa fa-arrow-circle-right"></i></a>
                @else
                    <a href="{{url('module/register/'.$module->module_type_id)}}" class="small-box-footer">Open Module <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>

        @endforeach
        {{--<!-- ./col -->--}}
        {{--<div class="col-lg-3 col-xs-6">--}}
            {{--<!-- small box -->--}}
            {{--<div class="small-box bg-green">--}}
                {{--<div class="inner">--}}
                    {{--<h3>53<sup style="font-size: 20px">%</sup></h3>--}}

                    {{--<p>Bounce Rate</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="ion ion-stats-bars"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- ./col -->--}}
        {{--<div class="col-lg-3 col-xs-6">--}}
            {{--<!-- small box -->--}}
            {{--<div class="small-box bg-yellow">--}}
                {{--<div class="inner">--}}
                    {{--<h3>44</h3>--}}

                    {{--<p>User Registrations</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="ion ion-person-add"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- ./col -->--}}
        {{--<div class="col-lg-3 col-xs-6">--}}
            {{--<!-- small box -->--}}
            {{--<div class="small-box bg-red">--}}
                {{--<div class="inner">--}}
                    {{--<h3>65</h3>--}}

                    {{--<p>Unique Visitors</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="ion ion-pie-graph"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- ./col -->--}}
    </div>
    <!-- /.row -->


</section>
<!-- /.content -->

    @endsection