@extends('layouts.package.master-order')
@section('program-highlight')
    <section class="container">
    <link rel="stylesheet" href="{{asset('themes/defaults/assets/select1/jquery.typeahead.css')}}">
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="{{asset('themes/defaults/assets/select1/jquery.typeahead.js')}}"></script>



        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        <span class="title_text">{{trans('profile.travel_history')}}  <span class="text-success"> {{Session::get('member_info')}}</span></span>
                    </h3>
                    <div class="pull-right"><a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}"><i class="fa fa-reply"></i> {{trans('profile.back_to_list')}}</a> |
                        <span class="step-number">{{trans('profile.step')}} 10 / 10</span>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" action="{{action('Package\VisitedController@update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="visited_id" value="{{$Visited->visited_id}}">
                        <div class="col-md-12">
                            <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="visa_type_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>
                                            <select class="form-control select2" name="visa_issued_country_id" id="visa_issued_country_id" required>
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}" {{$rows->country_id==$Visited->country_id?'selected':''}}>{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="visa_type_id" class="form-label required">{{trans('profile.visa_type')}} <span class="text-red">*</span></label>
                                            <select class="form-control select2" name="visa_type_id" id="visa_type_id" required>
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach($VisaType as $rows)
                                                    <option value="{{$rows->visa_type_id}}" {{$rows->visa_type_id==$Visited->visa_type_id?'selected':''}} >{{$rows->visa_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="program" class="form-label required">{{trans('profile.visa_number')}} <span class="text-red">*</span></label>
                                            <input class="form-control" type="text" name="visa_number" id="visa_number" value="{{$Visited->visa_number}}" required/>
                                        </div>
                                    </div>

                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label>{{trans('profile.visa_doi')}}<span class="text-red">*</span></label>

                                            <div class="input-group date">

                                                <input type="text" class="form-control pull-right" name="visa_doi" id="datepicker" value="{{$Visited->visa_doi}}" autocomplete="off" required>
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>{{trans('profile.visa_doe')}}  <span class="text-red">*</span></label>

                                            <div class="input-group date">

                                                <input type="text" class="form-control pull-right" name="visa_doe" id="datepicker2" value="{{$Visited->visa_doe}}" required>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>

                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.location_will_or_visited')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="name" id="name" value="{{isset($Visited->name)?$Visited->name:''}}" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>--}}
                                        {{--<input class="form-control" type="text" name="address" id="address" value="{{isset($Visited->address)?$Visited->address:''}}" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>--}}

                                        {{--<select class="form-control" name="country_id" id="country_id" required>--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($country as $rows)--}}
                                                {{--<promotion value="{{$rows->country_id}}" {{$rows->country_id==$Visited->country_id?'selected':''}} >{{$rows->country}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 box-state">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="state_id" class="form-label required">{{trans('profile.State')}}</label>--}}
                                        {{--<select  class="form-control select2" name="state_id" id="state_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($state as $rows)--}}
                                            {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$Visited->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 box-state city_id">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="city_id" class="form-label required">{{trans('profile.City')}}</label>--}}
                                        {{--<select class="form-control select2" name="city_id" id="city_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($city as $rows)--}}
                                            {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$Visited->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 box-state city_sub1_id">--}}
                                    {{--<div class="form-group">--}}

                                        {{--<label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>--}}
                                        {{--<select class="form-control select2" name="city_sub1_id" id="city_sub1_id">--}}
                                            {{--<promotion value="">-- {{trans('profile.Choose')}} --</promotion>--}}
                                            {{--@foreach($city_sub as $rows)--}}
                                            {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$Visited->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 city_sub1_id">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>--}}
                                        {{--<input class="form-control" type="text" name="zip_code" id="zip_code" value="{{$Visited->zip_code}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                            {{--</div>--}}
                            <div class="row">

                            <div class="search-location">
                                <div class="col-md-12">
                                    <label for="program" class="form-label required">{{trans('profile.location_will_or_visited')}} </label>
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
                                            <div class="typeahead__query">
                                                <input class="js-typeahead form-control" name="q" type="search" value="{{$Visited->name}}" autofocus autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </div>

                            <div style="display: none" class="row new-location">

                                <div class="col-md-6">
                                    <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                        {{ Form::label('name', trans('profile.location_will_or_visited'), ['class' => 'control-label']) }}
                                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                    {{ $errors->first('name') }}
                                                    </span>
                                        @endif
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                        {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
										                {{ $errors->first('category_id') }}
									            </span>
                                        @endif
                                    </fieldset>
                                </div>
                            </div>
                            <div style="display: none" class="row new-location">


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="program" class="form-label required">{{trans('profile.Address')}} <span class="text-red">*</span></label>
                                            <input class="form-control" type="text" name="address" id="address"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="country_id" class="form-label required">{{trans('profile.Country')}} <span class="text-red">*</span></label>

                                            <select style="width: 100%" class="form-control select2" name="country_id" id="country_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                @foreach(\App\Country::where('language_code',Auth::user()->language)->get() as $rows)
                                                    <option value="{{$rows->country_id}}" >{{$rows->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state">
                                        <div class="form-group">
                                            <label for="state_id" class="form-label required">{{trans('profile.State')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="state_id" id="state_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\State::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->state_id}}" {{$rows->state_id==$AddressInfo->state_id?'selected':''}}>{{$rows->state}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_id">
                                        <div class="form-group">
                                            <label for="city_id" class="form-label required">{{trans('profile.City')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_id" id="city_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\City::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_id}}" {{$rows->city_id==$AddressInfo->city_id?'selected':''}}>{{$rows->city}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 box-state city_sub1_id">
                                        <div class="form-group">
                                            <label for="city_sub1_id" class="form-label required">{{trans('profile.CitySub')}}</label>
                                            <select style="width: 100%" class="form-control select2" name="city_sub1_id" id="city_sub1_id">
                                                <option value="">-- {{trans('profile.Choose')}} --</option>
                                                {{--@foreach(App\CitySub1::where('language_code',Auth::user()->language)->get() as $rows)--}}
                                                {{--<promotion value="{{$rows->city_sub1_id}}" {{$rows->city_sub1_id==$AddressInfo->city_sub1_id?'selected':''}}>{{$rows->city_sub1_name}}</promotion>--}}
                                                {{--@endforeach--}}
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 city_sub1_id">
                                        <div class="form-group">
                                            <label for="zip_code" class="form-label">{{trans('profile.ZipCode')}}</label>
                                            <input class="form-control" readonly type="text" name="zip_code" id="zip_code"  />
                                        </div>
                                    </div>


                            </div>


                            <hr>
                            @if(Session::has('message'))
                                <h3 class="alert alert-success">{{Session::get('message')}}</h3>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" align="right">
                                        <a href="{{url('member/visited')}}" class="btn btn-warning btn-lg pull-left"><i class="fa fa-arrow-circle-left"></i> {{trans('profile.previous')}}</a>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> {{trans('profile.update')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

    <script>
        function SP_source() {
            return "{{url('/')}}/";
        }
    </script>
    <script src="{{asset('member/assets/bootstrap/js/app_search.js')}}"></script>

@endsection