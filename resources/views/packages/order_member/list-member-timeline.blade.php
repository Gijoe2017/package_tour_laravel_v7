@extends('layouts.package.master-order')

@section('program-highlight')
    <style>
        .dropdown-menu>li>a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }
    </style>

    <section class="invoice">
        <div class="container">
            @if($Members->count())
                <div class="box-header">
                    <div class="row">
                    <div class="col-md-6">
                        <h2 class="box-title"><i class="fa fa-users"></i> {{trans('profile.member_list_timeline')}} </h2>
                    </div>
                    <div class="col-md-6" align="right">
                        <a href="{{url('package/add/tour/info/'.Session::get('BookingID'))}}" class="btn btn-default"><i class="fa fa-reply"></i> {{trans('common.back_to_list')}}</a>
                    </div>
                    </div>
                </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                       <thead>
                       <tr>
                           <th>{{trans('profile.identification_id')}}</th>
                           <th>{{trans('profile.FullName')}}</th>
                           <th>{{trans('profile.date_of_birth')}}</th>
                           <th>{{trans('profile.religion')}}</th>
                           <th>{{trans('profile.nationality')}}</th>
                           <th></th>
                       </tr>
                       </thead>
                       <tbody>

                       @foreach($Members as $rows)
                       <tr>
                           <td>{{$rows->identification_id}}</td>
                           <td>{{\App\models\Title::where('user_title_id',$rows->user_title_id)->active()->first()->user_title}}
                               {{$rows->first_name}} {{$rows->last_name }} {{$rows->middle_name?'('.$rows->middle_name.')':''}}</td>
                           <td>{{$rows->date_of_birth}}</td>
                           <td>{{\App\models\Religion::where('religion_id',$rows->religion_id)->active()->first()->religion}}</td>
                           <td>{{\App\models\Nationality::where('nationality_id',$rows->current_nationality_id)->active()->first()->nationality}}</td>
                           <td>
                               <button id="bt-{{$rows->passport_id}}" class="btn btn-info btn-sm select-member" data-id="{{$rows->passport_id}}" ><i class="fa fa-check-circle"></i> {{trans('profile.select')}}</button>
                           </td>
                       </tr>
                        @endforeach

                       </tbody>
                       <tfoot>
                       <tr><th colspan="6"></th>
                           {{--<th>{{trans('profile.identification_id')}}</th>--}}
                           {{--<th>{{trans('profile.FullName')}}</th>--}}
                           {{--<th>{{trans('profile.date_of_birth')}}</th>--}}
                           {{--<th>{{trans('profile.Religion')}}</th>--}}
                           {{--<th>{{trans('profile.Nationality')}}</th>--}}
                       </tr>
                       </tfoot>
                   </table>
                </div>
                @else
                <div class="box-body">
                    <div class="col-md-6">
                        <a href="{{url('package/member/create')}}" class="btn btn-outline-success btn-block btn-lg"> {{trans('profile.add_tourist')}}</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('package/member/list')}}" class="btn btn-outline-warning btn-block btn-lg"><i class="fa fa-folder-open"></i> {{trans('profile.show_list_tourist')}}</a>
                    </div>
                </div>

            @endif
        </div>
    </section>

    <script>
        $('body').on('click','.select-member',function () {
            var passport_id=$(this).data('id');
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/add/member/booking',
                data:{'passport_id':passport_id},
                success:function(data){
                   // alert(data);
                    if(data=='Not'){
                        alert('{{trans('profile.booking_is_complete')}}');
                    }else{
                        $('#bt-'+passport_id).html('<i class="fa fa-check-circle"></i> {{trans('profile.selected')}}')
                        $('#bt-'+passport_id).removeClass( "btn-info" ).addClass( "btn-default" );
                    }
                }
            });
        });
    </script>

    @endsection