@extends('layouts.package.master-tour-booking')
@section('contents')
<div class="container main-container headerOffset">
    <div class="row">
        <div class="breadcrumbDiv col-lg-12">
            <ul class="breadcrumb">
                <li><a href="{{ url(Session::get('Language').'/location/list') }}">Home</a></li>
                <li><a href="{{ url(Session::get('Language').'/booking/order') }}">Cart</a></li>
                <li class="active"> Checkout</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-6 col-xxs-12 text-center-xs">
            <h1 class="section-title-inner"><span><i class="glyphicon glyphicon-shopping-cart"></i> Checkout</span></h1>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar col-xs-6 col-xxs-12 text-center-xs">
            <h4 class="caps" style="cursor:pointer"><i class="fa fa-chevron-left"></i> <span onclick="goBack()">Back to shopping</span> </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="row userInfo">
                <div class="col-xs-12 col-sm-12">
                    @include('Package-Tour.linkmenu')
                    <form id="f_billing" action="{{ url(Session::get('Language').'/booking/payments') }}" method="post">
                    <div class="w100 clearfix">
                        {{ csrf_field() }}
                        <div class="row userInfo">
                            <div class="col-lg-12">
                                <h2 class="block-title-2"> To add a billing address, please fill out the form below. </h2>
                            </div>                            
                                <div class="col-xs-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-1">
                                            <input type="checkbox" name="same" id="same" value="1" class="form-control">
                                        </div> 
                                        <div class="col-lg-11">
                                            <label class="checkbox-inline" for="same">
                                                 My delivery and billing addresses are the same.
                                            </label>
                                        </div>
                                    </div>                        
                                    <hr>
                                </div>                            
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group required">
                                    <label for="InputName">First Name <sup>*</sup> </label>
                                    <input required type="text" class="form-control" id="InputName" placeholder="First Name" name="InputName">
                                </div>
                                
                                <div class="form-group required">
                                    <label for="InputLastName">Last Name <sup>*</sup> </label>
                                    <input required type="text" class="form-control" id="InputLastName" placeholder="Last Name" name="InputLastName">
                                </div>
                                <div class="form-group">
                                    <label for="InputEmail">Email </label>
                                    <input type="text" class="form-control" id="InputEmail" placeholder="Email" name="InputEmail">
                                </div>
                                <div class="form-group">
                                    <label for="InputCompany">Company </label>
                                    <input type="text" class="form-control" id="InputCompany" placeholder="Company">
                                </div>
                                <div class="form-group required">
                                    <label for="InputAddress">Address <sup>*</sup> </label>
                                    <input required type="text" class="form-control" id="InputAddress" placeholder="Address" name="InputAddress">
                                </div>
                                <div class="form-group">
                                    <label for="InputAddress2">Address (Line 2) </label>
                                    <input type="text" class="form-control" id="InputAddress2" placeholder="Address" name="InputAddress2">
                                </div>
                                <div class="form-group required">
                                    <label for="InputCity">City <sup>*</sup> </label>
                                    <input required type="text" class="form-control" id="InputCity" placeholder="City" name="InputCity">
                                </div>
                                <div class="form-group required">
                                    <label for="InputState">State <sup>*</sup> </label>
                                    <select class="form-control" required aria-required="true" id="InputState" name="InputState">
                                       <option value="">
                                            {{trans('profile.Choose')}}{{trans('profile.State')}}
                                        </option>
                                        @if(count($State))
                                            @if($Account==0)
                                                @foreach($State as $rows)
                                                    <option value="{{$rows->StateID}}"> {{$rows->State}} </option>
                                                @endforeach
                                            @else
                                                @foreach($State as $rows)
                                                    <option value="{{$rows->StateID}}" {{$rows->StateID==$Account->StateID?'selected':''}}> {{$rows->State}} </option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group required">
                                    <label for="InputZip">Zip / Postal Code <sup>*</sup> </label>
                                    <input required type="text" class="form-control" id="InputZip"
                                           placeholder="Zip / Postal Code">
                                </div>
                                <div class="form-group required">
                                    <label for="InputCountry">Country <sup>*</sup> </label>
                                    <select class="form-control" required aria-required="true" id="InputCountry" name="InputCountry">
                                        <option value=""> {{trans('profile.Choose')}}{{trans('profile.Country')}} </option>
                                        @if(count($Country))
                                            @if($Account==0)
                                                @foreach($Country as $rows)
                                                    <option value="{{$rows->CountryID}}"> {{$rows->Country}} </option>
                                                @endforeach
                                            @else
                                                @foreach($Country as $rows)
                                                    <option value="{{$rows->CountryID}}" {{$rows->CountryID==$Account->CountryID?'selected':''}}> {{$rows->Country}} </option>
                                                @endforeach                                                    
                                            @endif
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="InputAdditionalInformation">Additional information</label>
                                    <textarea rows="3" cols="26" name="InputAdditionalInformation" class="form-control"
                                              id="other"></textarea>
                                </div>
                                <div class="form-group required">
                                    <label for="InputMobile">Mobile phone <sup>*</sup></label>
                                    <input required type="tel" name="InputMobile" class="form-control" id="InputMobile">
                                </div>
                                <div class="form-group required">
                                    <label for="addressAlias">Please assign an address title for future reference. <sup>*</sup></label>
                                    <input required type="text" value="My address" name="addressAlias" class="form-control" id="addressAlias">
                                </div>
                            </div>
                        </div>
                        <!--/row end-->

                    </div>
                    <div class="cartFooter w100">
                        <div class="box-footer">

                            <div class="pull-left">
                                <a class="btn btn-default" href="{{ url(Session::get('Language').'/booking/address') }}"> <i class="fa fa-arrow-left"></i>
                                    &nbsp; Address </a></div>
                            <button type="submit" class="btn btn-primary btn-small pull-right">Payment <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                    <!--/ cartFooter -->
                    </form>
                </div>
            </div>
            <!--/row end-->

        </div>
        
        @include('Package-Tour.total')
        <!--/rightSidebar-->

    </div>
    <!--/row-->

    <div style="clear:both"></div>
</div>
<script>
function goBack() {
    window.location.href = "{{ url(Session::get('Language').'/package/list')}}";
}
</script>
<script type="text/javascript"> 
                                $("#same").change(function () {
                                    if($("#same").is(':checked')){
                                       $('#InputName').val("{{session('first_name')}}");
                                       $('#InputLastName').val("{{session('last_name')}}");
                                       $('#InputEmail').val("{{session('email')}}");
                                       $('#InputCompany').val("{{session('company')}}");
                                       $('#InputAddress').val("{{session('address')}}");
                                       $('#InputAddress2').val("{{session('address2')}}");
                                       $('#InputCity').val("{{session('city')}}");
                                       $('#InputState').val("{{session('state_id')}}");
                                       $('#InputState').change();
                                       $('#InputCountry').val("{{session('country_id')}}");
                                       $('#InputCountry').change();
                                       $('#InputZip').val("{{session('zipcode')}}");
                                       $('#InputMobile').val("{{session('tel')}}");
                                       $('#InputAdditionalInformation').val("{{session('info')}}");
                                       $('#addressAlias').val("{{session('address_alias')}}");
                                       // alert($('#InputCountry').val());
                                    }else{
                                        $('#InputName').val("");
                                        $('#InputLastName').val("");
                                       $('#InputEmail').val("");
                                       $('#InputCompany').val("");
                                       $('#InputAddress').val("");
                                       $('#InputAddress2').val("");
                                       $('#InputCity').val("");
                                       $('#InputState').val("");
                                       $('#InputState').change();
                                       $('#InputCountry').val("");
                                       $('#InputCountry').change();
                                       $('#InputZip').val("");
                                       $('#InputMobile').val("");
                                       $('#InputAdditionalInformation').val("");
                                       $('#addressAlias').val("");
                                    }
                                });                       
                            </script>
<!-- /main-container-->
<div class="gap"></div>
@stop()