@extends('layouts.package.master')

@section('program-latest')
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-md-12">
        <div class="row">

    @if($Agency->count()>0)
        @foreach($Agency as $rows)

            <?php
              $timeline=\App\Timeline::where('id',$rows->timeline_id)->first();
              $media=\App\Media::where('id',$timeline->avatar_id)->first();
              $timeline_id=$rows->timeline_id;
              $countPackage=DB::table('package_tour')
                  ->where('timeline_id',$rows->timeline_id)
                  ->where('packageStatus','CP')
                  ->where('package_close','N')
                  ->count();

              $countPeriod=DB::table('package_details')
                        ->where('status','Y')
                        ->whereIn('packageID',function($query)use($timeline_id){
                            $query->select('packageID')->from('package_tour')
                                ->where('packageStatus','CP')
                                ->where('package_close','N')
                                ->where('timeline_id',$timeline_id);
                        })
                        ->count();

            ?>
            <div class="col-md-3 text-center">
                <h5>{{$timeline->name}}</h5>
                <figure class="card card-product">
                   <div class="img-wrap">
                       @if($media!=null)
                           <img class="logo" src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @else
                           <img class="logo" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                       @endif
                    </div>
               </figure>
                <div class="body"><a href="{{url('home/package/agent/'.$rows->timeline_id)}}" >{{$countPackage}} Package tour</a></div>
                <div class="body"><a href="{{url('home/package/agent/'.$rows->timeline_id)}}" >{{$countPeriod}} Period</a></div>
            </div>
        @endforeach

        @else
            <div class="col-md-12 text-center">
                <h5>Have No Agency!!</h5>
            </div>

        @endif
        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

