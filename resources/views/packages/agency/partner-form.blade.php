
    <div class="modal-header modal-info">
        <h3 class="modal-title-site"> Request Partner.</h3>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
    </div>
    <div class="modal-body">
        <div class="col-md-4 text-center">
            <div class="img-wrap">
                <img src=" @if($timeline->avatar_id) {{ url('location/avatar/small/'.$timeline->avatar->source) }} @else {{ url('location/avatar/small/default-avatar-location.png') }} @endif" alt="{{ $timeline->name }}" title="{{ $timeline->name }}">
            </div>
            <div class="col-lg-12 text-center">{{$timeline->name}}</div>
        </div>
        <div class="col-lg-4 text-center">
            <img class="img-circle" src="{{asset('/images/icon-connect.png')}}" style="max-width: 100px">
        </div>
        <div class="col-md-4 text-center">
            <div class="img-wrap">
                <img src=" @if($timeline_me->avatar_id) {{ url('location/avatar/small/'.$timeline_me->avatar->source) }} @else {{ url('location/avatar/small/default-avatar-location.png') }} @endif" alt="{{ $timeline_me->name }}" title="{{ $timeline_me->name }}">
            </div>
            <div class="col-lg-12 text-center">{{$timeline_me->name}}</div>
        </div>
    </div>
    <div class="modal-footer">

        <button type="submit" id="send-request-partner" data-id="{{$timeline->id}}" class="btn btn-info btn-lg btn-block">
            <i class="fa fa-send-o"></i> {{trans('common.send_request_partner')}}
        </button>
    </div>


<script language="JavaScript">
    $('#send-request-partner').on('click',function () {
       $.ajax({
            type:'get',
            url:SP_source() + 'package/request/partner',
            data:{'timeline_id':$(this).data('id')},
            success:function (data) {
                if(data=='1'){
                    $('.modal-body').html('<h2 class="text-center">Send already!</h2>');
                    $('#send-request-partner').hide();
                }else{
                    $('.modal-body').html('<h2 class="text-center">Have to send already!</h2>');
                    $('#send-request-partner').hide();
                }
            }
        });
    });
</script>



