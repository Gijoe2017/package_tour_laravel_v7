
<form  method="POST" action="{{ url('/login2') }}">
    {!! csrf_field() !!}
<div class="modal-header bg-warning">
    <h4 class="modal-title">{{trans('common.signin')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

        @if(Session::has('message'))
            <div class="alert alert-danger">{{Session::get('message')}}</div>
        @endif
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label">{{trans('common.email_address')}}</label>

                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

        </div>


    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="control-label">{{trans('common.password')}}</label>

            <input type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

    </div>


    <div class="form-group">
        <div class="col-md-8 col-md-offset-2">
            <a class="btn btn-link" href="{{ url('/password/reset') }}">{{trans('common.forget_password')}}</a>
        </div>
    </div>




</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">{{trans('common.login')}}</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('common.close')}}</button>
</div>
</form>



