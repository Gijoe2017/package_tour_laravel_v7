@extends('layouts.package.master')
@section('program-highlight')
    <section class="section-content bg padding-y-sm">
        <div class="container">
            <div class="row ">
                <div class="col-lg-12">
                    <div class="col-lg-6 box">
                        @if(Session::has('message'))
                            @if(Session::get('status')=='success')
                                <div class="alert alert-success">{{Session::get('message')}}</div>
                            @else
                                <div class="alert alert-danger">{{Session::get('message')}}</div>
                            @endif

                        @endif
                        <h3 class="modal-title">{{trans('auth.member_login')}}</h3><hR>
                        <form  class="form-group" method="POST" action="{{ url('/login2') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="login_type" value="order">

                            <div class="input-group w-100 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{trans('auth.email_address')}}<span class='text-danger'>*</span></label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="input-group w-100 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{trans('auth.password')}}<span class='text-danger'>*</span></label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control login" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a class="btn btn-link" href="{{ url('/password/reset') }}"><i class="fa fa-question"></i> {{trans('auth.forgot_password')}}</a>
                                </div>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary login_submit" disabled><i class="fa fa-btn fa-user"></i> {{trans('auth.login')}}</button>
                            </div>
                            </form>
                        <br> <br> <br>
                </div>
                <div class="col-lg-6">
                        <div class="col-md-12" style="padding-top: 15px">
                            <div class="panel panel-info">
                                <h3 class="modal-title">{!! trans('auth.register') !!} </h3><hR>

                                <div class="panel-body">

                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register2') }}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="login_type" value="order">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{trans('auth.name')}}<span class='text-danger'>*</span></label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        {{ $errors->first('name') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{trans('auth.user_name')}}<span class='text-danger'>*</span></label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="username" value="{{ old('username') }}" required>
                                                @if ($errors->has('username'))
                                                    <span class="help-block">
                                                        {{ $errors->first('username') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{trans('auth.email_address')}}<span class='text-danger'>*</span></label>

                                            <div class="col-md-6">
                                                <input type="email" class="form-control" id="email-regis" name="email" value="{{ old('email') }}" required>
                                                <span id="err_email" class="text-danger"></span>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        {{ $errors->first('email') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{trans('auth.password')}}<span class='text-danger'>*</span> </label>
                                            <div class="col-md-6">
                                                <input type="password" class="form-control check-pwd" id="password_regis" name="password" required>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        {{ $errors->first('password') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{trans('auth.confirm_password')}}<span class='text-danger'>*</span></label>
                                            <div class="col-md-6">
                                                <span id="pwd-error" class="text-danger"></span>
                                                <input type="password" class="form-control check-pwd" id="password_confirm_regis" name="password_confirmation" required>

                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                    {{ $errors->first('password_confirmation') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-btn fa-user"></i> {{trans('auth.register')}}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <script language="JavaScript">

        $('body').on('keyup','.login',function () {
           if($(this).val()!=''){
                $('.login_submit').prop( "disabled", false );
           }else{
               $('.login_submit').prop( "disabled", true );
           }
        });

        $('body').on('keyup','#email-regis',function (e) {

            $.ajax({
                type:'get',
                url:SP_source() +'ajax/check/email',
                data:{'email':e.target.value},
                success:function (data) {
                    if(data=='Yes'){
                        $("#err_email").html('{{trans('auth.already_have_an_account')}}');
                        $('#email-regis').select();
                        $('#email-regis').focus();
                    }else{
                        $("#err_email").html('');
                    }
                }
            });
        });
        $('body').on('keypress','#password_regis',function (e) {

            $.ajax({
                type:'get',
                url:SP_source() +'ajax/check/email',
                data:{'email':$('#email-regis').val()},
                success:function (data) {
                    if(data=='Yes'){
                        $("#err_email").html('{{trans('auth.already_have_an_account')}}');
                        $('#password_regis').val('');
                        $('#email-regis').select();
                        $('#email-regis').focus();
                    }else{
                        $("#err_email").html('');
                    }
                }
            });
        });

        $('body').on('blur','.check-pwd',function () {

            var pwd1=$('#password_regis').val();
            var pwd2=$('#password_confirm_regis').val();
            if(pwd2!=''){
                if(pwd1!=pwd2){
                    $('#pwd-error').html('{{trans('auth.password_not_match')}}');
                    $('#password_confirm_regis').val('');
                    $('#password_confirm_regis').focus();
                }else{
                    $('#pwd-error').html('');
                }
            }else{

            }

        });
    </script>

@endsection

