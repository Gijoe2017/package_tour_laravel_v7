<style type="text/css">
    .form-horizontal{
         padding-top: 0px;
    }
</style>
<form method="POST" class="signup-form" action="{{ url('/register') }}">
    {{ csrf_field() }}
    <div class="modal-header">
        <h3>{!! trans('auth.register') !!}</h3>
    </div>
    <div class="modal-body">
        <ul class="signup-errors text-danger list-unstyled"></ul>

            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group{{ $errors->has('affiliate') ? ' has-error' : '' }}">
                        {{ Form::label('affiliate', trans('auth.affiliate_code')) }}<i class="optional">(optional)</i>
                        @if(isset($_GET['affiliate']))
                            {{ Form::text('affiliate', $_GET['affiliate'], ['class' => 'form-control', 'id' => 'affiliate', 'disabled' =>'disabled']) }}
                            {{ Form::hidden('affiliate', $_GET['affiliate']) }}
                        @else
                            {{ Form::text('affiliate', NULL, ['class' => 'form-control', 'id' => 'affiliate', 'placeholder'=> trans('auth.affiliate_code')]) }}
                        @endif

                        @if ($errors->has('affiliate'))
                            <span class="help-block">
                    {{ $errors->first('affiliate') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                        {{ Form::label('email', trans('auth.email_address')) }}
                        {{ Form::text('email', NULL, ['class' => 'form-control', 'id' => 'email', 'placeholder'=> trans('auth.welcome_to')]) }}
                        @if ($errors->has('email'))
                            <span class="help-block">
                    {{ $errors->first('email') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', trans('auth.name')) }}
                        {{ Form::text('name', NULL, ['class' => 'form-control', 'id' => 'name', 'placeholder'=> trans('auth.name')]) }}
                        @if ($errors->has('name'))
                            <span class="help-block">
                    {{ $errors->first('name') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group required {{ $errors->has('gender') ? ' has-error' : '' }}">
                        {{ Form::label('gender', trans('common.gender')) }}
                        {{ Form::select('gender', array('female' => 'Female', 'male' => 'Male', 'other' => 'None'), null, ['placeholder' => trans('auth.select_gender'), 'class' => 'form-control']) }}
                        @if ($errors->has('gender'))
                            <span class="help-block">
                    {{ $errors->first('gender') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
                        {{ Form::label('username', trans('common.username')) }}
                        {{ Form::text('username', NULL, ['class' => 'form-control', 'id' => 'username', 'placeholder'=> trans('common.username')]) }}
                        @if ($errors->has('username'))
                            <span class="help-block">
                    {{ $errors->first('username') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group required {{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('password', trans('auth.password')) }}
                        {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder'=> trans('auth.password')]) }}
                        @if ($errors->has('password'))
                            <span class="help-block">
                    {{ $errors->first('password') }}
                  </span>
                        @endif
                    </fieldset>
                </div>
            </div>

            <div class="row">
                @if(Setting::get('birthday') == "on")
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            {{ Form::label('birthday', trans('common.birthday')) }}<i class="optional">(optional)</i>
                            <div class="input-group date datepicker">
                    <span class="input-group-addon addon-left calendar-addon">
                      <span class="fa fa-calendar"></span>
                    </span>
                                {{ Form::text('birthday', NULL, ['class' => 'form-control', 'id' => 'datepicker1']) }}
                                <span class="input-group-addon addon-right angle-addon">
                      <span class="fa fa-angle-down"></span>
                    </span>
                            </div>
                        </fieldset>
                    </div>
                @endif

                @if(Setting::get('city') == "on")
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            {{ Form::label('city', trans('common.current_city')) }}<i class="optional">(optional)</i>
                            {{ Form::text('city', NULL, ['class' => 'form-control', 'placeholder' => trans('common.current_city')]) }}
                        </fieldset>
                    </div>
                @endif
            </div>

            <div class="row">
                @if(Setting::get('captcha') == "on")
                    <div class="col-md-12">
                        <fieldset class="form-group{{ $errors->has('captcha_error') ? ' has-error' : '' }}">
                            {!! app('captcha')->display() !!}
                            @if ($errors->has('captcha_error'))
                                <span class="help-block">
                    {{ $errors->first('captcha_error') }}
                  </span>
                            @endif
                        </fieldset>
                    </div>
                @endif
            </div>



    </div>
    <div class="modal-footer">
        {{ Form::button(trans('auth.signup_to_dashboard'), ['type' => 'submit','class' => 'btn btn-success btn-submit']) }}

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>
