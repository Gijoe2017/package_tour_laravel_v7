

    <div class="modal-header">
        <h3>{!! trans('auth.register') !!}</h3>
    </div>
    <div class="modal-body">
          <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6" style="margin-bottom: 15px">
                        <a href="{{url('/register')}}" class="btn btn-info btn-block">{{trans('common.signin')}}</a>
                    </div>
                    <div class="col-md-6" style="margin-bottom: 15px">
                        <a href="{{url('/register')}}" class="btn btn-success btn-block">{{trans('common.register')}}</a>
                    </div>
                </div>
          </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

