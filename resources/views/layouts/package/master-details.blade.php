<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Toechok Co.,Ltd">
    <meta name="csrf_token" content="{!! csrf_token() !!}"/>
    <?php
//    App::setLocale(Session::get('language2'));
    Date::setLocale(Session::get('language2'));
    $timeline="";
    if(Session::get('timeline_id')){
         $timeline=\App\Timeline::where('id','=',Session::get('timeline_id'))->first();
         $location = \App\Location::where('timeline_id', '=', Session::get('timeline_id'))->first();
         $media=\App\Media::where('id',$timeline->avatar_id)->first();
    }

    ?>
    @if(Session::has('pname'))
        <title>{{Session::get('pname')}}</title>
    @else

        @if($timeline)
        <title>{{$timeline->name}}</title>
        @else

        <title>Toechok Co.,Ltd</title>
        @endif
    @endif

    @if(isset($media))
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('location/avatar/small/'.$media->source) }}">
    @else
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo-toechok.png')}}">
    @endif

    <link href="https://fonts.googleapis.com/css?family=Sarabun|Trirong&display=swap" rel="stylesheet">
    <link href="{{asset('package/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Font awesome 5 -->

    <link href="{{asset('package/fonts/fontawesome/css/fontawesome-all.min.css')}}" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('member/css/flag-icon.min.css')}}">
    <!-- plugin: owl carousel  -->
    <link href="{{asset('package/plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('package/plugins/owlcarousel/assets/owl.theme.default.css')}}" rel="stylesheet">

    <!-- custom style -->
    <link href="{{asset('package/css/ui.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('package/css/responsive.css')}}" rel="stylesheet" media="only screen and (max-width: 1200px)" />

    <!-- custom javascript -->

    <style>


        .dtHorizontalVerticalExampleWrapper {
            max-width: 600px;
            margin: 0 auto;
        }
        #travel-details th, td {
            white-space: nowrap;
        }


        #divLoading {
            margin: 0px;
            display: none;
            padding: 0px;
            position: fixed;
            right: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            background-color: rgb(255, 255, 255);
            z-index: 30001;
            opacity: 0.5;}

        #loading {
            position: absolute;
            color: White;
            top: 50%;
            left: 45%;}
        .withlist{
            position: absolute;
            font-size: 8px;
            margin-left: -20px;
            margin-top: -5px;
        }

        .omise-checkout-button{
            display: block;
            width: 100%;
            padding: 8px 12px;
            margin-bottom: 0;
            font-size: 16px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #00a65a;
            border-color: #008d4c;
            color: white;
        }

    </style>

    <!-- jQuery -->
    {{--<script src="{{asset('package/js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>--}}
    <script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('package/js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>
    <script language="JavaScript">
        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                var t = getTimeRemaining(endtime);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }
            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
        }
    </script>


    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5efc394d223eac00125582e8&product=inline-share-buttons&cms=sop' async='async'></script>

</head>
<body>
<header class="section-header">
    <nav class="navbar navbar-top navbar-expand-lg navbar-dark bg-secondary">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTop">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('/')}}"><i class="fa fa-home"></i> </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">

                            @if(Auth::check())
                                @if(Session::get('language') != null)
                                    <?php $key = Session::get('language') ?>
                                @else
                                    <?php $key = App\Setting::get('language'); ?>
                                @endif
                            @else
                                <?php $key = Session::get('language'); ?>
                            @endif

                            @if($key == 'en')
                                <span class="flag-icon flag-icon-us"></span>
                            @elseif($key == 'iw')
                                <span class="flag-icon flag-icon-il"></span>
                            @elseif($key == 'ja')
                                <span class="flag-icon flag-icon-jp"></span>
                            @elseif($key == 'zh')
                                <span class="flag-icon flag-icon-cn"></span>
                            @elseif($key == 'hi')
                                <span class="flag-icon flag-icon-in"></span>
                            @elseif($key == 'fa')
                                <span class="flag-icon flag-icon-ir"></span>
                            @else
                                <span class="flag-icon flag-icon-{{ $key }}"></span>
                            @endif

                            {{trans('common.Language')}} </a>

                        <ul class="dropdown-menu small">
                            @foreach( Config::get('app.locales') as $key => $value)
                                <li>
                                    <a href="#" class="switch-language dropdown-item" data-language="{{ $key }}">
                                        @if($key == 'en')
                                            <span class="flag-icon flag-icon-us"></span>
                                        @elseif($key == 'iw')
                                            <span class="flag-icon flag-icon-il"></span>
                                        @elseif($key == 'ja')
                                            <span class="flag-icon flag-icon-jp"></span>
                                        @elseif($key == 'zh')
                                            <span class="flag-icon flag-icon-cn"></span>
                                        @elseif($key == 'hi')
                                            <span class="flag-icon flag-icon-in"></span>
                                        @elseif($key == 'fa')
                                            <span class="flag-icon flag-icon-ir"></span>
                                        @else
                                            <span class="flag-icon flag-icon-{{ $key }}"></span>
                                        @endif
                                        {{ $value }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    {{--<li><a href="" class="nav-link"> {{trans('common.help')}} </a></li>--}}
                    {{--<li><a href="" class="nav-link"> {{trans('common.wishlist')}} </a></li>--}}
                    {{--<li><a href="" class="nav-link"> {{trans('common.checkout')}} </a></li>--}}
                </ul>
                {{--<ul class="navbar-nav">--}}
                    {{--<li><a href="" class="nav-link"> My Account </a></li>--}}
                    {{--<li><a href="" class="nav-link"> Wishlist </a></li>--}}
                    {{--<li><a href="" class="nav-link"> Checkout </a></li>--}}
                {{--</ul>--}}
                    <!-- list-inline //  -->
            </div> <!-- navbar-collapse .// -->
        </div>
        <!-- container //  -->
    </nav>
    <section class="header-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6-24 col-sm-5 col-4">
                        <div class="brand-wrap"><a href="{{url('/')}}">
                            <img class="logo" src="{{asset('images/logo-toechok.png')}}"></a>
                            <a href="{{url('/home/package/all')}}"><h2 class="logo-text">{{trans('common.package_tour')}}</h2> </a>
                        </div> <!-- brand-wrap.// -->
                </div>
                <div class="col-lg-10-24 col-sm-12 order-3 order-lg-2">
                    <form method="post" action="{{action('Package\PackageTourController@search_tour')}}">
                        {!! csrf_field() !!}
                        <div class="input-group w-100">
                            {{--<select class="custom-select"  name="category_name">--}}
                                {{--<option value="">All type</option>--}}
                                {{--@foreach(\App\Tourtype::where('LanguageCode',Session::get('language'))->get() as $rows)--}}
                                    {{--<option value="{{$rows->TourCategoryID}}">{{$rows->TourCategoryName}}</option>--}}
                                {{--@endforeach--}}
                                {{--<option value="comments">Only best</option>--}}
                                {{--<option value="content">Latest</option>--}}
                            {{--</select>--}}
                            <input type="text" class="form-control" name="txtsearch"  placeholder="{{trans('common.search_in')}} Toechok">

                            <div class="input-group-append">
                                <button class="btn btn-danger" name="submit" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form> <!-- search-wrap .end// -->
                </div> <!-- col.// -->
                <?php
                $countCart=0;
                //dd(Session::get('cart_session_id'));
                if(Auth::check()){
                    $Mycart=\App\Mycart::where('auth_id',Auth::user()->id)->get();
//                    dd($Mycart);
                    if($Mycart){
                        foreach ($Mycart as $cart){
                            $countCart+=$cart->count_cart()->get()->sum('number_of_person');
                        }
                    }

                    $countWithlist=DB::table('package_wishlist')
                        ->whereIn('packageDescID',function ($query) {
                            $query->select('packageDescID')->from('package_details')
                                ->where('status','Y');
                        })
                        ->where('user_id',Auth::user()->id)
                        ->count();


                }else{
                   // dd(Session::get('sessionID'));
                    $Mycart=\App\Mycart::where('session_id',Session::getId())->first();

                    if($Mycart){
                        $countCart=$Mycart->count_cart()->get()->sum('number_of_person');
                    }

                }
                //dd(Session::getId());

                ?>
                <div class="col-lg-8-24 col-sm-7 col-8  order-2  order-lg-3">
                    <div class="widgets-wrap float-right">
                        @if($countCart>0)
                            @if(Auth::check())
                                <a href="{{url('booking/u/'.Auth::user()->id)}}" class="widget-header mr-3">
                            @else
                                <a href="{{url('booking/u/none')}}" class="widget-header mr-3">
                            @endif
                                <div class="icontext">
                                    <div class="icon-wrap"><i class="icon-sm round border fa fa-shopping-cart"></i></div>
                                    <div class="text-wrap">
                                        <span class="small badge badge-danger count-cart">{{$countCart}}</span>
                                        <small>{{trans('common.cart')}}</small>
                                    </div>
                                </div>
                                </a>
                                @else
                                    <div class="icontext">
                                        <div class="icon-wrap"><i class="icon-sm round border fa fa-shopping-cart"></i></div>
                                        <div class="text-wrap">
                                            <span class="small badge badge-danger">{{$countCart}}</span>
                                            <small>{{trans('common.cart')}}</small>
                                        </div>
                                    </div>
                                @endif

                                @if(Auth::check())
                                        <a href="{{url('home/package/wish/list')}}" class="widget-header mr-3">
                                            <div class="icontext">
                                                <div class="icon-wrap"><i class="icon-sm round border fa fa-heart"></i>
                                                    @if($countWithlist>0)
                                                    <span class="small withlist badge badge-danger">{{$countWithlist}}</span>
                                                        @endif
                                                </div>
                                                <div class="text-wrap">
                                                    <small>{{trans('common.wish')}}</small>
                                                    <small>{{trans('common.list')}}</small>
                                                </div>
                                            </div>
                                        </a>
                                @else
                                        <a class="widget-header mr-3"   href="{{url('/register/2')}}">
                                            <div class="icontext">
                                                <div class="icon-wrap"><i class="icon-sm round border fa fa-heart"></i></div>
                                                <div class="text-wrap">
                                                    <small>{{trans('common.wish')}}</small>
                                                    <small>{{trans('common.list')}} </small>
                                                </div>
                                            </div>
                                        </a>
                                        {{--<div class="dropdown-menu dropdown-menu-right">--}}
                                            {{--<form action="{{url('/login2')}}" method="post" class="px-4 py-3">--}}
                                                {{--{{csrf_field()}}--}}
                                                {{--<input type="hidden" name="package" value="Y">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label>Email address</label>--}}
                                                    {{--<input type="emails" name="emails" class="form-control" placeholder="emails@example.com">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label>Password</label>--}}
                                                    {{--<input type="password" name="password" class="form-control" placeholder="Password">--}}
                                                {{--</div>--}}
                                                {{--<button type="submit" class="btn btn-primary">Sign in</button>--}}
                                            {{--</form>--}}
                                            {{--<hr class="dropdown-divider">--}}
                                            {{--<a class="dropdown-item" href="#">Have account? Sign up</a>--}}
                                            {{--<a class="dropdown-item" href="#">Forgot password?</a>--}}
                                        {{--</div>--}}
                                @endif
                        <div class="widget-header dropdown">

                            @if(Auth::check())
                                <a href="#" data-toggle="dropdown" data-offset="20,10">
                                    <div class="brand-wrap icontext">
                                        <div class="icon-wrap ">
                                            <img class="logo round"  src="{{ Auth::user()->avatar }}"  >
                                        </div>
                                        <div class="text-wrap">
                                            <small>{{str_limit(Auth::user()->name,10)}}</small>
                                            <small>{{trans('common.my_account')}} <i class="fa fa-caret-down"></i> </small>
                                        </div>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{url('my/account')}}"><i class="fa fa-user"></i> {{trans('common.my_account')}}</a>
                                    <a class="dropdown-item" href="{{url('home/request/partner')}}"><i class="fa fa-user"></i> {{trans('common.request_partner')}}</a>
                                    @if(\App\Booking::count())
                                    <a class="dropdown-item" href="{{url('my/bookings')}}"><i class="fa fa-shopping-cart"></i> {{trans('common.my_bookings')}}</a>
                                    @endif
                                    <?php
                                        $check=DB::table('locations as a')
                                            ->join('business_verified_modules as b','b.timeline_id','=','a.timeline_id')
                                            ->join('location_user as c','c.location_id','=','a.id')
                                            ->where('b.business_verified_status','verified')
                                            ->where('c.user_id',Auth::user()->id)
                                            ->get();
                                    ?>
                                    @if($check->count())
                                    <a class="dropdown-item" href="{{url('member/manage/all')}}">
                                        <i class="fa fa-file"></i> {{trans('common.manage_module')}}
                                    </a>
                                    @endif
                                    <form action="{{url('/logout')}}" method="post">
                                        {{csrf_field()}}
                                        <button type="submit" class="dropdown-item" href="{{url('logout')}}"><i class="fa fa-key"></i> {{trans('auth.sign_out')}}</button>
                                    </form>
                                    <hr class="dropdown-divider">
                                </div>
                            @else
                                <a href="#" data-toggle="dropdown" data-offset="20,10">
                                    <div class="icontext">
                                        <div class="icon-wrap"><i class="icon-sm round border fa fa-user"></i></div>
                                        <div class="text-wrap">
                                            <small>{{trans('auth.sign_in_join')}}</small>
                                            <div>{{trans('common.my_account')}} <i class="fa fa-caret-down"></i> </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">

                                    <form action="{{url('/login2')}}" method="post" class="px-4 py-3">
                                        {{csrf_field()}}
                                        <input type="hidden" name="package" value="Y">
                                        @if(Session::has('message'))
                                            <div class="form-group">
                                                <span class="text-danger">{{Session::get('message')}}</span>
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <span class="text-danger" id="email-error"></span><BR>
                                            <label>{{trans('auth.email_address')}}</label>
                                            <input type="email" name="email" id="email" class="form-control check-email" placeholder="email@example.com">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('auth.password')}}</label>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary">{{trans('auth.sign_in')}}</button>
                                    </form>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="{{url('/register/2')}}">{{trans('auth.have_account_sign_up')}}</a>
                                    <a class="dropdown-item" href="{{url('/password/reset')}}">{{trans('auth.forgot_password')}}</a>
                                </div>
                            @endif
                        <!--  dropdown-menu .// -->
                        </div>  <!-- widget-header .// -->

                    </div> <!-- widgets-wrap.// -->
                </div> <!-- col.// -->



            </div> <!-- row.// -->
        </div> <!-- container.// -->
    </section>

    @if(Session::get('whoner'))
    <section class="header-main-2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6-24 col-sm-5 col-4">

                </div>
                <div class="col-lg-8-24 col-sm-12 order-3 order-lg-2">
                    <a href="{{url('/home/package/agent/'.$timeline->id)}}" target="_blank">
                        <div class="brand-wrap">
                            @if($media!=null)
                                <img class="logo" src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                            @else
                                <img class="logo" src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
                            @endif
                            {{--<img class="logo" src="{{asset('/user/avatar/small/'.$timeline->avatar()->first()->source)}}">--}}
                            <h2 class="logo-text">{{$timeline->name}}</h2>
                        </div> <!-- brand-wrap.// -->
                    </a>
                </div> <!-- col.// -->
                <div class="col-lg-10-24 col-sm-7 col-8  order-2  order-lg-3">
                    <form action="#">
                        <div class="input-group w-100">
                            <select class="custom-select"  name="category_name">
                                <option value="">All type</option>
                                @foreach(\App\Tourtype::where('LanguageCode',Session::get('language'))->get() as $rows)
                                    <option value="{{$rows->TourCategoryID}}">{{$rows->TourCategoryName}}</option>
                                @endforeach
                                {{--<option value="comments">Only best</option>--}}
                                {{--<option value="content">Latest</option>--}}
                            </select>
                            <input type="text" class="form-control" style="width:50%;" placeholder="{{trans('common.search_in')}} {{$timeline->name}}">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form> <!-- search-wrap .end// -->
                </div> <!-- col.// -->
            </div> <!-- row.// -->
        </div> <!-- container.// -->
    </section>
    @endif


    <!-- header-main .// -->
</header>
<div class="modal modal-primary fade" id="popupForm" tabindex="-1" role="dialog" aria-labelledby="popupForm" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="modalDetails" class="modal-content">

        </div>
    </div>
</div>




<div class="modal modal-primary fade" id="Policy" tabindex="-1" role="dialog" aria-labelledby="Policy" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="modalDetails" class="modal-content">
            <div id="modalDetails" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    Modal body..
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal modal-primary fade" id="popupLogin" tabindex="-1" role="dialog" aria-labelledby="popupLogin" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div id="modalDetails" class="modal-content">
            <div id="modalDetails" class="modal-content">

            </div>
        </div>
    </div>
</div>
<!-- section-header.// -->
<div id="preload" style="display: none; position: absolute; z-index: 100; top:10%; margin-left: 40%" class="preload" align="center">
    <div id="loader-wrapper" class="loader"></div>
    <p style="color:#7B8BB3"><img src="{{asset('images/preload.gif')}}"></p>
</div>


<div id="divLoading" class="loader" style="z-index: 150">
    <p id="loading"><img src="{{asset('images/preload.gif')}}"></p>
</div>



@include('layouts.package.menu')

<!-- ========================= SECTION Highlight ========================= -->
@yield('program-highlight')
<!-- ========================= SECTION Highlight END// ========================= -->

<!-- ========================= SECTION Suggest ========================= -->
@yield('program-suggest')
<!-- ========================= SECTION Suggest .END// ========================= -->

<!-- ========================= SECTION Latest ========================= -->
@yield('program-latest')
<!-- ========================= SECTION Latest END// ========================= -->



<!-- ========================= SECTION Country ========================= -->
@yield('program-country')
<!-- ========================= SECTION Country .END// ========================= -->

<!-- ========================= SECTION LINKS ========================= -->
@yield('program-list-time')
<!-- ========================= SECTION LINKS END.// ========================= -->



<div class="modal modal-primary fade" id="loginForm" >
    <div class="modal-dialog">
        <div id="modalDetails" class="modal-content">
            <div class="modal-header">
                <h2><i class="fa fa-user"></i> {{trans('common.signin')}}</h2>
            </div>
            <div class="modal-body">
                <form action="{{url('/login2')}}" method="post" class="px-4 py-3">
                    {{csrf_field()}}
                    <input type="hidden" name="package" value="Y">
                    <div class="form-group">
                        <label>{{trans('common.emails')}}</label>
                        <input type="email" name="email" class="form-control" placeholder="email@example.com">
                    </div>
                    <div class="form-group">
                        <label>{{trans('common.password')}}</label>
                        <input type="password" name="password" class="form-control" placeholder="******">
                    </div>
                    <button type="submit" class="btn btn-primary">{{trans('common.signin')}}</button>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-6">
                <a class="dropdown-item" href="{{url('/register/2')}}">{{trans('auth.have_account_sign_up')}}</a>
                </div>    <div class="col-md-6">
                <a class="dropdown-item" href="{{url('password/reset')}}">{{trans('auth.forgot_password')}}?</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
    <div class="container">
        <section class="footer-top padding-top">
            <div class="row">
                <aside class="col-sm-4 col-md-4 white">
                    <h5 class="title">{{trans('common.customer_services')}}</h5>

                    <ul class="list-unstyled">
                        @foreach(App\Policy::orderby('order_by','asc')->active()->get() as $rows)
                            <li> <a href="#" data-id="{{$rows->id}}" data-toggle="modal" data-target="#Policy" class="policy-details">{{$rows->title}}</a></li>
                        @endforeach
                        {{--<li> <a href="#">{{trans('common.money_refund')}}</a></li>--}}
                        {{--<li> <a href="#">{{trans('common.terms_and_policy')}}</a></li>--}}
                        {{--<li> <a href="#">{{trans('common.open_dispute')}}</a></li>--}}
                    </ul>
                </aside>
                <aside class="col-sm-8 col-md-8 white">
                    <p class="pb-2 text-center white">Delivering the latest product trends and industry news straight to your inbox</p>

                    <div class="row justify-content-md-center">
                        <div class="col-lg-8 col-sm-6">
                            <form class="row-sm form-noborder">
                                <div class="col-8">
                                    <input class="form-control" placeholder="Your Email" type="email">
                                </div> <!-- col.// -->
                                <div class="col-4">
                                    <button type="submit" class="btn btn-block btn-warning"> <i class="fa fa-envelope"></i> Subscribe </button>
                                </div> <!-- col.// -->
                            </form>
                            <small class="form-text text-white-50">We’ll never share your email address with a third-party. </small>
                        </div> <!-- col-md-6.// -->
                    </div>
                </aside>
                {{--<aside class="col-sm-6  col-md-6 white">--}}
                {{--<h5 class="title">{{trans('common.my_account')}}</h5>--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li> <a href="{{url('/register/2')}}"> {{trans('common.user_login')}} </a></li>--}}
                {{--<li> <a href="{{url('/register/2')}}"> {{trans('common.user_register')}}</a></li>--}}
                {{--<li> <a href="#"> {{trans('common.account_setting')}} </a></li>--}}
                {{--<li> <a href="{{url('my/bookings')}}"> {{trans('common.my_orders')}} </a></li>--}}
                {{--<li> <a href="{{url('home/package/wish/list')}}"> {{trans('common.my_wishlist')}} </a></li>--}}
                {{--</ul>--}}
                {{--</aside>--}}
                {{--<aside class="col-sm-3  col-md-3 white">--}}
                {{--<h5 class="title">{{trans('common.about')}}</h5>--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li> <a href="#"> {{trans('common.our_history')}}</a></li>--}}
                {{--<li> <a href="#"> {{trans('common.how_to_buy')}}</a></li>--}}
                {{--<li> <a href="#"> {{trans('common.delivery_and_payment')}} </a></li>--}}
                {{--<li> <a href="#"> {{trans('common.advertice')}} </a></li>--}}
                {{--<li> <a href="#"> {{trans('common.partnership')}} </a></li>--}}
                {{--</ul>--}}
                {{--</aside>--}}
                {{--<aside class="col-sm-3">--}}
                {{--<article class="white">--}}
                {{--<h5 class="title">{{trans('common.contact')}}</h5>--}}
                {{--<p>--}}
                {{--<strong>{{trans('common.phone')}}: </strong> +66 02 870 6465 <br>--}}
                {{--<strong>{{trans('common.fax')}}:</strong> +66 02 870 6465--}}
                {{--</p>--}}

                {{--<div class="btn-group white">--}}
                {{--<a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>--}}
                {{--<a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>--}}
                {{--<a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>--}}
                {{--<a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>--}}
                {{--</div>--}}
                {{--</article>--}}
                {{--</aside>--}}
            </div> <!-- row.// -->
            <br>
        </section>
        <section class="footer-bottom row border-top-white">
            <div class="col-sm-6">
                {{--<p class="text-white-50"> Made with <3 <br>  by Vosidiy M.</p>--}}
            </div>
            <div class="col-sm-6">
                <p class="text-md-right text-white-50">
                    Copyright &copy
                    <a href="http://bootstrap-ecommerce.com" class="text-white-50">Toechok Co.,Ltd</a>
                </p>
            </div>
        </section> <!-- //footer-top -->
    </div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->

<!-- ========================= FOOTER ========================= -->
{{--<footer class="section-footer bg-secondary">--}}
    {{--<div class="container">--}}
        {{--<section class="footer-top padding-top">--}}
            {{--<div class="row">--}}
                {{--<aside class="col-sm-3 col-md-3 white">--}}
                    {{--<h5 class="title">{{trans('common.customer_services')}}</h5>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li> <a href="#">{{trans('common.help_center')}}</a></li>--}}
                        {{--<li> <a href="#">{{trans('common.money_refund')}}</a></li>--}}
                        {{--<li> <a href="#">{{trans('common.terms_and_policy')}}</a></li>--}}
                        {{--<li> <a href="#">{{trans('common.open_dispute')}}</a></li>--}}
                    {{--</ul>--}}
                {{--</aside>--}}
                {{--<aside class="col-sm-3  col-md-3 white">--}}
                    {{--<h5 class="title">{{trans('common.my_account')}}</h5>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li> <a href="{{url('/register/2')}}"> {{trans('common.user_login')}} </a></li>--}}
                        {{--<li> <a href="{{url('/register/2')}}"> {{trans('common.user_register')}}</a></li>--}}
                        {{--<li> <a href="#"> {{trans('common.account_setting')}} </a></li>--}}
                        {{--<li> <a href="{{url('my/bookings')}}"> {{trans('common.my_orders')}} </a></li>--}}
                        {{--<li> <a href="{{url('home/package/wish/list')}}"> {{trans('common.my_wishlist')}} </a></li>--}}
                    {{--</ul>--}}
                {{--</aside>--}}
                {{--<aside class="col-sm-3  col-md-3 white">--}}
                    {{--<h5 class="title">{{trans('common.about')}}</h5>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li> <a href="#"> {{trans('common.our_history')}}</a></li>--}}
                        {{--<li> <a href="#"> {{trans('common.how_to_buy')}}</a></li>--}}
                        {{--<li> <a href="#"> {{trans('common.delivery_and_payment')}} </a></li>--}}
                        {{--<li> <a href="#"> {{trans('common.advertice')}} </a></li>--}}
                        {{--<li> <a href="#"> {{trans('common.partnership')}} </a></li>--}}
                    {{--</ul>--}}
                {{--</aside>--}}
                {{--<aside class="col-sm-3">--}}
                    {{--<article class="white">--}}
                        {{--<h5 class="title">{{trans('common.contact')}}</h5>--}}
                        {{--<p>--}}
                            {{--<strong>{{trans('common.phone')}}: </strong> +66 02 870 6465 <br>--}}
                            {{--<strong>{{trans('common.fax')}}:</strong> +66 02 870 6465--}}
                        {{--</p>--}}

                        {{--<div class="btn-group white">--}}
                            {{--<a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>--}}
                            {{--<a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>--}}
                            {{--<a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>--}}
                            {{--<a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>--}}
                        {{--</div>--}}
                    {{--</article>--}}
                {{--</aside>--}}
            {{--</div> <!-- row.// -->--}}
            {{--<br>--}}
        {{--</section>--}}
        {{--<section class="footer-bottom row border-top-white">--}}
            {{--<div class="col-sm-6">--}}
                {{--<p class="text-white-50"> Made with <3 <br>  by Vosidiy M.</p>--}}
            {{--</div>--}}
            {{--<div class="col-sm-6">--}}
                {{--<p class="text-md-right text-white-50">--}}
                    {{--Copyright &copy--}}
                    {{--<a href="http://bootstrap-ecommerce.com" class="text-white-50">Toechok Co.,Ltd</a>--}}
                {{--</p>--}}
            {{--</div>--}}
        {{--</section> <!-- //footer-top -->--}}
    {{--</div><!-- //container -->--}}
{{--</footer>--}}
<!-- ========================= FOOTER END // ========================= -->




<script src="{{asset('member/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
{{--<script src="{{asset('package/js/bootstrap.bundle.min.js')}}"></script>--}}
<script src="{{asset('package/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('package/js/script.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('package/js/modal.js')}}" type="text/javascript"></script>--}}
<!-- DataTables -->


<script language="javascript">
//    document.getElementById('divLoading').style.display = 'block';
    var step=$('#step').val();
    function SP_source() {
        return "{{url('/')}}/";
    }

    $(document).on('submit','.form-booking',function(){
        // code
        $('#divLoading').show()
    });


$('body').on('click','.policy-details',function(){
    var title = "Privacy Policy ";
    var body = "Welcome to Shop system";

    $.ajax({
        type:'get',
        url:SP_source() +'ajax/policy/show/details',
        data:{'id':$(this).data('id')},
        success:function(data){

            $("#Policy .modal-title").html(title);
            $("#Policy .modal-body").html(data);
            $("#Policy").modal("show");
        }
    });

});

    $('body').on('blur','.check-emails',function (e) {
        $.ajax({
            type:'get',
            url:SP_source() +'check/emails/member',
            data:{'email':e.target.value},
            success:function (data) {

                if(data=='1'){
                    $('#emails-error').html('');
                    return true;
                }else{
                    $('#emails-error').html('{{trans('auth.email_not_found')}}');
                    $('#emails').select();
                }
            }
        });
    });

    $('body').on('change','.tour-type',function () {

        element=".show_price-"+$(this).data('id');

        $.ajax({
            type:'get',
            url:SP_source() +'ajax/show/price/selected',
            data:{'packageDescID':$(this).data('id'),'psub_id':$(this).val()},
            success:function (data) {

                $(element).html(data);
            }
        });
    });

    $('body').on('click','.switch-language',function(e){
        e.preventDefault();
//        $.ajax({
//            type:'get',
//            url:SP_source() +'ajax/switch-language_home',
//            data:{language: $(this).data('language')},
//            success:function (data) {
//                // alert('test');
//                window.location =  window.location.href;
//            }
//        });
      //  alert('test');
        $.post(SP_source() + 'ajax/switch-language_home', {language: $(this).data('language')}, function(data) {
            if (data.status == 200) {
                window.location =  SP_source() +'home/package/all';
            }
            else if (data.status == 201) {
                notify(data.message,'warning');
            }
        });
    });

    $('body').on('click','.switch-currency',function () {
        var currency=$(this).data('id');
        $.post(SP_source() + 'ajax/switch-currency', {currency: currency}, function(data) {
            if (data.status == 200) {
//                alert(currency);
                window.location =  window.location.href;
            }else if (data.status == 201) {
                notify(data.message,'warning');
            }
        });
    });

    $('body').on('click','.add-cart',function () {
        $('.form-booking').submit();
    });

    $('body').on('click','.delete-wishlist',function () {
        if(confirm('{{trans('common.confirm_delete')}}')){
            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'/ajax/delete/wishlist',
                data:{'id':$(this).data('id')},
                success:function (data) {
                    $('#divLoading').hide();
//                    $("#cart-detail").html(data);
                     window.location =  window.location.href;
                }
            });
        }

    });

    $('body').on('click','.add-wishlist',function () {
        $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/add/wishlist',
                data:{'id':$(this).data('id'),'step':step},
                success:function (data) {
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
//                     window.location =  window.location.href;
                }
            });
    });

    $('body').on('click','.remove-cart',function () {
        if(confirm('Confirm delete this package?')){
            $('#divLoading').show();

            $.ajax({
                type:'get',
                url:SP_source() +'ajax/remove/cart',
                data:{'cart_id':$(this).data('id'),'package_id':$(this).data('name'),'step':step},
                success:function (data) {
                    //alert(data);
                    count_item_cart();
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
//                    window.location =  window.location.href;
                }
            });
        }
    });


    function count_item_cart(){
        $.ajax({
            type:'get',
            url:SP_source() +'ajax/count/item/cart',
            data:{'cart_id':'check'},
            success:function (data) {
                $('.count-cart').html(data);
            }
        });

    }

    $('body').on('click','.addition-delete',function () {
        if(confirm('Confirm delete this additional?')){
            $('#divLoading').show();
            $.ajax({
                type:'get',
                url:SP_source() +'ajax/delete/addition_cart',
                data:{'id':$(this).data('id'),'step':step},
                success:function (data) {
                   // alert('test');
                    $('#divLoading').hide();
                    $("#cart-detail").html(data);
//                    window.location =  window.location.href;
                }
            });
        }
    });

    $('body').on('change','.cart-type-update',function () {

    $('#divLoading').show();
       // alert('test');
        $.ajax({
            type:'get',
            url:SP_source() +'ajax/update/cart',
            data:{'cart_id':$(this).data('id'),'tour_type':$(this).val(),'colum':'type','step':step},
            success:function (data) {
                $('#divLoading').hide();
                $("#cart-detail").html(data);
            }
        });
    });


//    $('#update_number_of_person').on('change',function () {
    $('body').on('change','.cart-amount-update',function () {
      //  alert($(this).val());
        $('#divLoading').show();
        $.ajax({
            type:'get',
            url:SP_source() +'ajax/update/cart',
            data:{'cart_id':$(this).data('id'),'number_of_person':$(this).val(),'colum':'amount','step':step},
            success:function (data) {
                count_item_cart();
                $('#divLoading').hide();
                $("#cart-detail").html(data);
            }
        });
    });

    $('body').on('change','.cart-visa-update',function () {
      //  alert($(this).val());
        $('#divLoading').show();
        $.ajax({
            type:'get',
            url:SP_source() +'ajax/update/cart',
            data:{'cart_id':$(this).data('id'),'number_of_need_visa':$(this).val(),'colum':'visa','step':step},
            success:function (data) {
                count_item_cart();
                $('#divLoading').hide();
                $("#cart-detail").html(data);
            }
        });
    });


     $('body').on('change','.addition_cart',function(){
        $('#divLoading').show();
        $.ajax({
            type:'get',
            url:SP_source() +'ajax/addition/cart',
            data:{'cart_id':$(this).data('id'),'addition':$(this).val(),'step':step},
            success:function (data) {
                $('#divLoading').hide();
                $("#cart-detail").html(data);
//                window.location =  window.location.href;
            }
        });
    });

    $('body').on('click','.addition_cart_button',function () {
        alert('Choose additional option.');
        $('#addition_cart').focus();
    });

</script>


</body>
</html>