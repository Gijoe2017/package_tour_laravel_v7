
<section class="bg2">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="main_nav">
                        <ul class="navbar-nav">
                            @if(Session::get('whoner'))
                            <li class="nav-item">
                                <a  class="nav-link" href="{{url('/home/package/agent/'.$timeline->id)}}">
                                    {{trans('common.home')}}
                                </a>
                            </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('home/package/'.Session::get('group'))}}"> {{trans('common.home')}} </a>
                                </li>
                                @endif
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{url('home/package/common/about')}}">{{trans('common.about')}}</a>--}}
                            {{--</li>--}}

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('common.package')}} </a>
                                <div class="dropdown-menu" aria-labelledby="dropdown07">

                                    @foreach( \App\PackageCountry::where('packageDateStart','>',date('Y-m-d'))->where('language_code',Session::get('language'))->get() as $rows)
                                        <?php
//                                        $countTime=DB::table('package_details')
//                                            ->where('status','Y')
//                                            ->where('country_id',$rows->country_id)
//                                            ->get();
//
//                                        $countCountry=DB::table('package_details')
//                                            ->where('status','Y')
//                                            ->where('country_id',$rows->country_id)
//                                            ->groupby('packageID')
//                                            ->get();
                                        $countTime=DB::table('package_details as a')
                                            ->join('package_tourin as d','d.packageID','=','a.packageID')
                                            ->where('a.status','Y')
                                            ->where('d.CountryCode',$rows->country_id)
                                            ->get();

                                        $countCountry=DB::table('package_details as a')
                                            ->join('package_tourin as d','d.packageID','=','a.packageID')
                                            ->where('a.closing_date','>',date('Y-m-d'))
                                            ->where('a.status','Y')
                                            ->where('d.CountryCode',$rows->country_id)
                                            ->groupby('a.packageID')
                                            ->get();

                                            $key=strtolower($rows->country_iso_code);
                                        ?>
                                        @if($countCountry->count())
                                            @if($rows->country_id>0)
                                                <a class="dropdown-item list-icon2" href="{{url('home/package/country/'.$rows->country_id)}}">
                                                    @if($key == 'en')
                                                        <span class="flag-icon flag-icon-us"></span>
                                                    @elseif($key == 'iw')
                                                        <span class="flag-icon flag-icon-il"></span>
                                                    @elseif($key == 'ja')
                                                        <span class="flag-icon flag-icon-jp"></span>
                                                    @elseif($key == 'zh')
                                                        <span class="flag-icon flag-icon-cn"></span>
                                                    @elseif($key == 'hi')
                                                        <span class="flag-icon flag-icon-in"></span>
                                                    @elseif($key == 'fa')
                                                        <span class="flag-icon flag-icon-ir"></span>
                                                    @else
                                                        <span class="flag-icon flag-icon-{{ $key }}"></span>
                                                    @endif
                                                     {{$rows->country}} ({{$countCountry->count()}}, {{$countTime->count()}} {{trans('common.time_zone')}})</span>
                                                </a>
                                            @endif
                                        @endif

                                    @endforeach
                                    {{--<a class="dropdown-item list-icon2" href="#"><img src="{{asset('package/images/icons/flag-th.png')}}"> <span>ไทย (5, 10 ช่วงเวลา)</span></a>--}}
                                    {{--<a class="dropdown-item list-icon2" href="#"><img src="{{asset('package/images/icons/flag-usa.png')}}"> <span>พม่า (5, 10 ช่วงเวลา)</span></a>--}}
                                    {{--<div class="dropdown-divider"></div>--}}
                                    {{--<a class="dropdown-item list-icon2" href="#"><img src="{{asset('package/images/icons/flag-usa.png')}}"> <span>สหรัฐอเมริกา (5, 10 ช่วงเวลา)</span></a>--}}
                                    {{--<a class="dropdown-item list-icon2" href="#"><img src="{{asset('package/images/icons/flag-usa.png')}}"> <span>สหรัฐอเมริกา (5, 10 ช่วงเวลา)</span></a>--}}

                                 </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('home/agency/list')}}">{{trans('common.agency')}}</a>
                            </li>

                            <li class="nav-item">
                                @if(Auth::check())
                                    <?php
                                    $timeline=isset($timeline->id)?$timeline->id:Auth::user()->timeline_id;
                                    ?>
                                    <a class="nav-link" data-toggle="modal" data-target="#popupForm"  href="{{url('home/partner/form/'.$timeline)}}">{{trans('common.request_partner')}}</a>
                                @else
                                    <a class="nav-link" data-toggle="modal" data-target="#popupLogin"  href="{{url('/ajax/login_wishlist/')}}">{{trans('common.request_partner')}}</a>
                                @endif
                            </li>
                                {{--<li class="nav-item">--}}
                                    {{--<a class="nav-link" href="{{url('/social')}}">{{trans('common.social_network')}}</a>--}}
                                {{--</li>--}}
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{url('home/package/common/contact')}}">{{trans('common.contact')}}</a>--}}
                            {{--</li>--}}
                        </ul>


                    </div> <!-- collapse .// -->
                </nav>

            </div> <!-- col.// -->
        </div> <!-- row.// -->
    </div> <!-- container .// -->
</section>