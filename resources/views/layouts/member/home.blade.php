@extends('theme.AdminLTE.layout_master')

{{--@section('content')--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">CPU Traffic</span>--}}
                    {{--<span class="info-box-number">90<small>%</small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-red"><i class="fa fa-taxi"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Cars</span>--}}
                    {{--<span class="info-box-number">40<small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}

        {{--<!-- fix for small devices only -->--}}
        {{--<div class="clearfix visible-sm-block"></div>--}}

        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Tour</span>--}}
                    {{--<span class="info-box-number"><small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Customers</span>--}}
                    {{--<span class="info-box-number">90<small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
    {{--</div>--}}
{{--@endsection--}}


@section('content-chart')
    <style type="text/css">
        .btn{
            text-align: left;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h2 class="box-title">Search User.</h2>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <div class="btn-group">
                            <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-9">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="txtSearch" name="txtSearch">
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-info btn-flat">Search Go!</button>
                            </span>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <a href="{{url('member/create')}}" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Create new member.</a>
                        </div>
                    </div>
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">

                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
    <div class="row">
        <?php $i=0;?>
        @foreach($Users as $rows)
            <?php if($i==3){$i=0;}
                $i++;
            ?>
            <!-- /.col -->
                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header {{$bgColor[$i]}} ">
                            <h3 class="widget-user-username">{{$rows->UserTitle.$rows->FirstName.' '.$rows->LastName}}</h3>
                            <h5 class="widget-user-desc">Lead Developer</h5>
                        </div>
                        <div class="widget-user-image">
                            <?php $fileName = public_path('images/user/visa/'.$rows->PhotoForVisa);?>
                            @if (file_exists($fileName))
                                <img class="img-circle" src="{{asset('images/user/visa/'.$rows->PhotoForVisa)}}" alt="User Avatar">
                            @else
                                <img class="img-circle" src="{{asset('images/user/visa/avatar.png')}}" alt="User Avatar">
                            @endif
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">3,200</h5>
                                        <span class="description-text">E-mail</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">1</h5>
                                        <span class="description-text">Award</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header">3</h5>
                                        <span class="description-text">Visa</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->

                            </div>

                            <div >
                                <button type="button" class="btn btn-info btn-xs pull-left"><i class="fa fa-user"></i> Update Profile</button>
                                <a class="btn btn-success btn-xs pull-right" href="{{url('user/moreInfo/'.$rows->PassportID)}}">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>

                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
                <!-- /.col -->
        @endforeach


        <!-- /.col -->
    </div>

@endsection