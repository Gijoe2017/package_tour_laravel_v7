<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        @if(Auth::check())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="user-image" title="{{ Auth::user()->name }}">
                </div>
                <div class="pull-left info">
                    <p> {{Auth::user()->name}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif
        <!-- search form -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
          {{--<span class="input-group-btn">--}}
                {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                {{--</button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}

        <form id="form-set-timeline"  action="{{action('Member\MemberController@setTimeline')}}" method="post" >
        {{csrf_field()}}
            <fieldset class="form-group">
            <select name="timeline" id="timeline" class="form-control">
                <option value="">{{trans('common.select_timeline')}}</option>
                @if(Session::get('Timelines'))
                    @foreach(Session::get('Timelines') as $rows)
                    <option value="{{$rows->id}}" {{Session::get('timeline_id')==$rows->id?'selected':''}}>{{$rows->name}}</option>
                    @endforeach
                @endif
            </select>
            {{--{{ Form::label('timeline', trans('common.timeline'), ['class' => 'control-label']) }}--}}
            {{--{{ Form::select('timeline', array('' => trans('common.select_timeline')) + Session::get('timeline_options'), Session::get('timeline_id'), array('class' => 'form-control')) }}--}}
            {{--@if ($errors->has('timeline'))--}}
                {{--<span class="help-block">--}}
                    {{--{{ $errors->first('timeline') }}--}}
                {{--</span>--}}
            {{--@endif--}}
        </fieldset>


        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{!! (Request::segment(2)=='dashboard' ? 'active' : '') !!}">
                <a href="{{url('member/dashboard/'.Session::get('timeline_id'))}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview {!! (Request::segment(1)=='booking' ? 'active' : '') !!}">
                <a href="#"><i class="fa fa-calendar-check-o"></i> <span>{{trans('common.booking')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::segment(2)=='backend' ? 'active' : '') !!}">
                        <a href="{{url('booking/backend/main')}}"><i class="fa fa-circle-o"></i> {{trans('common.package_list')}}</a>
                    </li>
                </ul>
            </li>
            @if(!Session::get('module_id') )
                <?php
                  $modules=\App\ModuleType::where('language_code',Session::get('language'))->get();
                ?>
                @foreach($modules as $module)
                    @if($module->module_open()->where('business_verified_status','verified')->where('timeline_id',Session::get('timeline_id'))->first())
                    <li>
                        <a href="{{url('module/manage/'.$module->module_type_id)}}">
                            <i class="fa {{$module->module_icon}}"></i> <span>{{$module->module_name}} </span>
                        </a>
                    </li>
                    @endif
                @endforeach
            @endif

            @if(Session::get('module_id')=='1')

                {{--@if(Session::get('mode')!='package')--}}
                {{--<li>--}}
                    {{--<a href="{{url('/member/package/manage')}}">--}}
                        {{--<i class="fa fa-calendar"></i> <span>Package tour </span>--}}
                        {{--<span class="pull-right-container">--}}
                              {{--<small class="label pull-right bg-green">{{Session::get('Packages')}}</small>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--@elseif(Session::get('mode')=='member')--}}

                {{--@endif--}}
                    {{--@if(Session::get('passport_id'))--}}
                        {{--<li>--}}
                            {{--<a href="{{url('member/manage/'.Session::get('passport_id'))}}">--}}
                                {{--<i class="fa fa-users"></i> <span>Members </span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--@endif--}}

                {{--@if(Session::get('mode')=='package')--}}
                    {{--<li>--}}
                        {{--<a href="{{url('/member/package/manage')}}">--}}
                            {{--<i class="fa fa-calendar"></i> <span>Package tour </span>--}}
                            {{--<span class="pull-right-container">--}}
                            {{--<small class="label pull-right bg-green">{{Session::get('Packages')}}</small>--}}
                            {{--</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                {{--@else--}}
                @if(Session::get('mode')=='member')

                    @if(Session::get('passport_id'))
                        <li class="treeview {!! (Request::segment(1)=='member' ? 'active' : '') !!}" >
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <span>Passport details</span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{!! (Request::segment(1)=='member' && Request::segment(2)=='edit'? 'active' : '') !!}">
                                <a href="{{url('member/edit/'.Session::get('passport_id'))}}"><i class="fa fa-circle-o text-aqua"></i> {{trans('LProfile.general')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='edit-step2' ? 'active' : '') !!}">
                                <a href="{{url('member/edit-step2')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.passport')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='contact' || Request::segment(2)=='edit-step3' ? 'active' : '') !!}">
                                <a href="{{url('member/contact')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.contact')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='step4' || Request::segment(2)=='family' ? 'active' : '') !!}">
                                <a href="{{url('member/step4')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.family')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='education' ? 'active' : '') !!}">
                                <a href="{{url('member/education')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.education')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='documents' ? 'active' : '') !!}">
                                <a href="{{url('member/documents')}}"><i class="fa fa-file-pdf-o"></i> {{trans('LProfile.document_files')}}</a>
                            </li>

                            <li class="{!! (Request::segment(2)=='reference' ? 'active' : '') !!}">
                                <a href="{{url('member/reference')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.reference')}}</a>
                            </li>

                            <li class="{!! (Request::segment(2)=='occupation' ? 'active' : '') !!}">
                                <a href="{{url('member/occupation')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.occupation')}}</a>
                            </li>

                            <li class="{!! (Request::segment(2)=='place' ? 'active' : '') !!}">
                                <a href="{{url('member/place')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.place_stay')}}</a>
                            </li>
                            <li class="{!! (Request::segment(2)=='visited' ? 'active' : '') !!}">
                                <a href="{{url('member/visited')}}"><i class="fa fa-circle-o"></i> {{trans('LProfile.travel_history')}}</a>
                            </li>
                            {{--<li><a href="{{url('report/group/3')}}"><i class="fa fa-circle-o"></i> Accessories</a></li>--}}
                        </ul>
                    </li>
                    @else
                        <li class="treeview {!! (Request::segment(1)=='member' ? 'active' : '') !!}" >
                            <a href="{{url('/member/list')}}">
                                <i class="fa fa-user-md"></i>
                                <span>Passport</span>
                                <span class="pull-right-container">
                                        <small class="label pull-right bg-red">
                                        {{App\models\UserProfile::where('timeline_id',Session::get('timeline_id'))->count()}}
                                        </small>
                                </span>
                            </a>
                        </li>
                    @endif
                @elseif(Session::get('mode')=='package')
                    <li class="treeview {!! (Request::segment(1)=='package' ? 'active' : '') !!}">
                            <a href="#"><i class="fa fa-cube"></i> <span>{{trans('common.package_manager')}}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{!! (Request::segment(2)=='partner' ? 'active' : '') !!}">
                                    <a href="{{url('package/partner')}}"><i class="fa fa-circle-o"></i> {{trans('common.package_partner')}}</a>
                                </li>

                                <li class="{!! (Request::segment(2)=='list' ? 'active' : '') !!}">
                                    <a href="{{url('package/list')}}">
                                        <i class="fa fa-circle-o"></i> {{trans('common.package_list')}}
                                        <span class="pull-right-container">
                                        <small class="label pull-right bg-green">{{Session::get('Packages')}}</small>
                                        </span>
                                    </a>
                                </li>
                                <li class="{!! (Request::segment(2)=='create' ? 'active' : '') !!}"><a href="{{url('package/create')}}"><i class="fa fa-circle-o"></i> {{trans('common.create_new_package')}}</a></li>
                               @if(Session::get('event')!='')
                                <li class="{!! (Request::segment(2)=='general' ? 'active' : '') !!}"><a href="{{url('package/edit/'.Session::get('package'))}}"><i class="fa fa-circle-o"></i> {{trans('common.general')}}</a></li>
                                <li class="{!! (Request::segment(2)=='info' ? 'active' : '') !!}"><a href="{{url('package/info')}}"><i class="fa fa-circle-o"></i> {{trans('common.information')}}</a></li>
                                <li class="{!! (Request::segment(2)=='schedule' ? 'active' : '') !!}"><a href="{{url('package/schedule')}}"><i class="fa fa-circle-o"></i> {{trans('common.schedule')}}</a></li>
                                <li class="{!! (Request::segment(2)=='details' ? 'active' : '') !!}"><a href="{{url('package/details')}}"><i class="fa fa-circle-o"></i> {{trans('common.details')}}</a></li>
                                <li class="{!! (Request::segment(2)=='condition' ? 'active' : '') !!}"><a href="{{url('package/condition/list')}}"><i class="fa fa-circle-o"></i> {{trans('common.condition')}}</a></li>
                                @endif

                                <li class="{!! (Request::segment(2)=='list' ? 'active' : '') !!}">
                                    <a href="{{url('member/list')}}">
                                        <i class="fa fa-users"></i> <span>{{trans('common.passport')}}</span>
                                        <span class="pull-right-container">
                                        <small class="label pull-right bg-red">
                                        {{App\models\UserProfile::where('timeline_id',Session::get('timeline_id'))->count()}}
                                        </small>
                                        </span>
                                    </a>
                                </li>


                            </ul>

                        </li>
                @endif

                @elseif(Session::get('module_id')=='3')
                    <li class="treeview {!! (Request::segment(1)=='product' ? 'active' : '') !!}">
                        <a href="#"><i class="fa fa-cube"></i> <span>{{trans('common.product')}}</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="{!! (Request::segment(2)=='list' ? 'active' : '') !!}"><a href="{{url('product/list')}}"><i class="fa fa-circle-o"></i> {{trans('common.product_list')}}</a></li>
                            <li class="{!! (Request::segment(2)=='create' ? 'active' : '') !!}"><a href="{{url('product/create')}}"><i class="fa fa-circle-o"></i> {{trans('common.create_new_product')}}</a></li>
                        </ul>
                    </li>
                    <li class="treeview {!! (Request::segment(1)=='order' ? 'active' : '') !!}">
                        <a href="#"><i class="fa fa-opencart"></i> <span>Orders</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="{!! (Request::segment(2)=='list' ? 'active' : '') !!}"><a href="{{url('order/list')}}"><i class="fa fa-circle-o"></i> Order List</a></li>
                            <li class="{!! (Request::segment(2)=='create' ? 'active' : '') !!}"><a href="{{url('order/create')}}"><i class="fa fa-circle-o"></i> Create Order</a></li>
                        </ul>
                    </li>
                    <li class="treeview {!! (Request::segment(1)=='report' ? 'active' : '') !!}">
                    <a href="#"><i class="fa fa-folder-o"></i> <span>Reports</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{!! (Request::segment(2)=='product' ? 'active' : '') !!}"><a href="{{url('report/product')}}"><i class="fa fa-circle-o"></i> Report Products</a></li>
                        <li class="{!! (Request::segment(2)=='order' ? 'active' : '') !!}"><a href="{{url('report/order')}}"><i class="fa fa-circle-o"></i> Report Order</a></li>
                    </ul>
                </li>
                @endif

                <li class="treeview {!! (Request::segment(1)=='setting' ? 'active' : '') !!}">
                <a href="#"><i class="fa fa-gear"></i> <span>{{trans('common.settings')}}</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::segment(2)=='title' ? 'active' : '') !!}"><a href="{{url('setting/title')}}"><i class="fa fa-circle-o"></i> Set Title</a></li>
                    <li class="{!! (Request::segment(2)=='status' ? 'active' : '') !!}"><a href="{{url('setting/status')}}"><i class="fa fa-circle-o"></i> Set Status</a></li>
                    <li class="{!! (Request::segment(2)=='relation' ? 'active' : '') !!}"><a href="{{url('setting/relation')}}"><i class="fa fa-circle-o"></i> Set Relation</a></li>
                    <li class="{!! (Request::segment(2)=='addresstype' ? 'active' : '') !!}"><a href="{{url('setting/addresstype')}}"><i class="fa fa-circle-o"></i> Set Address Type</a></li>
                    {{--<li><a href="{{url('report/group/3')}}"><i class="fa fa-circle-o"></i> Accessories</a></li>--}}

                </ul>
            </li>

            {{--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>