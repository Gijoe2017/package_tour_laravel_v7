<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b><img src="{{asset('images/logo-toechok.png')}}"> Toe</b> Chok</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><img src="{{asset('images/logo-toechok.png')}}"> Toe</b> Chok</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::check())
                            @if(Auth::user()->language != null)
                                <?php $key = Auth::user()->language; ?>
                            @else
                                <?php $key = App\Setting::get('language'); ?>
                            @endif

                        @else
                            <?php $key = Session::get('language'); ?>

                        @endif


                        @if($key == 'en')
                            <span class="flag-icon flag-icon-us"></span>
                        @elseif($key == 'iw')
                            <span class="flag-icon flag-icon-il"></span>
                        @elseif($key == 'ja')
                            <span class="flag-icon flag-icon-jp"></span>
                        @elseif($key == 'zh')
                            <span class="flag-icon flag-icon-cn"></span>
                        @elseif($key == 'hi')
                            <span class="flag-icon flag-icon-in"></span>
                        @elseif($key == 'fa')
                            <span class="flag-icon flag-icon-ir"></span>
                        @else
                            <span class="flag-icon flag-icon-{{ $key }}"></span>
                        @endif
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach( Config::get('app.locales') as $key => $value)
                            <li class="">
                                <a href="#" class="switch-language" data-language="{{ $key }}" data-id="{{url()->current()}}">

                                    @if($key == 'en')
                                        <span class="flag-icon flag-icon-us"></span>
                                    @elseif($key == 'iw')
                                        <span class="flag-icon flag-icon-il"></span>
                                    @elseif($key == 'ja')
                                        <span class="flag-icon flag-icon-jp"></span>
                                    @elseif($key == 'zh')
                                        <span class="flag-icon flag-icon-cn"></span>
                                    @elseif($key == 'hi')
                                        <span class="flag-icon flag-icon-in"></span>
                                    @elseif($key == 'fa')
                                        <span class="flag-icon flag-icon-ir"></span>
                                    @else
                                        <span class="flag-icon flag-icon-{{ $key }}"></span>
                                    @endif
                                        {{ $value }}
                                        <?php
                                        $checklist="<span class=\"pull-right badge bg-aqua\"><i class=\"fa fa-check-circle\"></i></span>";
                                        ?>
                                        @if(Session::get('mode_lang')=='member')

                                            @foreach(App\models\LanguageInfo::where('passport_id',Session::get('passport_id'))->get() as $rows)
                                                {!! $rows->language_code==$key?$checklist:'' !!}
                                            @endforeach

                                        @elseif(Session::get('mode_lang')=='education')

                                            @foreach(App\models\LanguageEducation::where('passport_id',Session::get('passport_id'))->get() as $rows)
                                                {!! $rows->language_code==$key?$checklist:'' !!}
                                            @endforeach
                                        @elseif(Session::get('mode_lang')=='reference')

                                            @foreach(App\models\LanguageRef::where('passport_id',Session::get('passport_id'))->get() as $rows)
                                                {!! $rows->language_code==$key?$checklist:'' !!}
                                            @endforeach
                                        {{--@elseif(Session::get('mode')=='occupation')--}}
                                            {{--@foreach(App\models\LanguageOccu::where('passport_id',Session::get('passport_id'))->get() as $rows)--}}
                                                {{--{!! $rows->language_code==$key?$checklist:'' !!}--}}
                                            {{--@endforeach--}}
                                        {{--@elseif(Session::get('mode')=='place')--}}
                                            {{--@foreach(App\models\LanguagePlace::where('passport_id',Session::get('passport_id'))->get() as $rows)--}}
                                                {{--{!! $rows->language_code==$key?$checklist:'' !!}--}}
                                            {{--@endforeach--}}
                                        @elseif(Session::get('mode_lang')=='visited')
                                            <?php $check=App\models\LanguageVisited::where('passport_id',Session::get('passport_id'))->first();?>
                                             @if($check)
                                                @foreach( $check->visit_language() as $rows)
                                                {!! $rows->language_code==$key?$checklist:'' !!}
                                                @endforeach
                                             @endif
                                        @else
                                            @foreach(App\models\LanguageInfoFamily::where('relation_id',Session::get('relation_id'))->get() as $rows)
                                                {!! $rows->language_code==$key?$checklist:'' !!}
                                            @endforeach
                                        @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>

                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                {{--<li class="dropdown notifications-menu">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="fa fa-bell-o"></i>--}}
                        {{--<span class="label label-warning">10</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">You have 10 notifications</li>--}}
                        {{--<li>--}}
                            {{--<!-- inner menu: contains the actual data -->--}}
                            {{--<ul class="menu">--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-users text-aqua"></i> 5 new members joined today--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the--}}
                                        {{--page and may cause design problems--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-users text-red"></i> 5 new members joined--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-shopping-cart text-green"></i> 25 sales made--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<i class="fa fa-user text-red"></i> You changed your username--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer"><a href="#">View all</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <!-- Tasks: style can be found in dropdown.less -->
                {{--<li class="dropdown tasks-menu">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="fa fa-flag-o"></i>--}}
                        {{--<span class="label label-danger">9</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">You have 9 tasks</li>--}}
                        {{--<li>--}}
                            {{--<!-- inner menu: contains the actual data -->--}}
                            {{--<ul class="menu">--}}
                                {{--<li><!-- Task item -->--}}
                                    {{--<a href="#">--}}
                                        {{--<h3>--}}
                                            {{--Design some buttons--}}
                                            {{--<small class="pull-right">20%</small>--}}
                                        {{--</h3>--}}
                                        {{--<div class="progress xs">--}}
                                            {{--<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"--}}
                                                 {{--aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">20% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<!-- end task item -->--}}
                                {{--<li><!-- Task item -->--}}
                                    {{--<a href="#">--}}
                                        {{--<h3>--}}
                                            {{--Create a nice theme--}}
                                            {{--<small class="pull-right">40%</small>--}}
                                        {{--</h3>--}}
                                        {{--<div class="progress xs">--}}
                                            {{--<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"--}}
                                                 {{--aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">40% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<!-- end task item -->--}}
                                {{--<li><!-- Task item -->--}}
                                    {{--<a href="#">--}}
                                        {{--<h3>--}}
                                            {{--Some task I need to do--}}
                                            {{--<small class="pull-right">60%</small>--}}
                                        {{--</h3>--}}
                                        {{--<div class="progress xs">--}}
                                            {{--<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"--}}
                                                 {{--aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">60% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<!-- end task item -->--}}
                                {{--<li><!-- Task item -->--}}
                                    {{--<a href="#">--}}
                                        {{--<h3>--}}
                                            {{--Make beautiful transitions--}}
                                            {{--<small class="pull-right">80%</small>--}}
                                        {{--</h3>--}}
                                        {{--<div class="progress xs">--}}
                                            {{--<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"--}}
                                                 {{--aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">80% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<!-- end task item -->--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer">--}}
                            {{--<a href="#">View all tasks</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <!-- User Account: style can be found in dropdown.less -->
                @if(Auth::check())
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="user-image" title="{{ Auth::user()->name }}">
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="user-image" title="{{ Auth::user()->name }}">
                            <p>
                                {{Auth::user()->name}}
                                <small>Member since {{date('F m',strtotime(date('Y-m-m')))}}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <form action="{{url('/logout')}}" method="post">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-default btn-flat" href="{{url('logout')}}"><i class="fa fa-key"></i> Sign out</button>
                                </form>

                            </div>
                        </li>
                    </ul>
                </li>
                @endif
                <!-- Control Sidebar Toggle Button -->
                {{--<li>--}}
                    {{--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</header>