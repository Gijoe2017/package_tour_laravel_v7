<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Visa Information</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('member/assets/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('member/assets/Ionicons/css/ionicons.min.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('member/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/bootstrap-formhelpers.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/skins/skin-red.css')}}">
    <link rel="stylesheet" href="{{asset('member/css/style.css')}}">
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('member/assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>

    <!-- DataTables -->
    {{--<link rel="stylesheet" href="{{asset('member/assets/plugins/datepicker/bootstrap-datetimepicker.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('datetime/css/bootstrap-datetimepicker.css')}}">--}}
            <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('member/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('member/assets/plugins/datatables/dataTables.bootstrap.css')}}">
    <link href="{{asset('member/js/datepicker/css/datepicker.css')}}" rel='stylesheet' type='text/css' />
    <script src="{{asset('member/js/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('member/js/dropzone/dropzone.min.js')}}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-bottom: 16px solid blue;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .preload{
            background-color: rgba(250, 250, 250, .5);
            color: rgba(250, 250, 250, .5);
            z-index: 100;
            padding-top: 15%;
            width: 90%;
            height: 100%;
            position: absolute;
        }

    </style>

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('member/assets/plugins/iCheck/all.css')}}">
    <link rel="stylesheet" href="{{asset('member/dropzone/dropzone.css')}}">

</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    @include('layouts.member.include.header')
    <!-- Left side column. contains the logo and sidebar -->
    {{--@include('layouts.member.include.sidebar')--}}

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        {{--@yield('pageHeader')--}}
        <!-- Main content -->
        <section class="content">

            <!-- Info boxes -->
            @yield('content')
            <!-- /.row -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.member.include.footer')


</div><!-- ./wrapper -->


<!-- Bootstrap 3.3.5 -->
<script src="{{asset('member/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('member/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('member/assets/plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('member/assets/dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('member/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('member/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('member/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('member/assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('member/assets/plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{asset('member/assets/dist/js/demo.js')}}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{asset('member/assets/plugins/iCheck/icheck.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('member/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('member/assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>


<!-- InputMask -->
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script src="{{asset('member/assets/dist/js/bootstrap-formhelpers.js')}}"></script>
<script src="{{asset('member/assets/dist/js/bootstrap-filestyle.js')}}"></script>
{{--<script src="{{asset('member/assets/dist/js/autosaveform.js')}}"></script>--}}



<script>
    function SP_source() {
        return "{{url('/')}}/";
    }
    var baseUrl = "{{ url('member/upload/docs') }}";
    var token = "{{ Session::token() }}";

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#FileUpload", {
        dictDefaultMessage: "<img src='{{asset('images/upload.png')}}' width='150'>",
        url: baseUrl ,
        params: {
            _token: token
        },
        success: function(file, response){
            console.log('WE NEVER REACH THIS POINT.');
            var id='show';
            document.getElementById('FileUpload').style.backgroundColor = 'write';
//            showImage(id);
        },
    });
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        addRemoveLinks: true,
        acceptedFiles: ".png, .jpg,.jpeg, .pdf, .doc, .docs",
        accept: function(file, done) {

        },
    };






    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    })

    function confirmDel(){
        if(confirm('Are you sure you want to delete?')){
            return true;
        }else{
            return false;
        }
    }

</script>



<script type="text/javascript">

//    var formsave1=new autosaveform({
//        formid: 'AutoForm',
//        pause: 1000
//    });





$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
});


    function confirmDel() {
        if(confirm('Do you want to delete!')){
            return true;
        }else{
            return false;
        }
    }


    $(function () {
        $("[data-mask]").inputmask();

        $('#Table_customer').DataTable({
            "ordering": false
        });
        $('#exam').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });


    $(function () {
        $('#table_master').DataTable()
        $('#table_master2').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : false,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : false
        })
    })





    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
//        $(":file").filestyle({buttonName: "btn-primary"});

    $('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        content: function(){return '<img src="'+$(this).data('img') + '" />';}
    });
    $("#imgInp").change(function(){
        alert('test');
        readURL(this);
    });

</script>



</body>
</html>
