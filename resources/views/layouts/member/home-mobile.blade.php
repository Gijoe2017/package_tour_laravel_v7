@extends('theme.AdminLTE.layout_master')

{{--@section('content')--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">CPU Traffic</span>--}}
                    {{--<span class="info-box-number">90<small>%</small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-red"><i class="fa fa-taxi"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Cars</span>--}}
                    {{--<span class="info-box-number">40<small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}

        {{--<!-- fix for small devices only -->--}}
        {{--<div class="clearfix visible-sm-block"></div>--}}

        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Booking</span>--}}
                    {{--<span class="info-box-number">{{\App\Bookings::isset()}}<small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Customers</span>--}}
                    {{--<span class="info-box-number">90<small></small></span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
    {{--</div>--}}
{{--@endsection--}}


@section('content-chart')
    <style type="text/css">
        .btn{
            text-align: left;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Booking List.</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <div class="btn-group">
                            <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <!-- DIRECT CHAT PRIMARY -->
                            @if(isset($Bookings))
                                @foreach($Bookings as $rows)

                                    <?php
                                    $date2=date_create($rows->PickupDate.' '.$rows->PickupTime);
                                    $date1=date_create(date('Y-m-d H:i:s'));
                                    $diff=date_diff($date1,$date2);
                                    $m= $diff->format("%m");
                                    $day= $diff->format("%d");

                                    if($m>0){
                                        if($m>1){
                                            $days=$diff->format("%m Months %d Days");
                                        }else{
                                            $days=$diff->format("%m Month %d Days");
                                        }

                                    }else{
                                        if($day>1){
                                            $days=$diff->format("%d Days");
                                        }else if($day>0){
                                            $days=$diff->format("%d Day");
                                        }else{
                                            $days=$diff->format("%h : %i m");
                                        }
                                    }
                                    ?>
                                    <div class="box box-default collapsed-box">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">{{$rows->CustFirstName}}</h4>
                                            <h5>Pickup Date: <strong class="text-danger">{{date('F d, Y',strtotime($rows->PickupDate)).' - '.date('H:i',strtotime($rows->PickupTime))}}</strong><br>
                                                Origin: <strong>{{$rows->Origin1}}</strong><br>
                                                @if(!empty($rows->OrginPlace))
                                                    Meeting Point: {{$rows->OrginPlace}}
                                                @endif

                                            </h5>
                                            <div class="navbar-custom-menu-detail">
                                                <ul class="nav navbar-nav">
                                                    <!-- Notifications: style can be found in dropdown.less -->
                                                    <li class="dropdown notifications-menu">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-calendar"></i>
                                                            <span class="label label-success"> Action</span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <!-- inner menu: contains the actual data -->
                                                                <ul class="menu">
                                                                    @if($rows->BookStatus!='X' and $rows->BookStatus!='C')
                                                                        <li>
                                                                            <a href="{{url('booking/update/'.$rows->BookID)}}">
                                                                                <i class="fa fa-edit text-aqua"></i>Edit
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-calendar-plus-o text-aqua"></i>Booking Request <i class="fa fa-check-square-o text-green"></i>
                                                                        </a>
                                                                    </li>

                                                                    <li>
                                                                        <a href="{{$rows->BookStatus=='R'?url('update/followed/'.$rows->BookID):'#'}}">
                                                                            <i class="fa fa-phone-square text-yellow"></i>Following Request
                                                                            @if(\App\HistoryStatus::where('BookID',$rows->BookID)->where('BookStatus','F')->isset()>0)
                                                                                <i class="fa fa-check-square-o text-green"></i>
                                                                            @endif
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{$rows->BookStatus=='R'|| $rows->BookStatus=='F' || $rows->BookStatus=='A'?url('update/setting_car/'.$rows->BookID):'#'}}">
                                                                            <i class="fa fa-taxi text-red"></i>Booking Confirm

                                                                            @if(\App\HistoryStatus::where('BookID',$rows->BookID)->where('BookStatus','A')->isset()>0)
                                                                                <i class="fa fa-check-square-o text-green"></i>
                                                                            @endif
                                                                        </a>
                                                                    </li>

                                                                    <li>
                                                                        <a href="{{$rows->BookStatus=='A' || $rows->BookStatus=='F'?url('update/complete/'.$rows->BookID):'#'}}">
                                                                            <i class="fa fa-folder text-green"></i>Complete Service
                                                                            @if(\App\HistoryStatus::where('BookID',$rows->BookID)->where('BookStatus','C')->isset()>0)
                                                                                <i class="fa fa-check-square-o text-green"></i>
                                                                            @endif
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{$rows->BookStatus=='A' || $rows->BookStatus=='F'?url('update/cancel/'.$rows->BookID):'#'}}">
                                                                            <i class="fa fa-calendar-times-o text-red"></i>Booking Cancel

                                                                            @if(\App\HistoryStatus::where('BookID',$rows->BookID)->where('BookStatus','X')->isset()>0)
                                                                                <i class="fa fa-check-square-o text-green"></i>
                                                                            @endif
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="footer"><a href="">Close</a></li>
                                                        </ul>
                                                    </li>
                                                    <!-- Tasks: style can be found in dropdown.less -->
                                                    <li class="dropdown tasks-menu">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-history"></i>
                                                            <span class="label label-warning"> History</span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <!-- inner menu: contains the actual data -->
                                                                <ul class="menu">
                                                                    <?php
                                                                    $History=DB::table('taxi_status_update as a')
                                                                            ->join('taxi_status as b','b.StatusCode','=','a.BookStatus')
                                                                            ->join('users as c','c.id','=','a.UpdateBy')
                                                                            ->where('a.BookID',$rows->BookID)
                                                                            ->orderby('a.UpdateDate','desc')
                                                                            ->get();
                                                                    //  dd($History);
                                                                    ?>
                                                                    @foreach($History as $rowH)
                                                                        <li><!-- Task item -->
                                                                            <a href="#">
                                                                                <i class="fa fa-clock-o"></i>
                                                                                <small>{{date('F d, Y - H:i',strtotime($rowH->UpdateDate))}}</small><br>
                                                                                @if($rowH->BookStatus=='R')
                                                                                    {{$rowH->StatusName}} by <small class="text-danger"> {{$rows->CustFirstName}}</small>
                                                                                @else
                                                                                    {{$rowH->StatusName}} by <small class="text-danger"> {{$rowH->name}}</small>
                                                                                @endif
                                                                                <br>
                                                                                <div class="progress xs">
                                                                                    <div class="progress-bar progress-bar-{{$rowH->StatusColor}}" style="width: {{$rowH->StatusProcess}}%" role="progressbar"
                                                                                         aria-valuenow="{{$rowH->StatusProcess}}" aria-valuemin="0" aria-valuemax="100">
                                                                                        <span class="sr-only">{{$rowH->StatusProcess}}% Complete</span>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    @endforeach

                                                                    <li class="footer">
                                                                        <a href="#">View all tasks</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <!-- User Account: style can be found in dropdown.less -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="box-tools pull-right">
                                                @if($rows->BookStatus!='C')
                                                    <span data-toggle="tooltip" title="" class="badge {{$day>0?'bg-blue':'bg-red'}}" data-original-title="{{$days}} to work">{{$days}}</span>
                                                @endif
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"> Detail</i>
                                                </button>
                                            </div>

                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <!-- Conversations are loaded here -->
                                            <div class="direct-chat-messages">
                                                <!-- Message. Default to the left -->
                                                <div class="direct-chat-msg">

                                                    <!-- /.direct-chat-info -->
                                                    <div>
                                                        Booking Number: #{{$rows->BookID}}
                                                        <br>
                                                        Group: {{$rows->BooKGroup=='1'?'Airport':'Golf'}}<br>
                                                        Booking Date: {{date("F d, Y - H:i",strtotime($rows->created_at))}}<br>
                                                        Origin: <strong>{{$rows->Origin1}}</strong><br>
                                                        @if(!empty($rows->FilightNumber))
                                                            Flight/Meeting Point: {{$rows->FilightNumber}}
                                                        @endif
                                                        @if(!empty($rows->OrginPlace))
                                                            Meeting Point: {{$rows->OrginPlace}} <br>
                                                        @endif

                                                        Destination: <strong>{{$rows->Destination1}}</strong><br>
                                                        @if(!empty($rows->FilightNumberDes))
                                                            {{$rows->FilightNumberDes}}
                                                        @endif
                                                        @if(!empty($rows->DestinationPlace))
                                                            Point of Arrival: {{$rows->DestinationPlace}} <br>
                                                        @endif

                                                        Type Of Vehicle: <strong>{{$rows->VehicleTypeName}}</strong><br>
                                                        Type Of Trip: <strong>{{$rows->TypeOfTrip}}</strong><br>
                                                        Pickup Date: <strong class="text-danger">{{date('F d, Y',strtotime($rows->PickupDate)).' - '.date('H:i',strtotime($rows->PickupTime))}}</strong><br>
                                                        Passengers: Adult(s): <strong>{{$rows->AdultNumber}}</strong>
                                                        @if($rows->ChildNumber>0)
                                                            Child : <strong>{{$rows->ChildNumber}}</strong>
                                                        @endif
                                                        <br>
                                                        @if(!empty($rows->Comments))
                                                            Comment: <strong>{{$rows->Comments}} </strong><br>
                                                        @endif
                                                        --Contact Info--<br>
                                                        {{$rows->CustFirstName}}<br>
                                                        Country: {{$rows->Country}}<br>
                                                        Email: {{$rows->CustEmail}}<br>
                                                        Phone: {{$rows->phoneCode}} {{$rows->Phone}}
                                                    </div>
                                                    <!-- /.direct-chat-text -->
                                                </div>
                                                <!-- /.direct-chat-msg -->

                                                <!-- Message to the right -->
                                                <!-- /.direct-chat-msg -->
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <form action="#" method="post">
                                            </form>
                                        </div>
                                        <!-- /.box-footer-->
                                    </div>
                                @endforeach

                            @endif



                        </div>
                    </div><!-- /.row -->

                </div><!-- ./box-body -->




                <div class="box-footer">

                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                           Booking pattaya taxi
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-footer -->
            </div><!-- /.box -->


            <div class="box">

                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <a href="{{url('booking/R')}}">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="ion ion-ios-gear-outline"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Booking Request</span>
                                    <span class="info-box-number">
                                     @if(\App\Bookings::where('BookStatus','R')->isset()>0)
                                            {{\App\Bookings::where('BookStatus','R')->isset()}}<small></small>
                                        @endif
                                </span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </a>
                    </div><!-- /.col -->
                    <div class="col-sm-12 col-xs-12">
                        <a href="{{url('booking/F')}}">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Following Request</span>
                                    <span class="info-box-number">
                                     @if(\App\Bookings::where('BookStatus','F')->isset()>0)
                                            {{\App\Bookings::where('BookStatus','F')->isset()}}<small></small>
                                        @endif
                                </span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </a>
                    </div><!-- /.col -->

                    <div class="col-sm-12 col-xs-12">
                        <a href="{{url('booking/A')}}">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Booking Confirm</span>
                                    <span class="info-box-number">
                                     @if(\App\Bookings::where('BookStatus','A')->isset()>0)
                                            {{\App\Bookings::where('BookStatus','A')->isset()}}<small></small>
                                        @endif

                                </span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </a>
                    </div><!-- /.col -->

                    <div class="col-sm-12 col-xs-12">
                        <a href="{{url('booking/C')}}">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="ion ion-ios-gear-outline"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Complete Service</span>
                                    <span class="info-box-number">
                                     @if(\App\Bookings::where('BookStatus','C')->isset()>0)
                                            {{\App\Bookings::where('BookStatus','C')->isset()}}<small></small>
                                        @endif

                                </span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </a>
                    </div><!-- /.col -->

                    <div class="col-sm-12 col-xs-12">
                        <a href="{{url('booking/X')}}">
                            <div class="info-box">
                                <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Booking Cancel</span>
                                    <span class="info-box-number">
                                     @if(\App\Bookings::where('BookStatus','X')->isset()>0)
                                            {{\App\Bookings::where('BookStatus','X')->isset()}}<small></small>
                                        @endif
                                </span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </a>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.box-footer -->
        </div><!-- /.col -->
    </div>
@endsection