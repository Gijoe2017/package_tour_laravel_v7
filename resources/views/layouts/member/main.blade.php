@extends('theme.AdminLTE.layout_customer')
@section('pageHeader')
    <section class="content-header">
            <h1>{{Session::get('menu')}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{Session::get('menu')}}</li>
        </ol>
    </section>
@endsection

@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                @if(Session::get('SetDate')=='Off')
                    <h3 style="font-size: 33px"> {{Session::get('SetDate')}}</h3>
                @else
                    <h3 style="font-size: 33px"> <span id="demo" class="text-danger"></span></h3>
                @endif
                <h2>Time out for Order</h2>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{Session::get('OrdersNum')}}</h3>
                <h2>Current Order</h2>
            </div>
            <div class="icon">
                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{Session::get('OrdersNumX')}}</h3>
                <h2>Cancel Product</h2>
            </div>
            <div class="icon">
                <i class="fa fa-cubes" aria-hidden="true"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>65</h3>
                <h2>Payments</h2>
            </div>
            <div class="icon">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Orders</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            @if(isset($Orders))
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr><th></th>
                        <th width="20%">Order Number</th>
                        <th width="20%">Order Date</th>
                        <th width="20%">Order Details</th>
                        <th width="20%">Total Price</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($Orders as $rows)
                        <tr>
                            <td class="text-center">
                                <a href="#" class="show_hide" data-id="{{$rows->OrderNumber}}">
                                    <span id="show{{$rows->OrderNumber}}"  class="fa fa-plus"></span>
                                </a>
                            </td>
                            <td >{{$rows->OrderNumber}}</td>
                            <td >{{$rows->OrderDate}}</td>
                            <td>
                                <a data-toggle="modal"  href="{{url('order/details/'.$rows->OrderNumber)}}"  data-target="#modalOrder" >
                                   <i class="fa fa-search-plus"></i> View Details
                                </a>
                            </td>
                            <td >{{number_format($rows->TotalPrice)}}</td>

                            <td>{{$rows->StatusDetails}}</td>
                            <td><a href="{{url('order/export/excel/'.$rows->OrderNumber)}}" class="btn btn-info"><i class="fa fa-download"></i> Export to Excel </a> </td>
                        </tr>
                        <tr id="trShow{{$rows->OrderNumber}}" style="display: none">
                            <td  colspan="7" align="right">
                                <div  id="Details{{$rows->OrderNumber}}"></div>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>

                </table>
                @endif
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        @if(Session::get('SetDate')!='Off')
        <a href="{{url('products/group/1')}}" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
        @else
            <a href="#" class="btn btn-sm btn-default btn-flat pull-left">Place New Order</a>
            <span class="text-red"> &nbsp; &nbsp; Please wait for open pre order</span>
        @endif

        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
    </div>
    <!-- /.box-footer -->
</div>
        <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div id="modalDetails" class="modal-content">

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <script src="{{asset('assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>

        <script language="javascript">
            $(document).ready(function(){
                $('.modal').on('hidden.bs.modal', function(){
                    $(this).removeData();

                });
            });
        </script>

    {{--<div class="row">--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<h3>Your Information</h3>--}}
                    {{--<span><i class="fa fa-edit"></i> Update</span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Items</span>--}}
                    {{--<span class="info-box-number">41,410</span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}

        {{--<!-- fix for small devices only -->--}}
        {{--<div class="clearfix visible-sm-block"></div>--}}

        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box">--}}
                {{--<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Orders</span>--}}
                    {{--<span class="info-box-number">760</span>--}}
                {{--</div><!-- /.info-box-content -->--}}
            {{--</div><!-- /.info-box -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-md-9 pull-left">--}}
            {{--<div class="box box-primary">--}}
                {{--<div class="box-header with-border">--}}
                    {{--<h3 class="box-title"><i class="fa fa-list-ul" aria-hidden="true"></i> Recent Orders</h3>--}}

                    {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                        {{--</button>--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- /.box-header -->--}}
                {{--<div class="box-body">--}}
                    {{--<ul class="products-list product-list-in-box">--}}

                        {{--<li class="item">--}}
                            {{--<div class="product-img">--}}
                                {{--<img src="dist/img/default-50x50.gif" alt="Product Image">--}}
                            {{--</div>--}}
                            {{--<div class="product-info">--}}
                                {{--<a href="javascript:void(0)" class="product-title">Samsung TV--}}
                                    {{--<span class="label label-warning pull-right">$1800</span></a>--}}
                    {{--<span class="product-description">--}}
                          {{--Samsung 32" 1080p 60Hz LED Smart HDTV.--}}
                        {{--</span>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<!-- /.item -->--}}
                        {{--<li class="item">--}}
                            {{--<div class="product-img">--}}
                                {{--<img src="dist/img/default-50x50.gif" alt="Product Image">--}}
                            {{--</div>--}}
                            {{--<div class="product-info">--}}
                                {{--<a href="javascript:void(0)" class="product-title">Bicycle--}}
                                    {{--<span class="label label-info pull-right">$700</span></a>--}}
                    {{--<span class="product-description">--}}
                          {{--26" Mongoose Dolomite Men's 7-speed, Navy Blue.--}}
                        {{--</span>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<!-- /.item -->--}}
                        {{--<li class="item">--}}
                            {{--<div class="product-img">--}}
                                {{--<img src="dist/img/default-50x50.gif" alt="Product Image">--}}
                            {{--</div>--}}
                            {{--<div class="product-info">--}}
                                {{--<a href="javascript:void(0)" class="product-title">Xbox One <span--}}
                                            {{--class="label label-danger pull-right">$350</span></a>--}}
                    {{--<span class="product-description">--}}
                          {{--Xbox One Console Bundle with Halo Master Chief Collection.--}}
                        {{--</span>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<!-- /.item -->--}}
                        {{--<li class="item">--}}
                            {{--<div class="product-img">--}}
                                {{--<img src="dist/img/default-50x50.gif" alt="Product Image">--}}
                            {{--</div>--}}
                            {{--<div class="product-info">--}}
                                {{--<a href="javascript:void(0)" class="product-title">PlayStation 4--}}
                                    {{--<span class="label label-success pull-right">$399</span></a>--}}
                    {{--<span class="product-description">--}}
                          {{--PlayStation 4 500GB Console (PS4)--}}
                        {{--</span>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<!-- /.item -->--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- /.box-body -->--}}
                {{--<div class="box-footer text-center">--}}
                    {{--<a href="javascript:void(0)" class="uppercase">View All Products</a>--}}
                {{--</div>--}}
                {{--<!-- /.box-footer -->--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-3 pull-right">--}}

            {{--<!-- Profile Image -->--}}
            {{--<div class="box box-primary">--}}
                {{--<div class="box-body box-profile">--}}
                    {{--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">--}}
                    {{--<h3 class="profile-username text-center">HP Sportware</h3>--}}
                    {{--<p class="text-muted text-center">puma store</p>--}}
                    {{--<style>--}}
                        {{--p {--}}
                            {{--text-align: center;--}}
                            {{--font-size: 20px;--}}
                        {{--}--}}
                    {{--</style>--}}

                    {{--<i class="fa fa-clock-o"></i>Time out--}}
                    {{--<p id="demo" class="text-red"></p>--}}


                    {{--<script>--}}
                        {{--// Set the date we're counting down to--}}
                        {{--var countDownDate = new Date("Oct 13, 2017 15:37:25").getTime();--}}

                        {{--// Update the count down every 1 second--}}
                        {{--var x = setInterval(function() {--}}

                            {{--// Get todays date and time--}}
                            {{--var now = new Date().getTime();--}}

                            {{--// Find the distance between now an the count down date--}}
                            {{--var distance = countDownDate - now;--}}

                            {{--// Time calculations for days, hours, minutes and seconds--}}
                            {{--var days = Math.floor(distance / (1000 * 60 * 60 * 24));--}}
                            {{--var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));--}}
                            {{--var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));--}}
                            {{--var seconds = Math.floor((distance % (1000 * 60)) / 1000);--}}

                            {{--// Output the result in an element with id="demo"--}}
                            {{--document.getElementById("demo").innerHTML = days + "D " + hours + "H "--}}
                                    {{--+ minutes + "m " + seconds + "s ";--}}

                            {{--// If the count down is over, write some text--}}
                            {{--if (distance < 0) {--}}
                                {{--clearInterval(x);--}}
                                {{--document.getElementById("demo").innerHTML = "EXPIRED";--}}
                            {{--}--}}
                        {{--}, 1000);--}}
                    {{--</script>--}}
                    {{--<ul class="list-group list-group-unbordered">--}}
                        {{--<li class="list-group-item">--}}
                            {{--<b>QTY</b> <a class="pull-right"><b class="text-red">1,322</b></a>--}}
                        {{--</li>--}}

                        {{--<li class="list-group-item">--}}
                            {{--<b>TOTOL PAYMENT</b> <a class="pull-right"><b class="text-red">1,322,000 ฿</b></a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}

                    {{--<a href="#" class="btn btn-success btn-block"><i class="fa fa-credit-card"></i> <b>Order Confirmation</b></a>--}}
                {{--</div><!-- /.box-body -->--}}
            {{--</div><!-- /.box -->--}}

            {{--<!-- About Me Box -->--}}
            {{--<div class="box box-primary">--}}
                {{--<div class="box-header with-border">--}}
                    {{--<h3 class="box-title">Address</h3>--}}
                {{--</div><!-- /.box-header -->--}}
                {{--<div class="box-body">--}}
                    {{--<strong><i class="fa fa-book margin-r-5"></i>  Company</strong>--}}
                    {{--<p class="text-muted">--}}
                        {{--513 in bangkok thailand--}}
                    {{--</p>--}}

                    {{--<hr>--}}

                    {{--<strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>--}}
                    {{--<p class="text-muted">Malibu, California</p>--}}



                    {{--<hr>--}}

                    {{--<strong class=" text-red"><i class="fa fa-file-text-o margin-r-5 text-red"></i> Notes</strong>--}}
                    {{--<p class=" text-red">Please check the order before you press the confirmation button.</p>--}}
                {{--</div><!-- /.box-body -->--}}
            {{--</div><!-- /.box -->--}}
        {{--</div>--}}



    {{--</div>--}}


        <script language="javascript">
            $(".show_hide").click(function(){
                var index=$(this).data("id");
                if ( $('#trShow'+$(this).data("id")).css("display") == "none" ){
                    $("#show"+$(this).data("id")).removeClass("fa fa-plus");
                    $("#show"+$(this).data("id")).addClass("fa fa-minus");
                    $.ajax({
                        url:'{{URL::to('order/showDetails')}}',
                        type:'get',
                        data:{'id':$(this).data("id")},
                        success:function (data) {
                            $('#Details'+index).html(data);
                        }
                    });

                }else{
                    $("#show"+$(this).data("id")).removeClass("fa fa-minus");
                    $("#show"+$(this).data("id")).addClass("fa fa-plus");
                }

                $('#trShow'+$(this).data("id")).toggle();
            });
        </script>


@endsection
