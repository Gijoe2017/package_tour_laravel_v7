<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <meta name="csrf_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('member/assets/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('member/assets/Ionicons/css/ionicons.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('member/assets/plugins/iCheck/all.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{asset('member/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('member/assets/select2/dist/css/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/bootstrap-formhelpers.css')}}">

    <!-- bootstrap wysihtml5 - text editor -->
    {{--<link rel="stylesheet" href="{{asset('member/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">--}}

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {{--<link rel="stylesheet" href="{{asset('member/assets/dist/css/skins/skin-red.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/css/flag-icon.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('member/assets/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
{{--<body class="hold-transition skin-blue sidebar-mini">--}}
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<h1>--}}
                {{--Advanced Form Elements--}}
                {{--<small>Preview</small>--}}
            {{--</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Forms</a></li>--}}
                {{--<li class="active">Advanced Elements</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        {{--@yield('pageHeader')--}}
        <!-- Main content -->
        <section class="content">

            <div id="preload" style="display: none;" class="preload" align="center">
                <div id="loader-wrapper" class="loader"></div>
                <p style="color:#7B8BB3">Please wait for the results to come in......</p>
            </div>

            <!-- Info boxes -->
            @yield('content')
                    <!-- /.row -->
            <!-- Main Chart row -->
            @yield('content-chart')
                    <!-- /.row -->
            <!-- Main Report row -->
            @yield('content-report')
                    <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('layouts.member.include.footer')
        <!-- Modal -->
        <div class="modal modal-primary fade" id="popupForm" tabindex="-1" role="dialog" aria-labelledby="popupForm" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div id="modalDetails" class="modal-content">

                </div>
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal" id="modal-add-language" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">

                </div>
            </div>
        </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                <p>New phone +1(800)555-1234</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                <p>nora@example.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                <p>Execution time 5 seconds</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Resume
                                <span class="label label-success pull-right">95%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Laravel Integration
                                <span class="label label-warning pull-right">50%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Back End Framework
                                <span class="label label-primary pull-right">68%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Allow mail redirect
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Expose author name in posts
                            <input type="checkbox" class="pull-right" checked>
                        </label>
                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Show me as online
                            <input type="checkbox" class="pull-right" checked>
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Turn off notifications
                            <input type="checkbox" class="pull-right">
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Delete chat history
                            <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('member/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('member/assets/select2/dist/js/select2.full.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('member/assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('member/assets/moment/min/moment.min.js')}}"></script>
<script src="{{asset('member/assets/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('member/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('member/assets/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('member/assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('member/assets/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{asset('member/assets/plugins/iCheck/icheck.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('member/assets/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('member/assets/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
{{--<script src="../../dist/js/demo.js"></script>--}}
<!-- Page script -->



<script src="{{asset('member/assets/dist/js/bootstrap-formhelpers.js')}}"></script>
<script src="{{asset('member/assets/dist/js/bootstrap-filestyle.js')}}"></script>
<script src="{{asset('member/assets/bootstrap/js/app.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('member/assets/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('member/assets/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<!-- CK Editor -->
{{--<script src="{{asset('member/assets/ckeditor/ckeditor.js')}}"></script>--}}
{{--<script src="{{asset('member/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>--}}




<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>
<script type="text/javascript">
    $('ul.pagination').hide();
    $(function () {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                //  jQuery("time.timeago").timeago();
            }
        });
    });



    $('#txtsearch').on('blur', function (e) {
        if(e.target.value){
            $('#form-set-country').submit();
        }
    });

    $('#country').on('change',function (e) {
        if(e.target.value){
            $('#form-set-country').submit();
        }

    });




</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 150,
            toolbar:[


                ['font',['bold','italic','underline','clear']],
                ['para',['ul','ol']],
                ['view',['fullscreen','codeview']],


//                ['style',['style']],
//                ['font',['bold','italic','underline','clear']],
//                ['fontname',['fontname']],
//                ['color',['color']],
//                ['para',['ul','ol','paragraph']],
//                ['height',['height']],
//                ['table',['table']],
//                ['insert',['media','link','hr']],
//                ['view',['fullscreen','codeview']],
//                ['help',['help']],
//                ['cleaner',['cleaner']], // The Button
            ],
            cleaner:{
                action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                newline: '<br>', // Summernote's default is to use '<p><br></p>'
                notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                icon: '<span class="note-icon-eraser"> Clear format</span>',
                keepHtml: false, // Remove all Html formats
                keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], // If keepHtml is true, remove all tags except these
                keepClasses: false, // Remove Classes
                badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                badAttributes: ['style', 'start'], // Remove attributes from remaining tags
                limitChars: false, // 0/false|# 0/false disables option
                limitDisplay: 'both', // text|html|both
                limitStop: false // true/false
            }
        });
    });

//    $(document).ready(function () {
//        $('#packageHighlight').summernote({
//            onPaste: function (e) {
//                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
//                e.preventDefault();
//                document.execCommand('insertText', false, bufferText);
//            }
//        });
//    });

    $('body').on('hidden.bs.modal', '.modal', function () {
        //alert('test');
        window.location.reload();
      //  $(this).removeData('bs.modal');
    });

//    $(document).ready(function () {
//        $('.modal').on('hidden.bs.modal',function () {
//            $(this).removeData();
//        });
//    });

//    //Flat red color scheme for iCheck
//    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//        checkboxClass: 'icheckbox_flat-green',
//        radioClass   : 'iradio_flat-green'
//    });




//
//$(function () {
//
//    $('#packageHighlight').wysihtml5({
//        toolbar: {
//            "font-styles": false, // Font styling, e.g. h1, h2, etc.
//            "emphasis": true, // Italics, bold, etc.
//            "lists": false, // (Un)ordered lists, e.g. Bullets, Numbers.
//            "html": false, // Button which allows you to edit the generated HTML.
//            "link": false, // Button to insert a link.
//            "image": false, // Button to insert an image.
//            "color": false, // Button to change color of font
//            "blockquote": false, // Blockquote
//        }
//    });
//});
//
//$(function () {
//    $('#condition_title').wysihtml5({
//        toolbar: {
//            "font-styles": false, // Font styling, e.g. h1, h2, etc.
//            "emphasis": true, // Italics, bold, etc.
//            "lists": false, // (Un)ordered lists, e.g. Bullets, Numbers.
//            "html": false, // Button which allows you to edit the generated HTML.
//            "link": false, // Button to insert a link.
//            "image": false, // Button to insert an image.
//            "color": false, // Button to change color of font
//            "blockquote": false, // Blockquote
//        }
//    });
//});

    $('#use_operator').on('click',function () {
        if(this.checked==true){
            $('.use-operator').show();
            $('.not-use-operator').hide();
            $('.deposit').hide();
            $('.visa').hide();
            $("#operator_code").prop('required',true);
            $("#condition_left").prop('required',true);
            $("#condition_unit").prop('required',true);
            $("#condition_title").prop('required',false);
        }else{
            $("#operator_code").prop('required',false);
            $("#condition_left").prop('required',false);
            $("#condition_unit").prop('required',false);
            $("#condition_title").prop('required',true);
            $('.use-operator').hide();
            $('.not-use-operator').show();
        }
    });

    function SP_source() {
        return "{{url('/')}}/";
    }

    $(function () {
//        $('#select-partner').hide();
        //Initialize Select2 Elements
        $('.select2').select2();
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
        //Money Euro
        $('[data-mask]').inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true

        });

        $('#datepicker1').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });

        //Colorpicker
        $('.my-colorpicker1').colorpicker();
        //color picker with addon
        $('.my-colorpicker2').colorpicker();

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    });

    $('#identification_id').on('blur',function () {
        elm=document.getElementById('identification_id');
        if(!check_IdCard(elm)){
           $('#inputError').text('  Please check Id!');
        }
       //alert(check);
    })

    function check_IdCard(elm){

       var idcard=elm.value;
        if(idcard.length>0){
            var id=idcard.replace(/\-/g,'');
            if(!checkID(id)){
//                alert('รหัสประชาชนไม่ถูกต้อง');
                elm.value='';
                elm.focus();
                return false;
            }else{
                return true;
            }
        }

    }

    $('.package-from').on('click',function () {
        if($(this).val()=='Yes'){
            $('#select-partner').hide();
            $("#owner_timeline_id").prop('required',false);
        }else{
            $('#select-partner').show();
            $("#owner_timeline_id").prop('required',true);
        }
    });



    function checkID(id)
    {
        if(id.length != 13) return false;
        for(i=0, sum=0; i < 12; i++)
            sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!=parseFloat(id.charAt(12)))
        return false; return true;
    }


    function confirm_delete() {
        if(confirm('Are you sure to delete this record?')){
            return true;
        }else{
            return false;
        }
    }


    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        content: function(){return '<img src="'+$(this).data('img') + '" />';}
    });
    $("#imgInp").change(function(){
        readURL(this);
    });


    $(function () {



        $('#example1').DataTable();
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    });



</script>
</body>
</html>
