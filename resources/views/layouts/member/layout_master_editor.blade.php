<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <meta name="csrf_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="{{asset('member/assets/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('member/assets/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('member/assets/Ionicons/css/ionicons.min.css')}}">

  <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/AdminLTE.min.css')}}">


    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {{--<link rel="stylesheet" href="{{asset('member/assets/dist/css/skins/skin-red.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('member/assets/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('member/css/flag-icon.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!-- include summernote -->
    <script src="{{asset('member/assets/bootstrap/3.3/js/jquery.js')}}"></script>
    <script src="{{asset('member/assets/bootstrap/3.3/js/popper.js')}}"></script>
    <script src="{{asset('member/assets/bootstrap/3.3/js/bootstrap.js')}}"></script>

    {{--<script src="{{asset('member/assets/bootstrap/4.1/js/jquery.js')}}"></script>--}}
    {{--<script src="{{asset('member/assets/bootstrap/4.1/js/bootstrap.js')}}"></script>--}}

    <link rel="stylesheet" href="{{asset('member/assets/summernote/summernote.css')}}">
    <script src="{{asset('member/assets/summernote/summernote.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300,
                tabsize: 2,
                followingToolbar: true,
            });
        });
    </script>

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
{{--<body class="hold-transition skin-blue sidebar-mini">--}}
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    @include('layouts.member.include.header')
    <!-- Left side column. contains the logo and sidebar -->
    @include('layouts.member.include.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<h1>--}}
                {{--Advanced Form Elements--}}
                {{--<small>Preview</small>--}}
            {{--</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Forms</a></li>--}}
                {{--<li class="active">Advanced Elements</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        @yield('pageHeader')
        <!-- Main content -->
        <section class="content">

            <div id="preload" style="display: none;" class="preload" align="center">
                <div id="loader-wrapper" class="loader"></div>
                <p style="color:#7B8BB3">Please wait for the results to come in......</p>
            </div>
            <!-- Info boxes -->
            @yield('content')
                    <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('layouts.member.include.footer')
        <!-- Modal -->
        <div class="modal modal-primary fade" id="popupForm" tabindex="-1" role="dialog" aria-labelledby="popupForm" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div id="modalDetails" class="modal-content">

                </div>
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal" id="modal-add-language" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">

                </div>
            </div>
        </div>


</div>












</body>
</html>
