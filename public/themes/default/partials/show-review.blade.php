<?php

$RateAll=DB::table('star_rating')
    ->where('timeline_id',$location->timeline_id)
    ->where('user_id',Auth::user()->id)
    ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
    ->first();

$Overall=0;$Overall2=0;$allUser=0;
if($RateAll){
    if($RateAll->star>0){
        $Overall=round($RateAll->star/$RateAll->userCount,1);
        $allUser=$RateAll->userCount;
    }
    //    dd($RateAll);
}
if($allUser>0){
    Session::put('allUser',$allUser);
}
$Group2=array();$Group1=array();
//$review_date=date('Y-m-d H:i:s');
for($i=1;$i<=8;$i++){
    $Rate=DB::table('star_rating')
        ->where('timeline_id',$location->timeline_id)
        ->where('user_id',Auth::user()->id)
        ->where('rate_group',$i)
        ->first();

    $review_date[$i]='0';
    $RateStar[$i]=0;
    $Star[$i]=0;
    $width[$i]=0;
    if($Rate){
        $RateStar[$Rate->rate_group]=$Rate->star_rating;
        $Star[$Rate->rate_group]=$Rate->star_rating;;
        $width[$Rate->rate_group]=$Rate->star_rating*20;
        $review_date[$Rate->rate_group]=$Rate->updated_at;

        if($i=='1'||$i=='6'||$i=='7'||$i=='8'){
            $Group1[]=$Star[$Rate->rate_group];
        }else if($i=='2'||$i=='3'||$i=='4'||$i=='5'){
            //  dd($i.'='.$Star[$i]);
            $Group2[]=$Star[$i];
        }
    }
}

if(count($Group1)){
    $Overall=round(array_sum($Group1)/count($Group1),1);
}
if(count($Group2)){

    $Overall2=round(array_sum($Group2)/count($Group2),1);

}
$Users=DB::table('star_rating')
    ->where('timeline_id',$location->timeline_id)
    ->groupby('user_id')
    ->get();

if($Users){
    Session::put('allUser',$Users->count());
}

?>
<link href="{{ Theme::asset()->url('css/review.css') }}" rel="stylesheet">
<style type="text/css">
    h5{font-size: 15px}
    h3{margin-top: 0}

</style>
<div class="panel panel-default panel-post animated">
    <div class="panel-heading no-bg">
        <div class="post-author">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-success"><i class="fa fa-edit"></i> {{trans('common.your_review_info')}}
                        @if($review_date[1]!='0')
                            <br><small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[1]))}} </small>
                        @endif
                    </h3>
                </div>

            </div>
            <hr>
            <div class="row">

                <div class="col-md-7">
                    <h3><i class="fa fa-location-arrow"></i> {{trans('common.location')}} <span class="label bg-blue pull-right">{{$Overall}}</span></h3>
                </div>
                <div class="col-md-3">
                    <ul class="rating-stars-big">
                        <li style="width:{{$Overall*20}}%" class="stars-active">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </li>
                        <li>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </li>
                    </ul>
                </div>

                    <div class="col-md-2 pull-right">
                        <div class="ScoreOutOfTen">
                            <div class="ReviewScore">
                                <i class="ficon ficon-review-icon ReviewScore-Icon"></i>
                                <span class="ReviewScore-Number" data-selenium="hotel-header-review-score">{{$Overall}}</span>

                            </div>
                            <div class="ScoreOutOfTen__OfTen">
                                <div class="ScoreOutOfTen__OfTen__Text">/</div>
                                <div class="ScoreOutOfTen__OfTen__Text">5</div>
                            </div>
                        </div>
                    </div>



            </div>

<hr>

<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/1')}}">
            <span>{{trans('common.over_all_rating')}}<span class="pull-right text-success"><strong>{{$Star[1]==0?'N/A':$Star[1]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[1]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[1]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[1]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[1]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
      <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[1]))}} </small>
    </div>
        @endif
</div>
<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/6')}}">
            <span>{{trans('common.cleanliness')}}<span class="pull-right text-success"><strong>{{$Star[6]==0?'N/A':$Star[6]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[6]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[6]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[6]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[6]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
      <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[6]))}} </small>
    </div>
        @endif
</div>
<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/7')}}">
            <span>{{trans('common.normal_parking')}}<span class="pull-right text-success"><strong>{{$Star[7]==0?'N/A':$Star[7]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[7]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[7]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[7]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[7]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
      <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[7]))}} </small>
    </div>
        @endif
</div>
<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/8')}}">
            <span>{{trans('common.normal_toilet')}}<span class="pull-right text-success"><strong>{{$Star[8]==0?'N/A':$Star[8]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[8]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[8]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[8]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[8]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
      <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[8]))}} </small>
    </div>
        @endif
</div>
            <hr>
<div class="row">
    <div class="col-md-7">
        <h3><i class="fa fa-wheelchair-alt"></i> {{trans('common.disabled_facilities')}} </h3>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap" >

            <ul class="rating-stars-big">
                <li style="width:{{$Overall2*20}}%" class="stars-active">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </li>
                <li>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </li>
            </ul>

        </div>
    </div>

    <div class="col-md-2 pull-right">
        <div class="ScoreOutOfTen">
            <div class="ReviewScore">
                <i class="ficon ficon-review-icon ReviewScore-Icon"></i>
                <span class="ReviewScore-Number" data-selenium="hotel-header-review-score">{{$Overall2}}</span>
            </div>
            <div class="ScoreOutOfTen__OfTen">
                <div class="ScoreOutOfTen__OfTen__Text">/</div>
                <div class="ScoreOutOfTen__OfTen__Text">5</div>
            </div>
        </div>
    </div>

</div>
<hr>

<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/2')}}">
            <span>{{trans('common.inside_rating')}} <span class="pull-right text-success"><strong>{{$Star[2]==0?'N/A':$Star[2]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[2]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[2]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[2]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[2]!='0')

    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
        <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[2]))}} </small>
    </div>
        @endif
</div>

<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/3')}}">
            <span>{{trans('common.outside_rating')}} <span class="pull-right text-success"><strong>{{$Star[3]==0?'N/A':$Star[3]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[3]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>



        </div>

    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[3]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[3]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[3]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
        <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[3]))}} </small>
    </div>
        @endif

</div>
<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/4')}}">
            <span>{{trans('common.toilet_rating')}} <span class="pull-right text-success"><strong>{{$Star[4]==0?'N/A':$Star[4]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[4]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>



        </div>

    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[4]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[4]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[4]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
        <small><i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[4]))}} </small>
    </div>
        @endif

</div>
<div class="row">
    <div class="col-md-4">
        <a href="{{url($timeline->username.'/review/5')}}">
            <span>{{trans('common.parking_rating')}} <span class="pull-right text-success"><strong>{{$Star[5]==0?'N/A':$Star[5]}}</strong></span></span>
        </a>
    </div>
    <div class="col-md-3">
        <div class="rating-wrap">

                <ul class="rating-stars">
                    <li style="width:{{$width[5]}}%" class="stars-active">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </li>
                </ul>

        </div>

    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[5]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[5]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
    @if($review_date[5]!='0')
    <div class="col-md-12" style="margin-top: -15px; color:#9c9c9c">
        <small>   <i class="fa fa-calendar"></i> {{trans('common.your_review_last_date')}} : {{date('F d, Y H:i:s',strtotime($review_date[5]))}} </small>
    </div>
    @endif
</div>
</div>
</div>
</div>

