<div class="user-profile-buttons">
	<div class="row locationlike-links">
		@if($location->is_admin(Auth::user()->id))
			@if(!$location->likes->contains(Auth::user()->id))								
				<div class="col-md-6 col-sm-6 col-xs-6 left-col"><a href="#" class="btn btn-options btn-block btn-default location-like like" data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-6 col-sm-6 col-xs-6 left-col hidden"><a href="#" class="btn btn-options btn-block btn-success location-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@else
				<div class="col-md-6 col-sm-6 col-xs-6 left-col hidden"><a href="#" class="btn btn-options btn-block btn-default location-like like " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-6 col-sm-6 col-xs-6 left-col"><a href="#" class="btn btn-options btn-block btn-success location-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@endif
			<div class="col-md-6 col-sm-6 col-xs-6 right-col">
				<a href="{{ url('/'.$timeline->username.'/location-settings/general') }}" class="btn btn-options btn-block btn-default"><i class="fa fa-gear"></i>
					{{ trans('common.settings') }}
				</a>
			</div>
		@else
			@if(!$location->likes->contains(Auth::user()->id))								
				<div class="col-md-12 col-sm-12 col-xs-12  location"><a href="#" class="btn btn-options btn-block btn-default location-like like" data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-12 col-sm-12 col-xs-12  location hidden"><a href="#" class="btn btn-options btn-block btn-success location-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@else
				<div class="col-md-12 col-sm-12 col-xs-12  location hidden"><a href="#" class="btn btn-options btn-block btn-default location-like like " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-12 col-sm-12 col-xs-12  location"><a href="#" class="btn btn-options btn-block btn-success location-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@endif			
			<div class="col-md-6 col-sm-6 col-xs-6 left-col location hidden">
				<a href="{{ url('/'.Auth::user()->username.'/location-settings/general') }}" class="btn btn-options btn-block btn-default">
					<i class="fa fa-inbox"></i> {{ trans('common.messages') }}
				</a>
			</div>
		@endif
		</div>
	</div>

	<div class="user-bio-block">
		<div class="bio-header">{{ trans('common.star_rating') }}</div>
		<div class="bio-description">
			@include('star.show-star')
		</div>

		<div class="col-md-6">
			@if(Session::get('allUser')>0)
			{{trans('common.from')}} {{Session::get('allUser')}} {{trans('common.review')}}
			@endif
		</div>
		<div class="col-md-6 text-right">
			<i class="fa fa-edit"></i> <a href="{{url($timeline->username.'/review/1')}}">{{trans('common.click_to_review')}}</a>
		</div>
		<hr>
	</div>

	<!-- Change avatar form -->
	<form class="change-avatar-form hidden" action="{{ url('ajax/change-avatar') }}" method="post" enctype="multipart/form-data">
		<input name="timeline_id" value="{{ $timeline->id }}" type="hidden">
		<input name="timeline_type" value="{{ $timeline->type }}" type="hidden">
		<input class="change-avatar-input hidden" accept="image/jpeg,image/png" type="file" name="change_avatar" >
	</form>

	<!-- Change cover form -->
	<form class="change-cover-form hidden" action="{{ url('ajax/change-cover') }}" method="post" enctype="multipart/form-data">
		<input name="timeline_id" value="{{ $timeline->id }}" type="hidden">
		<input name="timeline_type" value="{{ $timeline->type }}" type="hidden">
		<input class="change-cover-input hidden" accept="image/jpeg,image/png" type="file" name="change_cover" >
	</form>

	<div class="user-bio-block">
		<div class="bio-header">{{ trans('common.about')}}</div>
		<div class="bio-description">
			{{ ($timeline['about'] != NULL) ? $timeline['about'] : trans('messages.no_description') }}
		</div>
		<ul class="bio-list list-unstyled">
			<li>
				<i class="fa fa-folder-o" aria-hidden="true"></i>
				<span>
					<?php
                    $category=\App\Category::where('category_id',$location->category_id)->active()->first();
                    $sub1category=$category->subcategory()->where('category_sub1_id',$location->category_sub1_id)->first();

					?>
					@if($location->category_sub2_id>0)
						{{ $sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)->active()->first()->category_sub2_name  }}
					@elseif($location->category_sub1_id>0)
						{{ $category->subcategory()->where('category_sub1_id',$location->category_sub1_id)->first()->sub1_name }}
					@else
						{{ \App\Category::where('category_id',$location->category_id)->active()->first()->name  }}
					@endif
					</span>
			</li>
			<li>
			@if($location->address != null)
				<i class="fa fa-map-marker" aria-hidden="true"></i><span>{{ $location->address }}</span>
			@else
				<i class="fa fa-map-marker" aria-hidden="true"></i>
			@endif

				<span>
					@if($city)
						{{ $city->city }}
					@endif
					@if($state)
						{{$state->state}}
					@endif
					@if($country)
						{{$country->country}}
					@endif
				</span>
			</li>
			@if($timeline->longitude)
			<li>
				<a href="http://www.google.com/maps/place/{{$timeline->latitude}},{{$timeline->longitude}}" target="_blank">
				<i class="fa fa-map" aria-hidden="true"></i><span> {{trans('common.map')}}</span>
				</a>
			</li>
			@endif


			@if($location->website != null)
				<li>
					<i class="fa fa-globe" aria-hidden="true"></i><span>{{ $location->website }}</span>
				</li>
			@endif

			@if($location->phone != null)
				<li>
					<i class="fa fa-phone" aria-hidden="true"></i><span>{{ $location->phone }}</span>
				</li>
			@endif
		</ul>
	</div>

	<div class="widget-pictures widget-best-pictures"><!-- /pages-liked -->
		<div class="picture side-left">
			{{ trans('common.members') }}
		</div>
		@if(count($location_members) > 0)
			<div class="side-right show-all">
				<a href="{{ url($timeline->username.'/location-members') }}">{{ trans('common.show_all') }}</a>
			</div>
		@endif
		<div class="clearfix"></div>
		<div class="best-pictures my-best-pictures">
			<div class="row">
				@if(count($location_members) > 0)
					@foreach($location_members->take(12) as $location_member)
					<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
						<a href="{{ url($location_member->username) }}" class="image-hover" data-toggle="tooltip" data-placement="top" title="{{ $location_member->name }}">
							<img src="{{ $location_member->avatar }}" alt="{{ $location_member->name }}" title="{{ $location_member->name }}">
						</a>
					</div>
					@endforeach
				@else
					<div class="alert alert-warning">{{ trans('messages.no_members') }}</div>
				@endif
			</div>
		</div>
	</div> <!-- /pages-liked -->

