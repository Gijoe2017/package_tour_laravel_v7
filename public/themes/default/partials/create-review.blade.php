<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{asset('member/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">

<style type="text/css">
    .star-rating-g1 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g2 {
        line-height:32px;
        font-size:1em;
    }
    .star-rating-g3 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g4 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g5 {
        line-height:32px;
        font-size:1em;
    }
    .star-rating-g6 {
        line-height:32px;
        font-size:1em;
    }
    .star-rating-g7 {
        line-height:32px;
        font-size:1em;
    }
    .star-rating-g8 {
        line-height:32px;
        font-size:1em;
    }

    .form-control{
        border: 1px solid ghostwhite;
    }

    .star-rating-g1 .fa-star{color: #f99e00;}
    .star-rating-g2 .fa-star{color: #f99e00;}
    .star-rating-g3 .fa-star{color: #f99e00;}
    .star-rating-g4 .fa-star{color: #f99e00;}
    .star-rating-g5 .fa-star{color: #f99e00;}
    .star-rating-g6 .fa-star{color: #f99e00;}
    .star-rating-g7 .fa-star{color: #f99e00;}
    .star-rating-g8 .fa-star{color: #f99e00;}

</style>
<form action="{{ url('') }}" method="post" class="create-review-form">
  {{ csrf_field() }}
    <?php
    $star_rating=0;
    $Rate=DB::table('star_rating')
        ->where('timeline_id',$location->timeline_id)
        ->where('user_id',Auth::user()->id)
        ->where('rate_group',$rate_group)
        ->first();
    if($Rate){
        $star_rating=$Rate->star_rating;
    }
    ?>
    <input type="hidden" id="star_rating" name="star_rating" value="{{$star_rating}}">
    <input type="hidden" id="timeline_id" name="timeline_id" value="{{$location->timeline_id}}">
    <input type="hidden" name="rate_group" value="{{$rate_group}}">

    <div class="panel panel-default panel-post panel-create"> <!-- panel-create -->
        <div class="panel-heading">
            <div class="row">
            <div class="col-md-5">
                <div class="heading-text">
                    @if($timeline->type=='user')
                        {{ trans('messages.review') }}
                    @else
                        {{ trans('common.review').' '.$rate_group_title }}
                    @endif
                </div>
            </div>
            <div class="col-md-4">{{trans('common.your_point_review')}}</div>
            <div class="col-md-3">
                <div class="star-rating-g{{$rate_group}}" style="margin-top: -5px">
                    <span class="fa fa-star-o" data-rating="1"></span>
                    <span class="fa fa-star-o" data-rating="2"></span>
                    <span class="fa fa-star-o" data-rating="3"></span>
                    <span class="fa fa-star-o" data-rating="4"></span>
                    <span class="fa fa-star-o" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="{{$star_rating}}">
                </div>
            </div>
                <div class="col-md-6">
                    <label>Data visited this location..</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group-sm">
                        <input class="form-control" data-date-format="yyyy-mm-dd" type="text" id="datepicker" name="date_visited_location"  placeholder="Data visited this location.." autocomplete="off">
                    </div>
                </div>
            </div>
        </div>



        <div class="panel-body">
                <textarea name="description" class="form-control createpost-form comment" cols="30" rows="3" id="createPost" cols="30" rows="2" placeholder="{{ trans('messages.post-placeholder') }}"></textarea>
                <div class="user-tags-added" style="display:none">
                    &nbsp; -- {{ trans('common.with') }}
                    <div class="user-tag-names"></div>
                </div>
                <div class="user-tags-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-user-plus"></i></span>
                    <div class="form-group">
                        <input type="text" id="userTags" class="form-control user-tags youtube-text" placeholder="{{ trans('messages.who_are_you_with') }}" autocomplete="off" value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="users-results-wrapper"></div>
                <div class="youtube-iframe"></div>
        
                <div class="video-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-film"></i></span>
                    <div class="form-group">
                        <input type="text" name="youtubeText" id="youtubeText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_watching') }}"  value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
              @if((env('SOUNDCLOUD_CLIENT_ID') != "" || (env('SOUNDCLOUD_CLIENT_ID') != null)))
                <div class="music-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-music" aria-hidden="true"></i></span>
                   <div class="form-group">
                      <input type="text" name="soundCloudText" autocomplete="off" id ="soundCloudText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_listening_to') }}"  value="" >
                      <div class="clearfix"></div>
                   </div>
                </div>
                <div class="soundcloud-results-wrapper"></div>
              @endif
              
            <div class="location-addon post-addon" style="display: none">
                  <span class="post-addon-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                 <div class="form-group">
                    <input type="text" name="location" id="pac-input" class="form-control" placeholder="{{ trans('messages.where_are_you') }}"  autocomplete="off" value="" onKeyPress="return initMap(event)"><div class="clearfix"></div>
                 </div>                 
            </div>
              <div class="emoticons-wrapper  post-addon" style="display:none">
                  
              </div>
              <div class="images-selected post-images-selected" style="display:none">
                  <span>3</span> {{ trans('common.photo_s_selected') }}
              </div>
              <div class="images-selected post-video-selected" style="display:none">
                  <span>3</span>
              </div>
              <!-- Hidden elements  -->
              <input type="hidden" name="timeline_id" value="{{ $timeline->id }}">
              <input type="hidden" name="youtube_title" value="">
              <input type="hidden" name="youtube_video_id" value="">
              <input type="hidden" name="locatio" value="">
              <input type="hidden" name="soundcloud_id" value="">
              <input type="hidden" name="user_tags" value="">
              <input type="hidden" name="soundcloud_title" value="">
              <input type="file"   class="post-images-upload hidden" multiple="multiple"  accept="image/jpeg,image/png,image/gif" name="post_images_upload[]" id="post_images_upload[]">
              <input type="file" class="post-video-upload hidden"  accept="video/mp4" name="post_video_upload" >
              <div id="post-image-holder"></div>
        </div><!-- panel-body -->
        <div class="panel-footer">
            <ul class="list-inline left-list">
                <li><a href="#" id="addUserTags"><i class="fa fa-user-plus"></i></a></li>
                <li><a href="#" id="imageUpload"><i class="fa fa-camera-retro"></i></a></li>
                {{-- <li><a href="#" id="selfVideoUpload"><i class="fa fa-film"></i></a></li> --}}
                @if((env('SOUNDCLOUD_CLIENT_ID') != "" || (env('SOUNDCLOUD_CLIENT_ID') != null)))
                  <li><a href="#" id="musicUpload"><i class="fa fa-music"></i></a></li>
                @endif
                <li><a href="#" id="videoUpload"><i class="fa fa-youtube"></i></a></li>
                <li><a href="#" id="locationUpload"><i class="fa fa-map-marker"></i></a></li>
                <li><a href="#" id="emoticons"><i class="fa fa-smile-o"></i></a></li>
            </ul>
            <ul class="list-inline right-list">
                @if($user_post == 'group' && Auth::user()->is_groupAdmin(Auth::user()->id, $timeline->groups->id) || $user_post == 'group' && $timeline->groups->event_privacy == 'members' && Auth::user()->is_groupMember(Auth::user()->id, $timeline->groups->id))                 
                  <li><a href="{!! url($username.'/groupevent/'.$timeline->groups->id) !!}" class="btn btn-default">{{ trans('common.create_event') }}</a></li>
                @endif
                <li><button type="submit" class="btn btn-submit btn-success">{{ trans('common.review') }}</button></li>
            </ul>

            <div class="clearfix"></div>
        </div>

    </div>
</form>


@if(Setting::get('postcontent_ad') != NULL)
    <div id="link_other" class="page-image">
        {!! htmlspecialchars_decode(Setting::get('postcontent_ad')) !!}
    </div>
@endif
<!-- jQuery 3 -->

    <!-- bootstrap datepicker -->
    <script src="{{asset('member/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>


    <script language="javascript">

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
    var $star_rating1 = $('.star-rating-g1 .fa');
    var $star_rating2 = $('.star-rating-g2 .fa');
    var $star_rating3 = $('.star-rating-g3 .fa');
    var $star_rating4 = $('.star-rating-g4 .fa');
    var $star_rating5 = $('.star-rating-g5 .fa');
    var $star_rating6 = $('.star-rating-g6 .fa');
    var $star_rating7 = $('.star-rating-g7 .fa');
    var $star_rating8 = $('.star-rating-g8 .fa');
    var timeline_id=$('#timeline_id').val();

    var SetRatingStar1 = function() {
        return $star_rating1.each(function() {
            if (parseInt($star_rating1.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating1.on('click', function() {
        $star_rating1.siblings('input.rating-value').val($(this).data('rating'));
//            alert($(this).data('rating'));
        SaveRatings(1,$(this).data('rating'),timeline_id);
        return SetRatingStar1();
    });

    var SetRatingStar2 = function() {
        return $star_rating2.each(function() {
            if (parseInt($star_rating2.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {

                return $(this).removeClass('fa-star-o').addClass('fa-star');

            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating2.on('click', function() {
        $star_rating2.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(2,$(this).data('rating'),timeline_id);
        return SetRatingStar2();
    });

    var SetRatingStar3 = function() {
        return $star_rating3.each(function() {
            if (parseInt($star_rating3.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {

                return $(this).removeClass('fa-star-o').addClass('fa-star');

            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating3.on('click', function() {
        $star_rating3.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(3,$(this).data('rating'),timeline_id);
        return SetRatingStar3();
    });


    var SetRatingStar4 = function() {
        return $star_rating4.each(function() {
            if (parseInt($star_rating4.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating4.on('click', function() {
        $star_rating4.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(4,$(this).data('rating'),timeline_id);
        return SetRatingStar4();
    });

    var SetRatingStar5 = function() {
        return $star_rating5.each(function() {
            if (parseInt($star_rating5.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating5.on('click', function() {
        $star_rating5.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(5,$(this).data('rating'),timeline_id);
        return SetRatingStar5();
    });

        var SetRatingStar6 = function() {
            return $star_rating6.each(function() {
                if (parseInt($star_rating6.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        };

        $star_rating6.on('click', function() {
            $star_rating6.siblings('input.rating-value').val($(this).data('rating'));
            SaveRatings(6,$(this).data('rating'),timeline_id);
            return SetRatingStar6();
        });


        var SetRatingStar7 = function() {
            return $star_rating7.each(function() {
                if (parseInt($star_rating7.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        };

        $star_rating7.on('click', function() {
            $star_rating7.siblings('input.rating-value').val($(this).data('rating'));
            SaveRatings(7,$(this).data('rating'),timeline_id);
            return SetRatingStar7();
        });

        var SetRatingStar8 = function() {
            return $star_rating8.each(function() {
                if (parseInt($star_rating8.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        };

        $star_rating8.on('click', function() {
            $star_rating8.siblings('input.rating-value').val($(this).data('rating'));
            SaveRatings(8,$(this).data('rating'),timeline_id);
            return SetRatingStar8();
        });



        SetRatingStar1();
    SetRatingStar2();
    SetRatingStar3();
    SetRatingStar4();
    SetRatingStar5();
    SetRatingStar6();
    SetRatingStar7();
    SetRatingStar8();


    function SaveRatings(group,id,timeline_id){
//            alert(timeline_id+' '+id);
        $.confirm({
            title: 'Confirm!',
            content: 'Thank you for review your point is number '+id+' ?',
            confirmButton: 'OK',
            cancelButton: 'No',
            confirmButtonClass: 'btn-primary',
            cancelButtonClass: 'btn-danger',
            confirm: function(){
                $('#star_rating').val(id);
                $.ajax({
                    type:'get',
                    url:SP_source() + 'ajax/star_rating/data',
                    data:{'id':id,'group':group,'timeline_id':timeline_id},
                    success:function(data){
                        postPanel.addClass('fadeOut');
                        setTimeout(function(){
                            postPanel.remove();
                        },800);
                        notify('You have successfully','warning');
                    }
                });
            },
            cancel: function(){

            }
        });

    }
    $(document).ready(function() {

    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_vuWi_hzMDDeenNYwaNAj0PHzzS2GAx8&libraries=places&callback=initMap"
        async defer></script>

<script>
function initMap(event) 
{    
    var key;  
    var map = new google.maps.Map(document.getElementById('pac-input'), {
    });

    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));        

    if(window.event)
    {
        key = window.event.keyCode; 

    }
    else 
    {
        if(event)
            key = event.which;      
    }       

    if(key == 13){       
    //do nothing 
    return false;       
    //otherwise 
    } else { 
        var autocomplete = new google.maps.places.Autocomplete(input);  
        autocomplete.bindTo('bounds', map);

    //continue as normal (allow the key press for keys other than "enter") 
    return true; 
    } 
}
</script>




