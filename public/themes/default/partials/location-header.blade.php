
<div class="timeline-cover-section">
	<div class="timeline-cover">
		<a href="{{ url($timeline->username) }}">
		<img src=" @if($timeline->cover_id) {{ url('location/cover/'.$timeline->cover->source) }} @else {{ url('location/cover/default-cover-location.png') }} @endif" alt="{{ $timeline->name }}" title="{{ $timeline->name }}">
		</a>
		@if($timeline->locations->is_admin(Auth::user()->id) == true)
			<a href="#" class="btn btn-camera-cover change-cover"><i class="fa fa-camera" aria-hidden="true"></i><span class="change-cover-text">{{ trans('common.change_cover') }}</span></a>
		@endif
		<div class="user-cover-progress hidden">
			
		</div>
		<div class="user-timeline-name">		
			<a href="{{ url($timeline->username) }}">{{ $timeline->name }}</a>
			{!! verifiedBadge($timeline) !!}
		</div>
		
	</div>
	<div class="timeline-list">
		<ul class="list-inline locaionlike-links">

			@if(Auth::user()->get_location($location->id) != NULL)
				@if(($location->member_privacy == "only_admins" && $location->is_admin(Auth::user()->id)) || ($location->member_privacy == "members" && Auth::user()->get_location($location->id)->pivot->active == 1))
				<li class="{{ Request::segment(2) == 'add-location-members' ? 'active' : '' }}">
					<a href="{{ url($timeline->username.'/add-location-members')}}" ><span class="top-list"> {{ trans('common.addmembers') }}</span></a>
				</li>
				@endif
			@endif

			<?php
				$Role=0;
				$Role=DB::table('role_user')->where('user_id',Auth::user()->id)->first();
			?>

				{{--@if($Role->role_id!=3)--}}
				{{--<li>--}}
					{{--<a href="{{ url('member/list/'.$location->timeline_id)}}">--}}
					{{--<span class="top-list">--}}
						{{--{{ trans('common.tour_management')}}--}}
					{{--</span>--}}
					{{--</a>--}}
				{{--</li>--}}
			{{--@endif--}}
				@if($location->is_admin(Auth::user()->id))
					<li>
						<a href="{{ url('member/dashboard/'.$location->timeline_id)}}">
						<span class="top-list">
							 {{ trans('common.tour_management') }}
						</span>
						</a>
					</li>
				@endif

			<li class="{{ Request::segment(2) == 'location-members' ? 'active' : '' }}"><a href="{{ url($timeline->username.'/location-members/')}}">
				<span class="top-list">
					{{ $location->members() != false ? count($location->members()) : 0 }} {{ trans('common.members') }}
				</span>
				</a>
			</li>
		
		<li class="{{ Request::segment(2) == 'location-admin' ? 'active' : '' }}">
			<a href="{{ url($timeline->username.'/location-admin/') }}">
				<span class="top-list">
					{{ $location->admins() != false ? count($location->admins()) : 0 }} {{ trans('common.admins') }}
				</span>
			</a>
		</li>	

		<li class="{{ Request::segment(2) == 'location-likes' ? 'active' : '' }}">
			<a href="{{ url($timeline->username.'/location-likes') }}">
				<span class="top-list">
					{{ $location->likes()->count() }} {{ trans('common.people_like_this') }}
				</span>
			</a>
		</li>

		<li class="{{ Request::segment(2) == 'location-posts' ? 'active' : '' }}"><a href="{{ url($timeline->username.'/location-posts') }}"><span class="top-list">{{ count($timeline->posts()->where('active', 1)->get()) }} {{ trans('common.posts') }}</span></a></li>
		@if(!$location->is_admin(Auth::user()->id))
		<li class="dropdown largescreen-report"><a href="#" class=" dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="top-list"> <i class="fa fa-ellipsis-h"></i></span></a>
			<ul class="dropdown-menu  report-dropdown">
				
				<li class="">
					<a href="#" class="save-timeline" data-timeline-id="{{ $timeline->id }}">
						<span class="top-list"><i class="fa fa-save"></i>
						@if($timeline->usersSaved()->where('user_id',Auth::user()->id)->get()->isEmpty())
							{{ trans('common.save') }}
						@else
							{{ trans('common.unsave') }}
						@endif
						</span>
					</a>
				</li>
				
					@if(!$timeline->reports->contains(Auth::user()->id))
						<li class=""><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.report') }}</a></li>
						
						<li class="hidden "><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.reported') }}</a></li>
					@else
						<li class="hidden "><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.report') }}</a></li>
						
						<li class=""><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.reported') }}</a></li>
					@endif
					
					@if(!$timeline->reports->contains(Auth::user()->id))
						<li class="smallscreen-report"><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}">{{ trans('common.report') }}</a></li>
						<li class="hidden smallscreen-report"><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}">{{ trans('common.reported') }}</a></li>
					@else
						<li class="hidden smallscreen-report"><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}">{{ trans('common.report') }}</a></li>
						<li class="smallscreen-report"><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}">{{ trans('common.reported') }}</a></li>
					@endif
				
			</ul>
		</li>
		@endif
			
	</ul>
	<div class="status-button">
		<a href="#" class="btn btn-status">{{ trans('common.status') }}</a>
	</div>

	<div class="timeline-user-avtar">
		<a href="{{ url($timeline->username) }}">
		<img src=" @if($timeline->avatar_id) {{ url('location/avatar/small/'.$timeline->avatar->source) }} @else {{ url('location/avatar/default-location-avatar.png') }} @endif" alt="{{ $timeline->name }}" title="{{ $timeline->name }}" alt="{{ $timeline->name }}">
		</a>

			@if($timeline->locations->is_admin(Auth::user()->id) == true)
			<div class="chang-user-avatar">
				<a href="#" class="btn btn-camera change-avatar"><i class="fa fa-camera" aria-hidden="true"></i><span class="avatar-text">{{ trans('common.update_profile') }}<span>{{ trans('common.picture') }}</span></span></a>
			</div>	
		@endif	
		<div class="user-avatar-progress hidden"></div>
	</div>
</div>
</div>
<script type="text/javascript">
	@if($timeline->background_id != NULL)
		$('body')
		.css('background-image', "url({{ url('/wallpaper/'.$timeline->wallpaper->source) }})")
		.css('background-attachment', 'fixed');

	@endif
</script>