<style type="text/css">
	.navbar-default .navbar-toggle{
		border-color:white;
	}
	.navbar-toggle{
		margin-right:0;
	}
</style>
<nav class="navbar socialite navbar-default no-bg guest-nav">
	<div class="container-fluid">

		{{--<nav class="navbar socialite navbar-default no-bg">--}}
			{{--<div class="container-fluid">--}}
		<!-- Brand and toggle get grouped for better mobile display -->
		{{--<div class="navbar-header">--}}

			{{--<a class="navbar-brand socialite" href="{{ url('/') }}">--}}
				{{--<img class="socialite-logo" src="{!! url('setting/'.Setting::get('logo')) !!}" alt="{{ Setting::get('site_name') }}" title="{{ Setting::get('site_name') }}">--}}
			{{--</a>--}}
		{{--</div>--}}


		<div class="navbar-header">
			<a href="{{url('/register')}}" class="navbar-toggle collapsed" >
				<i class="fa fa-unlock-alt"></i> {{trans('common.signin')}}
			</a>
			<a class="navbar-brand socialite" href="{{ url('/') }}">
				<img class="socialite-logo" src="{!! url('setting/'.Setting::get('logo')) !!}" alt="{{ Setting::get('site_name') }}" title="{{ Setting::get('site_name') }}">
			</a>
			<div class="navbar-toggle">
				<ul class="list-inline notification-list">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle no-padding" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="user-name">
								<?php $key=\App::getLocale()?>
								@if($key == 'en')
									<span class="flag-icon flag-icon-us"></span>
								@elseif($key == 'iw')
									<span class="flag-icon flag-icon-il"></span>
								@elseif($key == 'ja')
									<span class="flag-icon flag-icon-jp"></span>
								@elseif($key == 'zh')
									<span class="flag-icon flag-icon-cn"></span>
								@elseif($key == 'hi')
									<span class="flag-icon flag-icon-in"></span>
								@elseif($key == 'fa')
									<span class="flag-icon flag-icon-ir"></span>
								@else
									<span class="flag-icon flag-icon-{{ $key }}"></span>
								@endif

							</span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul class="dropdown-menu">
						@foreach( Config::get('app.locales') as $key => $value)
							<li class="">
								<a href="#" class="switch-language" data-language="{{ $key }}">
									@if($key == 'en')
										<span class="flag-icon flag-icon-us"></span>
									@elseif($key == 'iw')
										<span class="flag-icon flag-icon-il"></span>
									@elseif($key == 'ja')
										<span class="flag-icon flag-icon-jp"></span>
									@elseif($key == 'zh')
										<span class="flag-icon flag-icon-cn"></span>
									@elseif($key == 'hi')
										<span class="flag-icon flag-icon-in"></span>
									@elseif($key == 'fa')
										<span class="flag-icon flag-icon-ir"></span>
									@else
										<span class="flag-icon flag-icon-{{ $key }}"></span>
									@endif
									{{ $value }}</a>
							</li>
						@endforeach
					</ul>
				</li>

			</ul>
			</div>

				{{--<button type="button" class="navbar-toggle collapsed"  >--}}
					{{--<i class="fa fa-map-marker"></i> {{trans('common.tripable')}}--}}
				{{--</button>--}}



		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right" id="navbar-right">
				<li>
					<ul class="list-inline notification-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle no-padding" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="user-name">
                                    <?php $key = Session::get('language') ?>
								@if($key == 'en')
									<span class="flag-icon flag-icon-us"></span>
								@elseif($key == 'iw')
									<span class="flag-icon flag-icon-il"></span>
								@elseif($key == 'ja')
									<span class="flag-icon flag-icon-jp"></span>
								@elseif($key == 'zh')
									<span class="flag-icon flag-icon-cn"></span>
								@elseif($key == 'hi')
									<span class="flag-icon flag-icon-in"></span>
								@elseif($key == 'fa')
									<span class="flag-icon flag-icon-ir"></span>
								@else
									<span class="flag-icon flag-icon-{{ $key }}"></span>
								@endif

							</span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
							<ul class="dropdown-menu">
								@foreach( Config::get('app.locales') as $key => $value)
									<li class=""><a href="#" class="switch-language-guest" data-language="{{ $key }}">
											@if($key == 'en')
												<span class="flag-icon flag-icon-us"></span>
											@elseif($key == 'iw')
												<span class="flag-icon flag-icon-il"></span>
											@elseif($key == 'ja')
												<span class="flag-icon flag-icon-jp"></span>
											@elseif($key == 'zh')
												<span class="flag-icon flag-icon-cn"></span>
											@elseif($key == 'hi')
												<span class="flag-icon flag-icon-in"></span>
											@elseif($key == 'fa')
												<span class="flag-icon flag-icon-ir"></span>
											@else
												<span class="flag-icon flag-icon-{{ $key }}"></span>
											@endif

											{{ $value }}</a></li>
								@endforeach
							</ul>
						</li>
						<li class="dropdown message notification">
							<a href="{{url('/register')}}"  role="button" aria-haspopup="true" aria-expanded="false">
								Sign in
								<span class="small-screen">{{ trans('common.notifications') }}</span>
							</a>

						</li>

						<li class="smallscreen-message">
							<a href="{{ url('messages') }}">
								<i class="fa fa-comments" aria-hidden="true">
									<span class="count" v-if="unreadConversations" >@{{ unreadConversations }}</span>
								</i>
								<span class="small-screen">{{ trans('common.messages') }}</span>
							</a>
						</li>
						{{--<li class="chat-list-toggle">--}}
							{{--<a href="#"><i class="fa fa-users" aria-hidden="true"></i><span class="small-screen">chat-list</span></a>--}}
						{{--</li>--}}
					</ul>
				</li>


			</ul>
			</div>
		</div>



		
	</div><!-- /.container-fluid -->
</nav>	
