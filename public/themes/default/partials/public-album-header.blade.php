<div class="timeline-cover-section">
	<div class="timeline-cover">
		<a href="{{ url($timeline->username) }}">
		<img src=" @if($timeline->cover_id) {{ url('public_album/cover/'.$timeline->cover->source) }} @else {{ url('public_album/cover/default-cover-album.png') }} @endif" alt="{{ $timeline->name }}" title="{{ $timeline->name }}">
		</a>
		@if($timeline->public_album->is_admin(Auth::user()->id) == true)
			<a href="#" class="btn btn-camera-cover change-cover"><i class="fa fa-camera" aria-hidden="true"></i><span class="change-cover-text">{{ trans('common.change_cover') }}</span></a>
		@endif
		<div class="user-cover-progress hidden">
			
		</div>
		<div class="user-timeline-name">		
			<a href="{{ url($timeline->username) }}">{{ $timeline->name }}</a>
			{!! verifiedBadge($timeline) !!}
		</div>
		
	</div>
	<div class="timeline-list">
		<ul class="list-inline public-album-like-links">

			@if(Auth::user()->get_public_album($public_album->id) != NULL)
			@if(($public_album->member_privacy == "only_admins" && $public_album->is_admin(Auth::user()->id)) || ($public_album->member_privacy == "members" && Auth::user()->get_public_album($public_album->id)->pivot->active == 1))
			<li class="{{ Request::segment(2) == 'add-public-album-members' ? 'active' : '' }}">
				<a href="{{ url($timeline->username.'/add-public-album-members')}}" ><span class="top-list"> {{ trans('common.addmembers') }}</span></a>
			</li>	
			@endif

			@endif

				@if($public_album->is_admin(Auth::user()->id))
					<li>
						<a href="{{ url('member/manage/'.$public_album->timeline_id)}}">
						<span class="top-list">
							 {{ trans('common.tour_management') }}
						</span>
						</a>
					</li>
				@endif

			<li class="{{ Request::segment(2) == 'public-album-members' ? 'active' : '' }}"><a href="{{ url($timeline->username.'/public-album-members/')}}">
				<span class="top-list">
					{{ $public_album->members() != false ? count($public_album->members()) : 0 }} {{ trans('common.members') }}
				</span>
			</a>
		</li>
		
		<li class="{{ Request::segment(2) == 'public-album-admin' ? 'active' : '' }}">
			<a href="{{ url($timeline->username.'/public-album-admin/') }}">
				<span class="top-list">
					{{ $public_album->admins() != false ? count($public_album->admins()) : 0 }} {{ trans('common.admins') }}
				</span>
			</a>
		</li>	

		<li class="{{ Request::segment(2) == 'public-album-likes' ? 'active' : '' }}">
			<a href="{{ url($timeline->username.'/public-album-likes') }}">
				<span class="top-list">
					{{ $public_album->likes()->count() }} {{ trans('common.people_like_this') }}
				</span>
			</a>
		</li>
		
		<li class="{{ Request::segment(2) == 'public-album-posts' ? 'active' : '' }}"><a href="{{ url($timeline->username.'/public-album-posts') }}"><span class="top-list">{{ count($timeline->posts()->where('active', 1)->get()) }} {{ trans('common.posts') }}</span></a></li>
		@if(!$public_album->is_admin(Auth::user()->id))
		<li class="dropdown largescreen-report"><a href="#" class=" dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="top-list"> <i class="fa fa-ellipsis-h"></i></span></a>
			<ul class="dropdown-menu  report-dropdown">
				
				<li class="">
					<a href="#" class="save-timeline" data-timeline-id="{{ $timeline->id }}">
						<span class="top-list"><i class="fa fa-save"></i>
						@if($timeline->usersSaved()->where('user_id',Auth::user()->id)->get()->isEmpty())
							{{ trans('common.save') }}
						@else
							{{ trans('common.unsave') }}
						@endif
						</span>
					</a>
				</li>
				
					@if(!$timeline->reports->contains(Auth::user()->id))
						<li class=""><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.report') }}</a></li>
						
						<li class="hidden "><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.reported') }}</a></li>
					@else
						<li class="hidden "><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.report') }}</a></li>
						
						<li class=""><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}"> <i class="fa fa-flag" aria-hidden="true"></i> {{ trans('common.reported') }}</a></li>
					@endif
					
					@if(!$timeline->reports->contains(Auth::user()->id))
						<li class="smallscreen-report"><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}">{{ trans('common.report') }}</a></li>
						<li class="hidden smallscreen-report"><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}">{{ trans('common.reported') }}</a></li>
					@else
						<li class="hidden smallscreen-report"><a href="#" class="page-report report" data-timeline-id="{{ $timeline->id }}">{{ trans('common.report') }}</a></li>
						<li class="smallscreen-report"><a href="#" class="page-report reported" data-timeline-id="{{ $timeline->id }}">{{ trans('common.reported') }}</a></li>
					@endif
				
			</ul>
		</li>
		@endif
			
	</ul>
	<div class="status-button">
		<a href="#" class="btn btn-status">{{ trans('common.status') }}</a>
	</div>
	<div class="timeline-user-avtar">
		<a href="{{ url($timeline->username) }}">
		<img src=" @if($timeline->avatar_id) {{ url('public_album/avatar/small/'.$timeline->avatar->source) }} @else {{ url('public_album/avatar/default-public-album-avatar.png') }} @endif" alt="{{ $timeline->name }}" title="{{ $timeline->name }}" alt="{{ $timeline->name }}">
		</a>
			@if($timeline->public_album->is_admin(Auth::user()->id) == true)
			<div class="chang-user-avatar">
				<a href="#" class="btn btn-camera change-avatar"><i class="fa fa-camera" aria-hidden="true"></i><span class="avatar-text">{{ trans('common.update_profile') }}<span>{{ trans('common.picture') }}</span></span></a>
			</div>	
		@endif	
		
		<div class="user-avatar-progress hidden"></div>
	</div>
</div>
</div>
<script type="text/javascript">
	@if($timeline->background_id != NULL)
		$('body')
		.css('background-image', "url({{ url('/wallpaper/'.$timeline->wallpaper->source) }})")
		.css('background-attachment', 'fixed');

	@endif
</script>