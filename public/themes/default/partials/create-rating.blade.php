<?php
$RateAll=DB::table('star_rating')
        ->where('timeline_id',$location->timeline_id)
        ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
        ->first();
$Overall=0;$allUser=0;
if($RateAll){

    if($RateAll->star>0){
        $Overall=round($RateAll->star/$RateAll->userCount,1);
        $allUser=$RateAll->userCount;
    }
//    dd($RateAll);


}
if($allUser>0){
    Session::put('allUser',$allUser);
}

for($i=1;$i<=5;$i++){
    $Rate=DB::table('star_rating')
        ->where('timeline_id',$location->timeline_id)
        ->where('user_id',Auth::user()->id)
        ->where('rate_group',$i)
//        ->orderby('rate_group','asc')
        ->first();

    if($Rate){
        $RateStar[$Rate->rate_group]=$Rate->star_rating;
        $Sum=DB::table('star_rating')
            ->select(DB::raw('SUM(star_rating) as star'),DB::raw('count(user_id) as userCount'))
            ->where('timeline_id',$location->timeline_id)
            ->where('rate_group',$Rate->rate_group)
            ->groupby('rate_group')
            ->first();
        if($Sum){
            $Star[$Rate->rate_group]=round($Sum->star/$Sum->userCount,1);
            $width[$Rate->rate_group]=round($Sum->star/$Sum->userCount,1)*20;
        }
    }else{
        $RateStar[$i]=0;
        $Star[$i]=0;
        $width[$i]=0;
    }
}

?>
<link href="{{ Theme::asset()->url('css/review.css') }}" rel="stylesheet">
<style type="text/css">
    h5{font-size: 15px}
</style>
<div class="panel panel-default panel-post animated">
<div class="panel-heading no-bg">
<div class="post-author">

<input type="hidden" id="timeline_id" name="timeline_id" value="{{$location->timeline_id}}">

<style type="text/css">
    .star-rating-g1 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g2 {
        line-height:32px;
        font-size:1em;
    }
    .star-rating-g3 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g4 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g5 {
        line-height:32px;
        font-size:1em;
    }

    .star-rating-g1 .fa-star{color: #f99e00;}
    .star-rating-g2 .fa-star{color: #f99e00;}
    .star-rating-g3 .fa-star{color: #f99e00;}
    .star-rating-g4 .fa-star{color: #f99e00;}
    .star-rating-g5 .fa-star{color: #f99e00;}

</style>

<div class="row">
    <div class="col-md-8"><h3>Star</h3></div>
    <div class="col-md-4 text-right">
        <div class="ScoreOutOfTen">
            <div class="ReviewScore">
                <i class="ficon ficon-review-icon ReviewScore-Icon"></i>
                <span class="ReviewScore-Number" data-selenium="hotel-header-review-score">{{$Overall}}</span>
            </div>
            <div class="ScoreOutOfTen__OfTen">
                <div class="ScoreOutOfTen__OfTen__Text">/</div>
                <div class="ScoreOutOfTen__OfTen__Text">5</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-md-12"><h5><i class="fa fa-location-arrow"></i> {{trans('common.location')}}</h5></div>
    </div>
<div class="row">
    <div class="col-md-4">
        <span>{{trans('common.over_all_rating')}} <span class="pull-right text-danger"><strong>{{$Star[1]==0?'N/A':$Star[1]}}</strong></span></span>
    </div>
    <div class="col-md-3">
        <div class="star-rating-g1" style="margin-top: -5px">
            <span class="fa fa-star-o" data-rating="1"></span>
            <span class="fa fa-star-o" data-rating="2"></span>
            <span class="fa fa-star-o" data-rating="3"></span>
            <span class="fa fa-star-o" data-rating="4"></span>
            <span class="fa fa-star-o" data-rating="5"></span>
            <input type="hidden" name="whatever1" class="rating-value" value="{{$RateStar[1]}}">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[1]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[1]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
</div>
<div class="row">
    <div class="col-md-12">
        <h5><i class="fa fa-wheelchair-alt"></i> {{trans('common.disabled_facilities')}} </h5>
        </div>
</div>
<div class="row">
    <div class="col-md-4" style="margin-top: 5px">
        <span>{{trans('common.inside_rating')}} <span class="pull-right text-success"><strong>{{$Star[2]==0?'N/A':$Star[2]}}</strong></span></span>
    </div>
    <div class="col-md-3">
        <div class="star-rating-g2">
            <span class="fa fa-star-o" data-rating="1"></span>
            <span class="fa fa-star-o" data-rating="2"></span>
            <span class="fa fa-star-o" data-rating="3"></span>
            <span class="fa fa-star-o" data-rating="4"></span>
            <span class="fa fa-star-o" data-rating="5"></span>
            <input type="hidden" name="whatever2" class="rating-value" value="{{$RateStar[2]}}">

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[2]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[2]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
</div>

<div class="row">
    <div class="col-md-4" style="margin-top: 5px">
       <span>{{trans('common.outside_rating')}} <span class="pull-right text-success"><strong>{{$Star[3]==0?'N/A':$Star[3]}}</strong></span></span>
    </div>
    <div class="col-md-3">
        <div class="star-rating-g3">
            <span class="fa fa-star-o" data-rating="1"></span>
            <span class="fa fa-star-o" data-rating="2"></span>
            <span class="fa fa-star-o" data-rating="3"></span>
            <span class="fa fa-star-o" data-rating="4"></span>
            <span class="fa fa-star-o" data-rating="5"></span>
            <input type="hidden" name="whatever2" class="rating-value" value="{{$RateStar[3]}}">

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[3]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[3]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
</div>

<div class="row">
    <div class="col-md-4" style="margin-top: 5px">
         <span>{{trans('common.toilet_rating')}} <span class="pull-right text-success"><strong>{{$Star[4]==0?'N/A':$Star[4]}}</strong></span></span>
    </div>
    <div class="col-md-3">
        <div class="star-rating-g4">
            <span class="fa fa-star-o" data-rating="1"></span>
            <span class="fa fa-star-o" data-rating="2"></span>
            <span class="fa fa-star-o" data-rating="3"></span>
            <span class="fa fa-star-o" data-rating="4"></span>
            <span class="fa fa-star-o" data-rating="5"></span>
            <input type="hidden" name="whatever2" class="rating-value" value="{{$RateStar[4]}}">

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[4]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[4]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
</div>

<div class="row">
    <div class="col-md-4" style="margin-top: 5px">
         <span>{{trans('common.parking_rating')}} <span class="pull-right text-success"><strong>{{$Star[5]==0?'N/A':$Star[5]}}</strong></span></span>
    </div>
    <div class="col-md-3">
        <div class="star-rating-g5">
            <span class="fa fa-star-o" data-rating="1"></span>
            <span class="fa fa-star-o" data-rating="2"></span>
            <span class="fa fa-star-o" data-rating="3"></span>
            <span class="fa fa-star-o" data-rating="4"></span>
            <span class="fa fa-star-o" data-rating="5"></span>
            <input type="hidden" name="whatever2" class="rating-value" value="{{$RateStar[5]}}">

        </div>
    </div>
    <div class="col-lg-4">
        <div class="progress progress-sm">
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{{$width[5]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$width[5]}}%">

            </div>
        </div>
    </div>
    <div class="col-md-1">/5</div>
</div>


</div>
</div>
</div>

<!-- jQuery 3 -->
<script src="{{asset('member/assets/jquery/dist/jquery.min.js')}}"></script>
<script language="javascript">
    var $star_rating1 = $('.star-rating-g1 .fa');
    var $star_rating2 = $('.star-rating-g2 .fa');
    var $star_rating3 = $('.star-rating-g3 .fa');
    var $star_rating4 = $('.star-rating-g4 .fa');
    var $star_rating5 = $('.star-rating-g5 .fa');
    var timeline_id=$('#timeline_id').val();

    var SetRatingStar1 = function() {
        return $star_rating1.each(function() {
            if (parseInt($star_rating1.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating1.on('click', function() {
        $star_rating1.siblings('input.rating-value').val($(this).data('rating'));
//            alert($(this).data('rating'));
        SaveRatings(1,$(this).data('rating'),timeline_id);
        return SetRatingStar1();
    });

    var SetRatingStar2 = function() {
        return $star_rating2.each(function() {
            if (parseInt($star_rating2.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {

                return $(this).removeClass('fa-star-o').addClass('fa-star');

            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating2.on('click', function() {
        $star_rating2.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(2,$(this).data('rating'),timeline_id);
        return SetRatingStar2();
    });

    var SetRatingStar3 = function() {
        return $star_rating3.each(function() {
            if (parseInt($star_rating3.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {

                return $(this).removeClass('fa-star-o').addClass('fa-star');

            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating3.on('click', function() {
        $star_rating3.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(3,$(this).data('rating'),timeline_id);
        return SetRatingStar3();
    });

    var SetRatingStar4 = function() {
        return $star_rating4.each(function() {
            if (parseInt($star_rating4.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating4.on('click', function() {
        $star_rating4.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(4,$(this).data('rating'),timeline_id);
        return SetRatingStar4();
    });

    var SetRatingStar5 = function() {
        return $star_rating5.each(function() {
            if (parseInt($star_rating5.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating5.on('click', function() {
        $star_rating5.siblings('input.rating-value').val($(this).data('rating'));
        SaveRatings(5,$(this).data('rating'),timeline_id);
        return SetRatingStar5();
    });



    SetRatingStar1();
    SetRatingStar2();
    SetRatingStar3();
    SetRatingStar4();
    SetRatingStar5();

    function SaveRatings(group,id,timeline_id){
//            alert(timeline_id+' '+id);
        $.ajax({
            type:'get',
            url:SP_source() + 'ajax/star_rating/data',
            data:{'id':id,'group':group,'timeline_id':timeline_id},
            success:function(data){
                alert(data);
            }
        });
    }
    $(document).ready(function() {

    });
</script>
