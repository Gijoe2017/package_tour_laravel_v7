    @if(isset($post->shared_post_id))
        <?php
        $sharedOwner = $post;
        $post = App\Post::where('id', $post->shared_post_id)->with('comments')->first();
        ?>
    @endif


    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('status') }}" role="alert">
      {!! Session::get('message') !!}
    </div>
    @endif

    @if($post!=null)
        @if($post->timeline_id==Session::get('timeline_id'))
            <div class="card card-pin">
             <div class="panel">
                  {{--<div class="panel-heading no-bg">--}}
                      {{--<div class="post-author"></div>--}}
                  {{--</div>--}}
                  <div class="panel-body">
                      <form action="{{ url('') }}" method="post" class="create-post-form1">
                          {{ csrf_field() }}

                              <label class="text-info" style="font-size: 14px"> {{trans('common.where_did_you_go')}}</label>
                              <div class="search-location">
                                  <input type="hidden" id="username" name="username" value="{{Auth::user()->name}}">
                                  {{--<div class="js-result-container"></div>--}}
                                  <div class="typeahead__container">
                                      <div class="typeahead__field">
                                          <div class="typeahead__query">
                                              <input class="js-typeahead" id="q" name="q" type="search" autofocus autocomplete="off" required >
                                          </div>
                                          <div class="typeahead__button">
                                              <button type="submit"><i class="fa fa-map-marker"></i></button>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="new-location" style="display: none">

                              <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                  {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                                  {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                    {{ $errors->first('name') }}
                                </span>
                                  @endif
                              </fieldset>
                              <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                  {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                  {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                  @if ($errors->has('category_id'))
                                      <span class="help-block">
										{{ $errors->first('category_id') }}
									</span>
                                  @endif
                              </fieldset>

                              {{--<fieldset class="form-group required {{ $errors->has('category_sub1_id') ? ' has-error' : '' }}">--}}
                              {{--{{ Form::label('category_sub1_id', trans('common.category_sub1'), ['class' => 'control-label']) }}--}}
                              {{--{{ Form::select('category_sub1_id', array('' => trans('common.select_category_sub1')), '', array('class' => 'form-control')) }}--}}
                              {{--@if ($errors->has('category_sub1_id'))--}}
                              {{--<span class="help-block">--}}
                              {{--{{ $errors->first('category_sub1_id') }}--}}
                              {{--</span>--}}
                              {{--@endif--}}
                              {{--</fieldset>--}}

                              <fieldset class="form-group required {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                  {{ Form::label('country_id', trans('common.country'), ['class' => 'control-label']) }}
                                  {{ Form::select('country_id', array('' => trans('common.select_country'))+ $country_options, '', array('class' => 'form-control')) }}
                                  @if ($errors->has('country_id'))
                                      <span class="help-block">
										    {{ $errors->first('country_id') }}
									  </span>
                                  @endif
                              </fieldset>

                              <fieldset id="box-state" class="form-group {{ $errors->has('state_id') ? ' has-error' : '' }}">
                                  {{ Form::label('state_id', trans('common.state'), ['class' => 'control-label']) }}
                                  {{ Form::select('state_id', array('' => trans('common.select_state')), '', array('class' => 'form-control')) }}
                                  @if ($errors->has('state_id'))
                                      <span class="help-block">
                                        {{ $errors->first('state_id') }}
                                  </span>
                                  @endif
                              </fieldset>


                              </div>
                                <BR>
                              {!! Theme::partial('create-post2',compact('timeline','user_post')) !!}

                              {{--<fieldset class="form-group">--}}
                                  {{--<div class="text-center">--}}
                                      {{--<button type="submit" class="btn btn-submit btn-success">{{ trans('common.post') }}</button>--}}
                                      {{--{{ Form::submit(trans('common.create_location'), ['class' => 'btn btn-default btn-block']) }}--}}
                                  {{--</div>--}}
                              {{--</fieldset>--}}
                      </form>.
                  </div>

          </div>
            </div>
        @endif
        <div class="card card-pin">
    <div class="panel panel-default panel-post animated" id="post{{ $post->id }}">
      <div class="panel-heading no-bg">
        <div class="post-author">
          <div class="post-options">
            <ul class="list-inline no-margin">
              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                  @if($post->notifications_user->contains(Auth::user()->id))
                    <li class="main-link">
                      <a href="#" data-post-id="{{ $post->id }}" class="notify-user unnotify">
                        <i class="fa  fa-bell-slash" aria-hidden="true"></i>{{ trans('common.stop_notifications') }}
                        <span class="small-text">{{ trans('messages.stop_notification_text') }}</span>
                      </a>
                    </li>
                    <li class="main-link hidden">
                      <a href="#" data-post-id="{{ $post->id }}" class="notify-user notify">
                        <i class="fa fa-bell" aria-hidden="true"></i>{{ trans('common.get_notifications') }}
                        <span class="small-text">{{ trans('messages.get_notification_text') }}</span>
                      </a>
                    </li>
                  @else
                    <li class="main-link hidden">
                      <a href="#" data-post-id="{{ $post->id }}" class="notify-user unnotify">
                        <i class="fa  fa-bell-slash" aria-hidden="true"></i>{{ trans('common.stop_notifications') }}
                        <span class="small-text">{{ trans('messages.stop_notification_text') }}</span>
                      </a>
                    </li>
                    <li class="main-link">
                      <a href="#" data-post-id="{{ $post->id }}" class="notify-user notify">
                        <i class="fa fa-bell" aria-hidden="true"></i>{{ trans('common.get_notifications') }}
                        <span class="small-text">{{ trans('messages.get_notification_text') }}</span>
                      </a>
                    </li>
                  @endif

                  @if(Auth::user()->id == $post->user->id)
                    <li class="main-link">
                      <a href="#" data-post-id="{{ $post->id }}" class="edit-post">
                        <i class="fa fa-edit" aria-hidden="true"></i>{{ trans('common.edit') }}
                        <span class="small-text">{{ trans('messages.edit_text') }}</span>
                      </a>
                    </li>
                  @endif

                  @if((Auth::id() == $post->user->id) || ($post->timeline_id == Auth::user()->timeline_id))
                    <li class="main-link">
                      <a href="#" class="delete-post" data-post-id="{{ $post->id }}">
                        <i class="fa fa-trash" aria-hidden="true"></i>{{ trans('common.delete') }}
                        <span class="small-text">{{ trans('messages.delete_text') }}</span>
                      </a>
                    </li>
                  @endif

                  @if(Auth::user()->id != $post->user->id)
                    <li class="main-link">
                      <a href="#" class="hide-post" data-post-id="{{ $post->id }}">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>{{ trans('common.hide_notifications') }}
                        <span class="small-text">{{ trans('messages.hide_notification_text') }}</span>
                      </a>
                    </li>

                    <li class="main-link">
                      <a href="#" class="save-post" data-post-id="{{ $post->id }}">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        @if(!Auth::user()->postsSaved->contains($post->id))
                          {{ trans('common.save_post') }}
                          <span class="small-text">{{ trans('messages.post_save_text') }}</span>
                        @else
                          {{ trans('common.unsave_post') }}
                          <span class="small-text">{{ trans('messages.post_unsave_text') }}</span>
                        @endif
                      </a>
                    </li>

                    <li class="main-link">
                      <a href="#" class="manage-report report" data-post-id="{{ $post->id }}">
                        <i class="fa fa-flag" aria-hidden="true"></i>{{ trans('common.report') }}
                        <span class="small-text">{{ trans('messages.report_text') }}</span>
                      </a>
                    </li>
                  @endif
                  <li class="divider"></li>

                  <li class="main-link">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/share-post/'.$post->id)) }}" class="fb-xfbml-parse-ignore" target="_blank"><i class="fa fa-facebook-square"></i>Facebook {{ trans('common.share') }}</a>
                  </li>

                  <li class="main-link">
                    <a href="https://twitter.com/intent/tweet?text={{ url('/share-post/'.$post->id) }}" target="_blank"><i class="fa fa-twitter-square"></i>Twitter {{ trans('common.tweet') }}</a>
                  </li>

                  <li class="main-link">
                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-share-alt"></i>Embed {{ trans('common.post') }}</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
            <?php
            $date=\Date::parse($post->created_at);
            ?>
          <div class="user-avatar">
            @if($media!=null)
              <a href="{{ url($timeline->username) }}">
                <img src="{{url('location/avatar/small/'.$media->source) }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
              </a>
            @else
              <img src="{{url('location/avatar/default-location-avatar.png') }}" alt="{{$timeline->name }}" title="{{ $timeline->name }}">
            @endif
            {{--<a href="{{ url($post->user->username) }}"><img src="{{ $post->user->avatar }}" alt="{{ $post->user->name }}" title="{{ $post->user->name }}"></a>--}}
          </div>
          <div class="user-post-details">
            <ul class="list-unstyled no-margin">
              <li>
                @if(isset($sharedOwner))
                  <a href="{{ url($sharedOwner->user->username) }}" title="{{ '@'.$sharedOwner->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                    {{ $sharedOwner->user->name }}
                  </a>
                  shared
                @endif
                <a href="{{ url($timeline->username) }}" title="{{ '@'.$post->user->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
                  {{ $timeline->name }}
                </a>
                @if($post->user->verified)
                    <span class="verified-badge bg-success">
                        <i class="fa fa-check"></i>
                    </span>
                @endif

                @if(isset($sharedOwner))
                  's post
                @endif

                @if($post->users_tagged->count() > 0)
                  {{ trans('common.with') }}
                      <?php $post_tags = $post->users_tagged->pluck('name')->toArray(); ?>
                      <?php $post_tags_ids = $post->users_tagged->pluck('id')->toArray(); ?>
                  @foreach($post->users_tagged as $key => $user)
                    @if($key==1)
                      {{ trans('common.and') }}
                      @if(count($post_tags)==1)
                        <a href="{{ url($user->username) }}"> {{ $user->name }}</a>
                      @else
                        <a href="#" data-toggle="tooltip" title="" data-placement="top" class="show-users-modal" data-html="true" data-heading="{{ trans('common.with_people') }}"  data-users="{{ implode(',', $post_tags_ids) }}" data-original-title="{{ implode('<br />', $post_tags) }}"> {{ count($post_tags).' '.trans('common.others') }}</a>
                      @endif
                      @break
                    @endif
                    @if($post_tags != null)
                      <a href="{{ url($user->username) }}" class="user"> {{ array_shift($post_tags) }} </a>
                    @endif
                  @endforeach

                @endif


              </li>
              <li>

                <span>
                    <?php
                    $category=\App\Category::where('category_id',$location->category_id)->active()->first();
                    if($location->category_sub1_id){
                        $sub1category=$category->subcategory()->where('category_sub1_id',$location->category_sub1_id)->first();
                    }

                    ?>
                    @if($location->category_sub2_id>0)
                        {{ $sub1category->sub2category()->where('category_sub2_id',$location->category_sub2_id)->active()->first()->category_sub2_name  }},
                    @elseif($location->category_sub1_id>0)
                        {{ $category->subcategory()->where('category_sub1_id',$location->category_sub1_id)->first()->sub1_name }},
                    @else
                        @if($location->category_id)
                        {{ \App\Category::where('category_id',$location->category_id)->active()->first()->name  }},
                        @endif
                    @endif
                </span>

                  <span>
					  @if($city)
                          {{ $city->city }}
                      @endif
                      @if($state)
                          {{$state->state}}
                      @endif
                      @if($country)
                          {{$country->country}}
                      @endif
				</span>


                @if($post->location != NULL && !isset($sharedOwner))
                  {{ trans('common.at') }}
                      <span class="post-place">
                      <a target="_blank" href="{{ url('locations'.$post->location) }}">
                        <i class="fa fa-map-marker"></i> {{ $post->location }}
                      </a>
                      </span>

              </li>
              @endif

            </ul>

          </div>
            <div  style="font-size: 12px;padding-left: 50px; color:#859AB5 ">

                    <span>
                        <div class="rating-wrap">
                            <span>
                                ({{ $post->user->name }}
                                @if($post->rate_group!='')
                                    {{trans('common.reviewed_on')}}
                                    <?php
                                        $rate_group=$post->rate_group;
                                        if($rate_group=='1') {
                                            $rate_group_title = trans('common.over_all_rating');
                                        }else if($rate_group=='2'){
                                            $rate_group_title=trans('common.inside_rating');
                                        }else if($rate_group=='3'){
                                            $rate_group_title=trans('common.outside_rating');
                                        }else if($rate_group=='4'){
                                            $rate_group_title=trans('common.toilet_rating');
                                        }else if($rate_group=='5'){
                                            $rate_group_title=trans('common.parking_rating');
                                        }else if($rate_group=='6'){
                                            $rate_group_title=trans('common.cleanliness');
                                        }else if($rate_group=='7'){
                                            $rate_group_title=trans('common.normal_parking');
                                        }else if($rate_group=='8'){
                                            $rate_group_title=trans('common.normal_toilet');
                                        }
                                    ?>
                                    {{$rate_group_title}}
                                @else
                                    {{trans('common.posted_on')}}
                                @endif
                            </span>
                            @if($post->rate_group!='')
                                <?php
                                $width=0;$star_rating=0;
                                if($post->star_rating){
                                    $width=$post->star_rating*20;
                                    $star_rating=$post->star_rating;
                                }
                                ?>
                                <ul class="rating-stars">
                                <li style="width:{{$width}}%" class="stars-active">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </li>
                                <li>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </li>
                            </ul>
                            @endif
                            <span>{{$date->ago()}})</span>
                        </div>
                    </span>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="text-wrapper">
                <?php
                $links = preg_match_all("/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $post->description, $matches);
                $main_description = $post->description;
                ?>
                @foreach($matches[0] as $link)
                    <?php
                        $linkPreview = new LinkPreview($link);
                        $parsed = $linkPreview->getParsed();
                        foreach ($parsed as $parserName => $main_link) {
                            $data = '<div class="row link-preview">
                                      <div class="col-md-3">
                                        <a target="_blank" href="'.$link.'"><img src="'.$main_link->getImage().'"></a>
                                      </div>
                                      <div class="col-md-9">
                                        <a target="_blank" href="'.$link.'">'.$main_link->getTitle().'</a><br>'.substr($main_link->getDescription(), 0, 500). '...'.'
                                      </div>
                                    </div>';
                        }
                        $main_description = str_replace($link, $data, $main_description);
                        ?>
                @endforeach

                <p class="post-description">
                 {{--************************ Cannot fig here ****************************--}}
                    {{$main_description}}
                 {{--{!! clean($main_description) !!}--}}
                </p>

                <div class="post-image-holder  @if(count($post->images()->get()) == 1) single-image @endif">
                    @foreach($post->images()->get() as $postImage)
                        @if($postImage->type=='image')
                            @if(!file_exists(storage_path('user/gallery/mid/'.$postImage->source)))
                                <a href="{{ url('user/gallery/'.$postImage->source) }}" data-lightbox="imageGallery.{{ $post->id }}" >
                                    <img src="{{ url('user/gallery/mid/'.$postImage->source) }}"  title="{{ $post->user->name }}" alt="{{ $post->user->name }}">
                                </a>
                            @else
                                <img src="{{ url('location/avatar/no-image-full.jpg') }}">
                            @endif
                        @endif
                    @endforeach
                </div>
                <div class="post-v-holder">
                    @foreach($post->images()->get() as $postImage)
                        @if($postImage->type=='video')
                            <video width="100%" preload="none" height="auto" poster="{{ url('user/gallery/video/'.$postImage->title) }}.jpg" controls class="video-video-playe">
                                <source src="{{ url('user/gallery/video/'.$postImage->source) }}" type="video/mp4">
                                <!-- Captions are optional -->
                            </video>
                        @endif
                    @endforeach
                </div>
            </div>
            @if($post->youtube_video_id)
                <iframe  src="https://www.youtube.com/embed/{{ $post->youtube_video_id }}" frameborder="0" allowfullscreen></iframe>
            @endif
            @if($post->soundcloud_id)
                <div class="soundcloud-wrapper">
                    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $post->soundcloud_id }}&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                </div>
            @endif

            <ul class="actions-count list-inline">
                @if($post->users_liked()->count() > 0)
                    <?php
                    $liked_ids = $post->users_liked->pluck('id')->toArray();
                    $liked_names = $post->users_liked->pluck('name')->toArray();
                    ?>
                    <li>
                        <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.likes') }}"  data-users="{{ implode(',', $liked_ids) }}" data-original-title="{{ implode('<br />', $liked_names) }}"><span class="count-circle"><i class="fa fa-thumbs-up"></i></span> {{ $post->users_liked->count() }} {{ trans('common.likes') }}</a>
                    </li>
                @endif

                @if($post->comments->count() > 0)
                    <li>
                        <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>{{ $post->comments->count() }} {{ trans('common.comments') }}</a>
                    </li>
                @endif

                @if($post->shares->count() > 0)
                    <?php
                    $shared_ids = $post->shares->pluck('id')->toArray();
                    $shared_names = $post->shares->pluck('name')->toArray(); ?>
                    <li>
                        <a href="#" class="show-users-modal" data-html="true" data-heading="{{ trans('common.shares') }}"  data-users="{{ implode(',', $shared_ids) }}" data-original-title="{{ implode('<br />', $shared_names) }}"><span class="count-circle"><i class="fa fa-share"></i></span> {{ $post->shares->count() }} {{ trans('common.shares') }}</a>
                    </li>
                @endif


            </ul>
            <div class="text-right">
                @if($timeline->type == 'location')
                    @if($post->rate_group>0)
                        @if($post->date_visited_location!='0000-00-00')
                            <small style="color: #9cc2cb"><i class="fa fa-calendar"></i> {{trans('common.date_visit_location').' '.\Date::parse($post->date_visited_location.'01:01:10')->format('j F Y')}}</small>
                        @endif
                    @endif
                @endif
            </div>
        </div>


    <?php
        $display_comment ="";
        $user_follower = $post->chkUserFollower(Auth::user()->id,$post->user_id);
        $user_setting = $post->chkUserSettings($post->user_id);

        if($user_follower != NULL)
        {
            if($user_follower == "only_follow") {
                $display_comment = "only_follow";
            }elseif ($user_follower == "everyone") {
                $display_comment = "everyone";
            }
        }
        else{
            if($user_setting){
                if($user_setting == "everyone"){
                    $display_comment = "everyone";
                }
            }
        }

        ?>

      <div class="panel-footer socialite">

        <ul class="list-inline footer-list">

          @if(!$post->users_liked->contains(Auth::user()->id))

            <li><a href="#" class="like-post like-{{ $post->id }}" data-post-id="{{ $post->id }}"><i class="fa fa-thumbs-o-up"></i>{{ trans('common.like') }}</a></li>

            <li class="hidden"><a href="#" class="like-post unlike-{{ $post->id }}" data-post-id="{{ $post->id }}"><i class="fa fa-thumbs-o-down"></i></i>{{ trans('common.unlike') }}</a></li>
          @else
            <li class="hidden"><a href="#" class="like-post like-{{ $post->id }}" data-post-id="{{ $post->id }}"><i class="fa fa-thumbs-o-up"></i>{{ trans('common.like') }}</a></li>
            <li><a href="#" class="like-post unlike-{{ $post->id }}" data-post-id="{{ $post->id }}"><i class="fa fa-thumbs-o-down"></i></i>{{ trans('common.unlike') }}</a></li>
          @endif
          <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>{{ trans('common.comment') }}</a></li>

          @if(Auth::user()->id != $post->user_id)
            @if(!$post->users_shared->contains(Auth::user()->id))
              <li><a href="#" class="share-post share" data-post-id="{{ $post->id }}"><i class="fa fa-share-square-o"></i>{{ trans('common.share') }}</a></li>
              <li class="hidden"><a href="#" class="share-post shared" data-post-id="{{ $post->id }}"><i class="fa fa fa-share-square-o"></i>{{ trans('common.unshare') }}</a></li>
            @else
              <li class="hidden"><a href="#" class="share-post share" data-post-id="{{ $post->id }}"><i class="fa fa-share-square-o"></i>{{ trans('common.share') }}</a></li>
              <li><a href="#" class="share-post shared" data-post-id="{{ $post->id }}"><i class="fa fa fa-share-square-o"></i>{{ trans('common.unshare') }}</a></li>
            @endif
          @endif
            <li>
                {{--<a href="{{ url($timeline->username) }}" data-modal-title="Toechok popup location"><i class="fa fa-image"></i> เพิ่มภาพ</a>--}}
                <a href="#" class="popup" data-content="{{ url($timeline->username) }}/popup"><i class="fa fa-image"></i> {{trans('common.add_photo')}}</a>
            </li>
        </ul>
      </div>

      @if($post->comments->count() > 0 || $post->user_id == Auth::user()->id || $display_comment == "everyone")
        <div class="comments-section all_comments" style="display:none">
          <div class="comments-wrapper">
            <div class="to-comment">  <!-- to-comment -->
              @if($display_comment == "only_follow" || $display_comment == "everyone" || $user_setting == "everyone" || $post->user_id == Auth::user()->id)
                <div class="commenter-avatar">
                  <a href="#"><img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" title="{{ Auth::user()->name }}"></a>
                </div>
                <div class="comment-textfield">
                  <form action="#" class="comment-form" method="post" files="true" enctype="multipart/form-data" id="comment-form">
                    <div class="comment-holder">{{-- commentholder --}}
                      <input class="form-control post-comment" autocomplete="off" data-post-id="{{ $post->id }}" name="post_comment" placeholder="{{ trans('messages.comment_placeholder') }}" >
                      <input type="file" class="comment-images-upload hidden" accept="image/jpeg,image/png,image/gif" name="comment_images_upload">
                      <ul class="list-inline meme-reply hidden">
                        <li><a href="#" id="imageComment"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                        {{-- <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li> --}}
                      </ul>
                    </div>
                    <div id="comment-image-holder"></div>
                  </form>
                </div>
                <div class="clearfix"></div>
              @endif
            </div><!-- to-comment -->

            <div class="comments post-comments-list"> <!-- comments/main-comment  -->
              @if($post->comments->count() > 0)
                @foreach($post->comments as $comment)
                  {!! Theme::partial('comment',compact('comment','post')) !!}
                @endforeach
              @endif
            </div>
          </div>
        </div>
      @endif
    </div>

        </div>
  @endif






