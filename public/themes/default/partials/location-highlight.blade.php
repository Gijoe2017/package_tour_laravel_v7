    @if(isset($post->shared_post_id))
        <?php
        $sharedOwner = $post;
        $post = App\Post::where('id', $post->shared_post_id)->with('comments')->first();
        ?>
    @endif


    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('status') }}" role="alert">
      {!! Session::get('message') !!}
    </div>
    @endif

    @if($post!=null)
        @if($post->timeline_id==Session::get('timeline_id'))
            <div class="col-md-8 col-md-offset-2">
             <div class="panel">
                  <div class="panel-body">
                      <form action="{{ url('') }}" method="post" class="create-post-form-highlight">
                          {{ csrf_field() }}
                              <label class="text-info" style="font-size: 14px"> {{trans('common.where_did_you_go')}}</label>
                              <div class="search-location">
                                  <input type="hidden" id="username" name="username" value="{{Auth::user()->name}}">
                                  {{--<div class="js-result-container"></div>--}}
                                  <div class="typeahead__container">
                                      <div class="typeahead__field">
                                          <div class="typeahead__query">
                                              <input class="js-typeahead" id="q" name="q" type="search" autofocus autocomplete="off" required >
                                          </div>
                                          <div class="typeahead__button">
                                              <button type="submit"><i class="fa fa-map-marker"></i></button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="new-location" style="display: none">
                              <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                  {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                                  {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                        {{ $errors->first('name') }}
                                     </span>
                                  @endif
                              </fieldset>
                              <fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                  {{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
                                  {{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                  @if ($errors->has('category_id'))
                                      <span class="help-block">
										{{ $errors->first('category_id') }}
									</span>
                                  @endif
                              </fieldset>

                              <fieldset style="display: none" class="form-group category_sub1 required {{ $errors->has('category_sub1_id') ? ' has-error' : '' }}">
                                  {{ Form::label('category_sub1_id', trans('common.category_sub1'), ['class' => 'control-label']) }}
                                  {{ Form::select('category_sub1_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                  @if ($errors->has('category_sub1_id'))
                                      <span class="help-block">
                                    {{ $errors->first('category_sub1_id') }}
                                </span>
                                  @endif
                              </fieldset>

                                  <fieldset style="display: none"  class="form-group category_sub2 required {{ $errors->has('category_sub2_id') ? ' has-error' : '' }}">
                                      {{ Form::label('category_sub2_id', trans('common.category_sub2'), ['class' => 'control-label']) }}
                                      {{ Form::select('category_sub2_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                      @if ($errors->has('category_sub2_id'))
                                          <span class="help-block">
                                            {{ $errors->first('category_sub2_id') }}
                                          </span>
                                      @endif
                                  </fieldset>

                              <fieldset class="form-group required {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                  {{ Form::label('country_id', trans('common.country'), ['class' => 'control-label']) }}
                                  {{ Form::select('country_id', array('' => trans('common.select_country'))+ $country_options, '', array('class' => 'form-control')) }}
                                  @if ($errors->has('country_id'))
                                      <span class="help-block">
										    {{ $errors->first('country_id') }}
									  </span>
                                  @endif
                              </fieldset>

                              <fieldset id="box-state" class="form-group required {{ $errors->has('state_id') ? ' has-error' : '' }}">
                                  {{ Form::label('state_id', trans('common.state'), ['class' => 'control-label']) }}
                                  {{ Form::select('state_id', array('' => trans('common.select_state')), '', array('class' => 'form-control')) }}
                                  @if ($errors->has('state_id'))
                                      <span class="help-block">
                                        {{ $errors->first('state_id') }}
                                  </span>
                                  @endif
                              </fieldset>

                                  <fieldset style="display: none" class="form-group city_id required {{ $errors->has('city_id') ? ' has-error' : '' }}">
                                      {{ Form::label('city_id', trans('common.city'), ['class' => 'control-label']) }}
                                      {{ Form::select('city_id', array('' => trans('common.select_city')), '', array('class' => 'form-control')) }}
                                      @if ($errors->has('city_id'))
                                          <span class="help-block">
										    {{ $errors->first('city_id') }}
									  </span>
                                      @endif
                                  </fieldset>

                                  <fieldset style="display: none" class="form-group city_sub1_id required{{ $errors->has('city_sub1_id') ? ' has-error' : '' }}">
                                      {{ Form::label('city_sub1_id', trans('common.city_sub'), ['class' => 'control-label']) }}
                                      {{ Form::select('city_sub1_id', array('' => trans('common.select_city_sub')), '', array('class' => 'form-control')) }}
                                      @if ($errors->has('city_sub1_id'))
                                          <span class="help-block">
										    {{ $errors->first('city_sub1_id') }}
									  </span>
                                      @endif
                                  </fieldset>

                              </div>
                                <BR>
                              {!! Theme::partial('create-post3',compact('timeline','user_post')) !!}
                      </form>
                  </div>

          </div>
            </div>
        @endif

  @endif






