<div class="user-profile-buttons">
	<div class="row public-album-like-links">

		@if($public_album->is_admin(Auth::user()->id))
			@if(!$public_album->likes->contains(Auth::user()->id))								
				<div class="col-md-6 col-sm-6 col-xs-6 left-col"><a href="#" class="btn btn-options btn-block btn-default public-album-like like" data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-6 col-sm-6 col-xs-6 left-col hidden"><a href="#" class="btn btn-options btn-block btn-success public-album-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@else
				<div class="col-md-6 col-sm-6 col-xs-6 left-col hidden"><a href="#" class="btn btn-options btn-block btn-default public-album-like like " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-6 col-sm-6 col-xs-6 left-col"><a href="#" class="btn btn-options btn-block btn-success public-album-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@endif

			<div class="col-md-6 col-sm-6 col-xs-6 right-col">
				<a href="{{ url('/'.$timeline->username.'/public-album-settings/general') }}" class="btn btn-options btn-block btn-default"><i class="fa fa-gear"></i>
					{{ trans('common.settings') }}
				</a>
			</div>

		@else
			@if(!$public_album->likes->contains(Auth::user()->id))								
				<div class="col-md-12 col-sm-12 col-xs-12  page"><a href="#" class="btn btn-options btn-block btn-default public-album-like like" data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-12 col-sm-12 col-xs-12  page hidden"><a href="#" class="btn btn-options btn-block btn-success public-album-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@else
				<div class="col-md-12 col-sm-12 col-xs-12  page hidden"><a href="#" class="btn btn-options btn-block btn-default public-album-like like " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-thumbs-up"></i> {{ trans('common.like') }}</a></div>
				<div class="col-md-12 col-sm-12 col-xs-12  page"><a href="#" class="btn btn-options btn-block btn-success public-album-like liked " data-timeline-id="{{ $timeline->id }}"><i class="fa fa-check"></i> {{ trans('common.liked') }}</a></div>
			@endif			

			<div class="col-md-6 col-sm-6 col-xs-6 left-col page hidden">
				<a href="{{ url('/'.Auth::user()->username.'/settings/general') }}" class="btn btn-options btn-block btn-default">
					<i class="fa fa-inbox"></i> {{ trans('common.messages') }}
				</a>
			</div>

		@endif
		</div>
	</div>

	<!-- Change avatar form -->
	<form class="change-avatar-form hidden" action="{{ url('ajax/change-avatar') }}" method="post" enctype="multipart/form-data">
		<input name="timeline_id" value="{{ $timeline->id }}" type="hidden">
		<input name="timeline_type" value="{{ $timeline->type }}" type="hidden">
		<input class="change-avatar-input hidden" accept="image/jpeg,image/png" type="file" name="change_avatar" >
	</form>

	<!-- Change cover form -->
	<form class="change-cover-form hidden" action="{{ url('ajax/change-cover') }}" method="post" enctype="multipart/form-data">
		<input name="timeline_id" value="{{ $timeline->id }}" type="hidden">
		<input name="timeline_type" value="{{ $timeline->type }}" type="hidden">
		<input class="change-cover-input hidden" accept="image/jpeg,image/png" type="file" name="change_cover" >
	</form>

	<div class="user-bio-block">
		<div class="bio-header">{{ trans('common.about') }}</div>
		<div class="bio-description">
			{{ ($timeline['about'] != NULL) ? $timeline['about'] : trans('messages.no_description') }}
		</div>
		{{--{{dd($public_album)}}--}}
		<ul class="bio-list list-unstyled">
			<li>
				<i class="fa fa-folder-o" aria-hidden="true"></i><span>{{ \App\Category::where('category_id',$public_album->category_id)->where('language_code',Auth::user()->language)->first()->name  }}</span>
			</li>

			@if($public_album->address != null)
				<li>
					<i class="fa fa-map-marker" aria-hidden="true"></i><span>{{ $public_album->address }}</span>
				</li>
			@endif

			@if($public_album->website != null)
				<li>
					<i class="fa fa-globe" aria-hidden="true"></i><span>{{ $public_album->website }}</span>
				</li>
			@endif

			@if($public_album->phone != null)
				<li>
					<i class="fa fa-phone" aria-hidden="true"></i><span>{{ $public_album->phone }}</span>
				</li>
			@endif
		</ul>
	</div>

	<div class="widget-pictures widget-best-pictures"><!-- /pages-liked -->
		<div class="picture side-left">
			{{ trans('common.members') }}
		</div>
		@if(count($public_album_members) > 0)
			<div class="side-right show-all">
				<a href="{{ url($timeline->username.'/public-album-members') }}">{{ trans('common.show_all') }}</a>
			</div>
		@endif
		<div class="clearfix"></div>
		<div class="best-pictures my-best-pictures">
			<div class="row">
				@if(count($public_album_members) > 0)
					@foreach($public_album_members->take(12) as $public_album_member)
					<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
						<a href="{{ url($public_album_member->username) }}" class="image-hover" data-toggle="tooltip" data-placement="top" title="{{ $public_album_member->name }}">
							<img src="{{ $public_album_member->avatar }}" alt="{{ $public_album_member->name }}" title="{{ $public_album_member->name }}">
						</a>
					</div>
					@endforeach
				@else
					<div class="alert alert-warning">{{ trans('messages.no_members') }}</div>
				@endif

			</div>
		</div>
	</div> <!-- /pages-liked -->

