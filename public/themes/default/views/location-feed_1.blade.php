<!-- main-section -->
	<!-- <div class="main-content"> -->
{{--<link media="all" type="text/css" rel="stylesheet" href="{{asset('/themes/default/assets/css/4.0/bootstrap.min.css')}}">--}}
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery.jspanel.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/default/assets/select1/jquery.typeahead.css')}}">
<style type="text/css">

	.container {
		width: 98%;
	}

	.logo {
		border-radius: 50%;
	}

	.text-muted a {
		color: #989898;
	}

	.text-muted a:hover {
		color: grey;
	}

	.fa a:active, a:hover, a:visited {
		text-decoration: none;
	}

	/* Tool Colors */
	.general {
		background-color: #0095af;
	}

	.general .card-title {
		color: #f3f3f3;
	}

	.general .card-text {
		color: #f3f3f3;
	}

	.card-img {
		display: block;
		width: 100%;
		height: auto;
	}

	.overlay {
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		height: 100%;
		width: 100%;
		opacity: 0;
		transition: .2s ease;
		background-color: #008CBA;
	}

	.card-pin:hover .overlay {
		opacity: .5;
		border: 5px solid #f3f3f3;
		transition: ease .5s;
		background-color: #000000;
		cursor: zoom-in;
	}

	.more {
		color: white;
		font-size: 20px;
		position: absolute;
		bottom: 0;
		right: 0;
		text-transform: uppercase;
		transform: translate(-20%, -20%);
		-ms-transform: translate(-50%, -50%);
	}

	.download {
		color: white;
		font-size: 20px;
		position: absolute;
		bottom: 0;
		left: 0;
		margin-left: 20px;
		text-transform: uppercase;
		transform: translate(-20%, -20%);
		-ms-transform: translate(-50%, -50%);
	}

	.card-pin:hover .card-title {
		color: #ffffff;
		margin-top: 10px;
		text-align: center;
		font-size: 1.2em;
	}

	.card-pin:hover .more a {
		text-decoration: none;
		color: #ffffff;
	}

	.card-pin:hover .download a {
		text-decoration: none;
		color: #ffffff;
	}

	.social {
		position: relative;
		transform: translateY(-50%);
	}

	.social .fa {
		margin: 0 3px;
	}


	#post-image-holder img {

		width: 270px !important;
		height: auto !important;
		margin-right: 5px;
		padding-left: 13px;
	}
	html{
		font-size: 13px;
	}

</style>

		<div class="container">
			<div class="row">
                <div class="col-md-12">
					<h3 class="panel-title small-heading">
						{{ trans('locations') }}
					</h3>

					
					@if($mode != "eventlist")
						{{--<div class="timeline-posts">--}}
						<div class="infinite-scroll">
							<div class="card-columns">

							@if($locations->count() > 0)
								@foreach($locations as $location)
									<?php
										$timeline=$location->timeline()->first();
										$post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
										$media=\App\Media::where('id',$timeline->avatar_id)->first();
                                        $city=null;$state=null;
                                        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
                                        if($timeline->state_id){
                                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
                                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
                                        }
									?>

									{!! Theme::partial('location',compact('post','location','timeline','media','city','state','country','category_options','country_options','next_page_url')) !!}
									{!! Theme::asset()->container('footer')->usePath()->add('lightbox', 'js/lightbox.min.js') !!}

									@endforeach
								{{$locations->render()}}
							@else
								<div class="no-posts alert alert-warning">{{ trans('common.no_posts') }}</div>
							@endif
						</div>
							<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>
							<script src="{{asset('themes/default/assets/js/popup/jquery-ui-complete.min.js')}}"></script>
							{{--<script src="{{asset('themes/default/assets/js/popup/jquery.ui.touch-punch.min.js')}}"></script>--}}
							<script src="{{asset('themes/default/assets/js/popup/jquery.jspanel.min.js')}}"></script>
							<script src="{{asset('themes/default/assets/select1/jquery.typeahead.js')}}"></script>

						</div>
					@else
						{!! Theme::partial('eventslist',compact('user_events','username')) !!}
					@endif
				</div><!-- /col-md-6 -->
		</div>
		</div>
	<!-- </div> -->




<!-- /main-section -->




<script type="text/javascript">

    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                jQuery("time.timeago").timeago();
            }
        });
    });

    $('.popup').click(function(){
			var width=$(window).width();
			var height=$(window).height();
            $.jsPanel({
                iframe: {
					width:width,
                    height:height,
                    src:    $(this).data('content')+'#post',
                    name:   'myFrame',
                    style:  {'border': '10px solid transparent'}
                },
                size:     'auto',
                position: {left: 0, top: 0},
                theme:    'black',
            });
    });

</script>

