<!-- main-section -->
	<!-- <div class="main-content"> -->
{{--<link media="all" type="text/css" rel="stylesheet" href="{{asset('/themes/default/assets/css/bootstrap.min.css')}}">--}}
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery.jspanel.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/default/assets/select1/jquery.typeahead.css')}}">
<style type="text/css">

	.card {
		position: relative;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-direction: column;
		flex-direction: column;
		min-width: 0;
		word-wrap: break-word;
		background-color: #fff;
		background-clip: border-box;
		border-radius: .25rem
	}

	.card-body {
		-ms-flex: 1 1 auto;
		flex: 1 1 auto;
		padding: 1.25rem
	}

	.card-title {
		margin-bottom: .75rem
	}

	.card-subtitle {
		margin-top: -.375rem;
		margin-bottom: 0
	}

	.card-text:last-child {
		margin-bottom: 0
	}

	.card-link:hover {
		text-decoration: none
	}

	.card-link + .card-link {
		margin-left: 1.25rem
	}

	.card > .list-group:first-child .list-group-item:first-child {
		border-top-left-radius: .25rem;
		border-top-right-radius: .25rem
	}

	.card > .list-group:last-child .list-group-item:last-child {
		border-bottom-right-radius: .25rem;
		border-bottom-left-radius: .25rem
	}

	.card-header {
		padding: .75rem 1.25rem;
		margin-bottom: 0;
		background-color: rgba(0, 0, 0, .03);
		border-bottom: 1px solid rgba(0, 0, 0, .125)
	}

	.card-header:first-child {
		border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
	}

	.card-footer {
		padding: .75rem 1.25rem;
		background-color: rgba(0, 0, 0, .03);
		border-top: 1px solid rgba(0, 0, 0, .125)
	}

	.card-footer:last-child {
		border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
	}

	.card-header-tabs {
		margin-right: -.625rem;
		margin-bottom: -.75rem;
		margin-left: -.625rem;
		border-bottom: 0
	}

	.card-header-pills {
		margin-right: -.625rem;
		margin-left: -.625rem
	}

	.card-img-overlay {
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		padding: 1.25rem
	}

	.card-img {
		width: 100%;
		border-radius: calc(.25rem - 1px)
	}

	.card-img-top {
		width: 100%;
		border-top-left-radius: calc(.25rem - 1px);
		border-top-right-radius: calc(.25rem - 1px)
	}

	.card-img-bottom {
		width: 100%;
		border-bottom-right-radius: calc(.25rem - 1px);
		border-bottom-left-radius: calc(.25rem - 1px)
	}


	.panel-post .panel-footer.socialite .footer-list li{
		padding-left: 10px;
		padding-right: 10px;
	}

	.card-columns .card {
		margin-bottom: .75rem
	}



	@media (min-width: 576px) {
		.card-columns {
			-webkit-column-count: 3;
			column-count: 3;
			-webkit-column-gap: 1.25rem;
			column-gap: 1.25rem
		}

		.card-columns .card {
			display: inline-block;
			width: 100%
		}
	}

	#post-image-holder img {

		width: 270px !important;
		height: auto !important;
		margin-right: 5px;
		padding-left: 13px;
	}

	html{
		font-size: 13px;
	}

</style>

		<div class="container">
			<div class="row">
                <div class="col-md-12">
					<h3 class="panel-title small-heading">
						{{ trans('locations') }}
					</h3>
			   		@if (Session::has('message'))
				        <div class="alert alert-{{ Session::get('status') }}" role="alert">
				            {!! Session::get('message') !!}
				        </div>
				    @endif
					@if(isset($active_announcement))
						<div class="announcement alert alert-info">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<h3>{{ $active_announcement->title }}</h3>
							<p>{{ $active_announcement->description }}</p>
						</div>
					@endif
					
					@if($mode != "eventlist")
						{{--<div class="timeline-posts">--}}
						<div class="infinite-scroll">
							<div class="card-columns">
							@if($locations->count() > 0)
								@foreach($locations as $location)

									<?php
										$timeline=$location->timeline()->first();
										$post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
										$media=\App\Media::where('id',$timeline->avatar_id)->first();
                                        $city=null;$state=null;
                                        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
                                        if($timeline->state_id){
                                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
                                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
                                        }
									?>

									{!! Theme::partial('location',compact('post','location','timeline','media','city','state','country','category_options','country_options','next_page_url')) !!}
									{!! Theme::asset()->container('footer')->usePath()->add('lightbox', 'js/lightbox.min.js') !!}

									@endforeach
								{{$locations->render()}}
							@else
								<div class="no-posts alert alert-warning">{{ trans('common.no_posts') }}</div>
							@endif
						</div>
							<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>
							<script src="{{asset('themes/default/assets/js/popup/jquery-ui-complete.min.js')}}"></script>
							{{--<script src="{{asset('themes/default/assets/js/popup/jquery.ui.touch-punch.min.js')}}"></script>--}}
							<script src="{{asset('themes/default/assets/js/popup/jquery.jspanel.min.js')}}"></script>
							<script src="{{asset('themes/default/assets/select1/jquery.typeahead.js')}}"></script>

						</div>
					@else
						{!! Theme::partial('eventslist',compact('user_events','username')) !!}
					@endif
				</div><!-- /col-md-6 -->
		</div>
		</div>
	<!-- </div> -->




<!-- /main-section -->




<script type="text/javascript">

    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
                jQuery("time.timeago").timeago();
            }
        });
    });

    $('.popup').click(function(){
			var width=$(window).width();
			var height=$(window).height();
            $.jsPanel({
                iframe: {
					width:width,
                    height:height,
                    src:    $(this).data('content')+'#post',
                    name:   'myFrame',
                    style:  {'border': '10px solid transparent'}
                },
                size:     'auto',
                position: {left: 0, top: 0},
                theme:    'black',
            });
    });

</script>

