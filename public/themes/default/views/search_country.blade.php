<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{asset('themes/default/assets/select1/jquery.typeahead.css')}}">

    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <!--script src="../dist/jquery.typeahead.min.js"></script-->
    <script src="{{asset('themes/default/assets/select1/jquery.typeahead.js')}}"></script>

</head>
<body>

<div style="width: 100%; max-width: 800px; margin: 0 auto;">

    <h1>Country Demo</h1>

<div class="panel">
    <div class="panel panel-body">


    <input type="hidden" id="username" name="username" value="{{Auth::user()->name}}">
        <div class="js-result-container"></div>


            <div class="typeahead__container">
                <div class="typeahead__field">
                    <div class="typeahead__query">
                        <input class="js-typeahead"
                               name="q"
                               type="search"
                               autofocus
                               autocomplete="off">
                    </div>
                    <div class="typeahead__button">
                        <button type="submit"><i class="fa fa-map-marker"></i>

                        </button>
                    </div>
                </div>
            </div>

    </div>

</div>




</div>

</body>
</html>
