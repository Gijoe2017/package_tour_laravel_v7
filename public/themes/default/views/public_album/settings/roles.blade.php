<!-- <div class="main-content"> -->
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="post-filters">
				{!! Theme::partial('public-album-menu-settings',compact('timeline')) !!}
			</div>
		</div>
		<div class="col-md-8">						
			<div class="post-filters">
				<div class="panel panel-default">
				@include('flash::message')
					<div class="panel-heading">
						<h3 class="panel-title">{{ trans('common.manage_roles') }}</h3>
					</div>
					<div class="panel-body">						
						<div class="holder">
							<p>{{ trans('messages.manage_roles_text') }}</p>
						</div>
						@if(count($public_album_members) > 0)
						<ul class="list-group page-likes">
							
							@foreach($public_album_members as $public_album_member)
							<li class="list-group-item holder">
								<div class="connect-list">
									<div class="connect-link side-left">
										<div class="follower">
											<a href="{{ url(url($public_album_member->username)) }}">
												<img src="{{ $public_album_member->avatar }}" alt="{{ $public_album_member->name }}" class="img-icon img-30" title="{{ $public_album_member->name }}">
												{{ $public_album_member->name }}
											</a>
										</div>
									</div>
									@if($page->is_admin(Auth::user()->id))
									<div class="side-right follow-links">
										<div class="row">	

											<form class="margin-right" method="POST" action="{{ url('/member/updatepublic-album-role/') }}">
												{{ csrf_field() }}

												<div class="col-md-5 col-sm-5 col-xs-5 padding-5">
													{{ Form::select('member_role', $roles, $public_album_member->pivot->role_id , array('class' => 'form-control')) }}
												</div>

												{{ Form::hidden('user_id', $public_album_member->id) }}
												{{ Form::hidden('public_album_id', $public_album->id) }}

												<div class="col-md-3 col-sm-3 col-xs-3 padding-5">
													{{ Form::submit(trans('common.assign'), array('class' => 'btn btn-to-follow btn-default')) }}
												</div>

												<div class="col-md-4 col-sm-4 col-xs-4 padding-5">
													<a href="#" class="btn btn-to-follow btn-default remove-pagemember remove" data-user-id="{{ $public_album_member->id }} - {{ $public_album->id }}">
														<i class="fa fa-trash"></i> {{ trans('common.remove') }} 
													</a>
												</div>
											</form>	

											
										</div>
									</div>
									@endif
									<div class="clearfix"></div>
								</div>
							</li>
							@endforeach
						</ul>
						@else
						<div class="alert alert-warning">
							{{ trans('messages.no_members_to_admin') }}
						</div>
						@endif	
						
					</div>
				</div><!-- /panel -->
			</div>
		</div><!-- /col-md-8 -->
	</div>
	</div><!-- /container -->