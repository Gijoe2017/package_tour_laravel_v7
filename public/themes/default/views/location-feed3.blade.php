<!-- main-section -->
	<!-- <div class="main-content"> -->
<link media="all" type="text/css" rel="stylesheet" href="{{asset('/themes/default/assets/bootstrap.min.css')}}">
{{--<link rel="stylesheet" href="{{asset('themes/default/assets/js/popupiframe/responsive.min.css')}}"/>--}}
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery-ui.complete.min.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/default/assets/js/popup/jquery.jspanel.css')}}"/>

{{--<link rel="stylesheet" href="{{asset('themes/default/assets/select2/css/select2.css')}}"/>--}}


		<div class="container">
			<div class="row">

                <div class="col-md-12">
					<h3 class="panel-title small-heading">
						{{ trans('locations') }}
					</h3>
			   		@if (Session::has('message'))
				        <div class="alert alert-{{ Session::get('status') }}" role="alert">
				            {!! Session::get('message') !!}
				        </div>
				    @endif

					@if(isset($active_announcement))
						<div class="announcement alert alert-info">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<h3>{{ $active_announcement->title }}</h3>
							<p>{{ $active_announcement->description }}</p>
						</div>
					@endif
					
					@if($mode != "eventlist")
						{{--<div class="timeline-posts">--}}
						<div class="infinite-scroll">
							<div class="card-columns">
							@if($locations->count() > 0)
								@foreach($locations as $location)
									<?php
										$timeline=$location->timeline()->first();
										$post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
										$media=\App\Media::where('id',$timeline->avatar_id)->first();
                                        $city=null;$state=null;
                                        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
                                        if($timeline->state_id){
                                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
                                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
                                        }
									?>
									{!! Theme::partial('location',compact('post','location','timeline','media','city','state','country','next_page_url')) !!}
								@endforeach
								{{$locations->render()}}
							@else
								<div class="no-posts alert alert-warning">{{ trans('common.no_posts') }}</div>
							@endif
						</div>
						</div>
					@else
						{!! Theme::partial('eventslist',compact('user_events','username')) !!}
					@endif
				</div><!-- /col-md-6 -->
		</div>
		</div>
	<!-- </div> -->

<!-- /main-section -->

<script src="{{asset('themes/default/assets/js/jquery.jscroll.min.js')}}"></script>
{{--<script src="{{asset('themes/default/assets/js/popupiframe/jquery-2.1.4.min.js')}}"></script>--}}
{{--<script src="{{asset('themes/default/assets/js/popupiframe/responsive.min.js')}}"></script>--}}
<script src="{{asset('themes/default/assets/js/popup/jquery-ui-complete.min.js')}}"></script>
<script src="{{asset('themes/default/assets/js/popup/jquery.ui.touch-punch.min.js')}}"></script>
<script src="{{asset('themes/default/assets/js/popup/jquery.jspanel.min.js')}}"></script>

{{--<script src="{{asset('themes/default/assets/select2/js/select2.js')}}"></script>--}}

<script type="text/javascript">
    $('#location_id').select2({
        placeholder: "  {{trans('Llocation.TypeAndChooseLocationName')}}",
        minimumInputLength: 2,
        ajax: {
            url: '/ajax/location/find',
            dataType: 'json',
            data: function (params) {

                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {

                return {
                    results: data
                };
            },

            cache: true
        }
    });

    $('#location_id').on('change',function () {
        if($(this).val()){
            alert($(this).val());
            $.ajax({
                type:'get',
                url:'{{url('data')}}',
                data:{'location_id':$(this).val()},
                success:function(data){

                }
            });
        }

    });


    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    $('.popup').click(function(){

			var width=$(window).width();
			var height=$(window).height();
            $.jsPanel({
                iframe: {
					width:width,
                    height:height,
                    src:    $(this).data('content')+'#post',
                    name:   'myFrame',
                    style:  {'border': '10px solid transparent'}
                },
                size:     'auto',
                position: {left: 0, top: 0},
                theme:    'black',
            });
    });


    {{--$('#LocationName').select2({--}}
        {{--placeholder: "  {{trans('Common.Location')}}",--}}
        {{--minimumInputLength: 2,--}}
        {{--ajax: {--}}
            {{--url: '/location/find',--}}
            {{--dataType: 'json',--}}
            {{--data: function (params) {--}}
                {{--return {--}}
                    {{--q: $.trim(params.term)--}}
                {{--};--}}
            {{--},--}}
            {{--processResults: function (data) {--}}
                {{--return {--}}
                    {{--results: data--}}
                {{--};--}}
            {{--},--}}

            {{--cache: true--}}
        {{--}--}}
    {{--});--}}


</script>

