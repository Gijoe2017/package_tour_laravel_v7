<!-- <div class="main-content"> -->

<link rel="stylesheet" href="{{asset('themes/default/assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('themes/default/assets/css/jquery-ui/jquery-ui-1.8.16.custom.css')}}">

<!-- google maps -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyART_vyxkyKSwsEJPKAsJRybQnBfm3DjgA"></script>

{{--<script type="text/javascript" src="{{asset('themes/default/assets/js/jquery.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('themes/default/assets/js/jquery-ui.min.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('themes/default/assets/js/gmaps.js')}}"></script>

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="post-filters">
					{!! Theme::partial('location-menu-settings',compact('timeline')) !!}
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('common.map')}}
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
							@include('flash::message')   
							
							<form action="{{ url('/'.$username.'/location-settings/maps')}}" method="POST">
								{{ csrf_field() }}



								<Div class="form-group">
									<input id='gmaps-input-address' class="form-control" placeholder='Start typing a place name...' type='text' />
								</Div>
								<div class="row">
									<div class="col-lg-6">
										Latitude: <span id='gmaps-output-latitude'></span>
									</div>
									<div class="col-lg-6">
										Longitude: <span id='gmaps-output-longitude'></span>
									</div>
									<div id='gmaps-error'></div>
								</div>



								<BR>
								<div id='gmaps-canvas'></div>
								<BR><BR>
								<div class="row">




									<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::label('latitude', trans('common.latitude'), ['class' => 'control-label']) }}
											{{ Form::text('latitude', $timeline->latitude, ['class' => 'form-control', 'placeholder' => trans('common.latitude')]) }}
										</fieldset></div>
									<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::label('longitude', trans('common.longitude'), ['class' => 'control-label']) }}
											{{ Form::text('longitude', $timeline->longitude, ['class' => 'form-control', 'placeholder' => trans('common.longitude')]) }}
										</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::select('map_verified', array('' => trans('common.select_verify')) + ['Y'=>'Map Verified','C'=>'Map Checking','N'=>'Map Not Verified'], $timeline->map_verified, array('class' => 'form-control')) }}
										</fieldset>
									</div>
								</div>
<hr>
								<div class="pull-right">
									{{ Form::submit(trans('common.update_location_map'), ['class' => 'btn btn-success']) }}
								</div>
								<div class="clearfix"></div>

							{{ Form::close() }}




						</div><!-- /socialite-form -->
					</div>
				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div>







