<!-- <div class="main-content"> -->

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="post-filters">
					{!! Theme::partial('location-menu-settings',compact('timeline')) !!}
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('common.general_settings')}}
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
							@include('flash::message')   
							
							<form action="{{ url('/'.$username.'/location-settings/general')}}" method="POST">
								{{ csrf_field() }}
								<h3 class="panel-title">
									{{ trans('common.category')}}
								</h3>
								<hr>
								<fieldset class="form-group required {{ $errors->has('category_id') ? ' has-error' : '' }}">
									{{ Form::label('category_id', trans('common.category'), ['class' => 'control-label']) }}
									{{ Form::select('category_id', array('' => trans('common.select_category'))+ $category_options, $location->category_id, array('class' => 'form-control')) }}
									@if ($errors->has('category_id'))
										<span class="help-block">
										{{ $errors->first('category_id') }}
										</span>
									@endif
								</fieldset>

								<div class="row">
									<div class="col-md-6 category_sub1">
									<fieldset class="form-group required {{ $errors->has('category_sub1_id') ? ' has-error' : '' }}">
										{{ Form::label('category_sub1_id', trans('common.category_sub1'), ['class' => 'control-label']) }}
										{{ Form::select('category_sub1_id', array('' => trans('common.select_category_sub1')) + $category_sub1_options, $location->category_sub1_id, array('class' => 'form-control')) }}
										@if ($errors->has('category_sub1_id'))
											<span class="help-block">
											{{ $errors->first('category_sub1_id') }}
											</span>
										@endif
									</fieldset>
									</div>

									<div class="col-md-6 category_sub2">
										<fieldset class="form-group required {{ $errors->has('category_sub2_id') ? ' has-error' : '' }}">
											{{ Form::label('category_sub2_id', trans('common.category_sub2'), ['class' => 'control-label']) }}
											{{ Form::select('category_sub2_id', array('' => trans('common.select_category_sub2')) + $category_sub2_options, $location->category_sub2_id, array('class' => 'form-control')) }}
											@if ($errors->has('category_sub2_id'))
												<span class="help-block">
											{{ $errors->first('category_sub2_id') }}
											</span>
											@endif
										</fieldset>
									</div>

								</div>

								<h3 class="panel-title">
									{{ trans('common.general_settings')}}
								</h3>
									<hr>
								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::label('username', trans('common.username'), ['class' => 'control-label']) }} 
											{{ Form::text('username', $timeline->username, ['class' => 'form-control', 'placeholder' => trans('common.username'), 'disabled' => 'disabled']) }}
											{{ Form::hidden('username', $timeline->username) }}
										</fieldset>
									</div>
									<div class="col-md-6">
										<div class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
											{{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
											{{ Form::text('name', $timeline->name, ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_location')]) }}
											@if ($errors->has('name'))
											<span class="help-block">
												{{ $errors->first('name') }}
											</span>
											@endif
										</div>
									</div>
								</div>

								<fieldset class="form-group text-area-form">
									{{ Form::label('about', trans('common.about'), ['class' => 'control-label']) }}
									{{ Form::textarea('about', $timeline->about, ['class' => 'form-control', 'placeholder' => trans('messages.create_location_placeholder'), 'rows' => '2', 'cols' => '20'])}}
								</fieldset>

								<h3 class="panel-title">
									{{ trans('common.address')}}
								</h3>
								<hr>
									<fieldset class="form-group">
										{{ Form::label('address', trans('common.address'), ['class' => 'control-label']) }}
										{{ Form::text('address', $timeline->locations->address, ['class' => 'form-control', 'placeholder' => trans('common.address_of_your_location'), 'rows' => '5'])}}
									</fieldset>


								<div class="row">
									<div class="col-md-4">
										<fieldset class="form-group required {{ $errors->has('country_id') ? ' has-error' : '' }}">
											{{ Form::label('country_id', trans('common.country'), ['class' => 'control-label']) }}
											{{ Form::select('country_id', array('' => trans('common.select_country'))+ $country_options,$timeline->country_id, array('class' => 'form-control')) }}
											@if ($errors->has('country_id'))
												<span class="help-block">
												{{ $errors->first('country_id') }}
												</span>
											@endif
										</fieldset>
									</div>
									<div class="col-md-4 state_id">
										<fieldset class="form-group required {{ $errors->has('state_id') ? ' has-error' : '' }}">
											{{ Form::label('state_id', trans('common.state'), ['class' => 'control-label']) }}
											{{ Form::select('state_id', array('' => trans('common.select_state')) + $state_options, $timeline->state_id, array('class' => 'form-control')) }}
											@if ($errors->has('state_id'))
												<span class="help-block">
												{{ $errors->first('state_id') }}
												</span>
											@endif
										</fieldset>
									</div>
									<div class="col-md-4 city_id">
										<fieldset class="form-group">
											{{ Form::label('city_id', trans('common.city'), ['class' => 'control-label']) }}
											{{ Form::select('city_id', array('' => trans('common.select_city')) + $city_options, $timeline->city_id, array('class' => 'form-control')) }}
										</fieldset>
									</div>
								</div>

								@if(count($city_sub_options))

									<div class="row city_sub1_id">
										<div class="col-md-6">
											<fieldset class="form-group">
												{{ Form::label('city_sub1_id', trans('common.city_sub'), ['class' => 'control-label']) }}
												{{ Form::select('city_sub1_id', array('' => trans('common.select_city_sub')) + $city_sub_options, $timeline->city_sub1_id, array('class' => 'form-control')) }}
											</fieldset>
										</div>
										<div class="col-md-6">
											<fieldset class="form-group">
												{{ Form::label('zip_code', trans('common.zip_code'), ['class' => 'control-label']) }}
												{{ Form::text('zip_code', $zipcode, ['class' => 'form-control','readonly'=>true, 'placeholder' => trans('common.zip_code')]) }}
											</fieldset>
										</div>
									</div>
								@endif

								<h3 class="panel-title">
									{{ trans('common.contact')}}
								</h3>
								<hr>
								<div class="row">

									<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::label('phone', trans('common.phone'), ['class' => 'control-label']) }}
											{{ Form::number('phone', $timeline->locations->phone, ['class' => 'form-control', 'placeholder' => trans('common.phone')]) }}
										</fieldset></div>
										<div class="col-md-6">
										<fieldset class="form-group">
											{{ Form::label('website', trans('common.website'), ['class' => 'control-label']) }}
											{{ Form::text('website', $timeline->locations->website, ['class' => 'form-control', 'placeholder' => trans('common.website')]) }}
										</fieldset>
									</div>
								</div>

								<div class="pull-right">
									{{ Form::submit(trans('common.update_location'), ['class' => 'btn btn-success']) }}
								</div>
								<div class="clearfix"></div>

							{{ Form::close() }}




						</div><!-- /socialite-form -->
					</div>
				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div>

