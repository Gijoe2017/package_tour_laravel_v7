

<link rel=stylesheet href={{asset('themes/default/assets/waterfall/styles/main.css')}}>




<div class=container>
    <h1>Location</h1>
    <div class=waterfall>

    </div>
</div>

<script src={{asset('themes/default/assets/waterfall/scripts/vendor.js')}}></script>
<script src={{asset('themes/default/assets/waterfall/bootstrap-waterfall.js')}}></script>


<script id=waterfall-template type=text/template>

    @if($locations->count() > 0)
    @foreach($locations as $location)
    <?php
    $timeline=$location->timeline()->first();
    $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
    $media=\App\Media::where('id',$timeline->avatar_id)->first();
    $city=null;$state=null;
    $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
    if($timeline->state_id){
        $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
        $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
    }
    ?>
    <ul class="list-group">
        <li class="list-group-item">
            {!! Theme::partial('location',compact('post','location','timeline','media','city','state','country','next_page_url')) !!}
        </li>
    </ul>
    @endforeach
    {{--{{$locations->render()}}--}}
    @endif


</script>


<script id=another-template type=text/template>
    @if($locations2->count() > 0)
        @foreach($locations2 as $location)
        <?php
        $timeline=$location->timeline()->first();
        $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
        $media=\App\Media::where('id',$timeline->avatar_id)->first();
        $city=null;$state=null;
        $country=$timeline->country()->where('language_code',Auth::user()->language)->first();
        if($timeline->state_id){
            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Auth::user()->language)->first();
            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Auth::user()->language)->first();
        }
        ?>
        <ul class="list-group">
            <li class="list-group-item">
            {!! Theme::partial('location',compact('post','location','timeline','media','city','state','country','next_page_url')) !!}
            </li>
        </ul>
        @endforeach
    {{$locations->render()}}
    @endif


</script>

<script>
    $('.waterfall')

        .data('bootstrap-waterfall-template', $('#waterfall-template').html())
        .on('finishing.mystist.waterfall', function () {
            setTimeout(function () { // simulate ajax
                $('.waterfall').data('mystist.waterfall').addPins($($('#another-template').html()))
            }, 10);
        })
        .waterfall()
</script>
