<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="user-scalable=0">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Vertical Line</title>
    {{--<link rel="stylesheet" href="{{asset('themes/default/assets/waterfall2/common/css/style.css')}}">--}}
    {{--<style>--}}
        {{--.item-move {--}}
            {{--transition: all .5s cubic-bezier(.55,0,.1,1);--}}
            {{---webkit-transition: all .5s cubic-bezier(.55,0,.1,1);--}}
        {{--}--}}
    {{--</style>--}}
    <script src="{{asset('themes/default/assets/waterfall2/common/js/item-factory.js')}}"></script>
    <link rel="stylesheet" href="{{asset('themes/default/assets/waterfall2/common/css/style.css')}}">
</head>
<body>
<div id="app">
    <waterfall
            :align="align"
            :line-gap="200"
            :min-line-gap="100"
            :max-line-gap="220"
            :single-max-width="300"
            :watch="items"
    @reflowed="reflowed"
    ref="waterfall"
    >
    <!-- each component is wrapped by a waterfall slot -->
    <waterfall-slot
            v-for="(item, index) in items"
            :width="item.width"
            :height="item.height"
            :order="index"
            :key="item.index"
            move-class="item-move"
    >
        <div class="item" :style="item.style" :index="item.index"></div>
    </waterfall-slot>
    </waterfall>
</div>
<script src="{{asset('themes/default/assets/waterfall2/common/js/vue.min.js')}}"></script>
<script src="{{asset('themes/default/assets/waterfall2/common/js/vue-waterfall.min.js')}}"></script>
{{--<script src="{{asset('themes/default/assets/waterfall2/common/js/item-factory.js')}}"></script>--}}
<script>

    var app = new Vue({
        el: '#app',
        components: {
            'waterfall': Waterfall.waterfall,
            'waterfall-slot': Waterfall.waterfallSlot
        },
        data: {
            align: 'center',
            items: ItemFactory.get(6),
            isBusy: false
        },
        methods: {
            addItems: function () {
                if (!this.isBusy && this.items.length < 500) {
                    this.isBusy = true
                    this.items.push.apply(this.items, ItemFactory.get(6))
                }
            },
            shuffle: function () {
                this.items.sort(function () {
                    return Math.random() - 0.5
                })
            },
            reflowed: function () {
                this.isBusy = false
            }
        }
    })

    document.body.addEventListener('click', function () {
        app.shuffle()
        // app.$refs.waterfall.$emit('reflow') // manually trigger reflow action
    }, false)

    window.addEventListener('scroll', function () {
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop
        if (scrollTop + window.innerHeight >= document.body.clientHeight) {
            app.addItems()
        }
    })

</script>
</body>
</html>
