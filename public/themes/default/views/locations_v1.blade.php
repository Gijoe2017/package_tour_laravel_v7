<!-- <div class="main-content"> -->
<div class="container">
	<div class="row">

		<!-- List of user locations-->

		<div class="col-md-12">
			<div class="post-filters locations-groups">

				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">

						<h3 class="panel-title small-heading">
							{{ trans('locations') }}
						</h3>

					</div>
					<div class="panel-body">

						@if($locations->count() > 0)
							@foreach($locations as $location)
								<?php
									$timeline=$location->timeline()->first();
                                    $media=\App\Timeline::where('id',$location->timeline_id)->first();
									$post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();
echo $location->timeline_id;
								?>
							{{dd($post)}}
							<div class="col-md-4">
							@if (Session::has('message'))
								<div class="alert alert-{{ Session::get('status') }}" role="alert">
									{!! Session::get('message') !!}
								</div>
							@endif
							<div class="panel panel-default panel-post animated" id="post{{ $location->id }}">
									<div class="panel-heading no-bg">
										<div class="post-author">
											<div class="post-options">
												<ul class="list-inline no-margin">
													<li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
														<ul class="dropdown-menu">

															<li class="main-link hidden">
																<a href="#" data-post-id="{{ $location->id }}" class="notify-user unnotify">
																	<i class="fa  fa-bell-slash" aria-hidden="true"></i>{{ trans('common.stop_notifications') }}
																	<span class="small-text">{{ trans('messages.stop_notification_text') }}</span>
																</a>
															</li>
															<li class="main-link">
																<a href="#" data-post-id="{{ $location->id }}" class="notify-user notify">
																	<i class="fa fa-bell" aria-hidden="true"></i>{{ trans('common.get_notifications') }}
																	<span class="small-text">{{ trans('messages.get_notification_text') }}</span>
																</a>
															</li>

															@if($location->timeline_id == Auth::user()->timeline_id)
																<li class="main-link">
																	<a href="#" class="delete-post" data-post-id="{{ $location->id }}">
																		<i class="fa fa-trash" aria-hidden="true"></i>{{ trans('common.delete') }}
																		<span class="small-text">{{ trans('messages.delete_text') }}</span>
																	</a>
																</li>
															@endif

															@if(Auth::user()->timeline_id != $location->timeline_id)
																<li class="main-link">
																	<a href="#" class="hide-post" data-post-id="{{ $location->id }}">
																		<i class="fa fa-eye-slash" aria-hidden="true"></i>{{ trans('common.hide_notifications') }}
																		<span class="small-text">{{ trans('messages.hide_notification_text') }}</span>
																	</a>
																</li>

																<li class="main-link">
																	<a href="#" class="save-post" data-post-id="{{ $location->id }}">
																		<i class="fa fa-save" aria-hidden="true"></i>
																		@if(!Auth::user()->postsSaved->contains($location->id))
																			{{ trans('common.save_post') }}
																			<span class="small-text">{{ trans('messages.post_save_text') }}</span>
																		@else
																			{{ trans('common.unsave_post') }}
																			<span class="small-text">{{ trans('messages.post_unsave_text') }}</span>
																		@endif
																	</a>
																</li>

																<li class="main-link">
																	<a href="#" class="manage-report report" data-post-id="{{ $location->id }}">
																		<i class="fa fa-flag" aria-hidden="true"></i>{{ trans('common.report') }}
																		<span class="small-text">{{ trans('messages.report_text') }}</span>
																	</a>
																</li>
															@endif
															<li class="divider"></li>

															<li class="main-link">
																<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/share-post/'.$location->id)) }}" class="fb-xfbml-parse-ignore" target="_blank"><i class="fa fa-facebook-square"></i>Facebook {{ trans('common.share') }}</a>
															</li>

															<li class="main-link">
																<a href="https://twitter.com/intent/tweet?text={{ url('/share-post/'.$location->id) }}"target="_blank"><i class="fa fa-twitter-square"></i>Twitter {{ trans('common.tweet') }}</a>
															</li>

															<li class="main-link">
																<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-share-alt"></i>Embed {{ trans('common.post') }}</a>
															</li>

														</ul>

													</li>

												</ul>
											</div>

											<div class="user-avatar">
												@if($timeline->avatar_id>0)
													<a href="{{ url($location->timeline->name) }}"  ><img src="{{ url('location/avatar/x-small/'.$media->avatar()->first()->source) }}"  title="{{ $location->timeline->name }}" alt="{{ $location->timeline->name }}"></a>
												@else
													<img src="{{ url('location/avatar/default-location-avatar.png') }}"  title="{{ $location->timeline->name }}" alt="{{ $location->timeline->name }}">
												@endif
											</div>
											<div class="user-post-details">
												<ul class="list-unstyled no-margin">
													<li>
														<a href="{{ url($location->timeline->username) }}" title="{{ '@'.$location->timeline->username }}" data-toggle="tooltip" data-placement="top" class="user-name user">
															{{ $location->timeline->name }}
														</a>
														<div class="small-text">
															@if(isset($timeline))
																@if($timeline->type != 'event' && $timeline->type != 'page'  && $timeline->type != 'location' && $timeline->type != 'public_album' && $timeline->type != 'group')
																	@if($post->timeline->type == 'page' || $post->timeline->type == 'group' || $post->timeline->type == 'user' || $post->timeline->type == 'location' || $post->timeline->type == 'public_album'|| $post->timeline->type == 'event')
																		@if(Auth::user()->where('id',$post->user->id)->first()->timeline_id != $post->timeline_id)
																			(posted on
																			<a href="{{ url($post->timeline->username) }}">{{ $post->timeline->name }}</a>
																			{{ $post->timeline->type }})

																		@endif
																	@endif
																@endif
															@endif

														</div>
													</li>
													<li>
														@if(isset($sharedOwner))
															<time class="post-time timeago" datetime="{{ $sharedOwner->created_at }}+00:00" title="{{ $sharedOwner->created_at }}+00:00">
																{{ $sharedOwner->created_at }}+00:00
															</time>
														@else
															<time class="post-time timeago" datetime="{{ $location->created_at }}+00:00" title="{{ $location->created_at }}+00:00">
																{{ $location->created_at }}+00:00
															</time>
														@endif

														{{--@if($post->location != NULL && !isset($sharedOwner))--}}
														  {{--{{ trans('common.at') }}--}}
																{{--<span class="post-place">--}}
																  {{--<a target="_blank" href="{{ url('locations'.$post->location) }}">--}}
																	  {{--<i class="fa fa-map-marker"></i> {{ $post->location }}--}}
																  {{--</a>--}}
																{{--</span>--}}

															{{--@endif--}}
													</li>
												</ul>

											</div>
										</div>
									</div>

								<div class="panel-body">
									<div class="text-wrapper">
										<div class="post-image-holder single-image">

											<?php
                                            	$media=$location->posts()->where('active', 1)->orderBy('created_at', 'desc')->first();
												if(!$media){
													$media=\App\Media::where('timeline_id',$location->timeline_id)->first();
												}

												//  $posts = $user->posts()->where('active', 1)->orderBy('created_at', 'desc')->with('comments')->first();
//												$post=\App\Post::where('id',$media->)->first();
											?>

											@if($media->source)
												<a href="{{ url('users/gallery/'.$media->source) }}" data-lightbox="imageGallery.{{ $location->id }}" ><img src="{{ url('users/gallery/mid/'.$media->source) }}"  title="{{ $location->timeline->name }}" alt="{{ $location->timeline->name }}"></a>
											@else
												<img src="{{ url('location/avatar/no-image-full.jpg') }}"  title="{{ $location->timeline->name }}" alt="{{ $location->timeline->name }}">
											@endif
											{{--<img src="{{ url('location/avatar/default-location-avatar.png') }}"  title="{{ $location->timeline->name }}" alt="{{ $location->timeline->name }}">--}}

										</div>
									</div>
								</div>
								</div>
									@if(isset($active_announcement))
										<div class="announcement alert alert-info">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<h3>{{ $active_announcement->title }}</h3>
											<p>{{ $active_announcement->description }}</p>
										</div>
									@endif
								</div>

							@endforeach
					     @endif

					</div>
				</div><!-- /panel -->
			</div>
		</div><!-- /col-md-6 -->

		<!-- List of user groups-->


	</div><!-- /row -->
</div>
<!-- </div> --><!-- /main-content -->