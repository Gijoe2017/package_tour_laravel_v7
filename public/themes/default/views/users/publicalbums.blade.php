<!-- <div class="main-content"> -->
	<div class="container">
		<div class="row">

			<!-- List of user locations-->

			<div class="col-md-6">
				<div class="post-filters locations-groups">
					
					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<div class="side-right create-btn">
								<a href="{{ url(Auth::user()->username.'/create-public-album') }}" class="btn btn-success">{{ trans('common.create_public_album') }}</a>
							</div>
							<h3 class="panel-title small-heading">
								{{ trans('common.public_album') }}
							</h3>
							
						</div>
						<div class="panel-body">
							@if(Auth::user()->own_publicalbums()->count())
								
							<ul class="list-group page-likes">
								@foreach(Auth::user()->own_publicalbums() as $userpublicalbum)
								<li class="list-group-item delete_public_album">
									<div class="connect-list">
										<div class="connect-link side-left">
											<a href="{{ url($userpublicalbum->username) }}">
												<img src=" @if($userpublicalbum->timeline_id && $userpublicalbum->avatar) {{ url('public_album/avatar/small/'.$userpublicalbum->avatar) }} @else {{ url('public_album/avatar/default-public-album-avatar.png') }} @endif" alt="{{ $userpublicalbum->name }}" title="{{ $userpublicalbum->name }}">{{ $userpublicalbum->name }}
											</a>
										</div>
										<div class="side-right">
											<a href="" class="side-right delete-public-album delete_location" data-public-album-delete-id="{{ $userpublicalbum->id }}"><span class="trash-icon bg-danger"><i class="fa fa-trash text-danger"></i></span></a>
										</div>
										<div class="clearfix"></div>
									</div>
								</li>
								@endforeach
							</ul>
							@else
								<div class="alert alert-warning">
									{{ trans('messages.no_public_albums') }}
								</div>
							@endif
						</div>
					</div><!-- /panel -->
				</div>
			</div><!-- /col-md-6 -->
			
			<!-- List of user groups-->
			
			<div class="col-md-6">
				<div class="post-filters locations-groups">
					
					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<h3 class="panel-title small-heading">
								{{ trans('common.joined_publicalbum') }}
							</h3>
						</div>
						<div class="panel-body">
							@if(Auth::user()->joinedLocations()->count())
								
							<ul class="list-group location-likes">
								@foreach(Auth::user()->joinedLocations() as $joinlocation)
								<li class="list-group-item holder">
									<div class="connect-list">
										<div class="connect-link side-left">
											<a href="{{ url($joinlocation->username) }}">
												<img src=" @if($joinlocation->timeline_id && $joinlocation->avatar) {{ url('location/avatar/small/'.$joinlocation->avatar) }} @else {{ url('location/avatar/default-location-avatar.png') }} @endif" alt="{{ $joinlocation->name }}" title="{{ $joinlocation->name }}">{{ $joinlocation->name }}
											</a>
										</div>
										<div class="side-right location-unjoin">
											<div class="left-col">
												<a href="#" class="btn btn-success unjoin-location joined" data-timeline-id="{{ $joinlocation->timeline_id }}">
													<i class="fa fa-check"></i> {{ trans('common.joined') }}
												</a>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</li>
								@endforeach
							</ul>
							@else
								<div class="alert alert-warning">
									{{ trans('messages.no_location') }}
								</div>
							@endif
						</div>
					</div>

				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div>
<!-- </div> --><!-- /main-content -->