<!-- main-section -->
<script src="{{asset('themes/default/assets/smartphoto/js/smartphoto.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('themes/default/assets/smartphoto/css/smartphoto.min.css')}}">
	<div class="container section-container @if($timeline->hide_cover) no-cover @endif">
		<div class="row">
			<div class="col-md-12">
				@if($timeline->type == "user")
					{!! Theme::partial('user-header',compact('user','timeline','liked_pages','joined_groups','followRequests','following_count','followers_count','follow_confirm','user_post','joined_groups_count','guest_events')) !!}
				@elseif($timeline->type == "page")
					{!! Theme::partial('page-header',compact('page','timeline')) !!}
				@elseif($timeline->type == "location") {{--//Narong Add/--}}
					{!! Theme::partial('location-header',compact('location','timeline')) !!}
				@elseif($timeline->type == "public_album")
					{!! Theme::partial('public-album-header',compact('timeline','public_album')) !!}
				@elseif($timeline->type == "group")
					{!! Theme::partial('group-header',compact('timeline','group')) !!}
				@elseif($timeline->type == "event")
					{!! Theme::partial('event-header',compact('event','timeline')) !!}
				@endif
			</div>

		</div>
		<div class="row">				 
			<div class="col-md-12">

				<div class="row">
					<div class="timeline">
					<a name="post"></a>

					<div class="col-md-6">
						@if(($location->timeline_post_privacy == "only_admins" && $location->is_admin(Auth::user()->id)) || ($location->timeline_post_privacy == "everyone"))
							{!! Theme::partial('create-review',compact('timeline','location','user_post','Rating','rate_group','rate_group_title')) !!}
						@elseif($location->timeline_post_privacy == "everyone")
							{!! Theme::partial('create-review',compact('timeline','location','user_post','Rating','rate_group','rate_group_title')) !!}
						@endif
					</div>

					<div class="col-md-6">
						{!! Theme::partial('show-review',compact('timeline','location','user_post','rate_group_title')) !!}
					</div>
					</div>

					<div class="timeline">
						<!-- Post box on timeline,page,group -->
						<div class="col-md-12">
							<div class="timeline-posts">
								@if($user_post == "user" || $user_post == "page" || $user_post == "location" || $user_post == "public_album" || $user_post == "group")
									@if(count($posts) > 0)
	 									@foreach($posts as $post)									
	 										{!! Theme::partial('review',compact('post','timeline','next_page_url','user')) !!}
	 									@endforeach
 									@else
 										<div class="no-posts alert alert-warning">{{ trans('messages.no_posts') }}</div>
 									@endif	
 								@endif

 								@if($user_post == "event")
 									@if($event->type == 'private' && Auth::user()->get_eventuser($event->id) || $event->type == 'public')
 										@if(count($posts) > 0)
		 									@foreach($posts as $post)
		 										{!! Theme::partial('review',compact('post','timeline','next_page_url','user')) !!}
		 									@endforeach
	 									@else
	 										<div class="no-posts alert alert-warning">{{ trans('messages.no_posts') }}</div>
	 									@endif
 									@else
 										<div class="no-posts alert alert-warning">{{ trans('messages.private_posts') }}</div>
 									@endif
 								@endif
							</div>	
						</div>
					</div>
				</div><!-- /row -->
			</div><!-- /col-md-10 -->

			<div class="col-md-2">
				{!! Theme::partial('timeline-rightbar') !!}
			</div>

		</div><!-- /row -->
	</div>

<script language="JavaScript" >
    window.addEventListener('DOMContentLoaded',function(){
        new SmartPhoto(".js-smartPhoto");
    });
</script>