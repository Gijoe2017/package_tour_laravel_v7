<!-- main-section -->

<div class="container">
	<div class="row">
		<div class="col-md-10">

			{!! Theme::partial('location-header',compact('timeline','location')) !!}
			
			<div class="row">
				<div class=" timeline">
					<div class="col-md-4">
						
						{!! Theme::partial('location-leftbar',compact('timeline','location','location_members','country','state','city')) !!}
					</div>

					<div class="col-md-8">
					<div class="panel panel-default">
						@include('flash::message')
							<div class="panel-heading no-bg panel-settings">
								<h3 class="panel-title">
									{{ trans('common.members') }}
								</h3>
							</div>
							<div class="panel-body">								
								@if(count($location_members) > 0)
								<ul class="list-group page-likes">
									@foreach($location_members as $location_member)
									<li class="list-group-item holder">
										<div class="connect-list">
											<div class="connect-link side-left">
												<a href="{{ url($location_member->username) }}">
													<img src="{{ $location_member->avatar }}" alt="{{ $location_member->name }}" class="img-icon img-30" title="{{
														$location_member->name }}">
													{{ $location_member->name }}
												</a>
												@if($location_member->verified)
													<span class="verified-badge bg-success">
									                    <i class="fa fa-check"></i>
									                </span>
									            @endif
											</div>
											@if($location->is_admin(Auth::user()->id))
											<div class="side-right follow-links">
												<div class="row">	

												<form class="margin-right" method="POST" action="{{ url('/member/update-location-role/') }}">
													{{ csrf_field() }}

													<div class="col-md-5 col-sm-5 col-xs-5 padding-5">
														{{ Form::select('member_role', $roles, $location_member->pivot->role_id , array('class' => 'form-control')) }}
													</div>

													{{ Form::hidden('user_id', $location_member->id) }}
													{{ Form::hidden('location_id', $location->id) }}

													<div class="col-md-3 col-sm-3 col-xs-3 padding-5">
														{{ Form::submit(trans('common.assign'), array('class' => 'btn btn-to-follow btn-default')) }}														
													</div>

													<div class="col-md-4 col-sm-4 col-xs-4 padding-5">
														<a href="#" class="btn btn-to-follow btn-default remove-location-member remove" data-user-id="{{ $location_member->id }} - {{ $location->id }}">
															<i class="fa fa-trash"></i> {{ trans('common.remove') }} 
														</a>
													</div>
												</form>	

													
												</div>
											</div>
											@endif
											<div class="clearfix"></div>
										</div>
									</li>
									@endforeach
								</ul>

								@else
								<div class="alert alert-warning">{{ trans('messages.no_members') }}</div>
								@endif
							</div><!-- /panel-body -->
						</div>

					</div><!-- /col-md-8 -->
				</div><!-- /main-content -->
			</div><!-- /row -->
		</div><!-- /col-md-10 -->

		<div class="col-md-2">
			{!! Theme::partial('timeline-rightbar') !!}
		</div>
	</div>
</div><!-- /container -->
