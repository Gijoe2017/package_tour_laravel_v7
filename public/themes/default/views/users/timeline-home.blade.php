<!-- main-section -->
<script src="{{asset('themes/default/assets/smartphoto/js/smartphoto.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('themes/default/assets/smartphoto/css/smartphoto.min.css')}}">

	<div class="container section-container @if($timeline->hide_cover) no-cover @endif">
		<div class="row">
			<div class="col-md-12">

					{!! Theme::partial('location-home-header',compact('location','timeline')) !!}


			</div>

		</div>
		<div class="row">				 
			<div class="col-md-10">
				<div class="row">
					<div class="timeline">
						<div class="col-md-4">

							{!! Theme::partial('location-home-leftbar',compact('timeline','location','location_members','country','city','state')) !!}

						</div>
						<a name="post"></a>

						<!-- Post box on timeline,page,group -->
						<div class="col-md-8">

							@if($timeline->type == "user" && $timeline_post == true)
								{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								
							@elseif($timeline->type == "page")
								@if(($page->timeline_post_privacy == "only_admins" && $page->is_admin(Auth::user()->id)) || ($page->timeline_post_privacy == "everyone"))
									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								@elseif($page->timeline_post_privacy == "everyone")	
									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								@endif
							@elseif($timeline->type == "public_album")

								@if(($public_album->timeline_post_privacy == "only_admins" && $public_album->is_admin(Auth::user()->id)) || ($public_album->timeline_post_privacy == "everyone"))
									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								@elseif($public_album->timeline_post_privacy == "everyone")
									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								@endif

							@elseif($timeline->type == "location"){{--//Narong Add/--}}
								@if(($location->timeline_post_privacy == "only_admins" && $location->is_admin(Auth::user()->id)) || ($location->timeline_post_privacy == "everyone"))
								<a href="{{url('/register')}}">
								{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								</a>
								@elseif($location->timeline_post_privacy == "everyone")

									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}
								@endif
								
							@elseif($timeline->type == "group")						
								@if(($group->post_privacy == "only_admins" && $group->is_admin(Auth::user()->id))|| ($group->post_privacy == "members" && Auth::user()->get_group($group->id) == 'approved') || $group->post_privacy == "everyone")									
									{!! Theme::partial('create-post',compact('timeline','user_post','username')) !!}
								@endif

							@elseif($timeline->type == "event")	
								@if(($event->timeline_post_privacy == 'only_admins' && $event->is_eventadmin(Auth::user()->id, $event->id)) || ($event->timeline_post_privacy == 'only_guests' && Auth::user()->get_eventuser($event->id)))													
									{!! Theme::partial('create-post',compact('timeline','user_post')) !!}								
								@endif
							@endif					

							<div class="timeline-posts">
								@if($user_post == "user" || $user_post == "page" || $user_post == "location" || $user_post == "public_album" || $user_post == "group")

									@if(count($posts) > 0)
	 									@foreach($posts as $post)
	 								
	 										{!! Theme::partial('post-home',compact('post','timeline','next_page_url','user')) !!}
	 									@endforeach
 									@else
 										<div class="no-posts alert alert-warning">{{ trans('messages.no_posts') }}</div>
 									@endif	
 								@endif

 								@if($user_post == "event")
 									@if($event->type == 'private' && Auth::user()->get_eventuser($event->id) || $event->type == 'public')
 										@if(count($posts) > 0)	
		 									@foreach($posts as $post)									
		 										{!! Theme::partial('post-home',compact('post','timeline','next_page_url','user')) !!}
		 									@endforeach
	 									@else
	 										<div class="no-posts alert alert-warning">{{ trans('messages.no_posts') }}</div>
	 									@endif	
 									@else
 										<div class="no-posts alert alert-warning">{{ trans('messages.private_posts') }}</div>
 									@endif	
 								@endif								
							</div>	
						</div>
					</div>
				</div><!-- /row -->
			</div><!-- /col-md-10 -->

			<div class="col-md-2">
				{!! Theme::partial('timeline-rightbar') !!}
			</div>

		</div><!-- /row -->
	</div>

<script language="JavaScript" >
    window.addEventListener('DOMContentLoaded',function(){
        new SmartPhoto(".js-smartPhoto");
    });
</script>