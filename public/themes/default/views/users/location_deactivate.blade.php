<!-- main-section -->
		<!-- <div class="main-content"> -->
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="post-filters">
							{!! Theme::partial('usermenu-settings') !!}
						</div>
					</div>
					<div class="col-md-8">
						@include('flash::message')
						<div class="panel panel-default">
							<div class="panel-heading no-bg panel-settings">
								<h3 class="panel-title">
									{{ trans('common.deactivate_account') }}
								</h3>
							</div>
							<div class="panel-body no-padding">
								<div class="accout-deactivate">
									<img src="{{  Theme::asset()->url('images/delete-img.png') }}" alt="images">
									<div class="delete-text">
										{{ trans('messages.confirm_deactivate_question') }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="user-delete-form text-center">
											<button href="#" class="btn btn-danger delete-location delete_location" data-locationdelete-id="{{ $location->id }}"><i class="fa fa-trash">{{ trans('messages.yes_deactivate') }}</i></button>

											{{--<form action="{{ url($username.'/deleteme') }}" class="user-delete-form">--}}
											{{--<button data-id="{{$username}}" type="button" class="btn btn-delete-location-user btn-danger">{{ trans('messages.yes_deactivate') }}</button>--}}
											{{--</form>--}}
										</div>
									</div>

								</div>
<hr>
							</div>
						<!--ending first panel-->
						
						
						</div>
					</div>
				</div>
			</div>
		<!-- </div> -->