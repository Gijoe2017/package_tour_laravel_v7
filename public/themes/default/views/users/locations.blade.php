<!-- <div class="main-content"> -->
	<div class="container">
		<div class="row">

			<!-- List of user locations-->

			<div class="col-md-6">
				<div class="post-filters locations-groups">
					
					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<div class="side-right create-btn">
								<a href="{{ url(Auth::user()->username.'/create-location') }}" class="btn btn-success">{{ trans('common.create_location') }}</a>
							</div>
							<h3 class="panel-title small-heading">
								{{ trans('locations') }}
							</h3>
							
						</div>
						<div class="panel-body">
							@if(Auth::user()->own_locations()->count())
								
							<ul class="list-group page-likes">
								@foreach(Auth::user()->own_locations() as $userlocations)

								<li class="list-group-item deletelocation">
									<div class="connect-list">
										<div class="connect-link side-left">
											<a href="{{ url($userlocations->username) }}">
												<img src=" @if($userlocations->timeline_id && $userlocations->avatar) {{ url('location/avatar/small/'.$userlocations->avatar) }} @else {{ url('location/avatar/default-location-avatar.png') }} @endif" alt="{{ $userlocations->name }}" title="{{ $userlocations->name }}">{{ $userlocations->name }}
											</a>
										</div>
										<div class="side-right">
											<a href="" class="side-right delete-location delete_location" data-locationdelete-id="{{ $userlocations->id }}"><span class="trash-icon bg-danger"><i class="fa fa-trash text-danger"></i></span></a>
										</div>
										<div class="clearfix"></div>
									</div>
								</li>
								@endforeach
							</ul>
							@else
								<div class="alert alert-warning">
									{{ trans('messages.no_locations') }}
								</div>
							@endif
						</div>
					</div><!-- /panel -->
				</div>
			</div><!-- /col-md-6 -->
			
			<!-- List of user groups-->
			
			<div class="col-md-6">
				<div class="post-filters locations-groups">
					
					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<h3 class="panel-title small-heading">
								{{ trans('common.joined_location') }}
							</h3>
						</div>
						<div class="panel-body">
							@if(Auth::user()->joinedLocations()->count())
								
							<ul class="list-group page-likes">
								@foreach(Auth::user()->joinedLocations() as $joinlocation)
								<li class="list-group-item holder">
									<div class="connect-list">
										<div class="connect-link side-left">
											<a href="{{ url($joinlocation->username) }}">
												<img src=" @if($joinlocation->timeline_id && $joinlocation->avatar) {{ url('location/avatar/small/'.$joinlocation->avatar) }} @else {{ url('location/avatar/default-location-avatar.png') }} @endif" alt="{{ $joinlocation->name }}" title="{{ $joinlocation->name }}">{{ $joinlocation->name }}
											</a>
										</div>
										<div class="side-right location-unjoin">
											<div class="left-col">
												<a href="#" class="btn btn-success unjoin-location joined" data-timeline-id="{{ $joinlocation->timeline_id }}">
													<i class="fa fa-check"></i> {{ trans('common.joined') }}
												</a>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</li>
								@endforeach
							</ul>
							@else
								<div class="alert alert-warning">
									{{ trans('messages.no_location') }}
								</div>
							@endif
						</div>
					</div>

				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div>
<!-- </div> --><!-- /main-content -->