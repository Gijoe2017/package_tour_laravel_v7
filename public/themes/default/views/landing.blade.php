<div class="container">

  
  <style type="text/css">

    .container {
      width: 98%;
    }

    .logo {
      border-radius: 50%;
    }

    .text-muted a {
      color: #989898;
    }

    .text-muted a:hover {
      color: grey;
    }

    .fa a:active, a:hover, a:visited {
      text-decoration: none;
    }

    /* Tool Colors */
    .general {
      background-color: #0095af;
    }

    .general .card-title {
      color: #f3f3f3;
    }

    .general .card-text {
      color: #f3f3f3;
    }

    .card-img {
      display: block;
      width: 100%;
      height: auto;
    }

    .overlay {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      height: 100%;
      width: 100%;
      opacity: 0;
      transition: .2s ease;
      background-color: #008CBA;
    }

    .card-pin:hover .overlay {
      opacity: .5;
      border: 5px solid #f3f3f3;
      transition: ease .5s;
      background-color: #000000;
      cursor: zoom-in;
    }

    .more {
      color: white;
      font-size: 20px;
      position: absolute;
      bottom: 0;
      right: 0;
      text-transform: uppercase;
      transform: translate(-20%, -20%);
      -ms-transform: translate(-50%, -50%);
    }

    .download {
      color: white;
      font-size: 20px;
      position: absolute;
      bottom: 0;
      left: 0;
      margin-left: 20px;
      text-transform: uppercase;
      transform: translate(-20%, -20%);
      -ms-transform: translate(-50%, -50%);
    }

    .card-pin:hover .card-title {
      color: #ffffff;
      margin-top: 10px;
      text-align: center;
      font-size: 1.2em;
    }

    .card-pin:hover .more a {
      text-decoration: none;
      color: #ffffff;
    }

    .card-pin:hover .download a {
      text-decoration: none;
      color: #ffffff;
    }

    .social {
      position: relative;
      transform: translateY(-50%);
    }

    .social .fa {
      margin: 0 3px;
    }

    .container{
      padding-right: 0;
      padding-left: 0;
    }
    .col-md-5{
      padding-right: 1px;
      padding-left: 1px;
    }
    #post-image-holder img {
      width: 270px !important;
      height: auto !important;
      margin-right: 5px;
      padding-left: 13px;
    }
    html{
      font-size: 13px;
    }

  </style>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel">
          <div class="panel-heading no-bg">
            <div class="post-author">
              <div class="post-options" style="text-align: center">
                  <h1>Toechok Co.,Ltd</h1>

                  <ul class="list-inline no-margin">
                      <li class="dropdown">
                          <a href="{{url('../home/package/all')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/package-tour.png') }}" alt="Package Tour" title="Package Tour"  style="height: 50px"><BR>
                                  {{ trans('common.package') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('../mall')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/icon-toechok-mall.png') }}" alt="Toechok Mall" title="Toechok Mall"  style="height: 50px"><BR>
                                  {{ trans('common.mall') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('home/location/22')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/eat-64x64.png') }}" alt="ที่กิน" title="ที่กิน"  style="height: 50px"><br>
                                  {{ trans('common.place_to_eat') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('home/location/14')}}"  role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/travel-64x64.png') }}" alt="ที่เที่ยว" title="ที่เที่ยว" style="height: 50px"><br>
                                  {{ trans('common.travel_destination') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('home/location/23')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/shop-64x64.png') }}" alt="ที่ช้อบ" title="ที่ช้อบ" style="height: 50px"><br>
                                  {{ trans('common.place_to_shop') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('home/location/29')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/accommodation-64x64.png') }}" alt="ที่พัก" title="ที่พัก" style="height: 50px"><br>
                                  {{ trans('common.place_to_stay') }}
                              </div>
                          </a>
                      </li>
                      <li class="dropdown">
                          <a href="{{url('home/location/other')}}" role="button" aria-haspopup="true" aria-expanded="false">
                              <div class="user-avatar">
                                  <img src="{{url('location/avatar/other-place-64x64.png') }}" alt="ที่อื่น ๆ" title="ี่อื่น ๆ" style="height: 50px"><br>
                                  {{ trans('common.other_place')}}
                              </div>
                          </a>
                      </li>
                      {{--<li class="dropdown">--}}
                          {{--<a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                              {{--{{ trans('common.my_location') }}--}}
                              {{--<i class="fa fa-angle-down"></i>--}}
                          {{--</a>--}}
                          {{--<ul class="dropdown-menu">--}}
                              {{--<li class="main-link">--}}
                                  {{--<a href="{{ url(Auth::user()->username.'/locations') }}">--}}
                                      {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                                      {{--{{ trans('common.my_visited_location') }}--}}
                                      {{--<span class="event-circle">{{ Auth::user()->own_locations()->count() }}</span>--}}
                                  {{--</a>--}}
                              {{--</li>--}}
                              {{--<li class="main-link">--}}
                                  {{--<a href="{{ url(Auth::user()->username.'/locations/like') }}">--}}
                                      {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                                      {{--{{ trans('common.my_wanted_location') }}--}}
                                      {{--<span class="event-circle">{{ Auth::user()->locationLikes()->count() }}</span>--}}
                                  {{--</a>--}}
                              {{--</li>--}}
                              {{--<li class="main-link">--}}
                                  {{--<a href="{{ url(Auth::user()->username.'/locations/member') }}">--}}
                                      {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                                      {{--{{ trans('common.my_member_location') }}--}}
                                      {{--<span class="event-circle">{{ Auth::user()->member_locations()->count() }}</span>--}}
                                  {{--</a>--}}
                              {{--</li>--}}
                              {{--<li class="main-link">--}}
                                  {{--<a href="{{ url(Auth::user()->username.'/locations') }}">--}}
                                      {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                                      {{--{{ trans('common.my_admin_location') }}--}}
                                      {{--<span class="event-circle">{{ Auth::user()->own_locations()->count() }}</span>--}}
                                  {{--</a>--}}
                              {{--</li>--}}
                          {{--</ul>--}}
                      {{--</li>--}}
                  </ul>
                  <br>
                {{--<div class="row">--}}
                  {{--<div class="col-md-2 col-md-offset-1">--}}
                    {{--<fieldset class="form-group required {{ $errors->has('search_country_id') ? ' has-error' : '' }}">--}}
                      {{--{{ Form::label('search_country_id', trans('common.country'), ['class' => 'control-label']) }}--}}
                      {{--{{ Form::select('search_country_id', array('' => trans('common.country'))+ $country_options,'', array('class' => 'form-control','id'=>'search_country_id')) }}--}}
                      {{--@if ($errors->has('search_country_id'))--}}
                        {{--<span class="help-block">--}}
                        {{--{{ $errors->first('search_country_id') }}--}}
                        {{--</span>--}}
                      {{--@endif--}}
                    {{--</fieldset>--}}
                  {{--</div>--}}
                  {{--<div class="col-md-5">--}}
                    {{--<fieldset class="form-group required {{ $errors->has('search_category_id') ? ' has-error' : '' }}">--}}
                      {{--{{ Form::select('search_category_id', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control','id'=>'search_category_id')) }}--}}
                      {{--@if ($errors->has('search_category_id'))--}}
                        {{--<span class="help-block">--}}
                                          {{--{{ $errors->first('search_category_id') }}--}}
                                          {{--</span>--}}
                      {{--@endif--}}
                    {{--</fieldset>--}}
                  {{--</div>--}}
                  {{--<div class="col-md-2">--}}
                    {{--<input class="form-control" placeholder="Search" name="name" type="text" id="name" >--}}
                  {{--</div>--}}
                  {{--<div class="col-md-1">--}}
                    {{--<button class="btn btn-default" type="submit" id="search_location_home" >Search</button>--}}
                  {{--</div>--}}
                {{--</div>--}}

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        @if (Session::has('message'))
          <div class="alert alert-{{ Session::get('status') }}" role="alert">
            {!! Session::get('message') !!}
          </div>
        @endif
        @if(isset($active_announcement))
          <div class="announcement alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h3>{{ $active_announcement->title }}</h3>
            <p>{{ $active_announcement->description }}</p>
          </div>
        @endif

        @if($mode != "eventlist")
          {{--<div class="timeline-posts">--}}
          <div class="infinite-scroll">
            <div class="card-columns">
              @if($locations->count() > 0)
                @foreach($locations as $location)
                        <?php
                        $timeline=$location->timeline()->first();
                        $post=\App\Post::where('timeline_id',$location->timeline_id)->latest()->first();

                        $media=\App\Media::where('id',$timeline->avatar_id)->first();
                        $city=null;$state=null;
                        $country=$timeline->country()->where('language_code',Session::get('language'))->first();
                        if(!$country){
                            $country=$timeline->country()->where('language_code','en')->first();
                        }

                        if($timeline->state_id){
                            $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code',Session::get('language'))->first();
                            if(!$state){
                                $state=$timeline->country->state()->where('state_id',$timeline->state_id)->where('language_code','en')->first();
                            }

                            $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code',Session::get('language'))->first();
                            if(!$city){
                                $city=$timeline->country->city()->where('city_id',$timeline->city_id)->where('language_code','en')->first();
                            }
                        }
                        if(!$country){
                            $country=$timeline->country()->where('language_code','en')->first();
                        }
                        ?>

                  {!! Theme::partial('location-home',compact('post','location','timeline','media','city','state','country','category_options','country_options','next_page_url')) !!}
                  {!! Theme::asset()->container('footer')->usePath()->add('lightbox', 'js/lightbox.min.js') !!}
                @endforeach
                {{$locations->render()}}
              @else
                <div class="no-posts alert alert-warning">{{ trans('common.no_posts') }}</div>
              @endif
            </div>
            
       

          </div>
        @else
          {!! Theme::partial('eventslist',compact('user_events','username')) !!}
        @endif
      </div>
    </div>
  </div>






{!! Theme::asset()->container('footer')->usePath()->add('app', '_app.jss') !!}

  <script type="text/javascript">

      $('#search_location_home').on('click',function () {
          var name=$('#name').val();
          var country=$('#search_country_id').val();
          var category=$('#search_category_id').val();
          search_location(name,country,category);
      });

      $('#search_country_id').on('change',function (e) {
          alert('test');
          var name=$('#name').val();
          var country=e.target.value;
          var category=$('#search_category_id').val();
          search_location(name,country,category);
      });
      $('#search_category_id').on('change',function (e) {
          var name=$('#name').val();
          var country=$('#search_country_id').val();
          var category=e.target.value;
          search_location(name,country,category);
      });

      $('#search_name').on('blur',function (e) {
          var name=e.target.value;
          var country=$('#search_country_id').val();
          var category=$('#search_category_id').val();
          search_location(name,country,category);

      });

      function search_location(name,country,category) {
          // alert(name+country+category);
          $.ajax({
              type:'get',
              url: SP_source() + 'search/location',
              data:{'name':name,'country_id':country,'category_id':category},
              success:function (data) {
//            alert(data);
                  location.reload();
              }
          });
      }

      $('ul.pagination').hide();
      $(function() {
          $('.infinite-scroll').jscroll({
              autoTrigger: true,
              loadingHtml: '<img class="center-block" src="{{asset('themes/default/assets/images/loading.gif')}}" alt="Loading..." />', // MAKE SURE THAT YOU PUT THE CORRECT IMG PATH
              padding: 0,
              nextSelector: '.pagination li.active + li a',
              contentSelector: 'div.infinite-scroll',
              callback: function() {
                  $('ul.pagination').remove();
                  jQuery("time.timeago").timeago();
              }
          });

      });

      $('.popup').click(function(){
          var width=$(window).width();
          var height=$(window).height();
          $.jsPanel({
              iframe: {
                  width:width,
                  height:height,
                  src:    $(this).data('content')+'#post',
                  name:   'myFrame',
                  style:  {'border': '10px solid transparent'}
              },
              size:     'auto',
              position: {left: 0, top: 0},
              theme:    'black',
          });
      });

  </script>