
typeof $.typeahead === 'function' && $.typeahead({
    input: ".js-typeahead",
    minLength: 1,
    order: "asc",
    group: false,
    maxItemPerGroup: 5,
    groupOrder: function (node, query, result, resultCount, resultCountPerGroup) {

        var scope = this,
            sortGroup = [];

        for (var i in result) {
            sortGroup.push({
                group: i,
                length: result[i].length
            });
        }

        sortGroup.sort(
            scope.helper.sort(
                ["length"],
                false, // false = desc, the most results on top
                function (a) {
                    return a.toString().toUpperCase()
                }
            )
        );

        return $.map(sortGroup, function (val, i) {
            return val.group
        });
    },
    hint: true,
    // href: SP_source() + 'member/create-location',
    template: function (query, item) {
        var template = '{{display}}';
        if (item.addNewItem) {
            template = '+Add new location name "'+ query +'"';
        }
        return template;
    },

    source: {
        location: {
            ajax: {
                url: SP_source() + '/data/location',
                path: "data.location"
            }
        }
    },

    callback: {
        onClickAfter: function (node, a, item, event) {
            event.preventDefault();
            $('.js-result-container').text('');
        },
        onResult: function (node, query, result,objCount, resultCount, resultCountPerGroup) {
            result.push({
                addNewItem: true,
                group: 'location'
            });
            var text = "";
            if (query !== "") {
                text = objCount + ' elements matching "' + query + '"';
            }
            $('.js-result-container').text(text);
            $('#name').val(query);
        },
        onClick: function (node, a, item, event) {
            if (item.addNewItem) {
                event.preventDefault();
                $('.search-location').hide();
                $('.new-location').show();
                $('.new-location2').show();
                $('#name').prop('required',true);
                $('#category_id').prop('required',true);
                $('#q').prop('required',false);
                $('#address').prop('required',true);
                $('#country_id').prop('required',true);
                $('.hide-address').show();
            }else{

                $('.hide-address').hide();
                $('.new-location2').hide();
                $('#name').val('');
                $('#address').prop('required',false);
                $('#name').prop('required',false);
                $('#category_id').prop('required',false);
                $('#stayed_address').prop('required',false);
                $('#country_id').prop('required',false);
                $('#state_id').prop('required',false);
                $('#city_id').prop('required',false);
                $('#city_sub1_id').prop('required',false);
                $('#zip_code').prop('required',false);
                $('#q').prop('required',true);

            }

        }

    },

    debug: true
});

/**
 * Created by narong on 11/8/2018.
 */
