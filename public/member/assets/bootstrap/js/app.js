$('body').delegate('.delete-rate','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    if(confirm('Are you want ot delete?')){
        var packageDescID=$('#packageDescID').val();
        $.ajax({
            type:'get',
            url:SP_source() + 'package/ajax/rate_del',
            data:{'psub_id':id,'packageDescID':packageDescID},
            success:function(data){
              $("#table-rate tbody").html(data);
            }
        });
    }else{
        return false;
    }
});

$('#operator_code').on('change',function (e) {
    //alert('test');
    if(e.target.value=='Deposit'){
        $.ajax({
            type:'get',
            url:SP_source() + 'package/ajax/condition/deposit/sub',
            data:{'operator_code':e.target.value},
            success:function (data) {
               // alert(data);
                $('#sub_opertor').show();
                $('#sub_opertor').html(data);
            }
        });
    }

    $('.deposit').hide();
    $('.visa').hide();
    $('.not-deposit').show();
    $("#condition_right").prop('required',false);
    if(e.target.value=='Between'){
        $('.between').show();
        $('#date_start').html('จำนวนวันเริ่ม *');
        $('#date_end').html('จำนวนวันสื้นสุด *');
        $("#condition_right").prop('required',true);
        $("#condition_left").prop('required',true);
        $("#value_deposit").prop('required',true);
        $("#condition_unit").prop('required',true);
    }else if(e.target.value=='Deposit') {
        $('.deposit').show();
        $('.not-deposit').hide();
        $("#condition_left").prop('required',false);
        $("#condition_value").prop('required',false);
        $("#condition_unit").prop('required',false);
        $("#value_tour").prop('required',false);
        $("#unit_value_tour").prop('required',false);
        $("#deposit").prop('required',true);
    }else if(e.target.value=='>'){
        $('.visa').show();
        $('.not-deposit').hide();
        $('#visa_expire').html('วันที่หมดอายุวีซา *');
        $("#deposit").prop('required',false);
        $("#condition_left").prop('required',false);
        $("#condition_value").prop('required',false);
        $("#condition_unit").prop('required',false);
        $("#value_tour").prop('required',false);
        $("#unit_value_tour").prop('required',false);
        $("#visa_expiry_date").prop('required',true);

    }else{

        $('.between').hide();
        $('#date_start').html('จำนวนวัน *');
        $("#condition_value").prop('required',false);
        $("#condition_unit").prop('required',false);
        $("#value_tour").prop('required',false);
        $("#unit_value_tour").prop('required',false);
        $("#condition_left").prop('required',true);
        $("#value_deposit").prop('required',true);
        // $("#condition_unit").prop('required',true);
    }

});

// $('.chk_condition').on('click',function (e) {
//     var status='n';
//     if($(this).prop('checked')==true){
//         status='y';
//     }
// // alert($(this).data('id'));
//     $.ajax({
//         type:'get',
//         url:SP_source() + 'package/ajax/condition/status',
//         data:{'id':$(this).data('id'),'status':status},
//         success:function (data) {
//             $('#show_status').show();
//             $("#show_status").fadeOut(2000);
//         }
//     });
// });










$('body').on('click','.switch-price_visa_condition',function () {

    var elm='span-price_include_visa-'+$(this).data('id');
    var status='N';
    if($(this).prop('checked')==true){
        status='Y';
    }

    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/condition/price_visa',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
           document.getElementById(elm).innerHTML=data;
        }
    });
});



$('body').on('click','.switch-price_default_condition',function () {
    var elm='span-price_system_vat-'+$(this).data('id');
    var status='N';
   
    if($(this).prop('checked')==true){
        status='Y';
    }

    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/condition/price_default',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
            document.getElementById(elm).innerHTML=data;
        }
    });
});






$('.chk_condition').on('click',function (e) {
    var status='n';
    if($(this).prop('checked')==true){
        status='y';
    }
// alert($(this).data('id'));
    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/condition/status',
        data:{'id':$(this).data('id'),'status':status},
        success:function (data) {
            $('#show_status').html(data);
            $('#show_status').show();
            $("#show_status").fadeOut(2000);
        }
    });
});






$('#add-members-package').on('keyup',function(){

    $('.package-suggested-users').empty();
    if($('#add-members-package').val() != null && $('#add-members-package').val() != "")
        pageId = $(this).data('package-id');
    $.post( SP_source() + 'ajax/get-members-package' , { searchname: $('#add-members-package').val() ,page_id: pageId, csrf_token: $('[name="csrf_token"]').attr('content') })
        .done(function( responseText ) {
//alert('test');
            if(responseText.status == 200)
            {
                var users_results = responseText.data;

                $.each(users_results, function(key, value) {

                    var user = value[0];
                    var joinStatus = '';
                    var user_id = '';
                    var page_id = '';

                    if(user.pages[0] != null)
                    {
                        user_id = user.pages[0].pivot.user_id;
                        page_id = user.pages[0].pivot.page_id;
                        joinStatus = 'Joined';
                    }
                    else
                    {
                        user_id = user.id;
                        page_id = pageId;
                        joinStatus = 'Join';
                    }


                    if(user.avatar_id != null){
                        avatarSource = user.avatar_url[0].source;

                    }else{
                        avatarSource = "default-"+user.gender+"-avatar.png";
                    }
                    var verified = '';

                    if(user.verified == 1)
                    {
                        var verified = '<span class="verified-badge verified-small bg-success"> <i class="fa fa-check"></i></span>';
                    }

                    $('.page-suggested-users').append('<div class="holder">' +
                        '<div class="follower side-left">' +
                        '<a href="' +  SP_source() + user.username + '">' +
                        '<img src="' + SP_source() + 'user/avatar/'+ avatarSource +'" alt="images">' +
                        '</a>' +
                        '<a href="' +  SP_source() + user.username + '">' +
                        '<span>' + user.name + '</span>' +
                        '</a>' + verified +
                        '</div>' +
                        '<div class="follow-links side-right">' +
                        '<div class="left-col">' +
                        '<a href="#" class="btn btn-to-follow btn-default add-package-member  add" data-user-id="'+user_id+' - '+page_id+'-'+joinStatus+'">' + joinStatus + '</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="clearfix"></div>'+
                        '</div>');

                });
            }
        });
});



$('.add_highlight').on('click',function (e) {

    if($(this).is(':checked')){
        var status='Y';
    }else{
        var status='N';
    }
   // alert(status+e.target.value);
    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/program/set_highlight',
        data:{'id':e.target.value,'status':status},
        success:function(data){

            // if (data.status == 200) {
            //     postPanel.addClass('fadeOut');
            //     setTimeout(function(){
            //         postPanel.remove();
            //     },800);
            //     notify('You have successfully deleted the post','warning');
            // }
        }
    });
    
});

$('.switch').on('click',function () {
    if($(this).is(':checked')){
        var status='Y';
    }else{
        var status='N';
    }
    var elm='span-'+$(this).data('id');
    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/details/set_status',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
            document.getElementById(elm).innerHTML=data;
        }
    });
});


$('body').on('change','#TourCategoryID',function (e) {
    
    $.ajax({
        type:'post',
        url:SP_source() + 'package/ajax/program/sub_category',
        data:{'group':e.target.value, '_token': $('[name="csrf_token"]').attr('content')},
        success:function(data){

            if(data!= ''){
                $('#TourSubCateID').empty();
                $('#TourSubCateID').append(data);
            }else{
                $('#TourSubCateID').empty();
            }

        }
    });
});

$('body').on('click','.switch-rate',function () {

    var elm='span-'+$(this).data('id');
    var promotion='promotion'+$(this).data('id');

    if($(this).is(':checked')){
        var status='Y';
        $('.promotion-'+$(this).data('id')).show();
    }else{
        var status='N';
        $('.promotion-'+$(this).data('id')).hide();
    }

    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/rate/set_status',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
            document.getElementById(elm).innerHTML=data;
        }
    });
});

$('body').on('click','.switch-additional',function () {
    if($(this).is(':checked')){
        var status='Y';
    }else{
        var status='N';
    }
    var elm='span-'+$(this).data('id');
    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/additional/set_status',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
           document.getElementById(elm).innerHTML=data;
        }
    });
});

$('body').on('click','.switch-promotion',function () {
    if($(this).is(':checked')){
        var status='Y';
    }else{
        var status='N';
    }

    $.ajax({
        type:'get',
        url:SP_source() + 'package/ajax/promotion/set_status',
        data:{'id':$(this).data('id'),'status':status},
        success:function(data){
          
             // if(data=='Mod' && status=='Y'){
             //     alert(data);
             //     $('.Between-'+$(this).data('id')).attr('checked','checked')
             // }else if(data=='Between' && status=='Y'){
             //     $('.Mod-'+$(this).data('id')).attr('checked','checked')
             //     alert(data);
             // }
            // $("#promotion_reload").load(location.href + "#promotion_reload", "");
              location.reload();
        }
    });
});

// when the modal is closed
$('body').on('hidden.bs.modal', '#popupForm',function () {
    // remove the bs.modal data attribute from it

    $(this).removeData('bs.modal');
    // and empty the modal-content element
    $('#popupForm.modal-content').empty();
});

$('body').on('change','#condition_group_id',function (e) {
    var text=  $("#operator_code option:first").text();
    $.ajax({
        type:'post',
        url:SP_source() + 'package/ajax/condition/group',
        data:{'group':e.target.value, '_token': $('[name="csrf_token"]').attr('content')},
        success:function(data){
            if(data!= ''){
                $('#operator_code').empty();
                $('#operator_code').append("<option value=''>"+ text +"</option>");
                $('#operator_code').append(data);
            }else{
                $('#operator_code').empty();
            }
            $('.deposit').hide();
            $('.visa').hide();
        }
    });
});






$('#keep_value_deposit').on('click',function () {
    if(this.checked==true){
        $('.keep_value_deposit').show();
        // $('#keep_all_costs').prop('checked', false);
        // $('#return_all_costs').prop('checked', false);
        $('#keep_all_deposit').prop('checked', false);
        $('#return_all_deposit').prop('checked', false);
        $("#value_deposit").prop('required',true);
        $("#unit_deposit").prop('required',true);
    }else{
        $("#value_deposit").prop('required',false);
        $("#unit_deposit").prop('required',false);
        $('.keep_value_deposit').hide();
    }

});

$('#keep_value_tour').on('click',function () {
    if(this.checked==true){
        $('.keep_value_tour').show();
        $('#keep_all_costs').prop('checked', false);
        $('#return_all_costs').prop('checked', false);
        $("#value_tour").prop('required',true);
        $("#unit_value_tour").prop('required',true);

        if($('#keep_value_deposit').prop('checked')==false){
            $("#value_deposit").prop('required',false);
            $("#unit_deposit").prop('required',false);
        }

    }else{
        // $("#value_deposit").prop('required',false);
        // $("#unit_deposit").prop('required',false);
        $("#value_tour").prop('required',false);
        $("#unit_value_tour").prop('required',false);
        $('.keep_value_tour').hide();
    }

});


$('#keep_all_deposit').on('click',function () {
    if(this.checked==false){
        $('.keep_value_tour').show();
        $('.keep_value_deposit').show();
    }else{
        $('#keep_value_deposit').prop('checked', false);
        $('#return_all_deposit').prop('checked', false);
        // $('#keep_value_tour').prop('checked', false);
        $("#value_deposit").prop('required',false);
        $("#unit_deposit").prop('required',false);
        // $('.keep_value_tour').hide();
        $('.keep_value_deposit').hide();
    }
});

$('#return_all_deposit').on('click',function () {
    if(this.checked==false){
        $('.keep_value_tour').show();
        $('.keep_value_deposit').show();
    }else{
        $('#keep_value_deposit').prop('checked', false);
        $('#keep_all_deposit').prop('checked', false);
        // $('#keep_value_tour').prop('checked', false);
        $("#value_deposit").prop('required',false);
        $("#condition_unit").prop('required',false);
        // $('.keep_value_tour').hide();
        $('.keep_value_deposit').hide();
    }

});





$('#keep_all_costs').on('click',function () {
    if(this.checked==false){
        $('.keep_value_tour').show();
        $('.keep_value_deposit').show();
    }else{

        // $('#keep_value_deposit').prop('checked', false);
        $('#return_all_costs').prop('checked', false);
        $('#keep_value_tour').prop('checked', false);
        // $("#value_deposit").prop('required',false);
        $("#unit_deposit").prop('required',false);
        $('.keep_value_tour').hide();
        $('.keep_value_deposit').hide();
    }
});

$('#return_all_costs').on('click',function () {
    if(this.checked==false){
        $('.keep_value_tour').show();
        $('.keep_value_deposit').show();
    }else{

        // $('#keep_value_deposit').prop('checked', false);
        $('#keep_all_costs').prop('checked', false);
        $('#keep_value_tour').prop('checked', false);
        // $("#value_deposit").prop('required',false);
        $("#condition_unit").prop('required',false);
        $('.keep_value_tour').hide();
        $('.keep_value_deposit').hide();
    }

});

// $('body').delegate('.edit-rate','click',function(e) {
//     e.preventDefault();
//
//     var id=$(this).data('id');
//     var packageDescID=$('#packageDescID').val();
//     $.ajax({
//         type:'get',
//         url:SP_source() + 'package/ajax/rate/edit01',
//         data:{'psub_id':id,'packageDescID':packageDescID},
//         success:function(data){
//            alert(data);
//
//         }
//     });
//
// });

$('#address-submit').on('click',function () {
    var address_type = document.getElementById("myform-address").elements.item(0).value;
    var address = document.getElementById("myform-address").elements.item(1).value;
    var additional = document.getElementById("myform-address").elements.item(2).value;
    var country_id = document.getElementById("myform-address").elements.item(3).value;
    var state_id = document.getElementById("myform-address").elements.item(4).value;
    var city_id = document.getElementById("myform-address").elements.item(5).value;
    var city_sub1_id = document.getElementById("myform-address").elements.item(6).value;
    var zip_code = document.getElementById("myform-address").elements.item(7).value;

    $.ajax({
        type:'post',
        url:SP_source() + 'member/ajax-save-address',

        data:{
            'address_type':address_type,
            'address':address,
            'additional':additional,
            'country_id':country_id,
            'state_id':state_id,
            'city_id':city_id,
            'city_sub1_id':city_sub1_id,
            'zip_code':zip_code,
            '_token': $('[name="csrf_token"]').attr('content'),
        },
        success:function(data){
            $("#address_type_id").val('').trigger('change');
            $("#country_id").val('').trigger('change');
            $("#state_id").val('').trigger('change');
            $("#city_id").val('').trigger('change');
            $("#city_sub1_id").val('').trigger('change');

            document.getElementById("myform-address").elements.item(1).value='';
            document.getElementById("myform-address").elements.item(2).value='';
            document.getElementById("myform-address").elements.item(7).value='';
            $('#show-address-detail').html('');
            $('#show-address-detail').html(data);
            $('#show-address-detail').show();
            // $('#myform-address').hide();
            // $("body").load(location.href + " #show-address-detail");
        }
    });
});


$('#continue-step3').on('click',function (e) {
    $('#event').val('continue');
    $('#form-'+$(this).attr('id')).submit();
});


$(".continue-btn").keydown(function(){
    alert('test');
    $('#event').val('continue');
});

$('#timeline').on('change',function (e) {
    $('#form-set-timeline').submit();
});

function delete_phone(id) {

    if(confirm('Are you delete this record?')){

        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-phone',
            data:{'phone_id':id},
            success:function(data){

                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }
}


$('.add-new').on('click',function () {

    $('.address-form').show();
    $(this).hide();
});
// $('.address-form').on('click',function () {
//     alert('test');
//     $('.address-form').show();
// });


$('#visa_issued_country_id').on('change',function (e) {
    var text=  $("#visa_type_id option:first").text();
    $.ajax({
        type:'get',
        url:SP_source() + 'member/visa-country',
        data:{'country_id':e.target.value},
        success:function(data){

            if(data!= ''){
                $('#visa_type_id').empty();
                $('#visa_type_id').append("<option value=''>"+ text +"</option>");
                $('#visa_type_id').append(data);
            }
        }
    });

});



$('body').delegate('.address-edit','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    $.ajax({
        type:'get',
        url:SP_source() + 'member/address/edit',
        data:{'address_id':id},
        success:function(data){
            $('#address_type_id').val(data.address_type_id);
            $('#address').val(data.address);
            $('#additional').val(data.additional);
            $("#country_id").val(data.country_id).change();
            $("#state_id").val(data.state_id).change();
            $("#city_id").val(data.city_id).change();
            $("#city_sub1_id").val(data.city_sub1_id).change();
            $("#zip_code").val(data.zip_code);



        }
    });

});

$('body').delegate('.delete_edit','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
//alert(id);
    $.ajax({
        type:'get',
        url:SP_source() + 'member/edit-address',
        data:{'address_id':id},
        success:function(data){
            $('#address_type_id').val(data['address_type_id']);
            $('#address').val(data['address']);
            // document.getElementById("myform-address").elements.item(1).value=data['address'];
            // document.getElementById("myform-address").elements.item(2).value=data['additional'];
            // document.getElementById("myform-address").elements.item(3).value=data['country_id'];
            // document.getElementById("myform-address").elements.item(4).value=data['state_id'];
            // document.getElementById("myform-address").elements.item(5).value=data['city_id'];
            // document.getElementById("myform-address").elements.item(6).value=data['city_sub_id'];
            // document.getElementById("myform-address").elements.item(7).value=data['zip_code'];
            $('#myform-address').show();
        }
    });


});


$('body').delegate('.order-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    if(confirm('Are you want to delete?')) {

        $.ajax({
            type:'get',
            url:SP_source() + 'package/member/delete-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});


$('body').delegate('.phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});


$('body').delegate('.order_delete_address','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    var passport_id=$('#passport_id').val();

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/member/delete-address',
            data:{'address_id':id,'passport_id':passport_id},
            success:function(data){
                $('#show-address-detail').html(data);
                $('#show-address-detail').show();
            }
        });
    }else{
        return false;
    }
});


$('body').delegate('.delete_address','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-address',
            data:{'address_id':id},
            success:function(data){
                $('#show-address-detail').html(data);
                $('#show-address-detail').show();
            }
        });
    }else{
        return false;
    }
 });

$('body').delegate('.order_delete_email','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');
   
    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/member/delete-email',
            data:{'id':id},
            success:function(data){
                $('#show-email-detail').html(data);
                $('#show-email-detail').show();
            }
        });
    }else{
        return false;
    }
 });

$('body').delegate('.delete_email','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-email',
            data:{'id':id},
            success:function(data){
                $('#show-email-detail').html(data);
                $('#show-email-detail').show();
            }
        });
    }else{
        return false;
    }
 });



$('body').delegate('.ref-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-ref-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});

$('body').delegate('.place-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-place-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});

$('body').delegate('.order_place-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/delete-place-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});

$('body').delegate('.occupation-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'member/delete-occupation-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});

$('body').delegate('.order_occupation-phone-delete','click',function(e) {
    e.preventDefault();
    var id=$(this).data('id');

    if(confirm('Are you want to delete?')) {
        $.ajax({
            type:'get',
            url:SP_source() + 'package/delete-occupation-phone',
            data:{'id':id},
            success:function(data){
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });
    }else{
        return false;
    }
});

// To switch language

$('body').delegate('.switch-language-education','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');
    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/education/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});




$('.switch-language').on('click',function(e){
alert('test');
    e.preventDefault();
    var url=$(this).data('id');
    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            window.location = url;
         // window.location =  window.location.href;
         // window.location = SP_source()+'/member/list';
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});

$('body').delegate('.switch-language-member','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');


    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});


$('body').delegate('.switch-language-family','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');

    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/family/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});

$('body').delegate('.switch-language-reference','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');

    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/reference/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});

$('body').delegate('.switch-language-place','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');

    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/place/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});

$('body').delegate('.switch-language-visited','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');

    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/visited/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});

$('body').delegate('.switch-language-occupation','click',function(e){
    e.preventDefault();
    var id=$(this).data('id');

    $.post(SP_source() + 'ajax/switch-language', {language: $(this).data('language')}, function(data) {
        if (data.status == 200) {
            // window.location =  window.location.href;
            window.location = SP_source()+'member/occupation/edit/'+ id;
        }
        else if (data.status == 201) {
            notify(data.message,'warning');
        }
    });
});







    $('#phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#add-phone').show();
            $('.continue').hide();
            $('#order-add-phone').show();
            $('#add-phone-none').hide();
        }
    });

    $('#email').keyup(function (e) {
        var text=e.target.value;
        if(text.length>2){
            $('#add-email').show();
            $('#add-email-none').hide();
            $('#order-add-email').show();
            $('#order-add-email-none').hide();
        }
    });


    $('#order-add-email').on('click',function () {
        $.ajax({
            type:'post',
            url:SP_source() + 'package/member/save-email',
            data:{
                'email':$('#email').val(),
                '_token': $('[name="csrf_token"]').attr('content'),
            },
            success:function(data){
                $('#email').val('');
                $('#show-email-detail').html(data);
                $('#show-email-detail').show();
            }
        });

    });

$('#add-email').on('click',function () {
    $.ajax({
        type:'post',
        url:SP_source() + 'member/save-email',
        data:{
            'email':$('#email').val(),
            '_token': $('[name="csrf_token"]').attr('content'),
        },
        success:function(data){
            $('#email').val('');
            $('#show-email-detail').html(data);
            $('#show-email-detail').show();
        }
    });

});


    $('#order-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#phone_number').val();
       // alert(phone_number);
        $.ajax({
            type:'post',
            url:SP_source() + 'package/member/save-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content'),
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#phone_number').val('');
                $('#add-phone-none').show();
                $('#add-phone').hide();
                $('.continue').show();
                $('#order-add-phone').hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });

    $('#add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#phone_number').val();
        alert('test');
        $.ajax({
            type:'post',
            url:SP_source() + 'member/save-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content'),
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#phone_number').val('');
                $('#add-phone-none').show();

                $('#add-phone').hide();
                $('#order-add-phone').hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });

    $('#ref_phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#ref-add-phone').show();
            $('#add-phone-none').hide();
        }
    });

     $('#place_phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#place-add-phone').show();
            $('#add-phone-none').hide();
        }
    });


     $('#order_place_phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#order_place-add-phone').show();
            $('#add-phone-none').hide();
        }
    });

    $('#ref-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#ref_phone_number').val();
        $.ajax({
            type:'post',
            url:SP_source() + 'member/save-ref-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content')
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#ref_phone_number').val('');
                $('#add-phone-none').show();
                $(this).hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });

    $('#place-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#place_phone_number').val();
        $.ajax({
            type:'post',
            url:SP_source() + 'member/save-place-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content')
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#place_phone_number').val('');
                $('#add-phone-none').show();
                $(this).hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });
    $('#order_place-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#order_place_phone_number').val();
        $.ajax({
            type:'post',
            url:SP_source() + 'member/save-place-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content')
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#place_phone_number').val('');
                $('#add-phone-none').show();
                $('#order_place-add-phone').hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });


    $('#occupation_phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#occupation-add-phone').show();
            $('#add-phone-none').hide();
        }
    });

    $('#order_occupation_phone_number').keyup(function (e) {
        var text=e.target.value;
        if(text.length>7){
            $('#order_occupation-add-phone').show();
            $('#add-phone-none').hide();
        }
    });

    $('#order_occupation-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#order_occupation_phone_number').val();
        $.ajax({
            type:'post',
            url:SP_source() + 'package/save-occupation-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content')
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#occupation_phone_number').val('');
                $('#add-phone-none').show();
                $('#order_occupation-add-phone').hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });

    $('#occupation-add-phone').on('click',function () {

        var phone_country_id=$('#phone_country_id').val();
        var phone_country_code=$('#phone_country_code').val();
        var phone_type=$('#phone_type').val();
        var phone_number=$('#occupation_phone_number').val();
        $.ajax({
            type:'post',
            url:SP_source() + 'member/save-occupation-phone',
            data:{
                'phone_country_id':phone_country_id,
                'phone_country_code':phone_country_code,
                'phone_type':phone_type,
                'phone_number':phone_number,
                '_token':$('meta[name="csrf_token"]').attr('content')
            },
            success:function(data){
                $("#phone_country_id").val('').trigger('change')
                $("#phone_country_code").val('').trigger('change')
                $("#phone_type").val('').trigger('change')
                $('#occupation_phone_number').val('');
                $('#add-phone-none').show();
                $(this).hide();
                $('#show-phone-detail').html(data);
                $('#show-phone-detail').show();
            }
        });

    });


    // $('#ref-phone-delete').on('click',function () {
    //     if(confirm('Are you delete this record?')){
    //         $.ajax({
    //             type:'get',
    //             url:SP_source() + 'member/delete-ref-phone',
    //             data:{'phone_id':$(this).data('id')},
    //             success:function(data){
    //                 $('#show-phone-detail').html(data);
    //                 $('#show-phone-detail').show();
    //             }
    //         });
    //     }
    //
    // });


    $("#addButtonEmail").click(function () {
        if( ($('.more-email .control-email').length+1) > 2) {
            alert("Only 3 control-email allowed");
            $(this).hide();
            return false;
        }
        $("#removeButtonEmail").show();
        var id = ($('.more-email .control-email').length + 1).toString();
        $('.more-email').append('<div class="col-md-4 control-email" id="control-email' + id + '"> <div class="form-group"><label class="control-label" for="inputEmail' + id + '">Email ' + id + '</label><div class="controls' + id + '"><input class="form-control" type="text" name="email[]" id="email' + id + '" placeholder="Insert Email"></div></div></div>');
    });

    $("#removeButtonEmail").click(function () {
        if ($('.more-email .control-email').length == 0) {
            alert("No more textbox to remove");
            $(this).hide();$("#addButtonEmail").show();
            return false;
        }
        $(".more-email .control-email:last").remove();
    });

    $("#addButtonPhone").click(function () {
        if( ($('.more-phone .control-phone').length+1) > 2) {
            alert("Only 3 control-email allowed");
            $(this).hide();
            return false;
        }
        $("#removeButtonPhone").show();
        var id = ($('.more-phone .control-phone').length + 1).toString();
        $('.more-phone').append('<div class="col-md-4 control-phone" id="control-phone' + id + '"> <div class="form-group"><label class="control-label" for="phone' + id + '">Phone ' + id + '</label><div class="controls' + id + '"><input class="form-control" type="text" name="phone[]" id="phone' + id + '" placeholder="Insert Phone"></div></div></div>');
    });

    $("#removeButtonPhone").click(function () {
        if ($('.more-phone .control-phone').length == 0) {
            alert("No more textbox to remove");
            $(this).hide();$("#addButtonPhone").show();
            return false;
        }
        $(".more-phone .control-phone:last").remove();
    });




$('#country_id').on('change',function(e){

    var text=  $("#state_id option:first").text();
    var text1=  $("#city_id option:first").text();
    var text2=  $("#city_sub1_id option:first").text();

    $.ajax({
        type:'post',
        url:SP_source() + 'ajax/get-state',
        data:{'country_id':e.target.value,csrf_token: $('[name="csrf_token"]').attr('content')},
        success:function(data){

            if(data.length>23){
                $('#state_id').empty();
                $('#state_id').append("<option value=''>"+ text +"</option>");

                $('#city_id').append("<option value=''>"+ text1 +"</option>");
                $('#city_id').append(data);

                $('#city_sub1_id').empty();
                $('#city_sub1_id').append("<option value=''>"+ text2 +"</option>");
                $("#zip_code").val('');

                $('#state_id').append(data);

                $('.state_id').show();
                $('.city_id').hide();
                $('.city_sub1_id').hide();
                $('#box-state').show();
                $('#state_id').prop('required',true);

            }else{
                $('#box-state').hide();
                $('#state_id').empty();
                $('.state_id').hide();
                $('.city_id').hide();
                $('.city_sub1_id').hide();
                $('#state_id').prop('required',false);

            }

        }
    });
});

$('#state_id').on('change',function(e){
    var text=  $("#city_id option:first").text();
    var text1=  $("#city_sub1_id option:first").text();
    var country_id=$("#country_id").val();
    $.ajax({
        type:'post',
        url:SP_source() + 'ajax/get-city',
        data:{'country_id':country_id,'state_id':e.target.value,csrf_token: $('[name="csrf_token"]').attr('content')},
        success:function(data){
            if(data!= ''){
                $('#city_id').empty();
                $('#city_id').append("<option value=''>"+ text +"</option>");
                $('#city_sub1_id').empty();
                $('#city_sub1_id').append("<option value=''>"+ text1 +"</option>");
                $("#zip_code").val('');
                $('#city_id').append(data);
                $('.city_id').show();
            }else{
                $('#city_id').empty();
                $('.city_id').hide();
                $('.city_sub1_id').hide();
            }

        }
    });
});

$('#city_id').on('change',function(e){
    var text=  $("#city_sub1_id option:first").text();
    var country_id=$("#country_id").val();
    var state_id=$("#state_id").val();

    $.ajax({
        type:'post',
        url:SP_source() + 'ajax/get-city-sub',
        data:{'country_id':country_id,'state_id':state_id,'city_id':e.target.value,csrf_token: $('[name="csrf_token"]').attr('content')},
        success:function(data){
            if(data!= ''){
                $('#city_sub1_id').empty();
                $('#city_sub1_id').append("<option value=''>"+ text +"</option>");
                $('#city_sub1_id').append(data);
                $("#zip_code").val('');
                $('.city_sub1_id').show();
            }else{
                $('#city_sub1_id').empty();
                $('.city_sub1_id').hide();
            }
        }
    });
});

$('#city_sub1_id').on('change',function(e){
    var text=  $("#city_sub1_id option:first").text();

    var country_id=$("#country_id").val();
    var state_id=$("#state_id").val();
    var city_id=$("#city_id").val();

    $.ajax({
        type:'post',
        url:SP_source() + 'ajax/get-zipcode',
        data:{'country_id':country_id,'state_id':state_id,'city_id':city_id,'city_sub_id':e.target.value,csrf_token: $('[name="csrf_token"]').attr('content')},
        success:function(data){
            $("#zip_code").val(data);

            if(data!=''){
                $("#zip_code").attr('readonly', true);
            }else{
                $("#zip_code").attr('readonly', false);
            }
        }
    });
});/**
 * Created by narong on 10/27/2018.
 */
